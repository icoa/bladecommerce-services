<?php

namespace services\Transformers;

use League\Fractal;
use AttributeOption;
use Site;

class BuilderAttributeTransformer extends Fractal\TransformerAbstract
{
    public function transform(AttributeOption $model)
    {
        return [
            'id' => $model->id,
            'active' => false,
            'img' => Site::img("/media/images/configuratore/icons/{$model->uname}.png"),
            'imgRetina' => Site::img("/media/images/configuratore/icons/{$model->uname}@2x.png"),
            'code' => $model->uname,
            'name' => $model->name,
        ];
    }
}