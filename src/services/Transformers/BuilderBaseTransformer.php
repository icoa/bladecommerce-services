<?php

namespace services\Transformers;

use League\Fractal;
use Product;
use Site;

class BuilderBaseTransformer extends Fractal\TransformerAbstract
{
    public function transform(Product $model)
    {
        $model->setFullData();
        $versioning = config('builder.versioning');
        return [
            'id' => $model->id,
            'sku' => $model->sku,
            'name' => $model->name,
            'link' => $model->link,
            'price' => (float)$model->price_label,
            'formattedPrice' => $model->price_final,
            'images' => [
                'base' => Site::img("/images/base/assets/products/builder/$model->sku.png?v=$versioning"),
                'thumbImg' => $model->thumbImg,
                'smallImg' => $model->smallImg,
                'defaultImg' => $model->defaultImg,
                'zoomImg' => $model->zoomImg,
                'thumbImgRetina' => $model->thumbImgRetina,
                'smallImgRetina' => $model->smallImgRetina,
                'defaultImgRetina' => $model->defaultImgRetina,
                'zoomImgRetina' => $model->zoomImgRetina,
            ]
        ];
    }
}