<?php

namespace services\Transformers;

use League\Fractal;
use Product;
use Site;

class BuilderDropletTransformer extends Fractal\TransformerAbstract
{
    public function transform(Product $model)
    {
        $model->setFullData();
        $model->load('builder_product');

        $versioning = config('builder.versioning');

        $drag = "/assets/products/builder/$model->sku.png?v=$versioning";
        $dragPath = "/assets/products/builder/$model->sku.png";

        $doubleSize = false;
        if ($model->builder_product->hasParam('doubleSize') and $model->builder_product->getParam('doubleSize')) {
            $drag = "/images/droplet/assets/products/builder/$model->sku@2x.png?v=$versioning";
            $doubleSize = true;
        }

        $offsetTop = $model->builder_product->getParam('offsetTop', 0);


        return [
            'id' => $model->id,
            'sku' => $model->sku,
            'name' => $model->name,
            'link' => $model->link,
            'price' => (float)$model->price_label,
            'formattedPrice' => $model->price_final,
            'offsetY' => $doubleSize ? -40 : -40,
            'offsetX' => $doubleSize ? -80 : -40,
            'width' => $doubleSize ? 160 : 80,
            'height' => $doubleSize ? 160 : 80,
            'attachment' => $model->builder_product->attachment,
            'doubleSize' => $doubleSize,
            'offsetTop' => $offsetTop,
            'images' => [
                'drag' => Site::img($drag),
                'dragRetina' => $doubleSize ? Site::img("/images/droplet/assets/products/builder/$model->sku@2x.png?v=$versioning") : Site::img("/images/droplet/assets/products/builder/$model->sku.png?v=$versioning"),
                'dragHalf' => Site::img("/images/droplet/assets/products/builder/$model->sku.png?v=$versioning"),
                'dragPath' => $dragPath,
            ]
        ];
    }
}