<?php

namespace services\Transformers;

use League\Fractal;
use Product;
use Site;

class BuilderBaseItemTransformer extends Fractal\TransformerAbstract
{
    public function transform(Product $model)
    {
        $model->setFullData();
        $blueprint_slots = $model->builder_product->blueprint->params['slots'];
        $slots = [];
        audit(compact('blueprint_slots'));
        foreach ($blueprint_slots as $blueprint_slot) {
            $slot = $blueprint_slot;
            $slot['x'] /= 2;
            $slot['y'] /= 2;
            $slot['width'] /= 2;
            $slot['height'] /= 2;
            $slot['centerX'] = $slot['x'] + $slot['width'] / 2;
            $slot['centerY'] = $slot['y'] + $slot['height'] / 2;

            $top = [
                'x' => $slot['centerX'],
                'y' => $slot['y'],
            ];
            $center = [
                'x' => $slot['centerX'],
                'y' => $slot['centerY'],
            ];
            $bottom = [
                'x' => $slot['centerX'],
                'y' => $slot['y'] + $slot['height'],
            ];

            $slot['attachments'] = compact('top', 'center', 'bottom');

            $slot['droplet'] = null;
            $slot['cssClass'] = [
                'slot' => true,
                'empty' => true,
                'hover' => false,
            ];
            $slots[] = $slot;
        }
        $versioning = config('builder.versioning');
        return [
            'id' => $model->id,
            'secure_key' => \Format::secure_key(),
            'sku' => $model->sku,
            'name' => $model->name,
            'link' => $model->link,
            'price' => (float)$model->price_label,
            'formattedPrice' => $model->price_final,
            'collection' => $model->collection_name,
            'collection_id' => $model->collection_id,
            'category' => $model->full_category_name,
            'images' => [
                'base' => Site::img("/images/base/assets/products/builder/$model->sku.png?v=$versioning"),
                'baseRetina' => Site::img("/images/base/assets/products/builder/$model->sku@2x.png?v=$versioning"),
                'basePath' => "/assets/products/builder/$model->sku.png",
            ],
            'slots' => $slots
        ];
    }
}