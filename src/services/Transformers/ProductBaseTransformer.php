<?php

namespace services\Transformers;

use League\Fractal;
use Product;
use Site;

class ProductBaseTransformer extends Fractal\TransformerAbstract
{
    public function transform(Product $model)
    {
        $model->setFullData();
        return [
            'id' => $model->id,
            'sku' => $model->sku,
            'name' => $model->name,
            'link' => $model->link,
            'price' => $model->price_final_raw,
            'formattedPrice' => $model->price_final,
            'images' => [
                'thumbImg' => $model->thumbImg,
                'smallImg' => $model->smallImg,
                'defaultImg' => $model->defaultImg,
                'zoomImg' => $model->zoomImg,
                'thumbImgRetina' => $model->thumbImgRetina,
                'smallImgRetina' => $model->smallImgRetina,
                'defaultImgRetina' => $model->defaultImgRetina,
                'zoomImgRetina' => $model->zoomImgRetina,
            ]
        ];
    }
}