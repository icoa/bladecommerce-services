<?php

namespace services\Transformers;

use League\Fractal;
use Product;
use Site;

class BuilderCategoryTransformer extends Fractal\TransformerAbstract
{
    public function transform(Product $model)
    {
        $defaultCategory = $model->getDefaultCategory();
        $mainCategory = $model->getMainCategory();
        $model->setFullData();
        return [
            'id' => $defaultCategory->id,
            'name' => $defaultCategory->name,
            'parent_name' => $mainCategory->name,
            'images' => [
                'base' => Site::img("/images/base/assets/products/builder/$model->sku.png"),
                'thumbImg' => $model->thumbImg,
                'smallImg' => $model->smallImg,
                'defaultImg' => $model->defaultImg,
                'zoomImg' => $model->zoomImg,
                'thumbImgRetina' => $model->thumbImgRetina,
                'smallImgRetina' => $model->smallImgRetina,
                'defaultImgRetina' => $model->defaultImgRetina,
                'zoomImgRetina' => $model->zoomImgRetina,
            ]
        ];
    }
}