<?php

namespace services\Transformers;

use League\Fractal;
use Collection;
use Site;

class BuilderCollectionTransformer extends Fractal\TransformerAbstract
{
    public function transform(Collection $model)
    {
        return [
            'id' => $model->id,
            'name' => $model->name,
            'description' => $model->ldesc,
            'img' => $model->image_thumb,
        ];
    }
}