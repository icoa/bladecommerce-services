<?php

namespace services\Recaptcha;

use Illuminate\Support\ServiceProvider;

class RecaptchaServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     */
    public function boot()
    {
        $app = $this->app;
        $app['validator']->extend('recaptcha', function ($attribute, $value) use ($app) {
            return $app['recaptcha']->verifyResponse($value, $app['request']->getClientIp());
        });
        if ($app->bound('form')) {
            $app['form']->macro('recaptcha', function ($attributes = []) use ($app) {
                return $app['recaptcha']->display($attributes, $app->getLocale());
            });
        }
    }

    /**
     * Register the service provider.
     */
    public function register()
    {
        $this->app->singleton('recaptcha', function ($app) {
            $version = config('recaptcha.version', 'v2');
            switch ($version) {
                case 'v3':
                    $secret = config('recaptcha.v3.secret');
                    $sitekey = config('recaptcha.v3.sitekey');
                    break;

                case 'invisible':
                    $secret = config('recaptcha.invisible.secret');
                    $sitekey = config('recaptcha.invisible.sitekey');
                    break;

                default:
                    $secret = config('recaptcha.secret');
                    $sitekey = config('recaptcha.sitekey');
                    break;
            }
            return new Recaptcha(
                $secret,
                $sitekey,
                $version
            );
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['captcha'];
    }
}