<?php
namespace services\Recaptcha;

use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Client;

class Recaptcha
{
    const CLIENT_API = 'https://www.google.com/recaptcha/api.js';
    const VERIFY_URL = 'https://www.google.com/recaptcha/api/siteverify';
    /**
     * The recaptcha secret key.
     *
     * @var string
     */
    protected $secret;
    /**
     * The recaptcha sitekey key.
     *
     * @var string
     */
    protected $sitekey;
    /**
     * The recaptcha API version (v2|v3).
     *
     * @var string
     */
    protected $version;
    /**
     * @var \GuzzleHttp\Client
     */
    protected $http;

    /**
     * NoCaptcha.
     *
     * @param string $secret
     * @param string $sitekey
     */
    public function __construct($secret, $sitekey, $version)
    {
        $this->secret = $secret;
        $this->sitekey = $sitekey;
        $this->version = $version;
        $this->http = new Client(['timeout' => 2.0]);
    }

    /**
     * Render HTML captcha.
     *
     * @param array $domOrScript
     * @param array $attributes
     *
     * @return string
     */
    public function display($domOrScript = ['dom', 'script'], $attributes = [])
    {
        $attributes['data-sitekey'] = $this->sitekey;
        $lang = \Utils::getIsoCodeFromLang(\Core::getLang());
        if (array_key_exists('lang', $attributes)) {
            $lang = $attributes['lang'];
        } else {
            $attributes['lang'] = $lang;
        }
        $html = null;
        if ($this->isVersion3()) {
            $html = $this->displayV3($domOrScript, $attributes);
        }
        if ($this->isInvisible()) {
            $html = $this->displayInvisible($domOrScript, $attributes);
        }
        if (is_null($html)) {
            $html = $this->displayV2($domOrScript, $attributes);
        }
        return $html;
    }


    /**
     * Render HTML captcha v2.
     *
     * @param array $domOrScript
     * @param array $attributes
     *
     * @return string
     */
    private function displayV2($domOrScript = ['dom', 'script'], $attributes = [])
    {
        $html = "";
        if (in_array('dom', $domOrScript)) {
            $html .= '<div class="g-recaptcha"' . $this->buildAttributes($attributes) . '></div>';
        }
        if (in_array('script', $domOrScript)) {
            $html .= '<script src="' . $this->getJsLink($attributes['lang']) . '" async defer></script>';
        }
        return $html;
    }


    /**
     * Render HTML captcha Invisible.
     *
     * @param array $domOrScript
     * @param array $attributes
     *
     * @return string
     */
    private function displayInvisible($domOrScript = ['dom', 'script'], $attributes = [])
    {
        $html = "";
        if (in_array('dom', $domOrScript)) {
            $id = \Utils::randomString(16);
            $html .= '<div id="' . $id . '" class="g-recaptcha"' . $this->buildAttributes($attributes) . '></div>';
        }
        if (in_array('script', $domOrScript)) {
            $html .= <<<HTML
<!-- Recaptcha support functions -->
<script>
function _bladeGrecaptchaOnload() {
    $('.g-recaptcha').each(function(i, el){
        var div = $(el); var elId = div.attr('id');
        if(div.data('rendered') != 1){
            var _id = grecaptcha.render(elId, {
                'sitekey' : '$this->sitekey'
            });          
            div.data('rendered', 1);
        }          
    });
}
function _bladeRecaptchaReady(){
    if (window.jQuery) {
        jQuery(document).ready(_bladeGrecaptchaOnload);
    } else {
        defer_functions.push(_bladeGrecaptchaOnload);
    }
}
</script>

HTML;
            $html .= '<script src="' . $this->getJsLink($attributes['lang']) . '&onload=_bladeRecaptchaReady&render=explicit" async defer></script>';
        }
        return $html;
    }


    /**
     * Render HTML captcha v3.
     *
     * @param array $domOrScript
     * @param array $attributes
     *
     * @return string
     */
    private function displayV3($domOrScript = ['dom', 'script'], $attributes = [])
    {
        if (!isset($attributes['data-action'])) {
            $attributes['data-action'] = 'login';
        }
        $html = "";
        if (in_array('dom', $domOrScript)) {
            $html .= '<div class="g-recaptcha"' . $this->buildAttributes($attributes) . '></div>';
            $html .= '<input type="hidden" name="inner-action" value="' . $attributes['data-action'] . '">';
        }
        if (in_array('script', $domOrScript)) {
            $html .= <<<HTML
<script src='https://www.google.com/recaptcha/api.js?render=$this->sitekey'></script>                
<script>
function _bladeGrecaptchaCustomLoad() {
    $('.g-recaptcha').each(function(i, el){
        var div = $(el);
        grecaptcha.execute('$this->sitekey', {action: div.data('action')}).then(function(token) {
             var input = $('<input type="hidden" name="g-recaptcha-response">').attr('value', token);
             div.append(input);
        });   
      });
    }
        
  grecaptcha.ready(function() {  
    if (window.jQuery) {
        jQuery(document).ready(_bladeGrecaptchaCustomLoad);
    } else {
        defer_functions.push(_bladeGrecaptchaCustomLoad);
    }         
  });
</script>
HTML;
        }
        return $html;
    }

    /**
     * Verify recaptcha response.
     *
     * @param string $response
     * @param string $action
     * @param string $clientIp
     *
     * @return bool|array
     */
    public function verifyResponse($response, $action = 'login', $clientIp = null)
    {
        if (empty($response)) {
            return false;
        }
        if (is_null($clientIp)) {
            $clientIp = \Request::getClientIp();
        }
        return $this->sendRequestVerify([
            'secret' => $this->secret,
            'response' => $response,
            'remoteip' => $clientIp,
        ], $action);
    }

    /**
     * Verify recaptcha response by Symfony Request.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function verifyRequest(Request $request)
    {
        return $this->verifyResponse(
            $request->get('g-recaptcha-response'),
            $request->getClientIp()
        );
    }

    /**
     * Get recaptcha js link.
     *
     * @param string $lang
     *
     * @return string
     */
    public function getJsLink($lang = null)
    {
        return $lang ? static::CLIENT_API . '?hl=' . $lang : static::CLIENT_API;
    }

    /**
     * Send verify request.
     *
     * @param array $query
     * @param string $givenAction
     *
     * @return array|bool
     */
    protected function sendRequestVerify(array $query = [], $givenAction = 'login')
    {
        $url = static::VERIFY_URL . '?' . http_build_query($query);
        $givenHostname = \Request::getHost();
        //audit(compact('query', 'url', 'givenAction', 'givenHostname'), __METHOD__ . '::requestParams');
        $checkResponse = null;
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_TIMEOUT, config('recaptcha.curl_timeout', 1));
        if(\App::environment() !== 'live'){
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        }
        $checkResponse = curl_exec($curl);
        audit($checkResponse, __METHOD__ . '::requestResponse');
        if (false === $checkResponse) {
            app('log')->error('[Recaptcha] CURL error: ' . curl_error($curl));
            audit('[Recaptcha] CURL error: ' . curl_error($curl));
            audit_error('[Recaptcha] CURL error: ' . curl_error($curl));
        }
        if (is_null($checkResponse) || empty($checkResponse) || $checkResponse === false) {
            return false;
        }
        $decodedResponse = json_decode($checkResponse, true);
        $valid = $decodedResponse['success'];

        //temp fix for timeout
        if ($valid != true) {
            if (isset($decodedResponse['error-codes']) and is_array($decodedResponse['error-codes'])) {
                foreach ($decodedResponse['error-codes'] as $error) {
                    if ($error == 'timeout-or-duplicate') {
                        $valid = true;
                        //audit_track($query, __METHOD__ . '::captchaParams | CAPTCHA VERIFICATION | TEMPORARY PASSING TIMEOUT');
                        //audit_track($decodedResponse, __METHOD__ . '::decodedResponse | CAPTCHA VERIFICATION');
                        $input = \Request::all();
                        //remote visible password from logs
                        if (isset($input['passwd'])) {
                            unset($input['passwd']);
                        }
                        if (isset($input['confirm_passwd'])) {
                            unset($input['confirm_passwd']);
                        }
                        //audit_track($input, __METHOD__ . '::requestParams | CAPTCHA VERIFICATION');
                    }
                }
            }
        }

        //if version is v3, then we have to check the minimum value for that action
        if ($this->isVersion3() and $valid) {
            $score = isset($decodedResponse['score']) ? $decodedResponse['score'] : null;
            $action = isset($decodedResponse['action']) ? $decodedResponse['action'] : null;
            $hostname = isset($decodedResponse['hostname']) ? $decodedResponse['hostname'] : null;

            if (!is_null($score) and !is_null($action)) {
                $minimumScore = config('recaptcha.v3.actions.' . $action, 0.5);
                audit("Minimum score for action '$action' is $minimumScore", __METHOD__);
                if ($score < $minimumScore) {
                    //audit_track("Minimum score ($score) not satisfied for action [$givenAction]");
                    //if we don't have minimum score, then check if the action and the hostname are matched
                    if ($score != 0 and $action == $givenAction and $hostname == $givenHostname) {
                        $valid = true;
                    } else {
                        $valid = false;
                    }
                }
            }
        }
        if (false === $valid) {
            //audit_track($query, __METHOD__ . '::captchaParams | CAPTCHA VERIFICATION');
            //audit_track($decodedResponse, __METHOD__ . '::decodedResponse | CAPTCHA VERIFICATION');
            $input = \Request::all();
            //remote visible password from logs
            if (isset($input['passwd'])) {
                unset($input['passwd']);
            }
            if (isset($input['confirm_passwd'])) {
                unset($input['confirm_passwd']);
            }
            //audit_track($input, __METHOD__ . '::requestParams | CAPTCHA VERIFICATION');
        }
        return $valid;
    }

    /**
     * Build HTML attributes.
     *
     * @param array $attributes
     *
     * @return string
     */
    protected function buildAttributes(array $attributes)
    {
        $html = [];
        foreach ($attributes as $key => $value) {
            $html[] = $key . '="' . $value . '"';
        }
        return count($html) ? ' ' . implode(' ', $html) : '';
    }

    /**
     * @return bool
     */
    public function isVersion3()
    {
        return $this->version == 'v3';
    }

    /**
     * @return bool
     */
    public function isInvisible()
    {
        return $this->version == 'invisible';
    }
}