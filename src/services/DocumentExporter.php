<?php  namespace services;


use Order;
use SimpleXMLElement;
use Cfg;
use DB;
use Format;


class DocumentExporter {

    public $xml;
    protected $params;
    protected $config;

    function __construct($params){
        $this->config = Cfg::getMultiple('DANEA_ORDER_STATUS,DANEA_PAYMENT_STATUS,DANEA_ORDER_FIX,ORDER_REFERENCE_PREFIX,DANEA_SHIPMENT_TERMS');
        $this->params = $params;
    }

    protected function DbToXml($string){
        return htmlspecialchars($string, ENT_NOQUOTES, 'UTF-8');
        /*return htmlspecialchars(mb_convert_encoding($string, 'UTF-8', 'Windows-1252'), ENT_NOQUOTES, 'Windows-1252');
        return mb_convert_encoding(htmlspecialchars($string, ENT_NOQUOTES, 'Windows-1252'), 'UTF-8', 'Windows-1252');*/
    }

    public function export()
    {
        ini_set('max_execution_time',3600);
        $config = $this->config;
        $params = $this->params;
        $status = explode(",",$config['DANEA_ORDER_STATUS']);
        $paymentStatus = explode(",",$config['DANEA_PAYMENT_STATUS']);


        $queryBuilder = Order::whereIn('status',$status)
            ->whereIn('payment_status',$paymentStatus)
            ->where('created_at','<=',$params['lastdate'])
            ->where('created_at','>=',$params['firstdate']);

        if($params['firstnum'] > 0){
            $queryBuilder->having('number','>=',$params['firstnum']);
        }
        if($params['lastnum'] > 0){
            $queryBuilder->having('number','<=',$params['lastnum']);
        }
        $prefix = $config['ORDER_REFERENCE_PREFIX'];
        $rows = $queryBuilder->select(['*',DB::raw("CONVERT(SUBSTRING_INDEX(reference,'$prefix',-1),UNSIGNED INTEGER) as number")])->get();

        $this->xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><EasyfattDocuments AppVersion="2" />');

        $this->createCompanyNode();

        $nodeDocuments = $this->xml->addChild('Documents');

        foreach ($rows as $order)
        {
            $nodeDocument = $nodeDocuments->addChild('Document');
            $this->createDocumentNode($order, $nodeDocument);
        }

        return $this->xml;
    }

    private function createCompanyNode()
    {
        $cfg = Cfg::getGroup('general');
        $country = \Country::getObj($cfg['SHOP_COUNTRY_ID']);
        $countryName = $country ? $country->name : '';

        $state = \State::getObj($cfg['SHOP_STATE_ID']);
        $stateName = $state ? $state->name : '';

        $nodeCompany = $this->xml->addChild('Company');
        $this->addNode($nodeCompany, 'Name', $cfg['SHOP_NAME']);
        $this->addNode($nodeCompany, 'Address', $cfg['SHOP_ADDRESS']);
        $this->addNode($nodeCompany, 'Postcode', $cfg['SHOP_CODE']);
        $this->addNode($nodeCompany, 'City', $cfg['SHOP_CITY']);
        $this->addNode($nodeCompany, 'Province', $stateName);
        $this->addNode($nodeCompany, 'Country', $countryName);
        $this->addNode($nodeCompany, 'Tel', $cfg['SHOP_PHONE']);
        $this->addNode($nodeCompany, 'Fax', $cfg['SHOP_FAX']);
        $this->addNode($nodeCompany, 'Email', $cfg['SHOP_EMAIL']);
        $this->addNode($nodeCompany, 'HomePage', \Site::root());
    }



    private function createDocumentNode($order, SimpleXMLElement $node)
    {
        $this->addNode($node, 'DocumentType', 'C');
        $this->addCustomerNodes($node, $order);
        $this->addDeliveryNodes($node, $order);
        $this->addNode($node, 'Date', date("Y-m-d",strtotime($order->created_at)));
        $orderFix = $this->config['DANEA_ORDER_FIX'];
        $this->addNode($node, 'Number', $order->number);
        if(trim($orderFix) != ''){
            $this->addNode($node, 'Numbering', '/'.trim($orderFix));
        }
        $this->addNode($node, 'InternalComment', "Ordine N. " . $order->reference);
        $this->addNode($node, 'PaymentName', $order->payment);

        $costDescription = [];
        $cost = 0;
        $cost_vat_code = 0;
        if($order->total_shipping > 0){
            $costDescription[] = 'Costi spedizione';
            $cost += $order->total_shipping;
            $cost_vat_code = $order->carrier_tax_rate;
            if ($cost_vat_code == 0) {
                $cost_vat_code = \Core::getDefaultTaxRate();
            }
            $cost_vat_code = ($cost_vat_code - 1) * 100;
        }
        if($order->total_payment > 0){
            $costDescription[] = 'Costi metodo pagamento';
            $cost += $order->total_payment;
        }
        if($order->total_wrapping > 0){
            $costDescription[] = 'Costi imballaggio';
            $cost += $order->total_wrapping;
        }

        $this->addNode($node, 'CostDescription', implode(", ",$costDescription));
        $this->addNode($node, 'CostVatCode', $cost_vat_code);
        $this->addNode($node, 'CostAmount', $cost);
        $PricesIncludeVat = $order->total_taxes > 0 ? 'true' : 'false';
        $ShipmentTerms = $this->config['DANEA_SHIPMENT_TERMS'];
        $this->addNode($node, 'Total', Format::float($order->total_order));
        $this->addNode($node, 'PricesIncludeVat', $PricesIncludeVat);
        $this->addNode($node, 'ShipmentTerms', $ShipmentTerms);
        $this->addNode($node, 'Carrier', $order->carrierName());
        $this->addNode($node, 'TransportedWeight', Format::float($order->total_weight));
        $this->addNode($node, 'TrackingNumber', $order->shipping_number);
        $this->addNode($node, 'CustomField1', $order->notes);
        $this->addNode($node, 'CustomField2', $order->carrierName());
        $this->addNode($node, 'CustomField3', $order->coupon_code);
        $this->addNode($node, 'FootNotes', '');
        $this->addNode($node, 'ExpectedConclusionDate', '');
        $nodeItems = $node->addChild('Rows');
        $items = $order->getProducts( false );
        foreach ($items as $item)
        {
            $nodeRow = $nodeItems->addChild('Row');
            $this->createRowNode($item, $nodeRow);
        }
    }

    private function addCustomerNodes(SimpleXMLElement $node, $order)
    {
        $customer = $order->getCustomer();
        $customer->full();
        $this->addNode($node, 'CustomerWebLogin', $customer->email);
        $this->addNode($node, 'CustomerName', $customer->getName());
        $this->addNode($node, 'CustomerAddress', $customer->address);
        $this->addNode($node, 'CustomerPostcode', $customer->postcode);
        $this->addNode($node, 'CustomerCity', $customer->city);
        //$this->addNode($node, 'CustomerProvince', $customer->stateName);
        $this->addNode($node, 'CustomerProvince', $customer->stateCode);
        $this->addNode($node, 'CustomerCountry', $customer->countryName);
        $this->addNode($node, 'CustomerFiscalCode', $customer->cf);
        $this->addNode($node, 'CustomerVatCode', $customer->vat_number);
        $this->addNode($node, 'CustomerTel', $customer->phone);
        $this->addNode($node, 'CustomerCellPhone', $customer->phone_mobile);
        $this->addNode($node, 'CustomerFax', $customer->fax);
        $this->addNode($node, 'CustomerEmail', $customer->email);
    }

    private function addDeliveryNodes(SimpleXMLElement $node, $order)
    {
        $shipping = $order->getShippingAddress();
        $this->addNode($node, 'DeliveryName', $shipping->name);
        $this->addNode($node, 'DeliveryAddress', $shipping->address);
        $this->addNode($node, 'DeliveryPostcode', $shipping->postcode);
        $this->addNode($node, 'DeliveryCity', $shipping->city);
        //$this->addNode($node, 'DeliveryProvince', $shipping->stateName);
        $this->addNode($node, 'DeliveryProvince', $shipping->stateCode);
        $this->addNode($node, 'DeliveryCountry', $shipping->countryName);
    }

    private function addCarrierNodes(SimpleXMLElement $node, $order)
    {
        $this->addNode($node, 'Carrier', $order->carrier->carrier);
        $this->addNode($node, 'TransportReason', $order->carrier->transport_reason);
        $this->addNode($node, 'GoodsAppearance', $order->carrier->goods_appearance);
        $this->addNode($node, 'NumOfPieces', $order->carrier->num_of_pieces);
        $this->addNode($node, 'TransportDateTime', $order->carrier->transport_date_time);
        $this->addNode($node, 'ShipmentTerms', $order->carrier->shipment_terms);
        $this->addNode($node, 'TransportedWeight', $order->carrier->transported_weight);
        $this->addNode($node, 'TrackingNumber', $order->carrier->tracking_number);
    }

    private function createRowNode($item, SimpleXMLElement $node)
    {
        $this->addNode($node, 'Code', $item->product_reference);
        $this->addNode($node, 'Description', $item->product_name);
        $this->addNode($node, 'Qty', $item->product_quantity);
        $this->addNode($node, 'Um', 'pz');
        $this->addNode($node, 'Price', Format::float($item->product_item_price));
        if($item->reduction_amount > 0){
            $this->addNode($node, 'Discounts', Format::float($item->reduction_amount));
        }
        if($item->reduction_percent > 0){
            $this->addNode($node, 'Discounts', Format::float($item->reduction_percent)."%");
        }
        $taxRate = (float)str_replace(",",".",$item->tax_rate);
        $VatCode = ($taxRate - 1) * 100;
        $this->addNode($node, 'VatCode', $VatCode);
        $this->addAttribute($node->VatCode, 'Perc', $VatCode);
        $this->addAttribute($node->VatCode, 'Class', 'Imponibile');
        $this->addAttribute($node->VatCode, 'Description', 'Aliquota '.$VatCode.'%');
    }

    private function createPaymentNode($payment, SimpleXMLElement $node)
    {
        $this->addNode($node, 'Advance', $payment->advance, true);
        $this->addNode($node, 'Date', $payment->date);
        $this->addNode($node, 'Amount', $payment->amount);
        $this->addNode($node, 'Paid', $payment->paid, true);
    }

    private function addNode(SimpleXMLElement $parent, $prop, $value, $isBoolean = false, $mandatory = false)
    {
        if ($isBoolean) {
            $value = ($value === false or $value === 0) ? "false" : $value;
            $value = ($value === true or $value === 1) ? "true" : $value;
        }

        if (($value and strlen($value) > 0) or $mandatory)
        {
            $value = $this->DbToXml($value);
            $parent->addChild($prop, $value);
        }
    }

    private function addAttribute(SimpleXMLElement $node, $prop, $value, $isBoolean = false, $mandatory = false)
    {
        if ($isBoolean) {
            $value = ($value === false or $value === 0) ? "false" : $value;
            $value = ($value === true or $value === 1) ? "true" : $value;
        }

        if (($value and strlen($value) > 0) or $mandatory)
        {
            $value = $this->DbToXml($value);
            $node->addAttribute($prop, $value);
        }
    }
}

class DocumentExporterEzi {

    public $xml;

    public function export()
    {
        $this->xml = new SimpleXMLElement('<?xml version="1.0" encoding="UTF-8"?><EasyfattDocuments AppVersion="2" Creator="Danea Easyfatt Enterprise  2011.15" CreatorUrl="http://www.danea.it/software/easyfatt" />');

        $this->createCompanyNode();

        $nodeDocuments = $this->xml->addChild('Documents');

        $orders = Order::with(array('items', 'customer', 'shipping', 'billing', 'carrier', 'payments'))->get();
        foreach ($orders as $order)
        {
            $nodeDocument = $nodeDocuments->addChild('Document');
            $this->createDocumentNode($order, $nodeDocument);
        }

        //dd($this->xml->Documents[0]->Rows);
        //echo $this->xml;

        return true;
    }

    private function createCompanyNode()
    {
        $company = Company::orderByRaw("RAND()")->first();

        $nodeCompany = $this->xml->addChild('Company');
        $this->addNode($nodeCompany, 'Name', $company->name);
        $this->addNode($nodeCompany, 'Address', $company->address);
        $this->addNode($nodeCompany, 'Postcode', $company->postcode);
        $this->addNode($nodeCompany, 'City', $company->city);
        $this->addNode($nodeCompany, 'Province', $company->province);
        $this->addNode($nodeCompany, 'Country', $company->country);
        $this->addNode($nodeCompany, 'FiscalCode', $company->fiscal_code);
        $this->addNode($nodeCompany, 'VatCode', $company->vat_code);
        $this->addNode($nodeCompany, 'Tel', $company->tel);
        $this->addNode($nodeCompany, 'Fax', $company->fax);
        $this->addNode($nodeCompany, 'Email', $company->email);
        $this->addNode($nodeCompany, 'HomePage', $company->home_page);
    }

    private function createDocumentNode($order, SimpleXMLElement $node)
    {
        $this->addNode($node, 'DocumentType', $order->type);
        $this->addNode($node, 'Date', $order->date);
        $this->addNode($node, 'Number', $order->number);
        $this->addNode($node, 'Numbering', $order->numbering);

        $this->addNode($node, 'CostDescription', $order->cost_description);
        $this->addNode($node, 'CostVatCode', $order->cost_vat_code);
        $this->addAttribute($node->CostVatCode, 'Perc', $order->cost_vat_code_perc);
        $this->addAttribute($node->CostVatCode, 'Class', $order->cost_vat_code_class);
        $this->addAttribute($node->CostVatCode, 'Description', $order->cost_vat_code_description);
        $this->addNode($node, 'CostAmount', $order->cost_amount);
        $this->addNode($node, 'TotalWithoutTax', $order->total_without_tax);
        $this->addNode($node, 'VatAmount', $order->vat_amount);
        $this->addNode($node, 'Total', $order->total);
        $this->addNode($node, 'PaymentAdvanceAmount', $order->payment_advance_amount);
        $this->addNode($node, 'PricesIncludeVat', $order->prices_include_vat, true);
        $this->addNode($node, 'WithholdingTaxPerc', $order->withholding_tax_perc);
        $this->addNode($node, 'WithholdingTaxPerc2', $order->withholding_tax_perc2);
        $this->addNode($node, 'WithholdingTaxAmount', $order->withholding_tax_amount);
        $this->addNode($node, 'WithholdingTaxAmountB', $order->withholding_tax_amount_b);
        $this->addNode($node, 'WithholdingTaxNameB', $order->withholding_tax_name_b);
        $this->addNode($node, 'ContribDescription', $order->contrib_description);
        $this->addNode($node, 'ContribPerc', $order->contrib_perc);
        $this->addNode($node, 'ContribSubjectToWithholdingTax', $order->contrib_subject_to_withholding_tax, true);
        $this->addNode($node, 'ContribAmount', $order->contrib_amount);
        $this->addNode($node, 'ContribVatCode', $order->contrib_vat_code);
        $this->addNode($node, 'DelayedVat', $order->delayed_vat, true);
        $this->addNode($node, 'DelayedVatDesc', $order->delayed_vat_desc);
        $this->addNode($node, 'DelayedVatDueWithinOneYear', $order->delayed_vat_due_within_one_year, true);
        $this->addNode($node, 'Warehouse', $order->warehouse);
        $this->addNode($node, 'PriceList', $order->price_list);
        $this->addNode($node, 'PaymentName', $order->payment_name);
        $this->addNode($node, 'PaymentBank', $order->payment_bank);
        $this->addNode($node, 'SalesAgent', $order->sales_agent);
        $this->addNode($node, 'ExpectedConclusionDate', $order->expected_conclusion_date);
        $this->addNode($node, 'DocReference', $order->doc_reference);

        $this->addNode($node, 'InternalComment', $order->internal_comment);
        $this->addNode($node, 'CustomField1', $order->custom_field1);
        $this->addNode($node, 'CustomField2', $order->custom_field2);
        $this->addNode($node, 'CustomField3', $order->custom_field3);
        $this->addNode($node, 'CustomField4', $order->custom_field4);
        $this->addNode($node, 'FootNotes', $order->foot_notes);

        $this->addCustomerNodes($node, $order);

        $this->addDeliveryNodes($node, $order);

        $this->addCarrierNodes($node, $order);

        $nodeItems = $node->addChild('Rows');
        $items = $order->items;
        foreach ($items as $item)
        {
            $nodeRow = $nodeItems->addChild('Row');
            $this->createRowNode($item, $nodeRow);
        }

        $nodePayments = $node->addChild('Payments');
        $payments = $order->payments;
        foreach ($payments as $payment)
        {
            $nodePayment = $nodePayments->addChild('Payment');
            $this->createPaymentNode($payment, $nodePayment);
        }
    }

    private function addCustomerNodes(SimpleXMLElement $node, $order)
    {
        $this->addNode($node, 'CustomerCode', $order->customer->code);
        $this->addNode($node, 'CustomerWebLogin', $order->customer->web_login);
        $this->addNode($node, 'CustomerName', $order->customer->name);
        $this->addNode($node, 'CustomerAddress', $order->customer->address);
        $this->addNode($node, 'CustomerPostcode', $order->customer->postcode);
        $this->addNode($node, 'CustomerCity', $order->customer->city);
        $this->addNode($node, 'CustomerProvince', $order->customer->province);
        $this->addNode($node, 'CustomerCountry', $order->customer->country);
        $this->addNode($node, 'CustomerFiscalCode', $order->customer->fiscal_code);
        $this->addNode($node, 'CustomerVatCode', $order->customer->vat_code);
        $this->addNode($node, 'CustomerTel', $order->customer->tel);
        $this->addNode($node, 'CustomerCellPhone', $order->customer->cell_phone);
        $this->addNode($node, 'CustomerFax', $order->customer->fax);
        $this->addNode($node, 'CustomerEmail', $order->customer->email);
        $this->addNode($node, 'CustomerPec', $order->customer->pec);
        $this->addNode($node, 'CustomerReference', $order->customer->reference);
    }

    private function addDeliveryNodes(SimpleXMLElement $node, $order)
    {
        $this->addNode($node, 'DeliveryName', $order->shipping->name);
        $this->addNode($node, 'DeliveryAddress', $order->shipping->address);
        $this->addNode($node, 'DeliveryPostcode', $order->shipping->postcode);
        $this->addNode($node, 'DeliveryCity', $order->shipping->city);
        $this->addNode($node, 'DeliveryProvince', $order->shipping->province);
        $this->addNode($node, 'DeliveryCountry', $order->shipping->country);
    }

    private function addCarrierNodes(SimpleXMLElement $node, $order)
    {
        $this->addNode($node, 'Carrier', $order->carrier->carrier);
        $this->addNode($node, 'TransportReason', $order->carrier->transport_reason);
        $this->addNode($node, 'GoodsAppearance', $order->carrier->goods_appearance);
        $this->addNode($node, 'NumOfPieces', $order->carrier->num_of_pieces);
        $this->addNode($node, 'TransportDateTime', $order->carrier->transport_date_time);
        $this->addNode($node, 'ShipmentTerms', $order->carrier->shipment_terms);
        $this->addNode($node, 'TransportedWeight', $order->carrier->transported_weight);
        $this->addNode($node, 'TrackingNumber', $order->carrier->tracking_number);
    }

    private function createRowNode($item, SimpleXMLElement $node)
    {
        $this->addNode($node, 'Code', $item->code);
        $this->addNode($node, 'SupplierCode', $item->supplier_code);
        $this->addNode($node, 'Description', $item->description);
        $this->addNode($node, 'Qty', $item->quantity);
        $this->addNode($node, 'Um', $item->um);
        $this->addNode($node, 'Size', $item->size);
        $this->addNode($node, 'Color', $item->color);
        $this->addNode($node, 'Lot', $item->lot);
        $this->addNode($node, 'ExpireDate', $item->expiry_date);
        $this->addNode($node, 'Serial', $item->serial);
        $this->addNode($node, 'Price', $item->price);
        $this->addNode($node, 'Discounts', $item->discounts);
        $this->addNode($node, 'EcoFee', $item->eco_fee);
        $this->addNode($node, 'Vat', $item->vat);
        $this->addAttribute($node->Vat, 'Perc', $item->vat_perc);
        $this->addAttribute($node->Vat, 'Class', $item->vat_class);
        $this->addAttribute($node->Vat, 'Description', $item->vat_description);
        $this->addNode($node, 'Total', $item->total);
        $this->addNode($node, 'WithholdingTax', $item->withholding_tax);
        $this->addNode($node, 'Stock', $item->stock);
        $this->addNode($node, 'Notes', $item->notes);
        $this->addNode($node, 'CommissionPerc', $item->commission_perc);
    }

    private function createPaymentNode($payment, SimpleXMLElement $node)
    {
        $this->addNode($node, 'Advance', $payment->advance, true);
        $this->addNode($node, 'Date', $payment->date);
        $this->addNode($node, 'Amount', $payment->amount);
        $this->addNode($node, 'Paid', $payment->paid, true);
    }

    private function addNode(SimpleXMLElement $parent, $prop, $value, $isBoolean = false, $mandatory = false)
    {
        if ($isBoolean) {
            $value = ($value === false or $value === 0) ? "false" : $value;
            $value = ($value === true or $value === 1) ? "true" : $value;
        }

        if (($value and strlen($value) > 0) or $mandatory)
        {
            $parent->addChild($prop, $value);
        }
    }

    private function addAttribute(SimpleXMLElement $node, $prop, $value, $isBoolean = false, $mandatory = false)
    {
        if ($isBoolean) {
            $value = ($value === false or $value === 0) ? "false" : $value;
            $value = ($value === true or $value === 1) ? "true" : $value;
        }

        if (($value and strlen($value) > 0) or $mandatory)
        {
            $node->addAttribute($prop, $value);
        }
    }
}