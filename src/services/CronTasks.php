<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 09/10/2015
 * Time: 13:33
 */

namespace services;

use Exception;
use Symfony\Component\Finder\Finder;
use Core\SitemapBuilder;

class CronTasks
{
    protected static $pretend = false;
    protected static $messages = [];

    static function pretend()
    {
        self::$pretend = true;
    }

    static function log($str, $prefix = null)
    {
        self::$messages[] = $prefix ? "[$prefix] $str" : $str;
    }

    static function getMessages()
    {
        return self::$messages;
    }

    static private function buildSitemap($lang, $type = null)
    {
        if (self::$pretend) {
            self::log("Pretending execution of: " . __METHOD__);
            return;
        }
        $languages = \Core::getLanguages();
        if (in_array($lang, $languages)) {
            $sitemap = new SitemapBuilder($lang);
            $sitemap->buildSitemap($type);
        }
    }


    static private function updateFromFixer()
    {
        $endpoint = 'latest';
        $access_key = 'd98ac84fcd6c9ef38e40cc9e093d377e';

        $uri = 'http://data.fixer.io/api/' . $endpoint . '?base=EUR&access_key=' . $access_key;
        $json = self::request($uri);

        // Decode JSON response:
        $exchangeRates = json_decode($json, true);
        audit($exchangeRates);
        if (is_array($exchangeRates) and $exchangeRates['success'] == 1) {
            foreach ($exchangeRates['rates'] as $code => $value) {
                \DB::table('currencies')
                    ->where('iso_code', $code)
                    ->update(array(
                        'conversion_rate' => $value,
                        'updated_at' => new \DateTime('now'),
                    ));
                $currency = \Currency::where('iso_code', $code)->first();
                if ($currency) {
                    $currency->uncache();
                }
                self::log("Updated currency [$code] to rate [$value]");
            }
            \Cache::forget('exchange.currency');
        }
    }

    static private function request($url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 20);
        curl_setopt($ch, CURLOPT_TIMEOUT, 20);
        curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1");
        curl_setopt($ch, CURLOPT_HTTPGET, true);
        curl_setopt($ch, CURLOPT_MAXREDIRS, 2);
        curl_setopt($ch, CURLOPT_MAXCONNECTS, 2);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }


    static function downloadCurrencies()
    {
        if (self::$pretend) {
            self::log("Pretending execution of: " . __METHOD__);
            return;
        }
        self::log(__METHOD__, 'RUNNING');
        $defaultCurrency = 'EUR';
        try {
            self::updateFromFixer();
            \Currency::initCache();
            print_r(self::getMessages());
        } catch (Exception $e) {
            self::log($e->getMessage(), 'ERROR');
            self::log($e->getTraceAsString());
        }
    }

    static function sitemap($type)
    {
        self::log(__METHOD__ . " [$type]", 'RUNNING');
        $languages = \Core::getLanguages();
        foreach ($languages as $lang) {
            try {
                self::buildSitemap($lang, $type);
            } catch (Exception $e) {
                \Utils::log($e->getMessage(), 'ERROR');
                self::log($e->getMessage(), 'ERROR');
                self::log($e->getTraceAsString());
            }
        }
    }

    static function googleMerchant($lang, $locale)
    {
        self::log(__METHOD__ . " [$lang,$locale]", 'RUNNING');
        try {
            $builder = new GoogleMerchantBuilder($lang, $locale);
            $builder->batch();
        } catch (Exception $e) {
            self::log($e->getMessage(), 'ERROR');
            self::log($e->getTraceAsString());
        }
    }

    static function zanox($lang, $locale)
    {
        self::log(__METHOD__ . " [$lang,$locale]", 'RUNNING');
        $builder = new ZanoxBuilder($lang, $locale);
        $builder->batch();
    }

    static function handleCache()
    {
        if (self::$pretend) {
            self::log("Pretending execution of: " . __METHOD__);
            return;
        }
        self::log(__METHOD__, 'RUNNING');
        $models = [
            '\Category',
            '\Brand',
            '\Collection',
            '\Trend',
            '\Nav',
            '\Attribute',
            '\AttributeOption',
            '\Section',
            '\Page',
            '\PriceRule',
            '\Lexicon',
        ];

        foreach ($models as $model) {
            try {
                $model::initCache();
            } catch (Exception $e) {
                self::log($e->getMessage(), 'ERROR');
                self::log($e->getTraceAsString());
            }
        }
        \Helper::uncache();
    }

    static function handleCacheAdvanced()
    {
        if (self::$pretend) {
            self::log("Pretending execution of: " . __METHOD__);
            return;
        }
        self::log(__METHOD__, 'RUNNING');
        $models = [
            '\Category',
            '\Brand',
            '\Collection',
            '\Trend',
            '\Nav',
            '\Attribute',
            '\AttributeOption',
            '\Section',
            '\Page',
            '\PriceRule',
        ];
        foreach ($models as $model) {
            $rows = $model::get();
            foreach ($rows as $row) {
                try {
                    $row->uncache();
                } catch (Exception $e) {
                    self::log($e->getMessage(), 'ERROR');
                    self::log($e->getTraceAsString());
                }
            }
        }
        //\RedirectLink::rebuildSchema();
    }

    static function handleCacheProducts($withMetadata = false)
    {
        if (self::$pretend) {
            self::log("Pretending execution of: " . __METHOD__);
            return;
        }
        self::log(__METHOD__, 'RUNNING');
        $rows = \Product::where('is_out_of_production', 0)->get();
        foreach ($rows as $row) {
            try {
                \ProductHelper::buildProductCache($row->id, $withMetadata);
            } catch (Exception $e) {
                self::log($e->getMessage(), 'ERROR');
                self::log($e->getTraceAsString());
            }
        }
        //\Product::initCache();
    }

    static function statesCheck()
    {
        if (self::$pretend) {
            self::log("Pretending execution of: " . __METHOD__);
            return;
        }
        self::log(__METHOD__, 'RUNNING');
        $rows = \PriceRule::get();
        $now = time();
        foreach ($rows as $row) {
            self::log("Checking promo [$row->id]");
            $from = strtotime($row->date_from);
            $to = strtotime($row->date_to);
            //start but no finish
            if ($from > 0 AND $now > $from AND $to == 0) {
                if ($row->active == 0) {
                    self::log("1 - This promo should be activated [$row->date_from]");
                    \ProductHelper::refreshPriceRule($row->id, 1);
                }
            }
            if ($to > 0 AND $now < $to AND $from == 0) {
                if ($row->active == 0) {
                    self::log("2 - This promo should be activated [$row->date_from]");
                    \ProductHelper::refreshPriceRule($row->id, 1);
                }
            }
            if ($to > 0 AND $now > $to AND $row->active == 1) {
                self::log("3 - This promo should be DE-activated [$row->date_to]");
                \ProductHelper::refreshPriceRule($row->id, 0);
            }
            if ($from > 0 AND $from > $now AND $row->active == 0) {
                self::log("4 - This promo should be DE-activated [$row->date_to]");
                \ProductHelper::refreshPriceRule($row->id, 0);
            }
        }
        \ProductHelper::updateProductsStates();
    }


    static function removeDeletedProducts()
    {
        if (self::$pretend) {
            self::log("Pretending execution of: " . __METHOD__);
            return;
        }
        self::log(__METHOD__, 'RUNNING');
        $query = "DELETE FROM cache_products WHERE id IN (SELECT id FROM products WHERE deleted_at IS NOT NULL)";
        \DB::statement($query);
    }

    static function report()
    {
        if (\Config::get('cron.sendReport')) {
            $subject = \Config::get('cron.sendReportSubject');
            $to = \Config::get('cron.sendReportTo');
            $from = \Config::get('cron.sendReportFrom');
            $url = \Config::get('app.url');
            $env = \App::environment();
            $report = "<h4>Cron job report for $url (env: '$env')";
            $logFile = storage_path("logs/cron-" . date('Y-m-d') . '.log');
            if (\File::exists($logFile)) {
                $fileContent = \File::get($logFile);
                $fileContent = str_replace(['cron_log.INFO: ', '[] []'], '', $fileContent);
                $report .= "<pre style='font-size:10px'>" . $fileContent . "</pre>";
                try {
                    \Mail::send('emails.default', ['body' => $report], function ($message) use ($to, $from, $subject) {
                        $message->from($from);
                        $message->to($to)->subject($subject);
                    });
                } catch (Exception $e) {
                    \Utils::error($e->getMessage(), __METHOD__);
                    \Utils::error($e->getTraceAsString(), __METHOD__);
                }
            }
        }
    }

    static function rebindImagesCounter()
    {
        $products = \Product::orderBy('id', 'desc')->select(['id'])->get();
        foreach ($products as $product) {
            $data = [];
            $data['cnt_images'] = \DB::table("images")->where("product_id", $product->id)->count();
            \DB::table('products')->where('id', $product->id)->update($data);
        }
    }


    static function wanted()
    {
        \ProductHelper::setMostWantedProducts();

    }

    static function killCache()
    {
        self::downloadCurrencies();
        self::handleCache();
        self::handleCacheAdvanced();
        self::handleCacheProducts();
        self::removeDeletedProducts();
        self::statesCheck();
    }

    static function killCacheImport()
    {
        self::downloadCurrencies();
        self::handleCache();
        self::handleCacheAdvanced();
        self::handleCacheProducts(true);
        self::removeDeletedProducts();
        self::statesCheck();
    }

    //php artisan cron:task googleAnalytics
    static function googleAnalytics()
    {
        try {
            $bust = date('dmY');
            $content = self::request('https://www.google-analytics.com/analytics.js');
            $host = str_replace(['http:', 'https:'], null, \Config::get('app.url'));
            $dir = public_path("cache/analytics");
            if (!is_dir($dir)) {
                mkdir($dir, 0755, true);
            }

            if ($content AND strlen($content) > 0) {
                $saveTo = public_path("cache/analytics/analytics.$bust.js");
                echo "Saving file to $saveTo" . PHP_EOL;
                $content = str_replace('"//www.google-analytics.com/plugins/ua/"+c', '"' . $host . '/cache/analytics/"+c.replace(/\.js/,".' . $bust . '.js")', $content);
                \File::put($saveTo, $content);

                $content = self::request('https://www.google-analytics.com/plugins/ua/ec.js');
                if ($content AND strlen($content) > 0) {
                    $saveTo = public_path("cache/analytics/ec.$bust.js");
                    echo "Saving file to $saveTo" . PHP_EOL;
                    \File::put($saveTo, $content);
                }
            }
        } catch (Exception $e) {

        }

    }


    static function sessionLottery()
    {
        $path = \Config::get('session.files');
        $lifetime = \Config::get('session.lifetime') * 60;
        $files = Finder::create()
            ->in($path)
            ->files()
            ->ignoreDotFiles(true)
            ->date('<= now - ' . $lifetime . ' seconds');

        $counter = 0;

        foreach ($files as $file) {
            echo $file->getRealPath() . " removed!" . PHP_EOL;
            unlink($file->getRealPath());
            $counter++;
        }
        echo "A total of $counter files has been removed!" . PHP_EOL;
    }


    static function redirectCache()
    {
        \RedirectLink::rebuildSchema();
        echo "RedirectLink cache has been rebuilded" . PHP_EOL;
    }


    static function product()
    {
        $id = 39506;
    }
}