<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 19/04/2018
 * Time: 16:22
 */

namespace services\Sirv;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Str;
use File;

class SirvResource
{
    /**
     * @var string
     */
    protected $etag;

    /**
     * @var string
     */
    protected $fileName;

    /**
     * @var string
     */
    protected $baseName;

    /**
     * @var string
     */
    protected $extension;

    /**
     * @var int
     */
    protected $size = 0;

    /**
     * @var Carbon
     */
    protected $lastModified;

    /**
     * @var string
     */
    private $cachePath;

    /**
     * @var SirvBucket|null
     */
    private $bucket;

    function __construct(array $params = [], SirvBucket $bucket = null)
    {
        $this->bucket = $bucket;
        if (isset($params['Key'])) {
            $fileName = $params['Key'];
            $tokens = explode('/', $fileName);
            $this->baseName = $fileName;
            $this->fileName = end($tokens);

            $tokens = explode('.', $this->fileName);
            $this->extension = Str::lower(end($tokens));
        }

        if (isset($params['LastModified'])) {
            try {
                $this->lastModified = Carbon::parse($params['LastModified']);
            } catch (Exception $e) {
                $this->lastModified = Carbon::now();
            }
        }

        if (isset($params['ETag'])) {
            try {
                $this->etag = str_replace('"', null, $params['ETag']);
            } catch (Exception $e) {

            }
        }

        if (isset($params['Size'])) {
            try {
                $this->size = (int)$params['Size'];
            } catch (Exception $e) {

            }
        }

        $this->cachePath = config('membership.sirv.cachePath');
    }

    /**
     * @return null|SirvBucket
     */
    function getBucket()
    {
        return $this->bucket;
    }

    /**
     * @return null|string
     */
    function getFolder()
    {
        $bucket = $this->getBucket();
        return ($bucket) ? $bucket->getName() : null;
    }

    /**
     * @return null|string
     */
    function getEtag()
    {
        return $this->etag;
    }

    /**
     * @return null|string
     */
    function getFilename()
    {
        return $this->fileName;
    }

    /**
     * @return null|string
     */
    function getBasename()
    {
        return $this->baseName;
    }

    /**
     * @return null|string
     */
    function getSize()
    {
        return $this->size;
    }

    /**
     * @return null|string
     */
    function getExtension()
    {
        return $this->extension;
    }

    /**
     * @return Carbon|null
     */
    function getLastModified()
    {
        return $this->lastModified;
    }

    /**
     * @return bool
     */
    function isDirectory()
    {
        return $this->getSize() == 0;
    }

    /**
     * @return bool
     */
    function isFile()
    {
        return !$this->isDirectory();
    }

    /**
     * @return bool
     */
    function isImage()
    {
        return in_array($this->extension, ['jpg', 'jpeg', 'png', 'gif']);
    }

    /**
     * @return array
     */
    function toArray()
    {
        return [
            'etag' => $this->getEtag(),
            'fileName' => $this->getFilename(),
            'baseName' => $this->getBasename(),
            'size' => $this->getSize(),
            'lastModified' => $this->getLastModified(),
        ];
    }

    /**
     * @return string
     */
    function getDigest()
    {
        return md5($this->getLastModified()->toDateTimeString() . '_' . $this->getSize());
    }

    /**
     * @return string
     */
    function getCacheFile()
    {
        return $this->cachePath . $this->getDigest();
    }

    /**
     * @return bool
     */
    function hasCacheFile()
    {
        return File::exists($this->getCacheFile());
    }

    /**
     *
     */
    function purgeCache()
    {
        try {
            File::delete($this->getCacheFile());
        } catch (Exception $e) {

        }
    }

    /**
     * @param $contents
     */
    function saveCacheFile($contents)
    {
        File::put($this->getCacheFile(), $contents);
    }

    /**
     * @return string
     */
    function getCacheContents()
    {
        return File::get($this->getCacheFile());
    }

    /**
     * @return int
     */
    function getImagePosition()
    {
        $folder = $this->getFolder();
        $extension = $this->getExtension();
        $fileName = $this->getFilename();
        $position = null;
        $pathName = str_replace('.' . $extension, null, $fileName);
        if ($pathName == $folder) {
            $position = 1;
        } else {
            $tokens = explode('_', $pathName);
            $position = (int)end($tokens);
        }
        return $position;
    }
}