<?php

namespace services\Sirv\Commands;

use Illuminate\Console\Command;
use services\Sirv\SirvManager;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App;

class SirvListCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'sirv:list';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all folders of the SIRV root';

    /**
     * @var services\Sirv\SirvRepository
     */
    protected $repository;

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->repository = App::make('services\Sirv\SirvRepository');
        $this->repository->setConsole($this);
        $this->info("Executing SirvListCommand...");
        $this->testConnection();
        $this->testFolder();
        return;
        $folder = config('membership.sirv.root');
        $this->testFilesInFolder($folder);
        //$this->testDownloadFile();
    }


    private function testConnection()
    {
        $this->line("Testing Connection...");
        $test = $this->repository->isConnected();
        if ($test) {
            $this->info("Sirv successfully connected");
        } else {
            $this->error("Sirv could not connect!");
        }
    }

    private function testFolder()
    {
        $folder = config('membership.sirv.root');
        $this->line("Testing folder [$folder]");
        $test = $this->repository->getClient()->checkIfObjectExists($folder);
        if ($test) {
            $this->info("Remote folder [$folder] exists!");
        } else {
            $this->error("Could not find any remote folder [$folder]");
        }
    }

    private function testFilesInFolder($folder)
    {
        $this->line("Listing content of folder [$folder]");
        $test = $this->repository->getClient()->checkIfObjectExists($folder);
        if ($test) {
            $items = $this->repository->getClient()->getBucketContents($folder);
            if (empty($items)) {
                $this->comment("Bucket '$folder' is empty'");
                return;
            }
            $many = count($items);
            $this->info("Found $many items in $folder");
            if (isset($items['dirs']) and !empty($items['dirs'])) {
                foreach ($items['dirs'] as $dir) {
                    $this->testFilesInFolder($dir['Prefix']);
                }
            }
        } else {
            $this->error("Could not find any remote folder [$folder]");
        }
    }

    private function testDownloadFile()
    {
        $file = 'Affiliazione/T4RB168/T4RB168_1.jpg';
        $destination = base_path("sources/T4RB168_1.jpg");
        $this->line("Testing download file...");

        $bool = $this->repository->getClient()->downloadFile($file, $destination);
        if ($bool) {
            $this->info("Remote file [$file] saved as local file [sources/$file]");
        } else {
            $this->error("Could not find any remote file [$file]");
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}