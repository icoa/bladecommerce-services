<?php

namespace services\Sirv\Commands;

use Illuminate\Console\Command;
use services\Sirv\SirvManager;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App;

class TestSirvClient extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'sirv:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Performs some test with Sirv Client';

    /**
     * @var services\Sirv\SirvRepository
     */
    protected $repository;

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->repository = App::make('services\Sirv\SirvRepository');
        $this->repository->setConsole($this);
        $this->info("Executing TestSirvClient...");
        $this->testConnection();
        $this->testFolder();
        $this->testFilesInFolder();
        $this->testDownloadFile();
    }


    private function testConnection()
    {
        $this->line("Testing Connection...");
        $test = $this->repository->isConnected();
        if ($test) {
            $this->info("Sirv successfully connected");
        } else {
            $this->error("Sirv could not connect!");
        }
    }

    private function testFolder()
    {
        $this->line("Testing folder...");
        $folder = config('sirv.testFolder');
        $test = $this->repository->getClient()->checkIfObjectExists($folder);
        if ($test) {
            $this->info("Remote folder [$folder] exists!");
        } else {
            $this->error("Could not find any remote folder [$folder]");
        }
    }

    private function testFilesInFolder()
    {
        $this->line("Testing files in folder...");
        $folder = config('sirv.testFolder');
        $test = $this->repository->getClient()->checkIfObjectExists($folder);
        if ($test) {
            $items = $this->repository->getClient()->getBucketContents($folder);
            //print_r($items);
        } else {
            $this->error("Could not find any remote folder [$folder]");
        }
    }

    private function testDownloadFile()
    {
        $file = 'scz38w_8.PNG';
        $destination = base_path("sources/$file");
        $this->line("Testing download file...");
        $folder = config('sirv.testFolder');
        $test = $this->repository->getClient()->checkIfObjectExists($folder);
        if ($test) {
            $bool = $this->repository->getClient()->downloadFile("$folder/$file", $destination);
            if ($bool) {
                $this->info("Remote file [$folder/$file] saved as local file [sources/$file]");
            } else {
                $this->error("Could not find any remote file [$folder/$file]");
            }
        } else {
            $this->error("Could not find any remote folder [$folder]");
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}