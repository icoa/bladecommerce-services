<?php

namespace services\Sirv\Commands;

use Illuminate\Console\Command;
use services\Sirv\SirvManager;
use services\Sirv\SirvRepository;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App;

class Sirv360Command extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'sirv:360';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grab images from the SIRV repository for the 360 spin support';

    /**
     * @var SirvRepository
     */
    protected $repository;

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->repository = App::make('services\Sirv\SirvRepository');
        $this->repository->setConsole($this);
        $this->info("Executing Sirv360Command...");
        $this->repository->walk360SpinsFolders();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}