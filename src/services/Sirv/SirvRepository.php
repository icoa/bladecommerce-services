<?php


namespace services\Sirv;

use Product;
use ProductImage;
use Illuminate\Support\Str;
use DB;
use File;
use services\Morellato\Repositories\Repository;
use services\src\services\Traits\TraitImageWhiteStrip;

class SirvRepository extends Repository
{

    use TraitImageWhiteStrip;

    /**
     * @var SirvClient
     */
    protected $sirvClient;

    /**
     * @var string
     */
    protected $sku;

    /**
     * @var int
     */
    protected $product_id;

    /**
     * @var int
     */
    protected $product_image_id;

    /**
     * @var bool
     */
    protected $coverExists = false;

    /**
     * @var int
     */
    protected $position = 0;

    /**
     * SirvRepository constructor.
     * @param SirvClient $sirvClient
     */
    function __construct(SirvClient $sirvClient)
    {
        $sirvClient->init(
            config('sirv.bucket'),
            config('sirv.key'),
            config('sirv.secret')
        );
        $this->sirvClient = $sirvClient;
    }

    /**
     * @return bool
     */
    function isConnected()
    {
        return $this->sirvClient->testConnection();
    }

    /**
     * @return SirvClient
     */
    function getClient()
    {
        return $this->sirvClient;
    }

    /**
     *
     */
    function grabImages()
    {
        $this->product_image_id = DB::selectOne("SHOW TABLE STATUS WHERE name='images'")->Auto_increment;
        $console = $this->getConsole();
        $folder = config('membership.sirv.root');
        $console->line("Checking remote folder [$folder]");
        //$test = $this->getClient()->checkIfObjectExists($folder);
        $test = true;
        if ($test) {

            $products = Product::withSource('local')
                //->where('sku', 'JF01417791')
                //->take(2)
                ->where('id', '>', 55596)
                ->select('id', 'sku')->get();
            foreach ($products as $product) {
                $console->line("Fetching images for sku [$product->sku]");
                $this->sku = $product->sku;
                $this->product_id = $product->id;
                $this->scanBySku($product->sku);
            }
        } else {
            $console->error("Could not find any remote folder [$folder]");
        }
    }

    /**
     * @param $sku
     */
    protected function scanBySku($sku)
    {
        $console = $this->getConsole();
        $folder = config('membership.sirv.root');
        $sku = $this->sanitizeSku($sku);
        $sirvFolder = $folder . '/' . $sku;
        $console->comment("Entering remote folder [$sirvFolder]");
        $test = $this->getClient()->checkIfObjectExists($sirvFolder);
        if ($test) {
            $items = $this->getClient()->getBucketContents($sirvFolder);
            $bucket = new SirvBucket($items);
            if (!$bucket->isEmpty()) {
                $this->coverExists = DB::table('images')->where('product_id', $this->product_id)->where('cover', 1)->count() > 0;
                $this->position = DB::table('images')->where('product_id', $this->product_id)->max('position');
                $files = $bucket->getFiles();
                foreach ($files as $file) {
                    $this->fetchResource($file);
                }
            }
        } else {
            $console->comment("Could not find any remote folder [$sirvFolder]");
        }
    }

    /**
     * @param SirvResource $resource
     */
    protected function fetchResource(SirvResource $resource)
    {
        $console = $this->getConsole();
        $baseName = $resource->getBasename();
        $cacheFile = $resource->getCacheFile();

        //print_r($resource->toArray());
        if ($resource->hasCacheFile()) {
            $console->info("Resource [$baseName] is already cached, skip downloading it...");
            $this->addResourceAsProductImage($resource);
        } else {
            $console->line("Fetching fresh resource [$baseName]");
            try {
                $this->getClient()->downloadFile($baseName, $cacheFile);
                $console->info("Resource [$baseName] successfully saved to [$cacheFile]");
                $this->addResourceAsProductImage($resource);
            } catch (\Exception $e) {
                $console->error("Cannot save resource [$baseName] as [$cacheFile]");
            }
        }
    }

    /**
     * @param SirvResource $resource
     */
    protected function addResourceAsProductImage(SirvResource $resource)
    {
        $console = $this->getConsole();
        $baseName = $resource->getBasename();
        $cacheFile = $resource->getCacheFile();
        $folder = $resource->getFolder();
        $imagePosition = $resource->getImagePosition();
        $md5 = $resource->getDigest();
        $extension = $resource->getExtension();
        $debug = compact('cacheFile', 'baseName', 'folder', 'imagePosition', 'md5');
        $productImage = ProductImage::getByMd5($md5);
        if ($productImage) {
            $console->info("ProductImage [$productImage->id] is already present and up-to-date; skip processing...");
            $this->updateProductImageCounter();
            return;
        }
        //print_r($debug);
        if ($imagePosition > 0) {
            $position = $imagePosition;

            $productImage = ProductImage::getByPosition($this->product_id, $position);
            $action = null;

            if (is_null($productImage)) {
                $productImage = new ProductImage();
                $action = 'insert';
            } else {
                $console->info("ProductImage [$productImage->id] is already present but we have to update it...");
                $action = 'update';
            }

            if ($action == 'insert') {
                $fileName = "{$this->product_image_id}.$extension";
                $this->product_image_id++;
                $this->position++;
            } else {
                $fileName = $productImage->filename;
            }

            //now copy the cache resource file as a targetFilename
            $targetFilename = public_path("assets/products/{$fileName}");

            try {
                $console->info("Copying [$cacheFile] to [$targetFilename]");
                File::copy($cacheFile, $targetFilename);
            } catch (\Exception $e) {
                $console->error("Copy failed");
                audit_all($e->getMessage(), __METHOD__, 'On Copy file');
                audit_all($e->getTraceAsString(), __METHOD__, 'On Copy file');
                return;
            }
            $cover = ($this->coverExists == false and $position == 1) ? 1 : 0;
            $data = [
                'filename' => $fileName,
                'cover' => $cover,
                'position' => $position,
                'product_id' => $this->product_id,
                'md5' => $md5,
                'version' => 1, //SIRV does not support versioning, so it is always 1
                'var' => 1,
            ];

            $languages = \Core::getLanguages();
            foreach ($languages as $language) {
                $data[$language] = ['published' => 1, 'legend' => null];
            }

            try {
                $productImage->make($data);
                $productImage->save();
                $console->info("ProductImage succesfully [$action] with ID => $productImage->id");
            } catch (\Exception $e) {
                $console->error("DB storage failed");
                audit_all($e->getMessage(), __METHOD__, 'On ProductImage DB storage');
                audit_all($e->getTraceAsString(), __METHOD__, 'On ProductImage DB storage');
                return;
            }
            $this->updateProductImageCounter();

            try {
                $this->removeWhiteBackground($productImage->product_id, $productImage->filename);
            } catch (\Exception $e) {
                $console->error("Remove White Background failed");
                audit_all($e->getMessage(), __METHOD__, 'On Remove White Background');
                audit_all($e->getTraceAsString(), __METHOD__, 'On Remove White Background');
            }
        }
    }

    /**
     * @param $sku
     * @return string
     */
    private function sanitizeSku($sku)
    {
        return Str::upper((str_replace('/', '-', $sku)));
    }

    /**
     *
     */
    function updateProductImageCounter()
    {
        $count = DB::table('images')->where('product_id', $this->product_id)->count();
        DB::table('products')->where('id', $this->product_id)->update(['cnt_images' => $count]);
        $this->setProductDefaultImg($this->product_id);
    }

    /**
     * @param $product_id
     */
    private function setProductDefaultImg($product_id)
    {
        $image_id = DB::table('images')->where('product_id', $product_id)->where('cover', 1)->pluck('id');
        if ($image_id > 0) {
            DB::table('products')->where('id', $product_id)->update(['default_img' => $image_id]);
        }
    }


    /**
     * @param $folder
     * @return bool
     */
    function grab360spins($folder)
    {
        $console = $this->getConsole();
        $spinsUriPattern = config('sirv.spinsUriPattern');
        $test = $this->getClient()->checkIfObjectExists($folder);
        if ($test) {
            $spins = [];
            $console->info("Entering 360 spins folder...");
            $folders = $this->getClient()->getFoldersList($folder);
            //audit($folders, 'SIRV FOLDERS');
            $many = count($folders);
            $console->comment("Found ($many) folders inside that folder...");

            /*$limit = 4;
            $folders = array_slice($folders, 0, $limit);*/

            foreach ($folders as $folder) {
                $folderObj = (object)$folder;

                $console->line("Accessing SPIN folder $folderObj->Name");
                $items = $this->getClient()->getBucketContents($folderObj->Prefix);
                //audit($items, 'SIRV ITEMS');
                $bucket = new SirvBucket($items);
                $files = $bucket->getFiles();
                if (!empty($files)) {
                    foreach ($files as $file) {
                        if ($file->getExtension() == 'spin') {
                            $fileName = $file->getFilename();
                            $sku = Str::upper(trim($folderObj->Name));
                            $console->info("Found SPIN file for: " . $fileName);
                            $spins[$sku] = $spinsUriPattern . $file->getBasename();
                        }
                    }
                }

            }
            if (!empty($spins)) {
                $this->bindSpinsToProducts($spins);
            }

        } else {
            $console->error("Could not find any remote folder [$folder]");
        }
        return $test;
    }

    /**
     * Walk all 360_X folders in SIRV
     */
    function walk360SpinsFolders()
    {
        $root = config('sirv.spinsFolder');
        $spread = config('sirv.spreadSpinsFolders');
        //TODO: this must be enabled
        $this->grab360spins($root);
        if ($spread) {
            $counter = 2;
            do {
                $innerFolder = $root . '_' . $counter;
                $continue = $this->grab360spins($innerFolder);
                $counter++;
            } while ($continue);
        }
    }

    /**
     * @return array
     */
    private function getProductsSkuMapping()
    {
        $cache = [];
        $rows = Product::inStocks()->select('id', 'sku')->get();
        foreach ($rows as $row) {
            $cache[$row->sku] = $row->id;
        }
        return $cache;
    }

    /**
     * @param array $spins
     */
    private function bindSpinsToProducts(array $spins)
    {
        $console = $this->getConsole();
        $many = count($spins);
        $console->line("Found ($many) spins to map within products...");
        $notFound = [];
        //print_r($spins);
        //getting the products cache
        $products = $this->getProductsSkuMapping();

        DB::beginTransaction();
        foreach ($spins as $sku => $spinUri) {
            if (isset($products[$sku])) {
                $console->comment("Updating SPIN file for product $sku");
                DB::table('products')->where('id', $products[$sku])->update([
                    'spin' => $spinUri,
                ]);
            } else {
                $notFound[] = $sku;
            }
        }
        if (!empty($notFound)) {
            $console->error("Warning: there are some SKUs that cannot be binded to products");
            audit_error($notFound, __METHOD__ . ' => Warning: there are some SKUs that cannot be binded to products');
            $this->warnAboutMissingProducts($notFound);
        }
        DB::commit();
    }

    /**
     * @param $skus
     */
    private function warnAboutMissingProducts($skus)
    {
        if (config('sirv.sendSpinEmail', false) == false)
            return;

        if (count($skus) > 0) {
            $list = implode('<br>', $skus);
            \Config::set('mail.pretend', false);
            $name = \Cfg::get('SHORTNAME');
            $from = \Cfg::get('MAIL_GENERAL_ADDRESS');

            $recipient_internal = 'f.politi@icoa.it';

            $reason = "SIRV360: Warning: there are some SKUs that cannot be binded to products.";

            $body = $reason . '<hr>' . $list;

            \Mail::send('emails.default', compact('body'), function ($mail) use ($recipient_internal, $from, $name) {
                $mail->from($from);
                $mail->subject($name . ' - sirv360MissingProducts');
                $mail->to($recipient_internal);
            });
        }
    }
}