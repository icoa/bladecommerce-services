<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 19/04/2018
 * Time: 16:22
 */

namespace services\Sirv;

use Carbon\Carbon;
use Exception;
use Illuminate\Support\Str;

class SirvBucket
{
    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var SirvResource[]
     */
    protected $files = [];

    /**
     * @var array
     */
    protected $resources = [];

    /**
     * SirvBucket constructor.
     * @param array $params
     */
    function __construct(array $params = [])
    {
        if (isset($params['current_dir'])) {
            $fileName = $params['current_dir'];
            $tokens = explode('/', $fileName);
            $this->path = $fileName;
            $this->name = end($tokens);
        }

        if (isset($params['contents']) and is_array($params['contents']) and !empty($params['contents'])) {
            try {
                foreach ($params['contents'] as $content) {
                    $resource = new SirvResource($content, $this);
                    $this->resources[] = $resource;
                    if ($resource->isFile()) {
                        $this->files[] = $resource;
                    }
                }
            } catch (Exception $e) {

            }
        }
    }

    /**
     * @return mixed|string
     */
    function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed|string
     */
    function getPath()
    {
        return $this->path;
    }

    /**
     * @return SirvResource[]
     */
    function getFiles()
    {
        return $this->files;
    }

    /**
     * @return array
     */
    function getResources()
    {
        return $this->resources;
    }

    /**
     * @return int
     */
    function getFilesCount()
    {
        return count($this->getFiles());
    }

    /**
     * @return bool
     */
    function isEmpty()
    {
        return $this->getFilesCount() == 0;
    }
}