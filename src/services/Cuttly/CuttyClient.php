<?php


namespace services\Cuttly;


use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use RuntimeException;
use services\Cuttly\Exceptions\CuttlyApiException;
use services\Cuttly\Exceptions\InvalidUrlCorsPolicy;
use services\Cuttly\Exceptions\InvalidUrlToShort;
use services\Cuttly\Exceptions\UrlToShortNotProvided;

class CuttyClient
{

    const CUTTLY_DOMAIN = 'cutt.ly';

    protected $endpoint = 'https://cutt.ly/api/api.php?key=[KEY]&short=[SHORT]&name=[ALIAS]';

    /**
     * @var string
     */
    protected $access_key;

    /**
     * @var string
     */
    protected $short;

    /**
     * @var string
     */
    protected $alias;

    /**
     * @var Client
     */
    protected $client;

    /**
     * IpApiAdapter constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
        $this->access_key = (string)config('morellato.cuttly.apiKey');
    }

    /**
     * @return string
     */
    public function getShort()
    {
        return (string)$this->short;
    }

    /**
     * @param string $short
     * @return CuttyClient
     */
    public function setShort($short)
    {
        $this->short = $short;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return (string)$this->alias;
    }

    /**
     * @param string $alias
     * @return CuttyClient
     */
    public function setAlias($alias)
    {
        if('' !== (string)$alias){
            $this->alias = $alias;
        }
        return $this;
    }


    /**
     * @param bool $with_cors
     * @return string
     */
    protected function getUri($with_cors = true)
    {
        $short = $this->getShort();
        if ($short === '')
            throw new UrlToShortNotProvided();

        if (false === filter_var($short, FILTER_VALIDATE_URL)) {
            throw new InvalidUrlToShort();
        }

        // check if the url has too many '?'
        if(count(explode('?', $short)) > 2){
            throw new InvalidUrlToShort();
        }

        if (true === $with_cors) {
            if (false === \Core::isUrlStrictToSameHost($short)) {
                throw new InvalidUrlCorsPolicy();
            }
        } else {
            $host = parse_url($short, PHP_URL_HOST);
            if ($host !== self::CUTTLY_DOMAIN) {
                throw new InvalidUrlCorsPolicy('Given Url to stats does not belong to the configured Cuttly domain: ' . self::CUTTLY_DOMAIN);
            }
        }

        $params = [
            'KEY' => $this->access_key,
            'SHORT' => urlencode($short),
            'ALIAS' => $this->getAlias(),
        ];
        audit($params, __METHOD__, 'params');

        $uri = str_replace(array_map(static function ($key) {
            return "[$key]";
        }, array_keys($params)), array_values($params), $this->endpoint);

        if($this->getAlias() === ''){
            $uri = str_replace('&name=', '', $uri);
        }

        return $uri;
    }

    /**
     * @param $status
     * @return bool
     */
    protected function isResponseStatusError($status)
    {
        return (int)$status !== 7;
    }

    /**
     * @return CuttlyShortPayload|CuttlyStatsPayload
     * @throws CuttlyApiException
     */
    public function fetchPayload()
    {
        $payload = new CuttlyShortPayload();

        $url = $this->getUri();
        audit($url, __METHOD__, 'URL');

        /** @var Response $response */
        $response = $this->client->get($url);
        $status = $response->getStatusCode();
        //audit($status, __METHOD__, 'STATUS');
        if ($status !== 200) {
            throw new RuntimeException('Cuttly service has not responded with a valid 200 OK Status code');
        }

        // Decode JSON response:
        $api_result = json_decode($response->getBody(), true);
        audit($api_result, __METHOD__, 'JSON RESPONSE');

        $params = $api_result['url'];
        if ($this->isResponseStatusError($params['status'])) {
            throw new CuttlyApiException('', (int)$params['status']);
        }

        $payload->fill($params);

        return $payload;
    }
}