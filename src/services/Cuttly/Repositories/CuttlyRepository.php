<?php


namespace services\Cuttly\Repositories;

use Exception;
use RuntimeException;
use services\Cuttly\CuttlyStatsPayload;
use services\Cuttly\CuttyClient;
use services\Cuttly\CuttyStatsClient;
use services\Cuttly\Exceptions\CuttlyApiException;
use services\Morellato\Repositories\Repository;

class CuttlyRepository extends Repository
{
    public function test()
    {
        $client = new CuttyClient();
        $url = 'http://morellato.local/menu-info.html?grpCode=VODAFONE&__utmSource=SAMESITE';
        $alias = 'KSLC-INFO';
        $client
            ->setShort($url)
            ->setAlias($alias)
            ->fetchPayload();
    }

    public function test_stats()
    {
        $client = new CuttyStatsClient();
        $url = 'https://cutt.ly/KSLC-INFO';
        $payload = $client
            ->setShort($url)
            ->fetchPayload();
        print_r($payload->toArray());
    }

    /**
     * @param $url
     * @param null $alias
     * @return string|null
     * @throws CuttlyApiException
     */
    public function createShortUrl($url, $alias = null)
    {
        $client = new CuttyClient();
        return $client
            ->setShort($url)
            ->setAlias(trim($alias))
            ->fetchPayload()
            ->getLink();

    }

    /**
     * @param $short
     * @return CuttlyStatsPayload
     * @throws CuttlyApiException
     */
    public function getShortUrlStats($short)
    {
        $client = new CuttyStatsClient();
        return $client
            ->setShort($short)
            ->fetchPayload();

    }
}