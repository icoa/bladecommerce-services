<?php

namespace services\Cuttly\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use services\Cuttly\Repositories\CuttlyRepository;

class CuttlyCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'cuttly:exec';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Performs some task to handle Cuttly web-service';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        /** @var CuttlyRepository $repository */
        $repository = app(CuttlyRepository::class);
        $repository->setConsole($this);
        //$repository->test();
        $repository->test_stats();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}