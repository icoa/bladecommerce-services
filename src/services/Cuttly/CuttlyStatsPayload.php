<?php


namespace services\Cuttly;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class CuttlyStatsPayload extends Model
{
    /**
     * @var array
     */
    protected $fillable = [
        'status',
        'clicks',
        'date',
        'title',
        'fullLink',
        'shortLink',
        'facebook',
        'twitter',
        'pinterest',
        'instagram',
        'googlePlus',
        'linkedin',
        'devices',
    ];

    /**
     * @return bool
     */
    public function isResolved()
    {
        return (int)$this->getAttribute('status') === 1;
    }

    /**
     * @param $key
     * @return bool
     */
    public function hasAttribute($key)
    {
        $attribute = $this->getAttribute($key);
        return ($attribute != null and Str::length($attribute) > 0);
    }

    /**
     * @return string|null
     */
    public function getLink()
    {
        return $this->isResolved() ? $this->getAttribute('fullLink') : null;
    }
}