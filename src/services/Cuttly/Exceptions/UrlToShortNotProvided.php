<?php


namespace services\Cuttly\Exceptions;

use RuntimeException;

class UrlToShortNotProvided extends RuntimeException
{
    protected $message = 'Given URL to short is empty';
}