<?php


namespace services\Cuttly\Exceptions;

use Exception;
use Throwable;

class CuttlyShortApiException extends Exception
{
    /**
     * CuttlyShortApiException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        $legend = [
            0 => 'This shortened link does not exist',
            2 => 'Invalid API key',
        ];

        if(array_key_exists($code, $legend)){
            $message = $legend[$code];
        }
        parent::__construct($message, $code, $previous);
    }
}