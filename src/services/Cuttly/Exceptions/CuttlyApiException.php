<?php


namespace services\Cuttly\Exceptions;

use Exception;
use Throwable;

class CuttlyApiException extends Exception
{
    /**
     * CuttlyApiException constructor.
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        $legend = [
            1 => 'the shortened link comes from the domain that shortens the link, i.e. the link has already been shortened.',
            2 => 'the entered link is not a link.',
            3 => 'the preferred link name is already taken',
            4 => 'Invalid API key',
            5 => 'the link has not passed the validation. Includes invalid characters',
            6 => 'The link provided is from a blocked domain',
        ];

        if(array_key_exists($code, $legend)){
            $message = $legend[$code];
        }
        parent::__construct($message, $code, $previous);
    }
}