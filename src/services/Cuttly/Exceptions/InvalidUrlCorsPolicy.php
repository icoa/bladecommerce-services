<?php


namespace services\Cuttly\Exceptions;

use RuntimeException;

class InvalidUrlCorsPolicy extends RuntimeException
{
    protected $message = 'Given Url to short does not respects the same-origin policy';
}