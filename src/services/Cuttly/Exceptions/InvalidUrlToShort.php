<?php


namespace services\Cuttly\Exceptions;

use RuntimeException;

class InvalidUrlToShort extends RuntimeException
{
    protected $message = 'Given Url to short is not a valid URI';
}