<?php


namespace services\Cuttly;

use GuzzleHttp\Psr7\Response;
use RuntimeException;
use services\Cuttly\Exceptions\CuttlyShortApiException;

class CuttyStatsClient extends CuttyClient
{

    protected $endpoint = 'https://cutt.ly/api/api.php?key=[KEY]&stats=[SHORT]';

    /**
     * @param $status
     * @return bool
     */
    protected function isResponseStatusError($status)
    {
        return (int)$status !== 1;
    }

    /**
     * @return CuttlyStatsPayload
     * @throws CuttlyShortApiException
     */
    public function fetchPayload()
    {
        $payload = new CuttlyStatsPayload();

        $url = $this->getUri(false);
        //audit($url, __METHOD__, 'URL');

        /** @var Response $response */
        $response = $this->client->get($url);
        $status = $response->getStatusCode();
        //audit($status, __METHOD__, 'STATUS');
        if ($status !== 200) {
            throw new RuntimeException('Cuttly service has not responded with a valid 200 OK Status code');
        }

        // Decode JSON response:
        $api_result = json_decode($response->getBody(), true);
        //audit($api_result, __METHOD__, 'JSON RESPONSE');

        $params = $api_result['stats'];
        if ($this->isResponseStatusError($params['status'])) {
            throw new CuttlyShortApiException('', (int)$params['status']);
        }

        $payload->fill($params);

        return $payload;
    }
}