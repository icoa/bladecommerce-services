<?php


namespace services\Cuttly;

use SingleModel;

/**
 * ShortLink
 *
 * @property integer $id
 * @property string $source
 * @property string $target
 * @property boolean $isRegExp
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\RedirectLink whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\RedirectLink whereSource($value)
 * @method static \Illuminate\Database\Query\Builder|\RedirectLink whereTarget($value)
 * @method static \Illuminate\Database\Query\Builder|\RedirectLink whereAlias($value)
 */
class ShortLink extends SingleModel
{
    /* LARAVEL PROPERTIES */

    protected $table = 'short_links';
    protected $guarded = array();
    protected $isBlameable = true;
    public $timestamps = true;
    public $softDelete = false;
    public $db_fields = array(
        'source',
        'alias',
        'target'
    );
}
