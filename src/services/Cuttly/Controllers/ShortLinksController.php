<?php


namespace services\Cuttly\Controllers;

use AdminAction;
use BackendController;
use Input;
use services\Cuttly\Repositories\CuttlyRepository;
use services\Cuttly\ShortLink;
use URL;

class ShortLinksController extends BackendController
{

    public $component_id = 88;
    public $title = 'Short Links';
    public $page = 'short_links.index';
    public $pageheader = 'Short Links';
    public $iconClass = 'font-link';
    public $model = ShortLink::class;

    protected $rules = array(
        'source' => 'required|unique:short_links,source',
        'alias' => 'max:32|unique:short_links,alias',
    );

    protected $friendly_names = array(
        'source' => 'URL sorgente',
        'alias' => 'Alias',
    );

    public function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Short Links');
        $this->toFooter("js/echo/short_links.js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/short_links.js");
        $this->page = 'short_links.create';
        $this->pageheader = 'Nuovo Short Link';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/short_links.js");
        $this->page = 'short_links.create';
        $this->pageheader = 'Modifica Short Link';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }


    public function getTable()
    {
        $model = $this->model;

        $lang_id = \Core::getLang();

        $pages = $model::select('id', 'source', 'target');

        return \Datatables::of($pages)
            ->edit_column('source', function ($data) {
                $link = \URL::action($this->action('getEdit'), $data['id']);
                return "<a href='{$link}'>{$data['source']}</a>";
            })
            ->edit_column('target', function ($data) {
                return "<a target='_blank' href='{$data['target']}'><b>{$data['target']}</b></a>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->rules['source'] = 'required|unique:short_links,source,' . $model->id;
        $this->rules['alias'] = 'required|unique:short_links,alias,' . $model->id;
        $this->_prepare();
    }


    function _prepare()
    {
        if (count($_POST) === 0) {
            return;
        }

        $_POST['source'] = (trim($_POST['source']));
        $_POST['alias'] = (trim($_POST['alias']));

        Input::replace($_POST);
    }


    public function _prepare_data_create()
    {
        $repository = new CuttlyRepository();
        try {
            $_POST['target'] = $repository->createShortUrl(Input::get('source'), Input::get('alias'));
            Input::replace($_POST);
        } catch (\Exception $e) {
            $this->errors[] = $e->getMessage();
        }
    }

    protected function column_actions($data)
    {
        $edit = URL::action($this->action("getEdit"), $data['id']);
        $destroy = URL::action($this->action("postDelete"), $data['id']);

        return <<<HTML
<ul class="table-controls">    
    <li><a href="$edit" class="btn" title="Modifica"><i class="icon-edit"></i></a> </li>    
    <li><a href="javascript:;" onclick="EchoTable.confirmAction('$destroy');" class="btn" title="Elimina"><i class="icon-remove"></i></a> </li>
</ul>
HTML;
    }

    protected function toolbar($what = 'default')
    {

        global $actions;
        $actions = [];
        switch ($what) {
            case 'create':
                $index = URL::action($this->action('getIndex'));

                $actions = array(
                    new AdminAction('Salva e rimani', "javascript:Echo.submitForm('reopen');", 'font-retweet', 'btn-primary', 'Salva tutti i dati del Record e riapre questa schermata'),
                    new AdminAction('Salva e chiudi', "javascript:Echo.submitForm('submit');", 'font-save', 'btn-success', 'Salva tutti i dati del Record e torna all\'Elenco dei Record'),
                    new AdminAction('Indietro', $index, 'font-arrow-left'),
                );

                break;

            default:
                $create = ($this->action("getCreate", TRUE));

                $deleteMulti = ($this->action("postDeleteMulti", TRUE));
                $actions = array(
                    new AdminAction('Nuovo', $create, 'font-plus', 'btn-success', 'Crea un nuovo record'),
                    new AdminAction('Cancella', $deleteMulti, 'font-remove', 'btn-danger confirm-action-multi', 'Cancella(o sposta nel Cestino) uno o più record selezionati'),
                    new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente'),
                );
                break;
        }
    }

    /**
     * @param ShortLink $model
     * @return mixed|string
     */
    public static function renderStats(ShortLink $model)
    {
        $repository = new CuttlyRepository();
        try {
            $payload = $repository->getShortUrlStats($model->target);
            return \Theme::partial('plugins.cuttly.stats', compact('payload'));
        } catch (\Exception $e) {
            return "<div class='alert alert-danger'>{$e->getMessage()}</div>";
        }
    }


}
