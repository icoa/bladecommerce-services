<?php
/**
 * Created by PhpStorm.
 * User: Akshay.D
 * Date: 18-05-2017
 * Time: 15:55
 */

namespace services;

use File, Config;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class ZooratePlaceOrder
{
    protected $console;
    protected $RequestToken;
    protected $RefreshToken;
    protected $EncryptedKey;
    protected $AccessToken;
    protected $MerchantCode;
    protected $ClientSecret;

    function __construct(Command $console)
    {
        $this->console = $console;
        $this->MerchantCode = Config::get('ftp.MerchantCode');
        $this->ClientSecret = Config::get('ftp.ClientSecret');
    }


    /**
     * Start the process for placing orders to Feedaty.
     *
     */
    public function start()
    {
        $this->console->comment("***Fetching Request Token***");
        try {
            $url = "api.feedaty.com/OAuth/RequestToken";
            $data['MerchantCode'] = $this->MerchantCode;
            $data['Secret Client'] = $this->ClientSecret;

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
            $result = curl_exec($curl);
            $info = curl_getinfo($curl);
            curl_close($curl);

            $result = json_decode($result, TRUE);
            if ($info['http_code'] == 200) {
                $this->RequestToken = $result['RequestToken'];
                $this->RefreshToken = $result['RefreshToken'];

                $sha1_string = SHA1($this->RequestToken . $this->ClientSecret);
                $this->EncryptedKey = base64_encode($this->MerchantCode . ":" . $sha1_string);

                $this->console->info("Fetched Request Token [$this->RequestToken]");
                $this->getAccessToken();
            } else {
                $this->console->error("Error occurred while fetching request token");
            }

        } catch (\Exception $e) {
            $this->console->error("Error while fetching request token. Error message: [$e->getMessage()]");
        }

    }

    /**
     * Get the Access token form Feedaty.
     */
    public function getAccessToken()
    {
//        $this->EncryptedKey = "MTg4NzI3MTc4OmE2MzAxODgyOGYxMDBmMGE2NmQ1ZTEyZmY4MGI4YmIyYjUwNmJkYjA =";
        $this->console->comment("***Fetching Access Token***");
        $this->console->info("Encryption Key [$this->EncryptedKey]");
        $this->console->info("Request Token [$this->RequestToken]");

        try {

            $url = "api.feedaty.com/OAuth/AccessToken";
            $headers = array(
                "Content-type: application/x-www-form-urlencoded",
                "Authorization: Basic $this->EncryptedKey",
                "oauth_token: $this->RequestToken&grant_type = authorization",
            );

//            $headers = array(
//                "Content-type: application/x-www-form-urlencoded",
//                "Authorization: Basic $this->EncryptedKey oauth_token = $this->RequestToken&grant_type = authorization",
//            );
            $data['oauth_token'] = $this->RequestToken;
            $data['grant_type Client'] = "authorization";

            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_USERAGENT, "Host Fiddler");
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($data));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            $result = curl_exec($curl);
            $info = curl_getinfo($curl);
            curl_close($curl);

            $result = json_decode($result, TRUE);
            if ($info['http_code'] == 200) {
                $this->RequestToken = $result['RequestToken'];
                $this->AccessToken = $result['AccessToken'];

                $this->console->info("AccessToken Token [$this->AccessToken]");
                $this->fetchOrder();

            } else {
                $this->console->error("Error occurred while fetching access token");
            }

        } catch (\Exception $e) {
            $this->console->error("Error while fetching access token. Error message: [$e->getMessage()]");
        }
    }

    /**
     * Fetched order from DB for the purpose of placing it into Feedaty
     */
    public function fetchOrder()
    {
        $this->console->comment("***Fetching order details to place to Feedaty***");

        $orders = DB::table('orders as o')
            ->join('order_details as od', 'o.id', '=', 'od.order_id')
            ->join('customers as c', 'c.id', '=', 'o.customer_id')
            ->select("o.id as ID", DB::raw("DATE_FORMAT(o.created_at,'%Y/%m/%d') as Date"), "c.id as CustomerID", "c.email as CustomerEmail", DB::raw("GROUP_CONCAT(od.product_id) as product_ids"))
            ->groupBy('o.id')
            ->take(5)
            ->get();

        foreach ($orders as $key => $order) {
            try {
                $product_ids = explode(",", $order->product_ids);
                $order->Products = DB::table('products as p')
                    ->join('products_lang as pl', 'p.id', '=', 'pl.product_id')
                    ->join('brands_lang as bl', 'bl.brand_id', '=', 'p.brand_id')
                    ->where('pl.lang_id', 'it')
                    ->where('bl.lang_id', 'it')
                    ->whereIn('p.id', ($product_ids))
                    ->select('p.id', 'pl.name as Name', 'p.sku as SKU', 'bl.name as Brand')
                    ->get();

                unset($order->product_ids);

            } catch (\Exception $e) {
                $this->console->error("Error occurred in fetching products. Order ID: [$order->id], Error message: [$e->getMessage()] ");
            }
        }
        $this->console->info("Products fetched successfully.");
        $this->placeOrderFeedaty($orders);
    }

    /**
     * Place the order to Feedaty
     * @param $order
     */
    public function placeOrderFeedaty($order)
    {
        $this->console->comment("***Started placing orders to Feedaty***");
        try {
            $url = "api.feedaty.com/Orders/Insert";
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($order));
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json', "Authorization: OAuth $this->AccessToken"));
            $result = curl_exec($curl);
            $info = curl_getinfo($curl);
            curl_close($curl);

            $http_code = $info['http_code'];
            if ($http_code == 200) {
                $result = json_decode($result, TRUE);
                if ($result['Success']) {
                    if ($result['Data'] != NULL) {
                        $this->storeFeedatyResponse($result['Data']);
                    } else {
                        $this->console->error("No data return in response");
                        return;
                    }
                } else {
                    $error_message = $result['Message'];
                    $this->console->error("Error occurred while placing order to Feedaty. Got response [$error_message]");
                    return;
                }
            } else {
                $this->console->error("Error occurred while placing order to Feedaty. Got response with http code [$http_code] ");
                return;
            }
        } catch (\Exception $e) {
            $this->console->error("Error occurred while placing order to Feedaty. Error message: [$e->getMessage()]");
            return;
        }
    }

    /**
     * Get the meaning of code
     * @param $code
     * @return array
     */
    public function getStatusDescription($code)
    {
        $response = [];
        switch ($code) {
            case 0 :
                $response['title'] = "Failed";
                $response['description'] = "Operation failed for technical reasons";
                break;

            case 1 :
                $response['title'] = "Success";
                $response['description'] = "Success";
                break;

            case 101 :
                $response['title'] = "CustomerIDNotVaid";
                $response['description'] = "CustomerID value null or invalid";
                break;

            case 102 :
                $response['title'] = "CustomerEmailNotValid";
                $response['description'] = "CustomerEmail value null or invalid";
                break;

            case 103 :
                $response['title'] = "CustomerOptedOut";
                $response['description'] = "The email provided in the CustomerEmail results in the list of users who have implemented the opt-out";
                break;

            case 104 :
                $response['title'] = "DateNotValid";
                $response['description'] = "The date of ’ order is invalid";
                break;

            case 105 :
                $response['title'] = "CultureCodeNotValid";
                $response['description'] = "Culture value not valid";
                break;

            case 201 :
                $response['title'] = "Duplicate";
                $response['description'] = "The order is already present (the same OrderID and CustomerID itself)";
                break;

            case 202 :
                $response['title'] = "OrderIDNotValid";
                $response['description'] = "The value null or invalid SKU";
                break;

            case 301 :
                $response['title'] = "SKUNotValid";
                $response['description'] = "The value null or invalid SKU";
                break;

            case 302 :
                $response['title'] = "ProductNameNotValid";
                $response['description'] = "Value Name null or invalid";
                break;

            case 401 :
                $response['title'] = "SurveyFlowLimitExceeded";
                $response['description'] = "Exceeded the limit set of survey can be sent to the customer within a certain time frame";
                break;

            default :
                $response['title'] = "Code not found";
                $response['description'] = "Invalid error code";
                break;

        }

        return $response;

    }

    /**
     * Store Feedaty response in table
     * @param $results
     */
    public function storeFeedatyResponse($results)
    {
        $this->console->comment("***Started storing feedaty response***");
        foreach ($results as $result) {
            $order_id = $result['OrderID'];
            $sku = $result['SKU'];
            $status_code = $result['Status'];

            try {
                $status_details = $this->getStatusDescription($result['Status']);
                $data = [
                    'order_id' => $order_id,
                    'sku' => $sku,
                    'status_code' => $status_code,
                    'status_code_title' => $status_details['title'],
                    'status_code_description' => $status_details['description'],
                ];
                DB::table('feedaty_order_response')->insert($data);

            } catch (\Exception $e) {
                $this->console->error("Error occurred while storing order response. Order ID: [$order_id], SKU: [$sku],  Error message: [$e->getMessage()] ");
            }
        }

        $this->console->comment("***Order stored successfully***");
    }
}