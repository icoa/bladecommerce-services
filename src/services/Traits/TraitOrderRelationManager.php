<?php


namespace services\Traits;

use Address;
use CartRule;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OrderDetail;
use Payment;
use Customer;
use Carrier;
use Campaign;
use Message;
use Currency;
use Cart;
use Exception;
use DB;
use Illuminate\Support\Collection;
use Order;

/**
 * Trait TraitOrderRelationManager
 * @package services\Traits
 */
trait TraitOrderRelationManager
{

    /**
     * @return BelongsTo
     */
    function shipping_method()
    {
        return $this->belongsTo(Carrier::class, 'carrier_id', 'id')->with('current_translation');
    }

    /**
     * @return BelongsTo
     */
    function payment_method()
    {
        return $this->belongsTo(Payment::class, 'payment_id', 'id')->with('current_translation');
    }

    /**
     * @return BelongsTo
     */
    function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    /**
     * @return BelongsTo
     */
    function cart_rule()
    {
        return $this->belongsTo(CartRule::class);
    }

    /**
     * @return HasMany
     */
    function order_details()
    {
        return $this->hasMany(OrderDetail::class);
    }

    /**
     * @return BelongsTo
     */
    function shipping_address()
    {
        return $this->belongsTo(Address::class);
    }

    /**
     * @return BelongsTo
     */
    function billing_address()
    {
        return $this->belongsTo(Address::class);
    }

    /**
     * @return Payment|null
     */
    public function payment()
    {
        if (isset($this->payment_method)) {
            return ($this->payment_method->hydrateWithTranslation());
        }
        return Payment::getObj($this->payment_id, $this->getLang());
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo('Customer');
    }

    /**
     * @return Cart|null
     */
    public function getCart()
    {
        return Cart::find($this->cart_id);
    }

    /**
     * @return Customer|null
     */
    public function getCustomer()
    {
        if (isset($this->customer)) {
            $this->customer->expand();
            return $this->customer;
        }
        /* @var $customer Customer */
        $customer = Customer::find($this->customer_id);
        return $customer ? $customer->expand() : null;
    }

    /**
     * @return Carrier|null
     */
    public function getCarrier()
    {
        if (isset($this->shipping_method)) {
            return ($this->shipping_method->hydrateWithTranslation());
        }
        return Carrier::getObj($this->carrier_id, $this->getLang());
    }

    /**
     * @return null|Payment
     */
    public function getPayment()
    {
        return $this->payment();
    }

    /**
     * @return Campaign|null
     */
    public function getCampaign()
    {
        $campaign = Campaign::getObj($this->campaign_id, $this->getLang());
        return $campaign;
    }

    /**
     * @return Message[]
     */
    public function getMessages()
    {
        return Message::where('order_id', $this->id)->orderBy('created_at')->get();
    }

    /**
     * @return int
     */
    public function getMessagesCount()
    {
        return count($this->getMessages());
    }

    /**
     * @return Collection
     */
    public function getTransactions()
    {
        $transactions = Collection::make([]);

        if ($this->module == Payment::MODULE_PAYPAL) {
            $transactions = DB::table('transactions_paypal')->whereIn('transaction_ref', function ($query) {
                $query->select('id')->from('transactions')->where('order_id', $this->id)->where('module', Payment::MODULE_PAYPAL);
            })->orderBy('created_at', 'desc')->get();
        }

        if ($this->module == Payment::MODULE_CREDIT_CARD) {
            $transactions = DB::table('transactions_safepay')->whereIn('transaction_ref', function ($query) {
                $query->select('id')->from('transactions')->where('order_id', $this->id)->where('module', Payment::MODULE_CREDIT_CARD);
            })->orderBy('created_at', 'desc')->get();
        }

        return $transactions;
    }

    /**
     * @return bool
     */
    public function hasTransactions()
    {

        $transactions = $this->getTransactions();

        return count($transactions) > 0;
    }
}