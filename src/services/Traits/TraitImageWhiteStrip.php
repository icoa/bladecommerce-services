<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 20/04/2018
 * Time: 12:08
 */

namespace services\src\services\Traits;

use DB, Utils, Config, Exception, File;
use Product, ProductImage;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Collection;
use Image;
use Carbon\Carbon;

trait TraitImageWhiteStrip
{
    /**
     * @param $product_id
     * @param $filename
     */
    public function removeWhiteBackground($product_id, $filename)
    {
        $spaces = $this->getSpacesForProduct($product_id);
        $spaceH = $spaces['spaceH'];
        $spaceV = $spaces['spaceV'];
        if($this->isConsole())
            $this->getConsole()->line("Performing crop at [$spaceH,$spaceV]");

        $this->cropImage($filename, $spaceH, $spaceV);

        if($this->isConsole())
            $this->getConsole()->info("Image [$filename] successfully cropped white");
    }

    /**
     * @param $product_id
     * @return array
     */
    public function getSpacesForProduct($product_id)
    {
        $default_category_id = Product::where('id', $product_id)->pluck('default_category_id');
        $spaceH = 0;
        $spaceV = 0;
        $crops = [
            '300' => [31, 80],
            '225' => [5, 17, 32, 109],
        ];
        if (in_array($default_category_id, $crops['300'])) {
            $spaceH = 300;
            $spaceV = 300;
        }
        if (in_array($default_category_id, $crops['225'])) {
            $spaceH = 225;
            $spaceV = 225;
        }
        return compact('spaceH', 'spaceV');
    }

    /**
     * @param $filename
     * @param int $spaceH
     * @param int $spaceV
     */
    protected function cropImage($filename, $spaceH = 0, $spaceV = 0)
    {
        $file = public_path('assets/products/' . $filename);
        if($this->isConsole())
            $this->getConsole()->line("Striping white from file [$file]");

        try {
            if (File::exists($file)) {

                $extension = strtolower(File::extension($file));

                if ($extension == 'jpeg')
                    $extension = 'jpg';

                $data = $this->getBoundaries($file, $extension);
                if (is_null($data))
                    return;
                //Utils::log($data, __METHOD__);
                $img = Image::make($file);
                $img->crop($data['inner_width'], $data['inner_height'], $data['left'], $data['top']);

                if ($spaceH > 0 and $spaceV > 0 and ($spaceV <= $data['top'] and $spaceH <= $data['left'])) {
                    $img->resizeCanvas($data['inner_width'] + $spaceH, $data['inner_height'] + $spaceV, 'center', false, '#ffffff');
                }

                $img->save($file);
                if($this->isConsole())
                    $this->getConsole()->info("Image $filename successfully cropped");
            }
        } catch (\Exception $e) {
            if($this->isConsole())
                $this->getConsole()->error($e->getMessage());
        }
    }

    /**
     * @param $img
     * @param $ext
     * @return array
     */
    protected function getBoundaries($img, $ext)
    {
        // If the $img is a filename, convert it into an image resource
        Utils::log("Reading $img");
        try {
            if (is_string($img)) {
                switch ($ext) {
                    case 'png':
                        $img = imagecreatefrompng($img);
                        break;

                    default:
                        $img = imagecreatefromjpeg($img);
                        break;
                }
            }
        } catch (\Exception $e) {
            try {
                if (is_string($img)) {
                    $img = imagecreatefromjpeg($img);
                }
            } catch (\Exception $e) {
                return null;
            }
        }

        // Get the width and height
        $width = imagesx($img);
        $height = imagesy($img);
        // Find the size of the borders
        $top = 0;
        $bottom = 0;
        $left = 0;
        $right = 0;

        $limit = 0xFDFDFD;
        //top
        for (; $top < $height; ++$top) {
            for ($x = 0; $x < $width; ++$x) {
                $color = imagecolorat($img, $x, $top);
                if ($color < $limit) {
                    break 2; //out of the 'top' loop
                }
            }
        }
        //bottom
        for (; $bottom < $height; ++$bottom) {
            for ($x = 0; $x < $width; ++$x) {
                if (imagecolorat($img, $x, $height - $bottom - 1) < $limit) {
                    break 2; //out of the 'bottom' loop
                }
            }
        }
        //left
        for (; $left < $width; ++$left) {
            for ($y = 0; $y < $height; ++$y) {
                if (imagecolorat($img, $left, $y) < $limit) {
                    break 2; //out of the 'left' loop
                }
            }
        }
        //right
        for (; $right < $width; ++$right) {
            for ($y = 0; $y < $height; ++$y) {
                if (imagecolorat($img, $width - $right - 1, $y) < $limit) {
                    break 2; //out of the 'right' loop
                }
            }
        }
        $inner_width = $width - ($left + $right);
        $inner_height = $height - ($top + $bottom);

        return compact('top', 'bottom', 'left', 'right', 'width', 'height', 'inner_width', 'inner_height');
    }

    public function stripWhiteFromAllImages()
    {
        $query = "select * from images where product_id in (select id from products where source='morellato' and isnull(deleted_at)) order by id desc";
        $rows = DB::select($query);
        $many = count($rows);
        $counter = 0;
        if($this->isConsole())
            $this->getConsole()->line("Found $many images...");
        foreach ($rows as $row) {
            $counter++;
            if($this->isConsole())
                $this->getConsole()->line("Processing image $row->filename ($counter/$many)");
            $spaces = $this->getSpacesForProduct($row->product_id);
            $spaceH = $spaces['spaceH'];
            $spaceV = $spaces['spaceV'];
            $this->cropImage($row->filename, $spaceH, $spaceV);
        }
    }
}