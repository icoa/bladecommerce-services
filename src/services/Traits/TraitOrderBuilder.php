<?php


namespace services\Traits;

use services\Models\BuilderPayload;
use Order;

/**
 * Trait TraitOrderBuilder
 * @package services\Traits
 * @mixin Order
 */
trait TraitOrderBuilder
{
    function hasBuilderProducts()
    {
        $products = $this->getProducts();
        foreach ($products as $product) {
            if ($product->hasParam('builder')) {
                return true;
            }
        }
        return false;
    }

    function getBuilderProductsWithPayload()
    {
        $items = [];
        $products = $this->getProducts();
        foreach ($products as $product) {
            if ($product->hasParam('builder') and $product->getParam('base')) {
                $product->payload = BuilderPayload::getByCode($product->getParam('builder'));
                $items[] = $product;
            }
        }
        return $items;
    }

    function getBuilderDocuments(){
        $files = [];
        if($this->hasBuilderProducts()){
            $bases = $this->getBuilderProductsWithPayload();
            foreach($bases as $base){
                $files[] = $base->payload->getFileData()['pdfUrl'];
            }
        }
        return $files;
    }
}