<?php

namespace services\Traits;

use Illuminate\Console\Command;

trait TraitConsole
{
    /**
     * @var Command
     */
    protected $console;

    /**
     * @param Command $console
     * @return $this
     */
    function setConsole(Command $console)
    {
        $this->console = $console;
        return $this;
    }

    /**
     * @return Command|null
     */
    function getConsole()
    {
        return $this->console;
    }

    /**
     * @param $what
     */
    protected function log($what)
    {
        $console = $this->getConsole();
        if ($console) {
            if (is_object($what) or is_array($what)) {
                print_r($what);
            } else {
                $console->comment($what);
            }
        }else{
            audit($what);
        }
    }
}