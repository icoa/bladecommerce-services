<?php

namespace services\Traits;

trait TraitProductRow
{
    /**
     * @return \Product
     */
    function getProduct()
    {
        if (isset($this->product) and $this->product instanceof \Product)
            return $this->product;

        return \Product::getFullObj($this->product_id);
    }

    /**
     * @return \ProductCombination
     */
    function getProductCombination()
    {
        if (isset($this->combination) and $this->combination instanceof \ProductCombination)
            return $this->combination;

        return \ProductCombination::getObj($this->product_combination_id);
    }

    /**
     * @return bool
     */
    function hasProductCombination()
    {
        return $this->product_combination_id > 0;
    }

    /**
     * @return bool
     */
    function isAffiliate()
    {
        return $this->affiliate_id > 0;
    }

    /**
     * @return bool
     */
    function isLocal()
    {
        return in_array($this->availability_mode, [\Product::AVAILABILITY_TYPE_LOCAL]);
    }

    /**
     * @return bool
     */
    function isOffline()
    {
        return in_array($this->availability_mode, [\Product::AVAILABILITY_TYPE_OFFLINE]);
    }

    /**
     * @return bool
     */
    function isMorellato()
    {
        return (!$this->isLocal() and !$this->isAffiliate() and !$this->isOffline());
    }


    /**
     * @return bool
     */
    public function isSubmittableToSap()
    {

        $product = $this->getProduct();
        if (is_null($product)) {
            $debug = [
                'id' => isset($this->id) ? $this->id : null,
                'order_id' => isset($this->order_id) ? $this->order_id : null,
                'product_id' => isset($this->product_id) ? $this->product_id : null,
            ];
            audit_error($debug, "Warning: product appears to be null, and it should not in " . __METHOD__);
            return false;
        }

        $productTest = $product->isSubmittableToSap();

        if (false === $productTest)
            return false;

        $membership_enabled = \Core::membership();
        //Membership/Affiliate product should not be sent to SAP
        if ($membership_enabled and isset($this->affiliate_id) and $this->affiliate_id > 0) {
            audit($productTest, 'AFFILIATE PASSED::' . __METHOD__);
            return false;
        }

        return true;
    }
}