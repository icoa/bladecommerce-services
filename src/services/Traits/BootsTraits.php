<?php


namespace services\Traits;


trait BootsTraits
{
    /**
     * Returns all the traits this class uses recursively.
     *
     * @param bool $autoload
     * @return array
     */
    final public static function getTraits($autoload = true)
    {
        return array_values(class_uses_deep(static::class, $autoload));
    }

    final public static function bootTraits()
    {
        $static = static::class;
        foreach (static::getTraits() as $trait) {
            try {
                $baseName = (new \ReflectionClass($trait))->getShortName();
            } catch (\ReflectionException $e) {
                continue;
            }
            if (method_exists($static, $method = "boot{$baseName}")) {
                $static::{$method}();
            }
        }
    }

    final public function initializeTraits()
    {
        foreach (static::getTraits() as $trait) {
            try {
                $baseName = (new \ReflectionClass($trait))->getShortName();
            } catch (\ReflectionException $e) {
                continue;
            }
            if (method_exists($this, $method = "initialize{$baseName}")) {
                $this->{$method}();
            }
        }
    }
}
