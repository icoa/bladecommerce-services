<?php


namespace services\Traits;

use Product;
use Illuminate\Database\Eloquent\Builder;
use DB;
use Trend;

/**
 * Trait TraitProductTrendManager
 * @package services\Traits
 * @mixin Product
 */
trait TraitProductTrendManager
{

    /**
     * @return array
     */
    public function getTrendsIds()
    {
        $key = "product_{$this->id}_trend";
        if(\Registry::has($key)){
            return \Registry::get($key);
        }
        $array = DB::table("trends_products")->where("product_id", $this->id)->lists("trend_id");

        if (empty($array))
            return $array;

        array_walk($array, 'intval');
        rsort($array);
        \Registry::set($key, $array);
        return $array;
    }

    /**
     * @param array $ids
     * @return array
     */
    public function getTrendsNames($ids = [])
    {
        $names = [];
        $ids = empty($ids) ? $this->getTrendsIds() : $ids;
        if (empty($ids))
            return $names;

        foreach ($ids as $id) {
            $trend = Trend::getPublicObj($id);
            if ($trend)
                $names[] = $trend->name;
        }

        return $names;
    }

    /**
     * @return string
     */
    public function getTrendsCodesAttribute()
    {
        $attribute = '';
        if(isset($this->related_entities) and isset($this->related_entities['trends'])){
            $trends = $this->related_entities['trends'];
        }else{
            $trends = $this->getTrendsIds();
        }
        if (empty($trends))
            return $attribute;

        $trends = array_map(function ($item) {
            return str_pad($item, 4, '0', STR_PAD_LEFT);
        }, $trends);

        $attribute = implode(',', $trends);
        return $attribute;
    }


    /**
     * @param Builder $builder
     * @param null $trends
     */
    function scopeWithTrends(Builder $builder, $trends = null)
    {
        $trends = is_null($trends) ? [] : (is_array($trends) ? $trends : [$trends]);
        $builder->join('trends_products', 'products.id', '=', 'trends_products.product_id');
        if (!empty($trends)) {
            $builder->whereIn('trends_products.trend_id', $trends);
        }
    }
}