<?php

namespace services\Traits;


trait TraitParams
{
    protected $params = [];

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    function setParam($name, $value)
    {
        $this->params[$name] = is_string($value) ? trim($value) : $value;
        return $this;
    }

    /**
     * @param array $params
     * @return $this
     */
    function setParams(array $params)
    {
        foreach ($params as $name => $value) {
            $this->setParam($name, $value);
        }
        return $this;
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    function getParam($key, $default = null)
    {
        return isset($this->params[$key]) ? $this->params[$key] : $default;
    }

    /**
     * @param $key
     * @return bool
     */
    function hasParam($key){
        return isset($this->params[$key]);
    }

    /**
     * @return array
     */
    function getParams()
    {
        return $this->params;
    }

    /**
     * @return $this
     */
    function resetParams()
    {
        $this->params = [];
        return $this;
    }
}