<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 20/03/2018
 * Time: 13:17
 */

namespace services\Traits;

use services\Models\BuilderProduct;

trait TraitProductBuilder
{
    /**
     * Define the relation
     * @return mixed
     */
    function builder_product()
    {
        return $this->hasOne(BuilderProduct::class, 'product_id');
    }

    /**
     * @param $query
     * @param string $type
     * @return mixed
     */
    function scopeWithBuilderType($query, $type)
    {
        return $query->whereHas('builder_product', function ($query) use ($type) {
            $query->where('type', $type);
        });
    }

    /**
     * @param $query
     * @return mixed
     */
    function scopeOnlyBases($query)
    {
        return $this->scopeWithBuilderType($query, BuilderProduct::TYPE_BASE);
    }

    /**
     * @param $query
     * @return mixed
     */
    function scopeOnlyDroplets($query)
    {
        return $this->scopeWithBuilderType($query, BuilderProduct::TYPE_DROPLET);
    }

    /**
     * @param $query
     * @return mixed
     */
    function scopeWithBuilders($query)
    {
        return $query->with('builder_product')->whereIn('id', function ($query) {
            $query->select('product_id')->from('builder_products');
        });
    }
}