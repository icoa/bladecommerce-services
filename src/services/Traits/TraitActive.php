<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 20/07/2018
 * Time: 12:33
 */

namespace services\Traits;


trait TraitActive
{
    function scopeActive($query)
    {
        return $query->where('active', 1);
    }
}