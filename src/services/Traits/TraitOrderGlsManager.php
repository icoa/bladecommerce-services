<?php

namespace services\Traits;

use Illuminate\Database\Eloquent\Builder;

use OrderState;
use PaymentState;
use Carbon\Carbon;
use services\Morellato\Gls\Service\OrderService;
use services\Morellato\Gls\Xml\Reader\CarrierShipmentXmlReader;
use services\Morellato\Gls\Xml\Reader\ShipmentXmlReader;
use AddressResponse;
use AddressTracking;
use Order;

/**
 * Trait TraitOrderGlsManager
 * @package services\Traits
 * @mixin Order
 */
trait TraitOrderGlsManager
{

    /**
     * @param $address_id
     * @return AddressResponse[]
     */
    function getAddressResponses($address_id)
    {
        return AddressResponse::where('order_id', $this->id)->where('address_id', $address_id)->get();
    }

    /**
     * @param null $product_id
     * @return AddressTracking[]
     */
    function getAddressTrackings($product_id = null)
    {
        return AddressTracking::where('order_id', $this->id)->where('product_id', $product_id)->get();
    }

    /**
     * @return array
     */
    function getGlsValidOrderStates(){
        $valid_status = [
            OrderState::STATUS_PENDING,
            OrderState::STATUS_WORKING,
            OrderState::STATUS_SHIPPED,
            OrderState::STATUS_DELIVERED,
            OrderState::STATUS_READY,
            OrderState::STATUS_PENDING_DELIVERY,
        ];
        return $valid_status;
    }

    /**
     * @return array
     */
    function getGlsInvalidPaymentStates(){
        $invalid_payment_states = [
            PaymentState::STATUS_PENDING_RIBA,
            PaymentState::STATUS_PENDING_BANKTRANSFER,
            PaymentState::STATUS_PENDING_CREDITCARD,
            PaymentState::STATUS_PENDING_PAYPAL,
            //PaymentState::STATUS_PENDING_SHOP,
            PaymentState::STATUS_ERROR,
            PaymentState::STATUS_EXPIRED,
            PaymentState::STATUS_USER_CANCELED,
            PaymentState::STATUS_REVERSED,
            PaymentState::STATUS_REFUNDED_PARTIALLY,
            PaymentState::STATUS_REFUNDED,
        ];
        return $invalid_payment_states;
    }

    /**
     * @param Builder $builder
     */
    function scopeTrackableWithGls(Builder $builder)
    {
        $valid_status = $this->getGlsValidOrderStates();
        $invalid_payment_states = $this->getGlsInvalidPaymentStates();
        $date = Carbon::yesterday()->addMonth(-1)->format('Y-m-d');

        $builder
            ->where('created_at', '>=', $date)
            ->forSap()
            ->withStatus($valid_status)
            ->withoutPaymentStatus($invalid_payment_states)
            ->whereNotIn('id', function ($query) {
                //29 = RESO MITTENTE
                //906 = CONSEGNATA
                $query->select('order_id')->from('mi_address_trackings')->where('result', 906)->orWhere('result', 29);
            });
    }

    /**
     * @param Builder $builder
     */
    function scopeWithoutGlsTracking(Builder $builder)
    {
        $valid_status = $this->getGlsValidOrderStates();
        $invalid_payment_states = $this->getGlsInvalidPaymentStates();
        $date = Carbon::yesterday()->addMonth(-1)->format('Y-m-d');

        $builder
            ->where('created_at', '>=', $date)
            ->forSap()
            ->withStatus($valid_status)
            ->withoutPaymentStatus($invalid_payment_states)
            ->whereNotIn('id', function ($query) {
                $query->select('order_id')->from('mi_address_trackings');
            });
    }

    /**
     * Decide the tracking mode for GLS
     *
     * @return string
     */
    function getGlsTrackingMode()
    {
        $shopStartingPoint = $this->hasShopAvailability();
        $shopEndingPoint = $this->hasDeliveryStore();

        $matrix = [
            'shop.shop' => OrderService::GLS_TRACKING_MODE_NUMRIT,
            'shop.customer' => OrderService::GLS_TRACKING_MODE_NUMRIT,
            'online.shop' => OrderService::GLS_TRACKING_MODE_BDA,
            'online.customer' => OrderService::GLS_TRACKING_MODE_BDA,
        ];

        $leftKey = ($shopStartingPoint) ? 'shop' : 'online';
        $rightKey = ($shopEndingPoint) ? 'shop' : 'customer';

        return $matrix["{$leftKey}.{$rightKey}"];
    }


    /**
     * @return string
     */
    function getGlsBda()
    {
        $orderSlip = $this->getMorellatoSlip();
        return ($orderSlip and \OrderHelper::isValidGlsCode($orderSlip->delivery_id)) ? $orderSlip->delivery_id : $this->shipping_number;
    }


    /**
     * @return string
     */
    function getGlsShippingNumber()
    {
        return $this->getGlsBda();
    }


    /**
     * @return string
     */
    function getGlsNumRit()
    {
        $orderSlip = $this->getMorellatoSlip();
        return ($orderSlip and isset($orderSlip->withdrawal_id) and \OrderHelper::isValidGlsCode($orderSlip->withdrawal_id)) ? $orderSlip->withdrawal_id : $this->carrier_shipping;
    }

    /**
     * @return string
     */
    function getGlsCarrierShipping()
    {
        return $this->getGlsNumRit();
    }

    /**
     * @return null|string
     */
    function getGlsXmlUrl()
    {
        $mode = $this->getGlsTrackingMode();

        if ($mode == OrderService::GLS_TRACKING_MODE_BDA) {
            /** @var ShipmentXmlReader $xmlReader */
            $xmlReader = app(ShipmentXmlReader::class);
            $xmlReader->setModeByOrder($this)->setBda($this->getGlsShippingNumber(), false);
            return $xmlReader->getGlsXmlUrl();
        }


        if ($mode == OrderService::GLS_TRACKING_MODE_NUMRIT) {
            /** @var CarrierShipmentXmlReader $xmlReader */
            $xmlReader = app(CarrierShipmentXmlReader::class);
            $xmlReader->setModeByOrder($this)->setCarrierShipment($this->getGlsCarrierShipping(), false);
            return $xmlReader->getGlsXmlUrl();
        }

        return null;
    }

    /**
     * @return string|null
     */
    function getGlsTrackingUrl()
    {
        /** @var \AddressTracking[] $addressTracking */
        $addressTrackings = $this->getAddressTrackings();
        if ($addressTrackings and count($addressTrackings)) {
            return $addressTrackings[0]->url;
        }
        return null;
    }

    /**
     * @return int|null
     */
    function getGlsLatestTrackingResult()
    {
        /** @var \AddressTracking[] $addressTracking */
        $addressTrackings = $this->getAddressTrackings();
        if ($addressTrackings and count($addressTrackings)) {
            return (int)$addressTrackings[0]->result;
        }
        return null;
    }
}