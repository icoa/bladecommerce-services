<?php

namespace services\Traits;

use Exception;
use services\Repositories\GiftCardRepository;
use Theme;
use Event;
use Frontend\CartManager;

/**
 * Trait TraitGiftCardCartManager
 * @package services\Traits
 * @mixin CartManager
 */
trait TraitGiftCardCartManager
{

    public function initializeTraitGiftCardCartManager(){
        if (self::isGiftCardServiceEnabled() === false)
            return;

        //audit(__METHOD__);
        Event::listen('cart.remove.product', 'Frontend\\CartManager@onRemoveProductWithGiftCard');
        Event::listen('cart.validate.context', 'Frontend\\CartManager@hasValidContextWithGiftCard');
    }

    /**
     * @return bool
     */
    public static function isGiftCardServiceEnabled()
    {
        return (bool)config('negoziando.giftcard.enabled', false);
    }

    /**
     * @param $product
     */
    public function onRemoveProductWithGiftCard($product)
    {
        //if the cart has been emptied and we still have an attached card, remove the card
        if ($this->isEmpty() and $this->hasGiftcard()) {
            audit('GiftCard removed with emptied cart', __METHOD__);
            $this->removeGiftcard();
        }
    }

    /**
     * @param $code
     * @return array
     */
    function checkGiftcard($code)
    {
        $success = false;
        $msg = null;
        $html = null;
        $repository = new GiftCardRepository();
        try {
            $giftCard = $repository->check($code);
            $html = Theme::partial('giftcard.cart.verify', compact('giftCard'));
            $success = true;
        } catch (Exception $e) {
            $msg = $e->getMessage();
        }
        return compact('success', 'msg', 'html');
    }


    function addGiftcard($code)
    {

        $success = false;
        $msg = null;
        $repository = new GiftCardRepository();
        try {

            if ($this->hasVirtualProducts()) {
                throw new Exception(lex('error_giftcard_interception'), 503);
            }

            $giftCard = $repository->check($code);

            if ($giftCard->amount <= 0) {
                throw new Exception(lex('error_giftcard_empty'), 505);
            }

            $cart = $this->cart();
            $params = $this->getParams();

            if (!isset($params['giftcard'])) {
                $params['giftcard'] = [];
            }
            $params['giftcard'] = ['code' => $giftCard->code, 'amount' => $giftCard->amount, 'id' => $giftCard->id];
            $this->setParams($params);

            $this->saveCart();

            $success = true;
            $msg = lex('success_giftcard_added');
        } catch (Exception $e) {
            $msg = $e->getMessage();
        }
        return compact('success', 'msg');
    }

    function removeGiftcard()
    {
        $cart = $this->cart();
        $params = $this->getParams();

        unset($params['giftcard']);

        $this->params = $params;

        $this->saveCart();

        $success = true;
        return compact('success');
    }

    /**
     * @return bool
     */
    function hasGiftcard()
    {
        $cart = $this->cart();
        $params = $this->getParams();
        //audit(isset($params['giftcard']) and isset($params['giftcard']['code']), __METHOD__, 'params');
        return isset($params['giftcard']) and isset($params['giftcard']['code']);
    }

    /**
     * @return \GiftCard|null
     */
    function getGiftcard()
    {
        if ($this->hasGiftcard()) {
            $params = $this->getParams();
            $repository = new GiftCardRepository();
            return $repository->getByCode($params['giftcard']['code']);
        } else {
            return null;
        }
    }

    /**
     * @return null|string
     */
    function getGiftcardCode()
    {
        $giftCard = $this->getGiftcard();
        return $giftCard ? $giftCard->code . ' (' . \Format::currency($giftCard->amount, true) . ')' : null;
    }

    /**
     * @param \Payment $payment
     * @return bool
     */
    function getPaymentGiftCardAssert(\Payment $payment)
    {
        $giftcard_enabled = config('negoziando.giftcard.enabled', false);
        $isSelectedPayment = $payment->id == config('negoziando.giftcard.payment_id', 13);
        $orderHasGiftCard = $this->hasGiftcard();
        $orderTotal = $this->getTotalFinal();

        //$data = compact('giftcard_enabled', 'isSelectedPayment', 'orderHasGiftCard', 'orderTotal');
        //audit($data, __METHOD__);

        if ($isSelectedPayment) { //enable "special gift card gateway" only if a Gift Card has been used and the total order is 0
            if ($giftcard_enabled === false)
                return false;

            if ($orderHasGiftCard === false)
                return false;

            if ($orderTotal == 0) {
                return true;
            }

            return false;

        } else { //if an order with gift card is 0, than all the other gateways must be disabled
            if ($giftcard_enabled === false)
                return true;

            if ($orderHasGiftCard === false)
                return true;

            if ($orderTotal > 0)
                return true;

            return false;
        }
    }

    /**
     * @return bool|string
     */
    public function hasValidContextWithGiftCard()
    {
        //audit(__METHOD__);
        //audit($this->hasVirtualProducts(), __METHOD__, 'hasVirtualProducts()');
        //audit($this->hasGiftcard(), __METHOD__, 'hasGiftcard()');
        //if the current cart has both virtual products and gift card, invalidate the whole cart
        if ($this->hasVirtualProducts() and $this->hasGiftcard()) {
            audit('giftcard_invalid_state', __METHOD__);
            return 'giftcard_invalid_state';
        }
        return true;
    }
}