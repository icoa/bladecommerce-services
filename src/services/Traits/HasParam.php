<?php

namespace services\Traits;

trait HasParam
{
    /**
     * @param $value
     */
    public function setParamsAttribute($value)
    {
        $this->attributes['params'] = is_array($value) ? serialize($value) : null;
    }

    /**
     * @param $value
     * @return array|null
     */
    public function getParamsAttribute($value)
    {
        return ($value == null OR $value == '') ? null : unserialize($value);
    }

    /**
     * @param $name
     * @return bool
     */
    public function hasParam($name)
    {
        $params = $this->getAttribute('params');
        return isset($params[$name]) AND $params[$name] != '';
    }

    /**
     * @param $name
     * @param null $default
     * @return null|mixed
     */
    public function getParam($name, $default = null)
    {
        $params = $this->getAttribute('params');
        return $this->hasParam($name) ? $params[$name] : $default;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    public function setParam($name, $value)
    {
        $params = $this->getAttribute('params');
        if (is_null($value)) {
            if (isset($params[$name]))
                unset($params[$name]);
        } else {
            $params[$name] = $value;
        }

        $this->setParamsAttribute($params);
    }

}