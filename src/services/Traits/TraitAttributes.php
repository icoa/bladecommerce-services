<?php

namespace services\Traits;


trait TraitAttributes
{
    protected $attributes = [];

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    function setAttribute($name, $value)
    {
        array_set($this->attributes, $name, is_string($value) ? trim($value) : $value);
        return $this;
    }

    /**
     * @param $attributes
     * @return $this
     */
    function setAttributes($attributes)
    {
        if (is_object($attributes)) {
            if (method_exists($attributes, 'toArray')) {
                $attributes = $attributes->toArray();
            } else {
                $attributes = (array)$attributes;
            }
        }

        foreach ($attributes as $name => $value) {
            $this->setAttribute($name, $value);
        }
        return $this;
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed|null
     */
    function getAttribute($key, $default = null)
    {
        return array_get($this->attributes, $key, $default);
    }

    /**
     * @param $key
     * @return bool
     */
    function hasAttribute($key)
    {
        return array_get($this->attributes, $key) !== null;
    }

    /**
     * @return array
     */
    function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @return $this
     */
    function resetAttributes()
    {
        $this->attributes = [];
        return $this;
    }

    /**
     * @param $key
     */
    function removeAttribute($key){
        array_forget($this->attributes, $key);
    }
}