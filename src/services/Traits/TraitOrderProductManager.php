<?php


namespace services\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OrderDetail;
use GiftCard;
use GiftCardMovement;
use Product;
use ProductCombination;
use services\Repositories\GiftCardRepository;
use Exception;
use DB;
use Format;
use Order;

/**
 * Trait TraitOrderProductManager
 * @package services\Traits
 * @mixin Order
 */
trait TraitOrderProductManager
{
    /**
     * @return HasMany
     */
    function details()
    {
        return $this->hasMany('OrderDetail');
    }

    /**
     * @param bool $resolve
     * @param null $warehouse
     * @param null $shop_id
     * @return OrderDetail[]
     */
    function getProducts($resolve = true, $warehouse = null, $shop_id = null)
    {
        $builder = OrderDetail::where('order_id', $this->id);
        $conditions = [];
        if ($warehouse) {
            $conditions['warehouse'] = $warehouse;
        }
        if ($shop_id) {
            $conditions['availability_shop_id'] = $shop_id;
        }
        if (!empty($conditions)) {
            $builder->where(function ($query) use ($conditions) {
                $query->where($conditions)->orWhere(function ($query) {
                    $query->where(['availability_mode' => self::MODE_STAR]);
                });
            });
        }

        $rows = $builder->get();
        foreach ($rows as $row) {
            $row->product = null;
            if ($resolve) {
                $product = Product::getObj($row->product_id);
                if ($product) {
                    $product->setFullData();
                }
                $row->product = $product;
            }
        }
        //add "fake" additional rows if the current inner scope is 'DOCUMENT' or 'FEES'
        if ($this->hasInnerScope([self::SCOPE_DOCUMENT, self::SCOPE_FEES])):

            //create a 'Gift Card' fake row
            if ($this->hasGiftcard() and $resolve) {
                $giftCardMovement = $this->getGiftcardMovement();
                if ($giftCardMovement) {
                    $amount = $giftCardMovement->amount * -1;
                    $amountUntaxed = $amount;
                    $tax_rate = 0;
                    //$amountUntaxed = \Core::untax($amount, \Core::getDefaultTaxRate());
                    //$tax_rate = \Core::getDefaultTaxRate();
                    $product = Product::getObj($giftCardMovement->giftCard->product_id);
                    $data = OrderDetail::getGeneralAttributes($amount, $amountUntaxed);
                    $data['order_id'] = $this->id;
                    $data['product_id'] = $giftCardMovement->giftCard->product_id;
                    $data['product_name'] = $giftCardMovement->giftCard->name . ' [*]';
                    $data['product_reference'] = $product->sku;
                    $data['product_sap_reference'] = $product->sap_sku;
                    $data['tax_rate'] = $tax_rate;
                    $data['product_combination_id'] = 'GF';
                    $data['product_quantity'] = -1;

                    if ($resolve) {
                        $data['product'] = $product;
                    }
                    $orderDetail = new OrderDetail($data);
                    $rows->push($orderDetail);
                    audit('addedGiftCardFakeRow', __METHOD__);
                }
            }

            //create a Voucher/Fidelity points fake row, but only in the 'DOCUMENT' scope
            if ($resolve and $this->hasInnerScope(self::SCOPE_DOCUMENT) and ($this->hasFidelityPoints() or $this->hasVouchers())) {
                $amountPoints = $this->getFidelityUsedPointsAmount();
                $amountVouchers = $this->getTotalUsedVouchersAmount();
                $fakeAmount = $amountPoints + $amountVouchers;
                if ($fakeAmount > 0) {
                    //get the fake amount from the OrderDetails, since this order can be splitted
                    $amount = 0;
                    foreach ($rows as $row) {
                        $amount += $row->reduction_amount_tax_incl * $row->product_quantity;
                    }
                    $amount = $amount * -1;
                    $tax_rate = \Core::getDefaultTaxRate();
                    $amountUntaxed = \Core::untax($amount, $tax_rate);

                    $data = OrderDetail::getGeneralAttributes($amount, $amountUntaxed);
                    $data['order_id'] = $this->id;
                    $data['product_id'] = null;
                    $data['product_name'] = trans('fee.pdf.product.vouchers');
                    $data['tax_rate'] = $tax_rate;
                    $data['product_quantity'] = -1;

                    $orderDetail = new OrderDetail($data);
                    $rows->push($orderDetail);
                    audit('addedVoucherFidelityFakeRow', __METHOD__);
                }
            }
        endif;
        return $rows;
    }


    function getProductsDetails()
    {
        $products = $this->getProducts();
        $currency_id = $this->currency_id;

        $total_products = \Format::currency($this->total_products, $currency_id);

        $lbl_sku = trans('template.sku');
        $lbl_product = trans('template.product');
        $lbl_unitprice = trans('template.unitprice');
        $lbl_quantity = trans('template.quantity');
        $lbl_total = trans('template.total');
        $lbl_total_products = trans('template.total_products');

        $table = <<<TABLE
<table style="width:100%; border-color:#ddd" border="1" cellpadding="4" cellspacing="4">
<thead>
<tr>
<th align="left">$lbl_sku</th>
<th align="left">$lbl_product</th>
<th align="right">$lbl_unitprice</th>
<th align="center">$lbl_quantity</th>
<th align="right">$lbl_total</th>
</tr>
</thead>
<tbody>
TABLE;

        foreach ($products as $p) {

            $price = \Format::currency($p->product_item_price, true, $currency_id);
            $price_total = \Format::currency($p->total_price_tax_incl, true, $currency_id);


            $table .= <<<HTML
<tr>
<td width="15%">$p->product_reference</td>
<td width="40%">$p->product_name</td>
<td width="15%" align="right">$price</td>
<td width="15%" align="center">$p->product_quantity</td>
<td width="15%" align="right">$price_total</td>
</tr>
HTML;
        }


        $table .= <<<TABLE
</tbody
<tfoot>
<tr>
<td colspan="4" align="right">$lbl_total_products:</td><td align="right"><strong>$total_products</strong></td>
</tr>
</tfoot>
</table>
TABLE;

        return $table;

    }


    /**
     * @return int
     */
    function getMissingProductsQty()
    {
        $products = $this->getProducts();
        $sum = 0;
        foreach ($products as $p) {
            if ($p->product) {
                $qty = ($p->product->qty > 0) ? $p->product->qty : 0;
                \Utils::log("{$p->product_quantity} > {$qty}", "getMissingProductsQty");
                if ($p->product_quantity > $qty) {
                    $sum += $qty - $p->product_quantity;
                }
            }

        }
        \Utils::log("{$sum}", "getMissingProductsQty SUM");
        return $sum;
    }

    /**
     * @param $product_id
     * @param null $price
     * @param int $quantity
     * @param int $product_combination_id
     */
    function addProduct($product_id, $price = null, $quantity = 1, $product_combination_id = 0)
    {
        $p = Product::getObj($product_id);
        $tax_rate = $p->getTaxRate();
        $tax_name = $p->getTaxName();
        if ($price == null) {
            $price = $p->price;
        }

        $weight = $p->weight;
        $name = trim($p->name);
        $sku = trim($p->sku);
        $sap_sku = trim($p->sap_sku);
        $ean13 = trim($p->ean13);
        $upc = trim($p->upc);
        $quantity_in_stock = $p->qty;
        if ($product_combination_id > 0) {
            $combination = ProductCombination::getObj($product_combination_id);
            if ($combination) {
                $weight += $combination->weight;
                $quantity_in_stock = $combination->quantity;
                $name = $name . '<br>' . $combination->label;
                if (trim($combination->sku) != '') {
                    $sku = $combination->sku;
                }
                if (trim($combination->ean13) != '') {
                    $ean13 = $combination->ean13;
                }
                if (trim($combination->upc) != '') {
                    $upc = $combination->upc;
                }
                if (trim($combination->sap_sku) != '') {
                    $sap_sku = $combination->sap_sku;
                }
            }
        }

        $data = [
            'order_id' => $this->id,
            'product_id' => $p->id,
            'product_name' => $name,
            'product_reference' => $sku,
            'product_sap_reference' => $sap_sku,
            'product_ean13' => $ean13,
            'product_upc' => $upc,
            'product_weight' => (float)$weight,
            'product_quantity' => (int)$quantity,
            'product_quantity_in_stock' => $quantity_in_stock,
            'product_quantity_refunded' => 0,
            'product_quantity_return' => 0,
            'product_quantity_reinjected' => 0,
            'product_buy_price' => (float)$p->buy_price,
            'product_sell_price' => (float)$p->sell_price,
            'product_sell_price_wt' => (float)$p->sell_price_wt,
            'product_price' => (float)$p->price,
            'product_unit_price' => (float)$p->unit_price,
            'product_item_price' => (float)$price,
            'product_item_price_tax_excl' => (float)\Core::untax($price, $tax_rate),
            'price_tax_incl' => 0,
            'price_tax_excl' => 0,
            'reduction_percent' => 0,
            'reduction_amount' => 0,
            'reduction_amount_tax_incl' => 0,
            'reduction_amount_tax_excl' => 0,
            'cart_rule_id' => 0,
            'product_combination_id' => $product_combination_id,
            'special' => 0,
            'tax_rate' => (float)$tax_rate,
            'tax_name' => $tax_name,
            'cart_price_tax_incl' => (float)$price,
            'cart_price_tax_excl' => (float)\Core::untax($price, $tax_rate),
            'total_price_tax_incl' => (float)($price * $quantity),
            'total_price_tax_excl' => (float)\Core::untax(($price * $quantity), $tax_rate),
            'warehouse' => $this->warehouse,
            'availability_mode' => $this->availability_mode,
            'availability_shop_id' => $this->availability_shop_id,
        ];

        $details = new \OrderDetail();
        $details->setAttributes($data);
        $details->save();

        if ($details AND $details->id > 0) {
            $p->decrementQty($quantity, $this->id, $product_combination_id);
        }
        $this->updateTotals();
    }

    /**
     * @param $detail
     */
    function removeDetail($detail)
    {
        \Utils::log($detail, __METHOD__);
        $p = Product::getObj($detail->product_id);
        $qty = $detail->product_quantity;
        if ($p) {
            $p->incrementQty($qty, $this->id, $detail->product_combination_id);
        }
        OrderDetail::where('id', $detail->id)->delete();
        $this->updateTotals();
    }


    /**
     * @return int
     */
    function getVirtualProductsCount()
    {
        $total = 0;
        $products = $this->getProducts();
        foreach ($products as $product) {
            if ($product->product->type == 'virtual') {
                $total++;
            }
        }
        return $total;
    }

    /**
     * @return int
     */
    function getDefaultProductsCount()
    {
        $total = 0;
        $products = $this->getProducts();
        foreach ($products as $product) {
            if ($product->product->type == 'default') {
                $total++;
            }
        }
        return $total;
    }

    /**
     * @return bool
     */
    function hasVirtualProducts()
    {
        return $this->getVirtualProductsCount() > 0;
    }

    /**
     * @return bool
     */
    function hasGiftcard()
    {
        $params = $this->getAttribute('params');
        return isset($params['giftcard']) and isset($params['giftcard']['code']);
    }

    /**
     * @return \GiftCard|null
     */
    function getGiftcard()
    {
        if ($this->hasGiftcard()) {
            $params = $this->getAttribute('params');
            $repository = new GiftCardRepository();
            return $repository->getByCode($params['giftcard']['code']);
        } else {
            return null;
        }
    }

    /**
     * @return null|GiftCardMovement
     */
    function getGiftcardMovement()
    {
        if ($this->hasGiftcard()) {
            $giftCardMovement = GiftCardMovement::where('order_id', $this->id)->with('giftCard')->first();
            if ($giftCardMovement) {
                $giftCardMovement->amount = Format::float(str_replace(',', '.', $giftCardMovement->amount));
                return $giftCardMovement;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    /**
     * @return float
     */
    function getSubTotalExceptExtra()
    {
        return $this->total_products + $this->total_shipping + $this->total_payment + $this->total_wrapping - $this->total_discounts;
    }

    /**
     * @param $delivery_id
     * @param $sales_id
     */
    public function setOrderSlip($delivery_id, $sales_id)
    {
        $data = [
            'order_id' => $this->id,
            'delivery_id' => \OrderHelper::isValidGlsCode($delivery_id) ? $delivery_id : null,
            'sales_id' => \OrderHelper::isValidGlsCode($sales_id) ? $sales_id : null,
            'created_at' => date('Y-m-d H:i:s')
        ];
        if ($data['delivery_id'] === null && $data['sales_id'] === null) {
            return;
        }
        //upsert the data
        $many = DB::table('mi_orders_slips')->where('order_id', $this->id)->count();
        if ($many > 0) {
            DB::table('mi_orders_slips')->where('order_id', $this->id)->update($data);
        } else {
            DB::table('mi_orders_slips')->insert($data);
        }
    }

    /**
     * @return string
     */
    public function getGrandTotalAttribute()
    {
        $factor = $this->isStarling() ? -1 : 1;
        return Format::currency($this->total_order * $factor, true);
    }


    /**
     * @param $status
     * @return OrderDetail[]|array
     */
    function getProductsWithStatus($status)
    {
        $status = is_array($status) ? $status : [$status];
        $rows = [];
        $products = $this->getProducts();
        foreach ($products as $product) {
            if (in_array($product->status, $status)) {
                $rows[] = $product;
            }
        }
        return $rows;
    }

    /**
     * @param $status
     * @return bool
     */
    function hasProductsWithStatus($status)
    {
        return !empty($this->getProductsWithStatus($status));
    }
}