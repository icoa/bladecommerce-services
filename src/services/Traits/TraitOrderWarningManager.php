<?php


namespace services\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Order;
use Payment;
use OrderState;
use PaymentState;
use Illuminate\Database\Eloquent\Builder;
use services\Models\OrderSlipDetail;
use services\Morellato\Soap\Service\AbstractOrderService;
use Carrier;
use services\Models\OrderSlip;
use services\Models\CustomerEmailWarning;
use Config;
use DB;
use MorellatoShop;
use Exception;

/**
 * Trait TraitOrderWarningManager
 * @package services\Traits
 * @mixin Order
 */
trait TraitOrderWarningManager
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    function customer_email_warnings(){
        return $this->hasMany(CustomerEmailWarning::class);
    }

    /**
     * @return CustomerEmailWarning[]
     */
    function getCustomerEmailWarnings()
    {
        $rows = CustomerEmailWarning::withOrder($this->id)->orderBy('created_at', 'desc')->get();
        return $rows;
    }

    /**
     * @param Builder $builder
     * @param $flag
     * @throws Exception
     */
    public function scopeWithWarnableFlag(Builder $builder, $flag = 0)
    {
        $flag = (int)$flag;

        if ($flag != 0 and $flag != 1)
            throw new Exception("Flag parameter must be 1 o 0");

        $builder->where('flag_warnable', $flag);
        $builder->has('customer_email_warnings', '<=', (int)config('order.max_warnings_per_order', 8));
    }



    /**
     * Determine if the order has a number of registered warnings
     *
     * @param string|array $type
     * @param int $limit
     *
     * @return bool
     */
    function hasNumberOfWarnings($type = null, $limit = 2)
    {
        $type = ($type) ? $type : CustomerEmailWarning::TYPE_SHOP_PICKUP;

        //get total warnings by type
        $totals = CustomerEmailWarning::withType($type)
            ->withOrder($this->id)
            ->count();

        return $totals >= $limit;
    }



    /**
     * Determine if the order has an exact number of registered warnings
     *
     * @param string|array $type
     * @param int $limit
     *
     * @return bool
     */
    function hasExactNumberOfWarnings($type = null, $limit = 0)
    {
        $type = ($type) ? $type : CustomerEmailWarning::TYPE_SHOP_PICKUP;

        //get total warnings by type
        $totals = (int)CustomerEmailWarning::withType($type)
            ->withOrder($this->id)
            ->count();

        return $totals === $limit;
    }

    /**
     * Determine if the order has a valid time windows/offset for customer warning
     *
     * @param string|array $type
     * @param int $days
     *
     * @return bool
     */
    function hasValidTimeOffsetForCustomerWarning($type = null, $days = 7)
    {
        $created = Carbon::parse($this->created_at);
        $now = Carbon::now();
        $type = ($type) ? $type : CustomerEmailWarning::TYPE_SHOP_PICKUP;

        $diff = $created->diffInDays($now);

        //audit(compact('created', 'now', 'type', 'diff', 'days'), __METHOD__.'::params');

        //first check from the order date
        if ($diff <= $days) {
            return false;
        }

        //audit_watch();
        //get the last customer warning
        $warning = CustomerEmailWarning::withType($type)
            ->withOrder($this->id)
            ->orderBy('created_at', 'desc')
            ->first();

        //audit($warning, __METHOD__, 'last customer warning');

        //audit_watch(false);

        //if there are no warning, than probably this will be the first warning
        if (is_null($warning)) {
            return true;
        }

        //if the check of the days is 0 and warning exists, so there is no need to check
        if($days == 0 and $warning)
            return true;

        //if 'days' is zero, than do not check for difference
        if ($days <= 0 and !is_null($warning))
            return false;

        //if the warning exists, 'created' belongs to the record
        $created = Carbon::parse($warning->created_at);
        $diff = $created->diffInDays($now);
        if ($diff <= $days) {
            return false;
        }

        return true;
    }

    /**
     * Selects all orders that can valid to warn Customer about missing shop pick-up
     *
     * @param Builder $builder
     */
    function scopeCustomerWarnableMissedPickup(Builder $builder)
    {
        $valid_order_states = [
            OrderState::STATUS_READY,
            OrderState::STATUS_READY_PICKUP,
        ];

        $payment_methods = [
            Payment::TYPE_SHIPPING_POINT,
            //Payment::TYPE_SHOP_ANTICIPATED, NB: La mail di sollecito non va inviata agli ordini con tipo di pagamento: PAGAMENTO ANTICIPATO
            Payment::TYPE_WITHDRAWAL
        ];

        $this->scopeWithoutOrderSlipDetailType($builder, OrderSlipDetail::TYPE_SOLD);
        //$this->scopeWithOrderSlipDetailType($builder, OrderSlipDetail::TYPE_SHIPPED);

        $this->scopeForSap($builder);
        $this->scopeWithWarnableFlag($builder);
        $this->scopeWithoutAffiliate($builder);
        $this->scopeAvailable($builder);
        $this->scopeWithStatus($builder, $valid_order_states);
        $this->scopeWithPayment($builder, $payment_methods);

        $builder->orderBy('id', 'desc');
    }

    /**
     * Selects all orders that can valid to warn Customer about missing shop pick-up
     *
     * @param Builder $builder
     */
    function scopeCustomerWarnablePackageReady(Builder $builder)
    {
        $valid_order_states = [
            OrderState::STATUS_READY,
            OrderState::STATUS_READY_PICKUP
        ];

        $payment_methods = [
            Payment::TYPE_SHIPPING_POINT,
            Payment::TYPE_SHOP_ANTICIPATED,
            Payment::TYPE_WITHDRAWAL,
            Payment::TYPE_CREDITCARD,
            Payment::TYPE_PAYPAL,
            Payment::TYPE_GIFTCARD,
        ];

        $this->scopeForSap($builder);
        $this->scopeWithWarnableFlag($builder);
        $this->scopeWithoutAffiliate($builder);
        $this->scopeAvailable($builder);
        $this->scopeWithStatus($builder, $valid_order_states);
        $this->scopeWithPayment($builder, $payment_methods);

        $builder->orderBy('id', 'desc');
    }

    /**
     * Selects all orders that can be warned to shop when a new carrier shipment has been assigned
     *
     * @param Builder $builder
     */
    function scopeShopWarnableCarrierShipment(Builder $builder)
    {
        $valid_order_states = [
            OrderState::STATUS_SHIPPED
        ];

        $valid_order_details_states = [
            \OrderDetail::STATUS_SHOP_SHIPPED
        ];

        $payment_methods = [
            Payment::TYPE_SHIPPING_POINT,
            Payment::TYPE_SHOP_ANTICIPATED,
            Payment::TYPE_WITHDRAWAL
        ];

        $this->scopeForSap($builder);
        $this->scopeWithWarnableFlag($builder);
        $this->scopeWithoutAffiliate($builder);
        $this->scopeAvailable($builder);
        $this->scopeWithStatus($builder, $valid_order_states);
        $this->scopeWithPayment($builder, $payment_methods);
        $this->scopeWithOrderDetailStatus($builder, $valid_order_details_states);

        $builder->orderBy('id', 'desc');
    }
}