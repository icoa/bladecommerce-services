<?php

namespace services\Traits;

use Utils;
use Exception;

trait TraitLocalDebug
{
    /**
     * @param $value
     * @param string $message
     * @param null $method
     */
    protected function audit($value, $message = '', $method = null)
    {
        if($this->localDebug === false)
            return;
        Utils::log($value, $message, $method);
    }

    /**
     * @param $bool
     * @param $value
     * @param string $message
     * @param null $method
     */
    protected function auditBy($bool, $value, $message = '', $method = null)
    {
        if($this->localDebug === false)
            return;

        if ($bool)
            Utils::log($value, $message, $method);
    }

    /**
     *
     */
    protected function audit_watch(){
        if($this->localDebug === false)
            return;

        audit_watch();
    }
}