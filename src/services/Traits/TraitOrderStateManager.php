<?php


namespace services\Traits;

use Carrier;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use OrderHistory;
use OrderState;
use PaymentState;
use Exception;
use Illuminate\Support\Collection;
use DB;
use Order;
use services\Models\OrderTracking;

/**
 * Trait TraitOrderStateManager
 * @package services\Traits
 * @mixin Order
 */
trait TraitOrderStateManager
{
    /**
     * @return bool
     */
    public function hasBeenPaid()
    {
        return $this->hasCurrentPaymentStatus([PaymentState::STATUS_COMPLETED]);
    }

    /**
     * @return bool
     */
    function hasShipment()
    {
        $status = $this->getStatus();
        return ($status->shipped == 1 AND $this->getTrackingUrl() !== false);
    }

    /**
     * @return HasMany
     */
    function histories()
    {
        return $this->hasMany(OrderHistory::class);
    }


    /**
     * @return HasMany
     */
    function histories_states()
    {
        return $this->hasMany(OrderHistory::class)->where('type', OrderHistory::TYPE_ORDER);
    }


    /**
     * @return HasMany
     */
    function histories_payment_states()
    {
        return $this->hasMany(OrderHistory::class)->where('type', OrderHistory::TYPE_PAYMENT);
    }


    /**
     * @return OrderState|null
     */
    function getStatus()
    {
        $model = OrderState::getObj($this->status, $this->getLang());
        return $model;
    }

    /**
     * @return PaymentState|null
     */
    function getPaymentStatus()
    {
        $model = PaymentState::getObj($this->payment_status, $this->getLang());
        return $model;
    }

    /**
     * @param bool $formatted
     * @return null|string
     */
    function statusName($formatted = false)
    {
        $record = $this->getStatus();
        if ($record == null) {
            return null;
        }
        if ($formatted) {
            $icon = str_replace('font', 'fa', $record->icon);
            $str = "<span class='label' style='background-color: $record->color'><i class='fa $icon'></i> $record->name</span>";
        } else {
            $str = $record->name;
        }
        return (string)$str;
    }

    /**
     * @param bool $formatted
     * @return null|string
     */
    function paymentStatusName($formatted = false)
    {
        $record = $this->getPaymentStatus();
        if ($record == null) {
            return null;
        }
        if ($formatted) {
            $icon = str_replace('font', 'fa', $record->icon);
            $str = "<span class='label' style='background-color: $record->color'><i class='fa $icon'></i> $record->name</span>";
        } else {
            $str = $record->name;
        }
        return (string)$str;
    }

    /**
     * @param $status_id
     * @param bool $update_order
     * @param null|string $reason
     * @param null|array $tracking_by
     */
    public function setStatus($status_id, $update_order = true, $reason = null, $tracking_by = null)
    {
        $type = OrderHistory::TYPE_ORDER;
        $historyRecordsCount = (int)OrderHistory::where(['order_id' => $this->id, 'status_id' => $status_id, 'type' => $type])->count();

        $obj = new OrderHistory();
        $obj->type = $type;
        $obj->order_id = $this->id;
        $obj->status_id = $status_id;
        $obj->setReason($reason, $tracking_by);

        if ((int)$this->status === (int)$status_id) {
            //if there are no records in the history, create an entry anyway
            if ($historyRecordsCount === 0) {
                $obj->save();
                $this->logger()->log('Added new OrderHistory order status ' . OrderState::statusToString($status_id), 'STATUS_EVENT');
            }
            return;
        } else {
            $obj->save();
            $this->logger()->log('Added new OrderHistory order status ' . OrderState::statusToString($status_id), 'STATUS_EVENT');
        }
        if ($update_order) {
            if ((int)$this->status !== (int)$status_id) {
                $old_status = $this->status;
                $this->status = $status_id;
                $this->save();
                $this->logger()->log('Status migrated from ' . OrderState::statusToString($old_status) . ' to ' . OrderState::statusToString($status_id), 'STATUS_EVENT');
                $this->getRepository()->orderStatusChanged($status_id, $old_status);
            }
        }
        $this->handleStatusUpdateEmail();
    }

    /**
     * @param $status_id
     * @param bool $update_order
     * @param string $reason
     */
    function setPaymentStatus($status_id, $update_order = true, $reason = null)
    {
        $type = OrderHistory::TYPE_PAYMENT;
        $historyRecordsCount = (int)OrderHistory::where(['order_id' => $this->id, 'status_id' => $status_id, 'type' => $type])->count();

        $obj = new OrderHistory();
        $obj->type = $type;
        $obj->order_id = $this->id;
        $obj->status_id = $status_id;
        $obj->setReason($reason);

        if ($this->payment_status == $status_id) {
            //if there are no records in the history, create an entry anyway
            if ($historyRecordsCount === 0) {
                $obj->save();
                $this->logger()->log('Added new OrderHistory payment status ' . PaymentState::statusToString($status_id), 'PAYMENT_STATUS_EVENT');
            }
            return;
        } else {
            $obj->save();
            $this->logger()->log('Added new OrderHistory payment status ' . PaymentState::statusToString($status_id), 'PAYMENT_STATUS_EVENT');
        }
        if ($update_order) {
            if ($this->payment_status != $status_id) {
                $old_status = $this->payment_status;
                $this->payment_status = $status_id;
                $this->save();
                $this->logger()->log('Status migrated from ' . PaymentState::statusToString($old_status) . ' to ' . PaymentState::statusToString($status_id), 'PAYMENT_STATUS_EVENT');
                $this->getRepository()->orderPaymentStatusChanged($status_id, $old_status);
            }
        }
        //if the order is a master, change the payment status to children
        if($this->isMaster()){
            try{
                $childrens = $this->getChildren();
                foreach($childrens as $children){
                    $children->setPaymentStatus($status_id, $update_order, $reason);
                }
            }catch (Exception $e){
                audit_exception($e, __METHOD__);
            }
        }
    }

    /**
     * Validate a status sendable
     *
     * @param $module_name
     * @return bool
     */
    function validateStateModule($module_name){
        if(is_null($module_name))
            return true;

        $bool = true;

        switch ($module_name){
            case 'online_carrier':
                $bool = false;
                $carrier = $this->getCarrier();
                if($carrier and $carrier->isType(Carrier::TYPE_NATIONAL_DELIVERY))
                    $bool = true;
                break;
        }

        return $bool;
    }

    /**
     * @param bool $test_only
     * @return bool
     */
    function handleStatusUpdateEmail($test_only = false)
    {
        $status = $this->getStatus();
        if ($status->id == 1 OR $status->send_email == 0) {
            return false;
        }
        if ($status->send_email == 1 and $this->validateStateModule($status->module_name)){
            if(true === $test_only)
                return true;

            //audit('Sending email', __METHOD__);
            $this->sendEmail('update_status');
        }else{
            //audit('Status email NOT sent', __METHOD__);
        }
        return false;
    }

    /**
     *
     */
    function handlePaymentStatusUpdateEmail()
    {
        $payment_status = $this->getPaymentStatus();
        if ($payment_status->module_name == 'refund') {
            $this->sendEmail('payment_status');
        }
    }


    /**
     * @param $status
     * @param null $tracking_by
     * @return bool
     */
    function updateStatus($status, $tracking_by = null)
    {
        if ($this->status == $status) {
            return false;
        }
        //audit(compact('status', 'tracking_by'), __METHOD__);
        $this->setStatus($status, true, 'Aggiornamento manuale', OrderTracking::byUser($tracking_by));
        return true;
    }

    /**
     * @param $status
     * @return bool
     */
    function updatePaymentStatus($status)
    {
        if ($this->payment_status == $status) {
            return false;
        }
        $old_status = $this->payment_status;
        $this->setPaymentStatus($status, true, 'Aggiornamento manuale');
        //$this->getRepository()->orderPaymentStatusChanged($status, $old_status);
        return true;
    }

    /**
     * @param string $type
     * @param null $status_id
     *
     * @return Builder
     */
    function historyStatesBuilder($type = OrderHistory::TYPE_ORDER, $status_id = null)
    {
        $builder = OrderHistory::where('order_id', $this->id)->where('type', $type);
        if ($status_id) {
            if (is_array($status_id)) {
                $builder->whereIn('status_id', $status_id);
            } else {
                $builder->where('status_id', $status_id);
            }
        }
        return $builder;
    }


    /**
     * Return all history states
     *
     * @param string $type
     * @param null $status_id
     *
     * @return Collection
     */
    function historyStates($type = OrderHistory::TYPE_ORDER, $status_id = null)
    {
        return $this->historyStatesBuilder($type, $status_id)->get();
    }

    /**
     * @param string $type
     * @param null $status_id
     * @return Collection
     */
    function getHistoryStates($type = OrderHistory::TYPE_ORDER, $status_id = null)
    {
        return $this->historyStates($type, $status_id);
    }


    /**
     * @param $state
     * @return bool
     */
    function hasOwnState($state)
    {
        if (!is_array($state))
            $state = [$state];

        return in_array($this->status, $state);
    }

    /**
     * @param $state
     * @return bool
     */
    function hasHistoryState($state)
    {
        $states = $this->historyStates(OrderHistory::TYPE_ORDER, $state);
        return !empty($states);
    }

    /**
     * @param $state
     * @return bool
     */
    function hasOverallState($state)
    {
        return ($this->hasOwnState($state) or $this->hasHistoryState($state));
    }


    /**
     * @param $state
     * @return bool
     */
    function hasOwnPaymentState($state)
    {
        if (!is_array($state))
            $state = [$state];

        return in_array($this->payment_status, $state);
    }

    /**
     * @param $state
     * @return bool
     */
    function hasHistoryPaymentState($state)
    {
        $states = $this->historyStates(OrderHistory::TYPE_PAYMENT, $state);
        return !empty($states);
    }

    /**
     * @param $state
     * @return bool
     */
    function hasOverallPaymentState($state)
    {
        return ($this->hasOwnPaymentState($state) or $this->hasHistoryPaymentState($state));
    }


    /**
     * @param $status
     * @return bool
     */
    public function hasCurrentStatus($status)
    {
        if (!is_array($status)) {
            $status = [$status];
        }
        return in_array($this->status, $status);
    }

    /**
     * @param $status
     * @return bool
     */
    public function hasCurrentPaymentStatus($status)
    {
        if (!is_array($status)) {
            $status = [$status];
        }
        return in_array($this->payment_status, $status);
    }

    /**
     * @param $status
     * @return bool
     */
    public function everHadStatus($status)
    {
        if ($this->hasCurrentStatus($status) === true)
            return true;

        return count($this->historyStates(OrderHistory::TYPE_ORDER, $status)) > 0;
    }

    /**
     * @param $status
     * @return bool
     */
    public function everHadPaymentStatus($status)
    {
        if ($this->hasCurrentPaymentStatus($status) === true)
            return true;

        return count($this->historyStates(OrderHistory::TYPE_PAYMENT, $status)) > 0;
    }

    /**
     * @param string $type
     * @param null $status_id
     * @return null|OrderHistory
     */
    public function getLastHistoryStatus($type = OrderHistory::TYPE_ORDER, $status_id = null)
    {
        return $this->historyStatesBuilder($type, $status_id)
            ->orderBy('created_at', 'desc')
            ->take(1)
            ->first();
    }

    /**
     * @param Builder $builder
     * @param $status
     * @throws Exception
     */
    public function scopeWithStatus(Builder $builder, $status)
    {
        if (!is_array($status))
            $status = $status > 0 ? [$status] : [];

        if (empty($status))
            throw new Exception("Please provide at least a status");

        $builder->whereIn('status', $status);
    }

    /**
     * @param Builder $builder
     * @param $status
     * @throws Exception
     */
    public function scopeWithPaymentStatus(Builder $builder, $status)
    {
        if (!is_array($status))
            $status = $status > 0 ? [$status] : [];

        if (empty($status))
            throw new Exception("Please provide at least a status");

        $builder->whereIn('payment_status', $status);
    }

    /**
     * @param Builder $builder
     * @param $status
     * @throws Exception
     */
    public function scopeWithoutStatus(Builder $builder, $status)
    {
        if (!is_array($status))
            $status = $status > 0 ? [$status] : [];

        if (empty($status))
            throw new Exception("Please provide at least a status");

        $builder->whereNotIn('status', $status);
    }

    /**
     * @param Builder $builder
     * @param $status
     * @throws Exception
     */
    public function scopeWithoutPaymentStatus(Builder $builder, $status)
    {
        if (!is_array($status))
            $status = $status > 0 ? [$status] : [];

        if (empty($status))
            throw new Exception("Please provide at least a status");

        $builder->whereNotIn('payment_status', $status);
    }


    /**
     * @param Builder $query
     * @param array|string $last_update
     */
    private function resolveLastUpdateQuery(Builder &$query, $last_update)
    {
        if ($last_update) {
            if (is_array($last_update)) {
                $query->whereBetween(DB::raw('DATE(updated_at)'), $last_update);
            } else {
                $query->whereDate('updated_at', '=', $last_update);
            }
        }
    }

    /**
     * @param Builder $builder
     * @param $status
     * @throws Exception
     */
    public function scopeWithHistoryStates(Builder $builder, $status, $last_update = null)
    {
        if (!is_array($status))
            $status = $status > 0 ? [$status] : [];

        if (empty($status))
            throw new Exception("Please provide at least a status");

        $builder->whereHas('histories_states', function ($query) use ($status, $last_update) {
            $query->whereIn('status_id', $status);
            $this->resolveLastUpdateQuery($query, $last_update);
        });
    }


    /**
     * @param Builder $builder
     * @param $status
     * @throws Exception
     */
    public function scopeWithHistoryPaymentStates(Builder $builder, $status, $last_update = null)
    {
        if (!is_array($status))
            $status = $status > 0 ? [$status] : [];

        if (empty($status))
            throw new Exception("Please provide at least a status");

        $builder->whereHas('histories_payment_states', function ($query) use ($status, $last_update) {
            $query->whereIn('status_id', $status);
            $this->resolveLastUpdateQuery($query, $last_update);
        });
    }


    /**
     * @param Builder $builder
     * @param $status
     * @throws Exception
     */
    public function scopeWithoutHistoryStates(Builder $builder, $status, $last_update = null)
    {
        if (!is_array($status))
            $status = $status > 0 ? [$status] : [];

        if (empty($status))
            throw new Exception("Please provide at least a status");

        $builder->whereDoesntHave('histories_states', function ($query) use ($status, $last_update) {
            $query->whereIn('status_id', $status);
            $this->resolveLastUpdateQuery($query, $last_update);
        });
    }


    /**
     * @param Builder $builder
     * @param $status
     * @throws Exception
     */
    public function scopeWithoutHistoryPaymentStates(Builder $builder, $status, $last_update = null)
    {
        if (!is_array($status))
            $status = $status > 0 ? [$status] : [];

        if (empty($status))
            throw new Exception("Please provide at least a status");

        $builder->whereDoesntHave('histories_payment_states', function ($query) use ($status, $last_update) {
            $query->whereIn('status_id', $status);
            $this->resolveLastUpdateQuery($query, $last_update);
        });
    }


    /**
     * @param $status_id
     * @param null $reason
     * @param null $tracking_by
     */
    public function setSoftStatus($status_id, $reason = null, $tracking_by = null)
    {
        try{
            $model = OrderTracking::fromOrderTracking($this, $status_id, $tracking_by);
            $model->feedback = $reason;
            $model->save();
        }catch (Exception $e){
            audit_exception($e, __METHOD__);
        }
    }

}