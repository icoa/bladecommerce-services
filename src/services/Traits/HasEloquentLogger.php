<?php

namespace services\Traits;

use services\Morellato\Loggers\EloquentLogger;

trait HasEloquentLogger
{
    /**
     * @var EloquentLogger
     */
    static protected $eloquentLogger;

    /**
     *
     */
    public static function bootHasEloquentLogger()
    {
        self::$eloquentLogger = new EloquentLogger();
    }

    /**
     * @return EloquentLogger
     */
    protected function getEloquentLogger(){
        return self::$eloquentLogger->setModel($this);
    }

    /**
     * @return EloquentLogger
     */
    public function logger()
    {
        return $this->getEloquentLogger();
    }
}