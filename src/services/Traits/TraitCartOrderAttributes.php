<?php

namespace services\Traits;

use Carrier;
use Payment;
use Campaign;
use CartRule;
use MorellatoShop;
use Order;

/**
 * Trait TraitCartOrderAttributes
 * @package services\Traits
 * @mixin Order
 */
trait TraitCartOrderAttributes
{
    protected $_carrier;
    protected $_payment;
    protected $_campaign;
    protected $_cart_rule;
    protected $_delivery_store;

    /**
     * @return Carrier|null
     */
    function getCarrierEntity()
    {
        if (isset($this->_carrier))
            return $this->_carrier;

        $this->_carrier = Carrier::getObj($this->carrier_id);
        return $this->_carrier;
    }


    /**
     * @return string|null
     */
    function getCarrierEntityName()
    {
        return $this->getCarrierEntity() ? $this->getCarrierEntity()->name : null;
    }


    /**
     * @return int|null
     */
    function getCarrierEntityId()
    {
        return $this->getCarrierEntity() ? $this->getCarrierEntity()->id : null;
    }


    /**
     * @return Payment|null
     */
    function getPaymentEntity()
    {
        if (isset($this->_payment))
            return $this->_payment;

        $this->_payment = $this->payment_id > 0 ? Payment::getObj($this->payment_id) : null;
        return $this->_payment;
    }


    /**
     * @return string|null
     */
    function getPaymentEntityName()
    {
        return $this->getPaymentEntity() ? $this->getPaymentEntity()->name : null;
    }


    /**
     * @return string|null
     */
    function getPaymentEntityModule()
    {
        return $this->getPaymentEntity() ? $this->getPaymentEntity()->module : null;
    }


    /**
     * @return int|null
     */
    function getPaymentEntityId()
    {
        return $this->getPaymentEntity() ? $this->getPaymentEntity()->id : null;
    }


    /**
     * @return Campaign|null
     */
    function getCampaignEntity()
    {
        if ((int)$this->campaign_id === 0)
            return null;

        if (isset($this->_campaign))
            return $this->_campaign;

        if ($this->campaign)
            return $this->campaign;

        $this->_campaign = Campaign::getObj($this->campaign_id);
        return $this->_campaign;
    }


    /**
     * @return string|null
     */
    function getCampaignEntityName()
    {
        return $this->getCampaignEntity() ? $this->getCampaignEntity()->name : null;
    }


    /**
     * @return int|null
     */
    function getCampaignEntityId()
    {
        return $this->getCampaignEntity() ? $this->getCampaignEntity()->id : null;
    }


    /**
     * @return string|null
     */
    function getCouponCode()
    {
        return $this->coupon_code;
    }


    /**
     * @return CartRule|null
     */
    function getCartRuleEntity()
    {
        if ((int)$this->cart_rule_id === 0)
            return null;

        if (isset($this->cart_rule)) {
            $this->cart_rule->hydrateWithTranslation();
            return $this->cart_rule;
        }

        if (isset($this->_cart_rule))
            return $this->_cart_rule;

        $this->_cart_rule = CartRule::getObj($this->cart_rule_id);
        return $this->_cart_rule;
    }


    /**
     * @return string|null
     */
    function getCartRuleEntityName()
    {
        return $this->getCartRuleEntity() ? $this->getCartRuleEntity()->name : null;
    }


    /**
     * @return int|null
     */
    function getCartRuleEntityId()
    {
        return $this->getCartRuleEntity() ? $this->getCartRuleEntity()->id : null;
    }

    /**
     * @return MorellatoShop|null
     */
    function getDeliveryStoreEntity()
    {
        if (isset($this->_delivery_store))
            return $this->_delivery_store;

        $this->_delivery_store = MorellatoShop::getObj($this->delivery_store_id);
        return $this->_delivery_store;
    }


    /**
     * @return string|null
     */
    function getDeliveryStoreEntityName()
    {
        return $this->getDeliveryStoreEntity() ? $this->getDeliveryStoreEntity()->name : null;
    }


    /**
     * @return string|null
     */
    function getDeliveryStoreEntityCode()
    {
        return $this->getDeliveryStoreEntity() ? $this->getDeliveryStoreEntity()->cd_neg : null;
    }


    /**
     * @return int|null
     */
    function getDeliveryStoreEntityId()
    {
        return $this->getDeliveryStoreEntity() ? $this->getDeliveryStoreEntity()->id : null;
    }
}