<?php

namespace services\Traits;

use Illuminate\Database\Eloquent\Builder;
use DB;
use Exception;
use OrderState;
use PaymentState;
use Order;

/**
 * Trait TraitOrderRefundManager
 * @package services\Traits
 */
trait TraitOrderRefundManager
{

    protected function getRefundableStates()
    {
        return [
            OrderState::STATUS_REFUNDED_PARTIALLY,
            OrderState::STATUS_REFUNDED,
            OrderState::STATUS_CANCELED,
            OrderState::STATUS_CANCELED_AFFILIATE,
            OrderState::STATUS_CANCELED_SAP,
        ];
    }

    /**
     * @param Builder $builder
     * @param null|array|string $date_range
     */
    function scopeRefundableOrSoftCancelled(Builder $builder, $date_range = null)
    {
        $this->scopeWithHistoryStates($builder, $this->getRefundableStates(), $date_range);
    }

}