<?php


namespace services\Traits;

use Illuminate\Database\Eloquent\Builder;
use Order;
use OrderDetail;
use Address;
use MorellatoShop;
use Exception;

/**
 * Trait TraitOrderAvailabilityManager
 * @package services\Traits
 * @mixin Order
 */
trait TraitOrderAvailabilityManager
{
    /**
     * @return bool
     */
    function isMaster()
    {
        return $this->availability_mode == 'master';
    }

    /**
     * @return bool
     */
    function isChild()
    {
        return $this->availability_mode != 'master' and $this->parent_id > 0;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    function getAllWarehouses()
    {
        return OrderDetail::where('order_id', $this->id)
            ->where('availability_mode', '!=', self::MODE_STAR)
            ->select(\DB::raw('DISTINCT(warehouse)'), 'availability_shop_id')
            ->get();
    }

    /**
     * @return Address|null
     */
    function getShippingAddress()
    {
        if (isset($this->shipping_address)) {
            $this->shipping_address->expand();
            return $this->shipping_address;
        }
        $address = Address::find($this->shipping_address_id);
        if ($address) {
            $address->expand();
        }
        return $address;
    }

    /**
     * @return Address|null
     */
    function getBillingAddress()
    {
        if ($this->billing_address_id == 0) {
            return $this->getShippingAddress();
        }
        if (isset($this->billing_address)) {
            $this->billing_address->expand();
            return $this->billing_address;
        }
        $address = Address::find($this->billing_address_id);
        if ($address) {
            $address->expand();
        }
        return $address;
    }

    /**
     * @return string
     */
    function getTrackingUrl()
    {
        //first, if the complete tracking_url is provided, return that url
        if (strlen(trim($this->tracking_url)) > 0) {
            return $this->tracking_url;
        }

        $carrier_tracking_url = $this->getGlsTrackingUrl();
        if ($carrier_tracking_url) {
            return $carrier_tracking_url;
        }

        $carrier = $this->getCarrier();
        if ($carrier === null) {
            return false;
        }
        if ($carrier->url == null or trim($carrier->url) == '') {
            return false;
        }
        if ($this->shipping_number == null or trim($this->shipping_number) == '') {
            return false;
        }
        return str_replace('@', $this->shipping_number, $carrier->url);
    }

    /**
     * @return MorellatoShop|null
     */
    function getDeliveryStore()
    {
        return $this->hasDeliveryStore() ? MorellatoShop::getObj($this->delivery_store_id) : null;
    }

    /**
     * @param Builder $query
     * @param null $store
     * @return Builder
     */
    function scopeWithDeliveryStore(Builder $query, $store = null)
    {
        if (null === $store) {
            return $query->where('delivery_store_id', '>', 0);
        }
        if ($store instanceof MorellatoShop)
            $store = $store->id;

        if (is_numeric($store))
            return $query->where('delivery_store_id', $store);

        if (is_array($store))
            return $query->whereIn('delivery_store_id', $store);

        return $query;
    }

    /**
     * @param Builder $query
     * @param null $store
     * @return Builder
     */
    function scopeWithoutDeliveryStore(Builder $query, $store = null)
    {
        if (null === $store) {
            return $query->where(function ($subquery) {
                $subquery->where('delivery_store_id', '=', '')->orWhereNull('delivery_store_id');
            });
        }
        if ($store instanceof MorellatoShop)
            $store = $store->id;

        if (is_numeric($store))
            return $query->where('delivery_store_id', '!=', $store);

        if (is_array($store))
            return $query->whereNotIn('delivery_store_id', $store);

        return $query;
    }

    /**
     * @param Builder $query
     * @param null $store
     * @return Builder
     */
    function scopeWithAvailabilityStore(Builder $query, $store = null)
    {
        $this->scopeWithAvailabilityModes($query, [Order::MODE_SHOP, Order::MODE_MIXED]);

        if (null === $store) {
            return $query->where(function ($subquery) {
                $subquery->where('availability_shop_id', '<>', '')->orWhereNotNull('availability_shop_id');
            });
        }
        if ($store instanceof MorellatoShop)
            $store = $store->code;

        if (is_string($store))
            return $query->where('availability_shop_id', $store);

        if (is_array($store))
            return $query->whereIn('availability_shop_id', $store);

        return $query;
    }

    /**
     * @param Builder $query
     * @param null $store
     * @return Builder
     */
    function scopeWithoutAvailabilityStore(Builder $query, $store = null)
    {
        $this->scopeWithoutAvailabilityModes($query, [Order::MODE_SHOP, Order::MODE_MIXED]);

        if (null === $store) {
            return $query->where(function ($subquery) {
                $subquery->where('availability_shop_id', '=', '')->orWhereNull('availability_shop_id');
            });
        }
        if ($store instanceof MorellatoShop)
            $store = $store->code;

        if (is_string($store))
            return $query->where('availability_shop_id', '!=', $store);

        if (is_array($store))
            return $query->whereNotIn('availability_shop_id', $store);

        return $query;
    }

    /**
     * @return MorellatoShop|null
     */
    function getAvailabilityShop()
    {
        return $this->hasShop() ? $this->getMorellatoShop() : null;
    }

    /**
     * @return bool
     */
    function hasDeliveryStore()
    {
        return $this->delivery_store_id > 0;
    }

    /**
     * @return bool
     */
    function hasStorePayment()
    {
        return $this->module == 'pick' or $this->module == 'premature' or $this->hasStorePostponePayment();
    }

    /**
     * @return bool
     */
    function hasStorePostponePayment()
    {
        return $this->module == 'postpone';
    }

    /**
     * @return mixed
     */
    function hasShopAvailability()
    {
        return $this->hasShop();
    }

    /**
     * @return bool
     */
    function hasDifferentStoreForWithdrawal()
    {
        if ($this->hasShopAvailability()) {
            if ($this->hasDeliveryStore() and $this->availability_shop_id != null) {
                $store = $this->getDeliveryStore();
                if ($store and $store->cd_neg != $this->availability_shop_id) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return bool
     */
    function hasSameStoreForWithdrawal()
    {
        if ($this->hasShopAvailability()) {
            return !$this->hasDifferentStoreForWithdrawal();
        }
        return false;
    }

    /**
     * @return mixed
     */
    function getSalesOrg()
    {
        return \Config::get('soap.order.defaults.orderHeaderIn.SALES_ORG');
    }

    /**
     * @return mixed
     */
    function getMode()
    {
        return $this->availability_mode;
    }

    /**
     * @return bool
     */
    function isB2B()
    {
        return $this->module == 'premature' or $this->module == 'postpone';
    }

    /**
     * @param Builder $builder
     * @param $modes
     * @throws Exception
     */
    public function scopeWithAvailabilityModes(Builder $builder, $modes)
    {
        if (!is_array($modes))
            $modes = [$modes];

        if (empty($modes))
            throw new Exception("Please provide at least an availability mode");

        $builder->whereIn('availability_mode', $modes);
    }


    /**
     * @param Builder $builder
     * @param $modes
     * @throws Exception
     */
    public function scopeWithoutAvailabilityModes(Builder $builder, $modes)
    {
        if (!is_array($modes))
            $modes = [$modes];

        if (empty($modes))
            throw new Exception("Please provide at least an availability mode");

        $builder->whereNotIn('availability_mode', $modes);
    }


    /**
     * @param Builder $builder
     * @throws Exception
     */
    public function scopeAvailable(Builder $builder)
    {
        $invalid_modes = [Order::MODE_MASTER, Order::MODE_OFFLINE, Order::MODE_LOCAL];
        $this->scopeWithoutAvailabilityModes($builder, $invalid_modes);
    }


    /**
     * @param Builder $builder
     * @throws Exception
     */
    public function scopeWithBasicAvailability(Builder $builder)
    {
        $invalid_modes = [Order::MODE_MASTER, Order::MODE_OFFLINE];
        $this->scopeWithoutAvailabilityModes($builder, $invalid_modes);
    }
}