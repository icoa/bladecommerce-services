<?php


namespace services\Traits;

use Illuminate\Database\Eloquent\Builder;
use Payment;
use Customer;
use Carrier;
use Campaign;
use Message;
use Currency;
use Cart;
use Exception;
use DB;
use Illuminate\Support\Collection;
use Order;

/**
 * Trait TraitOrderCartRelationManager
 * @package services\Traits
 * @mixin Order
 * @mixin Cart
 */
trait TraitOrderCartRelationManager
{

    /**
     * @return string|null
     */
    public function carrierName()
    {
        $record = $this->getCarrier();
        return ($record) ? $record->name : null;
    }

    /**
     * @return string|null
     */
    public function paymentName()
    {
        $record = $this->getPayment();
        return ($record) ? $record->name : null;
    }

    /**
     * @return string|null
     */
    public function customerName()
    {
        $record = $this->getCustomer();
        return ($record) ? $record->name : null;
    }

    /**
     * @return Currency|null
     */
    public function getCurrency()
    {
        return Currency::getObj($this->currency_id);
    }

    /**
     * @param $query
     * @param null $customer_id
     * @return mixed
     */
    public function scopeForCustomer($query, $customer_id = null)
    {
        if ($customer_id === null) {
            $customer_id = \FrontUser::getId();
        }

        if (feats()->receipts()) {
            $this->scopeWithoutStarlings($query);
        }

        return $query->where('customer_id', $customer_id)->where('availability_mode', '!=', 'master');
    }

    /**
     * @param $payments
     * @return bool
     */
    public function hasPayment($payments)
    {
        if (!is_array($payments))
            $payments = [$payments];

        return in_array($this->payment_id, $payments);
    }

    /**
     * @param $carriers
     * @return bool
     */
    public function hasCarrier($carriers)
    {
        if (!is_array($carriers))
            $carriers = [$carriers];

        return in_array($this->carrier_id, $carriers);
    }

    /**
     * @param Builder $builder
     * @param $payment
     * @throws Exception
     */
    public function scopeWithPayment(Builder $builder, $payment)
    {
        if ($payment instanceof Payment)
            $payment = [$payment->id];

        if (!is_array($payment))
            $payment = $payment > 0 ? [$payment] : [];

        if (empty($payment))
            throw new Exception('Please provide at least a payment method ID');

        $builder->whereIn('payment_id', $payment);
    }

    /**
     * @param Builder $builder
     * @param $payment
     * @throws Exception
     */
    public function scopeWithoutPayment(Builder $builder, $payment)
    {
        if ($payment instanceof Payment)
            $payment = [$payment->id];

        if (!is_array($payment))
            $payment = $payment > 0 ? [$payment] : [];

        if (empty($payment))
            throw new Exception('Please provide at least a payment method ID');

        $builder->whereNotIn('payment_id', $payment);
    }

    /**
     * @param Builder $builder
     * @param $carrier
     * @throws Exception
     */
    public function scopeWithCarrier(Builder $builder, $carrier)
    {
        if ($carrier instanceof Carrier)
            $carrier = [$carrier->id];

        if (!is_array($carrier))
            $carrier = $carrier > 0 ? [$carrier] : [];

        if (empty($carrier))
            throw new Exception('Please provide at least a carrier ID');

        $builder->whereIn('carrier_id', $carrier);
    }

    /**
     * @param Builder $builder
     * @param $carrier
     * @throws Exception
     */
    public function scopeWithoutCarrier(Builder $builder, $carrier)
    {
        if ($carrier instanceof Carrier)
            $carrier = [$carrier->id];

        if (!is_array($carrier))
            $carrier = $carrier > 0 ? [$carrier] : [];

        if (empty($carrier))
            throw new Exception('Please provide at least a carrier ID');

        $builder->whereNotIn('carrier_id', $carrier);
    }

    /**
     * @param Builder $builder
     * @param $customer
     * @throws Exception
     */
    public function scopeWithCustomer(Builder $builder, $customer)
    {
        if ($customer instanceof Customer)
            $customer = [$customer->id];

        if (!is_array($customer))
            $customer = $customer > 0 ? [$customer] : [];

        if (empty($customer))
            throw new Exception('Please provide at least a customer ID');

        $builder->whereIn('customer_id', $customer);
    }

    /**
     * @param Builder $builder
     * @param $customer
     * @throws Exception
     */
    public function scopeWithoutCustomer(Builder $builder, $customer)
    {
        if ($customer instanceof Customer)
            $customer = [$customer->id];

        if (!is_array($customer))
            $customer = $customer > 0 ? [$customer] : [];

        if (empty($customer))
            throw new Exception('Please provide at least a customer ID');

        $builder->whereNotIn('customer_id', $customer);
    }

}