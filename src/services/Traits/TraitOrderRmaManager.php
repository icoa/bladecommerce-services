<?php

namespace services\Traits;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Builder;
use Rma;
use DB;
use OrderDetail;
use FrontTpl;
use Exception;
use Order;

/**
 * Trait TraitOrderRmaManager
 * @package services\Traits
 * @mixin Order
 */
trait TraitOrderRmaManager
{

    /**
     * @return HasMany
     */
    function rmas()
    {
        return $this->hasMany(Rma::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|Rma[]
     */
    function getRma()
    {
        $rows = Rma::where('order_id', $this->id)->get();
        foreach ($rows as $row) {
            $row->expand();
        }
        return $rows;
    }

    /**
     * @return bool
     */
    function hasRma(){
        $count = Rma::where('order_id', $this->id)->count();
        return $count > 0;
    }


    /**
     * @param int $product_id
     * @return string
     */
    function getRmaProductsTable($product_id = 0)
    {
        $products = $this->getProducts();
        $available = $this->getProductsAvailableForRma();

        foreach ($products as $index => $p) {
            if (!in_array($p->product_id, $available)) {
                unset($products[$index]);
            }
        }

        $theme = FrontTpl::getTheme();

        return $theme->partial('order.rmatable', ['rows' => $products, 'product_id' => $product_id]);
    }


    /**
     * @return array
     */
    function getProductsAvailableForRma()
    {
        $query = "SELECT product_id FROM rma_details WHERE rma_id IN (SELECT id FROM rmas WHERE order_id=?)";
        $rows = DB::select($query, [$this->id]);
        $taken = [];
        foreach ($rows as $row) {
            $taken[] = $row->product_id;
        }
        $products = OrderDetail::where("order_id", $this->id)->whereNotIn('product_id', $taken)->lists('product_id');
        return $products;
    }

    /**
     * @param Builder $builder
     */
    function scopeWithRma(Builder $builder)
    {
        $builder->has('rmas');
    }

    /**
     * @param Builder $builder
     */
    function scopeWithoutRma(Builder $builder)
    {
        $builder->doesntHave('rmas');
    }

    /**
     * @param Builder $builder
     * @param $status
     * @throws Exception
     */
    public function scopeWithRmaStates(Builder $builder, $status)
    {
        if (!is_array($status))
            $status = $status > 0 ? [$status] : [];

        if (empty($status))
            throw new Exception("Please provide at least a status");

        $builder->whereHas('rmas', function ($query) use ($status) {
            $query->whereIn('status', $status);
        });
    }


    /**
     * @param Builder $builder
     * @param $status
     * @throws Exception
     */
    public function scopeWithoutRmaStates(Builder $builder, $status)
    {
        if (!is_array($status))
            $status = $status > 0 ? [$status] : [];

        if (empty($status))
            throw new Exception("Please provide at least a status");

        $builder->whereDoesntHave('rmas', function ($query) use ($status) {
            $query->whereIn('status', $status);
        });
    }
}