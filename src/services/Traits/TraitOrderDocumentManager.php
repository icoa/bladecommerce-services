<?php


namespace services\Traits;

use Illuminate\Database\Eloquent\Builder;
use Site;
use File;
use OrderManager;
use Order;
use Format;
use Illuminate\Support\Collection;
use Exception;
use stdClass;

/**
 * Trait TraitOrderDocumentManager
 * @package services\Traits
 * @mixin Order
 */
trait TraitOrderDocumentManager
{
    /**
     * @return bool
     */
    function hasInvoice()
    {
        $invoice = $this->getInvoice();
        if ($invoice) {
            return $invoice['hasFile'];
        }
        return false;
    }

    /**
     * @return bool
     */
    function hasDelivery()
    {
        $invoice = $this->getDelivery();
        if ($invoice) {
            return $invoice['hasFile'];
        }
        return false;
    }

    /**
     * @return array
     */
    function getDocumentData()
    {
        $isStarling = $this->isStarling();

        $shop = \Core::getShopInfo();

        $data = [
            'shop_header' => $shop['name'],
            'shop_address' => $shop['full_address'],
            'shop_phone' => $shop['full_phone'],
            'shop_details' => $shop['details'],
            'shop_email' => "<a href='mailto:{$shop['email']}'>" . $shop['email'] . "</a>",
        ];

        $billing = $this->getBillingAddress();
        $data += [
            'billing_header' => $billing->fullname(),
            'billing_address' => $billing->fullAddress(),
            'billing_legal' => $billing->legalInfo(),
        ];

        $shipping = $this->getShippingAddress();
        $data += [
            'shipping_header' => $shipping->fullname(),
            'shipping_address' => $shipping->fullAddress(),
            'shipping_legal' => $shipping->legalInfo(),
        ];

        $data['billing_different'] = ($billing->id === $shipping->id) ? false : true;

        $products_data = [];

        $currency_id = $this->currency_id;
        $products = $this->getProducts();

        $useRealSum = false;

        //if the order has "points" or "vouchers" than the totals must be setted to the "original" amount, without discounts
        $needsOriginalAmounts = $this->hasFidelityPoints() or $this->hasVouchers();

        foreach ($products as $p) {

            $p->total_price_tax_incl = round($p->total_price_tax_incl, 2);
            $p->total_price_tax_excl = round($p->total_price_tax_excl, 2);

            if ($isStarling) {
                $p->product_quantity *= -1;
                $p->product_item_price *= -1;
                $p->total_price_tax_incl *= -1;
            }

            //if there is at least a virtual product, then use sum in real time
            if ($p->product and $p->product->isVirtual()) {
                $useRealSum = true;
            }

            $o = new stdClass();
            $o->name = $p->product_name;
            $o->qty = $p->product_quantity;
            $o->unit_price = Format::currency($p->product_item_price, true, $currency_id);
            $total = ($needsOriginalAmounts) ? $p->product_item_price * abs($p->product_quantity) : $p->total_price_tax_incl;
            $o->total = Format::currency($total, true, $currency_id);

            $subname = "";
            if ($p->product_ean13 != '') $subname .= "EAN: " . $p->product_ean13 . "<br>";
            if ($p->product_reference != '') $subname .= "RIF.: " . $p->product_reference;
            $o->subname = $subname;
            $products_data[] = $o;
        }
        $data['products'] = $products_data;
        $data['maxItems'] = 7;
        $data['collections'] = Collection::make($products_data)->chunk($data['maxItems']);

        $factor = $isStarling ? -1 : 1;

        $data += [
            'total_products' => Format::currency($this->total_products * $factor, true, $currency_id),
            'total_order' => Format::currency($this->total_order * $factor, true, $currency_id),
            'total_shipping' => Format::currency($this->total_shipping * $factor, true, $currency_id),
            'total_taxes' => Format::currency($this->total_taxes * $factor, true, $currency_id),
            'total_discounts' => Format::currency(-$this->total_discounts * $factor, true, $currency_id),
            'total_products_tax_excl' => Format::currency($this->total_products_tax_excl * $factor, true, $currency_id),
            'total_costs' => Format::currency(($this->total_wrapping + $this->total_payment) * $factor, true, $currency_id),
            'tax_rate' => Format::rateToPercent(\Core::getDefaultTaxRate()),
            'total_weight' => Format::weight($this->total_weight),
        ];

        if ($useRealSum) {
            $total_products = 0;
            $total_products_tax_excl = 0;
            $total_taxes = 0;
            foreach ($products as $p) {
                $total_products += $p->total_price_tax_incl;
                $total_products_tax_excl += $p->total_price_tax_excl;
                //$row_taxes = $p->tax_rate == 0 ? 0 : $p->total_price_tax_excl - $p->total_price_tax_incl;
                $row_taxes = $p->total_price_tax_incl - $p->total_price_tax_excl;
                //audit("PI: $p->total_price_tax_incl | PE: $p->total_price_tax_excl | RT: $row_taxes");
                $total_taxes += $row_taxes;
            }
            $total_taxes += $this->getExtraCostsTaxes();
            $data['total_products'] = Format::currency($total_products * $factor, true, $currency_id);
            $data['total_products_tax_excl'] = Format::currency($total_products_tax_excl * $factor, true, $currency_id);
            $data['total_taxes'] = Format::currency($total_taxes * $factor, true, $currency_id);
        }

        $data['logo'] = Site::root() . \Cfg::get('DEFAULT_LOGO');
        $data['url'] = Site::root() . "/media/pdf";
        $data['logo'] = $data['url'] . '/' . 'logo.png';
        $data['site'] = Site::root();
        $data['short_name'] = \Cfg::get('SHORTNAME');

        $data['taxes'] = $this->getTaxDetails();


        $data['payment'] = $this->payment;
        $carrier = $this->getCarrier();
        $data['carrier'] = $carrier->name;


        return $data;
    }


    /**
     * @return array
     */
    function getInvoiceData()
    {

        $invoice = $this->getInvoice();

        $data = $this->getDocumentData();

        $data += [
            'invoice_number' => $invoice['number'],
            'invoice_date' => Format::date($invoice['date']),
            'order_reference' => $this->reference,
            'order_number' => $this->reference,
            'notes' => $invoice['notes'],
        ];

        $data['filename'] = $invoice['filename'];
        $data['filepath'] = $invoice['filepath'];
        $data['title'] = trans('fee.pdf.title', [
            'invoice_number' => $data['invoice_number'],
            'invoice_date' => $data['invoice_date'],
            'billing_header' => $data['billing_header'],
        ]);

        return $data;

    }

    /**
     * @return array
     */
    function getDeliveryData()
    {

        $invoice = $this->getDelivery();

        $data = $this->getDocumentData();

        $data += [
            'invoice_number' => $invoice['number'],
            'invoice_date' => Format::date($invoice['date']),
            'order_reference' => $this->reference,
            'order_number' => $this->reference,
            'notes' => $invoice['notes'],
        ];

        $data['filename'] = $invoice['filename'];
        $data['filepath'] = $invoice['filepath'];
        $data['title'] = "Documento di trasporto N° " . $data['invoice_number'] . " del " . $data['invoice_date'] . " - " . $data['billing_header'];

        return $data;

    }

    /**
     * @return array
     */
    function getTaxDetails()
    {
        $rows = [];
        $products_details = [];

        $currency_id = $this->currency_id;

        //if the order has "points" or "vouchers" than the totals must be setted to the "original" amount, without discounts
        $needsOriginalAmounts = $this->hasFidelityPoints() or $this->hasVouchers();

        $products = $this->getProducts();
        foreach ($products as $p) {
            $ratio = Format::rateToPercent($p->tax_rate);
            if (!isset($products_details[$ratio])) {
                $products_details[$ratio] = ['tax_incl' => 0, 'tax_excl' => 0, 'tax_total' => 0];
            }
        }

        foreach ($products as $p) {
            $p->product_quantity = abs($p->product_quantity);
            $ratio = Format::rateToPercent($p->tax_rate);
            //$p->cart_price_tax_incl = round($p->cart_price_tax_incl, 2);
            //$p->cart_price_tax_excl = round($p->cart_price_tax_excl, 2);
            if ($needsOriginalAmounts) {
                $p->cart_price_tax_incl = $p->product_item_price;
                $p->cart_price_tax_excl = $p->product_item_price_tax_excl;
            }
            $products_details[$ratio]['tax_incl'] += $p->cart_price_tax_incl * $p->product_quantity;
            $products_details[$ratio]['tax_excl'] += $p->cart_price_tax_excl * $p->product_quantity;
            $products_details[$ratio]['tax_total'] += ($p->cart_price_tax_incl - $p->cart_price_tax_excl) * $p->product_quantity;
            $products_details[$ratio]['name'] = trans('fee.pdf.taxes.products');
            if ($p->product_combination_id === 'GF') {
                $products_details[$ratio]['name'] = trans('fee.pdf.taxes.giftCard');
            }
            /*audit_error($products_details[$ratio], "ORDER: $this->id");
            $debug = [
                'product_quantity' => $p->product_quantity,
                'tax_rate' => $p->tax_rate,
                'cart_price_tax_incl' => $p->cart_price_tax_incl,
                'cart_price_tax_excl' => $p->cart_price_tax_excl,
                'product_item_price' => $p->product_item_price,
                'product_item_price_tax_excl' => $p->product_item_price_tax_excl,
            ];
            audit_error($debug, "ORDER: $this->id");*/
        }

        $factor = $this->isStarling() ? -1 : 1;

        foreach ($products_details as $rate => $d) {
            $o = new stdClass();
            //$o->name = trans('fee.pdf.taxes.products');
            $o->name = $d['name'];
            $o->rate = $rate;
            $o->tax_incl = $d['tax_incl'];
            $o->tax_incl_formatted = Format::currency($o->tax_incl * $factor, true, $currency_id);
            $o->tax_excl = $d['tax_excl'];
            $o->tax_excl_formatted = Format::currency($o->tax_excl * $factor, true, $currency_id);
            $o->tax_total = $d['tax_total'];
            $o->tax_total_formatted = Format::currency($o->tax_total * $factor, true, $currency_id);

            $rows[] = $o;
        }

        if ($this->total_shipping > 0) {
            $carrier_tax_rate = $this->carrier_tax_rate;
            if ($carrier_tax_rate == 0) {
                $carrier_tax_rate = \Core::getDefaultTaxRate();
            }
            $o = new stdClass();
            $o->name = trans('fee.pdf.taxes.shipment');
            $o->rate = Format::rateToPercent($carrier_tax_rate);
            $o->tax_incl = $this->total_shipping_tax_incl;
            $o->tax_incl_formatted = Format::currency($o->tax_incl * $factor, true, $currency_id);
            $o->tax_excl = $this->total_shipping_tax_excl;
            $o->tax_excl_formatted = Format::currency($o->tax_excl * $factor, true, $currency_id);
            $o->tax_total = $this->total_shipping_tax_incl - $this->total_shipping_tax_excl;
            $o->tax_total_formatted = Format::currency($o->tax_total * $factor, true, $currency_id);

            $rows[] = $o;
        }

        if ($this->total_payment > 0) {
            $tax_rate = \Core::getDefaultTaxRate();
            $o = new stdClass();
            $o->name = trans('fee.pdf.taxes.payment');
            $o->rate = Format::rateToPercent($tax_rate);
            $o->tax_incl = $this->total_payment_tax_incl;
            $o->tax_incl_formatted = Format::currency($o->tax_incl * $factor, true, $currency_id);
            $o->tax_excl = $this->total_payment_tax_excl;
            $o->tax_excl_formatted = Format::currency($o->tax_excl * $factor, true, $currency_id);
            $o->tax_total = $this->total_payment_tax_incl - $this->total_payment_tax_excl;
            $o->tax_total_formatted = Format::currency($o->tax_total * $factor, true, $currency_id);

            $rows[] = $o;
        }

        if ($this->total_wrapping > 0) {
            $tax_rate = \Core::getDefaultTaxRate();
            $o = new stdClass();
            $o->name = trans('fee.pdf.taxes.wrapping');
            $o->rate = Format::rateToPercent($tax_rate);
            $o->tax_incl = $this->total_wrapping_tax_incl;
            $o->tax_incl_formatted = Format::currency($o->tax_incl * $factor, true, $currency_id);
            $o->tax_excl = $this->total_wrapping_tax_excl;
            $o->tax_excl_formatted = Format::currency($o->tax_excl * $factor, true, $currency_id);
            $o->tax_total = $this->total_wrapping_tax_incl - $this->total_wrapping_tax_excl;
            $o->tax_total_formatted = Format::currency($o->tax_total * $factor, true, $currency_id);

            $rows[] = $o;
        }

        if ($this->total_discounts > 0) {
            $tax_rate = \Core::getDefaultTaxRate();
            $o = new stdClass();
            $o->name = trans('fee.pdf.taxes.discount');
            $o->rate = Format::rateToPercent($tax_rate);
            $o->tax_incl = $this->total_discounts;
            $o->tax_incl_formatted = Format::currency(-$o->tax_incl * $factor, true, $currency_id);
            $o->tax_excl = $this->total_discounts_tax_excl;
            $o->tax_excl_formatted = Format::currency(-$o->tax_excl * $factor, true, $currency_id);
            $o->tax_total = $o->tax_incl - $o->tax_excl;
            $o->tax_total_formatted = Format::currency(-$o->tax_total * $factor, true, $currency_id);

            $rows[] = $o;
        }

        return $rows;

    }

    /**
     * @return array|null
     */
    function getInvoice()
    {
        if (feats()->receipts())
            return $this->getReceiptParams();

        $invoice_id = $this->invoice_number;
        if ($invoice_id <= 0) {
            return null;
        }
        $number = OrderManager::generateReference($invoice_id, 'invoice');
        $data = [
            'type' => 'invoice',
            'id' => $this->invoice_number,
            'number' => $number,
            'code' => $number,
            'reference' => $number,
            'date' => Format::date($this->invoice_date),
            'notes' => $this->invoice_notes
        ];

        $filename = 'INVOICE_' . $this->id;
        $filepath = storage_path("files/invoices/$filename.pdf");
        $hasFile = File::exists($filepath);
        $url = Site::root() . "/files/invoice/{$this->id}?secure_key=" . $this->secure_key;

        $data['filename'] = $filename;
        $data['filepath'] = $filepath;
        $data['hasFile'] = $hasFile;
        $data['url'] = $url;
        return $data;
    }

    /**
     * @return array|null
     */
    function getDelivery()
    {
        $invoice_id = $this->delivery_number;
        if ($invoice_id <= 0) {
            return null;
        }
        $number = OrderManager::generateReference($invoice_id, 'delivery');
        $data = [
            'type' => 'delivery',
            'id' => $this->delivery_number,
            'number' => $number,
            'code' => $number,
            'reference' => $number,
            'date' => Format::date($this->delivery_date),
            'notes' => $this->delivery_notes
        ];
        $filename = 'DELIVERY_' . $number;
        $filepath = storage_path("files/delivery/$filename.pdf");
        $hasFile = File::exists($filepath);
        $url = Site::root() . "/files/delivery/{$data['id']}?secure_key=" . $this->secure_key;

        $data['filename'] = $filename;
        $data['filepath'] = $filepath;
        $data['hasFile'] = $hasFile;
        $data['url'] = $url;
        return $data;
    }
}