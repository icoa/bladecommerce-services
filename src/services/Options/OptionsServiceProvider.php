<?php

namespace services\Options;

use Illuminate\Support\ServiceProvider;

class OptionsServiceProvider extends ServiceProvider
{
    protected $options;

    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        $blade_custom_helpers = [
            'services' => 'Options/helpers.php',
        ];
        foreach ($blade_custom_helpers as $folder => $blade_custom_helper) {
            require_once blade_vendor_path($blade_custom_helper, $folder);
        }
        if ($this->app->runningInConsole()) {
            $this->commands([
                \services\Options\Console\OptionSetCommand::class,
            ]);
        }
    }

    /**
     * Register the application services.
     */
    public function register()
    {
        $this->app->bind('option', \services\Options\Option::class);
    }
}
