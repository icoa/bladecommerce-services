<?php

namespace services\Options\Console;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use services\Options\OptionFacade as Option;

class OptionSetCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $name = 'option:set';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an option.';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $key = $this->argument('key');
        $value = $this->argument('value');
        if (option_exists($key)) {
            $this->comment('Option already exists.');
            $this->info(option($key));
            return;
        }
        Option::set($key, $value);

        $this->info('Option added.');
        $this->info(option($key));
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('key', InputArgument::REQUIRED, 'The option key'),
            array('value', InputArgument::REQUIRED, 'The option value'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }
}
