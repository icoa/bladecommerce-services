<?php

namespace services\Morellato\Csv\Exporters;

use DB, Utils, Attribute, AttributeOption, SplTempFileObject;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use League\Csv\Writer;
use services\Morellato\Loggers\FileLogger as Logger;
use Maatwebsite\Excel\Facades\Excel;

class AttributesExporter
{

    protected $console;
    protected $files;
    protected $logger;

    function __construct(Command $console, Filesystem $files, Logger $logger)
    {
        $this->console = $console;
        $this->files = $files;
        $this->logger = $logger;
    }

    function make()
    {
        $this->export_attributes();
        $this->export_attributes_options();
    }


    private function export_attributes()
    {
        $sourcePath = storage_path('morellato/csv/export/');
        $filename = $sourcePath . "attributes_" . date('Ymd') . '.csv';
        touch($filename);

        $this->console->line("Exporting attributes...");
        $rows = Attribute::with('translations')->orderBy('id')->get();

        //we create the CSV into memory
        $csv = Writer::createFromPath($filename);

        //we insert the CSV header
        $csv->insertOne(['attribute_id', 'code', 'name_it', 'name_en']);

        foreach ($rows as $row) {
            $this->console->line("Reading [$row->code]");
            if ($row->translations[1]->published == 1) {
                $csv_data = [
                    $row->id,
                    $row->code,
                    $row->translations[1]->name,
                    $row->translations[0]->name,
                ];
                $csv->insertOne($csv_data);
            }
        }


        $this->console->comment("Saving CSV file as [$filename]");
        $output = $csv->output();
    }


    private function export_attributes_options()
    {
        $sourcePath = storage_path('morellato/csv/export/');
        $filename = $sourcePath . "attributes_options_" . date('Ymd') . '.csv';
        touch($filename);

        $this->console->line("Exporting attributes options...");
        $rows = AttributeOption::with('translations')->orderBy('attribute_id')->orderBy('id')->get();

        //we create the CSV into memory
        $csv = Writer::createFromPath($filename);

        //we insert the CSV header
        $csv->insertOne(['option_id', 'attribute_id', 'name_it', 'name_en']);

        foreach ($rows as $row) {
            $this->console->line("Reading [$row->id]");
            if (isset($row->translations[1]) and $row->translations[1]->published == 1) {

                $name_it = null;
                if (isset($row->translations[1])) {
                    $name_it = $row->translations[1]->name == '' ? $row->uname : $row->translations[1]->name;
                }

                $name_en = null;
                if (isset($row->translations[0])) {
                    $name_en = $row->translations[0]->name == '' ? $row->uname : $row->translations[0]->name;
                }

                $csv_data = [
                    $row->id,
                    $row->attribute_id,
                    $name_it,
                    $name_en,
                ];
                $csv->insertOne($csv_data);
            }
        }


        $this->console->comment("Saving CSV file as [$filename]");
        $output = $csv->output();
    }


    public function make_excel()
    {
        /** @var Attribute[] $attributes */
        $attributes = Attribute::rows('it')
            ->whereIn('frontend_input', ['select', 'multiselect'])
            ->where('published', 1)
            ->orderBy('code')
            ->get();

        Excel::create('attributes', function ($excel) use ($attributes) {

            foreach ($attributes as $attribute) {
                /** @var AttributeOption[] $options */
                $options = AttributeOption::rows('it')
                    ->where('attribute_id', $attribute->id)
                    ->where('active', 1)
                    ->orderBy('name')
                    ->get();

                if (!empty($options)) {
                    $excel->sheet($attribute->code, function ($sheet) use ($attribute, $options) {
                        return $sheet->loadView('excels.attributes', compact('attribute', 'options'));
                    });
                }

            }

        })->store('xlsx', storage_path('export'));

        $this->console->info('Excel exported!');

    }

}