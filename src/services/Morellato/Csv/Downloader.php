<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 30/01/2017
 * Time: 15:35
 */

namespace services\Morellato\Csv;

use Config;
use services\Ftp\Connection;
use Illuminate\Console\Command;
use services\Morellato\Loggers\DatabaseLogger as Logger;
use Illuminate\Support\Str;

class Downloader
{

    protected $ftp;
    protected $command;
    protected $config;
    protected $logger;

    function __construct(Command $command, Connection $ftp, Logger $logger)
    {
        $this->ftp = $ftp;
        $this->command = $command;
        $this->logger = $logger;
        $default = Config::get('ftp.default');
        $this->config = Config::get('ftp.connections.' . $default);
    }

    function download()
    {
        $this->ftp->connect();
        $host = $this->config['host'];
        $username = $this->config['username'];
        $this->command->info("Connected to {$username}@{$host}");
        $path = $this->config['outPath'];
        $this->command->line("Getting files from path $path");
        $files = $this->ftp->files($path);

        $many = count($files);
        $this->command->info("Found [$many] files to download");

        foreach ($files as $file) {
            //skipping directory
            if ($this->ftp->isFile($file)) {
                $extension = strtolower(substr($file, -3));
                if ($extension == 'csv') {
                    $filename = str_replace($path . '/', null, $file);
                    $newFile = storage_path('morellato/csv/' . $filename);
                    $this->ftp->download($file, $newFile);
                    $this->command->info("File [$file] saved to [$newFile]");
                }
            }
        }
    }

    function move()
    {
        $this->ftp->connect();
        $path = $this->config['outPath'];
        $savePath = $this->config['outSavePath'];
        $files = $this->ftp->files($path);
        foreach ($files as $file) {
            $this->command->line("Accessing file [$file]");
            //skipping directory
            if ($this->ftp->isFile($file)) {
                $extension = strtolower(substr($file, -3));
                if ($extension == 'csv') {
                    $filename = str_replace($path . '/', null, $file);
                    if ($this->mustAppendTimestamp($filename)) {
                        $filename = str_replace(['.csv', '.CSV'], date('.Ymd_hi') . '.csv', $filename);
                    }
                    $newFile = $savePath . '/' . $filename;
                    $this->command->line("Renaming file [$file] to [$newFile]");
                    try {
                        $this->ftp->move($file, $newFile);
                        $this->command->info("File [$file] moved to [$newFile]");
                    } catch (\Exception $e) {
                        $this->logger->error("Could not move CSV file in FTP. [$file] to [$newFile]", 'ftp');
                        $this->command->error($e->getMessage());
                    }
                }
            }
        }
    }

    function files()
    {
        $this->ftp->connect();
        $path = $this->config['outPath'];
        $files = $this->ftp->files($path);
        $this->command->comment("Reading folder $path");
        foreach ($files as $file) {

            //skipping directory
            if ($this->ftp->isFile($file)) {
                $this->command->line("Read remote file [$file]");
            }
        }
    }

    private function mustAppendTimestamp($file)
    {
        return Str::contains(Str::lower($file), ['pdv', 'locator']);
    }
}