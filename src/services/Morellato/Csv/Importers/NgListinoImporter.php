<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 30/01/2017
 * Time: 15:58
 */

namespace services\Morellato\Csv\Importers;
use DB;

class NgListinoImporter extends ListinoImporter
{
    protected $file_pattern = 'NG_LISTINO';
    protected $source = 'NG';
    protected $truncate = false;
}