<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 30/01/2017
 * Time: 15:58
 */

namespace services\Morellato\Csv\Importers;

use DB;

class NgShopImporter extends Importer
{

    protected $table = 'mi_shops';
    protected $file_pattern = 'PDV_NEGOZIANDO';
    protected $truncate = false;
    protected $update = true;
    protected $skip_first_row = false;
    protected $delete_not_found = false;

    protected $headings = [
        "cd_neg",
        "cd_internal",
        "name",
        "description",
        "tel",
        "fax",
        "city",
        "address",
        "cap",
        "state",
        "email",
        "excerpt",
        "link",
    ];

    protected $codes = [];


    protected function canInsert($obj)
    {
        $model = DB::table($this->table)->where('cd_neg', $obj->cd_neg)->first();
        $this->codes[] = $obj->cd_neg;
        return is_null($model);
    }

    protected function getRecordId($obj)
    {
        $model = DB::table($this->table)->where('cd_neg', $obj->cd_neg)->first();
        return $model ? $model->id : 0;
    }

    protected function onFinish()
    {
        $this->console->comment("Executing onFinish event");

        DB::table($this->table)->whereNull('affiliate_id')->whereNotIn('cd_neg', $this->codes)->update(['active' => 0, 'updated_at' => date('Y-m-d H:i:s')]);
        DB::table($this->table)->whereNull('affiliate_id')->whereIn('cd_neg', $this->codes)->update(['active' => 1, 'updated_at' => date('Y-m-d H:i:s')]);

        $rows = \MorellatoShop::get();
        foreach ($rows as $row) {
            $this->console->line("Resolving address for [$row->name]");
            $row->resolveAddress();
            $row->createUser();
        }
        //deleted all stores that are not contained in the CSV
        if($this->delete_not_found)
            DB::table($this->table)->whereNull('affiliate_id')->whereNotIn('cd_neg', $this->codes)->update(['deleted_at' => date('Y-m-d H:i:s')]);

        $this->console->call('shop:assign_markers');
    }

    protected function onUpdate($obj, $id)
    {
        if($this->delete_not_found)
            DB::table($this->table)->where('id', $id)->update(['deleted_at' => null]);
    }
}