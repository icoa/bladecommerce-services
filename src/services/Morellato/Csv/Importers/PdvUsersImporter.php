<?php
/**
 * Created by Rezart.
 * User: Rezart
 * Date: 06/11/2017
 * Time: 16:58
 */

namespace services\Morellato\Csv\Importers;
use DB;
use Hash;
use Utils;

class PdvUsersImporter extends Importer
{

    protected $table = 'mi_pdv_users';
    protected $file_pattern = 'PDV_USERS';
    protected $import_filename = true;

    protected $headings = [
        "email",
        "password",
        "first_name",
        'last_name',
        'active',
        'shop_code'
    ];

    protected function avoidDuplicate($obj){
        DB::table($this->table)->where('email',$obj->email)->where('imported_file',$obj->imported_file)->delete();
    }

    protected function insert($obj){

      unset($obj->source);

      $values = (array)$obj;
      $values['password'] = Hash::make(trim($obj->password));
      $values['shop_code'] = trim($obj->shop_code);

      if ($this->debug) {
          Utils::log($values, __METHOD__);
      } else {
          DB::table($this->table)->insert($values);
      }
    }

}
