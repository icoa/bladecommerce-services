<?php

namespace services\Morellato\Csv\Importers;
use DB;

class NgCategorieImporter extends CategorieImporter
{
    protected $file_pattern = 'NG_CATEGORIE';
    protected $source = 'NG';
    protected $truncate = false;
}