<?php

namespace services\Morellato\Csv\Importers;

use DB, Utils;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use League\Csv\Reader;
use services\Morellato\Loggers\FileLogger as Logger;

class Importer
{

    protected $debug = false;
    protected $truncate = true;
    protected $update = false;
    //it indicates if the source file name must be inserted in every row as a separate field value
    protected $import_filename = false;

    protected $console;
    protected $files;
    protected $logger;

    protected $file;
    protected $filename;
    protected $table = 'mi_missing_table';
    protected $counter = 0;
    protected $chunks = 250;
    protected $errors = [];
    protected $csvFiles = [];
    protected $file_pattern = '*';
    protected $exclude_file_pattern = ['_ZZ20'];
    protected $utf_encode = false;
    protected $skip_first_row = true;
    protected $source = null;

    protected $headings = [

    ];

    function __construct(Command $console, Filesystem $files, Logger $logger)
    {
        $this->console = $console;
        $this->files = $files;
        $this->logger = $logger;
    }

    function make()
    {
        $sourcePath = storage_path('morellato/csv');
        $this->csvFiles = $this->files->glob($sourcePath . '/' . $this->file_pattern . '*');
        $count = count($this->csvFiles);
        $this->console->info("Found {$count} CSV files to import...");

        //truncating the table
        if ($count > 0) {
            if ($this->truncate)
                DB::table($this->table)->truncate();

            foreach ($this->csvFiles as $csvFile) {
                $process = true;
                if(!empty($this->exclude_file_pattern)){
                    foreach($this->exclude_file_pattern as $needle){
                        if(str_contains($csvFile, $needle)){
                            $process = false;
                        }
                    }
                }
                if($process){
                    $this->setFile($csvFile)->import();
                }else{
                    $this->console->comment("File {$csvFile} has been skipped!");
                    $this->setFile($csvFile)->finish(false);
                }
            }
        }

        return $this;
    }

    function setFile($file)
    {
        $this->file = $file;
        $info = pathinfo($file);
        $this->filename = $info['basename'];
        return $this;
    }

    function import()
    {
        $this->console->comment("Importing file [$this->file] into table [$this->table]");
        $content = $this->files->get($this->file);
        //$csv_content = iconv("Windows-1252", "UTF-8", $content);
        $encoding = mb_detect_encoding($content);
        $csv_content = $content;
        if ($encoding != 'UTF-8') {
            if ($encoding == 'ASCII')
                $encoding = 'Windows-1252';

            //$csv_content = mb_convert_encoding($content,'UTF-8',$encoding);
            try {
                $csv_content = iconv($encoding, "UTF-8", $content);
            } catch (\Exception $e) {
                $this->console->error("Detecting illegal encoding [$encoding]");
            }

        }
        $tempFile = $this->file . '.TMP';
        $this->files->put($tempFile, $csv_content);
        $reader = Reader::createFromPath($tempFile);
        $reader->setDelimiter(';');
        $reader->setNewline("\r\n");
        $reader->setEnclosure('"');
        //$reader->setEncodingFrom('ANSI');

        $input_bom = $reader->getInputBOM();

        //Utils::log("Encoding: $encoding | BOM: $input_bom");

        if ($input_bom === Reader::BOM_UTF16_LE || $input_bom === Reader::BOM_UTF16_BE) {
            Utils::log("CONVERTING BOM!!!");
            $reader->appendStreamFilter('convert.iconv.UTF-16/UTF-8');
        }
        $results = $reader->fetchAll();
        $this->beforeProcessRows();
        $this->processRows($results);

        $this->console->info("Imported [$this->counter] rows!");
        if (count($this->errors) > 0) {
            $cntErrors = count($this->errors);
            $this->console->error("There are [$cntErrors] errors!");
            $this->logger->error("Found [$cntErrors] while processing CSV file [$this->file]");
            foreach ($this->errors as $err) {
                $this->console->error($err);
                $this->logger->error($err);
            }
        }
        $this->files->delete($tempFile);
        $this->finish();
        unset($results);
        unset($reader);
        unset($csv_content);
        unset($content);
    }

    function finish($with_callback = true)
    {
        if ($this->debug)
            return;

        $doneFolder = storage_path('morellato/csv/done');
        if (!$this->files->isDirectory($doneFolder)) {
            $this->files->makeDirectory($doneFolder);
        }

        $info = pathinfo($this->file);

        $dirname = $info['dirname'];
        $basename = $info['basename'];
        $newBasename = 'done/' . $basename;
        $oldFile = $dirname . '/' . $basename;
        $newFile = $dirname . '/' . $newBasename;
        $this->files->move($oldFile, $newFile);
        $this->console->info("CSV file [$basename] moved to [$newBasename]");

        if ($with_callback && method_exists($this, 'onFinish')) {
            $this->onFinish();
        }
    }


    protected function avoidDuplicate($obj)
    {

    }

    protected function beforeProcessRows()
    {

    }

    protected function canInsert($obj)
    {
        return true;
    }

    protected function getRecordId($obj)
    {
        return 0;
    }

    protected function shouldBeSkipped($obj)
    {
        return false;
    }


    protected function processRows($rows)
    {
        $count = count($rows);
        $this->console->comment("Processing [$count] rows...");

        DB::beginTransaction();
        $counter = 0;

        foreach ($rows as $row) {
            if ($counter == 0 and $this->skip_first_row) {
                $this->console->line('Skipping first row');
            } else {
                try {
                    $obj = new \stdClass();
                    foreach ($this->headings as $index => $heading) {
                        if (isset($row[$index]))
                            $obj->$heading = $this->sanitize($row[$index]);
                    }
                    if ($this->import_filename)
                        $obj->imported_file = $this->filename;

                    $obj->source = $this->source;

                    if (false === $this->shouldBeSkipped($obj)) {
                        if ($this->canInsert($obj)) {
                            $this->avoidDuplicate($obj);
                            $this->insert($obj);
                            $this->counter++;
                        } else {
                            if ($this->update) {
                                $id = $this->getRecordId($obj);
                                if ($id > 0) {
                                    $this->update($obj, $id);
                                }
                            }
                        }
                    } else {
                        $this->console->comment("Row has been skipped");
                    }

                    unset($row);
                    unset($obj);
                    unset($id);

                } catch (\Exception $e) {

                    $this->error($e->getMessage());
                    audit($e->getTraceAsString());

                }
            }

            $counter++;

            $this->console->line("Record imported ($counter/$count)");
        }

        DB::commit();

        unset($rows);

        $this->console->comment("Transaction has been committed");
    }

    protected function insert($obj)
    {
        $values = (array)$obj;
        $values['created_at'] = date('Y-m-d H:i:s');
        if ($this->debug) {
            Utils::log($values, __METHOD__);
        } else {
            DB::table($this->table)->insert($values);
            if (method_exists($this, 'onInsert')) {
                $this->onInsert($obj);
            }
        }
    }

    protected function update($obj, $id)
    {
        $values = (array)$obj;
        $values['created_at'] = date('Y-m-d H:i:s');
        if ($this->debug) {
            Utils::log($values, __METHOD__);
        } else {
            DB::table($this->table)->where('id', $id)->update($values);
            if (method_exists($this, 'onUpdate')) {
                $this->onUpdate($obj, $id);
            }
        }
    }


    protected function error($msg)
    {
        $this->errors[] = $msg;
    }

    protected function sanitize($str)
    {
        $str = str_replace(chr(0xC2) . chr(0xA0), '', $str);
        $str = (trim($str));
        if ($this->utf_encode) {
            $str = utf8_encode($str);
        }
        $str = str_replace(chr(0xC2) . chr(0xA0), '', $str);
        return $str;
    }


    public function restoreFiles()
    {
        $folder = storage_path('morellato/csv/done');
        $root = storage_path('morellato/csv/');
        $files = $this->files->files($folder);
        $many = count($files);
        $this->console->line("Found [$many] files to restore");
        foreach ($files as $file) {
            $basename = pathinfo($file)['basename'];
            $newFilename = $root . $basename;
            $this->files->move($file, $newFilename);
            $this->console->info("File [$basename] has been restored");
        }
    }

    public function getGiacenzaFilePattern($obj)
    {
        /**
         * this should return 'NG_GIACENZA' as well
         * GIACENZA_01
         * GIACENZA_03
         * GIACENZA_WM
         * GIACENZA_W3
         */

        return substr($obj->imported_file, 0, 11);
    }
}