<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 30/01/2017
 * Time: 15:58
 */

namespace services\Morellato\Csv\Importers;
use DB;

class NgGiacenzaImporter extends Importer
{

    protected $table = 'mi_giacenza_ng';
    protected $file_pattern = 'NG_GIACENZA';
    protected $import_filename = true;
    protected $source = 'NG';

    protected $headings = [
        "sku",
        "giacenzaH24",
        "giacenzaH48",
        "disponibilita_futura",
    ];

    protected function avoidDuplicate($obj){
        DB::table($this->table)->where('sku', $obj->sku)->where('imported_file', 'like', $this->getGiacenzaFilePattern($obj) . '%')->delete();
    }
}