<?php
namespace services\Morellato\Csv\Importers;

use DB;

class NgStoreLocatorImporter extends Importer
{

    protected $table = 'resellers';
    protected $file_pattern = 'StoreLocator';
    protected $truncate = false;
    protected $update = true;
    protected $skip_first_row = true;

    protected $cache = [];
    protected $brand_id = 0;

    protected $headings = [
        "Codice",
        "RagioneSociale",
        "Nazione",
        "Localita",
        "Provincia",
        "Regione",
        "Via",
        "Mail",
        "Lat",
        "Lng",
        "Stato",
        "Brand",
        "Note",
        "Telefono",
        "Link",
    ];

    /**
     * @return int
     */
    private function getBrandId()
    {
        if ($this->brand_id > 0)
            return $this->brand_id;

        $this->brand_id = (int)config('morellato.reseller_brand_id', 1);
        return $this->brand_id;
    }


    protected function beforeProcessRows()
    {
        $brand_id = $this->getBrandId();
        if ($brand_id > 1) {
            DB::table($this->table)->where('brand', '!=', $brand_id)->delete();
            $this->console->info("Deleted non-matching records");
        }
    }

    protected function canInsert($obj)
    {
        $model = DB::table($this->table)->where('code', $obj->Codice)->where('brand', $obj->Brand)->first();
        return is_null($model);
    }

    protected function getRecordId($obj)
    {
        $model = DB::table($this->table)->where('code', $obj->Codice)->where('brand', $obj->Brand)->first();
        return $model ? $model->id : 0;
    }

    protected function shouldBeSkipped($obj)
    {
        $brand_id = $this->getBrandId();
        if ($brand_id > 1 and $obj->Brand != $brand_id)
            return true;

        return false;
    }

    protected function insert($obj)
    {
        $values = [
            'code' => $obj->Codice,
            'name' => $obj->RagioneSociale,
            'country_id' => $this->getCountryId($obj->Nazione),
            'country_text' => $obj->Nazione,
            'province' => $obj->Provincia,
            'city' => $obj->Localita,
            'region' => $obj->Regione,
            'address' => $obj->Via,
            'email' => $obj->Mail,
            'latitude' => $obj->Lat,
            'longitude' => $obj->Lng,
            'telephone' => $obj->Telefono,
            'link' => $obj->Link,
            'brand' => $obj->Brand,
            'active' => $obj->Stato,
        ];

        DB::table($this->table)->insert($values);

    }

    protected function update($obj, $id)
    {
        $values = [
            'code' => trim($obj->Codice),
            'name' => trim($obj->RagioneSociale),
            'country_id' => $this->getCountryId($obj->Nazione),
            'country_text' => trim($obj->Nazione),
            'province' => trim($obj->Provincia),
            'city' => trim($obj->Localita),
            'region' => trim($obj->Regione),
            'address' => trim($obj->Via),
            'email' => trim($obj->Mail),
            //'latitude' => trim($obj->Lat),
            //'longitude' => trim($obj->Lng),
            'telephone' => trim($obj->Telefono),
            'link' => trim($obj->Link),
            'brand' => $obj->Brand,
            'active' => $obj->Stato,
        ];

        DB::table($this->table)->where('id', $id)->update($values);

    }


    private function getCountryId($nation)
    {

        $nation = trim($nation);

        if (isset($this->cache[$nation])) {
            return $this->cache[$nation];
        }

        $country = \Country::where('iso_code', $nation)->first();
        if ($country and $country->id > 0) {
            $this->cache[$nation] = $country->id;
            return $country->id;
        }

        return 0;
    }


    protected function onFinish()
    {
        $this->console->comment("Executing onFinish event");
        $rows = \Reseller::get();
        $many = count($rows);
        $this->console->line("Checking [$many] items...");
        foreach ($rows as $row) {
            $resolved = $row->resolveAddress($this->console);
            if ($resolved) {
                $this->console->info("Resolved address for [$row->name]");
            }
        }
    }
}