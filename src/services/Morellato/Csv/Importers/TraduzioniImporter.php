<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 30/01/2017
 * Time: 15:58
 */

namespace services\Morellato\Csv\Importers;
use DB;

class TraduzioniImporter extends Importer
{

    protected $table = 'mi_traduzioni';
    protected $file_pattern = 'TRADUZIONI';
    protected $utf_encode = true;
    protected $import_filename = true;

    protected $headings = [
        "sku",
        "name",
        //"description",
        "commercial_description",
        "lang_id",
        //"complete_description",
        //"attributes",
        //"category",
        //"gender",
    ];

    protected function avoidDuplicate($obj){
        DB::table($this->table)
            ->where('sku',$obj->sku)
            ->where('lang_id',$obj->lang_id)
            ->where('imported_file',$obj->imported_file)
            ->delete();
    }

    protected function canInsert($obj){
        if(strlen($obj->sku) == 0)
            return false;

        if(in_array($obj->lang_id, config('ftp.locales', ['IT', 'EN']))) {
            return true;
        }

        return false;
    }
}