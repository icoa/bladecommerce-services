<?php
/**
 * Created by Rezart.
 * User: Rezart
 * Date: 06/11/2017
 * Time: 16:58
 */

namespace services\Morellato\Csv\Importers;

use DB;
use Hash;
use Utils;
use services\Morellato\Database\StrapsCustomerImporter;

class StrapsCustomersImporter extends Importer
{

    protected $table = 'mi_customers';
    protected $file_pattern = 'anagcli';
    protected $import_filename = true;

    protected $headings = [
        'OrganizzazioneCommerciale',
        'SettoreMerceologico',
        'CodiceCliente',
        'CodiceSpedizione',
        'RagioneSocialeDest',
        'IndirizzoEmail',
        'IndirizzoDest',
        'CittaDest',
        'Provinciadest',
        'CAPDest',
        'Indirizzo',
        'Citta',
        'Provincia',
        'CAP',
        'Telefono',
        'PIVA',
        'Banca',
        'AbiCab',
        'CC',
        'DescrizionePag',
        'CodicePag',
        'Flag',
        'Fax',
        'RagioneSociale',
        'CF',
        'IBAN',
        'CodiceListino',
        'incoDeafult',
        'listinoNdefault',
        'DescrizioneListinoNDefault',
        'CanaleD',
        'CodiceResponsabileContabile',
        'NomeResponsabileContabile',
        'EmailResponsabileContabile',
        'FamPagamento',
        'ClasseCommerciale',
        'DescClasseCommerciale',
        'FatturatoAnnuo',
        'ClasseRischio',
        'DescClasseRischio',
        'PercepitoCommerciale',
        'ListinoProdotti',
        'Valuta',
        'FlagPromoPag',
        'DescPromoPag',
        'BloccaPagFlag',
        'Column1',
    ];

    protected function avoidDuplicate($obj)
    {
        DB::table($this->table)->where('CodiceCliente', trim($obj->CodiceCliente))->delete();
        DB::table($this->table)->where('IndirizzoEmail', trim($obj->IndirizzoEmail))->delete();
    }

    protected function onFinish()
    {
        /* @var StrapsCustomerImporter $importer */
        $importer = app('services\Morellato\Database\StrapsCustomerImporter');

        if (\App::environment() == 'live') {
            $importer->make();
            $importer->sendMailToCustomers();
        }

    }

}
