<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 30/01/2017
 * Time: 15:58
 */

namespace services\Morellato\Csv\Importers;

use DB;

class AnagraficaImporter extends Importer
{

    protected $table = 'mi_anagrafica';
    protected $file_pattern = 'ANAGRAFICA';
    protected $import_filename = true;

    protected $headings = [
        "sap_sku",
        "mastersku",
        "intrastat",
        "vat",
        "type",
        //"variants",
        "name",
        //"description",
        //"commercial_description",
        "weight",
        "attr_gender",
        "attr_materials",
        "brand",
        "size",
        "attr_colors",
        "attr_movement",
        "attr_functions",
        //"attr_label",
        "attr_collection",
        "attr_category",
        "attr_moments",
        "attr_karats",
        "canvas",
        "ean",
        "attr_bracelet_shortening",
        "attr_battery_change",
        "outlet",
        "sku",
        "sub_brand",
        "stone",
        "model",
        "caratura_pietra",
        "caratura_diamante",
        "maglia",
        "forma",
        "dial_type",
        "case_size",
        "case_finishing",
        "strap_material",
        "case_thickness",
        "dial_col_fin",
        "strap_type",
        "glass",
        "strap_colours",
        "lock_type",
        "diamonds",
        "water_resistance",
        "pietra1",
        "caratura_pietra1",
        "taglio_pietra1",
        "purezza_pietra1",
        "colore_diamante_pietra1",
        "dimensione_pietra1",
        "colore_pietra1",
        "forma_pietra1",
        "pietra2",
        "caratura_pietra2",
        "taglio_pietra2",
        "purezza_pietra2",
        "colore_diamante_pietra2",
        "dimensione_pietra2",
        "colore_pietra2",
        "forma_pietra2",
        "pietra3",
        "caratura_pietra3",
        "taglio_pietra3",
        "purezza_pietra3",
        "colore_diamante_pietra3",
        "dimensione_pietra3",
        "colore_pietra3",
        "forma_pietra3",
        "pietra4",
        "caratura_pietra4",
        "taglio_pietra4",
        "purezza_pietra4",
        "colore_diamante_pietra4",
        "dimensione_pietra4",
        "colore_pietra4",
        "forma_pietra4",
        "json",

    ];


    protected function avoidDuplicate($obj){
        DB::table($this->table)->where('sku', $obj->sku)->where('imported_file', 'like', $this->getGiacenzaFilePattern($obj) . '%')->delete();
    }
}