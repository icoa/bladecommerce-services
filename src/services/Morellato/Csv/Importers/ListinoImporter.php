<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 30/01/2017
 * Time: 15:58
 */

namespace services\Morellato\Csv\Importers;
use DB;

class ListinoImporter extends Importer
{

    protected $table = 'mi_listino';
    protected $file_pattern = 'LISTINO';
    protected $import_filename = true;

    protected $headings = [
        "sku",
        "valuta",
        "prezzo",
        "tipoprezzo",
        "date_from",
        "date_to",
    ];

    protected function avoidDuplicate($obj){
        DB::table($this->table)
            ->where('sku',$obj->sku)
            ->where('tipoprezzo',$obj->tipoprezzo)
            ->where('date_from',$obj->date_from)
            ->where('date_to',$obj->date_to)
            ->where('imported_file',$obj->imported_file)
            ->delete();
    }
}