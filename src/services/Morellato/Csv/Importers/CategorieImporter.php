<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 30/01/2017
 * Time: 15:58
 */

namespace services\Morellato\Csv\Importers;
use DB;

class CategorieImporter extends Importer
{

    protected $table = 'mi_categorie';
    protected $file_pattern = 'CATEGORIE';
    protected $import_filename = true;

    protected $headings = [
        "mastersku",
        "categoria1livello1",
        "categoria1livello2",
    ];


    protected function avoidDuplicate($obj){
        DB::table($this->table)->where('mastersku',$obj->mastersku)->where('imported_file',$obj->imported_file)->delete();
    }
}