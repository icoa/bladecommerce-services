<?php

namespace services\Morellato\Controllers;

use Address;
use Brand;
use Campaign;
use Carrier;
use Cart;
use CartProduct;
use CartRule;
use Category;
use Collection;
use Config;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Input, Response, Request;
use BaseController;
use Order;
use Customer;
use OrderDetail;
use OrderHistory;
use OrderState;
use Payment;
use PaymentState;
use Attribute;
use Carbon\Carbon;
use Product;
use Rma;
use RmaDetail;
use RmaOption;
use RmaState;
use Cache;
use services\ClerkFeed;
use services\Models\OrderSlip;
use Faker\Factory as FakerFactory;
use Wishlist;

/**
 * POSTMAN COLLECTION
 * https://grey-spaceship-9774.postman.co/collections/7952821-dfd6151a-0a2c-4704-9b29-57b685ef3f35/publish?version=latest&workspace=b07d6d5b-745e-4ebe-8e4b-b4d9a7847d44
 *
 * /api/orders/customer/dcf11fbf911e8345d0a1ce4a78b17817
 * /api/orders/search/17240
 * /api/orders/search/KRS86066
 * /api/orders/feed/it
 * /api/orders/status/25770
 * /api/orders/labels
 *
 * Class OrdersRestController
 * @package services\Morellato\Controllers
 */
class OrdersRestController extends BaseController
{

    protected $cache_ttl = 60;
    protected $fire_plugins = false;
    protected $pagesize = 100;
    protected $multiple_size = 50;
    /** @var \Faker\Generator */
    protected $faker;
    protected $is_faked = false;

    function __construct()
    {
        parent::__construct();
        $this->faker = FakerFactory::create('it_IT');
        $this->is_faked = Request::header('X-Sensitive') === 'faked';
        Config::set('session.driver', 'array');
    }

    /**
     * @return JsonResponse
     */
    function getPing()
    {
        $platform = config('plugins_settings.selligent.suffix');
        $msg = 'pong from ' . \Cfg::get('SHORTNAME');
        $time = Carbon::now()->toIso8601String();
        return Response::json(compact('msg', 'time', 'platform'));
    }

    function getPostman()
    {
        $postman_collection = base_path('vendor/bladecommmerce/services/src/services/Meta/Postman/orders.json');
        $data = json_decode(file_get_contents($postman_collection));
        $live_url = config('app.url');
        $host = explode('.', str_replace(['http://', 'https://'], '', $live_url));
        foreach ($data->item as $item) {
            $item->request->url->raw = str_replace('https://www.philipwatch.net', $live_url, $item->request->url->raw);
            $item->request->url->host = $host;
        }
        return Response::json($data);
    }

    /**
     * Return a custom payload about Order data
     *
     * @param $id
     * @return JsonResponse
     */
    function getStatus($id)
    {
        /** @var Order $order */
        $order = Order::getObj($id);
        if ($order) {
            $data = $this->transformOrder($order);
            return Response::json($data);
        }
        return $this->_error();
    }

    /**
     * Return all labels for order and product states
     *
     * @return JsonResponse
     */
    function getLabels()
    {
        $data = Cache::remember($this->getCacheKey(__METHOD__), $this->cache_ttl, function () {
            /** @var OrderState[] $order_states */
            $order_states = OrderState::with('translations')->get();
            $rows_os = [];
            foreach ($order_states as $order_state) {
                $rows_os[] = $this->transformState($order_state);
            }

            /** @var PaymentState[] $payment_states */
            $payment_states = PaymentState::with('translations')->get();
            $rows_ps = [];
            foreach ($payment_states as $payment_state) {
                $rows_ps[] = $this->transformState($payment_state);
            }

            /** @var Carrier[] $carriers */
            $carriers = Carrier::where('active', 1)->with('translations')->get();
            $rows_ca = [];
            foreach ($carriers as $carrier) {
                $rows_ca[] = $this->transformCarrier($carrier, true);
            }

            /** @var Payment[] $payments */
            $payments = Payment::where('active', 1)->with('translations')->get();
            $rows_pa = [];
            foreach ($payments as $payment) {
                $rows_pa[] = $this->transformPayment($payment, true);
            }

            /** @var RmaState[] $rma_states */
            $rma_states = RmaState::with('translations')->get();
            $rows_rs = [];
            foreach ($rma_states as $rma_state) {
                $rows_rs[] = $this->transformState($rma_state);
            }

            /** @var RmaOption[] $rma_options */
            $rma_options = RmaOption::with('translations')->get();
            $rows_ro = [];
            foreach ($rma_options as $rma_option) {
                $rows_ro[] = $this->transformState($rma_option);
            }

            return [
                'order_states' => $rows_os,
                'payment_states' => $rows_ps,
                'rma_states' => $rows_rs,
                'rma_options' => $rows_ro,
                'carriers' => $rows_ca,
                'payments' => $rows_pa,
            ];

        });

        return Response::json($data);
    }

    /**
     * Return all labels for order and product states
     *
     * @return JsonResponse
     */
    function getAttributes()
    {
        $data = Cache::remember($this->getCacheKey(__METHOD__), $this->cache_ttl, function () {
            /** @var Attribute[] $attributes */
            $attributes = Attribute::with('translations')->get();
            $data = [];
            foreach ($attributes as $attribute) {
                $data[] = $this->transformAttribute($attribute);
            }
            return $data;
        });

        return Response::json($data);
    }

    /**
     * Return all categories
     *
     * @return JsonResponse
     */
    function getCategories()
    {
        $data = Cache::remember($this->getCacheKey(__METHOD__), $this->cache_ttl, function () {
            /** @var Category[] $categories */
            $categories = Category::with('translations')->get();
            $data = [];
            foreach ($categories as $category) {
                $data[] = $this->transformCategory($category);
            }
            return $data;
        });

        return Response::json($data);
    }

    /**
     * Return all brands
     *
     * @return JsonResponse
     */
    function getBrands()
    {
        $data = Cache::remember($this->getCacheKey(__METHOD__), $this->cache_ttl, function () {
            /** @var Brand[] $brands */
            $brands = Brand::with('translations')->get();
            $data = [];
            foreach ($brands as $brand) {
                $data[] = $this->transformBrand($brand);
            }
            return $data;
        });

        return Response::json($data);
    }

    /**
     * Return all collections
     *
     * @param null $brand_id
     * @return JsonResponse
     */
    function getCollections($brand_id = null)
    {
        $data = Cache::remember($this->getCacheKey(__METHOD__ . $brand_id), $this->cache_ttl, function () use ($brand_id) {
            /** @var Collection[] $collections */
            $collections = $brand_id > 0 ? Collection::where('brand_id', $brand_id)->with('translations')->get() : Collection::with('translations')->get();
            $data = [];
            foreach ($collections as $collection) {
                $data[] = $this->transformCollection($collection);
            }
            return $data;
        });

        return Response::json($data);
    }

    /**
     * Return all cart rules
     *
     * @param null $mode
     * @return JsonResponse
     */
    function getCartrules($mode = null)
    {
        $data = Cache::remember($this->getCacheKey(__METHOD__ . $mode), $this->cache_ttl, function () use ($mode) {
            /** @var CartRule[] $cartRules */
            $cartRules = $mode === 'available' ? CartRule::available()->active()->with('translations')->get() : CartRule::active()->with('translations')->get();
            $data = [];
            foreach ($cartRules as $cartRule) {
                $data[] = $this->transformCartRule($cartRule);
            }
            return $data;
        });

        return Response::json($data);
    }

    /**
     * Return the JSON feed file
     *
     * @param $lang
     * @return JsonResponse|\Illuminate\Http\Response
     */
    function getFeed($lang)
    {
        $valid_languages = \Core::getLanguages();
        if (!in_array($lang, $valid_languages)) {
            return $this->_error('Unsupported language/locale', 500);
        }
        $locale = strtoupper($lang);
        if ($lang == 'en')
            $locale = 'US';

        $clerkFeed = new ClerkFeed($lang, $locale);
        $jsonFile = $clerkFeed->getJsonFile();
        if (file_exists($jsonFile))
            return Response::make(file_get_contents($jsonFile), 200, ['Content-type' => 'application/json']);

        return $this->_error('File does not exist or is not ready yet');
    }

    /**
     * Search orders by reference or by Customer ID
     *
     * @param $query
     * @return JsonResponse
     */
    function getSearch($query)
    {
        if (strlen(trim($query)) == 0)
            return $this->_error("Nothing provided");

        $builder = is_numeric($query) ? Order::where('customer_id', $query) : Order::where('reference', 'like', "%$query%");

        /** @var Order[] $orders */
        $orders = $builder->orderBy('id', 'desc')->take(10)->get();

        $payloads = [];

        foreach ($orders as $order) {
            $payloads[] = $this->transformOrder($order);
        }

        return Response::json($payloads);
    }

    /**
     * Search a customer by is digest
     *
     * @param $query
     * @return JsonResponse
     */
    function getCustomer($query)
    {
        $customer = null;
        if (strlen($query) === 32) {
            /** @var Customer $customer */
            $customer = Customer::where('active', 1)->where(\DB::raw('MD5(TRIM(email))'), $query)->first();
        }
        if (is_numeric($query)) {
            /** @var Customer $customer */
            $customer = Customer::where('active', 1)->find($query);
        }
        if ($customer) {
            $data = $this->transformCustomer($customer, true);
            $orders = $customer->getOrders();
            $orders_rows = [];
            foreach ($orders as $order) {
                $orders_rows[] = $this->transformOrder($order, false);
            }
            $data['orders'] = $orders_rows;
            return Response::json($data);
        }
        return $this->_error('Could not find any customer');
    }

    /**
     * Search a customer by is digest
     *
     * @param $query
     * @return JsonResponse
     */
    function getWishlist($query)
    {
        $customer = null;
        if (strlen($query) === 32) {
            /** @var Customer $customer */
            $customer = Customer::with('wishlists')->where('active', 1)->where(\DB::raw('MD5(TRIM(email))'), $query)->first();
        }
        if (is_numeric($query)) {
            /** @var Customer $customer */
            $customer = Customer::with('wishlists')->where('active', 1)->find($query);
        }
        if ($customer) {
            $data = $this->transformCustomer($customer);
            $wishlists_array = [];
            $wishlists = $customer->getWishlists();
            foreach ($wishlists as $wishlist) {
                $wishlists_array[] = $this->transformWishlist($wishlist, true);
            }
            $data['wishlists'] = $wishlists_array;
            return Response::json($data);
        }
        return $this->_error('Could not find any customer');
    }

    /**
     * @param string $reason
     * @param int $code
     * @return JsonResponse
     */
    protected function _error($reason = 'Not found', $code = 404)
    {
        return Response::json(['reason' => $reason], $code);
    }

    /**
     * @param Order $order
     * @param bool $detailed
     * @return array
     */
    protected function transformOrder(Order $order, $detailed = false)
    {
        $data = [
            'item_type' => 'order',
            'id' => (int)$order->id,
            'lang_id' => $order->lang_id,
            'reference' => $order->reference,
            'customer_id' => ($order->customer_id > 0) ? (int)$order->customer_id : null,
            'relations' => [
                'master' => $order->isMaster(),
                'child' => $order->isChild(),
                'parent_id' => ($order->parent_id > 0) ? (int)$order->parent_id : null,
                'customer_id' => ($order->customer_id > 0) ? (int)$order->customer_id : null,
                'campaign_id' => ($order->campaign_id > 0) ? (int)$order->campaign_id : null,
                'cart_rule_id' => ($order->cart_rule_id > 0) ? (int)$order->cart_rule_id : null,
                'cart_id' => ($order->cart_id > 0) ? (int)$order->cart_id : null,
                'payment_id' => ($order->payment_id > 0) ? (int)$order->payment_id : null,
                'carrier_id' => ($order->carrier_id > 0) ? (int)$order->carrier_id : null,
                'shipping_address_id' => ($order->shipping_address_id > 0) ? (int)$order->shipping_address_id : null,
                'billing_address_id' => ($order->billing_address_id > 0) ? (int)$order->billing_address_id : null,
            ],
            'state' => OrderState::statusToString($order->status),
            'payment_state' => PaymentState::statusToString($order->payment_status),
            'carrier' => $this->transformCarrier($order->getCarrier()),
            'payment' => $this->transformPayment($order->getPayment()),
            'created_at' => $this->transformDate($order->created_at),
            'updated_at' => $this->transformDate($order->updated_at),
        ];

        if (true === $detailed) {

            unset($data['carrier'], $data['payment']);

            $customer = $order->getCustomer();
            $orderHistoryStates = $order->historyStates(OrderHistory::TYPE_ORDER);
            $paymentHistoryStates = $order->historyStates(OrderHistory::TYPE_PAYMENT);
            $delivery_store = $order->getDeliveryStore();

            $data += [
                'state_history' => $orderHistoryStates->map(function ($obj) {
                    return $this->transformHistoryState($obj);
                }),
                'payment_state_history' => $paymentHistoryStates->map(function ($obj) {
                    return $this->transformHistoryState($obj);
                }),
                'tracking_url' => $order->getGlsTrackingUrl(),
                'tracking_code' => $order->shipping_number,
                'delivery_store' => $delivery_store ? $delivery_store->cd_neg : null,
                'details_url' => $order->getOrderFrontUrl(),
                'customer' => $customer ? $this->transformCustomer($customer) : null,
            ];

            $carrier = $order->getCarrier();
            $payment = $order->getPayment();
            $shipping_address = $order->getShippingAddress();
            $billing_address = $order->getBillingAddress();

            $data['shipping_method'] = $carrier ? $this->transformCarrier($carrier) : null;
            $data['payment_method'] = $payment ? $this->transformPayment($payment) : null;
            $data['shipping_address'] = $shipping_address ? $this->transformAddress($shipping_address) : null;
            $data['billing_address'] = $billing_address ? $this->transformAddress($billing_address) : null;

            //relations
            $data['gift'] = (bool)$order->gift;
            $data['currency'] = $order->getCurrency()->iso_code;
            $data['coupon_code'] = (string)$order->coupon_code === '' ? null : $order->coupon_code;
            $campaign = $order->getCampaignEntity();
            $data['campaign'] = $campaign ? $this->transformCampaign($campaign) : null;
            $cart_rule = $order->getCartRuleEntity();
            $data['cart_rule'] = $cart_rule ? $this->transformCartRule($cart_rule) : null;

            //amounts
            $data['amount_total'] = $order->getTotalOrderAmountAttribute();
            $data['amount_products'] = $order->getProductsAmountAttribute();
            $data['amount_shipping'] = $order->getShippingAmountAttribute();
            $data['amount_additional_costs'] = $order->getAdditionalCostsAmountAttribute();
            $data['amount_discount'] = $order->getDiscountAmountAttribute();
            $slip = $order->getMorellatoSlip();
            $data['order_slips'] = ($slip) ? $this->transformOrderSlip($slip) : null;

            $data['order_details'] = [];
            $orderDetails = $order->getProducts(false);
            foreach ($orderDetails as $orderDetail) {
                $data['order_details'][] = $this->transformOrderDetail($orderDetail);
            }
        }

        return $data;
    }

    /**
     * @param OrderHistory $orderHistory
     * @return array
     */
    protected function transformHistoryState(OrderHistory $orderHistory)
    {
        $data = [
            'type' => $orderHistory->type,
            'name' => $orderHistory->type == OrderHistory::TYPE_ORDER ? OrderState::statusToString($orderHistory->status_id) : PaymentState::statusToString($orderHistory->status_id),
            'created_at' => $this->transformDate($orderHistory->created_at),
            'updated_at' => $this->transformDate($orderHistory->updated_at),
        ];
        return $data;
    }

    /**
     * @param $date
     * @return string
     */
    protected function transformDate($date)
    {
        if ($date instanceof Carbon) {

        } else {
            $date = Carbon::parse($date);
        }
        return $date->toIso8601String();
    }

    /**
     * @param OrderState|PaymentState|RmaState|RmaOption $state
     * @return array
     */
    protected function transformState($state)
    {
        $payload = [
            'id' => (int)$state->id,
            'code' => $state->code,
            'translations' => [],
        ];

        foreach ($state->translations as $translation) {
            $payload['translations'][$translation->lang_id] = $translation->name;
        }
        return $payload;
    }

    /**
     * @param Customer $customer
     * @return array
     */
    protected function transformCustomer(Customer $customer, $detailed = false)
    {
        $payload = [
            'id' => (int)$customer->id,
            'lang_id' => $customer->lang_id,
            'code' => $customer->getUuidAttribute(),
            'email' => $this->is_faked ? $this->faker->email : $customer->email,
            'name' => $this->is_faked ? $this->faker->name : $customer->getName(),
            'flag_communication' => (bool)$customer->getComunicazioneAttribute(),
            'flag_segmentation' => (bool)$customer->getSegmentazioneAttribute(),
            'created_at' => $this->transformDate($customer->created_at),
            'updated_at' => $this->transformDate($customer->updated_at),
        ];

        if ($detailed) {
            $payload['gender'] = $customer->gender_short;
            $payload['customer_type'] = $customer->type;
            $payload['customer_group_id'] = (int)$customer->default_group_id;
            $payload['customer_group_name'] = $customer->group_name;
            $payload['customer_alt_group_id'] = $customer->alt_group_id ? (int)$customer->alt_group_id : null;
            $payload['customer_alt_group_name'] = $customer->alt_group_name;
            $payload['customer_shop_id'] = $customer->shop_code;
            $payload['customer_shop_name'] = $customer->shop_name;
            $payload['card_code'] = $this->is_faked ? $this->faker->isbn13 : $customer->card_code;
            $payload['birthday'] = $customer->birthday;
            $payload['newsletter'] = (bool)$customer->newsletter;

            $shipping_address = $customer->getAddress();
            $billing_address = $customer->getAddress(1);
            $payload['shipping_address'] = $shipping_address ? $this->transformAddress($shipping_address) : null;
            $payload['billing_address'] = $billing_address ? $this->transformAddress($billing_address) : null;
        }

        return $payload;
    }

    /**
     * @param Attribute $attribute
     * @return array
     */
    protected function transformAttribute(Attribute $attribute)
    {
        $payload = [
            'id' => (int)$attribute->id,
            'code' => $attribute->code,
            'description' => $attribute->ldesc,
            'type' => $attribute->frontend_input,
            'translations' => [],
        ];

        foreach ($attribute->translations as $translation) {
            $name = $translation->name;
            $slug = $attribute->transformApiLabelAttribute($name);
            $payload['translations'][$translation->lang_id] = compact('name', 'slug');
        }

        return $payload;
    }

    /**
     * @param Category $category
     * @return array
     */
    protected function transformCategory(Category $category)
    {
        $payload = [
            'id' => (int)$category->id,
            'parent_id' => $category->parent_id > 0 ? (int)$category->parent_id : null,
            'position' => (int)$category->position,
            'translations' => [],
        ];

        foreach ($category->translations as $translation) {
            $name = $translation->name;
            $slug = $translation->slug;
            $singular = $translation->singular ? $translation->singular : $name;
            $published = (bool)$translation->published;
            $payload['translations'][$translation->lang_id] = compact('name', 'slug', 'singular', 'published');
        }

        return $payload;
    }

    /**
     * @param Collection $collection
     * @return array
     */
    protected function transformCollection(Collection $collection)
    {
        $payload = [
            'id' => (int)$collection->id,
            'brand_id' => (int)$collection->brand_id,
            'position' => (int)$collection->position,
            'translations' => [],
        ];

        foreach ($collection->translations as $translation) {
            $name = $translation->name;
            $slug = $translation->slug;
            $published = (bool)$translation->published;
            $payload['translations'][$translation->lang_id] = compact('name', 'slug', 'published');
        }

        return $payload;
    }

    /**
     * @param Brand $brand
     * @return array
     */
    protected function transformBrand(Brand $brand)
    {
        $payload = [
            'id' => (int)$brand->id,
            'position' => (int)$brand->position,
            'image' => [
                'default' => $brand->image_default_url,
                'thumb' => $brand->image_thumb_url,
            ],
            'translations' => [],
        ];

        foreach ($brand->translations as $translation) {
            $name = $translation->name;
            $slug = $translation->slug;
            $published = (bool)$translation->published;
            $page_url = $brand->getPageUrl($translation->lang_id);
            $payload['translations'][$translation->lang_id] = compact('name', 'slug', 'published', 'page_url');
        }

        return $payload;
    }

    /**
     * @param Address $address
     * @return array
     */
    protected function transformAddress(Address $address)
    {
        $attributes = [
            "firstname" => "firstName",
            "lastname" => "lastName",
            "company" => "company",
            "address1" => "streetName",
            "address2" => "secondaryAddress",
            "postcode" => "postcode",
            "city" => "city",
            "state_code" => "stateAbbr",
            "state_name" => "state",
            "country_code" => null,
            "country_name" => null,
            "other" => "sentence",
            "phone" => "phoneNumber",
            "phone_mobile" => "phoneNumber",
            "fax" => "phoneNumber",
            "vat_number" => "taxId",
            "cf" => "taxId",
            "extrainfo" => "sentence",
        ];
        $payload = [];

        foreach ($attributes as $attribute => $fake_attribute) {

            $payload[$attribute] = ($this->is_faked and !is_null($fake_attribute)) ? $this->faker->$fake_attribute : $address->getAttribute($attribute);
        }

        return $payload;
    }

    /**
     * @param Carrier $carrier
     * @param bool $with_translations
     * @return array
     */
    protected function transformCarrier($carrier, $with_translations = false)
    {
        if (null === $carrier)
            return null;

        $payload = [
            'id' => (int)$carrier->id,
            'code' => $carrier->getTypeNameAttribute(),
            'name' => $carrier->name,
            'virtual' => (int)$carrier->virtual === 1,
        ];
        if ($with_translations) {
            unset($payload['name']);
            $payload['translations'] = [];
            foreach ($carrier->translations as $translation) {
                $payload['translations'][$translation->lang_id] = $translation->name;
            }
        }

        return $payload;
    }

    /**
     * @param Payment $payment
     * @return array
     */
    protected function transformPayment($payment, $with_translations = false)
    {
        if (null === $payment)
            return null;

        $payload = [
            'id' => (int)$payment->id,
            'code' => $payment->getModuleNameAttribute(),
            'name' => $payment->name,
            'online' => (int)$payment->online === 1,
        ];
        if ($with_translations) {
            unset($payload['name']);
            $payload['translations'] = [];
            foreach ($payment->translations as $translation) {
                $payload['translations'][$translation->lang_id] = $translation->name;
            }
        }

        return $payload;
    }

    /**
     * @param OrderDetail $orderDetail
     * @return array
     */
    protected function transformOrderDetail(OrderDetail $orderDetail)
    {
        $attributes = [
            'order_id' => 'int',
            'product_id' => 'int',
            'product_combination_id' => 'int',
            'affiliate_id' => 'int',
            'product_name' => 'string',
            'product_quantity' => 'int',
            'product_buy_price' => 'money',
            'product_sell_price_wt' => 'money',
            'product_price' => 'money',
            'product_item_price' => 'money',
            'product_item_price_tax_excl' => 'money',
            'reduction_percent' => 'int',
            'reduction_amount' => 'money',
            'reduction_amount_tax_incl' => 'money',
            'reduction_amount_tax_excl' => 'money',
            'product_ean13' => 'string',
            'product_reference' => 'string',
            'product_sap_reference' => 'string',
            'product_weight' => 'float',
            'tax_name' => 'string',
            'tax_rate' => 'float',
            'cart_rule_id' => 'int',
            'total_price_tax_incl' => 'money',
            'total_price_tax_excl' => 'money',
            'cart_price_tax_incl' => 'money',
            'cart_price_tax_excl' => 'money',
            'status' => 'string',
            'availability_mode' => 'string',
            'availability_shop_id' => 'string',
            'availability_solvable_shops' => 'array',
            'warehouse' => 'string',
            'created_at' => 'string',
            'updated_at' => 'string',
        ];
        $payload = [];

        foreach ($attributes as $attribute => $type) {
            $value = $orderDetail->getAttribute($attribute);
            $payload[$attribute] = $this->castValue($value, $type);
        }
        $payload['created_at'] = $this->transformDate($payload['created_at']);
        $payload['updated_at'] = $this->transformDate($payload['updated_at']);

        return $payload;
    }


    /**
     * @param OrderSlip $orderSlip
     * @return array
     */
    protected function transformOrderSlip(OrderSlip $orderSlip)
    {
        $payload = is_null($orderSlip) ? null : $orderSlip->toArray();
        if ($payload) {
            unset($payload['order_id']);
            $payload['created_at'] = $this->transformDate($payload['created_at']);
        }

        return $payload;
    }

    /**
     * @param Wishlist $wishlist
     * @param bool $with_products
     * @return array
     */
    protected function transformWishlist($wishlist, $with_products = false)
    {
        if (null === $wishlist)
            return null;

        $payload = [
            'id' => (int)$wishlist->id,
            'customer_id' => (int)$wishlist->customer_id,
            'code' => $wishlist->session_id,
            'name' => $wishlist->name,
            'default' => (int)$wishlist->is_default === 1,
            'created_at' => $this->transformDate($wishlist->created_at),
            'updated_at' => $this->transformDate($wishlist->updated_at),
        ];
        if ($with_products) {
            $products = $wishlist->getProducts();
            $payload['products'] = [];
            foreach ($products as $product_id) {
                $product = $this->transformProduct(Product::getPublicObj($product_id));
                if ($product) {
                    $payload['products'][] = $product;
                }
            }
        }

        return $payload;
    }

    /**
     * @param $value
     * @param $type
     * @return array|float|int|string|null|bool
     */
    protected function castValue($value, $type)
    {
        switch ($type) {

            case 'bool':
                if ($value === null or $value == '' or $value == 0)
                    return false;

                return (bool)$value;
                break;

            case 'int':
                if ($value === null or $value == '')
                    return null;

                return (int)$value;
                break;

            case 'float':
                if ($value === null or $value == '')
                    return null;

                return \Format::float($value);
                break;

            case 'money':
                if ($value === null or $value == '')
                    return null;

                return \Format::money($value);
                break;

            case 'array':
                if ($value === null or $value == '')
                    return [];

                return explode(',', $value);
                break;

            case 'string':
                return $value;
                break;
        }
        return null;
    }

    /**
     * @param CartRule $cartRule
     * @return array
     */
    protected function transformCartRule(CartRule $cartRule)
    {
        $campaign = $cartRule->getCampaignEntity();

        $data = [
            'id' => (int)$cartRule->id,
            'name' => $cartRule->name,
            'created_at' => $this->transformDate($cartRule->created_at),
            'updated_at' => $this->transformDate($cartRule->updated_at),
            'date_from' => $this->transformDate($cartRule->date_from),
            'date_to' => $this->transformDate($cartRule->date_to),
            'has_conditions' => $cartRule->conditions != '',
            'has_coupon' => (bool)$cartRule->has_coupon,
            'has_country_restriction' => (bool)$cartRule->country_restriction,
            'has_carrier_restriction' => (bool)$cartRule->carrier_restriction,
            'has_group_restriction' => (bool)$cartRule->group_restriction,
            'default_coupon' => $cartRule->has_coupon ? $cartRule->code : null,
            'campaign' => ($campaign) ? $this->transformCampaign($campaign) : null,
            'campaign_name' => ((string)$cartRule->campaign_name === '') ? null : $cartRule->campaign_name,
            'translations' => []
        ];

        foreach ($cartRule->translations as $translation) {
            $name = $translation->name;
            $description = trim(strip_tags($translation->description));
            $data['translations'][$translation->lang_id] = compact('name', 'description');
            if ($translation->lang_id === 'it')
                $data['name'] = $name;
        }

        return $data;
    }

    /**
     * @param Campaign $campaign
     * @return array
     */
    protected function transformCampaign(Campaign $campaign)
    {
        $data = [
            'id' => (int)$campaign->id,
            'name' => $campaign->name,
            'created_at' => $this->transformDate($campaign->created_at),
            'updated_at' => $this->transformDate($campaign->updated_at),
        ];
        return $data;
    }

    /**
     * @param Cart $cart
     * @param bool $detailed
     * @return array
     */
    protected function transformCart(Cart $cart, $detailed = false)
    {
        $data = [
            'item_type' => 'cart',
            'id' => (int)$cart->id,
            'lang_id' => $cart->lang_id,
            'secure_key' => ($this->is_faked) ? $this->faker->md5 : $cart->secure_key,
            'origin' => $cart->origin,
            'ip' => ($this->is_faked) ? $this->faker->ipv4 : $cart->ip,
            'coupon_code' => (string)$cart->coupon_code,
            'availability_mode' => $cart->availability_mode,
            'invoice_required' => (bool)$cart->invoice_required,
            'total_products' => isset($cart->cart_products) ? count($cart->cart_products) : 0,
            'relations' => [
                'customer_id' => $cart->customer_id > 0 ? (int)$cart->customer_id : null,
                'order_id' => is_null($cart->order) ? null : (int)$cart->order->id,
                'campaign_id' => $cart->campaign_id > 0 ? (int)$cart->campaign_id : null,
                'cart_rule_id' => $cart->cart_rule_id > 0 ? (int)$cart->cart_rule_id : null,
            ],
            'carrier' => $this->transformCarrier($cart->getCarrier()),
            'payment' => $this->transformPayment($cart->getPayment()),
            'abandoned' => $cart->isAbandoned(true),
            'recovery_url' => ($this->is_faked) ? \Link::absolute()->shortcut('cart') . '?secure_key=' . $this->faker->md5 : $cart->getRecoveryUrlAttribute(),
            'created_at' => $this->transformDate($cart->created_at),
            'updated_at' => $this->transformDate($cart->updated_at),
        ];

        if (true === $detailed) {
            $delivery_store = $cart->getDeliveryStore();
            $customer = $cart->getCustomer();
            $carrier = $cart->getCarrier();
            $payment = $cart->getPayment();
            $shipping_address = $cart->getShippingAddress();
            $billing_address = $cart->getBillingAddress();
            $campaign = $cart->getCampaign();
            $cart_rule = $cart->getCartRule();
            $data['customer'] = $customer ? $this->transformCustomer($customer) : null;
            $data['shipping_method'] = $carrier ? $this->transformCarrier($carrier) : null;
            $data['payment_method'] = $payment ? $this->transformPayment($payment) : null;
            $data['shipping_address'] = $shipping_address ? $this->transformAddress($shipping_address) : null;
            $data['billing_address'] = $billing_address ? $this->transformAddress($billing_address) : null;
            $data['delivery_store'] = $delivery_store ? $delivery_store->cd_neg : null;
            $data['campaign'] = $campaign ? $this->transformCampaign($campaign) : null;
            $data['cart_rule'] = $cart_rule ? $this->transformCartRule($cart_rule) : null;

            //amounts
            $data['currency'] = $cart->getCurrency()->iso_code;
            /*$data['amount_total'] = $order->getTotalOrderAmountAttribute();
            $data['amount_products'] = $order->getProductsAmountAttribute();
            $data['amount_shipping'] = $order->getShippingAmountAttribute();
            $data['amount_additional_costs'] = $order->getAdditionalCostsAmountAttribute();
            $data['amount_discount'] = $order->getDiscountAmountAttribute();*/


            $data['cart_products'] = [];
            $cartProducts = $cart->getProducts(false);
            foreach ($cartProducts as $cartProduct) {
                $data['cart_products'][] = $this->transformCartProduct($cartProduct);
            }
        }

        return $data;
    }

    /**
     * @param CartProduct $cartProduct
     * @return array
     */
    protected function transformCartProduct(CartProduct $cartProduct)
    {
        $attributes = [
            'cart_id' => 'int',
            'product_id' => 'int',
            'product_combination_id' => 'int',
            'affiliate_id' => 'int',
            'cart_rule_id' => 'int',
            'quantity' => 'int',
            'weight' => 'float',
            'price' => 'money',
            'reduction_amount' => 'money',
            'reduction_percent' => 'float',
            'availability_mode' => 'string',
            'availability_shop_id' => 'string',
            'availability_solvable_shops' => 'array',
            'warehouse' => 'string',
            'created_at' => 'string',
            'updated_at' => 'string',
        ];
        $payload = [];

        foreach ($attributes as $attribute => $type) {
            $value = $cartProduct->getAttribute($attribute);
            $payload[$attribute] = $this->castValue($value, $type);
        }
        $payload['cart_price'] = \Format::money(($cartProduct->reduction ? $cartProduct->price_tax_incl : $cartProduct->price));
        $payload['give_away'] = (bool)$cartProduct->special;
        $payload['created_at'] = $this->transformDate($payload['created_at']);
        $payload['updated_at'] = $this->transformDate($payload['updated_at']);

        return $payload;
    }

    /**
     * @param Product $product
     * @return array
     */
    protected function transformProduct(Product $product)
    {
        $attributes = [
            'id' => 'int',
            'sku' => 'string',
            'sap_sku' => 'string',
            'is_soldout' => 'bool',
            'is_new' => 'bool',
            'is_out_of_production' => 'bool',
            'is_outlet' => 'bool',
            'is_featured' => 'bool',
            'quantity' => 'int',
            'weight' => 'float',
            'price' => 'money',
            'created_at' => 'string',
            'updated_at' => 'string',
        ];
        $payload = [];

        foreach ($attributes as $attribute => $type) {
            $value = $product->getAttribute($attribute);
            $payload[$attribute] = $this->castValue($value, $type);
        }
        $payload['created_at'] = $this->transformDate($payload['created_at']);
        $payload['updated_at'] = $this->transformDate($payload['updated_at']);

        return $payload;
    }


    /**
     * @param Rma $rma
     * @param bool $detailed
     * @return array
     */
    protected function transformRma(Rma $rma, $detailed = false)
    {
        $data = [
            'item_type' => 'rma',
            'id' => (int)$rma->id,
            'lang_id' => $rma->lang_id,
            'secure_key' => ($this->is_faked) ? $this->faker->md5 : $rma->secure_key,
            'reference' => $rma->reference,
            'customer_id' => ($rma->customer_id > 0) ? (int)$rma->customer_id : null,
            'order_id' => ($rma->order_id > 0) ? (int)$rma->order_id : null,
            'state' => RmaState::statusToString($rma->status),
            'option' => RmaOption::statusToString($rma->option_id),
            'created_at' => $this->transformDate($rma->created_at),
            'updated_at' => $this->transformDate($rma->updated_at),
        ];

        if (true === $detailed) {
            $data['reason'] = trim(strip_tags($rma->reason));
            $data['response'] = trim(strip_tags($rma->response));
            $customer = $rma->getCustomer();
            $order = $rma->getOrder();
            $data['customer'] = $customer ? $this->transformCustomer($customer) : null;
            $data['order'] = $order ? $this->transformOrder($order) : null;
            $data['rma_details'] = [];
            /** @var RmaDetail[] $rmaDetails */
            $rmaDetails = $rma->getRmaDetails();
            foreach ($rmaDetails as $rmaDetail) {
                $data['rma_details'][] = $this->transformRmaDetail($rmaDetail);
            }
        }

        return $data;
    }

    /**
     * @param RmaDetail $rmaDetail
     * @return array
     */
    protected function transformRmaDetail(RmaDetail $rmaDetail)
    {
        $payload = $rmaDetail->toArray();
        foreach ($payload as $key => $value) {
            $payload[$key] = (int)$value;
        }

        $orderDetail = $rmaDetail->getOrderDetail();
        $payload['order_detail'] = ($orderDetail) ? $this->transformOrderDetail($orderDetail) : null;

        return $payload;
    }

    /***
     * @param string $method
     * @return string
     */
    protected function getCacheKey($method)
    {
        return "api.v1.orders.$method";
    }

    /**
     * @param Builder $query
     * @param int $page
     * @return array
     * @throws \Exception
     */
    protected function paginate(Builder $query, $page)
    {
        $total = (int)$query->count('id');
        $total_pages = 0;
        $items = [];

        if ($total > 0) {
            $total_pages = floor($total / $this->pagesize);
            if ($total % $this->pagesize > 0)
                $total_pages++;

            if ($page > $total_pages)
                throw new \Exception('Warning: "page" parameter cannot be greater than ' . $total_pages);

            $offset = ($page - 1) * $this->pagesize;

            $items = $query->take($this->pagesize)->offset($offset)->get();
        }

        return compact('total', 'total_pages', 'items');
    }
}