<?php

namespace services\Morellato\Controllers;

use Input, Json;
use BaseController;
use services\Repositories\ConfiguratorRepository;

/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 15/03/2018
 * Time: 13:29
 */
class ConfiguratorController extends BaseController
{

    /**
     * @var ConfiguratorRepository
     */
    protected $repository;

    function __construct(ConfiguratorRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct();
    }

    public function getDroplets($collection_id, $family_id)
    {
        $success = true;
        $items = $this->repository->apiDroplets($collection_id, $family_id);
        return Json::encode(compact('success', 'items'));
    }

    public function getBase($base_id)
    {
        $success = true;
        $item = $this->repository->apiBase($base_id);
        return Json::encode(compact('success', 'item'));
    }

    public function getFamilies($base_id)
    {
        $success = true;
        $items = $this->repository->apiFamilies($base_id);
        return Json::encode(compact('success', 'items'));
    }

    public function getCollections()
    {
        $success = true;
        $items = $this->repository->apiCollections();
        return Json::encode(compact('success', 'items'));
    }

    public function getCategories($collection_id)
    {
        $success = true;
        $items = $this->repository->apiCategories($collection_id);
        return Json::encode(compact('success', 'items'));
    }

    public function getBases($collection_id, $category_id)
    {
        $success = true;
        $items = $this->repository->apiBases($collection_id, $category_id);
        return Json::encode(compact('success', 'items'));
    }

    public function postSave(){
        $data = $this->repository->save(Input::all());
        return Json::encode(compact('data'));
    }

    public function getPreset($secure_key){
        $success = true;
        $item = $this->repository->apiPreset($secure_key);
        return Json::encode(compact('success', 'item'));
    }

    public function postCart($secure_key){
        $success = $this->repository->addToCart($secure_key);
        $url = \Link::shortcut('cart');
        return Json::encode(compact('success', 'url'));
    }
}