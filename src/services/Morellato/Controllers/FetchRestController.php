<?php

namespace services\Morellato\Controllers;

use Cart;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Input, Response, Request;
use BaseController;
use Order;
use Customer;
use OrderState;
use PaymentState;
use Payment;
use Carrier;
use Attribute;
use Carbon\Carbon;
use Cache;
use Rma;
use RmaOption;
use RmaState;
use services\ClerkFeed;

/**
 * POSTMAN COLLECTION
 * https://grey-spaceship-9774.postman.co/collections/7952821-dfd6151a-0a2c-4704-9b29-57b685ef3f35/publish?version=latest&workspace=b07d6d5b-745e-4ebe-8e4b-b4d9a7847d44
 *
 * /api/fetch/customer/10
 * /api/fetch/customers/2019-06-06
 * /api/fetch/order/10
 * /api/fetch/orders/2019-06-06
 * /api/fetch/orders/2012-04-05T12:42:58+02:00/2019-11-05T12:42:58+02:00?page=10
 *
 * Class OrdersRestController
 * @package services\Morellato\Controllers
 */
class FetchRestController extends OrdersRestController
{
    /**
     * @param $from
     * @param null $to
     * @return JsonResponse
     */
    function getCustomers($from, $to = null)
    {
        $from = Carbon::parse($from);
        $to = is_null($to) ? Carbon::now() : Carbon::parse($to);

        if ($to->lt($from))
            return $this->_error('Cannot find orders when END_DATE is lower than START_DATE', 400);

        $page = Input::get('page', 1);
        if (!is_numeric($page) or $page < 0)
            return $this->_error('Warning: "page" parameter must be numeric and greater than 0', 400);

        $page = (int)$page;

        $query = Customer
            ::where('updated_at', '>=', $from->format('Y-m-d H:i:s'))
            ->where('updated_at', '<=', $to->format('Y-m-d H:i:s'))
            ->orderBy('updated_at', 'desc')
            ->where('active', 1)
            ->where('deleted', 0);

        try {
            $paginate = $this->paginate($query, $page);
            $total = $paginate['total'];
            $total_pages = $paginate['total_pages'];

            /** @var Customer[] $orders */
            $customers = $paginate['items'];
        } catch (\Exception $e) {
            return $this->_error($e->getMessage(), 500);
        }

        $customer_rows = [];
        foreach ($customers as $customer) {
            $row = $this->transformCustomer($customer);
            $row['item_type'] = 'customer';
            $customer_rows[] = $row;
        }
        $data['items'] = $customer_rows;
        $data['pagination'] = [
            'page' => $page,
            'total_pages' => $total_pages,
            'total_items' => $total,
            'page_size' => $this->pagesize,
        ];
        return Response::json($data);
    }


    /**
     * Search a customer by is digest
     *
     * @param $id
     * @return JsonResponse
     */
    function getCustomer($id)
    {
        if (!is_numeric($id) or $id < 0)
            return $this->_error('Warning: "id" parameter must be numeric and greater than 0', 400);

        $customer = null;

        /** @var Customer $customer */
        $customer = Customer::where('active', 1)
            ->where('deleted', 0)->find($id);
        if ($customer) {
            $data = $this->transformCustomer($customer, true);

            return Response::json($data);
        }
        return $this->_error('Could not find any customer');
    }


    /**
     * @param $ids
     * @return JsonResponse
     */
    function getMultiplecustomer($ids)
    {
        if (!is_array($ids))
            $ids = explode(',', $ids);

        if (empty($ids))
            return $this->_error('Warning: given collection is empty', 400);

        foreach ($ids as $id) {
            if (!is_numeric($id) or $id < 0)
                return $this->_error('Warning: "id" parameter must be numeric and greater than 0', 400);
        }

        if (count($ids) > $this->multiple_size)
            return $this->_error('Warning: given collection is greater than the minimum allowed size', 400);

        $items = [];

        /** @var Customer[] $customers */
        $customers = Customer::where('active', 1)
            ->where('deleted', 0)->whereIn('id', $ids)->get();

        foreach ($customers as $customer) {
            $items[] = $this->transformCustomer($customer, true);
        }

        $payload = compact('items');
        return Response::json($payload);
    }

    /***
     * @param Builder|Order $query
     */
    protected function injectOrderFilters(&$query)
    {
        // ORDER STATUS
        $status = Input::get('status');
        if ($status) {
            $search_in = [];
            $tokens = explode(',', $status);
            foreach ($tokens as $token) {
                $id = null;
                if (is_numeric($token)) {
                    $id = $token;
                } else {
                    $id = OrderState::statusToInt($token);
                }
                if ($id > 0) {
                    $search_in[] = $id;
                }
            }
            if (!empty($search_in)) {
                $query->withStatus($search_in);
            }
        }

        // PAYMENT STATUS
        $payment_status = Input::get('payment_status');
        if ($payment_status) {
            $search_in = [];
            $tokens = explode(',', $payment_status);
            foreach ($tokens as $token) {
                $id = null;
                if (is_numeric($token)) {
                    $id = $token;
                } else {
                    $id = PaymentState::statusToInt($token);
                }
                if ($id > 0) {
                    $search_in[] = $id;
                }
            }
            if (!empty($search_in)) {
                $query->withPaymentStatus($search_in);
            }
        }

        $this->searchByCarrier($query);
        $this->searchByPayment($query);
        $this->searchByCustomer($query);
    }

    /***
     * @param Builder|Cart $query
     */
    protected function injectCartFilters(&$query)
    {
        $this->searchByCarrier($query);
        $this->searchByPayment($query);
        $this->searchByCustomer($query);
    }

    /**
     * @param Builder|Order|Cart $query
     */
    protected function searchByCarrier(&$query)
    {
        // SHIPMENT METHOD
        $carrier = Input::get('carrier');
        if ($carrier) {
            $search_in = [];
            $tokens = explode(',', $carrier);
            foreach ($tokens as $token) {
                $id = null;
                if (is_numeric($token)) {
                    $id = $token;
                } else {
                    $id = Carrier::statusToInt($token);
                }
                if ($id > 0) {
                    $search_in[] = $id;
                }
            }
            if (!empty($search_in)) {
                $query->withCarrier($search_in);
            }
        }
    }

    /**
     * @param Builder|Order|Cart $query
     */
    protected function searchByPayment(&$query)
    {
        // PAYMENT METHOD
        $payment = Input::get('payment');
        if ($payment) {
            $search_in = [];
            $tokens = explode(',', $payment);
            foreach ($tokens as $token) {
                $id = null;
                if (is_numeric($token)) {
                    $id = $token;
                } else {
                    $id = Payment::statusToInt($token);
                }
                if ($id > 0) {
                    $search_in[] = $id;
                }
            }
            if (!empty($search_in)) {
                $query->withPayment($search_in);
            }
        }
    }

    /**
     * @param Builder|Order|Cart|Rma $query
     */
    protected function searchByCustomer(&$query)
    {
        // PAYMENT METHOD
        $customer = Input::get('customer');
        if ($customer) {
            $search_in = [];
            $tokens = explode(',', $customer);
            foreach ($tokens as $token) {
                $id = null;
                if (is_numeric($token)) {
                    $id = $token;
                }
                if ($id > 0) {
                    $search_in[] = $id;
                }
            }
            if (!empty($search_in)) {
                $query->withCustomer($search_in);
            }
        }
    }

    /**
     * Get
     *
     * @param $from
     * @param null $to
     * @return JsonResponse
     */
    public function getOrders($from, $to = null)
    {
        $from = Carbon::parse($from);
        $to = is_null($to) ? Carbon::now() : Carbon::parse($to);

        //audit($from, 'FROM');
        //audit($to, 'TO');

        if ($to->lt($from))
            return $this->_error('Cannot find orders when END_DATE is lower than START_DATE', 400);

        $page = Input::get('page', 1);
        if (!is_numeric($page) or $page < 0)
            return $this->_error('Warning: "page" parameter must be numeric and greater than 0', 400);

        $page = (int)$page;

        $query = Order::with([
            'shipping_method',
            'payment_method',
        ])
            ->where('updated_at', '>=', $from->format('Y-m-d H:i:s'))
            ->where('updated_at', '<=', $to->format('Y-m-d H:i:s'))
            ->orderBy('updated_at', 'desc');

        $this->injectOrderFilters($query);

        try {
            $paginate = $this->paginate($query, $page);
            $total = $paginate['total'];
            $total_pages = $paginate['total_pages'];

            /** @var Order[] $orders */
            $orders = $paginate['items'];
        } catch (\Exception $e) {
            return $this->_error($e->getMessage(), 500);
        }

        $orders_rows = [];
        foreach ($orders as $order) {
            $orders_rows[] = $this->transformOrder($order, false);
        }
        $data['items'] = $orders_rows;
        $data['pagination'] = [
            'page' => $page,
            'total_pages' => $total_pages,
            'total_items' => $total,
            'page_size' => $this->pagesize,
        ];
        return Response::json($data);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    function getOrder($id)
    {
        if (!is_numeric($id) or $id < 0)
            return $this->_error('Warning: "id" parameter must be numeric and greater than 0', 400);

        /** @var Order $order */
        $order = Order::with(
            'customer',
            'shipping_method',
            'payment_method',
            'cart_rule',
            'campaign',
            'shipping_address',
            'billing_address'
        )->find($id);
        if ($order) {
            $payload = $this->transformOrder($order, true);
            return Response::json($payload);
        }
        return $this->_error('Cannot find any valid order, or the order has been deleted', 500);
    }

    /**
     * @param $ids
     * @return JsonResponse
     */
    function getMultipleorder($ids)
    {
        if (!is_array($ids))
            $ids = explode(',', $ids);

        if (empty($ids))
            return $this->_error('Warning: given collection is empty', 400);

        foreach ($ids as $id) {
            if (!is_numeric($id) or $id < 0)
                return $this->_error('Warning: "id" parameter must be numeric and greater than 0', 400);
        }

        if (count($ids) > $this->multiple_size)
            return $this->_error('Warning: given collection is greater than the minimum allowed size', 400);

        $items = [];

        /** @var Order[] $orders */
        $orders = Order::with(
            'customer',
            'shipping_method',
            'payment_method',
            'cart_rule',
            'campaign',
            'shipping_address',
            'billing_address'
        )
            ->whereIn('id', $ids)->get();

        foreach ($orders as $order) {
            $items[] = $this->transformOrder($order, true);
        }

        $payload = compact('items');
        return Response::json($payload);
    }


    /**
     * Get
     *
     * @param $from
     * @param null $to
     * @return JsonResponse
     */
    function getCarts($from, $to = null)
    {
        $from = Carbon::parse($from);
        $to = is_null($to) ? Carbon::now() : Carbon::parse($to);

        //audit($from, 'FROM');
        //audit($to, 'TO');

        if ($to->lt($from))
            return $this->_error('Cannot find carts when END_DATE is lower than START_DATE', 400);

        $page = Input::get('page', 1);
        if (!is_numeric($page) or $page < 0)
            return $this->_error('Warning: "page" parameter must be numeric and greater than 0', 400);

        $page = (int)$page;

        $query = Cart
            ::with('order', 'cart_products')
            ->where('updated_at', '>=', $from->format('Y-m-d H:i:s'))
            ->where('updated_at', '<=', $to->format('Y-m-d H:i:s'))
            ->orderBy('updated_at', 'desc');

        $this->injectCartFilters($query);

        try {
            $paginate = $this->paginate($query, $page);
            $total = $paginate['total'];
            $total_pages = $paginate['total_pages'];

            /** @var Cart[] $carts */
            $carts = $paginate['items'];
        } catch (\Exception $e) {
            return $this->_error($e->getMessage(), 500);
        }

        $cart_rows = [];
        foreach ($carts as $cart) {
            $cart_rows[] = $this->transformCart($cart, false);
        }
        $data['items'] = $cart_rows;
        $data['pagination'] = [
            'page' => $page,
            'total_pages' => $total_pages,
            'total_items' => $total,
            'page_size' => $this->pagesize,
        ];
        return Response::json($data);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    function getCart($id)
    {
        if (!is_numeric($id) or $id < 0)
            return $this->_error('Warning: "id" parameter must be numeric and greater than 0', 400);

        /** @var Cart $cart */
        $cart = Cart::with(
            'order',
            'cart_products',
            'customer',
            'carrier',
            'payment',
            'cart_rule',
            'campaign',
            'shipping_address',
            'billing_address'
        )->find($id);

        if ($cart) {
            $payload = $this->transformCart($cart, true);
            return Response::json($payload);
        }
        return $this->_error('Cannot find any valid cart, or the cart has been deleted', 500);
    }

    /**
     * @param $ids
     * @return JsonResponse
     */
    function getMultiplecart($ids)
    {
        if (!is_array($ids))
            $ids = explode(',', $ids);

        if (empty($ids))
            return $this->_error('Warning: given collection is empty', 400);

        foreach ($ids as $id) {
            if (!is_numeric($id) or $id < 0)
                return $this->_error('Warning: "id" parameter must be numeric and greater than 0', 400);
        }

        if (count($ids) > $this->multiple_size)
            return $this->_error('Warning: given collection is greater than the minimum allowed size', 400);

        $items = [];

        /** @var Cart[] $carts */
        $carts = Cart::with(
            'order',
            'cart_products',
            'customer',
            'carrier',
            'payment',
            'cart_rule',
            'campaign',
            'shipping_address',
            'billing_address'
        )->whereIn('id', $ids)->get();

        foreach ($carts as $cart) {
            $items[] = $this->transformCart($cart, true);
        }

        $payload = compact('items');
        return Response::json($payload);
    }


    /**
     * Get
     *
     * @param $from
     * @param null $to
     * @return JsonResponse
     */
    public function getRmas($from, $to = null)
    {
        $from = Carbon::parse($from);
        $to = $to === null ? Carbon::now() : Carbon::parse($to);

        if ($to->lt($from))
            return $this->_error('Cannot find rmas when END_DATE is lower than START_DATE', 400);

        $page = Input::get('page', 1);

        if (!is_numeric($page) or $page < 0)
            return $this->_error('Warning: "page" parameter must be numeric and greater than 0', 400);

        $page = (int)$page;

        $query = Rma
            ::where('updated_at', '>=', $from->format('Y-m-d H:i:s'))
            ->where('updated_at', '<=', $to->format('Y-m-d H:i:s'))
            ->orderBy('updated_at', 'desc');

        $this->injectRmaFilters($query);

        try {
            $paginate = $this->paginate($query, $page);
            $total = $paginate['total'];
            $total_pages = $paginate['total_pages'];

            /** @var Rma[] $rmas */
            $rmas = $paginate['items'];
        } catch (\Exception $e) {
            return $this->_error($e->getMessage(), 500);
        }

        $rmas_rows = [];
        foreach ($rmas as $rma) {
            $rmas_rows[] = [
                'item_type' => 'rma',
                'id' => (int)$rma->id,
                'lang_id' => $rma->lang_id,
                'reference' => $rma->reference,
                'customer_id' => ($rma->customer_id > 0) ? (int)$rma->customer_id : null,
                'order_id' => ($rma->order_id > 0) ? (int)$rma->order_id : null,
                'state' => RmaState::statusToString($rma->status),
                'option' => RmaOption::statusToString($rma->option_id),
                'created_at' => $this->transformDate($rma->created_at),
                'updated_at' => $this->transformDate($rma->updated_at),
            ];
        }
        $data['items'] = $rmas_rows;
        $data['pagination'] = [
            'page' => $page,
            'total_pages' => $total_pages,
            'total_items' => $total,
            'page_size' => $this->pagesize,
        ];
        return Response::json($data);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    function getRma($id)
    {
        if (!is_numeric($id) or $id < 0)
            return $this->_error('Warning: "id" parameter must be numeric and greater than 0', 400);

        /** @var Rma $rma */
        $rma = Rma::getObj($id);
        if ($rma) {
            $payload = $this->transformRma($rma, true);
            return Response::json($payload);
        }
        return $this->_error('Cannot find any valid rma, or the rma has been deleted', 500);
    }

    /**
     * @param $ids
     * @return JsonResponse
     */
    function getMultiplerma($ids)
    {
        if (!is_array($ids))
            $ids = explode(',', $ids);

        if (empty($ids))
            return $this->_error('Warning: given collection is empty', 400);

        foreach ($ids as $id) {
            if (!is_numeric($id) or $id < 0)
                return $this->_error('Warning: "id" parameter must be numeric and greater than 0', 400);
        }

        if (count($ids) > $this->multiple_size)
            return $this->_error('Warning: given collection is greater than the minimum allowed size', 400);

        $items = [];

        /** @var Rma[] $rmas */
        $rmas = Rma::whereIn('id', $ids)->get();

        foreach ($rmas as $rma) {
            $items[] = $this->transformRma($rma, true);
        }

        $payload = compact('items');
        return Response::json($payload);
    }


    /***
     * @param Builder|Rma $query
     */
    protected function injectRmaFilters(&$query)
    {
        // RMA STATUS
        $status = Input::get('status');
        if ($status) {
            $search_in = [];
            $tokens = explode(',', $status);
            foreach ($tokens as $token) {
                $id = null;
                if (is_numeric($token)) {
                    $id = $token;
                } else {
                    $id = RmaState::statusToInt($token);
                }
                if ($id > 0) {
                    $search_in[] = $id;
                }
            }
            if (!empty($search_in)) {
                $query->withStatus($search_in);
            }
        }

        // RMA OPTION
        $rma_option = Input::get('option');
        if ($rma_option) {
            $search_in = [];
            $tokens = explode(',', $rma_option);
            foreach ($tokens as $token) {
                $id = null;
                if (is_numeric($token)) {
                    $id = $token;
                } else {
                    $id = RmaOption::statusToInt($token);
                }
                if ($id > 0) {
                    $search_in[] = $id;
                }
            }
            if (!empty($search_in)) {
                $query->withRmaOption($search_in);
            }
        }

        $this->searchByOrder($query);
        $this->searchByCustomer($query);
    }

    /**
     * @param Builder|Cart|Rma $query
     */
    protected function searchByOrder(&$query)
    {
        // ORDER
        $order = Input::get('order');
        if ($order) {
            $search_in = [];
            $tokens = explode(',', $order);
            foreach ($tokens as $token) {
                $id = null;
                if (is_numeric($token)) {
                    $id = $token;
                }
                if ($id > 0) {
                    $search_in[] = $id;
                }
            }
            if (!empty($search_in)) {
                $query->withOrder($search_in);
            }
        }
    }

    /***
     * @param string $method
     * @return string
     */
    protected function getCacheKey($method)
    {
        return "api.v1.polling.$method";
    }
}