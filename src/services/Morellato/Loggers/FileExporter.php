<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 22/02/2017
 * Time: 13:35
 */

namespace services\Morellato\Loggers;

use DB, Utils, Config, Exception, Cache;
use Illuminate\Console\Command;
use services\Morellato\Loggers\DatabaseLogger as Logger;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;
use services\Ftp\Connection;
use View;

class FileExporter
{

    protected $console;
    protected $logger;
    protected $files;
    protected $ftp;

    protected $path;
    protected $config;

    function __construct(Command $console, Logger $logger, Filesystem $files, Connection $ftp)
    {
        $this->console = $console;
        $this->logger = $logger;
        $this->files = $files;
        $this->ftp = $ftp;
        $this->path = storage_path('morellato/csv/logs/');
        $default = Config::get('ftp.default');
        $this->config = Config::get('ftp.connections.'.$default);
    }

    function make(){
        $today = date('Y-m-d');
        $rows = DB::table('mi_logs')->where(DB::raw('DATE(created_at)'),$today)->where('verbose','error')->get();
        $many = count($rows);
        $this->console->line("Exporting [$many] errors...");
        $errors = [];
        foreach($rows as $row){
            $line = new \stdClass();
            $line->date = $row->created_at;
            $line->type = $row->type;
            $line->message = $row->message;
            $errors[] = $line;
        }
        $content = View::make('xml.morellato.errors',compact('errors'))->render();
        $file = 'errors_' . date('Ymd_Hi') . '.xml';
        $logFilename = $this->path . $file;
        $this->files->put($logFilename, $content);
        $this->console->info("Log file saved as [$logFilename]");
        try{
            $this->ftp->connect();
            $this->ftp->upload($logFilename, Config::get('ftp.remoteFtpLogsPath').'/'.$file);
            $this->console->info("Log file has been successfully uploaded into FTP");
        }catch (Exception $e){
            audit_error($e->getMessage(), __METHOD__);
        }

    }

}