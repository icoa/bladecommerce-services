<?php


namespace services\Morellato\Loggers;

use File;
use Exception;
use Illuminate\Database\Eloquent\Model;

class EloquentLogger
{
    /**
     * @var string
     */
    protected $root;

    /**
     * @var string
     */
    protected $logFile;

    /**
     * @var array
     */
    protected $lines = [];

    /**
     * @var Model
     */
    protected $model;


    /**
     * @return Model
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param Model $model
     * @return $this
     */
    public function setModel($model)
    {
        $this->model = $model;
        unset($this->logFile);
        return $this;
    }

    /**
     * @return string
     */
    protected function getDomainName()
    {
        $name = class_basename($this->getModel());
        return $name;
    }

    /**
     * @return string
     */
    protected function getLogFileName()
    {
        $model = $this->getModel();
        return isset($model) ? (string)$model->id : '';
    }

    /**
     * @param $line
     * @param $flush
     * @return $this
     */
    protected function addLine($line, $flush = true)
    {
        $this->lines[] = $line;

        if ($flush)
            $this->flush();

        return $this;
    }

    /**
     * @param $var
     * @param null $message
     */
    function log($var, $message = null)
    {
        $line = $this->getLine($var, $message);
        $this->addLine($line);
    }

    /**
     * @param $var
     * @param null $message
     */
    function error($var, $message = null)
    {
        $line = $this->getLine($var, $message, 'ERROR');
        $this->addLine($line);
    }

    /**
     * @param $var
     * @param null $message
     */
    function info($var, $message = null)
    {
        $this->log($var, $message);
    }

    /**
     * @param $var
     * @param null $message
     */
    function warning($var, $message = null)
    {
        $line = $this->getLine($var, $message, 'WARNING');
        $this->addLine($line);
    }

    /**
     * @param $var
     * @param null $message
     */
    function success($var, $message = null)
    {
        $line = $this->getLine($var, $message, 'SUCCESS');
        $this->addLine($line);
    }

    /**
     * @param $var
     * @param null $message
     * @param string $type
     * @return string
     */
    private function getLine($var, $message = null, $type = 'INFO')
    {
        if (is_object($var) and method_exists($var, 'toArray')) {
            $var = $var->toArray();
        }
        if (is_array($var) or is_object($var)) {
            $var = print_r($var, 1);
        }
        $line = '[' . date('Y-m-d H:i:s') . '] (' . $type . ') ';
        if ($message) {
            $line .= "$message => ";
        }
        $line .= $var;
        return $line;
    }


    protected function setup()
    {
        if (!isset($this->root)) {
            $domainName = $this->getDomainName();
            $root = storage_path('logs/morellato/' . $domainName . '/');
            if (!File::isDirectory($root)) {
                try {
                    File::makeDirectory($root, 765);
                } catch (Exception $e) {
                    audit_exception($e, __METHOD__);
                    return;
                }
            }
            $this->root = $root;
        }

        if (!isset($this->logFile)) {
            $logFileName = $this->getLogFileName();
            if (strlen($logFileName) == 0)
                return;

            $this->logFile = $this->root . $logFileName . '.log';
        }
    }


    function flush()
    {
        try {
            $this->setup();
            if(isset($this->logFile))
                File::append($this->logFile, implode($this->lines, PHP_EOL) . PHP_EOL);
        } catch (Exception $e) {
            audit_exception($e, __METHOD__);
            return;
        }
        $this->lines = [];
    }
}