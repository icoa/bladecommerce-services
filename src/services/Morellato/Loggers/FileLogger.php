<?php


namespace services\Morellato\Loggers;

use File;
use Exception;

class FileLogger
{
    protected $logFile;
    protected $errorFile;
    protected $rotate = false;

    function __construct()
    {
        $root = storage_path('logs/morellato/');
        if (!File::isDirectory($root)) {
            try {
                File::makeDirectory($root, 765);
            } catch (Exception $e) {
                audit_exception($e, __METHOD__);
            }
        }
        $this->logFile = $root . (($this->rotate) ? 'log-' . date('Y-m-d') . '.log' : 'log.log');
        $this->errorFile = $root . (($this->rotate) ? 'error-' . date('Y-m-d') . '.log' : 'error.log');
    }

    /**
     * @param $var
     * @param null $message
     * @param null $outer_id
     */
    function log($var, $message = null, $outer_id = null)
    {
        $line = $this->getLine($var, $message, $outer_id);
        try {
            File::append($this->logFile, $line);
        } catch (Exception $e) {
            audit_exception($e, __METHOD__);
        }
    }

    /**
     * @param $var
     * @param null $message
     * @param null $outer_id
     */
    function error($var, $message = null, $outer_id = null)
    {
        $line = $this->getLine($var, $message, $outer_id);
        try {
            File::append($this->errorFile, $line);
        } catch (Exception $e) {
            audit_exception($e, __METHOD__);
        }
    }

    /**
     * @param $var
     * @param null $message
     * @param null $outer_id
     */
    function info($var, $message = null, $outer_id = null)
    {
        $this->log($var, $message, $outer_id);
    }

    /**
     * @param $var
     * @param null $message
     * @param null $outer_id
     */
    function warning($var, $message = null, $outer_id = null)
    {
        $this->error($var, $message, $outer_id);
    }

    /**
     * @param $var
     * @param null $message
     * @param null $outer_id
     * @return string
     */
    private function getLine($var, $message = null, $outer_id = null)
    {
        if (is_array($var) or is_object($var)) {
            $var = print_r($var, 1);
        }
        $line = '[' . date('Y-m-d H:i:s') . '] ';
        if ($message) {
            $line .= "($message)=|> ";
        }
        if ($outer_id) {
            $line .= "OUTER {$outer_id}=|> ";
        }
        $line .= $var . PHP_EOL;
        return $line;
    }
}