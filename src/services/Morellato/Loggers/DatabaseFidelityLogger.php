<?php

namespace services\Morellato\Loggers;

use DB;

class DatabaseFidelityLogger extends DatabaseLogger
{

    protected $table = 'mi_fidelity_logs';

}