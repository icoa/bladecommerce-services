<?php

namespace services\Morellato\Loggers;

use DB;

class DatabaseLogger
{

    protected $table = 'mi_logs';
    protected $enabled = false;

    function __construct()
    {
        $this->enabled = config('logging.importer.enabled', false);
    }

    /**
     * @param $var
     * @param null $message
     * @param null $outer_id
     */
    function log($var, $message = null, $outer_id = null)
    {
        $line = $this->getLine($var, $message, $outer_id);
        $line['verbose'] = 'info';
        $this->insert($line);
    }

    /**
     * @param $var
     * @param null $message
     * @param null $outer_id
     */
    function info($var, $message = null, $outer_id = null)
    {
        $this->log($var, $message, $outer_id);
    }

    /**
     * @param $var
     * @param null $message
     * @param null $outer_id
     */
    function warning($var, $message = null, $outer_id = null)
    {
        $line = $this->getLine($var, $message, $outer_id);
        $line['verbose'] = 'warning';
        $this->insert($line);
    }

    /**
     * @param $var
     * @param null $message
     * @param null $outer_id
     */
    function error($var, $message = null, $outer_id = null)
    {
        $line = $this->getLine($var, $message, $outer_id);
        $line['verbose'] = 'error';
        $this->insert($line);
    }

    /**
     * @param $line
     */
    protected function insert($line)
    {
        if ($this->enabled === false)
            return;

        DB::table($this->table)->insert($line);
    }

    /**
     * @param $var
     * @param null $type
     * @param null $outer_id
     * @return array
     */
    protected function getLine($var, $type = null, $outer_id = null)
    {
        if ($this->enabled === false)
            return [];

        if (is_array($var) or is_object($var)) {
            $var = print_r($var, 1);
        }

        $data = [
            'outer_id' => $outer_id,
            'message' => $var,
            'type' => $type,
            'created_at' => date('Y-m-d H:i:s'),
        ];

        return $data;
    }
}