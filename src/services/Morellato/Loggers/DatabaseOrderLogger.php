<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 08/02/2017
 * Time: 13:18
 */

namespace services\Morellato\Loggers;

use DB;

class DatabaseOrderLogger extends DatabaseLogger
{

    protected $table = 'mi_orders_logs';

}