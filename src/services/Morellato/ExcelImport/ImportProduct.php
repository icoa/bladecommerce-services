<?php
/**
 * Created by PhpStorm.
 * User: Akshay.D
 * Date: 26-04-2017
 * Time: 18:44
 */

namespace services\Morellato\ExcelImport;

use DB, Utils, Config, Exception, App;
use Illuminate\Support\Facades\Schema;
use Product, ProductImage;
use Illuminate\Console\Command;
use services\Morellato\Loggers\DatabaseLogger as Logger;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel as Excel;

class ImportProduct
{
    protected $console;
    protected $filePath;
    protected $bot_user_id = 1;
    protected $product_id;
    protected $brand;
    protected $brand_id;
    protected $attribute;
    protected $attribute_id = [];
    protected $attribute_option_id = [];
    protected $collection;
    protected $collection_id;
    protected $sku;
    protected $ean;
    protected $is_set_products_attributes_position = 0;


    protected $failed_excel_rows = array();

    function __construct(Command $console)
    {
        $this->console = $console;
    }

    public function import($filePath)
    {
        $this->filePath = $filePath;
        try {

            Excel::load($this->filePath, function ($reader) use (&$excel) {
                $objExcel = $reader->getExcel();
                $sheet = $objExcel->getSheet(0);
                $highestRow = $sheet->getHighestRow();
                $highestColumn = $sheet->getHighestColumn();

                for ($row = 4; $row <= $highestRow; $row++) {
                    try {
                        $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                        $data = $rowData[0];
                        if ($data[0] != '' && $data[0] != null) {
//                            $this->console->comment("Product Brand [$data[0]], Style [$data[1]], EAN [$data[2]] ");
                            $is_record_exist = DB::table('product_import_xl')
                                ->where('brand', $data[0])
                                ->where('style', $data[1])
                                ->where('ean', $data[2])
                                ->first();

                            if (!$is_record_exist) {
                                $data = [
                                    'brand' => $data[0],
                                    'style' => $data[1],
                                    'ean' => $data[2],
                                    'season' => $data[3],
                                    'platform' => $data[4],
                                    'gender' => $data[5],
                                    'case_size' => $data[6],
                                    'case_material' => $data[7],
                                    'case_finish' => $data[8],
                                    'case_color' => $data[9],
                                    'case_shape' => $data[10],
                                    'case_thickness' => $data[11],
                                    'crystal_lens_color' => $data[12],
                                    'dial_color' => $data[13],
                                    'top_ring_material' => $data[14],
                                    'top_ring_finish' => $data[15],
                                    'top_ring_color' => $data[16],
                                    'lug_width' => $data[17],
                                    'stones' => $data[18],
                                    'attachment_material' => $data[19],
                                    'attachment_finish' => $data[20],
                                    'attachment_color' => $data[21],
                                    'case_atm' => $data[22],
                                    'function_1' => $data[23],
                                    'sap_desc' => $data[24],
                                ];
                                DB::table('product_import_xl')->insert($data);
//                                $this->console->info("Record created successfully");
                            } else {
//                                $this->console->error('Record already exist');
                            }
                        }
                    } catch (\Exception $e) {
                        $this->failed_excel_rows[] = $row;
                        $this->console->error("Failed to insert record for row : $row");
                    }
                }
                DB::commit();

                $this->console->info("All record stored to temporary table successfully");
                $this->console->info("Now inserting data to main tables");
                $temp_data = DB::table('product_import_xl')->get();
                foreach ($temp_data as $data) {
                    $this->attribute_id = [];
                    $this->attribute_option_id = [];
                    $this->brand = $data->brand;
                    $this->sku = $data->style;
                    $this->ean = $data->ean;
                    $this->collection = $data->platform;
                    $this->makeBrand();
                    $this->makeCollection();
                    $this->makeAttribute($data);
                    $this->makeProduct($data);
                    $this->console->comment("******************************************************************");
                }
            });
        } catch (\Exception $e) {
            $this->console->error($e->getMessage());
        }
    }

    protected function nullish($str)
    {
        $str = trim($str);
        return ($str == '' or is_null($str) or $str == 'null' or $str == '.' or str_contains(Str::upper($str), 'TO BE DEFINED'));
    }

    protected function sanitize($str)
    {
        $str = str_replace(['\\', ':', ';', '#', '?'], '', trim($str));
        return $str;
    }

    protected function setBlame(&$data)
    {
        $data['created_by'] = $this->bot_user_id;
        $data['updated_by'] = $this->bot_user_id;
    }

    protected function languages()
    {
        return \Core::getLanguages();
    }

    protected function explodeString($str, $delimiter = ',')
    {
        $data = [];
        $tokens = explode($delimiter, $str);
        foreach ($tokens as $token) {
            $token = trim($token);
            if (!$this->nullish($token))
                $data[] = trim($token);
        }
        return $data;
    }

    protected function getAttributeIdByCode($code)
    {
        return \Attribute::where('code', $code)->first()->id;
    }

    public function makeBrand()
    {
        $brand = $this->sanitize($this->brand);
        $id = $this->resolveBrand($brand);
        if (is_null($id)) {
            $this->console->info("Creating new brand: [$brand]");
            $id = $this->createBrand($brand);
        }
        $this->brand_id = $id;
    }

    protected function resolveBrand($name)
    {
        $lower = Str::lower($name);

        if ($lower == 'sector gioielli')
            $name = 'Sector';

        if ($lower == 'ice watch' or $lower == 'ice watches')
            $name = 'Ice-Watch';

        if ($lower == 'skagen')
            $name = 'Skagen Denmark';

        $slug = Str::slug($name);
        $id = DB::table("brands_lang")->where("slug", $slug)->pluck("brand_id");
        return $id > 0 ? $id : null;
    }

    protected function createBrand($name)
    {
        $name = ucfirst(Str::lower($name));
        $data = [];
        $this->setBlame($data);
        foreach ($this->languages() as $language) {
            $data[$language] = [
                'name' => $name,
                'slug' => Str::slug($name),
                'published' => 1,
            ];
        }
        $model = new \Brand();
        $model->make($data);
        $this->console->info("Created new brand [$name]");
        return $model->id;
    }

    public function makeCollection()
    {
        $name = $this->sanitize($this->collection);
        $brand = $this->sanitize($this->brand);
        $id = $this->resolveCollection($name);
        if (is_null($id)) {
            $this->console->info("Creating new collection: [$name]");
            $id = $this->createCollection($name, $brand);
        }
        $this->collection_id = $id;
    }

    protected function resolveCollection($name)
    {
        $slug = Str::slug($name);

        $id = DB::table("collections_lang")
            ->join('collections', 'collections.id', '=', 'collections_lang.collection_id')
            ->where("slug", $slug)
            ->where('brand_id', $this->brand_id)
            ->pluck("id");

        return $id > 0 ? $id : null;
    }

    protected function createCollection($name, $brand)
    {
        $name = ucfirst(Str::lower($name));
        $data = [
            'brand_id' => $this->brand_id,
        ];
        $this->setBlame($data);
        foreach ($this->languages() as $language) {
            $data[$language] = [
                'name' => $name,
                'slug' => Str::slug($name),
                'published' => 1,
            ];
        }
        $model = new \Collection();
        $model->make($data);
        $this->console->info("Created new collection [$name] for brand [$brand]");
        return $model->id;
    }

    public function makeAttribute($data)
    {
        /*
         * gender [GENERE]
         * Attribute id = 214
         */
        $gender = Str::lower($data->gender);
        $attr_gender_id = $this->getAttributeIdByCode('gender');
        $this->attribute_id[] = $attr_gender_id;
        if (trim($gender) != '') {
            $gender_id = $this->resolveAttributeOption($gender, $attr_gender_id);
            if (!$gender_id) {
//                $this->console->comment("Gender [$gender] should be created");
                $gender_id = $this->createGender($gender, $attr_gender_id);
//                $this->console->info("New Gender attribute option created with id: [$gender_id]");
            } else {
//                $this->console->info("Gender option already exist [$gender]");
            }
            $this->attribute_option_id[$attr_gender_id][] = $gender_id;
        } else {
            $this->console->comment("Gender option with empty value [$gender]");
        }

        /*
         * case material [MATERIALE CASSA]
         * Attribute id = 2
         */
        $attr_case_material_id = $this->getAttributeIdByCode('case_material');
        $this->attribute_id[] = $attr_case_material_id;
        $case_material = Str::lower($data->case_material);
        if (trim($case_material) != '') {
            $case_material_data = $this->explodeString($case_material);
            $case_material_data = array_unique($case_material_data);
            foreach ($case_material_data as $name) {
                $case_material_option_id = $this->resolveAttributeOption($name, $attr_case_material_id);
                if (!$case_material_option_id) {
//                    $this->console->comment("Case material [$name] should be created");
                    $case_material_option_id = $this->createAttributeOption($name, $attr_case_material_id);
//                    $this->console->info("New Case material attribute option created with id: [$case_material_option_id]");
                } else {
//                    $this->console->info("Case material option already exist [$name] [$case_material_option_id]");
                }
                $this->attribute_option_id[$attr_case_material_id][] = $case_material_option_id;
            }
        } else {
            $this->console->comment("Case material option with empty value [$case_material]");
        }

        /*
         * CASE FINISHING [FINITURA CASSA]
         * Attribute id = 69
         */
        $attr_case_finishing_id = $this->getAttributeIdByCode('finishing');
        $this->attribute_id[] = $attr_case_finishing_id;
        $case_finishing = Str::lower($data->case_finish);
        if (trim($case_finishing) != '') {
            $case_finishing_option_id = $this->resolveAttributeOption($case_finishing, $attr_case_finishing_id);
            if (!$case_finishing_option_id) {
//                $this->console->comment("Case finishing [$case_finishing] should be created");
                $case_finishing_option_id = $this->createAttributeOption($case_finishing, $attr_case_finishing_id);
//                $this->console->info("New Case finishing attribute option created with id: [$case_finishing_option_id]");
            } else {
//                $this->console->info("Case finishing option already exist [$case_finishing] [$case_finishing_option_id]");
            }
            $this->attribute_option_id[$attr_case_finishing_id][] = $case_finishing_option_id;
        } else {
            $this->console->comment("Case finishing option with empty value [$case_finishing]");
        }

        /*
         * CASE COLOR [COLORE CASSA]
         * Attribute id = 9
         */
        $attr_case_color_id = $this->getAttributeIdByCode('case_colour');
        $this->attribute_id[] = $attr_case_color_id;
        $case_color = Str::lower($data->case_color);
        if (trim($case_color) != '') {
            $case_color_data = $this->explodeString($case_color, '/');
            $case_color_data = array_unique($case_color_data);
            foreach ($case_color_data as $name) {
                $case_color_option_id = $this->resolveAttributeOption($name, $attr_case_color_id);
                if (!$case_color_option_id) {
//                    $this->console->comment("Case color [$name] should be created");
                    $case_color_option_id = $this->createAttributeOption($name, $attr_case_color_id);
//                    $this->console->info("New Case color attribute option created with id: [$case_color_option_id]");
                } else {
//                    $this->console->info("Case color option already exist [$name] [$case_color_option_id]");
                }
                $this->attribute_option_id[$attr_case_color_id][] = $case_color_option_id;
            }
        } else {
            $this->console->comment("Case color option with empty value [$case_color]");
        }

        /*
         * CASE FORM [FORMA CASSA]
         * Attribute id = 244
         */
        $attr_case_shape_id = $this->getAttributeIdByCode('forma');
        $this->attribute_id[] = $attr_case_shape_id;
        $case_shape = Str::lower($data->case_shape);
        if (trim($case_shape) != '') {
            $case_shape_data = $this->explodeString($case_shape, '/');
            $case_shape_data = array_unique($case_shape_data);
            foreach ($case_shape_data as $name) {
                $case_shape_option_id = $this->resolveAttributeOption($name, $attr_case_shape_id);
                if (!$case_shape_option_id) {
//                    $this->console->comment("Case shape [$name] should be created");
                    $case_shape_option_id = $this->createAttributeOption($name, $attr_case_shape_id);
//                    $this->console->info("New Case shape attribute option created with id: [$case_shape_option_id]");
                } else {
//                    $this->console->info("Case shape option already exist [$name] [$case_shape_option_id]");
                }
                $this->attribute_option_id[$attr_case_shape_id][] = $case_shape_option_id;
            }
        } else {
            $this->console->comment("Case shape option with empty value [$case_shape]");
        }

        /*
        * GLASS COLOR [COLORE VETRO]
        * Attribute id = 21
        */
        $attr_glass_color_id = $this->getAttributeIdByCode('color');
        $this->attribute_id[] = $attr_glass_color_id;
        $glass_color = Str::lower($data->crystal_lens_color);
        if (trim($glass_color) != '') {
            $glass_color_option_id = $this->resolveAttributeOption($glass_color, $attr_glass_color_id);
            if (!$glass_color_option_id) {
//                $this->console->comment("Glass color [$glass_color] should be created");
                $glass_color_option_id = $this->createAttributeOption($glass_color, $attr_glass_color_id);
//                $this->console->info("New Glass color attribute option created with id: [$glass_color_option_id]");
            } else {
//                $this->console->info("Glass color option already exist [$glass_color] [$glass_color_option_id]");
            }
            $this->attribute_option_id[$attr_glass_color_id][] = $glass_color_option_id;
        } else {
            $this->console->comment("Glass color option with empty value [$glass_color]");
        }

        /*
         * COLOR QUADRANT [COLORE QUADRANTE]
         * Attribute id = 10
         */
        $attr_color_quadrant_id = $this->getAttributeIdByCode('dial_colour');
        $this->attribute_id[] = $attr_color_quadrant_id;
        $color_quadrant = Str::lower($data->dial_color);
        if (trim($color_quadrant) != '') {
            $color_quadrant_data = $this->explodeString($color_quadrant, '/');
            $color_quadrant_data = array_unique($color_quadrant_data);
            foreach ($color_quadrant_data as $name) {
                $color_quadrant_option_id = $this->resolveAttributeOption($name, $attr_color_quadrant_id);
                if (!$color_quadrant_option_id) {
//                    $this->console->comment("Color quadrant [$name] should be created");
                    $color_quadrant_option_id = $this->createAttributeOption($name, $attr_color_quadrant_id);
//                    $this->console->info("New Color quadrant attribute option created with id: [$color_quadrant_option_id]");
                } else {
//                    $this->console->info("Color quadrant option already exist [$name] [$color_quadrant_option_id]");
                }
                $this->attribute_option_id[$attr_color_quadrant_id][] = $color_quadrant_option_id;
            }
        } else {
            $this->console->comment("Color quadrant option with empty value [$color_quadrant]");
        }

        /*
         * Top ring finish [FINITURA GHIERA]
         * Attribute id = 69
         */
        $attr_top_ring_finish_id = $this->getAttributeIdByCode('finishing');
        $this->attribute_id[] = $attr_top_ring_finish_id;
        $top_ring_finish = Str::lower($data->top_ring_finish);
        if (trim($top_ring_finish) != '') {
            $top_ring_finish_data = $this->explodeString($top_ring_finish);
            $top_ring_finish_data = array_unique($top_ring_finish_data);
            foreach ($top_ring_finish_data as $name) {
                $top_ring_finish_option_id = $this->resolveAttributeOption($name, $attr_top_ring_finish_id);
                if (!$top_ring_finish_option_id) {
//                    $this->console->comment("Top ring finish [$name] should be created");
                    $top_ring_finish_option_id = $this->createAttributeOption($name, $attr_top_ring_finish_id);
//                    $this->console->info("New Top ring finish attribute option created with id: [$top_ring_finish_option_id]");
                } else {
//                    $this->console->info("Top ring finish option already exist [$name] [$top_ring_finish_option_id]");
                }
                $this->attribute_option_id[$attr_top_ring_finish_id][] = $top_ring_finish_option_id;
            }
        } else {
            $this->console->comment("Top ring finish option with empty value [$top_ring_finish]");
        }

        /*
         * STONES [PIETRE]
         * Attribute id = 223
         */
        $attr_stones_id = $this->getAttributeIdByCode('stones');
        $this->attribute_id[] = $attr_stones_id;
        $stones = Str::lower($data->stones);
        if (trim($stones) != '') {
            $stone_option_id = $this->resolveAttributeOption($stones, $attr_stones_id);
            if (!$stone_option_id) {
//                $this->console->comment("Stone [$stones] should be created");
                $stone_option_id = $this->createAttributeOption($stones, $attr_stones_id);
//                $this->console->info("New Stone attribute option created with id: [$stone_option_id]");
            } else {
//                $this->console->info("Stone option already exist [$stones] [$stone_option_id]");
            }
            $this->attribute_option_id[$attr_stones_id][] = $stone_option_id;
        } else {
//            $this->console->comment("Stone option with empty value [$stones]");
        }

        /*
         * Cutting Material [MATERIALE CINTURINO]
         * Attribute id = 4
         */
        $attr_cutting_material_id = $this->getAttributeIdByCode('strap_material');
        $this->attribute_id[] = $attr_cutting_material_id;
        $cutting_material = Str::lower($data->attachment_material);
        if (trim($cutting_material) != '') {
            $cutting_material_data = $this->explodeString($cutting_material);
            $cutting_material_data = array_unique($cutting_material_data);
            foreach ($cutting_material_data as $name) {
                $cutting_material_option_id = $this->resolveAttributeOption($name, $attr_cutting_material_id);
                if (!$cutting_material_option_id) {
//                    $this->console->comment("Cutting material [$name] should be created");
                    $cutting_material_option_id = $this->createAttributeOption($name, $attr_cutting_material_id);
//                    $this->console->info("New Cutting material attribute option created with id: [$cutting_material_option_id]");
                } else {
//                    $this->console->info("Cutting material option already exist [$name] [$cutting_material_option_id]");
                }
                $this->attribute_option_id[$attr_cutting_material_id][] = $cutting_material_option_id;
            }
        } else {
            $this->console->comment("Cutting material option with empty value [$cutting_material]");
        }

        /*
         * Finishing Belt [FINITURA CINTURINO]
         * Attribute id = 69
         */
        $attr_finishing_belt_id = $this->getAttributeIdByCode('finishing');
        $this->attribute_id[] = $attr_finishing_belt_id;
        $finishing_belt = Str::lower($data->attachment_finish);
        if (trim($finishing_belt) != '') {
            $finishing_belt_data = $this->explodeString($finishing_belt);
            $finishing_belt_data = array_unique($finishing_belt_data);
            foreach ($finishing_belt_data as $name) {
                $finishing_belt_option_id = $this->resolveAttributeOption($name, $attr_finishing_belt_id);
                if (!$finishing_belt_option_id) {
//                    $this->console->comment("Finishing belt [$name] should be created");
                    $finishing_belt_option_id = $this->createAttributeOption($name, $attr_finishing_belt_id);
//                    $this->console->info("New Finishing belt attribute option created with id: [$finishing_belt_option_id]");
                } else {
//                    $this->console->info("Finishing belt option already exist [$name] [$finishing_belt_option_id]");
                }
                $this->attribute_option_id[$attr_finishing_belt_id][] = $finishing_belt_option_id;
            }
        } else {
            $this->console->comment("Finishing belt option with empty value [$finishing_belt]");
        }

        /*
         * STRAP COLOR [COLORE CINTURINO]
         * Attribute id = 6
         */
        $attr_strap_color_id = $this->getAttributeIdByCode('strap_colour');
        $this->attribute_id[] = $attr_strap_color_id;
        $strap_color = Str::lower($data->attachment_color);
        if (trim($strap_color) != '') {
            $strap_color = str_replace("/", ",", $strap_color);
            $strap_color_data = $this->explodeString($strap_color);
            $strap_color_data = array_unique($strap_color_data);
            foreach ($strap_color_data as $name) {
                $strap_color_option_id = $this->resolveAttributeOption($name, $attr_strap_color_id);
                if (!$strap_color_option_id) {
//                    $this->console->comment("Strap color [$name] should be created");
                    $strap_color_option_id = $this->createAttributeOption($name, $attr_strap_color_id);
//                    $this->console->info("New Strap color attribute option created with id: [$strap_color_option_id]");
                } else {
//                    $this->console->info("Strap color option already exist [$name] [$strap_color_option_id]");
                }
                $this->attribute_option_id[$attr_strap_color_id][] = $strap_color_option_id;
            }
        } else {
            $this->console->comment("Strap color option with empty value [$strap_color]");
        }

        /*
         * Movement / Functions [MOVIMENTO / FUNZIONI]
         * Attribute id
         * Movement = 1
         * Functions = 217
         */
        $attr_movement_id = $this->getAttributeIdByCode('movement');
        $attr_functions_id = $this->getAttributeIdByCode('functions');
        $this->attribute_id[] = $attr_movement_id;
        $this->attribute_id[] = $attr_functions_id;
        $mv_fun = Str::lower($data->function_1);
        if (trim($mv_fun) != '') {
            $mv_fun = $this->explodeString($mv_fun, '/'); // Separate movement and functions
            $movement_data = $this->explodeString($mv_fun[0]); //Separate movement options
            $movement_data = array_unique($movement_data);
            foreach ($movement_data as $name) {
                $movement_option_id = $this->resolveAttributeOption($name, $attr_movement_id);
                if (!$movement_option_id) {
//                    $this->console->comment("Movement [$name] should be created");
                    $movement_option_id = $this->createAttributeOption($name, $attr_movement_id);
//                    $this->console->info("New Movement attribute option created with id: [$movement_option_id]");
                } else {
//                    $this->console->info("Movement option already exist [$name] [$movement_option_id]");
                }
                $this->attribute_option_id[$attr_movement_id][] = $movement_option_id;
            }

            if (count($mv_fun) >= 2) {
                $functions_data = $this->explodeString($mv_fun[1]); //Separate functions options
                $functions_data = array_unique($functions_data);
                foreach ($functions_data as $name) {
                    $function_option_id = $this->resolveAttributeOption($name, $attr_functions_id);
                    if (!$function_option_id) {
//                        $this->console->comment("Function [$name] should be created");
                        $function_option_id = $this->createAttributeOption($name, $attr_functions_id);
//                        $this->console->info("New Function attribute option created with id: [$function_option_id]");
                    } else {
//                        $this->console->info("Function option already exist [$name] [$function_option_id]");
                    }
                    $this->attribute_option_id[$attr_functions_id][] = $function_option_id;
                }
            }
        } else {
            $this->console->comment("Movement / Function option with empty value [$mv_fun]");
        }
    }

    protected function resolveAttributeOption($name, $attribute_id)
    {
        $slug = Str::slug(trim($name));
        $attribute_option_id = DB::table('attributes_options_lang')
            ->join('attributes_options', 'attributes_options.id', '=', 'attributes_options_lang.attribute_option_id')
            ->where('attribute_id', $attribute_id)
            ->where('slug', $slug)
            ->pluck("attribute_option_id");

        return $attribute_option_id;
    }

    protected function createGender($gender, $attr_gender_id)
    {
        $name = ucfirst(Str::lower($gender));
        $attribute_id = $attr_gender_id;
        $nav_id = \AttributeOption::where('attribute_id', $attribute_id)->max('nav_id') + 1;
        $uname = $name[0];
        $uslug = Str::lower($name[0]);

        $data = compact('attribute_id', 'nav_id', 'uname', 'uslug');

        $this->setBlame($data);
        foreach ($this->languages() as $language) {
            $data[$language] = [
                'name' => $name,
                'slug' => Str::slug(Str::lower($name)),
                'published' => 1,
            ];
        }
        $model = new \AttributeOption();
        $model->make($data);
        $id = $model->id;
        return $id;
    }

    protected function createAttributeOption($name, $attribute_id)
    {
        $uname = ucfirst(Str::lower($name));
        $uslug = Str::slug(Str::lower($name));
        $data = compact('attribute_id', 'uname', 'uslug');
        $this->setBlame($data);

        foreach ($this->languages() as $language) {
            $data[$language] = [
                'name' => $uname,
                'slug' => $uslug,
                'published' => 1,
            ];
        }

        $model = new \AttributeOption();
        $model->make($data);
        $id = $model->id;
        return $id;
    }

    public function makeProduct($data)
    {
        $this->attribute_id = array_unique($this->attribute_id);
        $unique_attribute_ids_string = implode(',', $this->attribute_id);

//        $this->console->info("Brand name [$this->brand] id [$this->brand_id]");
//        $this->console->info("Collection name [$this->collection] id [$this->collection_id]");
//        $this->console->info("Attribute id's [$unique_attribute_ids_string]");

        $this->product_id = $this->checkProductExistence();
        if ($this->product_id) {
            $this->console->error("Product already exist for brand: [$this->brand], collection: [$this->collection], sku: [$this->sku], ean: [$this->ean] with product id: [$this->product_id]");
            $this->importProductAttributes($this->product_id);
        } else {
            $this->console->info("Product [$data->sap_desc] should be created");
            $this->product_id = DB::selectOne("SHOW TABLE STATUS WHERE name='products'")->Auto_increment;
            $this->console->info("Product auto incremented id [$this->product_id]");
            $product_data = [
                'id' => $this->product_id,
                'sku' => $this->sku,
                'ean13' => $this->ean,
                'weight' => 0,
                'new_from_date' => '',
                'new_to_date' => '',
                'price' => 0,
                'sell_price_wt' => 0,
                'sell_price' => 0,
                'price_nt' => 0,
                'brand_id' => $this->brand_id,
                'collection_id' => $this->collection_id,
                'default_category_id' => 0,
                'main_category_id' => 0,
                'position' => $this->product_id,
                'source' => 'morellato'
            ];

            $this->setBlame($product_data);
            $name = ucfirst(Str::lower($data->sap_desc));
            $uslug = Str::slug(Str::lower($name));
            foreach ($this->languages() as $language) {
                $product_data[$language] = [
                    'name' => $name,
                    'slug' => $uslug,
                    'published' => 1,
                ];
            }
            $model = new \Product();
            $model->make($product_data);
            $this->importProductAttributes($this->product_id);
            $this->product_id++;
        }

    }

    protected function checkProductExistence()
    {
        $product_id = \Product::where('sku', $this->sku)
            ->where('brand_id', $this->brand_id)
            ->where('collection_id', $this->collection_id)
            ->pluck("id");

        if ($product_id) {
            return $product_id;
        }
        return 0;
    }

    protected function importProductAttributes($product_id)
    {
        //now insert attributes
//        $counter = 1;
        $counter = DB::table('products_attributes_position')->where('product_id', $product_id)->select('position')->max('position');
        $callback = function ($attribute_id, $product_id) {
            $products_attributes = [
                'product_id' => $product_id,
                'attribute_id' => $attribute_id,
                'attribute_val' => 0,
            ];
            DB::table('products_attributes')->insert($products_attributes);
        };

        $callbackUpdatePosition = function ($attribute_id, $counter, $product_id) {
            $products_attributes_position = [
                'product_id' => $product_id,
                'attribute_id' => $attribute_id,
                'position' => $counter,
            ];
            DB::table('products_attributes_position')->insert($products_attributes_position);
        };


        foreach ($this->attribute_id as $key => $attribute_id) { //multiple attributes
            $product_attribute_id = DB::table('products_attributes')->where('product_id', $product_id)->where('attribute_id', $attribute_id)->pluck('id');
            if ($product_attribute_id) {
                $this->console->error("Skipping inserting Product attribute. Already exist with id [$product_attribute_id]");
            } else {
                $this->console->info("Product attribute [$attribute_id] should be created");
                $callback($attribute_id, $product_id);
            }

//            if (!$this->is_set_products_attributes_position) {
            $products_attributes_position_id = DB::table('products_attributes_position')->where('product_id', $product_id)->where('attribute_id', $attribute_id)->pluck('id');
            if (!$products_attributes_position_id) {
                $this->console->info("Product attribute position [$attribute_id] should be created");
                $counter = $counter + 1;
                $this->console->comment("Counter for product id [$product_id] is [$counter]");
                $callbackUpdatePosition($attribute_id, $counter , $product_id);
//                $counter++;
//                    $this->is_set_products_attributes_position = 1;
            }
//            }
        }
    }
}