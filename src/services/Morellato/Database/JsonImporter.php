<?php

namespace services\Morellato\Database;

use DB, Utils, Config, Exception, Cache;
use Illuminate\Console\Command;
use services\Morellato\Grabbers\GoogleTranslateGrabber;
use services\Morellato\Loggers\DatabaseLogger as Logger;
use Illuminate\Support\Str;

class JsonImporter
{

    protected $console;
    protected $logger;
    protected $chunk = 200;
    protected $debug = false;
    protected $write = true;
    protected $purge = false;
    protected $allowUniversalName = false;
    protected $bot_user_id = 1;
    protected $cache = [];
    protected $product_id;
    protected $product_combination_id;
    protected $source = null;
    protected $counters = ['inserted' => 0, 'updated' => 0, 'skipped' => 0];
    protected $skus = [];
    protected $limit_skus = [];
    protected $catalog_cache = [];

    function __construct(Command $console, Logger $logger, GoogleTranslateGrabber $translateGrabber)
    {
        $this->console = $console;
        $this->logger = $logger;
        $this->product_id = DB::selectOne("SHOW TABLE STATUS WHERE name='products'")->Auto_increment;
        $this->product_combination_id = DB::table('products_combinations')->max('id') + 1;
        $this->allowUniversalName = Config::get('ftp.allowUniversalName', false);
        $this->translateGrabber = $translateGrabber;
    }

    /**
     * @return array
     */
    function getCache()
    {
        return $this->cache;
    }

    /**
     * @param $skus
     */
    function setLimitSkus($skus)
    {
        $this->limit_skus = $skus;
        $this->debug = true;
    }

    /**
     * @return array
     */
    function getLimitSkus()
    {
        return $this->limit_skus;
    }

    /**
     * @return bool
     */
    function hasLimitSkus()
    {
        return !empty($this->limit_skus);
    }


    function log($var, $message = null)
    {
        if ($this->debug)
            Utils::log($var, $message);
    }

    function make($importOnyStocks = false)
    {
        if (feats()->switchDisabled(\Core\Cfg::SERVICE_FTP_CSV_DOWNLOAD)) {
            return;
        }
        $this->console->line('Building catalog cache');
        $this->buildCatalogCache();
        if ($importOnyStocks) {
            return $this->makeStocks();
        }
        Config::set('cache.driver', 'file');
        $hasLimitSkus = $this->hasLimitSkus();
        $cacheKey = 'morellato_importer_cache_' . date('Y-m-d-H');
        if ($this->purge or $hasLimitSkus) {
            $this->log($cacheKey, 'Purging cache');
            $this->console->comment('Purging cache');
            Cache::forget($cacheKey);
        }
        if (Cache::has($cacheKey)) {
            $this->cache = Cache::get($cacheKey);
            $this->log($cacheKey, 'Getting cache');
            $this->console->comment('Getting cache');
        } else {
            $this->console->comment('Making categories');
            $this->makeCategories();
            $this->console->comment('Making brands');
            $this->makeBrands();
            $this->console->comment('Making collections');
            $this->makeCollections();
            $this->console->comment('Making attributes');
            $this->makeAttributes();
            Cache::put($cacheKey, $this->cache, 60);
            $this->log($cacheKey, 'Saving cache');
            $this->console->comment('Saving cache');
        }

        $this->log($this->cache, 'cache');
        //return;

        //Utils::watch();

        $import_products = true;
        $import_variants = true; //TODO: this flag must be true in production

        //get the product from "anagrafica"
        if ($import_products):

            $builder = DB::table('mi_anagrafica')
                ->where(function ($query) {
                    $query->where('sap_sku', '=', DB::raw('mastersku'));
                    $query->orWhere('mastersku', '=', '');
                })
                ->where('json', '!=', '')
                ->orderBy('json', 'desc')
                ->orderBy('created_at', 'asc');

            if ($this->hasLimitSkus()) {
                $builder->whereIn('sku', $this->getLimitSkus());
            }

            $rows = $builder->get();

            $many = count($rows);
            $this->logger->log("Founded [$many] products to process", 'database_importer');
            $this->console->line("Founded [$many] products to process");

            $counter = 0;
            foreach ($rows as $row) {
                $this->dbCheckout($counter, $many);
                try {
                    $counter++;
                    list($product, $md5) = $this->product($row);
                    $this->log($product, $md5);
                    $this->doImportProduct($product, $md5);
                    $this->console->line("Processed [$counter/$many]");
                    unset($product);
                    unset($md5);
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage(), 'database_importer');
                    $this->console->error($e->getMessage());
                    audit_exception($e, __METHOD__);
                }
                unset($row);
            }
            $this->dbCheckout(-1, $many);
        endif;

        unset($rows);
        gc_collect_cycles();

        if ($import_variants):

            $builder = DB::table('mi_anagrafica')
                ->where(function ($query) {
                    $query->where('sap_sku', '<>', DB::raw('mastersku'));
                    $query->where('mastersku', '<>', '');
                })
                ->where('json', '!=', '')
                ->orderBy('created_at', 'desc')
                ->select(DB::raw('distinct(mastersku)'), 'variants');

            if ($this->hasLimitSkus()) {
                $builder->whereIn('sku', $this->getLimitSkus());
            }

            $masterskus = $builder->get();

            $many = count($masterskus);
            $this->logger->log("Founded [$many] variants to process", 'database_importer');
            $this->console->line("Founded [$many] variants to process");
            $counter = 0;
            foreach ($masterskus as $row) {
                $this->dbCheckout($counter, $many);
                try {
                    $counter++;
                    $variants = $row->variants;
                    if ($variants == '')
                        $variants = 'size';
                    $this->doImportVariants($row->mastersku, $variants);
                    unset($variants);
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage() . PHP_EOL . $e->getLine() . PHP_EOL . $e->getFile(), 'database_importer');
                    $this->console->error($e->getMessage());
                    audit_exception($e, __METHOD__);
                }
                unset($row);
            }
            $this->dbCheckout(-1, $many);
        endif;

        unset($masterskus);
        gc_collect_cycles();

        if ($this->counters['inserted'] > 0) {
            $many = $this->counters['inserted'];
            $this->logger->log("[$many] products have been inserted", 'database_importer');
        }
        if ($this->counters['updated'] > 0) {
            $many = $this->counters['updated'];
            $this->logger->log("[$many] products have been updated", 'database_importer');
        }
        if ($this->counters['skipped'] > 0) {
            $many = $this->counters['skipped'];
            $this->logger->log("[$many] products have been skipped", 'database_importer');
        }
    }


    protected function buildCatalogCache()
    {
        //Disable memory_limit by setting it to minus 1.
        ini_set('memory_limit', '-1');

        //Disable the time limit by setting it to 0.
        set_time_limit(0);

        $builder = \Product::where('source', 'morellato')->orderBy('id', 'desc');
        if ($this->hasLimitSkus()) {
            $builder->where(function ($query) {
                $query->whereIn('sku', $this->getLimitSkus())->orWhereIn('sap_sku', $this->getLimitSkus());
            });
        }
        $products = $builder->get();
        $many = count($products);
        $counter = 0;
        foreach ($products as $product) {
            $counter++;
            $payload = $product->toArray();
            $key = 'morellato_' . $product->sku;
            $this->catalog_cache[$key] = $payload;
            if ($product->sap_sku != '') {
                $key = 'morellato_' . $product->sap_sku;
                $this->catalog_cache[$key] = $payload;
                $this->console->line("Registered cache for '{$key}' ($counter/$many)");
            } else {
                $this->console->line("Registered cache for '{$key}' ($counter/$many)");
            }
            unset($product, $payload);
        }

        $builder = \Product::where(function ($query) {
            $query->whereNull('source')->orWhere('source', 'local');
        })->orderBy('id', 'desc');
        if ($this->hasLimitSkus()) {
            $builder->where(function ($query) {
                $query->whereIn('sku', $this->getLimitSkus())->orWhereIn('sap_sku', $this->getLimitSkus());
            });
        }
        $products = $builder->get();
        $many = count($products);
        $counter = 0;
        foreach ($products as $product) {
            $counter++;
            $payload = $product->toArray();
            $key = 'local_' . $product->sku;
            $this->catalog_cache[$key] = $payload;
            if ($product->sap_sku != '') {
                $key = 'local_' . $product->sap_sku;
                $this->catalog_cache[$key] = $payload;
                $this->console->line("Registered cache for '{$key}' ($counter/$many)");
            } else {
                $this->console->line("Registered cache for '{$key}' ($counter/$many)");
            }
            unset($product, $payload);
        }
    }


    protected function makeStocks()
    {
        $this->console->info('Executing makeStocks');

        $rows = DB::select("select * from mi_giacenza union select * from mi_giacenza_ng where sku not in (select sku from mi_giacenza)");

        $many = count($rows);

        $this->console->line("Found [$many] products to process");

        $counter = 0;

        foreach ($rows as $row) {
            $this->dbCheckout($counter, $many);
            $row->sap_sku = $row->sku;

            if (true) {
                $this->console->line("Found $row->sku");
                $price = $this->getPriceBySku($row->sap_sku);
                $special_prices = $this->getSpecialPricesBySku($row->sap_sku);
                $stocks = $this->getStocksBySku($row->sap_sku);
                $ng_stocks = $this->getNgStocksBySku($row->sap_sku);

                $this->log($price, "Price [$row->sku]");
                $this->console->line("Price[$row->sku]: $price");
                $this->log($special_prices, "Special Price [$row->sku]");
                $this->log($stocks, "Stocks [$row->sku]");
                $this->log($ng_stocks, "NG Stocks [$row->sku]");

                $product = $this->getProductModelBySku($row);

                if ($product) {
                    $this->console->comment("Found product with id: $product->id");
                    $this->removeProductRelatedData($product->id);

                    //setting price
                    if ($price > 0) {
                        $priceData = $this->parsePrices($price);
                        $this->log($priceData, 'Updating product price');
                        $this->console->line('Updating product price');
                        if ($this->write) {
                            DB::table('products')->where('id', $product->id)->update($priceData);
                        }
                    }

                    $product->special_prices = $special_prices;
                    $product->stocks = $stocks;
                    $product->ng_stocks = $ng_stocks;

                    $this->console->line('Importing special prices');
                    $this->importProductSpecialPrices($product, $product->id);
                    $this->console->line('Importing product stocks');
                    $this->importProductStocks($product, $product->id);
                    $this->console->info("Imported stocks for product [$row->sku]");

                    $combination = $this->getProductCombinationBySku($row);
                    if ($combination) {
                        $this->console->comment("Found combination with id: $combination->id");
                        $combination->stocks = $stocks;
                        $combination->ng_stocks = $ng_stocks;
                        $this->handleCombinationStocks($combination, $price);
                    }

                } else {
                    //try to search for a product combination
                    $this->log("Product not found. Searching combination [$row->sku]");
                    $this->console->line("Product not found. Searching combination [$row->sku]");
                    $combination = $this->getProductCombinationBySku($row);
                    if ($combination) {
                        $combination->stocks = $stocks;
                        $combination->ng_stocks = $ng_stocks;
                        $this->handleCombinationStocks($combination, $price);
                    }

                }
                $counter++;
                $this->console->line("Processed product ($counter/$many)");
            }
        }
        $this->dbCheckout(-1, $many);
    }

    protected function handleCombinationStocks($combination, $price)
    {
        $this->console->line("Found combination $combination->sku");
        $this->removeProductRelatedData($combination->product_id, $combination->id);

        audit($combination, __METHOD__, 'Combination');

        $data = [];
        //setting price
        if ($price > 0) {
            $data['buy_price'] = $price;
            $this->log($price, "Updating product combination price");
        }
        if ($this->write and !empty($data)) {
            DB::table('products_combinations')->where('id', $combination->id)->update($data);
        }

        $this->importProductStocks($combination, $combination->product_id, $combination->id);
        $this->console->info("Imported stocks for combination [$combination->sku]");
    }

    protected function product($product)
    {
        unset($product->created_at);

        $active_languages = \Core::getLanguages();
        $translations = DB::table('mi_traduzioni')->where('sku', $product->sku)->whereIn('lang_id', $active_languages)->get();

        $storage = [];

        $attributesDefinitions = [];

        foreach ($translations as $translation) {
            $lang_id = strtolower($translation->lang_id);
            $product_translation = (object)[
                'name' => $this->sanitize($translation->name),
                'lang_id' => $lang_id,
                'sdesc' => $this->parseHtml($translation->commercial_description),
            ];

            if ($this->nullish($translation->name) and $this->nullish($translation->description)) {
                throw new Exception("Translation data are incomplete for product [$product->sku]");
            }

            if ($this->allowUniversalName) {
                if ($this->nullish($translation->name)) {
                    $product_translation->name = $this->sanitize($product->name);
                    $this->logger->warning("Translation name is empty or incomplete for product [$product->sku]; used the name from Anagrafica, which is not translated!");
                }
            } else {
                if ($this->nullish($translation->name)) {
                    throw new Exception("Translation name is empty or incomplete for product [$product->sku]");
                }
            }

            if ($product_translation->sdesc == '')
                $product_translation->sdesc = $this->parseHtml($product->commercial_description);

            $storage[$lang_id] = $product_translation;
        }

        $categoryRelation = [];
        $givenCategoryInfo = DB::table('mi_categorie')->where('mastersku', $product->mastersku)->first();
        //check with sap_sku
        if (is_null($givenCategoryInfo))
            $givenCategoryInfo = DB::table('mi_categorie')->where('mastersku', $product->sap_sku)->first();

        if ($givenCategoryInfo) {
            if (!$this->nullish($givenCategoryInfo->categoria1livello2)) {
                $categories = $this->explodeString($givenCategoryInfo->categoria1livello2);
                foreach ($categories as $category) {
                    $categoryRelation[] = $this->cacheRead('categories', $category);
                }
            } else {
                $categoryRelation[] = $this->cacheRead('categories', $givenCategoryInfo->categoria1livello1);
            }
        }
        if (empty($categoryRelation)) {
            $categoryRelation[] = $this->cacheRead('categories', $product->attr_category);
        }


        $name = $this->sanitize($product->attr_collection);
        $brand = $this->sanitize($product->brand);
        $relations = [];
        $relations['brand'] = $this->cacheRead('brands', $product->brand);
        $relations['collection'] = $this->cacheRead('collections', "$name-$brand");
        $relations['category'] = $categoryRelation;

        $product->relations = $relations;

        $attributes = [];

        $attributes['size'] = $this->matchAttributes('size', Str::slug($product->size));

        //add Garanzia e Confezione if product category = 'Orologio'
        $attributes['warranty'] = [
            'attribute_id' => 13,
            'attribute_option_id' => 36,
        ];

        $attributes['package'] = [
            'attribute_id' => 12,
            'attribute_option_id' => 34,
        ];

        /*$attributes['materials'] = $this->matchAttributes('materials', $product->attr_materials);
        $attributes['colors'] = $this->matchAttributes('colors', $product->attr_colors);
        $attributes['functions'] = $this->matchAttributes('functions', $product->attr_functions);
        $attributes['movements'] = $this->matchAttributes('movements', $product->attr_movement);*/


        //boolean attributes: moments|bracelet_shortening|battery_change
        $booleans = $this->getBooleanAttributes();
        foreach ($booleans as $boolean => $attribute_id) {
            if ($product->{"attr_$boolean"} == 'SI') {
                $attributes[$boolean] = ['attribute_option_id' => 1, 'attribute_id' => $attribute_id];
            }
        }

        foreach ($attributes as $key => $attribute) {
            if (empty($attribute))
                unset($attributes[$key]);
        }

        $product->attributes = $attributes;
        $product->price = $this->getPriceBySku($product->sap_sku);
        $product->translations = $storage;

        //calculate the digest for the product
        //a product digest contains all relations except 'special prices' and 'stocks'
        $md5 = md5(serialize($product));

        $special_prices = $this->getSpecialPricesBySku($product->sap_sku);
        $stocks = $this->getStocksBySku($product->sap_sku);
        $ng_stocks = $this->getNgStocksBySku($product->sap_sku);
        $product->special_prices = $special_prices;
        $product->stocks = $stocks;
        $product->ng_stocks = $ng_stocks;

        return [$product, $md5];
    }


    protected function doImportProduct($product, $md5)
    {
        try {
            $this->importProduct($product, $md5);
        } catch (\Exception $e) {
            $this->insertError($e, $product);
            $msg = $e->getMessage() . PHP_EOL . $e->getFile() . '(' . $e->getLine() . ')';
            $this->logger->error("Product with SKU ($product->sku) could not be imported: " . $msg);
            throw new \Exception("Warning: product with SKU ($product->sku) could not be imported: " . $msg, $e->getCode());
        }
    }


    protected function importProduct($product, $md5)
    {
        //define action: import, update or skip?
        if (!$this->jsonIsImportable($product)) {
            $this->logger->warning("Product [$product->sku] could not be imported; json is not compatible", 'database_importer');
            $this->console->error("Product [$product->sku] could not be imported; json is not compatible");
            return;
        }
        $action = $this->importAction($product->sku, $product->sap_sku, $md5);

        $oldModel = $this->getProductModelBySku($product, false);
        if ($oldModel and $oldModel->id > 0) {
            $action = 'update';
        }
        if ($oldModel and $oldModel->id > 0) {
            $model = $oldModel;
        } else {
            $model = $this->getProductModelBySku($product);
            if ($model)
                $action = 'update';
        }

        if ($action == 'skip' and $model) {
            if ($model->translations()->count() == 0)
                $action = 'update';
        }

        $this->logger->log("Prepare to ($action) product with sku [$product->sku]", 'database_importer');
        $this->console->line("Prepare to ($action) product with sku [$product->sku]");


        //$this->log($product, 'PRODUCT');
        //$this->log($md5, 'MD5');

        //create data to import/update

        $canvas = $this->parseCanvas($product->canvas);

        $prices = null;
        if ($product->price > 0)
            $prices = $this->parsePrices($product->price);

        $data = [
            'sku' => $product->sku,
            'sap_sku' => $product->sap_sku,
            'mastersku' => $product->mastersku,
            'ean13' => $product->ean,
            'weight' => $product->weight,
            'new_from_date' => $canvas['new_from_date'],
            'new_to_date' => $canvas['new_to_date'],
            'brand_id' => $product->relations['brand'],
            'collection_id' => $product->relations['collection'],
            'default_category_id' => $product->relations['category'][0]['id'],
            'main_category_id' => $product->relations['category'][0]['parent_id'],
            'is_outlet' => $product->outlet == 'SI',
            'source' => 'morellato',
        ];

        if (is_array($prices) and $prices['sell_price_wt'] > 0) {
            $data['price'] = $prices['price'];
            $data['sell_price_wt'] = $prices['sell_price_wt'];
            $data['sell_price'] = $prices['sell_price'];
            $data['price_nt'] = $prices['price_nt'];
        }

        if ($data['main_category_id'] == 0)
            $data['main_category_id'] = $data['default_category_id'];


        //when 'skipping' a product only stocks and special prices are re-binded
        if ($action == 'skip') {
            $product_id = $model->id;
            $this->log("Skipping insert/update action: re-bind only special prices and stocks for ($model->sku|$model->id)");
            $this->console->line("Skipping insert/update action: re-bind only special prices and stocks for ($model->sku|$model->id)");
            $this->removeProductRelatedData($product_id);
            $this->importProductSpecialPrices($product, $product_id);
            $this->importProductStocks($product, $product_id);
            $this->counters['skipped']++;

            Utils::log($data, 'SKIPPING ACTION');
            //rapid fix for categories
            \Product::where('id', $product_id)->update($data);
            return $product_id;
        }

        if ($oldModel and $oldModel->id > 0) {
            $data['is_out_of_production'] = 0;
            $data['cnt_attributes'] = 0;
            $data['cnt_categories'] = 0;
        }

        //bind translations to the data source model
        foreach ($product->translations as $locale => $translation) {
            unset($translation->lang_id);
            $translation->published = 1;
            $translation->slug = Str::slug($translation->name);
            $data[$locale] = $translation;
        }

        $this->log($data, 'DATA TO UPSERT');


        if ($action == 'insert') {
            //INSERT PRODUCT HERE
            $product_id = $this->product_id;
            $model = new \Product();
            $data['is_out_of_production'] = 0;
            $data['id'] = $product_id;
            $data['position'] = $product_id;
            $data['qty'] = $this->getProductQuantityByStocks($product, $product_id);
            $this->setBlame($data);
            $attributes = $model->make($data, true);
            //$this->log($attributes, 'PRODUCTS FILLABLE INSERT');
            if ($this->write) {
                $model->save();
            }

            $this->importProductAttributes($product, $product_id);
            $this->importProductCategories($product, $product_id);
            $this->importProductSpecialPrices($product, $product_id);
            $this->importProductStocks($product, $product_id);
            $this->insertMigration($product->sku, $product->sap_sku, $md5);

            $this->logger->log("Product [$product->sku] successfully inserted with id [$product_id]", 'database_importer');
            $this->console->line("Product [$product->sku] successfully inserted with id [$product_id]");

            //update the general product id
            $this->product_id++;
            $this->counters['inserted']++;
            return $product_id;
        }

        //during the update only the main data are updated; in order to update the relations the records are deleted and re-inserted
        if ($action == 'update') {

            $model = $this->getProductModelBySku($product, true, true);

            if ($model !== null) {
                $product_id = $model->id;
                //now update the model
                //$this->log($data, 'PRODUCTS FILLABLE UPDATE');
                $modelData = $model->toArray();
                $data = array_merge($modelData, $data);
                $data['cnt_attributes'] = 0;
                $data['cnt_categories'] = 0;
                $data['is_out_of_production'] = 0;
                $attributes = $model->make($data, true);
                //$this->log($attributes, 'PRODUCTS UPDATE COMPLETE DATA');
                if ($this->write) {
                    $model->save();
                    \Product::where('id', $product_id)->update(['cnt_attributes' => 0, 'cnt_categories' => 0]);
                }

                //remove main relations
                $this->removeProductRelations($product_id);

                //re-insert all relations
                $this->importProductAttributes($product, $product_id);
                $this->importProductCategories($product, $product_id);
                $this->importProductSpecialPrices($product, $product_id);
                $this->importProductStocks($product, $product_id);
                $this->upsertMigration($product->sku, $product->sap_sku, $md5);

                $this->logger->log("Product [$product->sku] successfully updated with id [$product_id]", 'database_importer');
                $this->console->line("Product [$product->sku] successfully updated with id [$product_id]");

                $this->counters['updated']++;
                return $product_id;
            }
        }

        return null;
    }


    protected function jsonIsImportable($product)
    {
        $json = trim($product->json);
        $json = rtrim($json, '|');
        $segments = explode('|', $json);
        foreach ($segments as $segment) {
            $tokens = explode(':', $segment);
            if (count($tokens) == 2 and is_numeric($tokens[0])) {
                return true;
            }
        }
        return false;
    }


    protected function importProductAttributes($product, $product_id)
    {
        //now insert attributes

        //parse the json column
        $json = trim($product->json);
        $json = rtrim($json, '|');
        $json_attributes = [];
        if (strlen($json) > 0) {
            //00002:|00006:196|00010:188,3382|00015:|00021:256|00039:|00044: 5MM|00078:|00080:|00214:25|00217:|00226: 3 ATM|00257:|
            $segments = explode('|', $json);
            $many = count($segments);
            $this->log("Found [$many] segments from JSON column for product [$product->sku]");
            foreach ($segments as $segment) {
                list($attribute_id, $options_list) = explode(':', $segment);
                if (is_numeric($attribute_id)) {
                    $attribute_id = intval($attribute_id);
                    if ($attribute_id == 226) //patch for water resistant
                        $attribute_id = 14;
                    $options_list = trim($options_list);
                    if (strlen($options_list) == 0) {
                        $this->log("There are no matching attribute options for attribute [$attribute_id], in importing product [$product->sku]");
                    } else {
                        $data = [];
                        $data['attribute_id'] = $attribute_id;
                        $data['values'] = [];
                        $option_ids = explode(',', $options_list);
                        foreach ($option_ids as $option_id) {
                            $option_id = ltrim($option_id, '0');
                            if (is_numeric($option_id)) {
                                $this->log("Found numeric value [$option_id] for attribute [$attribute_id]");
                            } else {

                            }
                            //$data['attribute_option_id'] = $option_id;
                            $data['values'][] = $option_id;
                        }
                        $json_attributes[$attribute_id] = $data;
                    }
                } else {
                    $this->console->error("The attribute [$attribute_id] for product [$product->sku] is not numeric - cannot parse json");
                }
            }
        }
        $this->log($json_attributes, 'JSON ATTRIBUTES');

        $callback = function ($attribute, $counter, $name, $product_id) {
            $products_attributes = [
                'product_id' => $product_id,
                'attribute_id' => $attribute['attribute_id'],
                'attribute_val' => $attribute['attribute_option_id'],
            ];

            $this->log($products_attributes, 'PRODUCTS ATTRIBUTES INSERT => ' . $name);

            if ($this->write and $products_attributes['attribute_id'] > 0) {
                $exists = DB::table('products_attributes')
                    ->where('product_id', $products_attributes['product_id'])
                    ->where('attribute_id', $products_attributes['attribute_id'])
                    ->where('attribute_val', $products_attributes['attribute_val'])
                    ->first();
                if (is_null($exists)) {
                    DB::table('products_attributes')->insert($products_attributes);
                }
            }
        };


        $callbackUpdatePosition = function ($attribute, $counter, $name, $product_id) {

            $products_attributes_position = [
                'product_id' => $product_id,
                'attribute_id' => $attribute['attribute_id'],
                'position' => $counter,
            ];

            $this->log($products_attributes_position, 'PRODUCTS ATTRIBUTES POSITION INSERT => ' . $name);

            if ($this->write and $products_attributes_position['attribute_id'] > 0) {
                $exists = DB::table('products_attributes_position')
                    ->where('product_id', $products_attributes_position['product_id'])
                    ->where('attribute_id', $products_attributes_position['attribute_id'])
                    ->first();
                if (is_null($exists)) {
                    DB::table('products_attributes_position')->insert($products_attributes_position);
                }
            }
        };

        $counter = 1;

        foreach ($json_attributes as $attribute_id => $json_attribute) {
            $attribute = ['attribute_id' => $attribute_id];
            $name = $attribute_id;
            foreach ($json_attribute['values'] as $attribute_option_id) {
                $attribute['attribute_option_id'] = $attribute_option_id;
                $callback($attribute, $counter, $name, $product_id);
            }
            $callbackUpdatePosition($attribute, $counter, $name, $product_id);
            $counter++;
        }

        foreach ($product->attributes as $name => $attribute) {
            if (is_array($attribute) and !empty($attribute)) {

                if (isset($attribute['attribute_id'])) { //single attribute
                    $callback($attribute, $counter, $name, $product_id);
                    $callbackUpdatePosition($attribute, $counter, $name, $product_id);
                    $counter++;
                } else {
                    foreach ($attribute as $item) { //multiple attributes
                        $callback($item, $counter, $name, $product_id);
                    }
                    $callbackUpdatePosition($attribute[0], $counter, $name, $product_id);
                    $counter++;
                }
            }
        }
    }


    protected function importProductCategories($product, $product_id)
    {
        //now insert categories
        if (!empty($product->relations['category'])) {
            foreach ($product->relations['category'] as $category) {
                $category_data = [
                    'category_id' => $category['id'],
                    'product_id' => $product_id,
                ];
                $this->log($category_data, 'PRODUCTS CATEGORIES INSERT');
                if ($this->write) {
                    try {
                        DB::table('categories_products')->insert($category_data);
                    } catch (\Exception $e) {

                    }

                }
            }
        }
    }


    protected function importProductSpecialPrices($product, $product_id)
    {
        //now insert special prices
        if (!empty($product->special_prices)) {
            foreach ($product->special_prices as $special_price) {
                $special_price['product_id'] = $product_id;
                $special_price['reduction_type'] = 'fixed_amount';
                $special_price['reduction_value'] = $special_price['price'];
                unset($special_price['price']);
                $model = new \ProductPrice();
                $attributes = $model->make($special_price, true);
                $this->log($attributes, 'PRODUCTS SPECIAL PRICES INSERT');
                if ($this->write) {
                    $model->save();
                }
            }
        }
    }


    protected function importProductStocks($product, $product_id, $product_combination_id = null)
    {
        //now insert stocks
        if (!empty($product->stocks)) {
            foreach ($product->stocks as $stock) {
                $stock['product_id'] = $product_id;
                $stock['product_combination_id'] = $product_combination_id;
                if (in_array($stock['warehouse'], ['NG', 'W3'])) {
                    $stock['quantity_shop'] = $stock['quantity_24'];
                    $stock['quantity_24'] = 0;
                }
                $model = new \ProductSpecificStock();
                $attributes = $model->make($stock, true);
                if ($this->write) {
                    //update the virtual quantity???
                    $model->save();
                    $model->setProductQuantity();
                    if ($product_combination_id > 0) { //this is a combination
                        $quantity = $model->getProductQuantity();
                        \ProductCombination::where('id', $product_combination_id)->update(['quantity' => $quantity]);
                    }
                }
                $this->log($attributes, 'PRODUCTS STOCKS INSERT');
            }
        }
        //insert ng stocks
        if (!empty($product->ng_stocks)) {
            foreach ($product->ng_stocks as $stock) {
                $model = \ProductSpecificStock::where('product_id', $product_id)->where('product_combination_id', $product_combination_id)->first();

                if (is_null($model)) { //create
                    $model = new \ProductSpecificStock();
                    $stock['product_id'] = $product_id;
                    $stock['product_combination_id'] = $product_combination_id;
                    $stock['quantity_24'] = 0;
                    $attributes = $model->make($stock, true);
                    $this->log($attributes, 'PRODUCTS NG STOCKS INSERT');
                } else { //update
                    $model->quantity_shop = $stock['quantity_shop'];
                    $this->log("Stock already exists for [$product_id]", __METHOD__);
                    $this->log($model->toArray(), 'PRODUCTS NG STOCKS UPDATE');
                }
                if ($this->write) {
                    //update the virtual quantity???
                    $model->save();
                    $model->setProductQuantity();
                    if ($product_combination_id > 0) { //this is a combination
                        $quantity = $model->getProductQuantity();
                        \ProductCombination::where('id', $product_combination_id)->update(['quantity' => $quantity]);
                    }
                }

            }
        }
    }


    protected function getProductQuantityByStocks($product, $product_id, $product_combination_id = null)
    {
        //now insert stocks
        $qty = 0;
        if (!empty($product->stocks)) {
            foreach ($product->stocks as $stock) {
                $stock['product_id'] = $product_id;
                $stock['product_combination_id'] = $product_combination_id;
                $model = new \ProductSpecificStock();
                $model->make($stock, true);
                $qty += $model->getProductQuantity();
            }
        }
        return $qty;
    }

    protected function removeProductRelations($product_id)
    {
        //if mode = 2, then execute only removeProductRelatedData for the product
        if (config('ftp.csvImportMode', 1) == 2) {
            $this->removeProductRelatedData($product_id);
            return;
        }
        if ($this->write) {
            //remove categories
            DB::table('categories_products')->where('product_id', $product_id)->delete();
            //remove attributes
            DB::table('products_attributes')->where('product_id', $product_id)->delete();
            DB::table('products_attributes_position')->where('product_id', $product_id)->delete();
            $this->removeProductRelatedData($product_id);
        }
    }


    protected function removeWrongProduct($product_id)
    {
        DB::table('products')->where('id', $product_id)->delete();
        DB::table('products_lang')->where('product_id', $product_id)->delete();
        $this->removeProductRelations($product_id);
    }


    protected function removeProductRelatedData($product_id, $combination_id = null)
    {
        if ($this->write == false)
            return;

        DB::commit();

        try {
            if ($combination_id) {
                //remove special prices
                \ProductPrice::where('product_id', $product_id)->where('price_rule_id', 0)->where('country_id', 0)->delete();
                //remove stocks
                \ProductSpecificStock::where('product_id', $product_id)->where('product_combination_id', $combination_id)->delete();
            } else {
                //remove special prices
                \ProductPrice::where('product_id', $product_id)->where('price_rule_id', 0)->where('country_id', 0)->delete();
                //remove stocks
                \ProductSpecificStock::where('product_id', $product_id)->delete();
            }
        } catch (Exception $e) {
            audit_exception($e, __METHOD__);
        }

        DB::beginTransaction();
    }


    /**
     * @param $product
     * @param bool $morellatoOnly
     * @return \Product|null
     */
    protected function getProductModelBySku($product, $morellatoOnly = true, $skip_cache = false)
    {
        if ($morellatoOnly) {
            if($skip_cache === false){
                $key = 'morellato_' . $product->sap_sku;
                $alt_key = 'morellato_' . $product->sku;
                $payload = array_get($this->catalog_cache, $key);

                $found_product = null;
                if (is_array($payload)) {
                    $found_product = new \Product($payload);
                }

                if ($found_product instanceof \Product) {
                    //audit('returned by ' . $key, __METHOD__);
                    return $found_product;
                }

                $found_product = null;
                $payload = array_get($this->catalog_cache, $alt_key);

                if (is_array($payload)) {
                    $found_product = new \Product($payload);
                }

                if ($found_product instanceof \Product) {
                    //audit('returned by ' . $alt_key, __METHOD__);
                    return $found_product;
                }
            }

            $model = \Product::where(function ($query) use ($product) {
                $query->where('sku', $product->sku)->orWhere('sap_sku', $product->sap_sku)->orWhere('sku', $product->sap_sku);
            })->where('source', 'morellato')->first();

            return $model;
        } else {
            if($skip_cache === false){
                $key = 'local_' . $product->sap_sku;
                $alt_key = 'local_' . $product->sku;

                $payload = array_get($this->catalog_cache, $key);

                $found_product = null;
                if (is_array($payload)) {
                    $found_product = new \Product($payload);
                }

                if ($found_product instanceof \Product) {
                    //audit('returned by ' . $key, __METHOD__);
                    return $found_product;
                }

                $found_product = null;
                $payload = array_get($this->catalog_cache, $alt_key);

                if (is_array($payload)) {
                    $found_product = new \Product($payload);
                }

                if ($found_product instanceof \Product) {
                    //audit('returned by ' . $alt_key, __METHOD__);
                    return $found_product;
                }
            }

            $model = \Product::where(function ($query) use ($product) {
                $query->where('sku', $product->sku)->orWhere('sap_sku', $product->sap_sku)->orWhere('sku', $product->sap_sku);
            })->where(function ($query) use ($product) {
                $query->whereNull('source')->orWhere('source', 'local');
            })->first();
            return $model;
        }
    }


    protected function getProductCombinationBySku($product)
    {
        $combination = \ProductCombination::where(function ($query) use ($product) {
            $query->where('sku', $product->sku)->orWhere('sap_sku', $product->sap_sku)->orWhere('sku', $product->sap_sku);
        })->first();

        return $combination;
    }


    protected function matchAttributes($type, $str)
    {
        $tokens = $this->explodeString($str);
        $matches = [];
        foreach ($tokens as $token) {
            $matches[] = $this->cacheRead($type, $token);
        }
        return $matches;
    }


    protected function getBooleanAttributes()
    {
        if (isset($this->booleanAttributes)) {
            return $this->booleanAttributes;
        }
        $booleans = ['moments', 'bracelet_shortening', 'battery_change'];
        $data = [];
        foreach ($booleans as $boolean) {
            $attribute_id = $this->getAttributeIdByCode($boolean);
            $data[$boolean] = $attribute_id;
        }
        $this->booleanAttributes = $data;
        return $data;
    }


    protected function setBlame(&$data)
    {
        $data['created_by'] = $this->bot_user_id;
        $data['updated_by'] = $this->bot_user_id;
    }

    protected function languages()
    {
        return \Core::getLanguages();
    }


    protected function resolveBrand($name)
    {
        if ($this->nullish($name))
            return null;

        $lower = Str::lower($name);

        if ($lower == 'sector gioielli')
            $name = 'Sector';

        if ($lower == 'ice watch' or $lower == 'ice watches')
            $name = 'Ice-Watch';

        /*if ($lower == 'trussardi')
            $name = 'Trussardi';*/

        if ($lower == 'skagen')
            $name = 'Skagen Denmark';

        $slug = Str::slug($name);

        $id = DB::table("brands_lang")->where("slug", $slug)->pluck("brand_id");

        return $id > 0 ? $id : null;
    }


    protected function makeBrands()
    {
        $this->cache['brands'] = [];
        $builder = DB::table('mi_anagrafica')
            ->where('json', '!=', '')
            ->select(DB::raw('distinct(brand)'));

        if ($this->hasLimitSkus()) {
            $builder->whereIn('sku', $this->getLimitSkus());
        }

        $brands = $builder->lists('brand');

        foreach ($brands as $brand) {
            $brand = $this->sanitize($brand);
            if (!$this->nullish($brand)) {
                $id = $this->resolveBrand($brand);
                if ($id) {
                    $this->cacheStore('brands', $brand, $id);
                } else {
                    $this->cacheStore('brands', $brand, $this->createBrand($brand));
                }
            }
        }

    }


    protected function createBrand($name)
    {
        $name = ucfirst(Str::lower($name));
        $data = [];
        $this->setBlame($data);
        foreach ($this->languages() as $language) {
            $data[$language] = [
                'name' => $name,
                'slug' => Str::slug($name),
                'published' => 1,
            ];
        }
        $model = new \Brand();
        $model->make($data);
        $this->logger->log("Created new brand [$name]", 'database_importer');
        return $model->id;
    }


    protected function makeCollections()
    {
        $this->cache['collections'] = [];
        $builder = DB::table('mi_anagrafica')
            ->where('json', '!=', '')
            ->select(DB::raw('distinct(attr_collection) as collection'), 'brand')->orderBy('brand')->orderBy('collection');

        if ($this->hasLimitSkus()) {
            $builder->whereIn('sku', $this->getLimitSkus());
        }

        $collections = $builder->get();
        //$this->log($collections,__METHOD__);
        foreach ($collections as $collection) {
            $name = $this->sanitize($collection->collection);
            $brand = $this->sanitize($collection->brand);
            if (!$this->nullish($name)) {
                $id = $this->resolveCollection($name, $brand);
                if ($id) {
                    $this->cacheStore('collections', "$name-$brand", $id);
                } else {
                    $this->cacheStore('collections', "$name-$brand", $this->createCollection($name, $brand));
                }
            }
        }
    }

    protected function createCollection($name, $brand)
    {
        $name = ucfirst(Str::lower($name));
        $brand_id = null;
        try {
            $brand_id = $this->cacheRead('brands', $brand);
        } catch (\Exception $e) {
            $this->logger->error("Try to fetch brand_id in createCollection has caused error for collection,brand ($name,$brand)", 'database_importer');
        }
        $data = [
            'brand_id' => $brand_id,
        ];
        $this->setBlame($data);
        foreach ($this->languages() as $language) {
            $data[$language] = [
                'name' => $name,
                'slug' => Str::slug($name),
                'published' => 1,
            ];
        }
        $this->log($data, __METHOD__);
        $model = new \Collection();
        $model->make($data);
        $this->logger->log("Created new collection [$name] for brand [$brand]", 'database_importer');
        return $model->id;
    }


    protected function resolveCollection($name, $brand)
    {
        if ($this->nullish($name))
            return null;

        $brand_id = null;
        try {
            $brand_id = $this->cacheRead('brands', $brand);
        } catch (\Exception $e) {
            $this->logger->error("Try to fetch brand_id in resolveCollection has caused error for collection,brand ($name,$brand)", 'database_importer');
        }

        $slug = Str::slug($name);

        $id = DB::table("collections_lang")
            ->join('collections', 'collections.id', '=', 'collections_lang.collection_id')
            ->where("slug", $slug)
            ->where('brand_id', $brand_id)
            ->pluck("id");

        return $id > 0 ? $id : null;
    }


    protected function attributeNameCanBeSingle($name)
    {
        $name = Str::lower($name);
        return (str_contains($name, ['misura', 'size', 'lunghezza', 'larghezza', 'ampiezza', 'width', 'height']));
    }


    protected function makeAttributes()
    {

        $process_gender = false;
        $process_size = true;
        $process_karats = false;

        //gender
        if ($process_gender) {
            $aliases_rows = DB::table('mi_attribute_alias')->where('attribute', 'gender')->get();
            $aliases = [];
            foreach ($aliases_rows as $aliases_row) {
                $aliases[$aliases_row->alias] = ['attribute_id' => $aliases_row->attribute_id, 'attribute_option_id' => $aliases_row->attribute_option_id];
            }

            $builder = DB::table('mi_anagrafica')
                ->where('json', '!=', '')
                ->select(DB::raw('distinct(attr_gender) as gender'))->orderBy('gender');

            if ($this->hasLimitSkus()) {
                $builder->whereIn('sku', $this->getLimitSkus());
            }

            $rows = $builder->get();
            //$this->log($rows, 'gender');
            foreach ($rows as $row) {
                $gender = trim(Str::upper($row->gender));
                if ($gender == '')
                    continue;

                if (isset($aliases[$gender])) {
                    $this->cacheStore('genders', $gender, $aliases[$gender]);
                } else {
                    $this->cacheStore('genders', $gender, $this->createGender($gender));
                    $this->logger->log("Created new attribute: gender [$gender]", 'database_importer');
                }
            }
        }


        //size
        if ($process_size) {
            $attribute_id = $this->getAttributeIdByCode('size');

            $builder = DB::table('mi_anagrafica')
                ->where('json', '!=', '')
                ->select(DB::raw('distinct(size) as attribute'))->orderBy('attribute');

            if ($this->hasLimitSkus()) {
                $builder->whereIn('sku', $this->getLimitSkus());
            }

            $data = $builder->get();
            foreach ($data as $attribute) {
                $name = $attribute->attribute;
                if ($name != '') {
                    $attribute_option_id = $this->resolveAttribute($name, $attribute_id);
                    if ($attribute_option_id) {
                        $this->cacheStore('size', $name, compact('attribute_option_id', 'attribute_id'));
                    } else {
                        $this->log("Size $name should be created");
                        $attribute_option_id = $this->createAttributeOption($name, $attribute_id);
                        $this->log("New attribute option created with id: $attribute_option_id");
                        $this->cacheStore('size', $name, compact('attribute_option_id', 'attribute_id'));
                        $this->logger->log("Created new attribute: size [$name]", 'database_importer');
                    }
                }
            }
        }


        //carats
        if ($process_karats) {
            $attribute_id = $this->getAttributeIdByCode('carats');

            $builder = DB::table('mi_anagrafica')
                ->where('json', '!=', '')
                ->select(DB::raw('distinct(attr_karats) as attribute'))->orderBy('attribute');

            if ($this->hasLimitSkus()) {
                $builder->whereIn('sku', $this->getLimitSkus());
            }

            $data = $builder->get();
            foreach ($data as $attribute) {
                $name = $attribute->attribute;
                if ($name != '') {
                    $attribute_option_id = $this->resolveAttribute($name, $attribute_id);
                    if ($attribute_option_id) {
                        $this->cacheStore('karat', $name, compact('attribute_option_id', 'attribute_id'));
                    } else {
                        $this->log("Karat $name should be created");
                        $attribute_option_id = $this->createAttributeOption($name, $attribute_id);
                        $this->log("New attribute option created with id: $attribute_option_id");
                        $this->cacheStore('karat', $name, compact('attribute_option_id', 'attribute_id'));
                        $this->logger->log("Created new attribute: karat [$name]", 'database_importer');
                    }
                }
            }
        }
    }


    protected function createCategory($name, $parent_id)
    {
        $name = ucfirst(Str::lower($name));

        $data = [
            'parent_id' => $parent_id,
        ];
        $this->setBlame($data);
        foreach ($this->languages() as $language) {
            $data[$language] = [
                'name' => $name,
                'slug' => Str::slug($name),
                'published' => 1,
            ];
        }
        $this->log($data, __METHOD__);
        $model = new \Category();
        $model->make($data);

        $this->logger->log("Created new category [$name]", 'database_importer');

        return $model->id;
    }


    protected function resolveCategory($name, $parent_id = 0)
    {
        if ($this->nullish($name))
            return null;

        $lower = Str::lower($name);

        /*if ($lower == 'accessori') {
            $name = 'Home & fashion';
        }*/

        if ($lower == 'meccanico carica manuale' or $lower == 'complicazioni meccanico automatico' or $lower == 'meccanico automatico') {
            $name = 'Meccanico';
        }

        if ($lower == 'solo tempo' or $lower == 'tempo') {
            $name = 'Just time';
        }

        if ($lower == 'penna a sfera') {
            $name = 'Penna';
        }

        if ($lower == 'smart') {
            $name = 'smartwatch';
        }

        if ($lower == 'fedine') {
            $name = 'anelli';
        }

        if ($lower == 'penna') {
            $name = 'Penne a sfera';
        }

        if ($lower == 'tempo , data e fasi lunari' or $lower == 'tempo e data' or $lower == 'tempo giorno e data' or $lower == 'data e fasi lunari') {
            $name = 'Multifunzione';
        }

        $slug = Str::slug($name);


        $id = DB::table("categories_lang")->where("slug", $slug)->orWhere('singular', $name)->orWhere('name', $name)->pluck("category_id");
        $category = \Category::find($id);
        if ($category) {
            if ($category->parent_id != $parent_id) {
                $parent_id = $this->findCategoryAncestor($category->id, $category->parent_id);
            }
        }

        return $id > 0 ? compact('id', 'parent_id') : null;
    }


    protected function findCategoryAncestor($id, $parent_id)
    {
        if ($parent_id == 0) {
            return $id;
        }
        $category = \Category::find($parent_id);
        return $this->findCategoryAncestor($category->id, $category->parent_id);
    }


    protected function resolveAttribute($name, $attribute_id)
    {
        $slug = Str::slug(trim($name));
        $query = "select id from attributes_options left join attributes_options_lang on attributes_options.id = attributes_options_lang.attribute_option_id where slug='$slug' and attribute_id=$attribute_id";
        $result = DB::selectOne($query);
        if ($result and $result->id) {
            return $result->id;
        }
        return null;
    }


    protected function resolveAttributeNames($translations)
    {
        $values = array_values($translations);
        $slugs = [];
        foreach ($values as $value) {
            $slugs[] = Str::slug($value, '_');
        }

        $result = DB::table('attributes')
            ->leftJoin('attributes_lang', 'attributes.id', '=', 'attributes_lang.attribute_id')
            ->whereIn('code', $slugs)
            ->orWhereIn('alias', $slugs)
            ->orWhereIn('name', $values)->first();

        if ($result and $result->id) {
            return $result->id;
        }
        /*$str = implode(',', $values);
        $this->logger->warning("There are no existing attributes for [$str]", 'database_importer');*/
        return $this->createAttribute($translations);
    }


    protected function createAttribute($translations)
    {
        $model = new \Attribute();
        $data = [
            'code' => Str::slug(isset($translations['en']) ? $translations['en'] : $translations['it'], '_'),
            'ldesc' => 'Morellato: ' . isset($translations['it']) ? $translations['it'] : $translations['en']
        ];
        foreach ($translations as $locale => $translation) {
            $data[$locale] = [
                'name' => ucfirst(Str::lower($translation)),
                'slug' => Str::slug($translation),
                'published' => 1,
            ];
        }
        $attributes = $model->make($data);
        $str = implode(',', array_values($translations));
        $this->logger->log("Created attribute [$str]", 'database_importer');
        return (isset($model->id)) ? $model->id : null;
    }


    protected function createAttributeOptionTranslations($translations, $attribute_id)
    {
        $name = ucfirst(Str::lower(isset($translations['en']) ? $translations['en'] : $translations['it']));
        $uname = $name;
        $uslug = Str::slug($uname);
        $data = compact('attribute_id', 'uname', 'uslug');
        $this->setBlame($data);
        foreach ($translations as $locale => $name) {
            $data[$locale] = [
                'name' => ucfirst(Str::lower($name)),
                'slug' => Str::slug($name),
                'published' => 1,
            ];
        }
        $model = new \AttributeOption();
        $model->make($data);
        $id = $model->id;
        $str = implode(',', array_values($translations));
        $this->logger->log("Created attribute option [$str] for attribute ($attribute_id)", 'database_importer');
        return $id;
    }

    protected function resolveAttributeOptions($translations, $attribute_id)
    {
        $values = array_values($translations);
        if ($attribute_id == 14) { //water resistance
            return str_replace(['atm', 'at'], '', Str::lower($values[0]));
        }
        $slugs = [];
        foreach ($values as $value) {
            $slugs[] = Str::slug($value);
        }

        $result = DB::table('attributes_options')
            ->leftJoin('attributes_options_lang', 'attributes_options.id', '=', 'attributes_options_lang.attribute_option_id')
            ->where('attribute_id', $attribute_id)
            ->where(function ($query) use ($slugs, $values) {
                $query->whereIn('slug', $slugs)
                    ->orWhereIn('name', $values);
            })->first();


        if ($result and $result->id) {
            return $result->id;
        }
        /*$str = implode(',', $values);
        $this->logger->warning("There are no existing attribute options for [$str] with attribute_id ($attribute_id)", 'database_importer');*/
        return $this->createAttributeOptionTranslations($translations, $attribute_id);
    }


    protected function createAttributeOption($name, $attribute_id)
    {
        $name = ucfirst(Str::lower($name));
        $uname = $name;
        $uslug = Str::slug($uname);
        $data = compact('attribute_id', 'uname', 'uslug');
        $this->setBlame($data);
        foreach ($this->languages() as $language) {
            $data[$language] = [
                'name' => $name,
                'slug' => Str::slug($name),
                'published' => 1,
            ];
        }
        $model = new \AttributeOption();
        $model->make($data);
        $id = $model->id;
        return $id;
    }


    protected function getAttributeIdByCode($code)
    {
        return \Attribute::where('code', $code)->first()->id;
    }


    function createGender($name)
    {
        $name = ucfirst(Str::lower($name));

        $attribute_id = 214;
        $nav_id = \AttributeOption::where('attribute_id', $attribute_id)->max('nav_id') + 1;
        $uname = $name[0];
        $uslug = Str::lower($name[0]);

        $data = compact('attribute_id', 'nav_id', 'uname', 'uslug');

        $this->setBlame($data);
        foreach ($this->languages() as $language) {
            $data[$language] = [
                'name' => $name,
                'slug' => Str::slug($name),
                'published' => 1,
            ];
        }
        $model = new \AttributeOption();
        $model->make($data);
        $id = $model->id;

        //create the alias
        DB::table('mi_attribute_alias')->insert([
            'attribute' => 'gender',
            'attribute_id' => $attribute_id,
            'attribute_option_id' => $id,
            'alias' => Str::upper($name),
        ]);
        return $id;
    }


    public function makeCategories()
    {
        //$filter = $this->source == null ? 'where isnull(source)' : "where source='$this->source'";
        $this->cache['categories'] = [];
        $filter = '';
        if ($this->hasLimitSkus()) {
            $str_skus = implode("','", $this->getLimitSkus());
            $filter = "WHERE mastersku IN (SELECT sap_sku FROM mi_anagrafica WHERE sku IN ('$str_skus') UNION SELECT sap_sku FROM mi_anagrafica_ng WHERE sku IN ('$str_skus'))";
        }
        $query = "select categoria1livello1,categoria1livello2 from mi_categorie $filter GROUP BY categoria1livello1,categoria1livello2 order by categoria1livello2";
        $categories = DB::select($query);
        foreach ($categories as $row) {
            $parent = $this->sanitize($row->categoria1livello1);
            $parent_data = $this->resolveCategory($parent);

            if ($parent_data) {
                $this->cacheStore('categories', $parent, $parent_data);
            } else {
                $this->cacheStore('categories', $parent, $this->createCategory($parent, 0));
            }

            $parent_id = $this->cacheRead('categories', $parent)['id'];

            if (!$this->nullish($row->categoria1livello2)) {
                $categories = $this->explodeString($row->categoria1livello2);
                foreach ($categories as $category) {
                    $data = $this->resolveCategory($category, $parent_id);
                    if ($data) {
                        $this->cacheStore('categories', $category, $data);
                    } else {
                        $this->cacheStore('categories', $category, $this->createCategory($category, $parent_id));
                    }
                }
            }

        }

    }


    protected function doImportVariants($mastersku, $variant)
    {
        try {
            $this->importVariants($mastersku, $variant);
        } catch (\Exception $e) {
            $obj = new \stdClass();
            $obj->sku = $mastersku;
            $this->insertError($e, $obj);
            $this->log($e->getMessage(), __METHOD__);
            $this->log($e->getFile(), __METHOD__);
            $this->log($e->getLine(), __METHOD__);
            $msg = $e->getMessage() . PHP_EOL . $e->getFile() . "(" . $e->getLine() . ')';
            throw new \Exception("Warning: variant with SKU ($obj->sku) could not be imported: " . $msg, $e->getCode());
        }
    }


    function importVariants($mastersku, $variant)
    {
        $this->log("Importing variants for mastersku: $mastersku");
        $this->console->line("Importing variants for mastersku: $mastersku");
        $row = DB::table('mi_anagrafica')->where('sap_sku', $mastersku)->orderBy('sap_sku')->first();
        if (is_null($row)) {
            $row = DB::table('mi_anagrafica')->where('mastersku', $mastersku)->orderBy('sap_sku')->first();
        }
        $field = $variant;
        $matching = $variant;
        if ($variant == 'size') {
            if ($this->nullish($row->size)) {
                $row->size = $this->guessSize($row);
            }
        }
        if ($variant == 'color' OR $variant == 'colors') {
            $field = 'attr_colors';
            $matching = 'colors';
        }

        if (is_null($row)) {
            $this->console->error("Could not find MASTER PRODUCT for mastersku $mastersku");
            $this->logger->error("Could not find MASTER PRODUCT for mastersku $mastersku", 'database_importer');
            return;
        }

        //$row->sku = $row->mastersku;
        list($product, $md5) = $this->product($row);

        //$this->log($product,'MASTER VARIANT');
        //return;
        $masterProduct = \Product::where('mastersku', $mastersku)->first();
        if ($masterProduct) {
            $product_id = $masterProduct->id;
        } else {
            $product_id = $this->importProduct($product, $md5);
        }
        \Product::where('id', $product_id)->update(['is_out_of_production' => 0]);
        $this->console->line("Master product id is: $product_id");

        //Utils::watch();
        //variants
        $combinations = DB::table('mi_anagrafica')
            ->where('mastersku', $mastersku)
            //->where('sku', '<>', $product->sku)
            ->orderBy('sku')->get();

        $many = count($combinations);
        $this->logger->log("Found [$many] combinations for [$product->mastersku]", 'database_importer');
        $this->console->line("Found [$many] combinations for [$product->mastersku]");
        /*$this->log($combinations,'ALL PRODUCT COMBINATIONS');
        return;*/

        //create variants (combinations)
        //$attribute_id = $this->getAttributeIdByCode($variant);
        foreach ($combinations as $combination) {
            if ($this->jsonIsImportable($combination)) {
                if ($this->nullish($combination->size))
                    $combination->size = $this->guessSize($combination);

                $data = [
                    'name' => $variant . ': ' . $combination->$field,
                    'product_id' => $product_id,
                    'sku' => $combination->sku,
                    'sap_sku' => $combination->sap_sku,
                    'ean13' => $combination->ean,
                    'weight' => $combination->weight,
                    'buy_price' => $this->getPriceBySku($combination->sap_sku),
                    'price' => 0,
                ];

                $specialPrices = $this->getSpecialPricesBySku($combination->sap_sku);
                $stocks = $this->getStocksBySku($combination->sap_sku);
                $ng_stocks = $this->getNgStocksBySku($combination->sap_sku);
                $attributes_options_mapping = $this->matchAttributes($matching, $combination->$field);

                $combination->stocks = $stocks;
                $combination->ng_stocks = $ng_stocks;
                $quantity = $this->getProductQuantityByStocks($combination, $product_id);

                $md5 = md5(serialize($data));

                //$action = $this->importAction($combination->sku, $md5);
                $action = 'insert';
                $count = DB::table('products_combinations')->where('product_id', $product_id)->where(function ($query) use ($combination) {
                    $query->where('sku', $combination->sku)->orWhere('sap_sku', $combination->sap_sku)->orWhere('sku', $combination->sap_sku);
                })->count();
                if ($count > 0)
                    $action = 'update';

                $this->log($action, "ImportAction for combination [$combination->sku]");

                $product_combination_id = $this->product_combination_id;

                if ($action == 'insert') {
                    $data['id'] = $product_combination_id;
                    $data['quantity'] = $quantity;
                    $this->log($data, 'PRODUCT COMBINATION INSERT DATA');
                    $this->console->line("Inserted new combination with id: $product_combination_id");

                    //INSERT THE COMBINATION HERE
                    if ($this->write) {
                        DB::table('products_combinations')->insert($data);
                    }
                    $this->product_combination_id++;
                }

                if ($action == 'update' || $action == 'skip') {
                    $model = DB::table('products_combinations')->where('product_id', $product_id)->where(function ($query) use ($combination) {
                        $query->where('sku', $combination->sku)->orWhere('sap_sku', $combination->sap_sku)->orWhere('sku', $combination->sap_sku);
                    })->first();
                    if ($model and $model->id) {
                        $product_combination_id = $model->id;
                    }
                    $data['quantity'] = $quantity;
                    $this->log($data, 'PRODUCT COMBINATION UPDATE DATA');
                    //UPDATE THE COMBINATION HERE
                    if ($this->write) {
                        if ($model and $model->id) {
                            DB::table('products_combinations')->where('id', $product_combination_id)->update($data);
                            $this->console->line("Updated new combination with id: $product_combination_id");
                        } else {
                            DB::table('products_combinations')->insert($data);
                            $this->product_combination_id++;
                            $this->console->line("Inserted new combination with id: $product_combination_id");
                        }
                    }
                }

                $this->log($specialPrices, 'COMBINATION SPECIAL PRICES');
                $this->log($stocks, 'COMBINATION STOCKS');
                $this->importProductStocks($combination, $product_id, $product_combination_id);

                $this->log($attributes_options_mapping, "COMBINATION attributes_options_mapping for [$matching,{$combination->$field}]");

                if (!empty($attributes_options_mapping)) {
                    if ($this->write) {
                        DB::table('products_combinations_attributes')->where('combination_id', $product_combination_id)->delete();
                    }

                    foreach ($attributes_options_mapping as $mapping) {
                        $combination_options = [
                            'combination_id' => $product_combination_id,
                            'option_id' => $mapping['attribute_option_id'],
                            'attribute_id' => $mapping['attribute_id'],
                        ];

                        $this->log($combination_options, 'PRODUCT COMBINATION MAPPING');

                        //INSERT THE MAPPING HERE
                        if ($this->write) {
                            DB::table('products_combinations_attributes')->insert($combination_options);
                        }
                    }
                }
            } else {
                $this->logger->warning("Combination [$combination->sku] could not be imported; json is not compatible", 'database_importer');
            }


        }

    }


    function getPriceBySku($sku)
    {
        $p = DB::table('mi_listino')
            ->where('sku', $sku)
            ->where('tipoprezzo', 'P')
            //->where('source', $this->source)
            ->pluck('prezzo');
        return $this->price($p);
    }


    function getSpecialPricesBySku($sku)
    {
        $rows = DB::table('mi_listino')
            ->where('sku', $sku)
            ->where('tipoprezzo', 'S')
            //->where('source', $this->source)
            ->get();
        $data = [];
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $data[] = [
                    'price' => $this->price($row->prezzo),
                    'date_from' => $row->date_from,
                    'date_to' => $row->date_to,
                ];
            }
        }
        return $data;
    }


    function getStocksBySku($sku)
    {
        $rows = DB::table('mi_giacenza')
            ->where('sku', $sku)
            ->get();
        $data = [];
        foreach ($rows as $row) {
            $data[] = [
                'quantity_24' => (int)$row->giacenzaH24,
                'quantity_48' => (int)$row->giacenzaH48,
                'available_at' => $row->disponibilita_futura == '0000-00-00' ? null : $row->disponibilita_futura,
                'warehouse' => $this->getWarehouseCode($row)
            ];
        }
        return $data;
    }


    function getNgStocksBySku($sku)
    {
        $rows = DB::table('mi_giacenza_ng')
            ->where('sku', $sku)
            ->get();
        $data = [];
        foreach ($rows as $row) {
            $data[] = [
                'quantity_shop' => (int)$row->giacenzaH24,
                'quantity_48' => (int)$row->giacenzaH48,
                'available_at' => $row->disponibilita_futura == '0000-00-00' ? null : $row->disponibilita_futura,
                'warehouse' => 'NG'
            ];
        }
        return $data;
    }


    /**
     * Gets the warehouse id from the imported file name
     * @param mixed $stock
     * @return string
     * */
    private function getWarehouseCode($stock)
    {
        //GIACENZA_03 = 03
        //GIACENZA_01 = 77
        $check = substr($stock->imported_file, 0, 11);
        switch ($check) {
            default:
            case 'GIACENZA_01':
                return '01';
                break;
            case 'GIACENZA_03':
                return '03';
                break;
            case 'GIACENZA_W3':
                return 'W3';
                break;
            case 'GIACENZA_WM':
                return 'WM';
                break;
            case 'NG_GIACENZA':
            case 'GIACENZA_NG':
            case 'GIACENZA_BT':
                return 'NG';
                break;
        }
    }


    function guessSize($product)
    {
        if ($this->nullish($product->size)) {
            $guessSize = ltrim(str_replace($product->mastersku, '', $product->sku), '0');
            if (is_numeric($guessSize)) {
                return $guessSize;
            }
        }
        return null;
    }


    function cacheStore($tag, $key, $value)
    {
        $this->cache[$tag][Str::slug($key)] = $value;
    }

    function cacheRead($tag, $key, $default = null)
    {
        $key = Str::slug($key);
        return isset($this->cache[$tag][$key]) ? $this->cache[$tag][$key] : $default;
    }


    protected function nullish($str)
    {
        $str = trim($str);
        return ($str == '' or is_null($str) or $str == 'null' or $str == '.' or str_contains(Str::upper($str), 'TO BE DEFINED'));
    }


    protected function sanitize($str)
    {
        $str = str_replace(['\\', ':', ';', '#', '?'], '', trim($str));
        return $str;
    }


    protected function importAction($sku, $sap_sku, $md5)
    {
        if (trim($sap_sku) === '')
            $sap_sku = null;

        $row = DB::table('mi_migrations')->where('sku', $sku)->orWhere('sap_sku', $sap_sku)->first();
        $exists = \Product::where('sku', $sku)->orWhere('sap_sku', $sap_sku)->count() > 0;
        if ($row and $exists) {
            if ($row->md5 == $md5) {
                return 'skip';
            }
            return 'update';
        }
        return 'insert';
    }


    private function price($val)
    {
        if ($val <= 0) return 0;
        return number_format((float)$val, 3, '.', '');
    }


    private function listToArray($string)
    {
        $array = [];
        $dom = new \DOMDocument;
        $dom->loadHTML('<?xml encoding="utf-8" ?>' . $string);
        foreach ($dom->getElementsByTagName('li') as $node) {
            $value = trim($node->nodeValue);
            if ($value != '' AND !is_null($value))
                $array[] = $node->nodeValue;
        }
        return $array;
    }


    private function parseHtml($html)
    {
        if ($html == '<ul><li></li></ul>')
            return null;

        return trim(strip_tags($html));
    }


    public function parseCanvas($canvas)
    {
        return \ProductHelper::parseCanvas($canvas);
    }


    private function parsePrices($price)
    {
        $price_nt = $this->price($price / 1.22);
        return [
            'price' => $price,
            'sell_price_wt' => $price,
            'sell_price' => $price_nt,
            'price_nt' => $price_nt,
        ];
    }


    protected function explodeString($str, $delimiter = ',')
    {
        $data = [];
        $tokens = explode($delimiter, $str);
        foreach ($tokens as $token) {
            $token = trim($token);
            if (!$this->nullish($token))
                $data[] = trim($token);
        }
        return $data;
    }


    private function insertMigration($sku, $sap_sku, $md5)
    {
        if (trim($sap_sku) == '' or $sap_sku == 'null')
            $sap_sku = null;

        $data = compact('sku', 'sap_sku', 'md5');
        $data['imported_at'] = date('Y-m-d H:i:s');
        if ($this->write) {
            try {
                DB::table('mi_migrations')->insert($data);
            } catch (Exception $e) {

            }
        }
        return $data;
    }


    private function updateMigration($sku, $sap_sku, $md5)
    {
        if (trim($sap_sku) == '' or $sap_sku == 'null')
            $sap_sku = null;

        $data = compact('sku', 'sap_sku', 'md5');
        $data['imported_at'] = date('Y-m-d H:i:s');
        if ($this->write) {
            DB::table('mi_migrations')->where('sku', $sku)->orWhere('sap_sku', $sap_sku)->update($data);
        }
        return $data;
    }


    private function upsertMigration($sku, $sap_sku, $md5)
    {
        if (trim($sap_sku) == '')
            $sap_sku = 'null';

        $count = DB::table('mi_migrations')->where('sku', $sku)->orWhere('sap_sku', $sap_sku)->count();
        return $count > 0 ? $this->updateMigration($sku, $sap_sku, $md5) : $this->insertMigration($sku, $sap_sku, $md5);
    }


    private function insertError(\Exception $exception, $product)
    {
        $data = [
            'message' => $exception->getMessage(),
            'stack' => $exception->getTraceAsString(),
            'sku' => isset($product->sku) ? $product->sku : null,
            'data' => serialize($product),
        ];
        $data['created_at'] = date('Y-m-d H:i:s');
        if ($this->write) {
            DB::table('mi_errors')->insert($data);
        }
        return $data;
    }


    function bindSapSkus()
    {
        $this->console->info('Executing bindSapSkus');
        //this function only serves to bind SKU with SAP SKU
        $rows = DB::select("SELECT sku,sap_sku FROM mi_anagrafica WHERE sap_sku <> '' OR !ISNULL(sap_sku) ORDER BY sap_sku");

        $many = count($rows);
        $this->console->line("Found [$many] to parse");
        $counter = 0;
        foreach ($rows as $row) {
            $this->dbCheckout($counter, $many);
            $counter++;
            $sku = $this->sanitize($row->sku);
            $sap_sku = $this->sanitize($row->sap_sku);
            if ($sku != '') {
                DB::table('products')->where('sku', $sku)->update(['sap_sku' => $sap_sku]);
                DB::table('products_combinations')->where('sku', $sku)->update(['sap_sku' => $sap_sku]);
                DB::table('order_details')->where('product_reference', $sku)->update(['product_sap_reference' => $sap_sku]);
                $this->console->line("Update sap_sku ($counter/$many)");
            }
        }
        $this->dbCheckout(-1, $many);
    }


    function bindRelations()
    {
        $this->console->comment('Building catalog cache');
        $this->buildCatalogCache();
        //in 'full mode' CSV import there is no need to re-bind relations
        if (config('ftp.csvImportMode') == 1)
            return;

        $this->console->comment('Executing bindRelations');
        $this->makeCategories();
        //audit($this->cache, 'CACHE');
        $this->console->info('DONE with categories');

        $rows = DB::table('mi_anagrafica')
            ->where(function ($query) {
                $query->where('sap_sku', '=', DB::raw('mastersku'));
                $query->orWhere('mastersku', '=', '');
            })
            ->where('json', '!=', '')
            //->where('sku', 'J010669')
            ->orderBy('json', 'desc')
            ->orderBy('created_at', 'asc')
            //->take(10)
            ->get();

        $many = count($rows);
        $this->console->line("Founded [$many] products to process");

        $counter = 0;
        foreach ($rows as $row) {
            $this->dbCheckout($counter, $many);
            try {
                $counter++;
                $this->productSimpleBind($row);
                $this->console->line("Processed [$counter/$many]");
            } catch (Exception $e) {
                $this->console->error($e->getMessage());
            }
        }
        $this->dbCheckout(-1, $many);
        audit($this->skus, 'MISSING SKUS');
        $this->warnAboutMissingProducts();
    }


    protected function productSimpleBind($product)
    {

        $oldModel = $this->getProductModelBySku($product, true);
        if (is_null($oldModel)) {
            $this->console->error("Could not find any product with sku [$product->sku]");
            $this->skus[] = $product->sku;
            return $product;
        }

        $canvas = $this->parseCanvas($product->canvas);

        $active_languages = \Core::getLanguages();
        $translations = DB::table('mi_traduzioni')
            ->where('sku', $product->sku)
            ->whereIn('lang_id', $active_languages)
            ->orderBy('lang_id', 'desc')
            ->get();

        $storage = [];

        foreach ($translations as $translation) {
            $lang_id = strtolower($translation->lang_id);
            $product_translation = (object)[
                'lang_id' => $lang_id,
                'sdesc' => $this->parseHtml($translation->commercial_description),
            ];

            if ($product_translation->sdesc == '')
                $product_translation->sdesc = $this->parseHtml($product->commercial_description);

            if ($lang_id != 'it' and $product_translation->sdesc == '' and isset($storage['it']) and trim($oldModel->$lang_id->sdesc) == '') {
                $desc = trim($storage['it']->sdesc);
                if (strlen($desc) > 0) {
                    $results = $this->translateGrabber->reset()->addItem($desc)->translate($lang_id);
                    $this->console->line("Translating in $lang_id [$desc]...");
                    $product_translation->sdesc = $results[0];
                }
            }

            $storage[$lang_id] = $product_translation;
        }

        $categoryRelation = [];
        $givenCategoryInfo = DB::table('mi_categorie')->where('mastersku', $product->mastersku)->first();
        //check with sap_sku
        if (is_null($givenCategoryInfo))
            $givenCategoryInfo = DB::table('mi_categorie')->where('mastersku', $product->sap_sku)->first();

        if ($givenCategoryInfo) {
            if (!$this->nullish($givenCategoryInfo->categoria1livello2)) {
                $categories = $this->explodeString($givenCategoryInfo->categoria1livello2);
                foreach ($categories as $category) {
                    $categoryRelation[] = $this->cacheRead('categories', $category);
                }
            } else {
                $categoryRelation[] = $this->cacheRead('categories', $givenCategoryInfo->categoria1livello1);
            }
        }
        if (empty($categoryRelation)) {
            $categoryRelation[] = $this->cacheRead('categories', $product->attr_category);
        }
        $relations = [];
        $relations['category'] = $categoryRelation;

        $product->relations = $relations;

        $product->translations = $storage;

        //audit($product, __METHOD__);

        $this->console->line("Prepare to (update) product with sku [$product->sku]");
        $data = [
            'default_category_id' => $product->relations['category'][0]['id'],
            'main_category_id' => $product->relations['category'][0]['parent_id'],
            'new_from_date' => $canvas['new_from_date'],
            'new_to_date' => $canvas['new_to_date'],
        ];

        if ($data['main_category_id'] == 0)
            $data['main_category_id'] = $data['default_category_id'];

        //audit($data, 'DATA TO UPDATE');
        foreach ($oldModel->translations as $translation) {
            //audit($translation);
            $sdesc_translation = $product->translations[$translation->lang_id]->sdesc;
            if (trim($translation->sdesc) == '' and trim($sdesc_translation) != '') {
                DB::table('products_lang')->where(['lang_id' => $translation->lang_id, 'product_id' => $translation->product_id])->update([
                    'sdesc' => $sdesc_translation,
                ]);
            }
        }
        DB::table('products')->where('id', $oldModel->id)->update($data);

        $this->importProductCategories($product, $oldModel->id);


        return $product;
    }


    function warnAboutMissingProducts()
    {
        if (count($this->skus) > 0) {
            $list = implode('<br>', $this->skus);
            \Config::set('mail.pretend', false);
            $name = \Cfg::get('SHORTNAME');
            $from = \Cfg::get('MAIL_GENERAL_ADDRESS');

            $recipient_internal = 'f.politi@m.icoa.it';

            $reason = "There are new products in the CSV that are not contained in the database.";

            $body = $reason . '<hr>' . $list;

            \Mail::send('emails.default', compact('body'), function ($mail) use ($recipient_internal, $from, $name) {
                $mail->from($from);
                $mail->subject($name . ' - warnAboutMissingProducts');
                $mail->to($recipient_internal);
            });
        }
    }


    function dbCheckout($counter, $many, $transactions_flush = 250)
    {
        if ($counter == -1) {
            $this->console->info("==> DB::commit.last");
            DB::commit();
        }

        if ($counter == 0) {
            $this->console->comment("==> DB::beginTransaction.first");
            DB::beginTransaction();
        }

        if ($counter > 0 and $counter % $transactions_flush == 0) {
            $this->console->info("==> DB::commit");
            DB::commit();
            if ($counter < $many) {
                $this->console->comment("==> DB::beginTransaction");
                DB::beginTransaction();
            }
        }
    }
}