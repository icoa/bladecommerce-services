<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 06/02/2017
 * Time: 09:53
 */

namespace services\Morellato\Database;

use Carbon\Carbon;
use DB, Utils, Config, Exception;
use Product, ProductHelper;
use Illuminate\Console\Command;
use services\Models\BuilderProduct;
use services\Morellato\Loggers\DatabaseLogger as Logger;
use Illuminate\Support\Str;
use ProductSpecificStock;

class Optimizer
{

    protected $console;
    protected $logger;
    protected $debug = true;
    protected $write = true;
    protected $bot_user_id = 1;
    protected $fpNoImages = false;
    protected $fpNoStocks = true;
    protected $soldOutEnabled = false;

    function __construct(Command $console, Logger $logger)
    {
        $this->console = $console;
        $this->logger = $logger;
        $this->fpNoImages = \Config::get('ftp.fpNoImages', true);
        $this->fpNoStocks = \Config::get('ftp.fpNoStocks', true);
        $this->soldOutEnabled = \Config::get('ftp.soldOutEnabled', false);
    }


    //rebuild product cache for each imported product
    function make($mode = 'default')
    {
        if ($mode == 'default') {
            $this->bot_user_id = Config::get('ftp.botUserId');

            $this->console->line("Executing publishEntities");
            $this->fixMissingGroupsForCustomers();
            $this->publishEntities();
            $this->console->line("FINISH - Executing publishEntities");


            $this->console->line("Executing disableProducts");
            $this->disableProducts();
            $this->console->line("FINISH - Executing disableProducts");


            $this->console->line("Executing setSeoFields");
            $this->setSeoFields();
            $this->console->line("FINISH - Executing setSeoFields");


            $this->console->line("Executing setShopQuantities");
            $this->setShopQuantities();
            $this->console->line("FINISH - Executing setShopQuantities");


            $this->console->line("Executing scanProducts");
            $this->scanProducts();
            $this->console->line("FINISH - Executing scanProducts");


            $this->console->line("Executing rebuildCache");
            $this->rebuildCache();
            $this->console->line("FINISH - Executing rebuildCache");
        } else {
            $this->console->line("Executing handleMetadata");
            $this->handleMetadata();
            $this->console->line("FINISH - Executing handleMetadata");
            $this->console->line("Executing handleOuterRelations");
            $this->handleOuterRelations();
            $this->console->line("FINISH - Executing handleOuterRelations");
        }
    }


    protected function rebuildCache()
    {
        $yesterday = Carbon::yesterday();
        $rows = \Product::where('updated_at', '>=', $yesterday->format('Y-m-d H:i:s'))->get();
        $many = count($rows);
        $this->console->comment("Found [$many] products to process");
        $counter = 0;
        foreach ($rows as $row) {
            $this->dbCheckout($counter, $many);
            $counter++;
            $this->console->line("[rebuildCache] Processing product [$row->id] ($counter/$many)");
            $row->uncache();
        }
        $this->dbCheckout(-1, $many);
        $this->console->info("Product cache rebuilded");
    }


    protected function publishEntities()
    {
        $ids = DB::table('products')->whereNull('deleted_at')->where('is_out_of_production', 0)->lists('brand_id');
        DB::table('brands_lang')->whereIn('brand_id', $ids)->update(['published' => 1]);
        DB::table('brands_lang')->whereNotIn('brand_id', $ids)->update(['published' => 0]);
    }


    protected function scanProducts()
    {
        //remove from cache_products all products that are out-of-production
        DB::table('cache_products')->whereIn('id', function ($query) {
            $query->select('id')->from('products')->where('is_out_of_production', 1);
        })->delete();

        //now get all in-stocks products that have been recently updated
        $yesterday = Carbon::yesterday();
        $products_updated_ids = Product::where('updated_at', '>=', $yesterday->format('Y-m-d H:i:s'))
            ->where('is_out_of_production', 0)
            ->orderBy('id', 'desc')
            ->lists('id');

        $products_to_cache = Product::where('is_out_of_production', 0)
            ->whereNotIn('id', function ($query) {
                $query->select('id')->from('cache_products');
            })
            ->orderBy('id', 'desc')
            ->lists('id');

        $products = array_unique(array_merge($products_updated_ids, $products_to_cache));

        $many = count($products);
        $this->console->comment("Found [$many] products to process");
        $this->logger->log("Found [$many] products to optimize", 'cache_optimizer');

        $counter = 0;

        foreach ($products as $product_id) {
            $this->dbCheckout($counter, $many);
            $counter++;
            $this->console->line("[scanProducts] Processing product [$product_id] ($counter/$many)");
            try {
                ProductHelper::buildProductCache($product_id, false);
            } catch (\Exception $e) {
                $this->console->error($e->getMessage());
                $this->console->error($e->getTraceAsString());
            }
        }
        $this->dbCheckout(-1, $many);


        $this->console->info("[$counter] products optimized correctly");
        $this->logger->log("$counter products optimized correctly", 'cache_optimizer');


        $products_created_ids = Product::where('created_at', '>=', $yesterday->format('Y-m-d H:i:s'))
            ->where('is_out_of_production', 0)
            ->orderBy('id', 'desc')
            ->lists('id');

        $products = array_unique($products_created_ids);

        $many = count($products);
        $this->console->comment("Found [$many] products to process");
        $this->logger->log("Found [$many] products to optimize", 'cache_optimizer');

        $counter = 0;

        foreach ($products as $product_id) {
            $this->dbCheckout($counter, $many);
            $counter++;
            $this->console->line("[scanProducts] Processing product [$product_id] ($counter/$many)");
            try {
                ProductHelper::buildProductCache($product_id, true);
            } catch (\Exception $e) {
                $this->console->error($e->getMessage());
                $this->console->error($e->getTraceAsString());
            }
        }
        $this->dbCheckout(-1, $many);

    }


    protected function setTrend()
    {
        $mpAttributeId = Config::get('ftp.mpAttributeId');
        $trend_id = Config::get('ftp.mpTrendId');
        /*
         * matching
         * Idee per i Regali di Anniversario e di Matrimonio
         * ID: 15
         * Values: ANNIVERSARIO, MATRIMONIO, FIDANZAMENTO
         *
         *
         * Idee per i Regali di Laurea
         * ID: 14
         * Values: LAUREA
         *
         * Idee e Regali per Comunioni e Cresime
         * ID: 8
         * Values: COMUNIONE, CRESIMA
         *
         * Idee regali di Natale
         * ID: 19
         * Values: NATALE
         *
         * Idee Regalo
         * ID: 48
         * Values: all default
         * */

        Utils::watch();
        $matches = Config::get('ftp.mp_matching');

        $products = DB::table('mi_anagrafica')->where('attr_moments', '<>', '')->select('mastersku', 'attr_moments')->get();

        $many = count($products);
        $this->logger->log("Found [$many] products to enable as 'MOMENTI PREZIOSI'", 'trend_importer');
        if ($many == 0) {
            return;
        }

        $counter = 0;
        foreach ($products as $row) {
            $product_id = Product::where('sku', $row->mastersku)->pluck('id');
            $trend_id = isset($matches[trim($row->attr_moments)]) ? $matches[trim($row->attr_moments)] : null;
            $this->console->line("[$row->mastersku] PID: $product_id | TID: $trend_id");
            try {
                if ($product_id > 0 and $trend_id > 0) {
                    DB::table('trends_products')->insert(compact('trend_id', 'product_id'));
                    $this->console->info("Product [$row->mastersku] associated with trend [$trend_id]");
                    $this->logger->log("Product [$row->mastersku] associated with trend [$trend_id]", 'trend_importer');
                }
            } catch (Exception $e) {

            }

            //catch all
            $trend_id = $matches['*'];
            try {
                if ($product_id > 0 and $trend_id > 0) {
                    DB::table('trends_products')->insert(compact('trend_id', 'product_id'));
                    $counter++;
                    $this->console->info("Product [$row->mastersku] associated with trend [$trend_id]");
                    $this->logger->log("Product [$row->mastersku] associated with trend [$trend_id]", 'trend_importer');
                }
            } catch (Exception $e) {

            }

        }
        $this->console->info("[$counter] products associated with 'Momenti Preziosi'");
        $this->logger->log("[$counter] products associated with 'Momenti Preziosi'", 'trend_importer');
        //DB::commit();
    }


    protected function setSeoFields()
    {
        \Utils::watch();
        $this->console->line("Binding SEO fields for products");
        $rows = \Product_Lang::whereIn('product_id', function ($query) {
            $query->select('id')
                ->from('products')
                ->whereNull('deleted_at')
                ->where('is_out_of_production', 0);
        })->where(function ($query) {
            $query->whereNull('metadescription')
                ->orWhereNull('metatitle')
                ->orWhereNull('metakeywords');
        })->select(DB::raw('distinct(product_id) as id'))
            ->orderBy('product_id', 'desc')
            ->get();


        foreach ($rows as $row) {
            $seo_fields = \ProductHelper::generateSeo($row->id, true);
            //Utils::log($seo_fields, __METHOD__);
            try {
                foreach ($seo_fields as $locale => $data) {
                    $update = [
                        'metatitle' => $data['title'],
                        'h1' => $data['h1'],
                        'metakeywords' => $data['keywords'],
                        'metadescription' => $data['description'],
                    ];
                    DB::table('products_lang')->where('product_id', $row->id)->where('lang_id', $locale)->update($update);
                    DB::table('products')->where('id', $row->id)->update(['updated_at' => date('Y-m-d H:i:s')]);
                }
                $this->console->info("Updated SEO for product [$row->id]");
            } catch (\Exception $e) {
                $this->console->error($e->getMessage());
                $this->logger->error("Could not bind SEO fields for product [$row->id]: " . $e->getMessage());
            }
        }
    }


    protected function disableProducts()
    {
        DB::beginTransaction();
        $now = date('Y-m-d H:i:s');
        $now_month = (int)date('m');
        $now_day = (int)date('d');

        if ($this->fpNoStocks) {

            //get all in-production product ids
            $ids_online = DB::table('products')->whereNull('deleted_at')->where('source', 'morellato')->where('type', 'default')->whereIn('sap_sku', function ($query) {
                $query->select('sku')->from('mi_giacenza');
            })->lists('id');

            $ids_shops = DB::table('products')->whereNull('deleted_at')->where('source', 'morellato')->where('type', 'default')->whereIn('sap_sku', function ($query) {
                $query->select('sku')->from('mi_giacenza_ng');
            })->lists('id');

            $in_stocks_ids = array_unique(array_merge($ids_online, $ids_shops));
            //now update only the relevant products

            if ($this->soldOutEnabled) {
                //revert query: UPDATE products SET is_out_of_production=1, updated_by=1, updated_at=CURRENT_TIMESTAMP() WHERE is_soldout=1 and isnull(deleted_at);
                //revert query: DELETE FROM cache_products WHERE is_soldout=1;
                /**
                 * Remember the 'out-of-stocks' products, for later use
                 */
                $ids_out_of_stocks = DB::table('products')
                    ->whereNull('deleted_at')
                    ->where('source', 'morellato')
                    ->where('type', 'default')
                    ->where('is_out_of_production', 1)
                    ->lists('id');
                /*
                 * - Un prodotto non piú rappresentato nei file di giacenza entra nello stato di Sold out
                 * - Ogni cambio di Canvas tutti i prodotti sold out si trasformano in fuori produzione (date di passaggio: 15/02 - 15/04 - 15/06 - 15/08 - 15/10 - 15/12)
                 * */
                //remove the 'soldout' flag to all 'available' products
                $this->console->line("Removing the 'soldout' flag to all 'available' products");
                DB::table('products')
                    ->whereIn('id', $in_stocks_ids)
                    ->whereNull('deleted_at')
                    ->where('source', 'morellato')
                    ->where('type', 'default')
                    ->update(['is_soldout' => 0, 'updated_at' => $now, 'is_out_of_production' => 0]);
                $this->console->info('-> DONE');

                //enable the 'soldout' flag to all 'NOT-available' products
                $this->console->line("Adding the 'soldout' flag to all 'NOT-available' products");
                DB::table('products')
                    ->whereNotIn('id', $in_stocks_ids)
                    ->whereNull('deleted_at')
                    ->where('source', 'morellato')
                    ->where('type', 'default')
                    ->where('is_out_of_production', 0)
                    ->update(['is_soldout' => 1, 'updated_at' => $now, 'qty' => 0]);
                $this->console->info('-> DONE');

                $switch_day = 15;
                $switch_months = [2, 4, 6, 8, 10, 12];
                if (in_array($now_month, $switch_months) and $now_day == $switch_day) {
                    $this->console->line("Sold-out switching time");
                    DB::table('products')
                        ->whereNull('deleted_at')
                        ->where('source', 'morellato')
                        ->where('type', 'default')
                        ->where('is_out_of_production', 0)
                        ->where('is_soldout', 1)
                        ->update(['is_out_of_production' => 1]);
                    $this->console->info('-> DONE');
                }

                //all products that were out-of-stocks before this moment, and now are 'sold-out' but 'in-stocks', let's keep them 'out-of-stocks'
                DB::table('products')
                    ->whereNull('deleted_at')
                    ->where('source', 'morellato')
                    ->where('type', 'default')
                    ->where('is_out_of_production', 0)
                    ->where('is_soldout', 1)
                    ->whereIn('id', $ids_out_of_stocks)
                    ->update(['is_out_of_production' => 1]);

                //all products that are mapped to 'affiliate membership' should not be 'soldout'
                if (feats()->enabled('membership')) {
                    Product::inStocksForAffiliates()->update(['is_soldout' => 0, 'is_out_of_production' => 0]);
                }

                //instantly update cache_products
                DB::table('cache_products')->whereIn('id', function ($query) {
                    $query->select('id')->from('products')->whereNull('deleted_at')->where('is_soldout', 0);
                })->update(['is_soldout' => 0]);

                DB::table('cache_products')->whereIn('id', function ($query) {
                    $query->select('id')->from('products')->whereNull('deleted_at')->where('is_soldout', 1);
                })->update(['is_soldout' => 1]);

            } else {
                DB::table('products')
                    ->whereNotIn('id', $in_stocks_ids)
                    ->whereNull('deleted_at')
                    ->where('source', 'morellato')
                    ->where('type', 'default')
                    ->update(['is_out_of_production' => 1]);

                DB::table('products')
                    ->whereIn('id', $in_stocks_ids)
                    ->whereNull('deleted_at')
                    ->where('source', 'morellato')
                    ->where('type', 'default')
                    ->update(['is_out_of_production' => 0, 'updated_at' => $now]);
            }


        }

        //disable all products with no affiliate stocks
        if (\Core::membership()) {
            $ids = Product::inStocksForAffiliates()->select('id', 'sku')->lists('id');
            Product::whereIn('id', $ids)->update(['is_out_of_production' => 0, 'updated_at' => $now]);
        }

        if ($this->fpNoImages) {
            DB::table('products')
                ->where('type', 'default')
                ->where('default_img', 0)
                ->update(['is_out_of_production' => 1, 'updated_at' => $now]);
        }

        //instantly update cache_products
        DB::table('cache_products')->whereIn('id', function ($query) {
            $query->select('id')->from('products')->whereNull('deleted_at')->where('is_out_of_production', 1);
        })->delete();


        //take care of the 'builder' products
        BuilderProduct::whereYear('deleted_at', '=', 2020)->restore();
        BuilderProduct::whereIn('product_id', function($query){
            $query->select('id')->from('products')->whereNull('deleted_at')->where(function($subquery){
                $subquery->where('is_out_of_production', 1)->orWhere('is_soldout', 1);
            });
        })->delete();


        DB::commit();
    }


    protected function setShopQuantities()
    {
        $query = "select 
                    products.sku,
                    products.id,
                    mi_giacenza_ng.giacenzaH24 as qty 
                    from mi_giacenza_ng 
                    join products on mi_giacenza_ng.sku=products.sap_sku
                    UNION 
                  select 
                    products.sku,
                    products.id,
                    mi_giacenza.giacenzaH24 as qty 
                    from mi_giacenza 
                    join products on mi_giacenza.sku=products.sap_sku 
                    where imported_file like 'GIACENZA_W3%'
                    ";
        $rows = DB::select($query);
        $many = count($rows);
        $this->console->line("Found [$many] to process...");

        $counter = 0;
        $cache = [];
        $in_stocks_ids = [];
        foreach ($rows as $row) {
            $this->dbCheckout($counter, $many);
            $counter++;
            //$this->console->line("Set shop quantity [$row->qty] for product [$row->id]");
            $qty = isset($cache[$row->id]) ? $cache[$row->id] + $row->qty : $row->qty;
            $cache[$row->id] = $qty;
            $in_stocks_ids[] = $row->id;
            DB::table('products_specific_stocks')
                ->where('product_id', $row->id)
                ->whereNull('product_combination_id')
                ->update(['quantity_shop' => $qty]);
            $this->console->line("Saved shop quantity [$counter/$many]");
        }
        $this->dbCheckout(-1, $many);

        //delete all stocks that are not contained, since they cannot hold in the db
        DB::table('products_specific_stocks')
            ->whereNotIn('product_id', $in_stocks_ids)
            ->whereNull('product_combination_id')
            ->where('quantity_shop', '>', 0)
            ->update(['quantity_shop' => 0]);

        $query = "select 
                    products_combinations.sku,
                    products_combinations.id,
                    products_combinations.product_id,
                    mi_giacenza_ng.giacenzaH24 as qty 
                    from mi_giacenza_ng 
                    join products_combinations on mi_giacenza_ng.sku=products_combinations.sap_sku
                    UNION
                  select 
                    products_combinations.sku,
                    products_combinations.id,
                    products_combinations.product_id,
                    mi_giacenza.giacenzaH24 as qty 
                    from mi_giacenza 
                    join products_combinations on mi_giacenza.sku=products_combinations.sap_sku
                    where imported_file like 'GIACENZA_W3%'
                    ";
        $rows = DB::select($query);
        $many = count($rows);
        $this->console->line("Found [$many] to process...");
        $counter = 0;
        $cache = [];
        $in_stocks_ids = [];
        foreach ($rows as $row) {
            $this->dbCheckout($counter, $many);
            $counter++;
            //$this->console->line("Set shop quantity [$row->qty] for product [$row->id] and combination [$row->id]");
            $qty = isset($cache[$row->id]) ? $cache[$row->id] + $row->qty : $row->qty;
            $cache[$row->id] = $qty;
            $in_stocks_ids[] = $row->id;
            DB::table('products_specific_stocks')
                ->where('product_id', $row->product_id)
                ->where('product_combination_id', $row->id)
                ->update(['quantity_shop' => $qty]);
            $this->console->line("Saved shop quantity [$counter/$many]");
        }
        $this->dbCheckout(-1, $many);

        //delete all combinations stocks that are not contained, since they cannot hold in the db
        DB::table('products_specific_stocks')
            ->whereNotIn('product_combination_id', $in_stocks_ids)
            ->where('product_combination_id', '>', 0)
            ->where('quantity_shop', '>', 0)
            ->update(['quantity_shop' => 0]);

        //now update all products quantities that are contained
        $rows = ProductSpecificStock::select(DB::raw('distinct(product_id)'))->get();
        $many = count($rows);
        $this->console->line("Found [$many] to process...");
        $counter = 0;
        foreach ($rows as $row) {
            $this->dbCheckout($counter, $many);
            $counter++;
            $this->console->line("Setting overall product quantity [$counter/$many]");
            $qty = ProductSpecificStock::getOverallProductQuantity($row->product_id);
            DB::table('products')->where('id', $row->product_id)->update(['qty' => $qty]);
        }
        //set all quantity to 0 for soldout products
        DB::table('products')->where('is_soldout', 1)->update(['qty' => 0]);
        $this->dbCheckout(-1, $many);
    }


    function handleOuterRelations()
    {

        //now get all in-stocks products that have been recently updated
        $yesterday = Carbon::yesterday();
        $products = Product::where('updated_at', '>=', $yesterday->format('Y-m-d H:i:s'))
            ->where('is_out_of_production', 0)
            //->take(100)
            ->orderBy('id', 'desc')
            ->get();
        $many = count($products);
        $this->console->comment("Found [$many] products to process");

        $counter = 0;

        foreach ($products as $product) {

            $this->console->line("[handleOuterRelations] Processing product [$product->id] ($counter/$many)");

            try {
                $this->dbCheckout($counter, $many);
                //ProductHelper::handleMetadata($product);
                ProductHelper::generateImagesSetsCacheForProduct($product);
            } catch (\Exception $e) {
                $this->console->error($e->getMessage());
                $this->console->error($e->getTraceAsString());
                //DB::rollBack();
            }
            $counter++;
        }

        $this->dbCheckout(-1, $many);

        $this->console->info("[$counter] products updated correctly");
    }


    function fixMissingGroupsForCustomers()
    {
        DB::table('customers')->where('default_group_id', 0)->update(['default_group_id' => 3]);
    }


    function handleMetadata(){
        audit_watch();
        $products = Product::rows()->where(function ($query){
            $query->whereNull('metadata')->orWhereNull('indexable')->orWhereNull('attributes');
        })
            ->where('is_out_of_production', 0)
            ->orderBy('id', 'desc')
            ->get();
        $many = count($products);
        $this->console->comment("Found [$many] products to process");

        $counter = 0;

        foreach ($products as $product) {

            $this->console->line("[handleMetadata] Processing product [$product->id] ($counter/$many)");

            try {
                $this->dbCheckout($counter, $many);
                ProductHelper::handleMetadata($product);
            } catch (\Exception $e) {
                $this->console->error($e->getMessage());
                $this->console->error($e->getTraceAsString());
                //DB::rollBack();
            }
            $counter++;
        }

        $this->dbCheckout(-1, $many);

        $this->console->info("[$counter] products updated correctly");
    }


    function dbCheckout($counter, $many, $transactions_flush = 250){
        if($counter == -1){
            $this->console->info("==> DB::commit.last");
            DB::commit();
        }

        if($counter == 0){
            $this->console->comment("==> DB::beginTransaction.first");
            DB::beginTransaction();
        }

        if($counter > 0 and $counter % $transactions_flush == 0){
            $this->console->info("==> DB::commit");
            DB::commit();
            if($counter < $many){
                $this->console->comment("==> DB::beginTransaction");
                DB::beginTransaction();
            }
        }
    }

}