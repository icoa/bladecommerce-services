<?php

namespace services\Morellato\Database;

use DB, Utils, Config, Exception, Cache;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Category;
use Brand;
use Collection;
use Product;
use Mainframe;

class WarmUpper
{

    protected $console;
    protected $debug = true;
    protected $languages = [];
    protected $brands_ids = [];


    function __construct(Command $console)
    {
        $this->console = $console;
    }

    function log($var, $message = null)
    {
        if ($this->debug)
            Utils::log($var, $message);
    }

    function make()
    {
        $this->languages = Mainframe::languagesCodes();
        $this->warmCategories();
        $this->warmBrands();
        $this->warmCollections();
        $this->warmProducts();
    }

    private function warmCategories()
    {
        $this->console->comment("Executing " . __METHOD__);
        $rows = Category::select('id')->get();
        foreach ($this->languages as $language) {
            foreach ($rows as $row) {
                $obj = Category::getPublicObj($row->id, $language);
                if ($obj) {
                    $this->console->line("Category [$row->id][$language] warmed-up");
                }
            }
        }
    }

    private function warmBrands()
    {
        $this->console->comment("Executing " . __METHOD__);
        $rows = Brand::select('id')->get();
        foreach ($this->languages as $language) {
            foreach ($rows as $row) {
                $obj = Brand::getPublicObj($row->id, $language);
                if ($obj) {
                    $this->console->line("Brand [$row->id][$language] warmed-up");
                    $this->brands_ids[] = $row->id;
                }
            }
        }
    }

    private function warmCollections()
    {
        $this->console->comment("Executing " . __METHOD__);
        $rows = Collection::whereIn('brand_id', $this->brands_ids)->select('id')->get();
        foreach ($this->languages as $language) {
            foreach ($rows as $row) {
                $obj = Collection::getPublicObj($row->id, $language);
                if ($obj) {
                    $this->console->line("Collection [$row->id][$language] warmed-up");
                }
            }
        }
    }

    private function warmProducts()
    {
        $this->console->comment("Executing " . __METHOD__);
        $rows = Product::inStocks()->select('id')->get();
        foreach ($this->languages as $language) {
            foreach ($rows as $row) {
                $obj = Product::getFullProduct($row->id, $language);
                if ($obj) {
                    $this->console->line("Product [$row->id][$language] warmed-up");
                    $this->brands_ids[] = $row->id;
                }
            }
        }
    }
}