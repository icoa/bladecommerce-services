<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 22/03/2017
 * Time: 11:34
 */

namespace services\Morellato\Database\Sql;

use Core\Condition\LocalDebug;
use PDO;
use Config;
use services\Traits\TraitParams;
use Utils;
use Illuminate\Console\Command;
use services\Morellato\Loggers\DatabaseLogger as Logger;
use DB;
use Order;
use OrderDetail;
use OrderState;
use Carbon\Carbon;

class Connection extends LocalDebug
{
    use TraitParams;

    protected $localDebug = true;
    protected $isWin = false;
    protected $config;
    protected $pdo;
    protected $console;
    protected $logger;
    protected $results;
    protected $products = [];
    protected $shops = [];
    protected $details = [];
    protected $skus = [];
    protected $stores;
    protected $use_cache_table = false;
    protected $unused_shops = [];
    protected $preferred_shops = [];
    protected $preferences = [];
    protected $preferred_quantity = 5;
    protected $use_orders_for_quantities = false;
    protected $strict = false;
    protected $exclude_order_id;

    const BUFFER_KEY = 'SAP_TAKEN_QUANTITIES';

    public function __construct()
    {
        $this->isWin = stripos(PHP_OS, 'WIN') === 0;
        if (PHP_VERSION_ID >= 70000) {
            $this->isWin = true;
        }
        //$this->isWin = false;
        $this->config = Config::get('negoziando.database');
        $this->use_orders_for_quantities = (bool)config('negoziando.order_ttl_shops_quantity_enabled', false);
    }

    public function setConsole(Command $console)
    {
        $this->console = $console;
    }

    public function runningInConsole()
    {
        return (PHP_SAPI === 'cli');
    }

    public function setExcludeOrder($order_id)
    {
        $this->exclude_order_id = $order_id;
    }

    public function getExcludeOrder()
    {
        return ((int)$this->exclude_order_id > 0) ? $this->exclude_order_id : null;
    }

    /**
     * @return bool
     */
    public function isStrict()
    {
        return $this->strict;
    }

    /**
     * @param bool $strict
     * @return $this
     */
    public function setStrict($strict)
    {
        $this->strict = $strict;
        return $this;
    }


    public function connect()
    {
        $host = $this->config['host'];
        $database = $this->config['dbname'];
        $username = $this->config['username'];
        $password = $this->config['password'];

        try {
            $connectionString = $this->isWin ? "sqlsrv:server=$host;database=$database;MultipleActiveResultSets=false" : "dblib:host=$host;database=$database";
            if ($this->runningInConsole()) {
                if ($this->console) {
                    $this->console->info('Connection string: ' . $connectionString);
                } else {
                    print('Connection string: ' . $connectionString . PHP_EOL);
                }
            }
            $pdo = new PDO($connectionString, $username, $password);
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            if ($this->isWin) {
                if (PHP_VERSION_ID >= 70000) {
                    $pdo->setAttribute(PDO::SQLSRV_ATTR_FETCHES_NUMERIC_TYPE, false);
                    $pdo->setAttribute(PDO::SQLSRV_ATTR_ENCODING, PDO::SQLSRV_ENCODING_SYSTEM);
                }
            }
            $this->pdo = $pdo;
            if ($this->runningInConsole()) {
                if ($this->console) {
                    $this->console->info('Successfully connected to MSSQL' . $connectionString);
                } else {
                    print 'Successfully connected to MSSQL' . PHP_EOL;
                }
            }
        } catch (\PDOException $e) {
            audit($e->getMessage(), __METHOD__);
            Utils::error('There was a problem connecting: ' . $e->getMessage(), 'MSSQL');
            if ($this->runningInConsole()) {
                if ($this->console) {
                    $this->console->error($e->getMessage());
                } else {
                    print 'ERROR: ' . $e->getMessage() . PHP_EOL;
                }
            }
        }

    }

    /**
     * @param $sku
     * @param bool $best_shop_mode
     * @return mixed|null
     */
    public function getAvailabilityBySku($sku, $best_shop_mode = true)
    {
        $paramKey = 'sku_availability_' . $sku;
        if ($this->hasParam($paramKey)) {
            return $this->getParam($paramKey);
        }

        $stores = $this->getStores();
        $storesStr = implode("','", $stores);

        $featured_stores = $this->getFeaturedStores();
        $featuredStoresStr = implode("','", $featured_stores);
        //$this->log($featured_stores, 'Featured shops', __METHOD__);

        $preferred_stores = $this->getPreferredShops();
        $preferredStoresStr = implode("','", $preferred_stores);
        if(!empty($preferred_stores)){
            $this->log($preferred_stores, 'Preferred stores', __METHOD__);
        }

        $unused_stores = $this->getUnusedShops();
        $unusedStoresStr = implode("','", $unused_stores);
        if(!empty($unused_stores)){
            $this->log($unused_stores, 'Unused stores', __METHOD__);
        }

        $query = "select *, 
CASE
WHEN Codice_Magazzino IN ('$preferredStoresStr') THEN 100
WHEN Quantita_Giacenza >= {$this->preferred_quantity} THEN 75
WHEN Codice_Magazzino IN ('$featuredStoresStr') THEN 50
WHEN Codice_Magazzino IN ('$unusedStoresStr') THEN -100
ELSE 0
END
as Preference from DipSql.dbo.pvw_Krono_GetDispo 
where Codice_SAP='$sku' 
and Codice_Magazzino IN ('$storesStr') ";

        if ($best_shop_mode === true) {
            $query .= 'order by Preference DESC, Quantita_Giacenza DESC';
        } else {
            $query .= 'order by Codice_Magazzino ASC';
        }

        Utils::addQuery("MSSQL: $query");

        $this->log($query . ': ' . $sku, __METHOD__);

        $statement = $this->pdo->prepare($query);
        //$statement->bindValue(":code", $sku, PDO::PARAM_STR);
        //$statement->bindValue(":storesStr", $storesStr, PDO::PARAM_STR);
        $statement->execute();

        $results = $statement->fetchAll(PDO::FETCH_OBJ);

        $orders_quantities = [];
        $global_orders_quantities = [];
        if ($this->use_orders_for_quantities) {
            $orders_quantities = $this->getTakenQuantitiesFromOrders();
            audit($orders_quantities, 'CYCLE_ORDERS_TAKEN_QUANTITIES');
            if ($this->bufferIsOnlyGlobal() === false) {
                $global_orders_quantities = $this->getTakenBuffer(true);
                audit($global_orders_quantities, 'GLOBAL_ORDERS_TAKEN_QUANTITIES');
            }
            //$this->log($orders_quantities, 'ORDERS_TAKEN_QUANTITIES');
        }

        foreach ($results as $result) {
            $result->qty = (int)$result->Quantita_Giacenza;
            $result->code = trim($result->Codice_Magazzino);
            $sku = null;
            if (isset($result->CODICE_SAP)) {
                $sku = trim($this->sanitizeSku($result->CODICE_SAP));
            }
            if (!isset($result->Preference))
                $result->Preference = 0;

            //if we are using orders_quantities, then remove proper quantity found in orders
            if ($sku and $this->use_orders_for_quantities) {
                $search = $result->code . '_' . $sku;
                $taken_quantity = (int)array_get($orders_quantities, 'shops_products.' . $search, 0);
                if ($taken_quantity > 0) {
                    $result->qty -= $taken_quantity;
                    audit("$search has been decremented by $taken_quantity [local]", __METHOD__);
                    $this->log("$search has been decremented by $taken_quantity [local]", __METHOD__);
                    if ($result->qty <= 0) {
                        $result->qty = 0;
                    }
                }
                $global_taken_quantity = (int)array_get($global_orders_quantities, 'shops_products.' . $search, 0);
                if ($global_taken_quantity > 0) {
                    $result->qty -= $global_taken_quantity;
                    audit("$search has been decremented by $global_taken_quantity [global]", __METHOD__);
                    $this->log("$search has been decremented by $global_taken_quantity [global]", __METHOD__);
                    if ($result->qty <= 0) {
                        $result->qty = 0;
                    }
                }
            }

            if ($result->qty > 1 and $result->Preference <= 10) {
                $result->Preference = 20;
            }

            if ($result->qty <= 1) {
                $result->Preference = 10;
            }

            if ($result->qty <= 0) {
                $result->Preference = 0;
            }
        }
        $statement = null;
        $this->results = $results;
        $this->setParam($paramKey, $results);

        return $results;
    }


    public function getTotalQuantityBySku($sku)
    {
        $total = 0;
        $rows = $this->getAvailabilityBySku($sku);
        foreach ($rows as $row) {
            $total += (int)$row->qty;
        }
        return $total;
    }


    public function addProduct($sku, $quantity, $product_id)
    {
        $obj = new \stdClass();
        $obj->sku = $sku;
        $obj->qty = $quantity;
        $this->products[] = $obj;

        $productShops = [];
        $results = $this->getAvailabilityBySku($sku);
        $this->log($results, "getAvailabilityBySku results => $sku");

        $sku = $this->sanitizeSku($sku);
        foreach ($results as $result) {
            if ((int)$result->qty >= (int)$quantity) {
                $productShops[] = $result->code;
            }
            array_set($this->details, "{$sku}.{$result->code}", $result->qty);
            $this->addPreference($result);
        }

        if (!empty($productShops))
            $this->shops[$sku] = $productShops;

        // handle empty results
        if (empty($results)) {
            $this->log("Availability for product $sku is empty!");
            $this->shops[$sku] = [];
        }

        $this->skus[$sku] = $product_id;
        return $productShops;
    }

    public function getSolvableShopsByProduct($sku){
        $sku = $this->sanitizeSku($sku);
    }

    /**
     * @param $sku
     * @param $code
     * @return mixed
     */
    public function getMaximumOrderableQty($sku, $code)
    {
        $sku = $this->sanitizeSku($sku);
        return array_get($this->details, "$sku.$code", 1);
    }

    /**
     * @param $sku
     * @return string|string[]
     */
    public function sanitizeSku($sku)
    {
        return str_replace('.', '', $sku);
    }

    /**
     * @return $this
     */
    public function reset()
    {
        $this->params = [];
        $this->products = [];
        $this->shops = [];
        $this->details = [];
        $this->skus = [];
        $this->stores = null;
        $this->unused_shops = [];
        $this->preferred_shops = [];
        $this->preferences = [];
        $this->exclude_order_id = null;
        $this->results = null;
        $this->setStrict(false);
        return $this;
    }

    public function getBestShop($strict = true)
    {
        $this->setStrict($strict);
        $shops = $this->shops;
        //$this->log($shops, 'Selected shops by products', __METHOD__);
        //$this->log($this->details, 'Detailed quantities by shop', __METHOD__);
        //$this->log($this->preferences, 'Detailed preferences by shop', __METHOD__);

        if (empty($shops)) {
            $this->log('There are no valid shops to find', __METHOD__);
            return null;
        }
        $bestShop = null;
        try {
            $intersect = $this->getShopIntersection();
            $bestShop = is_array($intersect) ? $this->findBestShopWithIntersection($intersect) : null;
            $this->log("Founded best shop: $bestShop", __METHOD__);
        } catch (\Exception $e) {
            $this->log($e->getMessage(), __METHOD__ . ' ERROR!');
            $this->error($e->getMessage(), __METHOD__ . ' ERROR!');
        }

        $this->reset();

        return $bestShop;
    }

    /**
     * @return array|null
     */
    protected function getShopIntersection()
    {
        $shops = $this->shops;

        $cardinal = count($shops);
        //$this->log("Cardinal should be: $cardinal");
        $items_count = [];
        foreach ($shops as $sku => $stores) {
            foreach ($stores as $store) {
                if (isset($items_count[$store])) {
                    $items_count[$store]++;
                } else {
                    $items_count[$store] = 1;
                }
            }
        }
        //$this->log($items_count, 'items_count');

        for (; $cardinal > 0; $cardinal--) {
            $best_intersection = array_filter($items_count, static function ($value) use ($cardinal) {
                return $value >= $cardinal;
            });
            //$this->log($best_intersection, "Best intersection containing $cardinal items");
            if (!empty($best_intersection))
                return array_keys($best_intersection);

            //in strict mode, exits at first iteration
            if ($this->isStrict())
                $cardinal = 0;
        }

        return [];

    }

    /**
     * @param array $intersect
     * @return string
     */
    protected function findBestShopWithIntersection(array $intersect)
    {
        try {
            $stores = $this->getPreferencesWithIntersection($intersect);
            $original_stores = $this->getPreferencesWithIntersection($intersect);
            $stores_keys = array_keys($stores);
            //$this->log($stores, '$stores', __METHOD__);
            //$this->log($original_stores, '$original_stores', __METHOD__);
            //$this->log($stores_keys, '$stores_keys', __METHOD__);
            //$this->log($this->details, '$details', __METHOD__);

            //this var will hold all stores that are shared between all products
            $shared_stores_keys = [];

            //now should found the best match with highest quantities, between the products
            foreach ($this->details as $sku => $store_with_quantities) {
                //remote all stores that are not contained in the intersected stores
                $temp = [];
                foreach ($store_with_quantities as $key => $value) {
                    if (in_array($key, $stores_keys, true)) {
                        $temp[$key] = $value;
                    }
                }
                $store_with_quantities = $temp;
                unset($temp);
                asort($store_with_quantities, SORT_NUMERIC);
                $store_with_quantities = array_reverse($store_with_quantities, true);
                $this->details[$sku] = $store_with_quantities;
                $shared_stores_keys = empty($shared_stores_keys) ? array_keys($store_with_quantities) : array_intersect(array_keys($store_with_quantities), $shared_stores_keys);
            }
            //$this->log($shared_stores_keys, 'shared store keys *** ', __METHOD__);
            //$this->log($this->details, 'sorted details *** ', __METHOD__);

            //set inner weight
            foreach ($stores as $code => $store) {
                //if in preferred store, then augments the weight heavily
                $store->Weight = $store->Preference === 100 ? 1000 : $store->Preference;
                //it the store code is in the shared collection, increment weight by a fixed factor
                if (in_array($code, $shared_stores_keys, true)) {
                    $store->Weight += 100;
                }
            }
            // get all shop with products details, and add product quantity to weight
            foreach ($this->details as $sku => $store_with_quantities) {
                foreach ($store_with_quantities as $store_code => $quantity) {
                    $stores[$store_code]->Weight += (int)$quantity;
                }
            }
            // now sort all stores by its weight
            uasort($stores, static function ($a, $b) {
                if ($a->Weight === $b->Weight) {
                    return 0;
                }
                return ($a->Weight > $b->Weight) ? -1 : 1;
            });
            $this->log($stores, 'Founded stores by weight ***', __METHOD__);

            //get the first element
            $bestShop = (is_array($stores) and !empty($stores)) ? array_values($stores)[0] : null;

            //$this->log($bestShop, 'first element at the top', __METHOD__);

            //if 100, then this is the BEST possible, no need to find further
            if ($bestShop and $bestShop->Preference === 100)
                return $bestShop->code;

            // TODO: we are toggling 'random' elements
            if (false):
                // if 75, then the shop has a lot of quantities, but we need to prefer 'featured' shop between the ones with high availability
                if ($bestShop and $bestShop->Preference === 75) {
                    //only_featured_with_minimum
                    //$this->log('FILTER: only_featured_with_minimum', __METHOD__);
                    $stores = array_filter($stores, static function ($result) {
                        return $result->Preference === 50 and $result->qty >= $this->preferred_quantity;
                    });
                }

                // if 50, then the shop is 'featured', but we need to randomize the featured
                if ($bestShop and $bestShop->Preference === 50) {
                    //only_featured
                    //$this->log('FILTER: only_featured', __METHOD__);
                    $stores = array_filter($stores, static function ($result) {
                        return $result->Preference === 50;
                    });
                }
            endif;

            if ($bestShop)
                return $bestShop->code;

            //if the stores are empty, then revert to the original
            if (!is_array($stores) or empty($stores))
                $stores = $original_stores;

            //if the stores are empty, then there is no store
            if (!is_array($stores) or empty($stores))
                return null;

            //simple randomize the results
            return $stores[array_rand($stores)]->code;

        } catch (\Exception $e) {
            audit_exception($e, __METHOD__);
            audit_error($this, __METHOD__);
            if (!empty($this->preferences)) {
                $keys = array_keys($this->preferences);
                //if the stores are empty, then there is no store
                if (!is_array($keys) or empty($keys))
                    return null;

                return $keys[array_rand($keys)];
            }
            return null;
        }
    }


    public function getBestMapping()
    {
        $shops = $this->shops;
        //$this->log($this->preferred_shops, 'Preferred shops', __METHOD__);
        //$this->log($shops, 'Selected shops by products', __METHOD__);
        //$this->log($this->details, 'Detailed quantities by shop', __METHOD__);

        if (empty($shops)) {
            //$this->log('There are no valid shops to find', __METHOD__);
            return null;
        }
        $intersect = $this->getShopIntersection();
        //$this->log($intersect, '$intersect', __METHOD__);
        $bestShop = $this->findBestShopWithIntersection($intersect);
        $this->log($bestShop, 'FOUND BEST SHOP', __METHOD__);

        $list = [];

        //now we check if the best-shop is shared among all products, if not than the mapping for that product is the first element of 'details' collection

        //audit($this->details, 'Details', 'Decision making');
        //audit($this->skus, 'Skus', 'Decision making');
        //audit($shops, 'Shops', 'Decision making');
        foreach ($shops as $sku => $stores_codes) {
            if (in_array($bestShop, $stores_codes, true)) {
                $list[$this->skus[$sku]] = $bestShop;
            } else {
                if (!isset($this->details[$sku])) {
                    if (!empty($stores_codes)) {
                        $list[$this->skus[$sku]] = $stores_codes[0];
                    } else {
                        $list[$this->skus[$sku]] = null;
                    }
                } else {
                    $keys = array_keys($this->details[$sku]);
                    if (!empty($keys)) {
                        $list[$this->skus[$sku]] = $keys[0];
                    } else {
                        if (!empty($stores_codes)) {
                            $list[$this->skus[$sku]] = $stores_codes[0];
                        } else {
                            $list[$this->skus[$sku]] = null;
                        }
                    }
                }
            }
        }
        $this->log($list, 'Results mapping', __METHOD__);

        $this->reset();

        return $list;
    }


    public function getProductsByShop($code)
    {
        $paramKey = 'products_by_shop_' . $code;
        if ($this->hasParam($paramKey)) {
            return $this->getParam($paramKey);
        }
        $query = 'select TOP 100 * from DipSql.dbo.pvw_Krono_GetDispo where Codice_Magazzino=:code';
        //$this->log($query . ': ' . $code, __METHOD__);
        Utils::addQuery("MSSQL: $query");

        $statement = $this->pdo->prepare($query);
        $statement->bindValue(":code", $code, PDO::PARAM_STR);
        $statement->execute();

        $results = $statement->fetchAll(PDO::FETCH_OBJ);
        $skus = [];
        foreach ($results as $result) {
            $sku = trim($result->CODICE_SAP);

            $product = \Product::where('sap_sku', $sku)->first();
            if ($product) {
                $result->product_id = $product->id;
            }
        }

        $statement = null;
        $this->results = $results;
        //$this->log($results, __METHOD__, 'results');
        $this->setParam($paramKey, $results);
        return $results;

    }

    public function getStores()
    {
        if (isset($this->stores) and is_array($this->stores) and !empty($this->stores)) {
            return $this->stores;
        }

        $this->stores = DB::table('mi_shops')
            ->whereNull('deleted_at')
            ->where('active', 1)
            ->whereNull('affiliate_id')
            ->remember(60 * 24, 'Negoziando.GetStores')
            ->lists('cd_neg');

        return $this->stores;
    }

    public function getFeaturedStores()
    {
        if (isset($this->featured_stores) and is_array($this->featured_stores) and !empty($this->featured_stores)) {
            return $this->featured_stores;
        }

        $this->featured_stores = DB::table('mi_shops')
            ->whereNull('deleted_at')
            ->where('active', 1)
            ->where('featured', 1)
            ->whereNull('affiliate_id')
            ->remember(60 * 24, 'Negoziando.GetFeaturedStores')
            ->lists('cd_neg');

        return $this->featured_stores;
    }


    function test()
    {
        $sku = 'SO.GKK150639';
        $rows = $this->getAvailabilityBySku($sku);
        //$rows = $this->getTotalQuantityBySku($sku);
        var_dump($rows);
    }


    public function getVoucher($voucher)
    {
        $query = "select * from DipSql.dbo.pvw_Krono_getbuono where NumeroBuono=:voucher";
        //$this->log($query . ': ' . $voucher, __METHOD__);

        $statement = $this->pdo->prepare($query);
        $statement->bindValue(":voucher", $voucher, PDO::PARAM_STR);
        $statement->execute();

        $results = $statement->fetchAll(PDO::FETCH_OBJ);

        return empty($results) ? null : $results[0];
    }


    public function useVoucher($voucher)
    {
        $query = "exec DipSql.dbo.Zmor_Upd_SpegniBuono :voucher,:status";
        //audit($query, __METHOD__);
        try {
            $statement = $this->pdo->prepare($query);
            $statement->bindValue(":voucher", $voucher, PDO::PARAM_STR);
            $statement->bindValue(":status", 'E', PDO::PARAM_STR);
            $statement->execute();
            return true;
        } catch (\Exception $e) {
            audit($e->getMessage(), __METHOD__);
            audit_error($e->getMessage(), __METHOD__);
        }
        return false;
    }


    public function redeemVoucher($voucher)
    {
        $query = "exec DipSql.dbo.Zmor_Upd_SpegniBuono :voucher,:status";
        //audit($query, __METHOD__);
        try {
            $statement = $this->pdo->prepare($query);
            $statement->bindValue(":voucher", $voucher, PDO::PARAM_STR);
            $statement->bindValue(":status", 'R', PDO::PARAM_STR);
            $statement->execute();
            return true;
        } catch (\Exception $e) {
            audit($e->getMessage(), __METHOD__);
            audit_error($e->getMessage(), __METHOD__);
        }
        return false;
    }

    public function __destruct()
    {
        $this->pdo = null;
    }

    /**
     * @return array
     */
    public function getUnusedShops()
    {
        return array_unique($this->unused_shops);
    }

    /**
     * @param array $unused_shops
     */
    public function setUnusedShops($unused_shops)
    {
        $this->unused_shops = $unused_shops;
    }

    /**
     * @param string $shop
     */
    public function addUnusedShop($shop)
    {
        $this->unused_shops[] = (string)$shop;
    }

    /**
     * @return array
     */
    public function getPreferredShops()
    {
        return $this->preferred_shops;
    }

    /**
     * @param array $preferred_shops
     */
    public function setPreferredShops($preferred_shops)
    {
        $this->preferred_shops = $preferred_shops;
    }

    /**
     * @param string $shop
     */
    public function addPreferredShop($shop)
    {
        $this->preferred_shops[] = (string)$shop;
    }

    /**
     * @param $result
     */
    protected function addPreference($result)
    {
        if ($result and isset($result->code)) {
            $saved = array_get($this->preferences, $result->code);
            if ($saved !== null) {
                //if the stored record (shop) has
                if ($saved and $saved->Preference > $result->Preference) { //skip upsert
                    return;
                }
            }
            $result->Preference = (int)$result->Preference;
            $result->qty = (int)$result->qty;
            array_set($this->preferences, $result->code, $result);
        }
    }

    /**
     * @param string $code
     * @return object|null
     */
    protected function getPreference($code)
    {
        return array_get($this->preferences, $code);
    }

    /**
     * @param array $intersect
     * @return array
     */
    protected function getPreferencesWithIntersection(array $intersect)
    {
        $preferences = [];
        foreach ($intersect as $code) {
            $preferences[$code] = $this->getPreference($code);
        }
        return $preferences;
    }

    /**
     * @param bool $global
     * @return string
     */
    protected function bufferKey($global = false)
    {
        $key = self::BUFFER_KEY;
        if (true === $global) {
            return $key;
        }
        $exclude_order = $this->getExcludeOrder();
        if ($exclude_order !== null) {
            $key .= $exclude_order;
        }
        return $key;
    }

    /**
     * @return bool
     */
    protected function bufferIsOnlyGlobal()
    {
        $exclude_order = $this->getExcludeOrder();
        return ($exclude_order === null);
    }

    /**
     * @param bool $global
     * @return array
     */
    protected function getTakenBuffer($global = false)
    {
        $key = $this->bufferKey($global);

        $default = [
            'shops_products' => [],
            'shops' => [],
            'products' => [],
        ];

        if (\Registry::has($key)) {
            return \Registry::get($key, $default);
        }
        return $default;
    }

    /**
     * @param bool $global
     * @return bool
     */
    protected function isTakenBufferEmpty($global = false)
    {
        $data = $this->getTakenBuffer($global);
        $empty = false;
        foreach ($data as $key => $value) {
            $empty = empty($value);
        }
        return $empty;
    }

    /**
     * @param array $data
     * @param bool $global
     */
    protected function setTakenBuffer($data, $global = false)
    {
        $key = $this->bufferKey($global);
        \Registry::set($key, $data);
    }

    /**
     * Fetch latest X order, and try to calculate all 'taken' quantities by shop and product
     */
    public function getTakenQuantitiesFromOrders()
    {
        if ($this->isTakenBufferEmpty() === false) {
            return $this->getTakenBuffer();
        }
        $exclude_order = $this->getExcludeOrder();
        $ttl_hours = (int)config('negoziando.order_ttl_hours_shops_quantity', 24);
        if (\App::environment() === 'local') {
            $ttl_hours = 100000;
        }
        try {
            $pivot_date = Carbon::now()->addHours(-1 * $ttl_hours)->format('Y-m-d H:i:s');
            $invalid_modes = [Order::MODE_MASTER, Order::MODE_OFFLINE, Order::MODE_LOCAL];
            $builder = Order::forSap()
                ->withStatus([OrderState::STATUS_NEW, OrderState::STATUS_WORKING])
                ->withoutAvailabilityModes($invalid_modes)
                ->where('created_at', '>=', $pivot_date);

            if ($exclude_order) {
                $builder->where('id', '<>', $exclude_order);
            }

            $latest_orders_ids = $builder->orderBy('id', 'desc')->lists('id');

            /** @var OrderDetail[] $order_details */
            $order_details = OrderDetail::withOrders($latest_orders_ids)->withAvailabilityModes([Order::MODE_SHOP])->get();
            //audit($order_details);

            if (!empty($order_details)) {
                foreach ($order_details as $order_detail) {
                    $this->incrementTakenQuantity($order_detail->getSapSku(), $order_detail->availability_shop_id, $order_detail->product_quantity);
                }
            }
            //audit($data, 'DATA', __METHOD__);
        } catch (\Exception $e) {
            audit_exception($e, __METHOD__);
        }
        return $this->getTakenBuffer();
    }

    /**
     * @param $sku
     * @param $availability_shop_id
     * @param int $quantity
     * @param bool $globally
     */
    public function incrementTakenQuantity($sku, $availability_shop_id, $quantity = 1, $globally = false)
    {
        $data = $this->getTakenBuffer();
        $this->upsertTakenQuantity($data, $sku, $availability_shop_id, $quantity);
        $this->setTakenBuffer($data);
        if ($globally === true and $this->bufferIsOnlyGlobal() === false) {
            $data = $this->getTakenBuffer(true);
            $this->upsertTakenQuantity($data, $sku, $availability_shop_id, $quantity);
            $this->setTakenBuffer($data, true);
            audit($data, __METHOD__, 'GLOBALLY TAKEN BUFFER');
        }
    }

    /**
     * @param array $data
     * @param string $sku
     * @param string $availability_shop_id
     * @param int $quantity
     */
    protected function upsertTakenQuantity(&$data, $sku, $availability_shop_id, $quantity = 1)
    {
        $sku = (string)trim($this->sanitizeSku($sku));
        $availability_shop_id = (string)trim($availability_shop_id);
        $quantity = (int)$quantity;
        if ($sku !== '' and $availability_shop_id !== '') {
            $key_for_shops_products = $availability_shop_id . '_' . $sku;
            $key_for_shops = $availability_shop_id;
            $key_for_products = $sku;
            $qty_for_shops_products = (int)array_get($data, 'shops_products.' . $key_for_shops_products, 0);
            $qty_for_shops = (int)array_get($data, 'shops.' . $key_for_shops, 0);
            $qty_for_products = (int)array_get($data, 'products.' . $key_for_products, 0);
            //update quantities
            array_set($data, 'shops_products.' . $key_for_shops_products, $qty_for_shops_products + $quantity);
            array_set($data, 'shops.' . $key_for_shops, $qty_for_shops + $quantity);
            array_set($data, 'products.' . $key_for_products, $qty_for_products + $quantity);
        }
    }


}
