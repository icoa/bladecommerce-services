<?php

namespace services\Morellato\Database;

use DB, Utils, Config, Exception, Cache;
use Illuminate\Console\Command;
use services\Morellato\Loggers\DatabaseLogger as Logger;
use Illuminate\Support\Str;

class Importer
{

    protected $console;
    protected $logger;
    protected $chunk = 200;
    protected $debug = true;
    protected $write = true;
    protected $purge = false;
    protected $allowUniversalName = false;
    protected $bot_user_id = 1;
    protected $cache = [];
    protected $product_id;
    protected $product_combination_id;
    protected $counters = ['inserted' => 0, 'updated' => 0, 'skipped' => 0];

    function __construct(Command $console, Logger $logger)
    {
        $this->console = $console;
        $this->logger = $logger;
        $this->product_id = DB::selectOne("SHOW TABLE STATUS WHERE name='products'")->Auto_increment;
        $this->product_combination_id = DB::table('products_combinations')->max('id') + 1;
        $this->allowUniversalName = Config::get('ftp.allowUniversalName', false);
    }

    function log($var, $message = null)
    {
        if ($this->debug)
            Utils::log($var, $message);
    }

    function make()
    {
        Config::set('cache.driver', 'file');
        $cacheKey = 'morellato_importer_cache_' . date('Y-m-d-H');
        if ($this->purge) {
            $this->log($cacheKey, 'Purging cache');
            Cache::forget($cacheKey);
        }
        if (Cache::has($cacheKey)) {
            $this->cache = Cache::get($cacheKey);
            $this->log($cacheKey, 'Getting cache');
        } else {
            $this->makeCategories();
            $this->makeBrands();
            $this->makeCollections();
            $this->makeAttributes();
            Cache::put($cacheKey, $this->cache, 60);
            $this->log($cacheKey, 'Saving cache');
        }

        //$this->log($this->cache, 'cache');
        //return;

        Utils::watch();

        DB::beginTransaction();

        //get the product from "anagrafica"
        if (true):
            $rows = DB::table('mi_anagrafica')
                /*->where('attr_moments', 'SI')
                ->orWhere('attr_bracelet_shortening', 'SI')
                ->orWhere('attr_battery_change', 'SI')*/
                //->where('sku', '=', 'R8051300021')
                //->where('sku', '=', 'R3271697027')
                ->where('sku', '=', DB::raw('mastersku'))
                //->where('sku','IC.013427')
                //->where('sku', 'SO.DKKK251460')
                ->orderBy('created_at', 'asc')
                //->take(100)
                ->get();

            $many = count($rows);
            $this->logger->log("Founded [$many] products to process", 'database_importer');

            foreach ($rows as $row) {
                try {
                    list($product, $md5) = $this->product($row);
                    $this->log($product, $md5);
                    $this->doImportProduct($product, $md5);
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage(), 'database_importer');
                }
            }
        endif;

        if (true):
            $masterskus = DB::table('mi_anagrafica')
                ->where('sku', '<>', DB::raw('mastersku'))
                ->orderBy('created_at', 'desc')
                //->take(10)
                //->where('mastersku','SABK22')
                ->select(DB::raw('distinct(mastersku)'), 'variants')
                ->get();

            $many = count($masterskus);
            $this->logger->log("Founded [$many] variants to process", 'database_importer');

            foreach ($masterskus as $row) {
                try {
                    $this->doImportVariants($row->mastersku, $row->variants);
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage() . PHP_EOL . $e->getLine() . PHP_EOL . $e->getFile(), 'database_importer');
                }
            }
        endif;

        DB::commit();

        if ($this->counters['inserted'] > 0) {
            $many = $this->counters['inserted'];
            $this->logger->log("[$many] products have been inserted", 'database_importer');
        }
        if ($this->counters['updated'] > 0) {
            $many = $this->counters['updated'];
            $this->logger->log("[$many] products have been updated", 'database_importer');
        }
        if ($this->counters['skipped'] > 0) {
            $many = $this->counters['skipped'];
            $this->logger->log("[$many] products have been skipped", 'database_importer');
        }
    }


    protected function product($product)
    {
        unset($product->created_at);

        $active_languages = \Core::getLanguages();
        $translations = DB::table('mi_traduzioni')->where('sku', $product->mastersku)->whereIn('lang_id', $active_languages)->get();

        $storage = [];

        $attributesDefinitions = [];

        foreach ($translations as $translation) {
            $lang_id = strtolower($translation->lang_id);
            $product_translation = (object)[
                'name' => $this->sanitize($translation->name),
                'lang_id' => $lang_id,
                'sdesc' => $this->parseHtml($translation->commercial_description),
            ];

            if ($this->nullish($translation->name) and $this->nullish($translation->description)) {
                throw new Exception("Translation data are incomplete for product [$product->sku]");
            }

            if ($this->allowUniversalName) {
                if ($this->nullish($translation->name)) {
                    $product_translation->name = $this->sanitize($product->name);
                    $this->logger->warning("Translation name is empty or incomplete for product [$product->sku]; used the name from Anagrafica, which is not translated!");
                }
            } else {
                if ($this->nullish($translation->name)) {
                    throw new Exception("Translation name is empty or incomplete for product [$product->sku]");
                }
            }

            if ($product_translation->sdesc == '')
                $product_translation->sdesc = $this->parseHtml($product->commercial_description);

            $storage[$lang_id] = $product_translation;

            if (!$this->nullish($translation->attributes) and $lang_id == 'it') {
                $elements = $this->explodeString($translation->attributes, '|');
                foreach ($elements as $i => $element) {
                    if (starts_with($element, 'MATERIALNOTE'))
                        unset($elements[$i]);

                    $lowerElement = Str::lower($element);
                    if ($lowerElement == 'misura' or $lowerElement == 'size') {
                        unset($elements[$i]);
                    }
                }


                foreach ($elements as $translationLine) {
                    if (str_contains($translationLine, ':')) {
                        $columnPosition = strpos($translationLine, ':');
                        $attribute_name = substr($translationLine, 0, $columnPosition);

                        $proceed = true;
                        if (starts_with($attribute_name, 'MATERIALNOTE')) {
                            $proceed = false;
                        }
                        if ($attribute_name == 'MISURA' OR $attribute_name == 'SIZE') {
                            $proceed = false;
                        }

                        if ($proceed):
                            $attribute_values = substr($translationLine, $columnPosition);
                            $attribute_name = $this->sanitize($attribute_name);
                            if ($this->attributeNameCanBeSingle($attribute_name)) {
                                $attribute_values = str_replace(',', '.', $attribute_values);
                            }
                            $storedElements = [];
                            $attribute_values = $this->explodeString($this->sanitize($attribute_values));
                            foreach ($attribute_values as $value) {
                                $key = $attribute_name . ':' . $value;
                                $resolvedValue = $this->cacheRead('attributes', $key);
                                if (!empty($resolvedValue)) {
                                    $storedElements[] = $resolvedValue;
                                }
                            }
                            $attributesDefinitions[$attribute_name] = $storedElements;
                        endif;
                    }
                }

            }
        }

        $this->log($attributesDefinitions, '$attributesDefinitions');

        $categoryRelation = [];
        $givenCategoryInfo = DB::table('mi_categorie')->where('mastersku', $product->mastersku)->first();
        if ($givenCategoryInfo) {
            if (!$this->nullish($givenCategoryInfo->categoria1livello2)) {
                $categories = $this->explodeString($givenCategoryInfo->categoria1livello2);
                foreach ($categories as $category) {
                    $categoryRelation[] = $this->cacheRead('categories', $category);
                }
            } else {
                $categoryRelation[] = $this->cacheRead('categories', $givenCategoryInfo->categoria1livello1);
            }
        }
        if (empty($categoryRelation)) {
            $categoryRelation[] = $this->cacheRead('categories', $product->attr_category);
        }


        $relations = [];
        $relations['brand'] = $this->cacheRead('brands', $product->brand);
        $relations['collection'] = $this->cacheRead('collections', $product->attr_collection);
        $relations['category'] = $categoryRelation;

        $product->relations = $relations;

        $attributes = [];
        $attributes['genders'] = $this->cacheRead('genders', $product->attr_gender);
        $attributes['size'] = $this->matchAttributes('size', Str::slug($product->size));

        foreach ($attributesDefinitions as $name => $attributesDefinition) {
            $attributes[Str::slug($name)] = $attributesDefinition;
        }

        //add Garanzia e Confezione if product category = 'Orologio'
        $attributes['warranty'] = [
            'attribute_id' => 13,
            'attribute_option_id' => 36,
        ];

        $attributes['package'] = [
            'attribute_id' => 12,
            'attribute_option_id' => 34,
        ];

        /*$attributes['materials'] = $this->matchAttributes('materials', $product->attr_materials);
        $attributes['colors'] = $this->matchAttributes('colors', $product->attr_colors);
        $attributes['functions'] = $this->matchAttributes('functions', $product->attr_functions);
        $attributes['movements'] = $this->matchAttributes('movements', $product->attr_movement);*/


        //boolean attributes: moments|bracelet_shortening|battery_change
        $booleans = $this->getBooleanAttributes();
        foreach ($booleans as $boolean => $attribute_id) {
            if ($product->{"attr_$boolean"} == 'SI') {
                $attributes[$boolean] = ['attribute_option_id' => 1, 'attribute_id' => $attribute_id];
            }
        }

        foreach ($attributes as $key => $attribute) {
            if (empty($attribute))
                unset($attributes[$key]);
        }

        $product->attributes = $attributes;
        $product->price = $this->getPriceBySku($product->sku);
        $product->translations = $storage;

        //calculate the digest for the product
        //a product digest contains all relations except 'special prices' and 'stocks'
        $md5 = md5(serialize($product));

        $special_prices = $this->getSpecialPricesBySku($product->sku);
        $stocks = $this->getStocksBySku($product->sku);
        $product->special_prices = $special_prices;
        $product->stocks = $stocks;

        return [$product, $md5];
    }


    protected function doImportProduct($product, $md5)
    {
        try {
            $this->importProduct($product, $md5);
        } catch (\Exception $e) {
            $this->insertError($e, $product);
            $msg = $e->getMessage() . PHP_EOL . $e->getFile() . "(" . $e->getLine() . ')';
            $this->logger->error("Product with SKU ($product->sku) could not be imported: " . $msg);
            throw new \Exception("Warning: product with SKU ($product->sku) could not be imported: " . $msg, $e->getCode());
        }
    }


    protected function importProduct($product, $md5)
    {
        //define action: import, update or skip?
        $action = $this->importAction($product->sku, $md5);
        $oldModel = $this->getProductModelBySku($product->sku, false);
        if ($oldModel and $oldModel->id > 0) {
            $action = 'update';
        }
        if ($oldModel and $oldModel->id > 0) {
            $model = $oldModel;
        } else {
            $model = $this->getProductModelBySku($product->sku);
        }

        $this->logger->log("Prepare to ($action) product with sku [$product->sku]", 'database_importer');


        //$this->log($product, 'PRODUCT');
        //$this->log($md5, 'MD5');

        //create data to import/update

        $canvas = $this->parseCanvas($product->canvas);
        $prices = $this->parsePrices($product->price);

        $data = [
            'sku' => $product->sku,
            'ean13' => $product->ean,
            'weight' => $product->weight,
            'new_from_date' => $canvas['new_from_date'],
            'new_to_date' => $canvas['new_to_date'],
            'price' => $prices['price'],
            'sell_price_wt' => $prices['sell_price_wt'],
            'sell_price' => $prices['sell_price'],
            'price_nt' => $prices['price_nt'],
            'brand_id' => $product->relations['brand'],
            'collection_id' => $product->relations['collection'],
            'default_category_id' => $product->relations['category'][0]['id'],
            'main_category_id' => $product->relations['category'][0]['parent_id'],
            'is_outlet' => $product->outlet == 'SI',
            'source' => 'morellato',
        ];

        if ($data['main_category_id'] == 0)
            $data['main_category_id'] = $data['default_category_id'];

        //when 'skipping' a product only stocks and special prices are re-binded
        if ($action == 'skip') {
            $product_id = $model->id;
            $this->log("Skipping insert/update action: re-bind only special prices and stocks for ($model->sku|$model->id)");
            $this->removeProductRelatedData($product_id);
            $this->importProductSpecialPrices($product, $product_id);
            $this->importProductStocks($product, $product_id);
            $this->counters['skipped']++;

            Utils::log($data, 'SKIPPING ACTION');
            //rapid fix for categories
            \Product::where('id', $product_id)->update($data);
            return $product_id;
        }

        if ($oldModel and $oldModel->id > 0) {
            $data['is_out_of_production'] = 0;
            $data['cnt_attributes'] = 0;
            $data['cnt_categories'] = 0;
        }

        //bind translations to the data source model
        foreach ($product->translations as $locale => $translation) {
            unset($translation->lang_id);
            $translation->published = 1;
            $translation->slug = Str::slug($translation->name);
            $data[$locale] = $translation;
        }


        if ($action == 'insert') {
            //INSERT PRODUCT HERE
            $product_id = $this->product_id;
            $model = new \Product();
            $data['id'] = $product_id;
            $data['position'] = $product_id;
            $data['qty'] = $this->getProductQuantityByStocks($product, $product_id);
            $this->setBlame($data);
            $attributes = $model->make($data, true);
            //$this->log($attributes, 'PRODUCTS FILLABLE INSERT');
            if ($this->write) {
                $model->save();
            }

            $this->importProductAttributes($product, $product_id);
            $this->importProductCategories($product, $product_id);
            $this->importProductSpecialPrices($product, $product_id);
            $this->importProductStocks($product, $product_id);
            $this->insertMigration($product->sku, $md5);

            $this->logger->log("Product [$product->sku] successfully inserted with id [$product_id]", 'database_importer');

            //update the general product id
            $this->product_id++;
            $this->counters['inserted']++;
        }

        //during the update only the main data are updated; in order to update the relations the records are deleted and re-inserted
        if ($action == 'update') {

            if (is_null($model)) {
                $model = $this->getProductModelBySku($product->sku);
            }
            $product_id = $model->id;
            //now update the model
            //$this->log($data, 'PRODUCTS FILLABLE UPDATE');
            $modelData = $model->toArray();
            $data = array_merge($modelData, $data);
            $data['cnt_attributes'] = 0;
            $data['cnt_categories'] = 0;
            $attributes = $model->make($data, true);
            //$this->log($attributes, 'PRODUCTS UPDATE COMPLETE DATA');
            if ($this->write) {
                $model->save();
                \Product::where('id', $product_id)->update(['cnt_attributes' => 0, 'cnt_categories' => 0]);
            }

            //remove main relations
            $this->removeProductRelations($product_id);

            //re-insert all relations
            $this->importProductAttributes($product, $product_id);
            $this->importProductCategories($product, $product_id);
            $this->importProductSpecialPrices($product, $product_id);
            $this->importProductStocks($product, $product_id);
            $this->upsertMigration($product->sku, $md5);

            $this->logger->log("Product [$product->sku] successfully updated with id [$product_id]", 'database_importer');

            $this->counters['updated']++;
        }

        return $product_id;
    }


    protected function importProductAttributes($product, $product_id)
    {
        //now insert attributes
        $counter = 1;
        foreach ($product->attributes as $name => $attribute) {
            if (is_array($attribute) and !empty($attribute)) {

                $callback = function ($attribute, $counter, $name, $product_id) {
                    $products_attributes = [
                        'product_id' => $product_id,
                        'attribute_id' => $attribute['attribute_id'],
                        'attribute_val' => $attribute['attribute_option_id'],
                    ];

                    //$this->log($products_attributes, 'PRODUCTS ATTRIBUTES INSERT => ' . $name);

                    if ($this->write) {
                        DB::table('products_attributes')->insert($products_attributes);
                    }
                };


                $callbackUpdatePosition = function ($attribute, $counter, $name, $product_id) {

                    $products_attributes_position = [
                        'product_id' => $product_id,
                        'attribute_id' => $attribute['attribute_id'],
                        'position' => $counter,
                    ];

                    //$this->log($products_attributes_position, 'PRODUCTS ATTRIBUTES POSITION INSERT => ' . $name);

                    if ($this->write) {
                        DB::table('products_attributes_position')->insert($products_attributes_position);
                    }
                };

                if (isset($attribute['attribute_id'])) { //single attribute
                    $callback($attribute, $counter, $name, $product_id);
                    $callbackUpdatePosition($attribute, $counter, $name, $product_id);
                    $counter++;
                } else {
                    foreach ($attribute as $item) { //multiple attributes
                        $callback($item, $counter, $name, $product_id);
                    }
                    $callbackUpdatePosition($attribute[0], $counter, $name, $product_id);
                    $counter++;
                }

            }
        }
    }


    protected function importProductCategories($product, $product_id)
    {
        //now insert categories
        if (!empty($product->relations['category'])) {
            foreach ($product->relations['category'] as $category) {
                $category_data = [
                    'category_id' => $category['id'],
                    'product_id' => $product_id,
                ];
                $this->log($category_data, 'PRODUCTS CATEGORIES INSERT');
                if ($this->write) {
                    try {
                        DB::table('categories_products')->insert($category_data);
                    } catch (\Exception $e) {

                    }

                }
            }
        }
    }


    protected function importProductSpecialPrices($product, $product_id)
    {
        //now insert special prices
        if (!empty($product->special_prices)) {
            foreach ($product->special_prices as $special_price) {
                $special_price['product_id'] = $product_id;
                $special_price['reduction_type'] = 'fixed_amount';
                $special_price['reduction_value'] = $special_price['price'];
                unset($special_price['price']);
                $model = new \ProductPrice();
                $attributes = $model->make($special_price, true);
                $this->log($attributes, 'PRODUCTS SPECIAL PRICES INSERT');
                if ($this->write) {
                    $model->save();
                }
            }
        }
    }


    protected function importProductStocks($product, $product_id, $product_combination_id = null)
    {
        //now insert stocks
        if (!empty($product->stocks)) {
            foreach ($product->stocks as $stock) {
                $stock['product_id'] = $product_id;
                $stock['product_combination_id'] = $product_combination_id;
                $model = new \ProductSpecificStock();
                $attributes = $model->make($stock, true);
                if ($this->write) {
                    //update the virtual quantity???
                    $model->save();
                    $model->setProductQuantity();
                }
                $this->log($attributes, 'PRODUCTS STOCKS INSERT');
            }
        }
    }


    protected function getProductQuantityByStocks($product, $product_id, $product_combination_id = null)
    {
        //now insert stocks
        $qty = 0;
        if (!empty($product->stocks)) {
            foreach ($product->stocks as $stock) {
                $stock['product_id'] = $product_id;
                $stock['product_combination_id'] = $product_combination_id;
                $model = new \ProductSpecificStock();
                $model->make($stock, true);
                $qty += $model->getProductQuantity();
            }
        }
        return $qty;
    }

    protected function removeProductRelations($product_id)
    {

        if ($this->write) {
            //remove categories
            DB::table('categories_products')->where('product_id', $product_id)->delete();
            //remove attributes
            DB::table('products_attributes')->where('product_id', $product_id)->delete();
            DB::table('products_attributes_position')->where('product_id', $product_id)->delete();
            $this->removeProductRelatedData($product_id);
        }
    }


    protected function removeWrongProduct($product_id)
    {
        DB::table('products')->where('id', $product_id)->delete();
        DB::table('products_lang')->where('product_id', $product_id)->delete();
        $this->removeProductRelations($product_id);
    }


    protected function removeProductRelatedData($product_id)
    {
        //remove special prices
        \ProductPrice::where('product_id', $product_id)->where('price_rule_id', 0)->where('country_id', 0)->delete();
        //remove stocks
        \ProductSpecificStock::where('product_id', $product_id)->delete();
    }


    protected function getProductModelBySku($sku, $morellatoOnly = true)
    {
        return $morellatoOnly ? \Product::where('sku', $sku)->where('source', 'morellato')->first() : \Product::where('sku', $sku)->whereNull('source')->first();
    }


    protected function matchAttributes($type, $str)
    {
        $tokens = $this->explodeString($str);
        $matches = [];
        foreach ($tokens as $token) {
            $matches[] = $this->cacheRead($type, $token);
        }
        return $matches;
    }


    protected function getBooleanAttributes()
    {
        if (isset($this->booleanAttributes)) {
            return $this->booleanAttributes;
        }
        $booleans = ['moments', 'bracelet_shortening', 'battery_change'];
        $data = [];
        foreach ($booleans as $boolean) {
            $attribute_id = $this->getAttributeIdByCode($boolean);
            $data[$boolean] = $attribute_id;
        }
        $this->booleanAttributes = $data;
        return $data;
    }


    protected function setBlame(&$data)
    {
        $data['created_by'] = $this->bot_user_id;
        $data['updated_by'] = $this->bot_user_id;
        //$data['created_at'] = date('Y-m-d H:i:s');
        //$data['updated_at'] = date('Y-m-d H:i:s');
    }

    protected function languages()
    {
        return \Core::getLanguages();
    }


    protected function resolveBrand($name)
    {
        if ($this->nullish($name))
            return null;

        $lower = Str::lower($name);

        if ($lower == 'sector gioielli')
            $name = 'Sector';

        if ($lower == 'ice watch' or $lower == 'ice watches')
            $name = 'Ice-Watch';

        /*if ($lower == 'trussardi')
            $name = 'Trussardi';*/

        if ($lower == 'skagen')
            $name = 'Skagen Denmark';

        $slug = Str::slug($name);

        $id = DB::table("brands_lang")->where("slug", $slug)->pluck("brand_id");

        return $id > 0 ? $id : null;
    }


    protected function makeBrands()
    {
        $brands = DB::table('mi_anagrafica')->select(DB::raw('distinct(brand)'))->lists('brand');

        foreach ($brands as $brand) {
            $brand = $this->sanitize($brand);
            if (!$this->nullish($brand)) {
                $id = $this->resolveBrand($brand);
                if ($id) {
                    $this->cacheStore('brands', $brand, $id);
                } else {
                    $this->cacheStore('brands', $brand, $this->createBrand($brand));
                }
            }
        }

    }


    protected function createBrand($name)
    {
        $name = ucfirst(Str::lower($name));
        $data = [];
        $this->setBlame($data);
        foreach ($this->languages() as $language) {
            $data[$language] = [
                'name' => $name,
                'slug' => Str::slug($name),
                'published' => 1,
            ];
        }
        $model = new \Brand();
        $model->make($data);
        $this->logger->log("Created new brand [$name]", 'database_importer');
        return $model->id;
    }


    protected function makeCollections()
    {
        //Utils::watch();
        $collections = DB::table('mi_anagrafica')->select(DB::raw('distinct(attr_collection) as collection'), 'brand')->groupBy('collection')->orderBy('brand')->orderBy('collection')->get();
        //$this->log($collections,__METHOD__);
        foreach ($collections as $collection) {
            $name = $this->sanitize($collection->collection);
            $brand = $this->sanitize($collection->brand);
            if (!$this->nullish($name)) {
                $id = $this->resolveCollection($name, $brand);
                if ($id) {
                    $this->cacheStore('collections', $name, $id);
                } else {
                    $this->cacheStore('collections', $name, $this->createCollection($name, $brand));
                }
            }
        }
    }

    protected function createCollection($name, $brand)
    {
        $name = ucfirst(Str::lower($name));
        $brand_id = null;
        try {
            $brand_id = $this->cacheRead('brands', $brand);
        } catch (\Exception $e) {
            $this->logger->error("Try to fetch brand_id in createCollection has caused error for collection,brand ($name,$brand)", 'database_importer');
        }
        $data = [
            'brand_id' => $brand_id,
        ];
        $this->setBlame($data);
        foreach ($this->languages() as $language) {
            $data[$language] = [
                'name' => $name,
                'slug' => Str::slug($name),
                'published' => 1,
            ];
        }
        $this->log($data, __METHOD__);
        $model = new \Collection();
        $model->make($data);
        $this->logger->log("Created new collection [$name] for brand [$brand]", 'database_importer');
        return $model->id;
    }


    protected function resolveCollection($name, $brand)
    {
        if ($this->nullish($name))
            return null;

        $brand_id = null;
        try {
            $brand_id = $this->cacheRead('brands', $brand);
        } catch (\Exception $e) {
            $this->logger->error("Try to fetch brand_id in resolveCollection has caused error for collection,brand ($name,$brand)", 'database_importer');
        }

        $slug = Str::slug($name);

        $id = DB::table("collections_lang")
            ->join('collections', 'collections.id', '=', 'collections_lang.collection_id')
            ->where("slug", $slug)
            ->where('brand_id', $brand_id)
            ->pluck("id");

        return $id > 0 ? $id : null;
    }


    protected function attributeNameCanBeSingle($name)
    {
        $name = Str::lower($name);
        return (str_contains($name, ['misura', 'size', 'lunghezza', 'larghezza', 'ampiezza', 'width', 'height']));
    }


    protected function makeAttributes()
    {

        //gender
        //Utils::watch();
        $aliases_rows = DB::table('mi_attribute_alias')->where('attribute', 'gender')->get();
        $aliases = [];
        foreach ($aliases_rows as $aliases_row) {
            $aliases[$aliases_row->alias] = ['attribute_id' => $aliases_row->attribute_id, 'attribute_option_id' => $aliases_row->attribute_option_id];
        }
        $rows = DB::table('mi_anagrafica')->select(DB::raw('distinct(attr_gender) as gender'))->orderBy('gender')->get();
        //$this->log($rows, 'gender');
        foreach ($rows as $row) {
            $gender = trim(Str::upper($row->gender));
            if ($gender == '')
                continue;

            if (isset($aliases[$gender])) {
                $this->cacheStore('genders', $gender, $aliases[$gender]);
            } else {
                $this->cacheStore('genders', $gender, $this->createGender($gender));
                $this->logger->log("Created new attribute: gender [$gender]", 'database_importer');
            }
        }

        //size
        $attribute_id = $this->getAttributeIdByCode('size');
        $data = DB::table('mi_anagrafica')->select(DB::raw('distinct(size) as attribute'))->orderBy('attribute')->get();
        foreach ($data as $attribute) {
            $name = $attribute->attribute;
            if ($name != '') {
                $attribute_option_id = $this->resolveAttribute($name, $attribute_id);
                if ($attribute_option_id) {
                    $this->cacheStore('size', $name, compact('attribute_option_id', 'attribute_id'));
                } else {
                    $this->log("Size $name should be created");
                    $attribute_option_id = $this->createAttributeOption($name, $attribute_id);
                    $this->log("New attribute option created with id: $attribute_option_id");
                    $this->cacheStore('size', $name, compact('attribute_option_id', 'attribute_id'));
                    $this->logger->log("Created new attribute: size [$name]", 'database_importer');
                }
            }
        }


        //carats
        $attribute_id = $this->getAttributeIdByCode('carats');
        $data = DB::table('mi_anagrafica')->select(DB::raw('distinct(attr_karats) as attribute'))->orderBy('attribute')->get();
        foreach ($data as $attribute) {
            $name = $attribute->attribute;
            if ($name != '') {
                $attribute_option_id = $this->resolveAttribute($name, $attribute_id);
                if ($attribute_option_id) {
                    $this->cacheStore('karat', $name, compact('attribute_option_id', 'attribute_id'));
                } else {
                    $this->log("Karat $name should be created");
                    $attribute_option_id = $this->createAttributeOption($name, $attribute_id);
                    $this->log("New attribute option created with id: $attribute_option_id");
                    $this->cacheStore('karat', $name, compact('attribute_option_id', 'attribute_id'));
                    $this->logger->log("Created new attribute: karat [$name]", 'database_importer');
                }
            }
        }


        $active_languages = \Core::getLanguages();

        $skus = DB::table('mi_anagrafica')
            /*->where('attr_moments', 'SI')
            ->orWhere('attr_bracelet_shortening', 'SI')
            ->orWhere('attr_battery_change', 'SI')*/
            //->where('sku', '=', 'SQH14')
            //->where('sku', '=', DB::raw('mastersku'))
            //->where('sku','R0153121502')
            //->where('sku','FER0830105')
            //->where('sku','JM416AIK21')
            //->where('sku','R4251102522')
            //->where('sku','R8853118005')
            //->where('sku','R3251594001')
            //->where('sku','R8221193115')
            //->where('sku','FER0830250')
            ->orderBy('created_at', 'asc')
            ->select(DB::raw('distinct(mastersku)'))
            //->take(10)
            ->lists('mastersku');


        foreach ($skus as $sku) {

            try {
                $translations = DB::table('mi_traduzioni')
                    ->where('sku', $sku)
                    ->where('attributes', '<>', '')
                    ->whereIn('lang_id', $active_languages)->get();

                $attributesDefinitions = [];

                foreach ($translations as $translation) {
                    $lang_id = strtolower($translation->lang_id);

                    if (!$this->nullish($translation->attributes)) {
                        $elements = $this->explodeString($translation->attributes, '|');
                        foreach ($elements as $i => $element) {
                            if (starts_with($element, 'MATERIALENOTE')) {
                                unset($elements[$i]);
                            }
                        }
                        $attributesDefinitions[$lang_id] = $elements;
                    }
                }

                $parseAttributes = function ($def) {
                    if (empty($def))
                        return null;
                    $size = count($def['it']);
                    $locales = array_keys($def);
                    $translations = [];
                    for ($i = 0; $i < $size; $i++) {
                        $bundle = [];
                        foreach ($locales as $locale) {
                            $bundle[$locale] = isset($def[$locale][$i]) ? $def[$locale][$i] : null;
                        }
                        $translations[] = $bundle;
                    }
                    //$this->log($translations, 'TRANSLATIONS');
                    //resolve translations
                    $data = [];
                    foreach ($translations as $translationLines) {
                        $attribute = ['name' => [], 'values' => []];
                        foreach ($translationLines as $locale => $translationLine) {
                            if (str_contains($translationLine, ':')) {
                                $columnPosition = strpos($translationLine, ':');
                                $attribute_name = substr($translationLine, 0, $columnPosition);
                                $attribute_values = substr($translationLine, $columnPosition);
                                $attribute_name = $this->sanitize($attribute_name);
                                if ($this->attributeNameCanBeSingle($attribute_name)) {
                                    $attribute_values = str_replace(',', '.', $attribute_values);
                                }
                                $attribute_values = $this->explodeString($this->sanitize($attribute_values));
                                $attribute['name'][$locale] = $attribute_name;
                                $index = 0;
                                foreach ($attribute_values as $attribute_value) {
                                    if (empty($attribute['values'][$index])) {
                                        $attribute['values'][$index] = [];
                                    }
                                    $attribute['values'][$index][$locale] = $attribute_value;
                                    $index++;
                                }
                                //$attribute['values'][$locale] = $attribute_values;
                            }
                        }
                        $data[] = $attribute;
                    }

                    //$this->log($data, 'TRANSLATIONS');
                    return $data;

                };

                $data = $parseAttributes($attributesDefinitions);

                $resolveAndCacheAttributes = function ($data) {
                    foreach ($data as $attribute) {
                        $attribute_id = $this->resolveAttributeNames($attribute['name']);
                        $attribute_name = isset($attribute['name']['it']) ? $attribute['name']['it'] : $attribute['name']['en'];
                        //$this->log($attribute_id, 'RESOLVED ATTRIBUTE ID FOR: ' . $attribute_name);
                        if ($attribute_id > 0) {
                            foreach ($attribute['values'] as $attribute_option) {
                                $attribute_option_id = $this->resolveAttributeOptions($attribute_option, $attribute_id);
                                $attribute_option_name = isset($attribute_option['it']) ? $attribute_option['it'] : $attribute_option['en'];
                                //$this->log($attribute_option_id, 'RESOLVED ATTRIBUTE OPTION ID FOR ' . $attribute_option_name);
                                if ($attribute_option_id > 0) {
                                    $key = $attribute_name . ':' . $attribute_option_name;
                                    $this->cacheStore('attributes', $key, compact('attribute_id', 'attribute_option_id'));
                                }
                            }
                        }
                    }
                };

                if ($data != null)
                    $resolveAndCacheAttributes($data);

            } catch (Exception $e) {
                $this->logger->error("Could not match attributes for product [$sku]: " . $e->getMessage(), 'database_importer');
                $this->log($e->getMessage());
                $this->log($e->getTraceAsString());
            }


        }


        //material
        /*$attribute_id = $this->getAttributeIdByCode('material');
        $rows = DB::table('mi_anagrafica')->select(DB::raw('distinct(attr_materials) as attribute'))->orderBy('attribute')->get();
        $data = [];
        foreach ($rows as $row) {
            $data = array_merge($data, $this->explodeString($row->attribute));
        }
        $data = array_unique($data);
        //$this->log($data);
        foreach ($data as $name) {
            $attribute_option_id = $this->resolveAttribute($name, $attribute_id);
            if ($attribute_option_id) {
                $this->cacheStore('materials', $name, compact('attribute_option_id', 'attribute_id'));
            } else {
                $this->log("Material $name should be created");
                $attribute_option_id = $this->createAttribute($name, $attribute_id);
                $this->log("New attribute option created with id: $attribute_option_id");
                $this->cacheStore('materials', $name, compact('attribute_option_id', 'attribute_id'));
            }
        }*/


        //color
        /*$attribute_id = $this->getAttributeIdByCode('color');
        $rows = DB::table('mi_anagrafica')->select(DB::raw('distinct(attr_colors) as attribute'))->orderBy('attribute')->get();
        $data = [];
        foreach ($rows as $row) {
            $data = array_merge($data, $this->explodeString($row->attribute));
        }
        $data = array_unique($data);
        //$this->log($data);
        foreach ($data as $name) {
            $attribute_option_id = $this->resolveAttribute($name, $attribute_id);
            if ($attribute_option_id) {
                $this->cacheStore('colors', $name, compact('attribute_option_id', 'attribute_id'));
            } else {
                $this->log("Color $name should be created");
                $attribute_option_id = $this->createAttribute($name, $attribute_id);
                $this->log("New attribute option created with id: $attribute_option_id");
                $this->cacheStore('colors', $name, compact('attribute_option_id', 'attribute_id'));
            }
        }*/


        //movement
        /*$attribute_id = $this->getAttributeIdByCode('movement');
        $rows = DB::table('mi_anagrafica')->select(DB::raw('distinct(attr_movement) as attribute'))->orderBy('attribute')->get();
        $data = [];
        foreach ($rows as $row) {
            $data = array_merge($data, $this->explodeString($row->attribute));
        }
        $data = array_unique($data);
        //$this->log($data);
        foreach ($data as $name) {
            $attribute_option_id = $this->resolveAttribute($name, $attribute_id);
            if ($attribute_option_id) {
                $this->cacheStore('movements', $name, compact('attribute_option_id', 'attribute_id'));
            } else {
                $this->log("Movement $name should be created");
                $attribute_option_id = $this->createAttribute($name, $attribute_id);
                $this->log("New attribute option created with id: $attribute_option_id");
                $this->cacheStore('movements', $name, compact('attribute_option_id', 'attribute_id'));
            }
        }*/


        //functions
        /*$attribute_id = $this->getAttributeIdByCode('functions');
        $rows = DB::table('mi_anagrafica')->select(DB::raw('distinct(attr_functions) as attribute'))->orderBy('attribute')->get();
        $data = [];
        foreach ($rows as $row) {
            $data = array_merge($data, $this->explodeString($row->attribute));
        }
        $data = array_unique($data);
        //$this->log($data);
        foreach ($data as $name) {
            $attribute_option_id = $this->resolveAttribute($name, $attribute_id);
            if ($attribute_option_id) {
                $this->cacheStore('functions', $name, compact('attribute_option_id', 'attribute_id'));
            } else {
                $this->log("Function $name should be created");
                $attribute_option_id = $this->createAttribute($name, $attribute_id);
                $this->log("New attribute option created with id: $attribute_option_id");
                $this->cacheStore('functions', $name, compact('attribute_option_id', 'attribute_id'));
            }
        }*/


    }


    protected function createCategory($name, $parent_id)
    {
        $name = ucfirst(Str::lower($name));

        $data = [
            'parent_id' => $parent_id,
        ];
        $this->setBlame($data);
        foreach ($this->languages() as $language) {
            $data[$language] = [
                'name' => $name,
                'slug' => Str::slug($name),
                'published' => 1,
            ];
        }
        $this->log($data, __METHOD__);
        $model = new \Category();
        $model->make($data);

        $this->logger->log("Created new category [$name]", 'database_importer');

        return $model->id;
    }


    protected function resolveCategory($name, $parent_id = 0)
    {
        if ($this->nullish($name))
            return null;

        $lower = Str::lower($name);

        /*if ($lower == 'accessori') {
            $name = 'Home & fashion';
        }*/

        if ($lower == 'meccanico carica manuale' or $lower == 'complicazioni meccanico automatico' or $lower == 'meccanico automatico') {
            $name = 'Meccanico';
        }

        if ($lower == 'solo tempo' or $lower == 'tempo') {
            $name = 'Just time';
        }

        if ($lower == 'penna a sfera') {
            $name = 'Penna';
        }

        if ($lower == 'smart') {
            $name = 'smartwatch';
        }

        if ($lower == 'fedine') {
            $name = 'anelli';
        }

        if ($lower == 'tempo , data e fasi lunari' or $lower == 'tempo e data' or $lower == 'tempo giorno e data' or $lower == 'data e fasi lunari') {
            $name = 'Multifunzione';
        }

        $slug = Str::slug($name);


        $id = DB::table("categories_lang")->where("slug", $slug)->orWhere('singular', $name)->orWhere('name', $name)->pluck("category_id");
        $category = \Category::find($id);
        if ($category) {
            if ($category->parent_id != $parent_id) {
                $parent_id = $this->findCategoryAncestor($category->id, $category->parent_id);
            }
        }

        return $id > 0 ? compact('id', 'parent_id') : null;
    }


    protected function findCategoryAncestor($id, $parent_id)
    {
        if ($parent_id == 0) {
            return $id;
        }
        $category = \Category::find($parent_id);
        return $this->findCategoryAncestor($category->id, $category->parent_id);
    }


    protected function resolveAttribute($name, $attribute_id)
    {
        $slug = Str::slug(trim($name));
        $query = "select id from attributes_options left join attributes_options_lang on attributes_options.id = attributes_options_lang.attribute_option_id where slug='$slug' and attribute_id=$attribute_id";
        $result = DB::selectOne($query);
        if ($result and $result->id) {
            return $result->id;
        }
        return null;
    }


    protected function resolveAttributeNames($translations)
    {
        $values = array_values($translations);
        $slugs = [];
        foreach ($values as $value) {
            $slugs[] = Str::slug($value, '_');
        }

        $result = DB::table('attributes')
            ->leftJoin('attributes_lang', 'attributes.id', '=', 'attributes_lang.attribute_id')
            ->whereIn('code', $slugs)
            ->orWhereIn('alias', $slugs)
            ->orWhereIn('name', $values)->first();

        if ($result and $result->id) {
            return $result->id;
        }
        /*$str = implode(',', $values);
        $this->logger->warning("There are no existing attributes for [$str]", 'database_importer');*/
        return $this->createAttribute($translations);
    }


    protected function createAttribute($translations)
    {
        $model = new \Attribute();
        $data = [
            'code' => Str::slug(isset($translations['en']) ? $translations['en'] : $translations['it'], '_'),
            'ldesc' => 'Morellato: ' . isset($translations['it']) ? $translations['it'] : $translations['en']
        ];
        foreach ($translations as $locale => $translation) {
            $data[$locale] = [
                'name' => ucfirst(Str::lower($translation)),
                'slug' => Str::slug($translation),
                'published' => 1,
            ];
        }
        $attributes = $model->make($data);
        $str = implode(',', array_values($translations));
        $this->logger->log("Created attribute [$str]", 'database_importer');
        return (isset($model->id)) ? $model->id : null;
    }


    protected function createAttributeOptionTranslations($translations, $attribute_id)
    {
        $name = ucfirst(Str::lower(isset($translations['en']) ? $translations['en'] : $translations['it']));
        $uname = $name;
        $uslug = Str::slug($uname);
        $data = compact('attribute_id', 'uname', 'uslug');
        $this->setBlame($data);
        foreach ($translations as $locale => $name) {
            $data[$locale] = [
                'name' => ucfirst(Str::lower($name)),
                'slug' => Str::slug($name),
                'published' => 1,
            ];
        }
        $model = new \AttributeOption();
        $model->make($data);
        $id = $model->id;
        $str = implode(',', array_values($translations));
        $this->logger->log("Created attribute option [$str] for attribute ($attribute_id)", 'database_importer');
        return $id;
    }

    protected function resolveAttributeOptions($translations, $attribute_id)
    {
        $values = array_values($translations);
        if ($attribute_id == 14) { //water resistance
            return str_replace(['atm', 'at'], '', Str::lower($values[0]));
        }
        $slugs = [];
        foreach ($values as $value) {
            $slugs[] = Str::slug($value);
        }

        $result = DB::table('attributes_options')
            ->leftJoin('attributes_options_lang', 'attributes_options.id', '=', 'attributes_options_lang.attribute_option_id')
            ->where('attribute_id', $attribute_id)
            ->where(function ($query) use ($slugs, $values) {
                $query->whereIn('slug', $slugs)
                    ->orWhereIn('name', $values);
            })->first();


        if ($result and $result->id) {
            return $result->id;
        }
        /*$str = implode(',', $values);
        $this->logger->warning("There are no existing attribute options for [$str] with attribute_id ($attribute_id)", 'database_importer');*/
        return $this->createAttributeOptionTranslations($translations, $attribute_id);
    }


    protected function createAttributeOption($name, $attribute_id)
    {
        $name = ucfirst(Str::lower($name));
        $uname = $name;
        $uslug = Str::slug($uname);
        $data = compact('attribute_id', 'uname', 'uslug');
        $this->setBlame($data);
        foreach ($this->languages() as $language) {
            $data[$language] = [
                'name' => $name,
                'slug' => Str::slug($name),
                'published' => 1,
            ];
        }
        $model = new \AttributeOption();
        $model->make($data);
        $id = $model->id;
        return $id;
    }


    protected function getAttributeIdByCode($code)
    {
        return \Attribute::where('code', $code)->first()->id;
    }


    function createGender($name)
    {
        $name = ucfirst(Str::lower($name));

        $attribute_id = 214;
        $nav_id = \AttributeOption::where('attribute_id', $attribute_id)->max('nav_id') + 1;
        $uname = $name[0];
        $uslug = Str::lower($name[0]);

        $data = compact('attribute_id', 'nav_id', 'uname', 'uslug');

        $this->setBlame($data);
        foreach ($this->languages() as $language) {
            $data[$language] = [
                'name' => $name,
                'slug' => Str::slug($name),
                'published' => 1,
            ];
        }
        $model = new \AttributeOption();
        $model->make($data);
        $id = $model->id;

        //create the alias
        DB::table('mi_attribute_alias')->insert([
            'attribute' => 'gender',
            'attribute_id' => $attribute_id,
            'attribute_option_id' => $id,
            'alias' => Str::upper($name),
        ]);
        return $id;
    }


    protected function makeCategories()
    {
        $categories = DB::select("select categoria1livello1,categoria1livello2 from mi_categorie GROUP BY categoria1livello1,categoria1livello2 order by categoria1livello2");

        foreach ($categories as $row) {
            $parent = $this->sanitize($row->categoria1livello1);
            $parent_data = $this->resolveCategory($parent);

            if ($parent_data) {
                $this->cacheStore('categories', $parent, $parent_data);
            } else {
                $this->cacheStore('categories', $parent, $this->createCategory($parent, 0));
            }

            $parent_id = $this->cacheRead('categories', $parent)['id'];

            if (!$this->nullish($row->categoria1livello2)) {
                $categories = $this->explodeString($row->categoria1livello2);
                foreach ($categories as $category) {
                    $data = $this->resolveCategory($category, $parent_id);
                    if ($data) {
                        $this->cacheStore('categories', $category, $data);
                    } else {
                        $this->cacheStore('categories', $category, $this->createCategory($category, $parent_id));
                    }
                }
            }

        }

    }


    protected function doImportVariants($mastersku, $variant)
    {
        try {
            $this->importVariants($mastersku, $variant);
        } catch (\Exception $e) {
            $obj = new \stdClass();
            $obj->sku = $mastersku;
            $this->insertError($e, $obj);
            $this->log($e->getMessage(), __METHOD__);
            $this->log($e->getFile(), __METHOD__);
            $this->log($e->getLine(), __METHOD__);
            $msg = $e->getMessage() . PHP_EOL . $e->getFile() . "(" . $e->getLine() . ')';
            throw new \Exception("Warning: variant with SKU ($obj->sku) could not be imported: " . $msg, $e->getCode());
        }
    }


    function importVariants($mastersku, $variant)
    {
        $this->log("Importing variants for mastersku: $mastersku");
        $row = DB::table('mi_anagrafica')->where('mastersku', $mastersku)->orderBy('sku')->first();
        $field = $variant;
        $matching = $variant;
        if ($variant == 'size') {
            if ($this->nullish($row->size)) {
                $row->size = $this->guessSize($row);
            }
        }
        if ($variant == 'color' OR $variant == 'colors') {
            $field = 'attr_colors';
            $matching = 'colors';
        }
        //$row->sku = $row->mastersku;
        list($product, $md5) = $this->product($row);

        //$this->log($product,'MASTER VARIANT');
        //return;


        $product_id = $this->importProduct($product, $md5);
        //Utils::watch();
        //variants
        $combinations = DB::table('mi_anagrafica')
            ->where('mastersku', $mastersku)
            //->where('sku', '<>', $product->sku)
            ->orderBy('sku')->get();

        $many = count($combinations);
        $this->logger->log("Found [$many] combinations for [$product->mastersku]", 'database_importer');
        /*$this->log($combinations,'ALL PRODUCT COMBINATIONS');
        return;*/

        //create variants (combinations)
        //$attribute_id = $this->getAttributeIdByCode($variant);
        foreach ($combinations as $combination) {
            if ($this->nullish($combination->size))
                $combination->size = $this->guessSize($combination);

            $data = [
                'name' => $variant . ': ' . $combination->$field,
                'product_id' => $product_id,
                'sku' => $combination->sku,
                'ean13' => $combination->ean,
                'weight' => $combination->weight,
                'buy_price' => $this->getPriceBySku($combination->sku),
                'price' => 0,
            ];

            $specialPrices = $this->getSpecialPricesBySku($combination->sku);
            $stocks = $this->getStocksBySku($combination->sku);
            $attributes_options_mapping = $this->matchAttributes($matching, $combination->$field);

            $combination->stocks = $stocks;
            $quantity = $this->getProductQuantityByStocks($combination, $product_id);

            $md5 = md5(serialize($data));

            //$action = $this->importAction($combination->sku, $md5);
            $action = 'insert';
            $count = DB::table('products_combinations')->where('product_id', $product_id)->where('sku', $combination->sku)->count();
            if ($count > 0)
                $action = 'update';

            $this->log($action, "ImportAction for combination [$combination->sku]");

            $product_combination_id = $this->product_combination_id;

            if ($action == 'insert') {
                $data['id'] = $product_combination_id;
                $data['quantity'] = $quantity;
                $this->log($data, 'PRODUCT COMBINATION INSERT DATA');

                //INSERT THE COMBINATION HERE
                if ($this->write) {
                    DB::table('products_combinations')->insert($data);
                }
                $this->product_combination_id++;
            }

            if ($action == 'update' || $action == 'skip') {
                $model = DB::table('products_combinations')->where('product_id', $product_id)->where('sku', $combination->sku)->first();
                if ($model and $model->id) {
                    $product_combination_id = $model->id;
                }
                $data['quantity'] = $quantity;
                $this->log($data, 'PRODUCT COMBINATION UPDATE DATA');
                //UPDATE THE COMBINATION HERE
                if ($this->write) {
                    if ($model and $model->id) {
                        DB::table('products_combinations')->where('id', $product_combination_id)->update($data);
                    } else {
                        DB::table('products_combinations')->insert($data);
                        $this->product_combination_id++;
                    }
                }
            }

            $this->log($specialPrices, 'COMBINATION SPECIAL PRICES');
            $this->log($stocks, 'COMBINATION STOCKS');
            $this->importProductStocks($combination, $product_id, $product_combination_id);

            $this->log($attributes_options_mapping, "COMBINATION attributes_options_mapping for [$matching,{$combination->$field}]");

            if (!empty($attributes_options_mapping)) {
                if ($this->write) {
                    DB::table('products_combinations_attributes')->where('combination_id', $product_combination_id)->delete();
                }

                foreach ($attributes_options_mapping as $mapping) {
                    $combination_options = [
                        'combination_id' => $product_combination_id,
                        'option_id' => $mapping['attribute_option_id'],
                        'attribute_id' => $mapping['attribute_id'],
                    ];

                    $this->log($combination_options, 'PRODUCT COMBINATION MAPPING');

                    //INSERT THE MAPPING HERE
                    if ($this->write) {
                        DB::table('products_combinations_attributes')->insert($combination_options);
                    }
                }
            }


        }

    }


    function getPriceBySku($sku)
    {
        $p = DB::table('mi_listino')->where('sku', $sku)->where('tipoprezzo', 'P')->pluck('prezzo');
        return $this->price($p);
    }


    function getSpecialPricesBySku($sku)
    {
        $rows = DB::table('mi_listino')->where('sku', $sku)->where('tipoprezzo', 'S')->get();
        $data = [];
        if (!empty($rows)) {
            foreach ($rows as $row) {
                $data[] = [
                    'price' => $this->price($row->prezzo),
                    'date_from' => $row->date_from,
                    'date_to' => $row->date_to,
                ];
            }
        }
        return $data;
    }


    function getStocksBySku($sku)
    {
        $rows = DB::table('mi_giacenza')->where('sku', $sku)->get();
        $data = [];
        foreach ($rows as $row) {
            $data[] = [
                'quantity_24' => (int)$row->giacenzaH24,
                'quantity_48' => (int)$row->giacenzaH48,
                'available_at' => $row->disponibilita_futura == '0000-00-00' ? null : $row->disponibilita_futura,
                'warehouse' => $this->getWarehouseCode($row)
            ];
        }
        return $data;
    }


    /**
     * Gets the warehouse id from the imported file name
     * @param mixed $stock
     * @return string
     * */
    private function getWarehouseCode($stock)
    {
        //GIACENZA_03 = 03
        //GIACENZA_01 = 77
        $check = substr($stock->imported_file, 0, 11);
        switch ($check) {
            case 'GIACENZA_01':
            default:
                return '77';
                break;
            case 'GIACENZA_03':
                return '03';
                break;
        }
    }


    function guessSize($product)
    {
        if ($this->nullish($product->size)) {
            $guessSize = ltrim(str_replace($product->mastersku, '', $product->sku), '0');
            if (is_numeric($guessSize)) {
                return $guessSize;
            }
        }
        return null;
    }


    function cacheStore($tag, $key, $value)
    {
        $this->cache[$tag][Str::slug($key)] = $value;
    }

    function cacheRead($tag, $key, $default = null)
    {
        $key = Str::slug($key);
        return isset($this->cache[$tag][$key]) ? $this->cache[$tag][$key] : $default;
    }


    protected function nullish($str)
    {
        $str = trim($str);
        return ($str == '' or is_null($str) or $str == 'null' or $str == '.' or str_contains(Str::upper($str), 'TO BE DEFINED'));
    }


    protected function sanitize($str)
    {
        $str = str_replace(['\\', ':', ';', '#', '?'], '', trim($str));
        return $str;
    }


    protected function importAction($sku, $md5)
    {
        $row = DB::table('mi_migrations')->where('sku', $sku)->first();
        $exists = \Product::where('sku', $sku)->count() > 0;
        if ($row and $exists) {
            if ($row->md5 == $md5) {
                return 'skip';
            }
            return 'update';
        }
        return 'insert';
    }


    private function price($val)
    {
        if ($val <= 0) return 0;
        return number_format((float)$val, 3, '.', '');
    }


    private function listToArray($string)
    {
        $array = [];
        $dom = new \DOMDocument;
        $dom->loadHTML('<?xml encoding="utf-8" ?>' . $string);
        foreach ($dom->getElementsByTagName('li') as $node) {
            $value = trim($node->nodeValue);
            if ($value != '' AND !is_null($value))
                $array[] = $node->nodeValue;
        }
        return $array;
    }


    private function parseHtml($html)
    {
        if ($html == '<ul><li></li></ul>')
            return null;

        return trim(strip_tags($html));
    }


    public function parseCanvas($canvas)
    {
        $data = ['new_from_date' => null, 'new_to_date' => null];
        if ($canvas != '') {
            $year = substr($canvas, 0, 4);
            $bimester = substr($canvas, 4, 2);
            $matrix = [
                '01' => '01',
                '02' => '03',
                '03' => '05',
                '04' => '07',
                '05' => '09',
                '06' => '11',
            ];
            $month = isset($matrix[$bimester]) ? $matrix[$bimester] : '02';
            $day = '01';
            $data['new_from_date'] = "$year-$month-$day";
            $data['new_to_date'] = date('Y-m-d', strtotime($data['new_from_date'] . ' +2 months'));
            if ($data['new_from_date'] == '0000-00-00' or $data['new_from_date'] == '1970-01-01') {
                $data['new_from_date'] = null;
            }
            if ($data['new_to_date'] == '0000-00-00' or $data['new_to_date'] == '1970-01-01') {
                $data['new_to_date'] = null;
            }
        }

        return $data;
    }


    private function parsePrices($price)
    {
        $price_nt = $this->price($price / 1.22);
        return [
            'price' => $price,
            'sell_price_wt' => $price,
            'sell_price' => $price_nt,
            'price_nt' => $price_nt,
        ];
    }


    protected function explodeString($str, $delimiter = ',')
    {
        $data = [];
        $tokens = explode($delimiter, $str);
        foreach ($tokens as $token) {
            $token = trim($token);
            if (!$this->nullish($token))
                $data[] = trim($token);
        }
        return $data;
    }


    private function insertMigration($sku, $md5)
    {
        $data = compact('sku', 'md5');
        $data['imported_at'] = date('Y-m-d H:i:s');
        if ($this->write) {
            try {
                DB::table('mi_migrations')->insert($data);
            } catch (Exception $e) {

            }
        }
        return $data;
    }


    private function updateMigration($sku, $md5)
    {
        $data = compact('sku', 'md5');
        $data['imported_at'] = date('Y-m-d H:i:s');
        if ($this->write) {
            DB::table('mi_migrations')->where('sku', $sku)->update($data);
        }
        return $data;
    }


    private function upsertMigration($sku, $md5)
    {
        $count = DB::table('mi_migrations')->where('sku', $sku)->count();
        return $count > 0 ? $this->updateMigration($sku, $md5) : $this->insertMigration($sku, $md5);
    }


    private function insertError(\Exception $exception, $product)
    {
        $data = [
            'message' => $exception->getMessage(),
            'stack' => $exception->getTraceAsString(),
            'sku' => isset($product->sku) ? $product->sku : null,
            'data' => serialize($product),
        ];
        $data['created_at'] = date('Y-m-d H:i:s');
        if ($this->write) {
            DB::table('mi_errors')->insert($data);
        }
        return $data;
    }
}