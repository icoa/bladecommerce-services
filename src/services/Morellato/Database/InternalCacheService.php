<?php

namespace services\Morellato\Database;

use DB, Utils, Config, Exception, Cache;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Category;
use Brand;
use Collection;
use Product;
use Mainframe;

class InternalCacheService
{

    protected $console;
    protected $debug = true;
    protected $languages = [];
    protected $supported = [
        'admin',
        'config',
        'lang',
        'route',
        'position',
        'module',
        'attribute',
        'pricerule',
        'redis',
        'products',
        'full_products',
        'catalog',
        'carrier',
        'intelligent',
    ];


    function __construct(Command $console)
    {
        $this->console = $console;
        $this->languages = Mainframe::languagesCodes();
        $this->rememberStaticMinutes = \Config::get('cache.rememberStaticMinutes');
        $this->rememberStaticKey = \Config::get('cache.rememberStaticKey');
    }

    function log($var, $message = null)
    {
        if ($this->debug)
            Utils::log($var, $message);
    }

    function make($level)
    {
        $this->console->comment("Purging cache level for [$level]");
        if (!in_array($level, $this->supported)) {
            $this->console->comment("[$level] is not a supported cache level.");
            $supported = implode(', ', $this->supported);
            $this->console->comment("Please choose among the following cache levels: $supported");
            return;
        }

        try {
            $this->purge($level);
            $this->console->info("Cache successfully purged");
        } catch (Exception $e) {
            $this->console->error($e->getMessage());
        }

    }


    protected function purge($level)
    {
        switch ($level) {
            case 'admin':
                \Cache::forget('nav_items_0');
                $rows = \DB::table("widgets")
                    ->get();
                foreach ($rows as $row) {
                    \Cache::forget('nav_items_' . $row->id);
                }
                break;

            case 'config':
                \Cfg::purge();
                break;

            case 'lang':
                $languages = $this->languages;
                foreach ($languages as $lang) {
                    \Cache::forget("site_cache_" . $lang);
                    \Cache::forget("langobj_" . $lang);
                    \Cache::forget($lang . "_javascript_lexicon");
                }
                \Currency::initCache();
                \Cache::forget($this->rememberStaticKey . "_languages_1");
                \Cache::forget($this->rememberStaticKey . "_languages_0");
                break;

            case 'route':
                \Cache::forget("route_markers");
                \Cache::forget("route_nav");
                \Cache::forget("route_attribute");
                \Cache::forget("route_attribute_filters");
                \Cache::forget("shortcut_nav");
                \Cache::forget("link_nav");
                \Cache::forget("link_attribute");
                break;

            case 'position':
                $rows = \ModulePosition::get();
                foreach ($rows as $row) {
                    \Cache::forget("position-" . $row->name);
                    \Cache::forget("mobile-position-" . $row->name);
                }
                \Cache::forget("override_templates");
                break;

            case 'module':
                $rows = \Module::get();
                $languages = $this->languages;
                foreach ($rows as $row) {
                    foreach ($languages as $lang) {
                        \Cache::forget("module-$lang-" . $row->id);
                        \Cache::forget("mobile-module-$lang-" . $row->id);
                    }
                    $row->uncache();
                }
                break;

            case 'attribute':
                $languages = $this->languages;
                foreach ($languages as $lang) {
                    \Cache::forget("db-attributes-$lang");
                    \Cache::forget("catalog_attribute_nav_$lang");
                    \Cache::forget("catalog_special_$lang");
                }
                \Cache::forget("link_attribute");
                break;

            case 'pricerule':
                /*$rows = \Product::where("is_out_of_production", 0)->lists('id');
                foreach ($rows as $id) {
                    \Cache::forget("produce-price-rule-$id");
                }*/
                Cache::driver(config('cache.product_driver', 'apc'))->tags(['price-rules-products'])->flush();
                break;

            case 'carrier':
                $rows = \Carrier::all();
                foreach ($rows as $row) {
                    $row->uncache();
                }
                break;

            case 'redis':
                \Category::initCache();
                \Brand::initCache();
                \Collection::initCache();
                \Trend::initCache();
                \Nav::initCache();
                \Attribute::initCache();
                \AttributeOption::initCache();
                \Section::initCache();
                \Page::initCache();
                \PriceRule::initCache();
                \Lexicon::initCache();

                $rows = \Category::get();
                foreach ($rows as $row) {
                    $row->uncache();
                }
                $rows = \Brand::get();
                foreach ($rows as $row) {
                    $row->uncache();
                }
                $rows = \Collection::get();
                foreach ($rows as $row) {
                    $row->uncache();
                }
                $rows = \Trend::get();
                foreach ($rows as $row) {
                    $row->uncache();
                }
                $rows = \Nav::get();
                foreach ($rows as $row) {
                    $row->uncache();
                }
                $rows = \Attribute::get();
                foreach ($rows as $row) {
                    $row->uncache();
                }
                $rows = \AttributeOption::get();
                foreach ($rows as $row) {
                    $row->uncache();
                }
                $rows = \Section::get();
                foreach ($rows as $row) {
                    $row->uncache();
                }
                $rows = \Page::get();
                foreach ($rows as $row) {
                    $row->uncache();
                }
                $rows = \PriceRule::get();
                foreach ($rows as $row) {
                    $row->uncache();
                }
                break;

            case 'products':
                $rows = \Product::get();
                foreach ($rows as $row) {
                    $row->uncacheSimple();
                }

                break;

            case 'full_products':
                Cache::driver(config('cache.product_driver', 'apc'))->tags(['products'])->flush();
                break;

            case 'catalog':
                \Helper::uncache();
                break;

            case 'intelligent':
                \Helper::purgeAllTaggedCached(true);
                break;
        }
    }
}