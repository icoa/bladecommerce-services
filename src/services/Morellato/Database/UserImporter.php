<?php

namespace services\Morellato\Database;

use DB, Utils, Config, Exception;
use Illuminate\Console\Command;
use services\Morellato\Loggers\DatabaseLogger as Logger;
use Illuminate\Support\Str;
use App\Models\User;
use Sentry;
use Mail;

class UserImporter
{

    protected $console;
    protected $logger;
    protected $chunk = 200;
    protected $debug = true;
    protected $write = true;
    protected $purge = false;
    protected $allowUniversalName = false;
    protected $bot_user_id = 1;
    protected $report = [];
    protected $counters = ['inserted' => 0, 'updated' => 0, 'skipped' => 0];

    function __construct(Command $console, Logger $logger)
    {
        $this->console = $console;
        $this->logger = $logger;
    }

    function log($var, $message = null)
    {
        if ($this->debug)
            Utils::log($var, $message);
    }

    function make($mode ='with-inactive')
    {

        Utils::watch();

        DB::beginTransaction();

            if($mode == 'without-inactive'){
              DB::table('users')->where('clerks',1)->update(['activated' => 0]);
            }

            $rows = DB::table('mi_pdv_users')
                ->leftjoin('mi_shops','mi_pdv_users.shop_code','=','mi_shops.cd_neg')
                ->select('mi_pdv_users.*','mi_shops.id as shop_id')
                ->orderBy('created_at', 'asc')
                ->get();

            $many = count($rows);
            $this->logger->log("Founded [$many] user data to process", 'database_user_importer');

            foreach ($rows as $userData) {
                try {
                    $this->log($userData);
                    $this->doImportUserData($userData);
                } catch (Exception $e) {
                    $this->logger->error($e->getMessage(), 'database_user_importer');
                }
            }

            DB::table('mi_pdv_users')->truncate();

        DB::commit();

        if ($this->counters['inserted'] > 0) {
            $many = $this->counters['inserted'];
            $this->logger->log("[$many] users data have been inserted", 'database_user_importer');
            $this->report[] = ['message' => "[$many] users data have been inserted"];
        }
        if ($this->counters['updated'] > 0) {
            $many = $this->counters['updated'];
            $this->logger->log("[$many] users data have been updated", 'database_user_importer');
            $this->report[] = ['message' => "[$many] users data have been updated"];
        }
        if ($this->counters['skipped'] > 0) {
            $many = $this->counters['skipped'];
            $this->logger->log("[$many] users data have been skipped", 'database_user_importer');
            $this->report[] = ['message' => "[$many] users data have been skipped"];
        }
        $this->sendReport();
    }


    protected function doImportUserData($userData)
    {
        try {
            $this->importUserData($userData);
        } catch (\Exception $e) {
            $this->insertError($e, $userData);
            $msg = $e->getMessage() . PHP_EOL . $e->getFile() . "(" . $e->getLine() . ')';
            $this->logger->error("User with email ($userData->email) could not be imported: " . $msg);
            throw new \Exception("Warning: User with email ($userData->email) could not be imported: " . $msg, $e->getCode());
        }
    }


    protected function importUserData($userData)
    {
        //Get User Model
        $model = $this->getUserModelByEmail($userData->email);

        //define action: import, update?
        if(!$model)
        {
          $action = 'insert';
        }else{
          $action = 'update';
        }

        $this->logger->log("Prepare to ($action) user with email [$userData->email]", 'database_users_importer');

        //Retrieve the data needed for the upsert of the user.

        $data = [
            'email' => sanitize($userData->email),
            'password' => sanitize($userData->password),
            'first_name' => ucfirst(sanitize(trim($userData->first_name))),
            'last_name' => ucfirst(sanitize(trim($userData->last_name))),
            'activated' => sanitize($userData->active),
            'shop_id' => $userData->shop_id,
        ];

        if(!filter_var($data['email'], FILTER_VALIDATE_EMAIL)){

          $data = [
              'message' => 'Invalid email for User with email: '.$data['email'],
              'data' => serialize($userData),
          ];
          $data['created_at'] = date('Y-m-d H:i:s');
          $this->report[] = $data;
          $this->counters['skipped']++;
          return;
        }

        if(!isset($data['shop_id']))
        {
          $data = [
              'message' => 'Shop with code: '.$userData->shop_code.' was not found for User with email: '.$userData->email,
              'data' => serialize($userData),
          ];
          $data['created_at'] = date('Y-m-d H:i:s');
          $this->report[] = $data;
          $this->counters['skipped']++;
          return;
        }



        // If action is to be insert will create user.

        if ($action == 'insert') {

          $data['negoziando'] = 1;
          $data['clerk'] = 1;
          if($this->write){
            $user = Sentry::createUser($data);

            $this->logger->log("User [$user->email] successfully inserted with id [$user->id]", 'database_user_importer');

            $this->counters['inserted']++;
          }

        }

        // If action is to be updated will update user with the new data.

        if ($action == 'update') {

            try
            {
                // Find the user using the user id
                $user = Sentry::findUserByID($model->id);

                $user->update($data);
            }
            catch (Cartalyst\Sentry\Users\UserNotFoundException $e)
            {
                $this->logger->log('User '.[$model->email].' was not found.');
            }

            $this->logger->log("User [$model->email] successfully updated with id [$model->id]", 'database_user_importer');

            $this->counters['updated']++;
          }

    }

    protected function getUserModelByEmail($userEmail){

      return User::where('email', $userEmail)->first();

    }

    private function insertError(\Exception $exception, $userData)
    {
        $data = [
            'message' => $exception->getMessage(),
            'stack' => $exception->getTraceAsString(),
            'email' => isset($userData->email) ? $userData->email : null,
            'data' => serialize($userData),
        ];
        $data['created_at'] = date('Y-m-d H:i:s');

        $this->report[] = $data;

        if ($this->write) {
            DB::table('mi_errors')->insert($data);
        }
        return $data;
    }

    private function sendReport(){
     $recepients = explode(',',Config::get('negoziando.pdv_clerks_recipients'));
     $email = Mail::send('emails.importers.user_importer', ['report' => $this->report] , function($message) use ($recepients)
      {
        $message->from(Config::get('mail.from')['address']);
        foreach ($recepients as $recepient) {
          $message->to($recepient)->subject('Clerks Csv Importer Raport');
        }
      });
    }

    protected function sanitize($str)
    {
        $str = str_replace(['\\', ':', ';', '#', '?'], '', trim($str));
        return $str;
    }

}
