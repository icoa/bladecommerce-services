<?php

namespace services\Morellato\Database;

use DB, Utils, Config, Exception;
use services\Morellato\Loggers\FileLogger as Logger;
use Illuminate\Support\Str;
use Customer;
use Address;
use State;
use Mail;
use Email;

class StrapsCustomerImporter
{

    protected $logger;
    protected $debug = true;
    protected $write = true;
    protected $report = [];
    protected $counters = ['inserted' => 0, 'updated' => 0, 'skipped' => 0];

    /**
     * StrapsCustomerImporter constructor.
     * @param Logger $logger
     */
    function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param $var
     * @param null $message
     */
    function log($var, $message = null)
    {
        if ($this->debug)
            Utils::log($var, $message);
    }

    /**
     *
     */
    function make()
    {

        Utils::watch();

        $rows = DB::table('mi_customers')
            ->orderBy('created_at', 'asc')
            ->get();

        $many = count($rows);
        $this->logger->log("Founded [$many] customer data to process", 'straps_customer_importer');

        foreach ($rows as $userData) {
            DB::beginTransaction();
            try {
                $this->log($userData);
                $this->doImportCustomerData($userData);
                DB::commit();
            } catch (Exception $e) {
                $this->logger->error($e->getMessage(), 'straps_customer_importer');
                DB::rollBack();
            }
        }


        if ($this->counters['inserted'] > 0) {
            $many = $this->counters['inserted'];
            $this->logger->log("[$many] customers data have been inserted", 'straps_customer_importer');
            $this->report[] = ['message' => "[$many] customers data have been inserted"];
        }
        if ($this->counters['updated'] > 0) {
            $many = $this->counters['updated'];
            $this->logger->log("[$many] customers data have been updated", 'straps_customer_importer');
            $this->report[] = ['message' => "[$many] customers data have been updated"];
        }
        if ($this->counters['skipped'] > 0) {
            $many = $this->counters['skipped'];
            $this->logger->log("[$many] customers data have been skipped", 'straps_customer_importer');
            $this->report[] = ['message' => "[$many] customers data have been skipped"];
        }

        $this->sendReport();
    }

    /**
     * @param $userData
     * @throws Exception
     */
    protected function doImportCustomerData($userData)
    {
        try {
            $this->importCustomerData($userData);
        } catch (Exception $e) {
            audit_exception($e, __METHOD__);
            $msg = $e->getMessage() . PHP_EOL . $e->getFile() . "(" . $e->getLine() . ')';
            $this->logger->error("Customer with code ($userData->CodiceCliente) could not be imported: " . $msg);
            throw new Exception("Warning: User with code ($userData->CodiceCliente) could not be imported: " . $msg, $e->getCode());
        }
    }

    /**
     * @param $userData
     */
    protected function importCustomerData($userData)
    {
        //Get User Model
        $model = $this->getCustomerModelByCodeOrEmail($userData->CodiceCliente, $userData->IndirizzoEmail);

        //define action: import, update?
        if (is_null($model)) {
            $action = 'insert';
        } else {
            $action = 'update';
        }

        $this->logger->log("Prepare to ($action) customer with code [$userData->CodiceCliente]", 'database_users_importer');

        $default_b2b_group_id = config('cinturino.b2b_group', 5);
        $default_b2b_danger_group_id = config('cinturino.b2b_group_danger', 8);

        $email = $this->sanitize($userData->IndirizzoEmail);
        $customer_code = Str::upper($this->sanitize($userData->CodiceCliente));
        $name = Str::upper($this->sanitize($userData->RagioneSociale));
        $website = Str::upper($this->sanitize($userData->PIVA));
        $dangerous_customer = trim($userData->Flag) == 'PE';


        if (!$this->isValidEmail($email)) {
            $message = "Invalid email [$email] for Customer with code: $customer_code";
            audit($message, __METHOD__);
            $data = [
                'message' => $message,
                'data' => serialize($userData),
            ];
            $data['created_at'] = date('Y-m-d H:i:s');
            $this->report[] = $data;
            $this->counters['skipped']++;
            return;
        }

        if ($customer_code == '') {
            $message = "Invalid code [$customer_code] for Customer with email: $email";
            audit($message, __METHOD__);
            $data = [
                'message' => $message,
                'data' => serialize($userData),
            ];
            $data['created_at'] = date('Y-m-d H:i:s');
            $this->report[] = $data;
            $this->counters['skipped']++;
            return;
        }

        //Retrieve the data needed for the upsert of the customer.
        $attributes = Customer::getDefaultAttributes();

        $customer = is_null($model) ? new Customer($attributes) : $model;

        //disable event broadcasting
        $customer->broadcast_events = false;

        $data = [
            'email' => $email,
            'passwd' => null, //on default B2B customers have no password
            'name' => $name,
            'company' => $name,
            'website' => $website,
            'customer_code' => $customer_code,
            'firstname' => null,
            'lastname' => null,
            'active' => 0,
            'newsletter' => 0,
            'people_id' => 2,
            'default_group_id' => $dangerous_customer ? $default_b2b_danger_group_id : $default_b2b_group_id,
        ];

        // If action is to be insert will create user.
        if ($action == 'insert') {

            $customer->fill($data);

            if ($this->write) {
                $customer->save();
                $this->logger->log("Customer [$customer->customer_code] successfully inserted with id [$customer->id]", 'straps_customer_importer');
            }

            $this->counters['inserted']++;

        }

        // If action is to be updated will update user with the new data.
        if ($action == 'update') {

            unset($data['passwd']);
            unset($data['active']);
            unset($data['secure_key']);
            unset($data['newsletter']);
            unset($data['marketing']);
            unset($data['profile']);

            $customer->fill($data);

            if ($this->write) {

                $customer->save();
                $this->logger->log("Customer [$customer->customer_code] successfully updated with id [$customer->id]", 'straps_customer_importer');
            }

            $this->counters['updated']++;

        }

        audit($customer->toArray(), __METHOD__);

        $this->upsertCustomerAddress($customer, $userData);

    }


    /**
     * @param $code
     * @param $email
     * @return Customer|null
     */
    protected function getCustomerModelByCodeOrEmail($code, $email)
    {
        $email = Str::lower(trim($email));
        return Customer::where(function($query) use ($code, $email){
            $query->where('customer_code', $code)->orWhere('email', $email);
        })->orderBy('id', 'desc')->first();
    }


    /**
     *
     */
    private function sendReport()
    {
        audit($this->report, __METHOD__);
        if (config('cinturino.send_report', false) == true) {
            $recipients = explode(',', Config::get('cinturino.b2b_import_recipients'));
            if (!empty($recipients)) {
                Mail::send('emails.importers.user_importer', ['report' => $this->report], function ($message) use ($recipients) {
                    $message->from(\Cfg::get('MAIL_GENERAL_ADDRESS'));
                    foreach ($recipients as $recipient) {
                        $message->to($recipient)->subject('Cinturino B2B Csv Importer Report');
                    }
                });
            }
        }
    }


    /**
     * @param Customer $customer
     * @param $userData
     */
    public function upsertCustomerAddress(Customer $customer, $userData)
    {
        $addressAttributes = Address::getDefaultAttributes();
        $country_id = 10; //Italy

        $phone = $userData->Telefono;
        $cf = $userData->CF;
        $vat_number = $userData->PIVA;
        //shared attributes
        $addressAttributes['customer_id'] = $customer->id;
        $addressAttributes['firstname'] = '';
        $addressAttributes['lastname'] = '';
        $addressAttributes['company'] = $customer->company;
        $addressAttributes['people_id'] = 2;
        $addressAttributes['alias'] = $this->sanitize($userData->CodiceSpedizione);
        $addressAttributes['phone_mobile'] = null;
        $addressAttributes['cf'] = $this->sanitize($cf);
        $addressAttributes['vat_number'] = $this->sanitize($vat_number);
        $addressAttributes['country_id'] = $country_id;
        $addressAttributes['phone'] = $this->sanitize($phone);

        $state_code = $userData->Provinciadest == '' ? $userData->Provincia : $userData->Provinciadest;
        $state = $state_code != '' ? State::getStateByIso($state_code, $country_id) : null;
        $postcode = $userData->CAPDest == '' ? $userData->CAP : $userData->CAPDest;
        $address = $userData->IndirizzoDest == '' ? $userData->Indirizzo : $userData->IndirizzoDest;
        $city = $userData->CittaDest == '' ? $userData->Citta : $userData->CittaDest;

        $addressAttributes['postcode'] = $this->sanitize($postcode);
        $addressAttributes['city'] = $this->sanitize($city);
        $addressAttributes['state_id'] = ($state) ? $state->id : null;
        $addressAttributes['address1'] = $this->sanitize($address);
        $addressAttributes['address2'] = null;
        if (Str::contains($address, ' ')) {
            $spacePos = strrpos($address, ' ');
            $addressAttributes['address1'] = trim(substr($address, 0, $spacePos));
            $addressAttributes['address2'] = substr($address, $spacePos + 1);
            $addressAttributes['address1'] = trim($addressAttributes['address1'], ',');
            $addressAttributes['address2'] = trim($addressAttributes['address2'], ',');
        }

        $shippingAddress = $addressAttributes;
        $billingAddress = [];

        //we have two different address, or the 'IndirizzoDest' is empty
        if ($userData->IndirizzoDest != '' and $userData->Indirizzo != $userData->IndirizzoDest) {

            $state = $state_code != '' ? State::getStateByIso($userData->Provincia, $country_id) : null;
            $address = $userData->Indirizzo;
            $billingAddress = $addressAttributes;
            $billingAddress['billing'] = 1;
            $billingAddress['postcode'] = $this->sanitize($userData->CAP);
            $billingAddress['city'] = $this->sanitize($userData->Citta);
            $billingAddress['state_id'] = ($state) ? $state->id : null;
            $billingAddress['address1'] = $this->sanitize($address);
            $billingAddress['address2'] = null;
            if (Str::contains($address, ' ')) {
                $spacePos = strrpos($address, ' ');
                $billingAddress['address1'] = trim(substr($address, 0, $spacePos));
                $billingAddress['address2'] = substr($address, $spacePos + 1);
                $billingAddress['address1'] = trim($billingAddress['address1'], ',');
                $billingAddress['address2'] = trim($billingAddress['address2'], ',');
            }

        }


        //try to upsert the correct shipping address for this customer
        audit($shippingAddress, __METHOD__.'::SHIPPING ADDRESS');
        $customer_id = $shippingAddress['customer_id'];
        $state_id = $shippingAddress['state_id'];
        $postcode = $shippingAddress['postcode'];
        $billing = 0;
        if ($customer_id > 0) {
            $existingAddress = Address::where(compact('customer_id', 'state_id', 'postcode', 'billing'))->first();
            $address = is_null($existingAddress) ? new Address() : $existingAddress;
            $address->fill($shippingAddress);

            if ($this->write) {
                $address->save();
            }
        }

        //try to upsert the correct billing address for this customer
        if (!empty($billingAddress)) {
            audit($billingAddress, __METHOD__.'::BILLING ADDRESS');
            $customer_id = $billingAddress['customer_id'];
            $state_id = $billingAddress['state_id'];
            $postcode = $billingAddress['postcode'];
            $billing = 1;
            if ($customer_id > 0) {
                $existingAddress = Address::where(compact('customer_id', 'state_id', 'postcode', 'billing'))->first();
                $address = is_null($existingAddress) ? new Address() : $existingAddress;
                $address->fill($billingAddress);

                if ($this->write) {
                    $address->save();
                }
            }
        }
    }


    /**
     * @param $str
     * @return mixed
     */
    protected function sanitize($str)
    {
        $str = str_replace(['\\', ':', ';', '#', '?'], '', trim($str));
        return $str;
    }

    /**
     * @param $email
     * @return mixed
     */
    protected function isValidEmail($email)
    {
        $email = Str::lower(trim($email));
        return Utils::isEmail($email);
    }

    /**
     * Send to B2B Customers pre-generated passwords
     */
    function sendMailToCustomers()
    {
        $default_b2b_group_id = config('cinturino.b2b_group', 5);
        $default_b2b_danger_group_id = config('cinturino.b2b_group_danger', 8);
        $login_page = \Link::absolute()->shortcut('login');

        $send_email = config('cinturino.send_password_email', false);

        /* @var Customer[] $customers */
        $customers = Customer::whereNull('passwd')->withGroup([$default_b2b_group_id, $default_b2b_danger_group_id])->orderBy('id', 'desc')->get();
        foreach ($customers as $customer) {

            try {
                $customer->broadcast_events = false;
                $passwords = $customer->getStrapDefaultPassword();
                $plain = $passwords['plain'];
                $md5 = $passwords['md5'];
                $hash = $passwords['hash'];

                $customer->fill([
                    'active' => 1,
                    'passwd' => $hash,
                    'passwd_md5' => $md5,
                    'last_passwd_gen' => date('Y-m-d H:i:s'),
                ]);

                $customer->save();

                $recipient = $customer->email;
                if (\App::environment() == 'local') {
                    $recipient = 'f.politi@m.icoa.it';
                }

                $vars = [
                    'CUSTOMER_NAME' => $customer->getName(),
                    'CUSTOMER_EMAIL' => $customer->email,
                    'CUSTOMER_PLAIN' => $plain,
                    'LINK' => $login_page,
                ];

                audit($vars, __METHOD__ . '::vars');

                if ($send_email) {
                    $email = Email::getByCode('SEND_PASSWORD');
                    $email->setCustomer($customer)->send($vars, $recipient);
                }
            } catch (Exception $e) {
                audit_exception($e, __METHOD__);
                $msg = $e->getMessage() . PHP_EOL . $e->getFile() . "(" . $e->getLine() . ')';
                $this->logger->error("Customer with code ($customer->customer_code) could not be password-shipped: " . $msg);
            }


        }

    }

}
