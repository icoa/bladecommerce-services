<?php

namespace services\Morellato\Gls\Handler;

use Order;
use services\Morellato\Gls\Xml\Reader\CarrierShipmentXmlReader;
use services\Morellato\Gls\Xml\Reader\ShipmentXmlReader;
use services\Traits\TraitConsole;
use Exception;
use services\Morellato\Gls\Service\OrderService;
use Illuminate\Filesystem\Filesystem;
use services\Morellato\Loggers\DatabaseOrderLogger as Logger;

class OrderHandler
{

    use TraitConsole;

    /**
     * @var bool
     */
    protected $debug = false;

    /**
     * @var Filesystem
     */
    protected $files;

    /**
     * @var Logger
     */
    protected $logger;

    /**
     * @var ShipmentXmlReader
     */
    protected $shipmentXmlReader;

    /**
     * @var CarrierShipmentXmlReader
     */
    protected $carrierShipmentXmlReader;

    /**
     * @var OrderService
     */
    protected $orderService;

    /**
     * @var bool
     */
    protected $saveFile = true;

    /**
     * @var bool
     */
    protected $saveMigration = true;

    /**
     * OrderHandler constructor.
     * @param Filesystem $files
     * @param Logger $logger
     * @param ShipmentXmlReader $shipmentXmlReader
     * @param CarrierShipmentXmlReader $carrierShipmentXmlReader
     */
    function __construct(Filesystem $files, Logger $logger, ShipmentXmlReader $shipmentXmlReader, CarrierShipmentXmlReader $carrierShipmentXmlReader)
    {
        $this->files = $files;
        $this->logger = $logger;
        $this->shipmentXmlReader = $shipmentXmlReader;
        $this->carrierShipmentXmlReader = $carrierShipmentXmlReader;
    }

    /**
     * @return boolean
     */
    public function isDebug()
    {
        return $this->debug;
    }

    /**
     * @param boolean $debug
     *
     * @return $this
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
        return $this;
    }

    /**
     * @return ShipmentXmlReader
     */
    public function getShipmentXmlReader()
    {
        return $this->shipmentXmlReader;
    }

    /**
     * @return CarrierShipmentXmlReader
     */
    public function getCarrierShipmentXmlReader()
    {
        return $this->carrierShipmentXmlReader;
    }

    /**
     * @return OrderService
     */
    public function getOrderService()
    {
        return $this->orderService;
    }

    /**
     * @param OrderService $orderService
     */
    public function setOrderService($orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     *
     */
    public function make()
    {
        if(feats()->switchEnabled(\Core\Cfg::SERVICE_GLS_ORDER_MIGRATION)){
            $this->fetchOrders();
        }
    }

    /**
     *
     */
    private function fetchOrders()
    {
        $rows = Order::trackableWithGls()->orderBy('id', 'desc')->select('id')->get();
        $many = count($rows);
        $this->console->line("Found [$many] orders to check...");
        foreach ($rows as $row) {
            $this->console->line("Checking order [$row->id]");
            $this->trackOrder($row->id);
        }
    }

    /**
     * @param $order_id
     */
    public function trackOrder($order_id)
    {
        $orderService = new OrderService($order_id);
        $this->setOrderService($orderService);
        $order = $orderService->getOrder();
        $shipping_number = null;
        $carrier_shipping = null;

        $tracked = false;

        if (is_null($order)) {
            $msg = "Could not track order[$order_id] in Gls\\OrderHandler, since the model is not retrieved from the database.";
            $this->console->error($msg);
            audit_error($msg);
            return;
        }

        $glsTrackingMode = $order->getGlsTrackingMode();
        $shipping_number = $order->getGlsShippingNumber();
        $carrier_shipping = $order->getGlsCarrierShipping();

        switch ($glsTrackingMode) {

            //tracking by BDA
            case $orderService::GLS_TRACKING_MODE_BDA:
                $tracked = $this->trackOrderWithShippingNumber($order, $shipping_number);
                break;

            //tracking by NUMRIT
            case $orderService::GLS_TRACKING_MODE_NUMRIT:
                $tracked = $this->trackOrderWithCarrierShipping($order, $carrier_shipping);
                break;

        }

        if (false === $tracked) {
            audit_track("Could not trackOrder for order [$order_id]; no shipping_number or carrier_shipping provided", __METHOD__);
            $orderService->resetTrackingResponse();
        }
    }

    /**
     * @param Order $order
     * @param string $shipping_number
     * @return bool
     */
    protected function trackOrderWithShippingNumber(Order $order, $shipping_number)
    {
        if (false === \OrderHelper::isValidGlsCode($shipping_number)) {
            audit_track("Could not trackOrder for order [$order->id]; the provided shipping_number [$shipping_number] is not valid", __METHOD__);
            return false;
        }

        try {
            $this->console->line("Resolving tracking for BDA [$shipping_number]");
            $this->shipmentXmlReader->setModeByOrder($order)->setBda($shipping_number);
            $response = $this->shipmentXmlReader->getResponse();
            if($this->isDebug()){
                $apiUrl = $this->shipmentXmlReader->getGlsXmlUrl();
                $this->console->comment("Fetched API URI at: $apiUrl");
                audit($response, __METHOD__, 'XML RESPONSE');
                $this->getOrderService()->setDebug(true);
            }
            $responseData = $this->getOrderService()->saveTrackingResponse($response);
            $this->console->info("Tracking for order [$order->id]($order->reference) with BDA [$shipping_number] successfully | CARRIER_RESULT: {$responseData['result']} | ORDER_STATE: {$responseData['order_status_id']}");
            $this->logger->log("Tracking for order [$order->id]($order->reference) with BDA [$shipping_number] successfully | CARRIER_RESULT: {$responseData['result']} | ORDER_STATE: {$responseData['order_status_id']}", 'track_and_trace');
            return true;
        } catch (Exception $e) {
            $this->logger->error("Tracking for order [$order->id]($order->reference) with BDA [$shipping_number] has thrown an error: " . $e->getMessage(), 'track_and_trace');
            $this->console->error($e->getMessage() . " | BDA: $shipping_number");
            audit_error("Tracking for order [$order->id]($order->reference) with BDA [$shipping_number] has thrown an error: " . $e->getMessage());
        }
        return false;
    }

    /**
     * @param Order $order
     * @param string $carrier_shipping
     * @return bool
     */
    protected function trackOrderWithCarrierShipping(Order $order, $carrier_shipping)
    {
        if (false === \OrderHelper::isValidGlsCode($carrier_shipping)) {
            audit_track("Could not trackOrder for order [$order->id]; the provided carrier_shipping [$carrier_shipping] is not valid", __METHOD__);
            return false;
        }
        $shipping_number = null;
        try {
            $this->console->line("Resolving tracking for NUMRIT [$carrier_shipping]");
            $this->carrierShipmentXmlReader->setModeByOrder($order)->setCarrierShipment($carrier_shipping);
            $response = $this->carrierShipmentXmlReader->getResponse();
            if($this->isDebug()){
                $apiUrl = $this->carrierShipmentXmlReader->getGlsXmlUrl();
                $this->console->comment("Fetched API URI at: $apiUrl");
                audit($response, __METHOD__, 'XML RESPONSE');
                $this->getOrderService()->setDebug(true);
            }
            $shipping_number = $this->getOrderService()->saveCarrierTrackingResponse($response);
            if (is_null($shipping_number)) {
                throw new Exception("Could not determine shipping number from NUMRIT  [$carrier_shipping]");
            }
            return true;
        } catch (Exception $e) {
            $this->logger->error("Tracking for order [$order->id]($order->reference) with NUMRIT [$carrier_shipping] has thrown an error: " . $e->getMessage(), 'track_and_trace');
            $this->console->error($e->getMessage() . " | BDA: $shipping_number | NUMRIT: $carrier_shipping");
            audit_error("Tracking for order [$order->id]($order->reference) with NUMRIT [$carrier_shipping] has thrown an error: " . $e->getMessage());
        }
        return false;
    }


}