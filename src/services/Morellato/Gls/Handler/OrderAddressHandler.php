<?php

namespace services\Morellato\Gls\Handler;

use Config;
use DB;
use App;
use Utils;
use Exception;
use services\Morellato\Gls\Service\OrderService;


use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use services\Morellato\Loggers\DatabaseOrderLogger as Logger;
use services\Morellato\Gls\Xml\Reader\AddressXmlReader;
use OrderState;
use Carbon\Carbon;

class OrderAddressHandler
{

    protected $console;
    protected $files;
    protected $logger;

    protected $saveFile = true;
    protected $saveMigration = true;


    function __construct(Command $console, Filesystem $files, Logger $logger, AddressXmlReader $addressXmlReader)
    {
        $this->console = $console;
        $this->files = $files;
        $this->logger = $logger;
        $this->addressXmlReader = $addressXmlReader;
    }

    public function make()
    {
        if(feats()->switchEnabled(\Core\Cfg::SERVICE_GLS_ORDER_ADDRESS)){
            $this->fetchOrders();
        }
    }


    private function fetchOrders()
    {
        $start_id = Config::get('soap.order.startId');

        $invalid_status = [
            OrderState::STATUS_CANCELED,
            OrderState::STATUS_REFUNDED_PARTIALLY,
            OrderState::STATUS_REFUNDED,
            OrderState::STATUS_REJECTED,
            OrderState::STATUS_CLOSED,
            OrderState::STATUS_CANCELED_AFFILIATE,
            OrderState::STATUS_OUTOFSTOCK
        ];

        $invalid_status_str = implode(',', $invalid_status);

        $date = Carbon::yesterday()->addMonth(-1)->format('Y-m-d');

        $query = "select id from orders where id > $start_id
                  and isnull(deleted_at) and created_at >= '$date'
                  and status not in ($invalid_status_str)                  
                  and verified_address=0
                  and id not in (select order_id from mi_address_trackings where result=906) order by id desc";

        $this->console->comment("Query: $query");
        $rows = DB::select($query);
        $many = count($rows);
        $this->console->line("Found [$many] orders to check...");
        foreach ($rows as $row) {
            $this->console->line("Checking order [$row->id]");
            $this->checkAddressForOrder($row->id);
        }
    }


    private function testCustomOrder()
    {
        $order_id = 24026;
        $this->checkAddressForOrder($order_id);
    }


    private function testXml()
    {
        $this->console->line('Testing fake XML');
        $xmlPath = storage_path('xml/gls/CheckAddress.xml');
        $this->addressXmlReader->setResourceBody($this->files->get($xmlPath));
        $this->addressXmlReader->make(false);
        if ($this->addressXmlReader->isCorrect()) {
            $this->console->info('The address is correct!');
        } else {
            $this->console->error('The address is not correct!');
        }
    }


    public function checkAddressForOrder($order_id)
    {
        $orderService = new OrderService($order_id);
        $shipping_address = $orderService->getOrder()->getShippingAddress();
        Utils::log($shipping_address);
        $this->addressXmlReader->setAddress($shipping_address);
        $this->addressXmlReader->make();
        if ($this->addressXmlReader->isCorrect()) {
            $orderService->setVerified();
            $this->console->info('Address successfully resolved!');
        } else {
            $this->console->error('Could not resolve address properly!');
        }
        $orderService->saveAddressResponse($shipping_address->id, $this->addressXmlReader->getResponse());
    }


    static function getUrlForXml($order_id)
    {
        $orderService = new OrderService($order_id);
        $shipping_address = $orderService->getOrder()->getShippingAddress();
        $xmlReader = App::make('services\Morellato\Gls\Xml\Reader\AddressXmlReader');
        $xmlReader->setAddress($shipping_address);
        return $xmlReader->getUrl();
    }


}