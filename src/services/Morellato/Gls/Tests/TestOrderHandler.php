<?php


namespace services\Morellato\Gls\Tests;


use services\Morellato\Gls\Handler\OrderHandler;
use services\Traits\TraitConsole;
use Config;
use DB;
use Illuminate\Support\Collection;
use Utils;
use Exception;
use Order;
use Illuminate\Filesystem\Filesystem;

class TestOrderHandler
{
    use TraitConsole;

    protected $files;

    protected $orderHandler;


    /**
     * TestOrderHandler constructor.
     * @param Filesystem $files
     * @param OrderHandler $orderHandler
     */
    function __construct(Filesystem $files, OrderHandler $orderHandler)
    {
        $this->files = $files;
        $this->orderHandler = $orderHandler;
    }

    /**
     *
     */
    function attachConsole(){
        $this->orderHandler->setConsole($this->console);
    }



    /**
     * @param array $ids
     */
    function testTrackingWithCustomerOrders(array $ids)
    {
        $orders = Order::whereIn('id', $ids)->get();
        foreach ($orders as $order) {
            $this->getConsole()->comment("Tracking Order [$order->id]");
            $this->orderHandler->trackOrder($order->id);
        }
    }

    /**
     * @param $shipping_number
     * @param $mode
     */
    public function testShipment($shipping_number, $mode)
    {
        $this->getConsole()->line("Resolving tracking for BDA [$shipping_number]");
        $this->orderHandler->getShipmentXmlReader()->setMode($mode)->setBda($shipping_number);
        $response = $this->orderHandler->getShipmentXmlReader()->getResponse();
        audit($response, __METHOD__);
    }

    /**
     * @param $carrier_shipping
     * @param Order|null $order
     */
    public function testCarrierShipment($carrier_shipping, Order $order = null)
    {
        $this->getConsole()->line("Resolving tracking for NUMRIT [$carrier_shipping]");
        if (!is_null($order)) {
            $this->orderHandler->getCarrierShipmentXmlReader()->setModeByOrder($order);
        }
        $this->orderHandler->getCarrierShipmentXmlReader()->setCarrierShipment($carrier_shipping);
        $response = $this->orderHandler->getCarrierShipmentXmlReader()->getResponse();
        audit($response, __METHOD__);
    }
}