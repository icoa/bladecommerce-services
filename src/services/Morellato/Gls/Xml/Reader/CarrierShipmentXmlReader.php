<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 13/03/2017
 * Time: 11:38
 */

namespace services\Morellato\Gls\Xml\Reader;

use Exception;
use SimpleXMLElement;
use Utils;


class CarrierShipmentXmlReader extends ShipmentXmlReader
{

    protected $url = 'https://wwwdr.gls-italy.com/XML/get_xml_track.php?locpartenza=:loc&numrit=:numrit&CodCli=:code';
    protected $url_shop = 'https://wwwdr.gls-italy.com/XML/get_xml_track.php?locpartenza=:loc&numrit=:numrit&CodCli=:code';
    protected $url_default = 'https://wwwdr.gls-italy.com/XML/get_xml_track.php?locpartenza=:loc&numrit=:numrit&CodCli=:code';
    protected $response;

    protected function init()
    {
        $this->addParam('code', $this->config['CodCli']);
        $this->addParam('loc', $this->config['geo']['loc']);
    }

    /**
     * @param $mode
     * @return $this
     */
    function setMode($mode)
    {
        return $this;
    }

    /**
     * @param $code
     * @param $autoFetch
     */
    function setCarrierShipment($code, $autoFetch = true)
    {
        if (strlen($code) <= 2) {
            audit_error("Invalid carrier shipment code $code", __METHOD__);
            return;
        }
        $this->addParam('numrit', $code);

        if(false === $autoFetch)
            return;

        try {
            $this->fetchXml();
            $this->parse();
        } catch (Exception $e) {
            $this->addError($e->getMessage());
            audit_exception($e, __METHOD__);
        }
    }

    protected function parse()
    {
        unset($this->response);
        $this->errors = [];
        $xmlElement = new SimpleXMLElement((string)$this->resourceBody);

        $data = new \stdClass();
        $data->tracking = [];

        if (isset($xmlElement->TESTOERRORE)) {
            throw new Exception(trim($xmlElement->TESTOERRORE));
        }

        $xmlTrackingName = 'TRACKINGRITIRO';
        $hasShipment = false;
        if(isset($xmlElement->RITIRO->SPEDIZIONE) and is_object($xmlElement->RITIRO->SPEDIZIONE)){
            $xmlTrackingName = 'TRACKING';
            $hasShipment = true;
        }
        if($hasShipment == false){
            throw new Exception("Could not find any node 'SPEDIZIONE' in the XML");
        }
        $shipmentInfos = $xmlElement->RITIRO->SPEDIZIONE->children();
        foreach ($shipmentInfos as $shipmentInfo) {
            $name = $shipmentInfo->getName();
            if ($name == $xmlTrackingName) {
                $trackingInfos = $xmlElement->RITIRO->SPEDIZIONE->{$xmlTrackingName}->children();
                $trackingObj = null;
                foreach ($trackingInfos as $trackingInfo) {
                    $trackingName = $trackingInfo->getName();
                    if ($trackingName == 'Data') {
                        if (isset($trackingObj->Data)) {
                            $data->tracking[] = $trackingObj;
                        }
                        $trackingObj = new \stdClass();
                    }
                    $trackingObj->{$trackingName} = trim((string)$trackingInfo[0]);
                }
                if(is_object($trackingObj)){
                    $data->tracking[] = $trackingObj;
                }
            } else {
                $name = $shipmentInfo->getName;
                if(!in_array($name, ['RITIRO', 'SPEDIZIONE']))
                    $data->{$shipmentInfo->getName()} = trim((string)$shipmentInfo[0]);
            }
        }
        Utils::log($data, __METHOD__ . '::DATA');
        $this->response = $data;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getResponse()
    {
        if($this->hasErrors())
            throw new Exception(implode(',', $this->getErrors()));

        return $this->response;
    }
}