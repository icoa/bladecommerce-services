<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 13/03/2017
 * Time: 11:38
 */

namespace services\Morellato\Gls\Xml\Reader;

use Exception;
use SimpleXMLElement;
use Utils;


class AddressXmlReader extends XmlReader
{

    //protected $url = 'https://weblabeling.gls-italy.com/utility/wsCheckAddress.asmx/CheckAddress?SedeGls=:loc&CodiceClienteGls=:username&PasswordClienteGls=:password&SiglaProvincia=:state&Cap=:cap&Localita=:city&Indirizzo=:address';
    protected $url = 'https://checkaddress.gls-italy.com/wsCheckAddress.asmx/CheckAddress?SedeGls=:loc&CodiceClienteGls=:username&PasswordClienteGls=:password&SiglaProvincia=:state&Cap=:cap&Localita=:city&Indirizzo=:address';
    protected $response;

    protected function init()
    {
        $this->setCredentials();
    }

    function test()
    {
        audit(__METHOD__);
        $this->addParam('state', 'RM');
        $this->addParam('cap', '00052');
        $this->addParam('city', 'cerveteri');
        $this->addParam('address', 'via acerra 40');
        $this->fetchXml();
    }

    function setCredentials()
    {
        $this->addParam('loc', $this->config['geo']['loc']);
        $this->addParam('username', $this->config['geo']['username']);
        $this->addParam('password', $this->config['geo']['password']);
    }

    function setAddress($address)
    {
        $this->addParam('state', $address->stateCode);
        $this->addParam('cap', $address->postcode);
        $this->addParam('city', $address->city);
        $this->addParam('address', $address->address);
    }

    function getUrl(){
        return $this->makeUrl();
    }

    function make($fetch = true)
    {
        if($fetch)
            $this->fetchXml();

        $this->parse();
    }

    function setResourceBody($body){
        $this->resourceBody = $body;
    }

    protected function parse()
    {
        $xmlElement = new SimpleXMLElement((string)$this->resourceBody);
        Utils::log($xmlElement, __METHOD__);
        $data = new \stdClass();
        $data->Esito = (string)$xmlElement->Esito;
        $data->AddressList = [];

        $xmlNodes = $xmlElement->children();
        foreach($xmlNodes as $xmlNode){
            if($xmlNode->getName() == 'Address'){
                $address = new \stdClass();
                $addressInfos = $xmlNode->children();
                foreach ($addressInfos as $addressInfo) {
                    $address->{$addressInfo->getName()} = trim((string)$addressInfo[0]);
                }
                $data->AddressList[] = $address;
            }
        }
        Utils::log($data, __METHOD__);
        $this->response = $data;
    }

    function isCorrect(){
        if($this->response){
            if(isset($this->response->Esito)){
                return $this->response->Esito == 'Destinazione corretta.';
            }
        }
        return false;
    }

    function getResponse(){
        if($this->response and is_array($this->response->AddressList)){
            return $this->response;
        }
        return null;
    }
}