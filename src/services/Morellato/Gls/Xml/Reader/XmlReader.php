<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 13/03/2017
 * Time: 11:37
 */

namespace services\Morellato\Gls\Xml\Reader;

use DB, Utils, Config;
use Illuminate\Filesystem\Filesystem;
use services\Morellato\Loggers\DatabaseLogger as Logger;
use GuzzleHttp\Client;
use Exception;
use services\Traits\TraitLocalDebug;
use SimpleXMLElement;

class XmlReader
{
    use TraitLocalDebug;

    protected $localDebug = false;

    protected $url = 'https://wwwdr.gls-italy.com/XML/get_xml_track.php?locpartenza=:loc&bda=:bda&CodCli=:code';

    protected $params = [];
    protected $errors = [];

    protected $config;

    protected $logger;
    protected $client;
    protected $resourceBody;

    function __construct(Filesystem $files, Logger $logger, Client $client)
    {
        $this->files = $files;
        $this->logger = $logger;
        $this->client = $client;
        $this->config = Config::get('gls');
        $this->init();
    }

    protected function init()
    {

    }

    protected function setParams(array $params)
    {
        $this->params = array_merge($params, $this->params);
    }

    protected function addParam($key, $value)
    {
        $this->params[$key] = $value;
    }

    protected function getParam($key, $default = null)
    {
        return array_get($this->params, $key, $default);
    }

    /**
     * @param $error
     * @return $this
     */
    protected function addError($error){
        $this->errors[] = $error;
        return $this;
    }

    /**
     * @return bool
     */
    protected function hasErrors(){
        return !empty($this->errors);
    }

    /**
     * @return array
     */
    protected function getErrors(){
        return $this->errors;
    }

    protected function makeUrl()
    {
        $url = $this->url;
        foreach ($this->params as $key => $value) {
            $url = str_replace(':' . $key, $value, $url);
        }
        return $url;
    }

    function getGlsXmlUrl(){
        return $this->makeUrl();
    }

    /**
     * @param $mode
     * @return $this
     */
    function setMode($mode)
    {
        //this has been deprecated
        /*if (isset($this->url_shop) and isset($this->url_default)) {
            $this->url = ($mode == 'shop') ? $this->url_shop : $this->url_default;
        }*/
        $this->url = $this->url_default;
        return $this;
    }

    /**
     * @param \Order $order
     * @return $this|ShipmentXmlReader
     */
    function setModeByOrder(\Order $order)
    {
        if (is_null($order))
            return $this;

        $shopStartingPoint = $order->hasShopAvailability();
        $shopEndingPoint = $order->hasDeliveryStore();

        $leftKey = ($shopStartingPoint) ? 'shop' : 'online';
        $rightKey = ($shopEndingPoint) ? 'shop' : 'customer';

        $config_key = "gls.codes.{$leftKey}|{$rightKey}";

        $this->audit("Reading config key: $config_key", __METHOD__);

        $code = config($config_key, config('gls.CodCli'));
        $this->addParam('code', $code);

        if ($order->hasShopAvailability()) {
            $this->setMode('shop');
        }

        return $this;
    }


    protected function fetchXml()
    {
        $url = $this->makeUrl();
        $this->audit($url, 'FETCHING URL', __METHOD__);
        $res = $this->client->get($url);

        $statusCode = $res->getStatusCode();

        if ($statusCode == 200) {
            $contentType = $res->getHeader('Content-Type');
            $this->audit("Response Content-Type: $contentType", __METHOD__);
        } else {
            throw new Exception("GLS Xml Reader has responded with a status code of [$statusCode]");
        }

        $this->resourceBody = $res->getBody();

        $this->audit((string)$this->resourceBody, 'RESOURCE_BODY', __METHOD__);

        if (strlen($this->resourceBody) <= 0) {
            throw new Exception("GLS Xml Reader has responded with an empty body");
        }
    }


}