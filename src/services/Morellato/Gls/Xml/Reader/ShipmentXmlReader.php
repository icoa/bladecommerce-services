<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 13/03/2017
 * Time: 11:38
 */

namespace services\Morellato\Gls\Xml\Reader;

use Exception;
use SimpleXMLElement;
use Utils;


class ShipmentXmlReader extends XmlReader
{

    protected $url = 'https://wwwdr.gls-italy.com/XML/get_xml_track.php?locpartenza=:loc&bda=:bda&CodCli=:code';
    protected $url_shop = 'https://wwwdr.gls-italy.com/XML/get_xml_track.php?sedecon=:loc&bda=:bda&CodCli=:code';
    protected $url_default = 'https://wwwdr.gls-italy.com/XML/get_xml_track.php?locpartenza=:loc&bda=:bda&CodCli=:code';
    protected $response;
    const ERROR_SHIPMENT_NOT_FOUND = 404;

    protected function init()
    {
        $this->addParam('code', $this->config['CodCli']);
        $this->addParam('loc', $this->config['geo']['loc']);
    }

    function test()
    {
        $this->addParam('bda', '0083131970');
        $this->fetchXml();
    }

    function make()
    {
        $this->test();
        $this->parse();
    }

    /**
     * @param $bda
     * @param bool $autoFetch
     */
    function setBda($bda, $autoFetch = true)
    {
        $this->addParam('bda', $bda);

        if (false === $autoFetch)
            return;

        try {
            $this->fetchXml();
            $this->parse();
        } catch (Exception $e) {
            $code = $e->getCode();
            $this->addError($e->getMessage());
            if($code !== self::ERROR_SHIPMENT_NOT_FOUND){
                audit_exception($e, __METHOD__);
            }
        }
    }

    protected function parse()
    {
        unset($this->response);
        $this->errors = [];
        $xmlElement = new SimpleXMLElement((string)$this->resourceBody);

        $data = new \stdClass();
        $data->tracking = [];
        $trackingObj = new \stdClass();

        if (isset($xmlElement->TESTOERRORE)) {
            $error =  strtoupper(trim($xmlElement->TESTOERRORE));
            if($error === 'SPEDIZIONE NON TROVATA'){
                throw new Exception($error, self::ERROR_SHIPMENT_NOT_FOUND);
                return;
            }
            throw new Exception($error);
        }

        $shipmentInfos = $xmlElement->SPEDIZIONE->children();
        foreach ($shipmentInfos as $shipmentInfo) {
            $name = $shipmentInfo->getName();
            if ($name == 'TRACKING') {
                $trackingInfos = $xmlElement->SPEDIZIONE->TRACKING->children();
                $trackingObj = null;
                foreach ($trackingInfos as $trackingInfo) {
                    $trackingName = $trackingInfo->getName();
                    if ($trackingName == 'Data') {
                        if (isset($trackingObj->Data)) {
                            $data->tracking[] = $trackingObj;
                        }
                        $trackingObj = new \stdClass();
                    }
                    $trackingObj->{$trackingName} = trim((string)$trackingInfo[0]);
                }
                if (is_object($trackingObj)) {
                    $data->tracking[] = $trackingObj;
                }
            } else {
                $data->{$shipmentInfo->getName()} = trim((string)$shipmentInfo[0]);
            }
        }
        Utils::log($data, 'DATA');
        $this->response = $data;
    }

    /**
     * @return mixed
     * @throws Exception
     */
    public function getResponse()
    {
        if($this->hasErrors())
            throw new Exception(implode(',', $this->getErrors()));

        return $this->response;
    }
}