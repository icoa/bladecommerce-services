<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 15/02/2017
 * Time: 13:26
 */

namespace services\Morellato\Gls\Service;

use Order;
use OrderState;
use services\Models\OrderTracking;
use Utils;
use Config;
use View;
use Illuminate\Support\Str;
use DB;
use File;

class OrderService extends AbstractOrderService
{

    const GLS_TRACKING_MODE_NUMRIT = 'numrit';
    const GLS_TRACKING_MODE_BDA = 'bda';

    /**
     * @var bool
     */
    protected $debug = false;

    protected $order;
    protected $products;
    protected $root;
    protected $xml;
    protected $common = [];
    protected $table_address = 'mi_address_responses';
    protected $table_tracking = 'mi_address_trackings';

    /**
     * @return boolean
     */
    public function isDebug()
    {
        return $this->debug;
    }

    /**
     * @param boolean $debug
     *
     * @return $this
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
        return $this;
    }

    /**
     *
     */
    function setVerified()
    {
        $order_id = $this->getOrderId();
        DB::table('orders')->where('id', $order_id)->update(['verified_address' => 1]);
    }

    /**
     * @param $address_id
     * @param $response
     */
    function saveAddressResponse($address_id, $response)
    {
        if (is_null($response)) {
            return;
        }
        $result = $response->Esito;
        $order_id = $this->getOrderId();
        $address_list = serialize($response->AddressList);
        $model = DB::table($this->table_address)->where('order_id', $order_id)->where('address_id', $address_id)->first();
        $data = compact('result', 'order_id', 'address_id', 'address_list');

        if($this->isDebug()){
            audit($data, __METHOD__ . '::DATA');
            return;
        }

        if ($model) {
            $data['updated_at'] = date('Y-m-d H:i:s');
            DB::table($this->table_address)->where('id', $model->id)->update($data);
        } else {
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            DB::table($this->table_address)->insert($data);
        }
    }

    /**
     * @param $shipping_number
     * @param $location
     * @return mixed
     */
    function getTrackingUrl($shipping_number, $location)
    {
        $pattern = 'https://www.gls-italy.com/index.php?option=com_gls&view=track_e_trace&mode=search&numero_spedizione=:code&tipo_codice=nazionale';
        return str_replace(':code', $location . $shipping_number, $pattern);
    }


    protected function updateOrderStatusByResult($result)
    {
        $order_status_id = null;
        /** @var Order $order */
        $order = $this->getOrder();
        $order_id = $order->id;
        //now update the order status
        /*
         * the order should be updated against the value of $result variable
         * the relationship between API $result and order status, should be as following:
         *
         * $result  |   order status    |   order status id
         * 0        |   working         |   STATUS_WORKING (in one of the previous was SHIPPED, DELIVERED, CLOSED)
         * 902-903  |   shipped         |   STATUS_SHIPPED
         * 904-905  |   delivered       |   STATUS_DELIVERED
         * 906      |   closed          |   STATUS_CLOSED
         * */


        /**
         * UPDATE: now the reale state depends on this table:
         *
         *  Partenza        Consegna    Codice Status GLS      Codice Status Ordine
         *  Deufol          domicilio   902                    Spedito
         *  Deufol          negozio     902                    Spedito
         *  Negozio         domicilio   902                    Spedito
         *  Negozio         negozio     902                    Spedito
         *
         *  Deufol          domicilio   905                    In consegna
         *  Deufol          negozio     905                    In consegna
         *  Negozio         domicilio   905                    In consegna
         *  Negozio         negozio     905                    In consegna
         *
         *  Deufol          domicilio   906                    Consegnato
         *  Deufol          negozio     906                    Pronto per il ritiro
         *  Negozio         domicilio   906                    Consegnato
         *  Negozio         negozio     906                    Pronto per il ritiro
         */

        $shopStartingPoint = $order->hasShopAvailability();
        $shopEndingPoint = $order->hasDeliveryStore();

        $leftKey = ($shopStartingPoint) ? 'shop' : 'online';
        $rightKey = ($shopEndingPoint) ? 'shop' : 'customer';

        $gls_code = (int)$result;

        //same specs and alias
        if ($gls_code === 903)
            $gls_code = 902;

        if ($gls_code === 904 or $gls_code === 908)
            $gls_code = 905;

        $matrix = [
            '28.online.customer' => OrderState::STATUS_PENDING,
            '28.online.shop' => OrderState::STATUS_PENDING,
            '28.shop.customer' => OrderState::STATUS_PENDING,
            '28.shop.shop' => OrderState::STATUS_PENDING,

            '29.online.customer' => OrderState::STATUS_REJECTED,
            '29.online.shop' => OrderState::STATUS_REJECTED,
            '29.shop.customer' => OrderState::STATUS_REJECTED,
            '29.shop.shop' => OrderState::STATUS_REJECTED,

            '902.online.customer' => OrderState::STATUS_SHIPPED,
            '902.online.shop' => OrderState::STATUS_SHIPPED,
            '902.shop.customer' => OrderState::STATUS_SHIPPED,
            '902.shop.shop' => OrderState::STATUS_SHIPPED,

            '905.online.customer' => OrderState::STATUS_PENDING_DELIVERY,
            '905.online.shop' => OrderState::STATUS_PENDING_DELIVERY,
            '905.shop.customer' => OrderState::STATUS_PENDING_DELIVERY,
            '905.shop.shop' => OrderState::STATUS_PENDING_DELIVERY,

            '906.online.customer' => OrderState::STATUS_DELIVERED,
            '906.shop.customer' => OrderState::STATUS_DELIVERED,
            //'906.online.shop' => OrderState::STATUS_READY,
            //'906.shop.shop' => OrderState::STATUS_READY,
        ];

        $search_key = "$gls_code.$leftKey.$rightKey";

        if(in_array($search_key, ['906.online.shop', '906.shop.shop'], true)){
            //do nothing on GLS side
            $order->setSoftStatus(
                OrderState::STATUS_DELIVERED,
                'Migrazione automatica in base a meccaniche GLS con matrice: ' . $search_key,
                OrderTracking::byGls(OrderTracking::REASON_SHOP_CONFIRM)
                );
            return null;
        }

        if (isset($matrix[$search_key])) {
            $order_status_id = $matrix[$search_key];
        } elseif ($gls_code === 0) {
            //TODO: check if this is still valid for moving order states
            if (in_array($order->status, [OrderState::STATUS_SHIPPED, OrderState::STATUS_DELIVERED])) {
                $order_status_id = OrderState::STATUS_WORKING;
            }
        }

        if ($order_status_id) {
            if ($order->status != $order_status_id) {
                audit_track("Order [$order_id] has been selected to change status with [$order_status_id]", __METHOD__);
                if(!$this->isDebug())
                    $order->setStatus(
                        $order_status_id,
                        true,
                        'Migrazione automatica in base a meccaniche GLS con matrice: ' . $search_key,
                        OrderTracking::byGls()
                    );
            }
        }
        return $order_status_id;
    }


    /**
     * @param $response
     * @param null $product_id
     * @return array
     */
    function saveTrackingResponse($response, $product_id = null)
    {
        $result = null;
        $order_status_id = null;
        if (is_null($response)) {
            return compact('result', 'order_status_id');
        }

        $reference = $this->getOrderReference();
        $filePath = storage_path("morellato/orders/response/{$reference}_TRACKING.json");
        File::put($filePath, json_encode($response, JSON_PRETTY_PRINT));

        $data = $this->parseAndStoreTrackingData($response, $product_id);
        audit($data, __METHOD__ . '::DATA');
        $result = $data['result'];

        //fetch the new order status
        $order_status_id = $this->updateOrderStatusByResult($result);

        return compact('result', 'order_status_id');
    }


    /**
     * @param $response
     * @param $product_id
     * @return null|integer
     */
    function saveCarrierTrackingResponse($response, $product_id = null)
    {
        if (is_null($response)) {
            return null;
        }

        $reference = $this->getOrderReference();
        $filePath = storage_path("morellato/orders/response/{$reference}_TRACKING_CARRIER.json");
        File::put($filePath, json_encode($response, JSON_PRETTY_PRINT));

        $data = $this->parseAndStoreTrackingData($response, $product_id);
        audit($data, __METHOD__ . '::DATA');

        //fetch the new order status
        $this->updateOrderStatusByResult($data['result']);

        return $data['shipping_number'];
    }


    /**
     * @param $response
     * @param null $product_id
     * @return array
     */
    protected function parseAndStoreTrackingData($response, $product_id = null){
        $order_id = $this->getOrderId();
        $tracking = serialize($response);
        $result = $response->tracking[0]->Codice;
        $shipping_number = $delivery_id = $url = null;
        //we have to build the real tracking code with 'SedePartenza' and 'NumSped';
        $loc = (isset($response->SedePartenza) and strlen($response->SedePartenza) >= 2) ? $response->SedePartenza : null;
        $numSped = (isset($response->NumSped) and strlen($response->NumSped) >= 6) ? $response->NumSped : null;

        if($loc and $numSped){
            $shipping_number = \OrderHelper::isValidGlsCode($numSped) ? $numSped : null;
            if($shipping_number)
                $url = $this->getTrackingUrl($shipping_number, $loc);
        }

        $model = DB::table($this->table_tracking)->where('order_id', $order_id)->where('product_id', $product_id)->first();
        $data = compact('result', 'order_id', 'tracking', 'product_id', 'url', 'shipping_number');
        $data['updated_at'] = date('Y-m-d H:i:s');
        if ($model) {
            if(!$this->isDebug())
                DB::table($this->table_tracking)->where('id', $model->id)->update($data);

            Utils::log("Tracking for order [$order_id] has been updated with result [$result]", __METHOD__);
        } else {
            $data['created_at'] = date('Y-m-d H:i:s');
            if(!$this->isDebug())
                DB::table($this->table_tracking)->insert($data);

            Utils::log("Tracking for order [$order_id] has been saved with result [$result]", __METHOD__);
        }

        if($shipping_number){
            $shipping_number = $loc . $shipping_number;
            //update the real shipping_number
            if(!$this->isDebug())
                DB::table('orders')->where('id', $order_id)->update(compact('shipping_number'));
        }

        return $data;
    }


    function resetTrackingResponse()
    {
        $order_id = $this->getOrderId();
        DB::table($this->table_tracking)->where('order_id', $order_id)->delete();
        //$this->updateOrderStatusByResult(0);
    }
}