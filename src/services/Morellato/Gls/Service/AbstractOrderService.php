<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 01/03/2017
 * Time: 15:47
 */

namespace services\Morellato\Gls\Service;

use Order;
use View;

class AbstractOrderService
{
    /**
     * @var \Order|null
     */
    protected $order;
    protected $products;
    protected $root;
    protected $xml;
    protected $xml_template;

    function __construct($id)
    {
        $this->order = Order::find($id);
        if(is_null($this->order)){
            throw new \Exception("Could not find any order with id $id in Gls/AbstractOrderService");
        }
        $products = $this->order->getProducts();
        $this->products = $products;

        $this->root = [];
    }

    function getOrderReference(){
        return $this->order->reference;
    }

    function getOrderId(){
        return $this->order->id;
    }

    /**
     * @return \Order|null
     */
    function getOrder(){
        return $this->order;
    }

}