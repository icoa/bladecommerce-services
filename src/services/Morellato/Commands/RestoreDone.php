<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\CsvImporters\AnagraficaImporter;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class RestoreDone extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'csv:restore';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Restore a "done" csv file to the original version';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing CSV restore...");
        $importer = \App::make('services\Morellato\Csv\Importers\\Importer',[$this]);
        $importer->restoreFiles();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}