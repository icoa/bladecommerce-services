<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use services\Morellato\Database\WarmUpper;

class DatabaseWarmup extends Command
{
    /**
     * @var WarmUpper
     */
    protected $importer;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'db:warmup';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Try to warm-up a lot of entities used in the frontend';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->importer = \App::make('services\Morellato\Database\WarmUpper', [$this]);
        $this->info("Executing DatabaseWarmup...");
        $this->importer->make();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(

        );
    }

}