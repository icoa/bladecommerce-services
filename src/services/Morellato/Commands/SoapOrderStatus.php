<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\Soap\Handler\OrderStatusHandler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class SoapOrderStatus extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'soap:orderStatus';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Switch the status of an order from NEW to WORKING, with some checks. {--order=X for forcing single Order}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing Soap Order...");
        /** @var OrderStatusHandler $handler */
        $handler = \App::make('services\Morellato\Soap\Handler\OrderStatusHandler', [$this]);
        $order = $this->option('order');
        $pretend = $this->option('pretend') == 1;
        $handler->setPretend($pretend)->make($order);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('order', null, InputOption::VALUE_OPTIONAL, 'Force to skip the automatic selection, with given order ID'),
            array('pretend', null, InputOption::VALUE_OPTIONAL, 'Perform all the checks, but do not upsert anything or send notifications'),
        );
    }

}