<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\Csv\Downloader;
use services\Morellato\CsvImporters\AnagraficaImporter;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FtpMover extends Command
{
    protected $downloader;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'csv:ftpmove';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Move all remote FTP files into the proper directory';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        if(feats()->switchDisabled(\Core\Cfg::SERVICE_FTP_CSV_DOWNLOAD)){
            return;
        }
        $this->downloader = \App::make('services\Morellato\Csv\Downloader',[$this]);
        $this->info("Executing FtpMover...");
        $this->downloader->move();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}