<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class SoapOrderXml extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'soap:xml';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create the XML Request order for SAP';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing Soap XML...");
        $order = $this->argument('order');
        $handler = \App::make('services\Morellato\Soap\Handler\OrderHandler',[$this]);
        try{
            $handler->createXml($order);
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(array('order', InputArgument::REQUIRED, 'The order id'),);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}