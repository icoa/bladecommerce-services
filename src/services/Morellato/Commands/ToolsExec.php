<?php

namespace services\Morellato\Commands;

use App;
use Illuminate\Console\Command;
use services\Morellato\Repositories\TranslateRepository;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;


class ToolsExec extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'tools:exec';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute a task from the tools repository';


    protected $repository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->repository = App::make('services\Morellato\Repositories\ToolsRepository');
        $this->repository->setConsole($this);
        $this->line("Executing ToolTask...");
        $this->repository->exec($this->argument('task'));
    }




    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('task', InputArgument::REQUIRED, 'The task to perform'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}