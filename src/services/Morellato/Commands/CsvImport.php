<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\CsvImporters\AnagraficaImporter;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class CsvImport extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'csv:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import a csv into a temporary table';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing CSV importer...");

        if(feats()->switchDisabled(\Core\Cfg::SERVICE_FTP_CSV_DOWNLOAD)){
            return;
        }

        $importers = Config::get('ftp.importers');

        foreach($importers as $importerName){
            $importer = \App::make('services\Morellato\Csv\Importers\\'.$importerName.'Importer',[$this]);
            $importer->make();
            unset($importer);
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}