<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\Repositories\TaskRepository;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config, DB, App;

class ExecTask extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'morellato:exec';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Exec custom task against Morellato management';


    protected $repository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->repository = App::make('services\Morellato\Repositories\TaskRepository');
        $this->repository->setConsole($this);
        $this->line("Executing ExecTask...");
        $this->repository->exec($this->argument('task'));
    }




    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('task', InputArgument::REQUIRED, 'The task to perform'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}