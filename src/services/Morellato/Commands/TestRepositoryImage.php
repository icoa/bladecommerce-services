<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;
use DB;

class TestRepositoryImage extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'morellato:testrep';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fetch all info from Morellato Image Repository';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing TestRepositoryImage...");
        $sku = $this->argument('sku');
        $rows = DB::connection('repository')
            ->table('media')
            ->where('code', $sku)
            ->orderBy('VAR')->get();

        foreach($rows as $row){
            print_r($row);
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(array('sku', InputArgument::REQUIRED, 'The SKU to query'),);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}