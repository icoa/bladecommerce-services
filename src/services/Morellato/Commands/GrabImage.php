<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\Grabbers\ImageGrabber;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class GrabImage extends Command
{
    /**
     * @var ImageGrabber
     */
    protected $grabber;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'csv:grabimages'; //--mode=all

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grab a remove file from the Morellato repository';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->grabber = \App::make('services\Morellato\Grabbers\ImageGrabber',[$this]);
        $mode = $this->option('mode');
        $sku = $this->option('sku');
        $email = $this->option('email');

        $this->info("Executing GrabImages in (mode: $mode)...");

        if(\Utils::isEmail($email)){
            $this->grabber->setEmail($email);
            $this->comment("A report will be sent to $email");
        }

        $this->grabber->setInputSku($sku);
        $this->grabber->make($mode);
    }

    public function single($sku){
        $this->grabber->setInputSku($sku);
        $this->grabber->make('single');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('mode', null, InputOption::VALUE_OPTIONAL, 'If the importer must process only stocks.', 'regular'),
            array('sku', null, InputOption::VALUE_OPTIONAL, 'The SKU for the single product.', null),
            array('email', null, InputOption::VALUE_OPTIONAL, 'The Email to send the report.', null),
        );
    }

}