<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Bluespirit\Negoziando\Shop\ShopRepository;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class AssignGMapMarkerToShops extends Command
{
    /**
     * @var ShopRepository
     */
    protected $repository;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'shop:assign_markers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scan all shops and assign Google Maps markers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->repository = \App::make(ShopRepository::class);
        $this->repository->setConsole($this);
        $this->info('Executing AssignGMapMarkerToShops');
        $this->repository->assignGmapMarkers();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}
