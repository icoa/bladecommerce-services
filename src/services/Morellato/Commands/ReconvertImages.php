<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\CsvImporters\AnagraficaImporter;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class ReconvertImages extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'csv:reconvert';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Try to reconvert product images to cache';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing CSV reconvert Images...");
        $importer = \App::make('services\Morellato\Grabbers\ImageGrabber',[$this]);
        $importer->reconvertProductImages();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}