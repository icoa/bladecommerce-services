<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config, DB, Core;
use Order;

class TestGiftCard extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'morellato:testorder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Performs some test placing the given order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing TestGiftCard...");
        $this->testOrderPlace();
    }


    private function testOrderPlace()
    {
        $this->line("Testing OrderPlace...");
        $order = Order::find($this->argument('id'));
        $this->line("Found order " . $order->id);
        $order->getRepository()->orderPlaced();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('id', InputArgument::REQUIRED, 'The Order ID'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}