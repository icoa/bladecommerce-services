<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Sentry;
use Illuminate\Encryption\Encrypter;

class ApiUserEncrypter extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'api:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Show all enabled users with API scope and their encrypted passwords';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $scopes = ['e-one', 'e1'];
        foreach ($scopes as $scope) {
            $user = config("api.$scope");
            if ($user) {
                $username = $user['email'];
                $password = $this->password($user['password']);
                $this->info($username);
                $this->info($password);
            }
        }
    }

    /**
     * @param $text
     * @return string
     */
    private function password($text)
    {
        $encrypter = new Encrypter(config('app.key'));
        $secret = $encrypter->encrypt($text);
        return $secret;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}