<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\Database\Sql\Connection;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config, DB, Core;
use Illuminate\Support\Str;
use Order;

class TestNegoziando extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'morellato:testnegoziando';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Performs some test on Negoziando database; Usage: action=connection|bestshop|reassign|bestmapping|product|taken|custom|concurrency --sku=FO.FTW2103 --id=111 --preferred=683';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing TestNegoziando...");
        $action = $this->argument('action');

        switch ($action) {

            case 'connection':
                $this->testMsSql();
                break;

            case 'bestshop':
                $this->testBestShop();
                break;

            case 'reassign':
                $this->reassignBestShop();
                break;

            case 'bestmapping':
                $this->testBestMapping();
                break;

            case 'product':
                $this->testProduct();
                break;

            case 'taken':
                $this->testTaken();
                break;

            case 'custom':
                $this->testCustom();
                break;

            case 'random':
                $this->testRandom();
                break;

            case 'concurrency':
                $this->testConcurrency();
                break;
        }
    }


    private function testMsSql()
    {
        $this->line("Testing MsSql connection...");
        try {
            /** @var Connection $conn */
            $conn = Core::getNegoziandoConnection();
            $conn->setConsole($this);
            $conn->test();
            $this->info("-> OK");
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }


    private function testProduct()
    {
        audit_watch();
        $this->line('Testing Product availability algorithm...');
        $sku = trim($this->option('sku'));
        if ($sku === '') {
            $this->error('SKU is mandatory for testing product');
            return;
        }
        try {
            /** @var Connection $conn */
            $conn = Core::getNegoziandoConnection();
            $conn->setConsole($this);
            $conn->getAvailabilityBySku($sku);
            $this->info('-> OK');
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    /**
     * p5 morellato:testnegoziando bestshop --sku=SD5803 --preferred=103 --unused=N32
     */
    private function testBestShop()
    {
        audit_watch();
        $this->line('Testing BestShop algorithm...');
        $sku = trim($this->option('sku'));
        if ($sku === '') {
            $this->error('SKU is mandatory for testing best-shop');
            return;
        }

        try {
            /** @var Connection $conn */
            $conn = Core::getNegoziandoConnection();
            $conn->setConsole($this);

            $preferred = trim($this->option('preferred'));
            if ($preferred !== '') {
                $conn->addPreferredShop($preferred);
            }

            $unused = trim($this->option('unused'));
            if ($unused !== '') {
                $conn->addUnusedShop($unused);
            }

            $skus = explode(',', $sku);
            $product_id = 1000;

            foreach ($skus as $sku) {
                $conn->addProduct($sku, 1, $product_id);
                $product_id++;
            }

            $shop = $conn->getBestShop();
            if ($shop) {
                $this->info("Found best shop: $shop");
            } else {
                $this->error('No best shop has been found');
            }

            $this->comment(audit_queries());

            $this->info("-> OK");
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            audit_exception($e, __METHOD__);
        }
    }

    /**
     * DIVERGE:     p5 morellato:testnegoziando bestmapping --sku=SEX02ANP14,SV.5289495 --preferred=878
     * 100:         p5 morellato:testnegoziando bestmapping --sku=CZ.AT8124-08H,SV.5289495 --preferred=836
     * 100:         p5 morellato:testnegoziando bestmapping --sku=HH.HWU0124,FO.FS4813 --preferred=836
     */
    private function testBestMapping()
    {
        audit_watch();
        $this->line('Testing BestMapping algorithm...');
        $sku = trim($this->option('sku'));
        if ($sku === '') {
            $this->error('SKU is mandatory for testing best-mapping');
            return;
        }

        try {
            /** @var Connection $conn */
            $conn = Core::getNegoziandoConnection();
            $conn->setConsole($this);

            $preferred = trim($this->option('preferred'));
            if ($preferred !== '') {
                $conn->addPreferredShop($preferred);
            }

            $skus = explode(',', $sku);
            $product_id = 1000;

            foreach ($skus as $sku) {
                $quantity = 1;
                if(str_contains($sku, ';')){
                    list($sku, $quantity) = explode(';', $sku);
                }
                $conn->addProduct($sku, $quantity, $product_id);
                $product_id++;
            }

            $mapping = $conn->getBestMapping();
            $this->info('Found mapping:');
            print_r($mapping);

            //$this->comment(audit_queries());

            $this->info("-> OK");
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            audit_exception($e, __METHOD__);
        }
    }

    private function reassignBestShop()
    {
        audit_watch();
        $this->line("Testing Reassign algorithm...");
        $order_id = trim($this->option('id'));
        if ($order_id == '' or $order_id == 0) {
            $this->error("Order ID is mandatory for reassign best-shop");
            return;
        }

        try {
            $conn = Core::getNegoziandoConnection();
            $conn->setConsole($this);

            $order = Order::find($order_id);

            if (is_null($order))
                throw new \Exception("Provided order is not valid");

            $shop = \OrderHelper::findBestShopByOrderDetails($order);

            if ($shop) {
                $this->info("Found best shop: $shop");
            } else {
                $this->error('No best shop has been found');
            }

            $this->comment(audit_queries());

            $this->info("-> OK");
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            audit_exception($e, __METHOD__);
        }
    }

    private function testCustom()
    {
        // SKU => QUANTITY
        audit_watch();
        $this->line("Testing Custom conditions algorithm...");
        /*$products = [
            'R0153122578' => 1,
            'SAHM03' => 1,
            'SU3046' => 1,
            'R0153122603' => 2,
        ];
        $products = [
            'SAFZ08' => 1,
            'SAFZ153' => 1,
            'SAFZ25' => 1,
            'SCZG6' => 2,
        ];*/
        $products = [
            'HH.HWU0124' => 1,
            'FO.FS4813' => 1,
        ];
        try {
            /** @var Connection $conn */
            $conn = Core::getNegoziandoConnection();
            $conn->setConsole($this);
            $conn->setDebug(true);

            foreach ($products as $sku => $quantity) {
                $product_id = (int)\Product::where('sku', $sku)->orWhere('sap_sku', $sku)->pluck('id');
                if ($product_id <= 0)
                    $product_id = 1000;
                $conn->addProduct($sku, $quantity, $product_id);
            }

            $this->comment('Testing Best-Mapping');
            $shopMapping = $conn->getBestMapping();
            print_r($shopMapping);

            $this->comment('Testing Best-Shop');
            foreach ($products as $sku => $quantity) {
                $product_id = (int)\Product::where('sku', $sku)->orWhere('sap_sku', $sku)->pluck('id');
                if ($product_id <= 0)
                    $product_id = 1000;
                $conn->addProduct($sku, $quantity, $product_id);
            }
            $bestShop = $conn->getBestShop();
            if ($bestShop) {
                $this->info("-> OK, $bestShop");
            } else {
                $this->error('Could not find a best-shop');
            }

        } catch (\Exception $e) {
            $this->error($e->getMessage());
            audit_exception($e, __METHOD__);
        }
    }

    private function testTaken()
    {
        audit_watch();
        $this->line("Testing Taken algorithm...");

        try {
            /** @var Connection $conn */
            $conn = Core::getNegoziandoConnection();
            $conn->setConsole($this);

            $results = $conn->getTakenQuantitiesFromOrders();

            print_r($results);

            $this->info("-> OK");
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            audit_exception($e, __METHOD__);
        }
    }


    private function testRandom()
    {
        audit_watch();
        $this->line("Testing Random algorithm...");

        try {
            /** @var Connection $conn */
            $conn = Core::getNegoziandoConnection();
            $conn->setConsole($this);

            $orders = Order::where('availability_mode', 'shop')->orderBy(\DB::raw('rand()'))->take(10)->get();
            foreach ($orders as $order) {
                $this->line("Order: $order->id");
                $shop = \OrderHelper::findBestShopByOrderDetails($order);
                $this->info($shop);
                if($shop){
                    \OrderHelper::assignBestShopToOrderDetails($order, $shop);
                }
            }

            $this->info("-> OK");
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            audit_exception($e, __METHOD__);
        }
    }


    private function testConcurrency(){
        // SKU => QUANTITY
        audit_watch();
        $this->line("Testing Concurrency conditions algorithm...");
        /*$products = [
            'R0153122578' => 1,
            'SAHM03' => 1,
            'SU3046' => 1,
            'R0153122603' => 2,
        ];
        $products = [
            'SAFZ08' => 1,
            'SAFZ153' => 1,
            'SAFZ25' => 1,
            'SCZG6' => 2,
        ];*/
        $products = [
            'HH.HWU0124' => 1,
            'FO.FS4813' => 1,
        ];
        /*$products = [
            'R3273687002' => 1,
        ];*/
        $iterations = 3;
        try {

            for($i = 0; $i < $iterations; $i++){
                $this->comment('Testing concurrency at iteration N-'.$i);
                /** @var Connection $conn */
                $conn = Core::getNegoziandoConnection();
                $conn->setConsole($this);
                $conn->setDebug(true);

                foreach ($products as $sku => $quantity) {
                    $product_id = (int)\Product::where('sku', $sku)->orWhere('sap_sku', $sku)->pluck('id');
                    if ($product_id <= 0)
                        $product_id = 1000;
                    $conn->addProduct($sku, $quantity, $product_id);
                }

                $this->comment('Testing Best-Mapping');
                $shopMapping = $conn->getBestMapping();
                print_r($shopMapping);

                $this->comment('Testing Best-Shop');
                foreach ($products as $sku => $quantity) {
                    $product_id = (int)\Product::where('sku', $sku)->orWhere('sap_sku', $sku)->pluck('id');
                    if ($product_id <= 0)
                        $product_id = 1000;
                    $conn->addProduct($sku, $quantity, $product_id);
                }
                $bestShop = $conn->getBestShop();
                if ($bestShop) {
                    $this->info("-> OK, $bestShop");
                    foreach ($products as $sku => $quantity) {
                        $conn->incrementTakenQuantity($sku, $bestShop, $quantity, true);
                    }
                } else {
                    $this->error('Could not find a best-shop');
                }
            }
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            audit_exception($e, __METHOD__);
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('action', InputArgument::REQUIRED, 'The action to perform'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('sku', null, InputOption::VALUE_OPTIONAL, 'The SKU to use for best shop.', null),
            array('id', null, InputOption::VALUE_OPTIONAL, 'The order id for reassign', null),
            array('preferred', null, InputOption::VALUE_OPTIONAL, 'Set preferred store', null),
            array('unused', null, InputOption::VALUE_OPTIONAL, 'Set unused store', null),
        );
    }

}