<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class BuilderGrabImage extends Command
{
    protected $grabber;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'builder:grabimages'; //--mode=all

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Grab a remove file from the Morellato repository';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->grabber = \App::make('services\Morellato\Grabbers\BuilderAssetsGrabber', [$this]);
        $mode = $this->option('mode');

        $this->info("Executing BuilderAssetsGrabber in (mode: $mode)...");

        $this->grabber->make($mode);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('mode', null, InputOption::VALUE_OPTIONAL, 'The execution mode for the grabber.', 'regular'),
        );
    }

}