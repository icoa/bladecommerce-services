<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\Csv\Downloader;
use services\Morellato\CsvImporters\AnagraficaImporter;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FtpList extends Command
{
    protected $downloader;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'csv:ftplist';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List all files in the FTP repository';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->downloader = \App::make('services\Morellato\Csv\Downloader',[$this]);
        $this->info("Executing FtpList...");
        $this->downloader->files();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}