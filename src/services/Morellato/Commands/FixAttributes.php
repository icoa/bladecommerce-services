<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\CsvImporters\AnagraficaImporter;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config, DB;
use Attribute, Trend;

class FixAttributes extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'csv:fixattributes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Fix attributes options assigning a given value to all translations';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->fixShops();
        return;
        $counter = 0;
        $this->info("Executing Fix Attributes...");
        $rows = DB::select("select * from attributes_options where id in (select attribute_option_id from attributes_options_lang where name='' or ISNULL(name))");
        foreach($rows as $row){
            $data = [
                'name' => $row->uname,
                'slug' => $row->uslug,
            ];
            DB::table('attributes_options_lang')->where('attribute_option_id',$row->id)->update($data);
            $counter++;
        }
        $this->info("Fixed [$counter] attributes so far...");
        $this->line('Aliasing attributes...');
        DB::table('attributes')->where('id',14)->update(['alias' => 'resistenza_allacqua']);
        DB::table('attributes')->where('id',217)->update(['alias' => 'funzione']);
        $this->info('Done.');
        $this->line('Fixing categories');
        DB::table('categories_lang')->where('category_id',31)->where('lang_id','en')->update(['name' => 'Charms']);
        DB::table('categories_lang')->where('category_id',31)->where('lang_id','it')->update(['name' => 'Charm']);
        $this->info('Done.');
        $this->line('Creating new standard attributes');
        if(Attribute::where('code','moments')->count() == 0){
            $data = [
                'code' => 'moments',
                'ldesc' => 'Morellato: momenti preziosi',
                'frontend_input' => 'boolean',
                'it' => [
                    'name' => 'Momenti preziosi',
                    'slug' => 'momenti-preziosi',
                ],
                'en' => [
                    'name' => 'Precious moments',
                    'slug' => 'precious-moments',
                ]
            ];
            $obj = new Attribute();
            $obj->make($data);
            $this->info('Attribute "Momenti preziosi" created.');
        }
        if(Attribute::where('code','bracelet_shortening')->count() == 0){
            $data = [
                'code' => 'bracelet_shortening',
                'ldesc' => 'Morellato: accorciamento bracciale',
                'frontend_input' => 'boolean',
                'it' => [
                    'name' => 'Accorciamento bracciale',
                    'slug' => 'accorciamento-bracciale',
                ],
                'en' => [
                    'name' => 'Bracelet shortening',
                    'slug' => 'bracelet-shortening',
                ]
            ];
            $obj = new Attribute();
            $obj->make($data);
            $this->info('Attribute "Accorciamento bracciale" created.');
        }
        if(Attribute::where('code','battery_change')->count() == 0){
            $data = [
                'code' => 'battery_change',
                'ldesc' => 'Morellato: sostituzione batteria',
                'frontend_input' => 'boolean',
                'it' => [
                    'name' => 'Sostituzione batteria',
                    'slug' => 'sostituzione-batteria',
                ],
                'en' => [
                    'name' => 'Battery change',
                    'slug' => 'battery-change',
                ]
            ];
            $obj = new Attribute();
            $obj->make($data);
            $this->info('Attribute "Sostituzione batteria" created.');
        }

        $this->line("Handle 'Momenti preziosi' trend");
        if(DB::table('trends_lang')->where('name','like','%momenti%')->count() == 0){
            $data = [
                'it' => [
                    'name' => 'Morellato - Momenti preziosi',
                    'slug' => 'morellato-momenti-preziosi',
                    'published' => 1
                ],
                'en' => [
                    'name' => 'Morellato - Precious Moments',
                    'slug' => 'morellato-precious-moments',
                    'published' => 1
                ]
            ];
            $obj = new Trend();
            $obj->make($data);
            $this->info('Trend "Morellato - Momenti preziosi" created.');
        }
        DB::table('order_states_lang')->where('order_state_id',2)->where('lang_id','it')->update(['name' => 'In attesa']);
        DB::table('order_states_lang')->where('order_state_id',2)->where('lang_id','en')->update(['name' => 'Pending']);
    }


    private function fixNegoziando(){
        $queries = [];
        $rows = DB::select('select sku from mi_anagrafica_ng where sku not in (select sku from mi_anagrafica)');
        foreach($rows as $row){
            $sku = trim($row->sku);
            $query = "DELETE FROM products WHERE sku='$sku';";
            $queries[] = $query;
        }

        $queries[] = "DELETE FROM products_lang WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM products_attributes WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM products_attributes_position WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM products_combinations WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM products_combinations WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM products_specific_prices WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM products_specific_stocks WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM categories_products WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM cache_products WHERE id NOT IN (select id from products);";
        $queries[] = "DELETE FROM products_combinations_attributes WHERE combination_id NOT IN (select id from products_combinations);";
        $queries[] = "DELETE FROM images WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM images_lang WHERE product_image_id NOT IN (select id from images);";

        //echo implode(PHP_EOL,$queries);
        \Utils::log(implode(PHP_EOL,$queries));
    }


    private function fixShops(){
        $query = 'select users.id,mi_shops.email from users inner join mi_shops on users.shop_id=mi_shops.id order by cd_neg';
        $rows = DB::select($query);
        foreach($rows as $row){
            DB::table('users')->where('id',$row->id)->update(['email' => $row->email]);
        }
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}