<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class GlsAddress extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'gls:address';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use the XML API from GLS for shipping address validation';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //$this->makeAddress();
        $this->makeOrderAddressHandler();
    }

    private function makeAddress(){
        $this->info("Executing Make Address...");
        $handler = \App::make('services\Morellato\Gls\Xml\Reader\AddressXmlReader');
        $handler->make();
    }

    private function makeOrderAddressHandler(){
        $this->info("Executing Make Order Address...");
        $handler = \App::make('services\Morellato\Gls\Handler\OrderAddressHandler',[$this]);
        $handler->make();
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}