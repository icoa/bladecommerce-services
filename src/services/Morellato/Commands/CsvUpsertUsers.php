<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CsvUpsertUsers extends Command
{
    protected $importer;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'csv:upsert_users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import all records from mi_pdv_users temporary table to database users. Options: mode ["with-inactive","without-inactive"]';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->importer = \App::make('services\Morellato\Database\UserImporter',[$this]);
        $mode = $this->option('mode');
        $this->info("Executing CsvUpsertUsers with mode: ".$mode."...");
        $this->importer->make($mode);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
          array('mode', null, InputOption::VALUE_OPTIONAL, 'If the importer must process ALL "clerk" users, even the disabled ones (with active = 0). Default with-inactive.', 'with-inactive'),
        );
    }

}
