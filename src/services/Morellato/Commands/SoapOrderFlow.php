<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\Soap\Handler\OrderFlowHandler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class SoapOrderFlow extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'soap:orderflow';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Craft an order flow request for the Morellato web service. [use --option=XXX to force a single order]';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing Soap Order Flow...");
        /** @var OrderFlowHandler $handler */
        $handler = \App::make('services\Morellato\Soap\Handler\OrderFlowHandler',[$this]);
        $order_id = $this->option('order');
        if($order_id > 0){
            $order = \Order::find($order_id);
            if(is_null($order)){
                $this->error("The given order [$order_id] is null, cannot submit single order");
            }
            $this->comment("Force sending order $order->reference");
            $handler->sendOrder($order_id);
            $handler->executePostActions();
            return;
        }
        $handler->make();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('order', null, InputOption::VALUE_OPTIONAL, 'Force to skip the automatic selection, with given order ID'),
            array('pretend', null, InputOption::VALUE_OPTIONAL, 'Perform all the checks, but do not sends any email and warnings'),
        );
    }

}