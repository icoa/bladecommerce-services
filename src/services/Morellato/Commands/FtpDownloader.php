<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\Csv\Downloader;
use services\Morellato\CsvImporters\AnagraficaImporter;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FtpDownloader extends Command
{
    protected $downloader;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'csv:ftpdownload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download all csv into a temporary folder';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        if(feats()->switchDisabled(\Core\Cfg::SERVICE_FTP_CSV_DOWNLOAD)){
            return;
        }
        $this->downloader = \App::make('services\Morellato\Csv\Downloader',[$this]);
        $this->info("Executing FtpDownloader...");
        $this->downloader->download();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}