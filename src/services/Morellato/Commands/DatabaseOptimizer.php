<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DatabaseOptimizer extends Command
{
    protected $importer;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'csv:optimize';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Optimize and re-build cache for all new imported products';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->importer = \App::make('services\Morellato\Database\Optimizer', [$this]);
        $this->info("Executing DatabaseOptimizer...");
        $relations = $this->option('relations');
        $this->importer->make($relations == 1 ? 'relations' : 'default');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('relations', null, InputOption::VALUE_OPTIONAL, 'If the optimizer must process only relations binding.', null),
        );
    }

}