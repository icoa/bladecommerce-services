<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\CsvImporters\AnagraficaImporter;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class ExecuteJobs extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'csv:exec';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Perform all artisan commands to import a complete dataset';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing main Jobs queue...");

        $stocks = $this->option('stocks');
        $rebuild = $this->option('rebuild');
        $part = $this->option('part');

        if ($stocks == 1) {
            $this->call('csv:ftpdownload');
            $this->call('csv:import');
            $this->call('csv:database', ['--stocks' => 1]);
            $this->call('csv:database', ['--relations' => 1]);
            $this->call('csv:optimize');
            $this->call('csv:ftpmove');
            $this->call('csv:log');
            return;
        }

        if ($rebuild == 1) {
            $this->call('csv:grabimages');
            $this->call('csv:optimize');
            return;
        }

        //default tasks
        if($part == 1){
            $this->call('csv:ftpdownload');
            $this->call('csv:import');
        }
        if($part == 2){
            $this->call('csv:database');
        }
        if($part == 3){
            $this->call('csv:ftpmove');
            $this->call('csv:log');
        }
        if($part == 4){
            $this->call('csv:optimize', ['--relations' => 1]); //handle other relations, such as metadata and image sets; this should be done once a day
        }
        if($part <= 0){
            //default tasks
            $this->call('csv:ftpdownload');
            $this->call('csv:import');
            $this->call('csv:database');
            $this->call('csv:ftpmove');
            $this->call('csv:log');
        }

    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
        //return array(array('task', InputArgument::REQUIRED, 'The task block to perform'),);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('stocks', null, InputOption::VALUE_OPTIONAL, 'If the importer must process only stocks.', null),
            array('rebuild', null, InputOption::VALUE_OPTIONAL, 'If the importer must run image grabber and optimization', null),
            array('part', null, InputOption::VALUE_OPTIONAL, 'Which part of the import must be executed', null),
        );
    }

}