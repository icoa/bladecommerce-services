<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\Soap\Handler\OrderHandler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class SoapOrder extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'soap:order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Craft an order for the Morellato web service; {order_id} if want to test single order';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing Soap Order...");
        /** @var OrderHandler $handler */
        $handler = \App::make('services\Morellato\Soap\Handler\OrderHandler',[$this]);

        $order = $this->argument('order');
        if($order > 0){
            $handler->forceOrder($order);
        }else{
            $handler->make();
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('order', InputArgument::OPTIONAL, 'Given order ID to force sender'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}