<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;
use Mail;
use Cfg;

class EmailTest extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'email:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'A simple test for the Email delivery';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        Config::set('mail.pretend', false);
        $this->info("Executing email test...");
        $recipient = $this->argument('recipient');
        $customer = $this->option('customer');

        if ($recipient == '' and $customer == '') {
            $this->error("You have to specify a recipient as an argument or a customer as an option!");
            return;
        }

        if ($customer > 0) {
            $customerModel = \Customer::find($customer);
            if (is_null($customerModel)) {
                $this->error("Cannot find a valid Customer with ID = $customer!");
                return;
            }
            $recipient = $customerModel->email;
        }

        if ($recipient != '') {
            if (false === \Utils::isEmail($recipient)) {
                $this->error("$recipient is not a valid email address!");
                return;
            }
        }

        $domain = Config::get('services.mailgun.domain');
        $secret = Config::get('services.mailgun.secret');
        $this->line("Sending test email to $recipient");
        $this->line("Mailgun domain: $domain");
        $this->line("Mailgun secret: $secret");

        try {
            Mail::send('emails.ping', ['date' => date('Y-m-d H:i:s')], function ($m) use ($recipient) {
                $m->from(Cfg::get('MAIL_CUSTOM1_ADDRESS', 'blade@icoa.it'), 'Blade framework');
                $m->subject('Blade framework - Test email');
                $m->to($recipient);
            });
            $this->info('MAIL SENT');
        } catch (\Exception $e) {
            $this->error('ERROR: MAIL NOT SENT');
            $this->error($e->getMessage());
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('recipient', InputArgument::OPTIONAL, 'The email recipient'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('customer', null, InputOption::VALUE_OPTIONAL, 'The Customer ID', null),
        );
    }

}