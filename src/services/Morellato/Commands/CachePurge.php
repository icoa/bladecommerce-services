<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use services\Morellato\Database\InternalCacheService;

class CachePurge extends Command
{
    /**
     * @var InternalCacheService
     */
    protected $importer;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'cache:purge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Purge internal cache for Blade catalog entities';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->importer = \App::make('services\Morellato\Database\InternalCacheService', [$this]);
        $this->info("Executing CachePurge...");
        $this->importer->make($this->argument('target'));
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(array('target', InputArgument::REQUIRED, 'The target cache level'),);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(

        );
    }

}