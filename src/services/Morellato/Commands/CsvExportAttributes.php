<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\Csv\Exporters\AttributesExporter;
use services\Morellato\CsvImporters\AnagraficaImporter;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class CsvExportAttributes extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'csv:export_attributes';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Export all attributes and options into Excel';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing CSV exporter...");
        /** @var AttributesExporter $importer */
        $importer = \App::make('services\Morellato\Csv\Exporters\AttributesExporter',[$this]);
        $importer->make_excel();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}