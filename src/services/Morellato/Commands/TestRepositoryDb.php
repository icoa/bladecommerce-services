<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\Database\Sql\Connection;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config, DB;

class TestRepositoryDb extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'morellato:repository';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test the connection to Morellato Image Repository';

    protected $valid_mimes = ['image/jpeg', 'image/jpg', 'image/png'];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing TestRepositoryDb...");
        $this->testRepository();
        //$this->getWrongImages();
        //$this->fixOrderWithSapSku();
    }


    private function testRepository()
    {

        $sku = $this->argument('sku');

        $vars = DB::connection('repository')
            ->table('media')
            ->where('code', $sku)
            ->whereIn('CONTENT_TYPE', $this->valid_mimes)
            ->select(DB::raw('distinct(VAR)'))
            ->orderBy('VAR')
            ->lists('VAR');

        $many = count($vars);
        $this->comment("Found [$many] vars for product [$sku]");

        if (!empty($vars)) {
            foreach ($vars as $var) {

                $media = DB::connection('repository')
                    ->table('media')
                    ->where('code', $sku)
                    ->where('var', $var)
                    ->whereIn('CONTENT_TYPE', $this->valid_mimes)
                    ->select('FILENAME', 'SIZE', 'UPLOADED', 'CONTENT_TYPE', 'ENCODED_FILENAME', 'VAR', 'VERSION')
                    ->orderBy('VERSION', 'DESC')
                    ->first();

                $this->table(
                    ['FILENAME', 'SIZE', 'CONTENT_TYPE', 'ENCODED_FILENAME', 'VAR', 'VERSION', 'UPLOADED'],
                    [[$media->FILENAME, $media->SIZE, $media->CONTENT_TYPE, $media->ENCODED_FILENAME, $media->VAR, $media->VERSION, $media->UPLOADED]]
                );

                $repUri = "http://thumbnail.repository.morellato.com/download.php?code=$sku&size=raw&type=jpg&report=ER&var=$media->VAR";

                $this->line($repUri);

            }
        }
    }


    private function getWrongImages()
    {
        $query = 'select myTable.* from (select id,product_id,filename * 1 as file from images) as myTable where id!=file';
        $rows = \DB::select($query);
        foreach ($rows as $row) {
            $images = \ProductImage::where('filename', 'like', "%{$row->file}.%")->get();
            if (count($images) > 1) {
                $this->error("Product id [$row->product_id] has image clones");
                foreach ($images as $image) {
                    if ((int)$image->var <= 0) {
                        $this->comment("Image with ID [$image->id] is wrong");
                        \DB::table('images_wrong')->insert(['product_id' => $row->product_id, 'image_id' => $image->id]);
                    }
                }
            }
        }
    }


    private function fixOrderWithSapSku()
    {
        $rows = \DB::select("select * from order_details where created_by > 0 and isnull(product_sap_reference) and isnull(deleted_at) order by id desc limit 2000");
        \DB::beginTransaction();
        $many = count($rows);
        $this->line("Found $many items...");
        $counter = 0;
        foreach ($rows as $row) {
            $counter++;
            $this->comment("Processing row [$row->id] ($counter/$many)");
            $sap_sku = null;
            if ($row->product_combination_id > 0) {
                $product = \ProductCombination::find($row->product_combination_id);
                if($product)
                    $sap_sku = $product->sap_sku;
            } else {
                $product = \Product::find($row->product_id);
                if($product)
                    $sap_sku = $product->sap_sku;
            }
            if ($sap_sku != null) {
                $this->info("Found SAP_SKU: $sap_sku");
                \DB::table('order_details')->where('id', $row->id)->update(['product_sap_reference' => $sap_sku]);
            }

        }
        \DB::commit();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(array('sku', InputArgument::REQUIRED, 'The SKU to check'),);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
        return array(array('sku', null, InputOption::VALUE_REQUIRED, 'The SKU to check.', null),);
    }

}