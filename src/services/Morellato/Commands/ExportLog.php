<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\CsvImporters\AnagraficaImporter;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class ExportLog extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'csv:log';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a file log with all the errors from the parser and upload it to FTP';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing Export Log...");
        $importer = \App::make('services\Morellato\Loggers\FileExporter',[$this]);
        $importer->make();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}