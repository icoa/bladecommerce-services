<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;
use Mail;
use Cfg;

class BladeEmailTest extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'email:blade.test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'A simple test for the Email delivery(internal)';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        Config::set('mail.pretend', false);
        $this->info("Executing blade email test...");
        $code = $this->argument('code');
        $recipient = $this->argument('recipient');
        $customer = $this->option('customer');

        $email = \Email::getByCode($code);
        if (is_null($email)) {
            $this->error("Cannot find a valid internal email model with code = $code!");
            return;
        }

        if ($recipient == '' and $customer == '') {
            $this->error("You have to specify a recipient as an argument or a customer as an option!");
            return;
        }

        $customerModel = null;
        if ($customer > 0) {
            $customerModel = \Customer::find($customer);
            if (is_null($customerModel)) {
                $this->error("Cannot find a valid Customer with ID = $customer!");
                return;
            }
            $recipient = $customerModel->email;
        }

        if ($recipient != '') {
            if (false === \Utils::isEmail($recipient)) {
                $this->error("$recipient is not a valid email address!");
                return;
            }
        }

        $domain = Config::get('services.mailgun.domain');
        $secret = Config::get('services.mailgun.secret');
        $this->line("Sending test email to $recipient");
        $this->line("Mailgun domain: $domain");
        $this->line("Mailgun secret: $secret");


        try {
            $email->setCustomer($customerModel)->send([], $recipient);
            $this->info('MAIL SENT');
        } catch (\Exception $e) {
            $this->error('ERROR: MAIL NOT SENT');
            $this->error($e->getMessage());
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('code', InputArgument::REQUIRED, 'The internal mail code'),
            array('recipient', InputArgument::OPTIONAL, 'The email recipient'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('customer', null, InputOption::VALUE_OPTIONAL, 'The Customer ID', null),
        );
    }

}