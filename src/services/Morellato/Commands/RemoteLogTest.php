<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Exception;

class RemoteLogTest extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'log:test_remote';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'A simple test for the Remote logging';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        if (config('logging.remote.enabled')) {
            $this->line("Performing remote logging test");
            try {
                $c = 5 / 0;
            } catch (Exception $e) {
                audit_remote($e->getMessage(), 'RemoteLogTest::command', $e);
                $this->info("Test completed");
            }
        } else {
            $this->error("On this environment the Remote logging is actually disabled");
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}