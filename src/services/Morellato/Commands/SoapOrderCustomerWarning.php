<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\Soap\Handler\OrderCustomerWarningHandler;
use services\Morellato\Soap\Handler\OrderWarningHandler;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class SoapOrderCustomerWarning extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'soap:customerWarning';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Select order and determine if warnings must be sent to Customers or Shop {handler, the name of the warning handler to run - "help" accepted} [--order=X, to skip and force to one Order] [--pretend=1, run the job in simulation mode]';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing Soap Order Warning...");
        $handlers = $this->argument('handler');
        if($handlers[0] == 'help')
            return $this->showHelp();

        $callables = OrderWarningHandler::options();
        if($handlers[0] == 'all')
            $handlers = array_keys($callables);

        $switches = OrderWarningHandler::switchForOptions();

        $this->line("Selected handlers in input:");
        print_r($handlers);

        $isPretend = $this->option('pretend') == '1';
        if($isPretend)
            $this->comment("Running with pretend mode");

        foreach($callables as $key => $callable){
            if(in_array($key, $handlers, true)){

                if(feats()->switchEnabled($switches[$key])){
                    $this->info("Executing handler: $key");

                    /** @var OrderWarningHandler $handler */
                    $handler = \App::make($callable, [$this]);
                    $handler->setPretend($isPretend);
                    $order_id = $this->option('order');
                    if($order_id > 0){
                        $this->comment("Forcing to order $order_id");
                        $handler->single($order_id);
                    }else{
                        $this->comment("Auto-selecting orders...");
                        $handler->make();
                    }
                }

            }
        }
    }


    private function showHelp(){
        $callables = OrderWarningHandler::options();
        $this->info("Available handlers:");
        foreach($callables as $key => $callable){
            $description = $callable::getDescription();
            $this->comment("$key => $description");
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('handler', InputArgument::OPTIONAL | InputArgument::IS_ARRAY, 'Which type(s) of handlers to trigger', array(OrderWarningHandler::TYPE_MISSED_PICKUP)),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('order', null, InputOption::VALUE_OPTIONAL, 'Force to skip the automatic selection, with given order ID'),
            array('pretend', null, InputOption::VALUE_OPTIONAL, 'Perform all the checks, but do not sends any email and warnings'),
        );
    }

}