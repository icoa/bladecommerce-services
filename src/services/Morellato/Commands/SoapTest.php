<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class SoapTest extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'soap:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'A simple test for the SoapWrapper class';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        /*$rows = \Customer::where('email','like','%f.politi%')->get();
        foreach($rows as $row){
            \Customer::where('id',$row->id)->update(['passwd' => \Hash::make('testtest')]);
        }
        return;*/
        $this->info("Executing Soap Tester...");
        $handler = \App::make('services\Morellato\Soap\Handler\TestHandler',[$this]);
        $handler->make();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}