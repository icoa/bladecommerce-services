<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\Csv\Downloader;
use services\Morellato\CsvImporters\AnagraficaImporter;
use services\Morellato\Database\JsonImporter;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DatabaseImporter extends Command
{
    /**
     * @var JsonImporter
     */
    protected $importer;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'csv:database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import all records from temporary tables to database entities';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        if(\App::environment() === 'local'){
            audit_watch();
        }
        $this->importer = \App::make('services\Morellato\Database\JsonImporter',[$this]);
        $this->info("Executing DatabaseImporter...");
        $stocks = $this->option('stocks');
        $sapsku = $this->option('sapsku');
        $sku = (string)$this->option('sku');
        $relations = $this->option('relations');
        if($relations == 1){
            $this->importer->bindRelations();
            return;
        }
        if($sapsku == 1){
            $this->importer->bindSapSkus();
        }else{
            if(strlen($sku) > 0){
                $this->importer->setLimitSkus(explode(',', $sku));
            }
            $this->importer->make( $stocks == 1 );
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('stocks', null, InputOption::VALUE_OPTIONAL, 'If the importer must process only stocks.', null),
            array('sapsku', null, InputOption::VALUE_OPTIONAL, 'If the importer must process only sapsku binding.', null),
            array('relations', null, InputOption::VALUE_OPTIONAL, 'If the importer must process only relations binding.', null),
            array('sku', null, InputOption::VALUE_OPTIONAL, 'SKU for the single mode.', null),
        );
    }

}