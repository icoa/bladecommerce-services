<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use services\Morellato\Database\Sql\Connection;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config, DB, Core;

class NgQuery extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'morellato:ngquery';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Performs a given query on the Negoziando DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $conn = Core::getNegoziandoConnection();
        $start = microtime(true);
        $conn->setConsole($this);
        $this->info("Executing NgQuery...");
        $this->line("Test init at $start ms");
        $sku = $this->argument('sku');
        $isShop = $this->option('shop');
        $omit = $this->option('omit') == 1;

        if($isShop){
            $this->line("Getting SKUs by SHOP");
        }
        $skus = explode(',',$sku);
        foreach($skus as $sku){
            $sku = strtoupper($sku);
            if($isShop){
                $this->line("Querying for shop [$sku]");
                $data = $conn->getProductsByShop($sku);
                if($omit == false)
                    print_r($data);
            }else{
                $this->line("Querying for sku [$sku]");
                $data = $conn->getAvailabilityBySku($sku);
                if($omit == false)
                    print_r($data);
            }
        }
        $end = microtime(true);
        $time = abs($end - $start);
        $this->line("Test performed in $time ms");
    }




    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(array('sku', InputArgument::REQUIRED, 'The sku or a list of skus'),);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('shop', null, InputOption::VALUE_OPTIONAL, 'Determine if passing SKUs or a list of SHOPs', null),
            array('omit', null, InputOption::VALUE_OPTIONAL, 'Determine if the results must not printed out', null),
        );
    }

}