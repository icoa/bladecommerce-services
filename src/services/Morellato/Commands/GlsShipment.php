<?php

namespace services\Morellato\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use services\Morellato\Gls\Handler\OrderHandler;

class GlsShipment extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'gls:shipment';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use the XML API from GLS for shipment info. {--order=X for forcing single Order}';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->makeOrderHandler();
    }

    private function makeOrderHandler()
    {
        $this->info("Executing Make Order Handler...");
        $handler = \App::make(OrderHandler::class);
        $handler->setConsole($this);
        $order = $this->option('order');
        $audit = $this->option('audit');
        if(true == $audit){
            $this->comment("Query watcher enabled");
            audit_watch();
        }
        if($order and $order > 0){
            $this->comment("Forcing to order $order");
            $handler->trackOrder($order);
            return;
        }
        $handler->make();
        if(true == $audit){
            print_r(audit_queries());
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('order', null, InputOption::VALUE_OPTIONAL, 'Force to skip the automatic selection, with given order ID'),
            array('pretend', null, InputOption::VALUE_OPTIONAL, 'Perform all the checks, but do not sends any email and warnings'),
            array('audit', false, InputOption::VALUE_OPTIONAL, 'Perform query auditing'),
        );
    }

}