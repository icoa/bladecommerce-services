<?php

namespace services\Morellato\Commands;

use App;
use Illuminate\Console\Command;
use services\Morellato\Repositories\TranslateRepository;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use services\Morellato\Repositories\OrdersTestRepository;

class OrdersTestExec extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'orders:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute a task from the Orders test repository';

    /**
     * @var OrdersTestRepository
     */
    protected $repository;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        /** @var OrdersTestRepository repository */
        $this->repository = App::make('services\Morellato\Repositories\OrdersTestRepository');
        $this->repository->setConsole($this);
        $this->line("Executing OrdersTestRepository...");
        $this->repository->exec($this->argument('task'));
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('task', InputArgument::REQUIRED, 'The task to perform'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}