<?php

namespace services\Morellato\Grabbers;

use DB, Utils, Config, Exception, Cache;
use Illuminate\Cache\CacheManager;
use Illuminate\Cache\Repository;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;
use GuzzleHttp\Client;
use Carbon\Carbon;
use services\Traits\TraitLocalDebug;

class GoogleTranslateGrabber
{
    use TraitLocalDebug;

    protected $localDebug = false;

    /**
     * @var Command
     */
    protected $console;

    /**
     * @var Filesystem
     */
    protected $files;

    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Repository
     */
    protected $cache;

    protected $items = [];

    protected $results;
    protected $lang;
    protected $limit = 90;
    protected $format = 'text';

    protected $repository = 'https://translation.googleapis.com/language/translate/v2?key=:key';

    function __construct(Filesystem $files, Client $client)
    {
        $this->files = $files;
        $this->client = $client;
        $this->repository = str_replace(':key', config('morellato.google.translate'), $this->repository);
        $this->cache = \Cache::driver('file');
        $this->setup();
    }

    protected function setup()
    {

    }

    /**
     * @param string $item
     * @return $this
     */
    function addItem($item)
    {
        $this->items[] = $this->wrapSpecialTags($item);
        return $this;
    }

    /**
     * @param array $items
     * @return $this
     */
    function addItems($items)
    {
        $this->items[] = array_merge($items, $this->items);
        return $this;
    }

    /**
     * @return $this
     */
    function reset()
    {
        $this->format = 'text';
        $this->items = [];
        $this->results = null;
        return $this;
    }

    /**
     * @return int
     */
    function size()
    {
        return count($this->items);
    }

    /**
     * @return string
     */
    private function getPayload($start = 0)
    {
        $items = array_slice($this->items, $start, $this->limit);
        $payload = '{';
        foreach ($items as $item) {
            $item = addslashes($item);
            $payload .= "'q': \"$item\"," . PHP_EOL;
        }
        $payload .= "'target': '{$this->lang}'}";
        //audit($payload);
        return $payload;
    }

    /**
     * @param $lang
     * @return array
     */
    function translate($lang)
    {
        $this->lang = $lang;
        $this->fetchResource();
        return $this->getResults();
    }

    /**
     * @param $lang
     * @return array
     */
    function translateHtml($lang)
    {
        $this->format = 'html';
        return $this->translate($lang);
    }

    /**
     * @throws Exception
     */
    protected function fetchResource()
    {

        $size = $this->size();
        $actions = ceil($size / $this->limit);
        //audit("Performing $actions actions...");
        $this->results = [];

        for ($i = 0; $i < $actions; $i++) {
            $payload = $this->getPayload($i * ($this->limit));
            $res = $this->client->post($this->repository, ['body' => $payload, 'headers' => array('Content-Type' => 'application/json')]);

            $results = null;
            $statusCode = $res->getStatusCode();
            if ($statusCode == 200) {
                $results = $res->json();
                //audit($results, 'partial results');
                if (empty($this->results)) {
                    $this->results = $results;
                } else {
                    $translations = $results['data']['translations'];
                    $this->results['data']['translations'] = array_merge($this->results['data']['translations'], $translations);
                }
            } else {
                throw new \Exception("Translate service has responded with a status code of [$statusCode]");
            }
            usleep(100);
        }

        //audit($this->results);
    }

    /**
     * @return array
     * @throws Exception
     */
    private function getResults()
    {
        if (is_null($this->results) or !is_array($this->results) or empty($this->results)) {
            throw new Exception("Cannot parse results; body is empty or invalid");
        }
        $translations = $this->results['data']['translations'];
        $strings = [];
        foreach ($translations as $translation) {
            $strings[] = $this->unwrapSpecialTags($translation['translatedText']);
        }
        return $strings;
    }


    public function wrapSpecialTags($content)
    {
        $replace = array();
        $matches = array();
        $searches = array();
        if (preg_match_all("#\[(.*)\]#isU", $content, $matches)) {
            foreach ($matches[0] as $token) {
                if (!in_array($token, $searches)) {
                    $searches[] = $token;
                    $replace[] = "<span class=\"notranslate\">{$token}</span>";
                }
            }
            $content = str_replace($searches, $replace, $content);
        }
        return $content;
    }


    public function unwrapSpecialTags($content)
    {
        $matches = array();
        $content = str_replace(
            ['&quot;', '&#39;', '&gt;', '&lt;'],
            ['"', '’', '>', '<']
            , $content);
        if (preg_match_all("#<span class=\"notranslate\">(.*)<\/span>#isU", $content, $matches)) {
            $content = str_replace($matches[0], $matches[1], $content);
        }
        return $content;
    }


}