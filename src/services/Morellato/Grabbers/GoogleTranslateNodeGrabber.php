<?php

namespace services\Morellato\Grabbers;

use DB, Utils, Config, Exception, Cache;
use Illuminate\Cache\CacheManager;
use Illuminate\Cache\Repository;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;
use GuzzleHttp\Client;
use Carbon\Carbon;
use Illuminate\Support\Arr;

class GoogleTranslateNodeGrabber extends GoogleTranslateGrabber
{

    protected $nodePath;

    protected function setup()
    {
        $this->nodePath = config('morellato.translate_node_path');
    }

    /**
     * @throws Exception
     */
    protected function fetchResource()
    {
        if($this->format == 'text'){
            parent::fetchResource();
            return;
        }

        $size = $this->size();
        $actions = ceil($size / $this->limit);
        $this->audit("Performing $actions actions...");
        $this->results = [];

        for ($i = 0; $i < $actions; $i++) {
            $text = $this->items[$i];
            $cmd = $this->getNodeCommand($text, $this->format);
            $this->audit($cmd, 'Node command');
            $output = shell_exec($cmd);
            $this->audit($output, 'Node output');
            $payload = json_decode($output);
            $this->audit($payload, 'Node Payload Results');
            Arr::set($this->results, 'data.translations.0.translatedText', $payload->text);
        }

        $this->audit($this->results);
    }


    protected function getNodeCommand($text, $format = 'text')
    {
        $text = addslashes($text);
        $jsFile = $format == 'text' ? 'translate_text.js' : 'translate.js';
        $cmd = 'node ' . $this->nodePath . $jsFile . " from it to {$this->lang} text \"$text\"";
        return $cmd;
    }
}