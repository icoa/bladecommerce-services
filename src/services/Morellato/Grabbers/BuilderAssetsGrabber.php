<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 02/02/2017
 * Time: 10:23
 */

namespace services\Morellato\Grabbers;

use DB, Utils, Config, Exception, File;
use Illuminate\Support\Facades\Schema;
use MaxMind\Db\Reader\Util;
use Product, ProductImage;
use Illuminate\Console\Command;
use services\Morellato\Loggers\DatabaseLogger as Logger;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Image;
use Carbon\Carbon;

class BuilderAssetsGrabber extends ImageGrabber
{


    protected $valid_mimes = ['image/jpeg', 'image/jpg', 'image/png'];

    protected $root;

    protected $var = 8; //asset for builder are only stores in 8 position

    protected $errors = [];

    protected $mtime = null;

    function make($mode = 'regular')
    {
        $this->root = public_path('assets/products/builder/');
        if (!File::isDirectory($this->root)) {
            File::makeDirectory($this->root, 775);
        }
        $this->scanForProducts($mode);
    }

    function scanForProducts($mode)
    {
        audit_watch();
        $rows = Product::withBuilders()
            //->take(10)
            ->get();
        $many = count($rows);
        $this->console->line("Scanning $many products");

        foreach ($rows as $row) {
            $this->fileType = 'png';
            $this->sku = $row->sku;
            try {
                $this->console->line("Fething assets for $this->sku");
                $this->getResourceHeader();
                $this->console->info("HEAD is ok");
                $this->imageName = $this->sku . '.png';
                if(!$this->isSkippable()){
                    $this->download();
                }
            } catch (Exception $e) {
                $this->console->error($e->getMessage());
                $this->errors[] = $row->sku;
            }
        }

        print_r($this->errors);
        audit($this->errors, 'SKU WITH ERRORS');
    }


    function getResourceHeader()
    {
        $repo = $this->makeRepository($this->sku, $this->fileType, $this->var);
        audit($repo, 'repo');
        $res = $this->client->head($repo);

        $mime = null;
        $statusCode = $res->getStatusCode();
        audit($statusCode, 'statusCode');

        if ($statusCode == 200) {
            $contentType = $res->getHeader('Content-Type');
            audit($contentType, 'contentType');
            if ($contentType) {
                $mime = trim(explode(';', $contentType)[0]);
            }

            audit($mime, 'mime');
            if ($mime == '') {
                throw new \Exception("Image downloader for sku [$this->sku] has responded with an empty image");
            }

            $date = $res->getHeader('date');
            $this->mtime = strtotime(str_replace(' GMT', '', $date));
            audit($this->mtime, 'mtime');
            audit($date, 'date');
        } else {
            throw new \Exception("Image downloader for sku [$this->sku] has responded with a status code of [$statusCode]");
        }
    }

    function isSkippable()
    {
        $filename = $this->imageName;
        $file = $this->root . $filename;
        if (File::exists($file)) {
            return true;
            $filemtime = filemtime($file);
            audit($filemtime . ' ' . date('Y-m-d H:i:s', $filemtime), "lastModified $file");
            audit($this->mtime . ' ' . date('Y-m-d H:i:s', $this->mtime), "remote date $file");
            $diff = abs($filemtime - $this->mtime);
            audit($diff, 'diff');
            if ($diff >= 100) {
                $this->console->line("File $file skipped");
                return true;
            }
        }
        return false;
    }

    function saveResource()
    {
        $filename = $this->imageName;
        $file = $this->root . $filename;

        $this->files->put($file, $this->resourceBody);
        audit("Touching file $file with $this->mtime");
        touch($file, $this->mtime, $this->mtime);

        audit("Touched file time: " . filemtime($file));
        $this->console->info("File $file saved");
        return $filename;
    }


}