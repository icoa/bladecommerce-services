<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 02/02/2017
 * Time: 10:23
 */

namespace services\Morellato\Grabbers;

use DB, Utils, Config, Exception;
use Illuminate\Support\Facades\Schema;
use MaxMind\Db\Reader\Util;
use Product, ProductImage;
use Illuminate\Console\Command;
use services\Morellato\Loggers\FileLogger as Logger;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Image;
use Carbon\Carbon;
use services\src\services\Traits\TraitImageWhiteStrip;
use Mail;

class ImageGrabber
{

    use TraitImageWhiteStrip;

    protected $console;
    protected $logger;
    protected $files;
    protected $client;
    protected $sku;
    protected $input_sku;
    protected $email;

    protected $resourceBody;
    protected $resourceMime;

    protected $product_image_id;
    protected $tmpStoragePath;
    protected $publicStoragePath;
    protected $product_id;


    protected $imageCount = 0;
    protected $imagePosition = 0;
    protected $totalImages = 0;
    protected $imageName;
    protected $fileSize;
    protected $fileType;
    protected $fileMD5;
    protected $var;
    protected $version;
    protected $imageLoop = 0;
    protected $isImageCoverSet = 0;
    protected $import_mimes = array();
    protected $check_cover_already_set = 0;
    protected $size = '2000';

    //protected $valid_mimes = ['image/jpeg', 'image/jpg', 'image/png'];
    protected $valid_mimes = ['image/jpeg', 'image/jpg'];
    protected $product;
    protected $media;
    protected $dbImage;
    protected $coverExists = false;
    protected $removeWhite = true;


    protected $repository = 'http://thumbnail.repository.morellato.com/download.php?code=:sku&size=1500&type=jpg&report=ER&var=1';
    //protected $repositoryNew = 'http://thumbnail.repository.morellato.com/download.php?code=:sku&size=1500&type=:type&report=ER&var=:var';
    protected $repositoryNew = 'http://thumbnail.repository.morellato.com/download.php?code=:sku&size=:size&type=:type&report=ER&var=:var';


    protected $debug = true;
    protected $notFounds = [];
    protected $messages = [];

    function __construct(Command $console, Logger $logger, Filesystem $files, Client $client)
    {
        $this->console = $console;
        $this->logger = $logger;
        $this->files = $files;
        $this->client = $client;
        $this->product_image_id = DB::selectOne("SHOW TABLE STATUS WHERE name='images'")->Auto_increment;
        $this->tmpStoragePath = Config::get('ftp.imageTmpStorage') . '/';
        $this->publicStoragePath = public_path('assets/products/');
        $this->import_mimes = Config::get('ftp.import_mimes');
        $this->size = Config::get('ftp.repository_size', 'raw');

        if (!$this->files->isDirectory($this->tmpStoragePath)) {
            $this->files->makeDirectory($this->tmpStoragePath, 775, true);
        }
    }

    /**
     * @return Command
     */
    function getConsole()
    {
        return $this->console;
    }

    /**
     * @return mixed
     */
    public function getInputSku()
    {
        return $this->input_sku;
    }

    /**
     * @param mixed $input_sku
     */
    public function setInputSku($input_sku)
    {
        $this->input_sku = $input_sku;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    function single($sku)
    {
        $this->setInputSku($sku);
        $this->make('single');
        return $this->messages;
    }

    function isConsole()
    {
        return \App::runningInConsole();
    }

    function make($mode = 'regular')
    {
        if(feats()->switchDisabled(\Core\Cfg::SERVICE_IMAGE_REPOSITORY)){
            return;
        }
        try {
            $this->scanForProducts($mode);
            if ($mode != 'single') {
                $this->console->call('tools:exec', ['task' => 'rebind_cover_images']);
                $this->console->call('tools:exec', ['task' => 'rebind_cover_products']);
            }
            if ($this->email)
                $this->sendReport();
        } catch (Exception $e) {
            audit_exception($e, __METHOD__);
            throw new $e;
        }
    }

    function info($msg)
    {
        $this->messages[] = 'INFO => ' . $msg;
        if ($this->isConsole()) {
            $this->getConsole()->info($msg);
        }
    }

    function line($msg)
    {
        $this->messages[] = $msg;
        if ($this->isConsole()) {
            $this->getConsole()->line($msg);
        }
    }

    function comment($msg)
    {
        $this->messages[] = $msg;
        if ($this->isConsole()) {
            $this->getConsole()->comment($msg);
        }
    }

    function error($msg)
    {
        $this->messages[] = '!ERROR! => ' . $msg;
        if ($this->isConsole()) {
            $this->getConsole()->error($msg);
        }
    }

    function table($headers, $rows)
    {
        $text = '';
        foreach ($headers as $index => $header) {
            $text .= "$header | ";
        }
        $text = rtrim($text, ' | ');
        $text .= PHP_EOL;
        foreach ($rows as $lines) {
            foreach ($lines as $line) {
                $text .= "$line | ";
            }
            $text = rtrim($text, ' | ');
            $text .= PHP_EOL;
        }
        $this->messages[] = $text;

        if ($this->isConsole()) {
            $this->getConsole()->table($headers, $rows);
        }
    }

    /**
     * Fetch all products and related images and stored it in cache folder
     * @param $mode
     */
    function scanForProducts($mode)
    {
        $pivot_date = null;

        try {
            $connectionTest = DB::connection('repository')
                ->table('media')
                ->max('UPLOADED');

            $this->info("Connection test to repository DB successfully passed!");

        } catch (Exception $e) {
            $this->error("Could not connect to repository DB!");
            audit_exception($e);
            audit_remote($e->getMessage(), __METHOD__);
            return;
        }

        if ($mode == 'regular') {
            $pivot_date = Carbon::yesterday()->format('Y-m-d');

            $repositorySkus = DB::connection('repository')
                ->table('media')
                ->where('UPLOADED', '>=', $pivot_date)
                ->select(DB::raw('distinct(CODE)'))
                ->orderBy('CODE')
                ->lists('CODE');

            $products = Product::where('source', 'morellato')
                ->where(function ($query) use ($repositorySkus) {
                    $query->whereIn('sku', $repositorySkus)->orWhereIn('sap_sku', $repositorySkus);
                })
                ->select('id', 'sku', 'sap_sku')
                ->orderBy('id', 'desc')
                ->get();
        } elseif ($mode == 'complete') {
            $products = Product::select('id', 'sku', 'sap_sku')
                ->orderBy('id', 'desc')
                ->get();
        } elseif ($mode == 'update') {
            $products = Product::select('id', 'sku', 'sap_sku')
                ->whereIn('sku', function ($query) {
                    $query->select('sku')->from('import_excel_watches');
                })->orWhereIn('sku', function ($query) {
                    $query->select('sku')->from('import_excel_jewels');
                })
                ->orderBy('id', 'desc')
                ->get();
        } elseif ($mode == 'single' and $this->input_sku) {
            $products = Product::select('id', 'sku', 'sap_sku')
                ->where('sku', $this->input_sku)
                ->orderBy('id', 'desc')
                ->get();
        } else {
            $products = Product::whereNotIn('id', function ($query) {
                $query->from('images')->select('product_id');
            })->select('id', 'sku', 'sap_sku')
                ->orderBy('id', 'desc')
                ->get();
        }

        $many_products = count($products);
        $this->comment("Found [$many_products] products to process");

        $counter = 0;
        foreach ($products as $product) {

            DB::beginTransaction();
            $counter++;
            $this->product = $product;
            $sku = $this->getMorellatoSku($product);

            $vars = $this->getVarsBySku($sku, ($mode == 'regular' and $pivot_date) ? $pivot_date : null);

            $many = count($vars);
            $this->comment("Found [$many] vars for product [$sku] ($counter/$many_products)");

            if (!empty($vars)) {

                $this->imagePosition = (int)DB::table('images')->where('product_id', $product->id)->max('position');
                $this->coverExists = DB::table('images')->where('product_id', $product->id)->where('cover', 1)->count() > 0;
                $this->info("Product last image position [$this->imagePosition]");

                foreach ($vars as $var) {

                    $fetchImage = false;
                    $imageFilename = null;

                    $media = DB::connection('repository')
                        ->table('media')
                        ->where('code', $sku)
                        ->where('var', $var)
                        ->whereIn('CONTENT_TYPE', $this->valid_mimes)
                        ->select('FILENAME', 'SIZE', 'UPLOADED', 'CONTENT_TYPE', 'ENCODED_FILENAME', 'VAR', 'VERSION')
                        ->orderBy('VERSION', 'DESC')
                        ->first();

                    $this->media = $media;

                    $this->fileType = $this->getExtension($media->CONTENT_TYPE);
                    $mediaFilename = $media->FILENAME;

                    $this->table(
                        ['FILENAME', 'SIZE', 'CONTENT_TYPE', 'ENCODED_FILENAME', 'VAR', 'VERSION'],
                        [[$media->FILENAME, $media->SIZE, $media->CONTENT_TYPE, $media->ENCODED_FILENAME, $media->VAR, $media->VERSION]]
                    );

                    //get existing image by id,var
                    $dbImage = ProductImage::where('product_id', $product->id)->where('var', $var)->whereNull('created_by')->first();
                    $this->dbImage = $dbImage;
                    if ($dbImage) {
                        $imageFilename = $dbImage->filename;
                        //check saved extension
                        $tokens = explode('.', $imageFilename);
                        $imageFilename = $tokens[0] . '.' . $this->fileType;
                        //check if we have same version
                        if ($dbImage->version != $media->VERSION) {
                            $fetchImage = true;
                        }
                        //check the MD5
                        if ($dbImage->md5 != $media->ENCODED_FILENAME) {
                            $fetchImage = true;
                        }
                    } else {
                        $fetchImage = true;
                        $imageFilename = $this->product_image_id . '.' . $this->fileType;
                    }

                    if ($fetchImage) {
                        $this->comment("Image [$mediaFilename] should be fetched and saved as [$imageFilename]");
                        $this->fetchImageFromRepository($imageFilename);
                    } else {
                        $this->comment("Image [$mediaFilename] is already present as [$imageFilename]");
                    }

                }
            } else {
                $msg = "No images found for [$sku] in the repository";
                $this->error($msg);
                $this->logger->warning($msg, 'image_grabber');
                $this->notFounds[] = $sku;
            }

            $this->updateProductImageCounter();
            DB::commit();

        } //End Products loop


    }


    function getMorellatoSku($product)
    {
        return strlen($product->sap_sku) > 0 ? $product->sap_sku : $product->sku;
    }


    function fetchImageFromRepository($targetFilename)
    {
        $sku = $this->getMorellatoSku($this->product);
        $product_id = $this->product->id;
        $this->imageName = $this->media->FILENAME;
        $this->var = $this->media->VAR;
        $this->setSku($sku);
        $importFilepath = null;


        try {
            $this->comment("Fetching remote resource $this->imageName");
            $this->fetchResource();
            $resourceFilename = $this->saveResource();
            $savedFilename = $this->tmpStoragePath . $resourceFilename;
            $this->info("New resource saved as [$savedFilename]");


            $data = [
                'filename' => $targetFilename,
                'cover' => $this->coverExists ? 0 : 1,
                'position' => $this->imagePosition + 1,
                'product_id' => $product_id,
                'md5' => $this->media->ENCODED_FILENAME,
                'version' => $this->media->VERSION,
                'var' => $this->media->VAR,
            ];

            if (is_null($this->dbImage)) { //the database image (ProductImage) does not exist

                $languages = \Core::getLanguages();
                foreach ($languages as $language) {
                    $data[$language] = ['published' => 1, 'legend' => null];
                }

                $this->comment("Saving new product image into the database");

                $model = new ProductImage();
                $model->make($data);

                if ($data['cover'] == 1) {
                    $this->coverExists = true;
                    if ($model->id > 0)
                        DB::table('products')->where('id', $product_id)->update(['default_img' => $model->id]);
                }

                $this->logger->log("Image file for [$sku] has been successfully imported", 'image_grabber');

                //update the counters
                $this->imagePosition++;
                $this->product_image_id++;

                //now move the saved resource to the locale images folder
                if ($model->id > 0) {
                    $importFilepath = $this->publicStoragePath . $model->id . '.' . $this->fileType;
                    $this->files->copy($savedFilename, $importFilepath);
                    $this->info("Temp file [$savedFilename] moved to $importFilepath");
                    DB::table('images')->where('id', $model->id)->update(['created_by' => null]);
                }

            } else {
                //now move the saved resource to the locale images folder
                $importFilepath = $this->publicStoragePath . $targetFilename;
                $this->files->copy($savedFilename, $importFilepath);
                $this->info("Temp file [$savedFilename] moved to $importFilepath");
                //simply update the reference
                $this->comment("Updating existing product image");
                //unset($data['filename']);
                unset($data['cover']);
                unset($data['position']);
                $this->dbImage->fill($data);
                $this->dbImage->save();
                $this->logger->log("Image file for [$sku] has been successfully updated", 'image_grabber');
                DB::table('images')->where('id', $this->dbImage->id)->update(['created_by' => null]);
            }

            //remove white background
            if ($this->removeWhite and $importFilepath) {
                $filename = str_replace($this->publicStoragePath, '', $importFilepath);
                $this->removeWhiteBackground($product_id, $filename);
            }


        } catch (\Exception $e) {
            $this->error("Remote resource cannot be fetched correctly");
            $msg = "Remote resource cannot be fetched correctly for product [$this->imageName]: " . $e->getMessage();
            $this->error($msg);
            $this->logger->error($msg, 'image_grabber');
        }
    }


    function updateProductImageCounter()
    {
        $count = DB::table('images')->where('product_id', $this->product->id)->count();
        DB::table('products')->where('id', $this->product->id)->update(['cnt_images' => $count]);
    }

    /**
     * Fetched product id using sku and set product id and sku to global variable
     * @param $sku
     */
    function setSku($sku)
    {
        $this->sku = str_replace(['/'], '-', $sku);
        $this->sku = $sku;
    }

    function test()
    {
        $sku = 'R3271981001';
        $this->setSku($sku);
        try {
            $this->fetchResource();
            $this->info("Image grabbed with mime: $this->resourceMime");
            $saved = $this->saveResource();
            $this->info("Image successfully saved as [$saved]");
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }

    /**
     * @param $sku
     * @param $fileType
     * @param $var
     * @return $updatedRepository
     */
    function makeRepository($sku, $fileType, $var)
    {
        $updatedRepository = str_replace(':sku', $sku, $this->repositoryNew);
        $updatedRepository = str_replace(':size', $this->size, $updatedRepository);
        $updatedRepository = str_replace(':type', $fileType, $updatedRepository);
        $updatedRepository = str_replace(':var', $var, $updatedRepository);
        return $updatedRepository;
    }

    /**
     * @throws Exception
     */
    function fetchResource()
    {
        if (is_null($this->sku) or $this->sku == '') {
            throw new \Exception("Sku cannot be empty");
        }
        $uri = $this->makeRepository($this->sku, $this->fileType, $this->var);
        $this->line("Getting: $uri");
        $res = $this->client->get($uri);

        $mime = null;
        $statusCode = $res->getStatusCode();

        if ($statusCode == 200) {
            $contentType = $res->getHeader('Content-Type');
            if ($contentType) {
                $mime = explode(';', $contentType)[0];
            }
        } else {
            throw new \Exception("Image downloader for sku [$this->imageName] has responded with a status code of [$statusCode]");
        }

        $this->resourceBody = $res->getBody();
        $this->resourceMime = $mime;

        if (strlen($this->resourceBody) <= 0) {
            throw new \Exception("Image downloader for sku [$this->sku] has responded with an empty body");
        }
    }

    /**
     * @param $content_type
     * @return string
     */
    function getExtension($content_type)
    {
        switch ($content_type) {
            case 'image/png':
                return 'png';
                break;

            case 'image/gif':
                return 'gif';
                break;

            case 'image/jpeg':
            case 'image/jpg':
                return 'jpg';
                break;

            default:
                return null;
                break;
        }
    }

    function saveResource()
    {
        $filename = $this->imageName;
        $this->files->put($this->tmpStoragePath . $filename, $this->resourceBody);
        return $filename;
    }

    function download()
    {
        try {
            $this->fetchResource();
            $this->saveResource();
        } catch (\Exception $e) {
            $msg = "Remote resource for product ($this->sku) cannot be downloaded: " . $e->getMessage();
            $this->logger->error($msg, 'image_grabber');
        }
    }


    /**
     * @return null
     */
    private function cacheFileExists()
    {
        $cacheFile = $this->tmpStoragePath . $this->imageName;
        if ($this->files->exists($cacheFile)) {
            return $this->imageName;
        }
        return null;
    }


    private function cleanCacheFiles()
    {
        $files = $this->files->files($this->tmpStoragePath);
        foreach ($files as $file) {
            $size = filesize($file);
            //$this->line("$file => $size");
            if ($size == 93734) {
                $this->files->delete($file);
            }
        }
    }


    public function cropWhiteBackground($file = null)
    {
        $this->line(__METHOD__);
        if (is_null($file))
            $file = base_path('sources/source-white.jpg');

        if ($this->files->exists($file)) {
            $extension = strtolower($this->files->extension($file));
            if ($extension == 'jpeg')
                $extension = 'jpg';

            $data = $this->getBoundaries($file, $extension);
            $this->line("Boundaries");
            //print_r($data);
            //Utils::log($data, __METHOD__);
            $img = Image::make($file);
            $img->crop($data['inner_width'], $data['inner_height'], $data['left'], $data['top']);
            /*$spaceH = 300;
            $spaceV = 300;
            $img->resizeCanvas($data['inner_width'] + $spaceH, $data['inner_height'] + $spaceV, 'center', false, '#ffffff');*/

            $img->save(base_path('sources/test-white.' . $extension));
            $this->info("Img saved");
        }
    }


    function __destruct()
    {
        if (!empty($this->notFounds)) {
            $this->logger->error($this->notFounds, 'These SKUs have 0 images on the repository');
        }
    }


    /**
     * @param $sku
     * @param null $pivot_date
     * @return array
     */
    public function getVarsBySku($sku, $pivot_date = null)
    {
        $builder = DB::connection('repository')
            ->table('media')
            ->where('code', $sku)
            ->whereIn('CONTENT_TYPE', $this->valid_mimes)
            ->select(DB::raw('distinct(VAR)'))
            ->orderBy('VAR');

        if ($pivot_date) {
            $builder->where('UPLOADED', '>=', $pivot_date);
        }

        $vars = $builder->lists('VAR');
        return $vars;
    }


    protected function sendReport()
    {
        $rows = $this->messages;
        $recipient_internal = $this->email;
        Mail::send('emails.morellato.imagegrabber', compact('rows'), function ($mail) use ($recipient_internal) {
            $mail->from(\Cfg::get('MAIL_GENERAL_ADDRESS'));
            $mail->subject("Blade report: fetching immagini da repository Morellato");
            $mail->to($recipient_internal);
            $mail->bcc('f.politi@icoa.it');
        });
    }

}