<?php

namespace services\Morellato\ResponseCache;

use Illuminate\Http\Request;
use services\Morellato\ResponseCache\CacheProfiles\CacheProfile;

class RequestHasher
{
    /**
     * @var \services\Morellato\ResponseCache\CacheProfiles\CacheProfile
     */
    protected $cacheProfile;

    /**
     * @param CacheProfile $cacheProfile
     */
    public function setCacheProfile(CacheProfile $cacheProfile){
        $this->cacheProfile = $cacheProfile;
    }

    /**
     * Get a hash value for the given request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return string
     */
    public function getHashFor(Request $request)
    {
        $uri = $request->getUri();
        $method = $request->getMethod();
        $suffix = $this->cacheProfile->cacheNameSuffix($request);
        //audit(compact('uri','method','suffix'), __METHOD__);

        return 'responsecache-'.md5(
            "{$uri}/{$method}/".$suffix
        );
    }
}
