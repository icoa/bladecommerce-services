<?php

namespace services\Morellato\ResponseCache\CacheProfiles;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use FrontTpl;

class CacheEcommerceRequests extends BaseCacheProfile implements CacheProfile
{
    /**
     * Determine if the given request should be cached;.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     */
    public function shouldCacheRequest(Request $request)
    {
        $scope = FrontTpl::getScope();
        $scope_id = FrontTpl::getScopeId();
        $scopeName = $scope . '-' . $scope_id;

        $logical = false;
        switch ($scope){
            case 'product':
            case 'catalog':
            case 'home':
            case 'page':
            case 'section':
                $logical = true;
                break;
        }

        if(in_array($scopeName, config('responsecache.notCacheableLayouts'))){
            return false;
        }

        if ($request->ajax()) {
            return false;
        }

        if ($this->isRunningInConsole()) {
            return false;
        }

        if($request->isMethod('get')){
            $not_cacheable_params = [
                'grpCode',
                'grpcode',
                'cmpCode',
                'cmpcode',
            ];

            foreach($not_cacheable_params as $param){
                if($request->has($param) && trim($request->get($param)) !== ''){
                    return false;
                }
            }
        }

        return $request->isMethod('get') and $logical;
    }

    /**
     * Determine if the given response should be cached.
     *
     * @param \Symfony\Component\HttpFoundation\Response $response
     *
     * @return bool
     */
    public function shouldCacheResponse(Response $response)
    {
        return $response->isSuccessful() || $response->isRedirection();
    }
}
