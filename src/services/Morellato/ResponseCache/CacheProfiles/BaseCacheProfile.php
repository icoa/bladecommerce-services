<?php

namespace services\Morellato\ResponseCache\CacheProfiles;

use Carbon\Carbon;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use FrontUser;
use CartManager;
use Core;
use Sentry;

abstract class BaseCacheProfile
{
    /**
     * @var \Illuminate\Foundation\Application
     */
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    /**
     * Return the time when the cache must be invalided.
     *
     * @return \DateTime
     */
    public function cacheRequestUntil(Request $request)
    {
        return Carbon::now()->addMinutes($this->app['config']->get('responsecache.cacheLifetimeInMinutes'));
    }

    /**
     * Set a string to add to differentiate this request from others.
     *
     * @return string
     */
    public function cacheNameSuffix(Request $request)
    {
        $keys = [];
        $cart_id = \Core::cartIsNotEmpty();
        $auth_id = \Core::customerIsLogged();

        if ($auth_id) {
            $keys[] = 'A_' . $auth_id;
        }
        if($cart_id){
            $keys[] = 'C_' . $cart_id;
        }
        $theme = config('mobile') ? 'mobile' : 'frontend';
        $keys[] = 'T_' . $theme;

        $keys[] = 'L_' . Core::getLang();
        $keys[] = 'Y_' . Core::getCurrency();
        $country = Core::getCountry();
        if ($country) {
            $keys[] = 'N_' . $country->id;
        }

        //adding more key for B2B in the frontend
        if (config('services.b2b', false) == true) {
            $user = Sentry::getUser();
            if ($user and $user->negoziando == 1) {
                $keys[] = 'S_' . $user->id;
            }
        }

        return implode('-', $keys);
    }

    /**
     * Determine if the app is running in the console.
     *
     * To allow testing this will return false the environment is testing.
     *
     * @return bool
     */
    public function isRunningInConsole()
    {
        if ($this->app->environment('testing')) {
            return false;
        }

        return $this->app->runningInConsole();
    }
}
