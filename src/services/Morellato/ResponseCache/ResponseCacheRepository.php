<?php

namespace services\Morellato\ResponseCache;

use Illuminate\Foundation\Application;

use Cache;

class ResponseCacheRepository
{
    /**
     * @var \Illuminate\Cache\Repository
     */
    protected $cache;

    /**
     * @var \services\Morellato\ResponseCache\ResponseSerializer
     */
    protected $responseSerializer;

    /**
     * @var string
     */
    protected $cacheStoreName;

    /**
     * @param \Illuminate\Foundation\Application $app
     * @param \services\Morellato\ResponseCache\ResponseSerializer     $responseSerializer
     */
    public function __construct(Application $app, ResponseSerializer $responseSerializer)
    {
        $this->cache = $app['cache']->driver(config('responsecache.cacheStore'))->tags(['responsecache']);
        $this->responseSerializer = $responseSerializer;
    }

    /**
     * @param string                                     $key
     * @param \Symfony\Component\HttpFoundation\Response $response
     * @param \DateTime|int                              $minutes
     */
    public function put($key, $response, $minutes)
    {
        $this->cache->put($key, $this->responseSerializer->serialize($response), $minutes);
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function has($key)
    {
        return $this->cache->has($key);
    }

    /**
     * @param string $key
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function get($key)
    {
        return $this->responseSerializer->unserialize($this->cache->get($key));
    }

    public function flush()
    {
        $this->cache->flush();
    }
}
