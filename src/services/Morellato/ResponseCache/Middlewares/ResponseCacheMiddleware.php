<?php

namespace services\Morellato\ResponseCache\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use services\Morellato\ResponseCache\ResponseCache;

class ResponseCacheMiddleware
{
    /**
     * @var \services\Morellato\ResponseCache\ResponseCache
     */
    protected $responseCache;

    public function __construct(ResponseCache $responseCache)
    {
        $this->responseCache = $responseCache;
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Http\Response $request
     *
     * @return \Illuminate\Http\Response
     */
    public function handle(Request &$request, Response &$response)
    {
        if ($this->responseCache->shouldCache($request, $response)) {
            $cacheableResponse = $this->responseCache->cacheResponse($request, $response);
            $response->setContent($cacheableResponse->getContent());
            foreach($cacheableResponse->headers->all() as $key => $header){
                $response->headers->set($key, $header);
            }
        }

        return $response;
    }

    /**
     * @param Request $request
     * @return null|\Illuminate\Http\Response
     */
    public function handleRequest(Request $request){
        if ($this->responseCache->hasCached($request)) {
            return $this->responseCache->getCachedResponseFor($request);
        }
        return null;
    }
}
