<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 24/11/2017
 * Time: 16:12
 */

namespace services\Morellato\ResponseCache\Commands;


use Illuminate\Console\Command;
use services\Morellato\Database\Sql\Connection;
use services\Morellato\ResponseCache\ResponseCacheRepository;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config, DB, Core;

class ResponseCacheFlushCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'responsecache:flush';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Destroy the cache for frontend responses';

    /**
     * @var ResponseCacheRepository
     */
    protected $repository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->repository = app(ResponseCacheRepository::class);
        $this->line("Executing ResponseCacheFlush...");
        $this->repository->flush();
        $this->info("Cache flushed successfully");
    }




    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}