<?php

namespace services\Morellato\ResponseCache;

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use services\Morellato\ResponseCache\CacheProfiles\CacheProfile;

class ResponseCache
{
    /**
     * @var ResponseCache
     */
    protected $cache;

    /**
     * @var RequestHasher
     */
    protected $hasher;

    /**
     * @var CacheProfile
     */
    protected $cacheProfile;

    protected $flushed = false;

    /**
     * @var string
     */
    protected $calculated_hash;

    /**
     * @param \services\Morellato\ResponseCache\ResponseCacheRepository $cache
     * @param \services\Morellato\ResponseCache\RequestHasher $hasher
     */
    public function __construct(ResponseCacheRepository $cache, RequestHasher $hasher)
    {
        $this->cache = $cache;
        $this->hasher = $hasher;
        $this->cacheProfile = app(config('responsecache.cacheProfile'));
        $this->hasher->setCacheProfile($this->cacheProfile);
    }

    /**
     * Determine if the given request should be cached.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Symfony\Component\HttpFoundation\Response $response
     *
     * @return bool
     */
    public function shouldCache(Request $request, Response $response)
    {        
        if (!config('responsecache.enabled')) {
            return false;
        }

        if ($request->attributes->has('cacheresponse.doNotCache')) {
            return false;
        }

        if (!$this->cacheProfile->shouldCacheRequest($request)) {
            return false;
        }

        return $this->cacheProfile->shouldCacheResponse($response);
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function shouldCacheRequest(Request $request)
    {
        return $this->cacheProfile->shouldCacheRequest($request);
    }

    /**
     * Store the given response in the cache.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Symfony\Component\HttpFoundation\Response $response
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function cacheResponse(Request $request, Response $response)
    {
        $hash = $this->hasher->getHashFor($request);
        $this->calculated_hash = $hash;

        if (config('responsecache.addCacheTimeHeader')) {
            $response = $this->addCachedHeader($response);
        }

        $ttl = $this->cacheProfile->cacheRequestUntil($request);

        $this->cache->put($hash, $response, $ttl);

        $this->parseResponse($response);

        return $response;
    }


    /**
     * @param Response $response
     * @return Response
     */
    protected function parseResponse(Response &$response)
    {
        $content = $response->getContent();
        //replace user body class
        $userBodyClass = \FrontTpl::renderBodyClass(false);
        $content = str_replace('_userBodyClass_', $userBodyClass, $content);

        $response->setContent($content);
        return $response;
    }

    /**
     * Determine if the given request has been cached.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return bool
     */
    public function hasCached(Request $request)
    {
        if (!config('responsecache.enabled')) {
            return false;
        }
        //audit(__METHOD__);
        return $this->cache->has($this->hasher->getHashFor($request));
    }

    /**
     * Get the cached response for the given request.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getCachedResponseFor(Request $request)
    {
        $hash = $this->hasher->getHashFor($request);
        $this->calculated_hash = $hash;
        $response = $this->cache->get($hash);

        $content = $response->getContent();

        $searches = [];
        $replaces = [];

        //replace csrf
        $matches = array();
        $regex = "#\<meta name=\"csrf-token\" content=\"(.*)\" \/>#isU";
        if (preg_match_all($regex, $content, $matches)) {
            if (isset($matches[1]) and isset($matches[1][0])) {
                $searches[] = $matches[1][0];
                $replaces[] = csrf_token();
                //$content = str_replace($matches[1][0], csrf_token(), $content);
            }
        }
        $regex = "#\<input type=\"hidden\" name=\"csrf-token\" value=\"(.*)\" \/>#isU";
        if (preg_match_all($regex, $content, $matches)) {
            if (isset($matches[1]) and isset($matches[1][0])) {
                $searches[] = $matches[1][0];
                $replaces[] = csrf_token();
                //$content = str_replace($matches[1][0], csrf_token(), $content);
            }
        }
        /*$regex = "#data-stp=\"(.*)\">#isU";
        if (preg_match_all($regex, $content, $matches)) {
            if (isset($matches[1]) and isset($matches[1][0])) {
                $searches[] = $matches[1][0];
                $replaces[] = \FrontTpl::checkIfShowStorePopup();
                //$content = str_replace($matches[1][0], \FrontTpl::checkIfShowStorePopup(), $content);
            }
        }*/

        if (!empty($searches)) {
            $content = str_replace($searches, $replaces, $content);
        }

        $response->setContent($content);

        $this->parseResponse($response);

        return $response;
    }

    /**
     *  Flush the cache.
     */
    public function flush()
    {
        if ($this->flushed)
            return;

        $this->cache->flush();
        $this->flushed = true;
    }

    /**
     * Add a header with the cache date on the response.
     *
     * @param \Symfony\Component\HttpFoundation\Response $response
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function addCachedHeader(Response $response)
    {
        $clonedResponse = clone $response;
        $now = date('Y-m-d H:i:s');
        $calculated_hash = $this->calculated_hash;

        $clonedResponse->headers->set('X-Response-Cache', 'cached on ' . date('Y-m-d H:i:s'));
        $content = $clonedResponse->getContent();
        $content .= PHP_EOL . "<!-- Response cached on $now with fingerprint <$calculated_hash> -->";
        $clonedResponse->setContent($content);

        return $clonedResponse;
    }
}
