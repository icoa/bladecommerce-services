<?php


namespace services\Morellato\Geo;

use DB;
use CartManager;
use View;


class StoreLocator
{

    protected $latitude;
    protected $longitude;
    protected $radius = 50.0;
    protected $distance_unit = 111.045; //km

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return int
     */
    public function getRadius()
    {
        return $this->radius;
    }

    /**
     * @param int $radius
     */
    public function setRadius($radius)
    {
        $this->radius = $radius;
    }

    /**
     * @return float
     */
    public function getDistanceUnit()
    {
        return $this->distance_unit;
    }

    /**
     * @param float $distance_unit
     */
    public function setDistanceUnit($distance_unit)
    {
        $this->distance_unit = $distance_unit;
    }


    /**
     * @param $latitude
     * @param $longitude
     * @return $this
     */
    function setCoordinates($latitude, $longitude)
    {
        $this->setLatitude($latitude);
        $this->setLongitude($longitude);
        return $this;
    }

    /**
     * @return string
     */
    private function getQueryFilters()
    {
        $affiliateMode = CartManager::getAffiliateMode();
        $filters = 'AND ISNULL(affiliate_id)';
        if ($affiliateMode != \Frontend\CartManager::AFFILIATE_MODE_DEFAULT) {
            if ($affiliateMode == \Frontend\CartManager::AFFILIATE_MODE_COMPLETE) {
                $affiliate_id = CartManager::getSingleAffiliateId();
                if ($affiliate_id) {
                    $filters = 'AND affiliate_id=' . $affiliate_id;
                    $this->radius = 1000;
                }
            }
        }
        return $filters;
    }

    private function getQuery()
    {
        $filters = $this->getQueryFilters();

        if (trim($this->latitude) === '' and trim($this->longitude) === '') {
            // Italy: cfr: https://developers.google.com/public-data/docs/canonical/countries_csv
            $this->latitude = '41.87194';
            $this->longitude = '12.56738';
        }

        $query = <<<QUERY
SELECT *
  FROM (
 SELECT 
				z.*,
        p.radius,
        p.distance_unit
                 * DEGREES(ACOS(COS(RADIANS(p.latpoint))
                 * COS(RADIANS(z.latitude))
                 * COS(RADIANS(p.longpoint - z.longitude))
                 + SIN(RADIANS(p.latpoint))
                 * SIN(RADIANS(z.latitude)))) AS distance
  FROM mi_shops AS z
  JOIN ( 
        SELECT  $this->latitude AS latpoint, $this->longitude AS longpoint,
                $this->radius AS radius, $this->distance_unit AS distance_unit
    ) AS p ON 1=1
  WHERE 
	z.latitude is not null and z.longitude is not null
	and z.active=1
	and isnull(z.deleted_at)
	and z.latitude
     BETWEEN p.latpoint  - (p.radius / p.distance_unit)
         AND p.latpoint  + (p.radius / p.distance_unit)
    AND z.longitude
     BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
         AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
 ) AS d
 WHERE (distance <= radius or ISNULL(distance))
 $filters
 ORDER BY distance
QUERY;
        if ($this->latitude == 'Invalid IP address.' or $this->longitude == 'Invalid IP address.') {
            $query = null;
        }
        return $query;
    }

    function getStores()
    {
        $query = $this->getQuery();
        if (is_null($query)) {
            $query = "SELECT * from mi_shops where active = 1 and isnull(deleted_at) " . $this->getQueryFilters();
        }
        //\Utils::log($query, __METHOD__);
        $results = DB::select($query);
        $rows = [];
        foreach ($results as $result) {
            $obj = new \MorellatoShop((array)$result);
            $rows[] = $obj;
        }
        return $rows;
    }

    function getNearestStore()
    {
        $query = $this->getQuery();
        if (is_null($query)) {
            $query = "SELECT * from mi_shops where active = 1 and isnull(deleted_at) order by rand()";
        }
        $query .= " LIMIT 0,1";
        $result = DB::selectOne($query);
        if ($result) {
            $obj = new \MorellatoShop((array)$result);
            return $obj;
        }
        return null;
    }


    function toJson()
    {
        $ids = [];
        $query = "SELECT id from mi_shops where active = 1 and isnull(deleted_at) " . $this->getQueryFilters();
        $rows = DB::select($query);
        foreach ($rows as $row) {
            $ids[] = $row->id;
        }
        /** @var \MorellatoShop[] $rows */
        $rows = \MorellatoShop::whereNotNull('latitude')->whereNotNull('longitude')->whereIn('id', $ids)->get();
        $data = [];
        foreach ($rows as $row) {
            $data[] = $row->asJson();
        }
        return json_encode($data, JSON_NUMERIC_CHECK);
    }

    function getStore($id)
    {
        return \MorellatoShop::find($id);
    }

    function printLegend()
    {
        $data = [
            'items' => [
                (object)[
                    'name' => 'BLUESPIRIT',
                    'icon' => '/media/images/map/blu@2x.png',
                ],
                (object)[
                    'name' => 'MORELLATO',
                    'icon' => '/media/images/map/mor@2x.png',
                ],
                (object)[
                    'name' => 'JOYÈ',
                    'icon' => '/media/images/map/joy@2x.png',
                ],
                (object)[
                    'name' => 'D\'AMANTE',
                    'icon' => '/media/images/map/dam@2x.png',
                ],
            ]
        ];
        return View::make('ajax.store_legend', $data);
    }
}