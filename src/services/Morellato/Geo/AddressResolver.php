<?php

namespace services\Morellato\Geo;

use Config;

class AddressResolver
{
    protected $url = 'https://maps.google.com/maps/api/geocode/json?address={ADDRESS}&key={KEY}';

    function make($address)
    {
        $address = urlencode($address);
        $data = [
            '{ADDRESS}' => $address,
            '{KEY}' => config('morellato.google.key_maps')
        ];

        $url = str_replace(array_keys($data), array_values($data), $this->url);

        try {

            // get the json response
            $resp_json = file_get_contents($url);

            // decode the json
            $resp = json_decode($resp_json, true);

            // response status will be 'OK', if able to geocode given address
            if ($resp['status'] == 'OK') {

                // get the important data
                $lat = $resp['results'][0]['geometry']['location']['lat'];
                $long = $resp['results'][0]['geometry']['location']['lng'];
                $formatted_address = $resp['results'][0]['formatted_address'];

                // verify if data is complete
                if ($lat && $long && $formatted_address) {
                    return compact('lat','long','formatted_address');
                } else {
                    return false;
                }

            } else {
                return false;
            }
        } catch (\Exception $e) {
            return false;
        }
    }
}