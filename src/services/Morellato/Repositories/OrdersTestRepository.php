<?php

namespace services\Morellato\Repositories;

use Carbon\Carbon;
use DB, App, Config, Utils;
use Illuminate\Filesystem\Filesystem;
use services\Models\OrderSlip;
use services\Models\OrderSlipDetail;
use services\Morellato\Gls\Handler\OrderHandler;
use Str;
use Product;
use Order;
use OrderState;
use PaymentState;
use League\Csv\Writer;
use League\Csv\Reader;

class OrdersTestRepository extends Repository
{
    /**
     * @var Filesystem
     */
    protected $files;

    /**
     * @var array
     */
    protected $commands = [
        ['table', 'Print summary orders table by latest max items'],
        ['table_pick', 'Print summary orders table by providing a list of orders ID(s)'],
        ['gls_tracking_max', 'Submit orders for tracking by latest max items'],
        ['gls_tracking_pick', 'Submit orders for tracking by providing a list of orders ID(s)'],
        ['migrate_local', 'Migrate all local orders to SHIPPED status'],
        ['gls_orphans', 'Search for GLS orphans order (without tracking) and reset to WS2 parameters'],
    ];

    /**
     * OrdersTestRepository constructor.
     * @param Filesystem $files
     */
    public function __construct(Filesystem $files)
    {
        $this->files = $files;
    }

    /** Called before exec */
    protected function before_exec()
    {
        //$this->switchConnection('bluespirit');
    }


    function table()
    {
        $console = $this->getConsole();
        $max = $console->ask('Please provide the number of Orders to fetch');
        if (!is_numeric($max)) {
            $console->error("$max must be a number");
            return;
        }
        $orders = Order::trackableWithGls()->take($max)->orderBy('id', 'desc')->get();
        $this->tableExportChoice($orders);
    }


    function table_pick()
    {
        $console = $this->getConsole();
        $list = $console->ask('Please provide a list of Order\'s ID(s) to fetch');

        $ids = explode(',', $list);
        $orders = Order::whereIn('id', $ids)->orderBy('id', 'desc')->get();
        $this->tableExportChoice($orders);
    }

    /**
     * @param Order[] $orders
     */
    private function tableExportChoice($orders)
    {
        $console = $this->getConsole();
        $mode = $console->choice('Please select export mode', ['csv' => 'CSV', 'console' => 'CONSOLE'], 'console');
        if ($mode == 'csv')
            $this->csvOrdersTable($orders);

        if ($mode == 'console')
            $this->printOrdersTable($orders);
    }

    /**
     * @param Order[] $orders
     *
     * @return array
     */
    private function getOrdersTable($orders)
    {
        $headers = [
            'ID',
            'RFR',
            'SAP',
            'STS',
            //'PAY_STS',
            'DLV_SHOP',
            'PAY_SHOP',
            'AVL_SHOP',
            'WRW_DIFF',
            'WRW_SAME',
            'GLS_TRK',
            'SAP_DLV',
            'SAP_SLS',
            'SAP_WRW',
            'GLS_XML',
            'GLS_RSL',
            'GLS_TRK',
        ];

        $rows = [];

        foreach ($orders as $order) {

            $row = [
                'id' => $order->id,
                'reference' => $order->reference,
                'sap_scenary' => $order->getSapScenary(),
                'status' => $order->statusName(),
                //'payment_status' => $order->paymentStatusName(),
                'delivery_store' => $this->consoleBool($order->hasDeliveryStore()),
                'store_payment' => $this->consoleBool($order->hasStorePayment()),
                'shop_availability' => $this->consoleBool($order->hasShopAvailability()),
                'withdrawal_different_store' => $this->consoleBool($order->hasDifferentStoreForWithdrawal()),
                'withdrawal_same_store' => $this->consoleBool($order->hasSameStoreForWithdrawal()),
                'gls_tracking_mode' => '[' . $order->getGlsTrackingMode() . ']',
                'delivery_id' => '[' . $order->getGlsBda() . ']',
                'sales_id' => '[' . $order->getSapSale() . ']',
                'withdrawal_id' => '[' . $order->getGlsNumRit() . ']',
                'gls_xml_url' => $order->getGlsXmlUrl(),
                'gls_tracking_result' => $order->getGlsLatestTrackingResult(),
                'gls_tracking_url' => $order->getGlsTrackingUrl(),
            ];
            $rows[] = $row;
        }

        return compact('headers', 'rows');
    }

    /**
     * @param Order[] $orders
     */
    private function printOrdersTable($orders)
    {

        $response = $this->getOrdersTable($orders);
        $this->getConsole()->table($response['headers'], $response['rows']);
    }

    /**
     * @param Order[] $orders
     */
    private function csvOrdersTable($orders)
    {
        //we create the CSV into memory
        $fileName = base_path('sources/csvOrdersTable.csv');
        touch($fileName);
        $csv = Writer::createFromPath($fileName);
        $csv->setDelimiter(';');

        $response = $this->getOrdersTable($orders);
        $csv->insertOne($response['headers']);
        $csv->insertAll($response['rows']);

        $this->getConsole()->info("Orders exported in $fileName");
    }

    /**
     * @param $element
     * @return string
     */
    private function consoleBool($element)
    {
        return ($element == 1 or true === $element) ? '[X]' : '[ ]';
    }


    function gls_tracking_max()
    {
        $console = $this->getConsole();
        $max = $console->ask('Please provide the number of Orders to fetch');
        if (!is_numeric($max)) {
            $console->error("$max must be a number");
            return;
        }
        $orders = Order::trackableWithGls()->take($max)->orderBy('id', 'desc')->get();
        $this->testGlsTracking($orders);
    }


    function gls_tracking_pick()
    {
        $console = $this->getConsole();
        $list = $console->ask('Please provide a list of Order\'s ID(s) to fetch');

        $ids = explode(',', $list);
        $orders = Order::whereIn('id', $ids)->orderBy('id', 'desc')->get();
        $this->testGlsTracking($orders);
    }

    /**
     * @return OrderHandler
     */
    function getGlsOrderHandler()
    {
        /** @var OrderHandler $handler */
        $handler = app('services\Morellato\Gls\Handler\OrderHandler');
        $handler->setConsole($this->getConsole());
        return $handler;
    }

    /**
     * @param Order[] $orders
     */
    private function testGlsTracking($orders)
    {
        $handler = $this->getGlsOrderHandler();
        $handler->setDebug(true);

        foreach ($orders as $order) {
            $handler->trackOrder($order->id);
        }

    }


    function migrate_local()
    {
        $console = $this->getConsole();
        $orders = Order::withStatus(OrderState::STATUS_WORKING)->where('availability_mode', Order::MODE_LOCAL)->get();
        $many = count($orders);
        foreach ($orders as $order) {
            $console->line("Processing order [$order->reference]($order->id)");
            $order->setStatus(OrderState::STATUS_SHIPPED);
            $console->info('-> OK, status migrated');
        }
    }


    function gls_orphans()
    {
        audit_watch();
        $console = $this->getConsole();
        /** @var Order[] $orders */
        $orders = Order::with('order_slip_details')
            ->withoutGlsTracking()
            ->withOrderSlipDetailType(OrderSlipDetail::TYPE_SHIPPING)
            ->orderBy('id', 'desc')
            ->get();
        $many = count($orders);
        foreach ($orders as $order) {
            $console->line("Processing order [$order->reference]($order->id)");
            $ws2_shipping = $order->getShippingNumberFromOrderSlipDetails();
            if ($ws2_shipping) {
                $console->comment("Found shipping number: $ws2_shipping");
                $slip = $order->getMorellatoSlip();
                if ($slip) {
                    $old_value = $slip->delivery_id;
                    $slip->delivery_id = $ws2_shipping;
                    if ($old_value != $ws2_shipping) {
                        $console->info("Delivery ID (BDA) switched from $old_value => $ws2_shipping");
                        $slip->save();
                    }
                }
            }
            $ws2_withdrawal = $order->getCarrierShippingNumberFromOrderSlipDetails();
            if ($ws2_withdrawal) {
                $console->info("Found carrier number: $ws2_withdrawal");
            }
        }
    }
}