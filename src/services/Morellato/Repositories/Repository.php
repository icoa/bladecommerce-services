<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 01/08/2017
 * Time: 11:19
 */

namespace services\Morellato\Repositories;

use Illuminate\Console\Command;
use DB;

class Repository
{

    protected $command;
    protected $commands = [];

    /**
     * @param Command $command
     */
    public function setConsole(Command $command)
    {
        $this->command = $command;
    }

    /**
     * @return Command
     */
    public function getConsole()
    {
        return $this->command;
    }

    /**
     * @return bool
     */
    function isConsole(){
        return \App::runningInConsole();
    }

    /** Called before exec */
    protected function before_exec(){

    }

    /**
     * @param $method
     */
    public function exec($method)
    {
        $this->before_exec();
        if (method_exists($this, $method)) {
            $this->getConsole()->line("Found [$method] to execute. Firing now...");
            $this->$method();
        } else {
            $this->getConsole()->error("Could not find any [$method] to fire. Exiting now...");
        }
    }


    /**
     * Switch connection to perform all queries on Cloud DB
     *
     * @param string $connection
     */
    public function switchConnection($connection = 'cloud')
    {
        $console = $this->getConsole();

        $dbName = config("database.connections.$connection.database");
        $console->comment("Switching connection to cloud database [$connection.$dbName]...");
        DB::disconnect();
        DB::setDefaultConnection($connection);
        DB::reconnect();
        $console->info('-> DONE');
    }


    /**
     * Print the help for the command repository
     */
    function help()
    {
        $console = $this->getConsole();
        $console->table(['Synopsis', 'Description'], $this->commands);
    }
}