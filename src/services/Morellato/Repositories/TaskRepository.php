<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 01/08/2017
 * Time: 11:19
 */

namespace services\Morellato\Repositories;

use Carbon\Carbon;
use DB;
use Frontend\Module;
use Illuminate\Encryption\Encrypter;
use Illuminate\Filesystem\Filesystem;
use Intervention\Image\Image;
use OrderState;
use services\Auth\GoogleAuthenticator\GoogleAuthenticator;
use services\Auth\GoogleAuthenticator\GoogleQrUrl;
use services\Membership\Models\Affiliate;
use services\Models\BuilderBlueprint;
use services\Models\BuilderPayload;
use services\Models\BuilderProduct;
use services\Models\OrderSlipDetail;
use services\Models\OrderTracking;
use services\Morellato\Database\JsonImporter;
use services\Morellato\Gls\Tests\TestOrderHandler;
use services\Morellato\Gls\Xml\Reader\AddressXmlReader;
use services\Morellato\Plugins\PhotoslurpPlugin;
use services\Morellato\Soap\Tests\TestOrderFlow;
use services\OrderCollectorService;
use services\Remote\Repositories\RemoteRepository;
use services\Repositories\CartRepository;
use services\Repositories\ConfiguratorRepository;
use services\Repositories\CsvProductsRepository;
use services\Repositories\OrderRepository;
use services\TagCommander\TagCommanderOrderRepository;
use Str;
use services\IpResolver;
use services\Morellato\Geo\AddressResolver;
use Product;
use Order;
use League\Csv\Writer;
use services\Morellato\Database\StrapsCustomerImporter;
use services\Strap\Repositories\StrapUserRepository;
use services\Morellato\Soap\Handler\OrderFlowHandler;
use services\Helpers\UUID;
use services\Bluespirit\Frontend\Lps\MyLuckyNumber;

class TaskRepository extends Repository
{

    protected $files;
    protected $errors = [];

    public function __construct(Filesystem $files)
    {
        $this->files = $files;
    }

    function ip()
    {
        $service = new IpResolver();
        $console = $this->getConsole();
        $ip = $console->ask('Please provide an IP number');
        $records = $service->resolve($ip);
        echo 'IP Number             : ' . $records['ipNumber'] . "\n";
        echo 'IP Version            : ' . $records['ipVersion'] . "\n";
        echo 'IP Address            : ' . $records['ipAddress'] . "\n";
        echo 'Country Code          : ' . $records['countryCode'] . "\n";
        echo 'Country Name          : ' . $records['countryName'] . "\n";
        echo 'Region Name           : ' . $records['regionName'] . "\n";
        echo 'City Name             : ' . $records['cityName'] . "\n";
        echo 'Latitude              : ' . $records['latitude'] . "\n";
        echo 'Longitude             : ' . $records['longitude'] . "\n";
    }


    function runAddress()
    {
        $service = new AddressResolver();
        $id = 22;
        $obj = \MorellatoShop::getObj($id);
        $obj->resolveAddress();
    }


    function runFase03()
    {
        //set warehouse to existing orders
        $start_id = \Config::get('soap.order.startId');
        $rows = \Order::where('id', '>', $start_id)->whereNull('warehouse')->get();
        DB::beginTransaction();
        $many = count($rows);
        $this->info("Found $many records to process...");
        $counter = 0;
        foreach ($rows as $row) {
            $counter++;
            $this->line("Processing record $row->id ($counter/$many)");
            $warehouse = null;
            switch ($row->availability_mode) {
                case 'online':
                    $warehouse = '01';
                    break;
                case 'shop':
                    $warehouse = 'NG';
                    break;
                case 'local':
                    $warehouse = 'LC';
                    break;
            }
            DB::table('orders')->where('id', $row->id)->update(['warehouse' => $warehouse]);
        }
        DB::commit();
        $this->info('-> DONE ORDERS');


        $rows = \OrderDetail::where('order_id', '>', $start_id)->whereNull('warehouse')->get();
        DB::beginTransaction();
        $many = count($rows);
        $this->info("Found $many records to process...");
        $counter = 0;
        foreach ($rows as $row) {
            $counter++;
            $this->line("Processing record $row->id ($counter/$many)");
            $warehouse = null;
            switch ($row->availability_mode) {
                case 'online':
                    $warehouse = '01';
                    break;
                case 'shop':
                    $warehouse = 'NG';
                    break;
                case 'local':
                    $warehouse = 'LC';
                    break;
            }
            DB::table('order_details')->where('id', $row->id)->update(['warehouse' => $warehouse]);
        }
        DB::commit();
        $this->info('-> DONE ORDER DETAILS');


        //check the products_specific_stocks from 77 to 01
        $query = "UPDATE products_specific_stocks SET warehouse='01' WHERE warehouse='77'";
        DB::statement($query);
        $this->info('-> DONE STOCKS');
    }


    private function runCombinations()
    {
        \Utils::watch();
        $rows = \ProductCombination::orderBy('product_id', 'desc')->get();
        $ids = [];
        foreach ($rows as $row) {
            $tempSku = substr($row->sku, 0, -2);
            $product = \Product::find($row->product_id);
            if ($product and $product->is_out_of_production == 0) {
                $tempProductSku = substr($product->sku, 0, -2);
                if ($tempSku != $tempProductSku) {
                    $ids[] = $product->id;
                }
            }
        }
        $ids = array_unique($ids);
        print_r($ids);
    }


    private function runMastersku()
    {
        $rows = \DB::select("select sku,mastersku,sap_sku from mi_anagrafica where sap_sku != mastersku and mastersku != '' and json != ''");
        $many = count($rows);
        $this->comment("Found [$many] records...");
        $counter = 0;
        \DB::beginTransaction();
        foreach ($rows as $row) {
            $counter++;
            $this->line("Processing $counter/$many");
            $id = \DB::table('products')->where('sku', $row->sku)->pluck('id');
            if ($id > 0) {
                $this->info("Updating $id");
                \DB::table('products')->where('id', $id)->update(['mastersku' => $row->mastersku]);
            }
        }
        \DB::commit();
    }


    private function _getCountry($code)
    {
        $row = \DB::table('countries')->where('iso_code', $code)->first();
        return $row;
    }

    private function _getState($code, $country_id)
    {
        $row = \DB::table('states')->where('iso_code', $code)->where('country_id', $country_id)->first();
        return $row;
    }


    private function runStates()
    {
        $rows = \DB::table('import_iso')->orderBy('country')->orderBy('state')->orderBy('name')->get();
        $now = date('Y-m-d H:i:s');

        \DB::beginTransaction();
        foreach ($rows as $row) {
            $country = $this->_getCountry($row->country);
            if ($country) {
                $state = $this->_getState($row->state, $country->id);
                if ($state) {
                    $this->info("Found state [$row->state | $row->name]");
                    \DB::table('states')->where('id', $state->id)->update(['name' => $row->name]);
                } else {
                    $this->comment("Non found state [$row->state | $row->name]");
                    $data = [
                        'name' => $row->name,
                        'iso_code' => $row->state,
                        'country_id' => $country->id,
                        'active' => 1,
                        'zone_id' => $country->zone_id,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ];
                    \DB::table('states')->insert($data);
                }
            } else {
                $this->error("Country not found [$row->country]");
            }
        }
        \DB::commit();
    }


    private function runWhite()
    {
        $service = \App::make('services\Morellato\Grabbers\ImageGrabber', [$this]);
        $service->cropWhiteBackground();
    }

    protected function import_text()
    {
        $this->description('it');
        $this->description('en');
        print_r($this->errors);
    }

    protected function description($lang)
    {
        $this->getConsole()->comment("Importing product description");
        $sourceDir = storage_path('import/descriptions/' . $lang);
        $files = $this->files->allFiles($sourceDir);
        //print_r($files);
        $debug = false;
        DB::beginTransaction();
        foreach ($files as $file) {
            try {
                $ext = $file->getExtension();
                $fileName = $file->getFilename();
                $canImport = true;
                if ($debug) {
                    $canImport = false;
                    if ('TLJ933.json' == $fileName or 'TLJ933.txt' == $fileName) {
                        $canImport = true;
                    }
                }
                if ($canImport) {
                    $content = $this->files->get($sourceDir . '/' . $fileName);
                    if ($ext == 'txt') {
                        $this->importDescriptionHtml($fileName, $content, $lang);
                    }
                    if ($ext == 'json') {
                        $this->importDescriptionJson($fileName, $content, $lang);
                    }
                }
            } catch (\Exception $e) {
                $this->getConsole()->error($e->getMessage());
            }
        }
        DB::commit();
    }


    private function getProductByFilename($filename)
    {
        $sku = str_replace(['.json', '.txt'], '', $filename);
        $obj = Product::where('sku', $sku)->first();
        if ($obj)
            return $obj;

        $sku = str_replace('-', '/', $sku);
        $obj = Product::where('sku', $sku)->first();
        return $obj;
    }


    private function importDescriptionHtml($fileName, $content, $lang)
    {
        $product = $this->getProductByFilename($fileName);
        if ($product) {
            $this->getConsole()->line("Importing description for product [$fileName]");
            $content = trim($content);
            DB::table('products_lang')->where('product_id', $product->id)->where('lang_id', $lang)->update(['ldesc' => $content]);
            $this->getConsole()->info('-> DONE');
        } else {
            $this->getConsole()->error("No product found for [$fileName]");
            $this->errors[] = $fileName;
        }
    }


    private function importDescriptionJson($fileName, $content, $lang)
    {
        $product = $this->getProductByFilename($fileName);
        if ($product) {
            $this->getConsole()->line("Importing json for product [$fileName]");
            $content = trim($content);
            DB::table('products_lang')->where('product_id', $product->id)->where('lang_id', $lang)->update(['head' => $content]);
            $this->getConsole()->info('-> DONE');
        } else {
            $this->getConsole()->error("No product found for [$fileName]");
            $this->errors[] = $fileName;
        }
    }


    public function fixdb()
    {
        $console = $this->getConsole();
        $this->getConsole()->line("Fixing DB");
        $text = <<<TEXT
0		|	112 118 132 133 134 135 136 139 141 157 159 160 162 163 164 165 166 173 174 187 227 229 234 236 240 243 246 249 252 254 261 266 269 274 277 282 286 360 361 363 369 440 506 508 510 513 515 517 519 599
TEXT;

        $lines = explode(PHP_EOL, $text);
        $many = count($lines);
        $console->line("There are $many lines");
        $pages = [];
        $modules = [];
        foreach ($lines as $line) {
            $tokens = explode('|', $line);
            $page_id = trim($tokens[0]);
            $module_ids = explode(' ', trim($tokens[1]));
            $pages[] = $page_id;
            $modules = array_merge($modules, $module_ids);
        }
        \Utils::watch();
        \Page::whereIn('id', $pages)->delete();
        \Module::whereIn('id', $modules)->delete();
    }


    public function attributes_refactory()
    {
        $console = $this->getConsole();
        $rows = DB::table('products_attributes')->get();
        DB::beginTransaction();
        $counter = 0;
        $many = count($rows);
        $console->line("Found [$many] records");
        foreach ($rows as $row) {
            $counter++;
            DB::table('products_attributes')->where('id', $row->id)->update(['id' => $counter]);
            $console->line("Done ($counter/$many)");
        }
        DB::commit();


        $rows = DB::table('products_attributes_position')->get();
        DB::beginTransaction();
        $counter = 0;
        $many = count($rows);
        $console->line("Found [$many] records");
        foreach ($rows as $row) {
            $counter++;
            DB::table('products_attributes_position')->where('id', $row->id)->update(['id' => $counter]);
            $console->line("Done ($counter/$many)");
        }
        DB::commit();

    }


    public function images_cmd()
    {
        $console = $this->getConsole();
        $console->line(__METHOD__);
        $product_ids = \Product::where('source', 'morellato')->lists('id');
        $images = \ProductImage::whereIn('product_id', $product_ids)->get();
        $console->line(count($images));
        $lines = [];
        $sourceDir = '/workarea/www/bluespirit/l4/public/assets/products/';
        $targetDir = '/workarea/morellato/';
        foreach ($images as $img) {
            $cmd = "cp {$sourceDir}{$img->filename} {$targetDir}{$img->filename}";
            $lines[] = $cmd;
        }
        \File::put(storage_path('cmd_images.txt'), implode(PHP_EOL, $lines));
        $console->info('DONE');
    }


    public function clear_zombies()
    {
        /*delete from products_lang where product_id not in (select id from products);
delete from products_attributes where product_id not in (select id from products);
delete from products_attributes_position where product_id not in (select id from products);
delete from categories_products where product_id not in (select id from products);
delete from images where product_id not in (select id from products);
delete from images_lang where product_image_id not in (select id from images);*/
    }


    public function clear_images()
    {

        $root = public_path('assets/products/');
        $imageFiles = \File::allFiles($root);
        $names = [];
        foreach ($imageFiles as $imageFile) {
            $names[] = $imageFile->getFilename();
        }
        $files = DB::table('images')->orderBy('filename')->lists('filename');
        $diff = array_diff($names, $files);
        print_r($diff);
        return;
        $many = count($files);
        $this->getConsole()->line("Found [$many] to delete");
        foreach ($files as $file) {

        }
    }


    public function dump()
    {

        $dbName = 'kronoshop_temp';

        $tablesToDump = [
            'affiliates',
            'attributes',
            'attributes_lang',
            'attributes_options',
            'attributes_options_lang',
            'attributes_rules',
            'attributes_sets',
            'attributes_sets_content',
            'attributes_sets_content_cache',
            'attributes_sets_rules',
            'attributes_sets_rules_cache',
            'builder_blueprints',
            'builder_payloads',
            'builder_products',
            'brands',
            'brands_lang',
            'cache_products',
            'cache_products_sets',
            'carriers',
            'carriers_group',
            'carriers_lang',
            'categories',
            'categories_lang',
            'categories_products',
            'collections',
            'collections_lang',
            'delivery',
            'emails',
            'emails_lang',
            'fidelity_points',
            'gift_cards',
            'gift_cards_movements',
            'images',
            'images_groups',
            'images_groups_lang',
            'images_lang',
            'images_sets',
            'images_sets_rules',
            'lexicons',
            'lexicons_lang',
            'import_affiliates_stocks',
            /*'images_type',
            'instagrams',
            'magic_tokens',
            'magic_tokens_templates',
            'magic_tokens_templates_lang',
            'menu_types',
            'menus',
            'menus_lang',*/
            //
            'mi_shops',
            'order_states',
            'order_states_lang',

            //'resellers',
            'products',
            'products_attributes',
            'products_attributes_lang',
            'products_attributes_position',
            'products_combinations',
            'products_combinations_attributes',
            'products_combinations_images',
            'products_lang',
            'products_relations',
            'products_specific_prices',
            'products_specific_stocks',
            /*'seo_blocks',
            'seo_blocks_lang',
            'seo_blocks_rules',
            'seo_texts',
            'seo_texts_lang',
            'seo_texts_rules',*/


            'override_rules',
            //'pages',
            //'pages_lang',
            'trends',
            'trends_lang',
            'trends_products',

            'payments',
            'payments_carrier',
            'payments_country',
            'payments_group',
            'payments_lang',
            'price_rules',
            'price_rules_country',
            'price_rules_currency',
            'price_rules_group',
            'price_rules_lang',
            'products_outer_stocks',
            'range_price',
            'range_weight',
            'vouchers',


        ];

        /*$tablesToDump = [
            'address',
            'customers',
            'orders',
            'order_details',
            'order_history',
            'newsletter',
            'mi_orders_slips',
        ];*/

        $notList = implode("','", $tablesToDump);

        $tables = DB::select("SELECT table_name FROM information_schema.tables where table_schema='$dbName' and table_name not in ('$notList')");

        $cmd = 'D:/MariaDB10.0/bin/mysqldump.exe --port=4306 --add-drop-table --no-create-db -uwebmaster -potevi12! ';
        foreach ($tables as $table) {
            $cmd .= "--ignore-table=$dbName.$table->table_name ";
        }
        $cmd .= "$dbName > sources/dump.sql";
        \File::put(base_path('mysqldump.bat'), $cmd);
    }


    public function check_images()
    {
        $files = DB::table('images')->orderBy('filename')->lists('filename');
        $root = public_path('assets/products/');
        $many = count($files);
        $this->getConsole()->comment("Found $many images that dont exist");
        $lines = [];
        foreach ($files as $file) {
            $realPath = $root . $file;
            if (!file_exists($realPath)) {
                $this->getConsole()->line($file);
                //$lines[] = "cp /workarea/www/production/l4/public/assets/products/$file /workarea/bluespirit/";
                $lines[] = "UPDATE images set md5=null where filename='$file';";
            }
        }
        \File::put(storage_path('cmd.txt'), implode(PHP_EOL, $lines));
    }


    public function countries()
    {
        $console = $this->getConsole();
        $console->line(__METHOD__);
        $rows = DB::table('countries_temp')->get();
        foreach ($rows as $row) {
            DB::table('countries')->where('iso_code', $row->iso)->update(['iso_code3' => $row->iso3, 'iso_number' => $row->numcode]);
        }
        $console->info('DONE');
    }


    public function import_customers()
    {
        $table = 'customers_import';
        $default = \Customer::getDefaultAttributes();
        $console = $this->getConsole();
        $default_group_id = 6;

        DB::beginTransaction();

        $move_to_default = true;
        if ($move_to_default) {
            DB::table('customers')
                ->where('default_group_id', $default_group_id)
                ->update(['default_group_id' => 3]);
        }

        $rows = DB::table($table)->where('email', '<>', '')->get();
        foreach ($rows as $row) {
            $data = $default;
            $data['active'] = 1;
            $data['firstname'] = trim($row->firstname);
            $data['lastname'] = trim($row->lastname);
            $data['email'] = trim($row->email);
            $data['name'] = $data['firstname'] . ' ' . $data['lastname'];
            $data['gender_id'] = $row->gender == 'F' ? 2 : 1;
            $data['last_passwd_gen'] = \Format::now();
            $data['passwd'] = \Hash::make(trim($row->password));
            $data['default_group_id'] = $default_group_id;
            if ($row->birthday != '' and $row->birthday != null) {
                $data['birthday'] = $row->birthday;
            }

            $exists = \Customer::where('email', $data['email'])->first();
            if ($exists) {
                $console->line("Customer {$data['email']} already exists");
                $exists->fill($data);
                $exists->save();
                $console->info('OK, Updated');
            } else {
                $console->line("Impporting Customer $row->email");
                $data['secure_key'] = \Format::secure_key();
                $customer = new \Customer($data);
                $customer->save();
                if ($customer->id > 0) {
                    $console->info('OK, Inserted');
                } else {
                    $console->error('Error, Not inserted');
                }
            }

        }

        DB::commit();

    }


    public function export_modules()
    {
        $ids = [385, 386, 387];
        $new_ids = [385, 386, 387];
        $columns = \Schema::getColumnListing('modules');
        //dd($columns);

        $columns_lang = \Schema::getColumnListing('modules_lang');

        $commands = [];

        $rows = \Module::whereIn('id', $ids)->get();
        $index = 0;
        foreach ($rows as $row) {
            $id = $new_ids[$index];
            $fields = $columns;
            $values = [];
            foreach ($columns as $column) {
                $v = $column == 'id' ? $id : $row->$column;
                if ($v === null)
                    $v = 'null';
                $values[] = $v;
            }
            $fieldsList = implode(', ', $fields);
            $valuesList = implode("', '", $values);
            $cmd = "insert into modules($fieldsList) values('$valuesList');";
            $commands[] = str_replace("'null'", 'null', $cmd);

            $lang_rows = \Module_Lang::where('module_id', $row->id)->get();
            foreach ($lang_rows as $lang_row) {
                $fields = $columns_lang;
                $values = [];
                foreach ($columns_lang as $column) {
                    $v = $column == 'module_id' ? $id : $lang_row->$column;
                    if ($v === null)
                        $v = 'null';
                    $values[] = $v;
                }
                $fieldsList = implode(', ', $fields);
                $valuesList = implode("', '", $values);
                $cmd = "insert into modules_lang($fieldsList) values('$valuesList');";
                $commands[] = str_replace("'null'", 'null', $cmd);
            }
            $index++;
        }

        \File::put(base_path('sources/export_modules.sql'), implode(PHP_EOL, $commands));
        $this->getConsole()->info('DONE');
    }


    public function temp_skus()
    {
        $query = "SELECT a.id, a.sku, a.sap_sku
FROM products a
INNER JOIN products b ON a.sku = b.sku and isnull(a.deleted_at) and isnull(b.deleted_at)
WHERE a.id <> b.id and !isnull(a.sap_sku) and a.sap_sku!=''
order by sap_sku asc, id asc";

        DB::table('temp_skus')->truncate();

        $rows = DB::select($query);

        $console = $this->getConsole();

        $many = count($rows);

        $console->line("Found $many records...");

        for ($counter = 0; $counter < $many; $counter += 2) {
            $row = $rows[$counter];
            $y = $counter + 1;
            $clone = isset($rows[$y]) ? $rows[$y] : null;

            if ($clone) {
                $data = [
                    'sku' => $row->sku,
                    'sap_sku' => $row->sap_sku,
                    'id1' => $row->id,
                    'id2' => $clone->id,
                ];

                DB::table('temp_skus')->insert($data);

                $console->line("Inserted sku [$row->sku] $counter/$many");
            }
        }
    }


    public function temp_orders()
    {

        $query = "select * from order_details where ISNULL(deleted_at) and product_id in (select id2 from temp_skus) order by product_id, order_id";

        $rows = DB::select($query);

        $console = $this->getConsole();

        $many = count($rows);

        $console->line("Found $many records...");

        $counter = 0;

        foreach ($rows as $row) {

            $correct_id = DB::table('temp_skus')->where('id2', $row->product_id)->pluck('id1');

            DB::table('order_details')->where('id', $row->id)->update(['product_id' => $correct_id]);

            $counter++;

            $console->line("Updated line [$row->id] with ($correct_id) $counter/$many");
        }

    }


    public function temp_carts()
    {

        $query = "select * from cart_product where ISNULL(deleted_at) and product_id in (select id2 from temp_skus) order by product_id, cart_id";

        $rows = DB::select($query);

        $console = $this->getConsole();

        $many = count($rows);

        $console->line("Found $many records...");

        $counter = 0;

        foreach ($rows as $row) {

            $correct_id = DB::table('temp_skus')->where('id2', $row->product_id)->pluck('id1');

            DB::table('cart_product')->where('id', $row->id)->update(['product_id' => $correct_id]);

            $counter++;

            $console->line("Updated line [$row->id] with ($correct_id) $counter/$many");
        }
    }


    public function temp_mvts()
    {

        $query = "select * from stock_mvts where product_id in (select id2 from temp_skus) order by product_id";

        $rows = DB::select($query);

        $console = $this->getConsole();

        $many = count($rows);

        $console->line("Found $many records...");

        $counter = 0;

        foreach ($rows as $row) {

            $correct_id = DB::table('temp_skus')->where('id2', $row->product_id)->pluck('id1');

            DB::table('stock_mvts')->where('id', $row->id)->update(['product_id' => $correct_id]);

            $counter++;

            $console->line("Updated line [$row->id] with ($correct_id) $counter/$many");
        }
    }


    public function storelocator()
    {
        $console = $this->getConsole();
        $console->line("Executing storelocator...");
        $rows = \Reseller::where(function ($query) {
            $query->whereNull('longitude')->orWhere('longitude', 0);
        })->orWhere(function ($query) {
            $query->whereNull('latitude')->orWhere('latitude', 0);
        })
            //->take(10)
            ->get();

        $many = count($rows);

        $console->line("Found $many records...");

        foreach ($rows as $row) {
            $resolved = $row->resolveAddress();
            if ($resolved) {
                $console->line("Resolved address for [$row->name] ($row->id)");
            } else {
                $console->comment("Could not resolve address for [$row->name] ($row->id)");
            }
        }
    }


    function text_attributes()
    {
        $console = $this->getConsole();
        $attributes = \Attribute::where('frontend_input', 'text')->get();
        foreach ($attributes as $attribute) {
            $console->line("Fixing $attribute->code");
            DB::beginTransaction();
            $query = "select *, count(product_id) as cnt from products_attributes where attribute_id=$attribute->id group by product_id having cnt>1 order by cnt desc, product_id desc";
            $rows = DB::select($query);
            $many = count($rows);
            if ($many > 0) {
                $console->line("Found $many duplicates");
                foreach ($rows as $row) {
                    $ids = DB::table('products_attributes')->where('attribute_id', $attribute->id)->where('product_id', $row->product_id)->orderBy('id', 'desc')->lists('id');
                    if (count($ids) > 1) {
                        $remove = $ids[0];
                        $console->line("Removing id [$remove] for $attribute->id | $row->product_id");
                        DB::table('products_attributes')->where('id', $remove)->delete();
                    }
                }
            }
            DB::commit();
        }
    }


    private function resolveCategory($category)
    {
        if ($category->parent_id == 0) {
            return $category->id;
        }
        return $this->resolveCategory(\Category::getObj($category->parent_id));
    }


    function remap_products_categories()
    {
        $console = $this->getConsole();
        $builder = \Product::orderBy('id', 'asc');
        //$builder->take(10);
        $products = $builder->get();

        $many = count($products);
        $counter = 0;
        DB::beginTransaction();
        foreach ($products as $product) {
            $counter++;
            $console->line("Optimizing product [$product->id] $counter / $many");
            $categories = \Category::whereIn('id', function ($query) use ($product) {
                $query->select('category_id')->from('categories_products')->where('product_id', $product->id);
            })->get();
            $default_category_id = null;
            $main_category_id = null;
            foreach ($categories as $category) {
                $parent_id = $this->resolveCategory($category);
                $console->line("Category: " . $category->id . " | parent: " . $parent_id);
                if (is_null($main_category_id)) {
                    $main_category_id = $parent_id;
                }
                if (is_null($default_category_id)) {
                    $default_category_id = $category->id;
                }
                $data = compact('main_category_id', 'default_category_id');
                print_r($data);
                DB::table('products')->where('id', $product->id)->update($data);
            }
        }
        DB::commit();
    }


    function en_collections()
    {
        $rows = DB::select("select * from collections_lang where lang_id='it' and collection_id not in (select collection_id from collections_lang where lang_id='en')");
        DB::beginTransaction();
        foreach ($rows as $row) {
            $data = [
                'lang_id' => 'en',
                'name' => $row->name,
                'collection_id' => $row->collection_id,
                'slug' => $row->slug,
                'published' => $row->published,
            ];
            DB::table('collections_lang')->insert($data);
        }
        DB::commit();
    }


    function links_for_301()
    {
        $console = $this->getConsole();
        $languages = \Core::getLanguages();

        $sourcePath = storage_path('morellato/csv/export/');
        $filename = $sourcePath . "links_" . date('Ymd') . '.csv';
        touch($filename);

        $console->line("Exporting links...");

        //we create the CSV into memory
        $csv = Writer::createFromPath($filename);

        //we insert the CSV header
        $csv->insertOne(['id', 'sku', 'lang_id', 'url']);

        $rows = Product::orderBy('id', 'desc')
            //->take(10)
            ->get(['id', 'sku']);

        foreach ($rows as $row) {
            foreach ($languages as $lang) {
                $link = \Link::to('product', $row->id, $lang);
                $data = [
                    'id' => $row->id,
                    'sku' => $row->sku,
                    'lang_id' => $lang,
                    'url' => $link,
                ];
                $csv->insertOne($data);
            }
            $console->line("Finished with $row->sku");
        }
    }


    function craft_products_names()
    {
        $console = $this->getConsole();
        $categories = [34, 23, 76, 83, 18, 5, 31];
        $languages = \Core::getLanguages();
        $rows = Product::orderBy('id', 'desc')
            ->whereIn('id', function ($query) use ($categories) {
                $query->select('product_id')->from('categories_products')->whereIn('category_id', $categories);
            })
            //->take(10)
            ->get();

        DB::beginTransaction();

        foreach ($rows as $row) {
            foreach ($languages as $lang) {
                $name = \ProductHelper::craftProductName($row, $lang);
                $console->line("Finished with $row->sku => [$lang] => $name");
                audit("$row->sku => [$lang] => $name");
                $slug = Str::slug($name);
                DB::table('products_lang')->where('product_id', $row->id)->where('lang_id', $lang)->update([
                    'name' => $name,
                    'slug' => $slug,
                ]);
            }
            $row->uncacheSimple();
        }
        DB::commit();
    }


    function export_triboo_missing_products()
    {
        $rows = DB::table('import_entities_sales_products')->whereIn('id', function ($query) {
            $query->select('record_id')->from('import_entities_errors')->where('scope', 'products_sales');
        })->orderBy('sku')
            ->get();

        $console = $this->getConsole();

        $sourcePath = storage_path('morellato/csv/export/');
        $filename = $sourcePath . "triboo_unfound_products_" . date('Ymd') . '.csv';
        touch($filename);

        $console->line("Exporting records...");

        //we create the CSV into memory
        $csv = Writer::createFromPath($filename);

        //we insert the CSV header
        $csv->insertOne(['id', 'order_id', 'sku', 'name']);

        foreach ($rows as $row) {
            $data = [
                'id' => $row->id,
                'order_id' => $row->order_id,
                'sku' => $row->sku,
                'name' => $row->product_name,
            ];
            $csv->insertOne($data);

            $console->line("Finished with $row->sku");
        }
    }


    function create_map_301()
    {
        $tpl = "rewrite ^%s$ %s permanent;";

        $languages = \Core::getLanguages();

        foreach ($languages as $language) {
            $sourcePath = base_path("sources/301_$language.conf");
            $rows = DB::table('import_301')
                ->where('lang_id', $language)
                ->orderBy('source', 'desc')
                //->take(10)
                ->get();

            if (count($rows) == 0)
                continue;

            $lines = [];

            foreach ($rows as $row) {
                $lines[] = sprintf($tpl, trim($row->source), trim($row->target));
            }

            \File::put($sourcePath, implode(PHP_EOL, $lines));
            $this->getConsole()->info($language . ' => done');
        }
    }


    function create_map_301_general()
    {
        $tpl = "rewrite ^%s$ %s permanent;";

        $sourcePath = base_path("sources/301_general.conf");
        $rows = DB::table('import_301')
            ->whereNull('lang_id')
            ->orderBy('source', 'desc')
            //->take(10)
            ->get();

        $lines = [];

        foreach ($rows as $row) {
            $lines[] = sprintf($tpl, trim($row->source), trim($row->target));
        }

        \File::put($sourcePath, implode(PHP_EOL, $lines));
        $this->getConsole()->info('GENERAL => done');
    }


    function remap_triboo_orders()
    {
        $console = $this->getConsole();
        $console->line("Building orders cache...");
        $cache = [];
        $orders = \Order::orderBy('id', 'desc')->select('id', 'reference')->get();
        foreach ($orders as $order) {
            $cache[$order->reference] = $order->id;
        }
        $console->info("DONE");

        $console->line("Remapping orders...");
        $rows = DB::table('import_orders')->whereIn('order', array_keys($cache))
            //->take(10)
            ->get();

        $many = count($rows);

        $console->line("Found $many lines to process...");
        $counter = 0;

        DB::beginTransaction();
        foreach ($rows as $row) {
            $counter++;
            $console->line("Processing line $counter/$many");
            if (isset($cache[$row->order])) {
                $data = [];
                if ($row->ordine_sap != '#N/A' and strlen(trim($row->ordine_sap)) > 0) {
                    $data['sap_sales_id'] = $row->ordine_sap;
                }
                if ($row->ddt != '#N/A' and strlen(trim($row->ddt)) > 0) {
                    $data['sap_delivery_id'] = $row->ddt;
                }
                DB::table('import_entities_sales')->where('id', $cache[$row->order])->update($data);
            } else {
                $console->error("Could not find $row->order");
            }
        }
        DB::commit();
    }


    function import_giftcards()
    {
        $console = $this->getConsole();
        $console->line("Importing gift cards...");
        $rows = DB::table('import_giftcards')->get();
        foreach ($rows as $row) {
            $console->line("Importing $row->code");
            $order = \Order::where('reference', $row->order_reference)->first();
            if ($order) {
                $order_id = $order->id;
                $code = $row->code;
                $orderDetail = \OrderDetail::where('order_id', $order_id)->first();
                if ($orderDetail) {
                    $order_detail_id = $orderDetail->id;
                    $product_id = $orderDetail->product_id;
                    $product = \Product::getObj($orderDetail->product_id);
                    $amount = $row->amount;
                    $original_amount = $product->buy_price;
                    $expires_at = $row->expires;
                    $name = $product->name;
                    $customer_name = $order->getCustomer()->name;
                    $data = compact('code', 'product_id', 'order_id', 'order_detail_id', 'amount', 'original_amount', 'expires_at', 'name', 'customer_name');
                    //audit($data);
                    print_r($data);
                    \GiftCard::updateOrCreate($data, ['code' => $code]);
                }
            } else {
                $console->error("Could not find order $row->order_reference");
            }
        }
    }


    function fix_images_en()
    {
        $rows = DB::select("select * from images_lang where lang_id='it' and `product_image_id`  not in (select `product_image_id`  from images_lang where lang_id='en')");
        foreach ($rows as $row) {
            try {
                DB::table('images_lang')->insert([
                    'product_image_id' => $row->product_image_id,
                    'lang_id' => 'en',
                    'legend' => '',
                    'published' => 1,
                ]);
                $this->getConsole()->line($row->product_image_id);
            } catch (\Exception $e) {
                $this->getConsole()->line($e->getMessage());
            }
        }
    }


    function remap_order_slips()
    {
        $rows = DB::table('mi_orders_slips')->get();
        DB::beginTransaction();
        DB::table('orders')->update(['shipping_number' => null]);
        foreach ($rows as $row) {
            DB::table('orders')->where('id', $row->order_id)->update(['shipping_number' => $row->delivery_id]);
        }
        DB::commit();
    }


    function redirect_domains()
    {
        $tpl = "server { 
	listen 80;
	server_name %s;	
	return 301 \$scheme://www.morellato.com%s;
}";

        $languages = \Core::getLanguages();

        $lines = [];
        $sourcePath = base_path("sources/domains.conf");

        foreach ($languages as $language) {

            $rows = DB::table('import_domains')
                ->where('lang_id', $language)
                ->orderBy('domain', 'asc')
                ->get();

            $domains = [];

            foreach ($rows as $row) {
                $target = $row->lang_id == 'it' ? null : "/" . $row->lang_id;
                $domains[] = $row->domain;
                $domains[] = 'www.' . $row->domain;
            }

            $lines[] = sprintf($tpl, trim(implode(' ', $domains)), trim($target));
            $this->getConsole()->info($language . ' => done');

        }

        \File::put($sourcePath, implode(PHP_EOL, $lines));

    }


    function fix_corrupted_images()
    {
        $ids_txt = '31435,31998,32045,52614,52615,52616,52616,52617,52617,52618,52618,52619,52619,52621,52621,52622,52622,52623,52623,52624,52624,52625,52625,52626,52626,52291,52295,52310,52311,52312,53710,52620,52620,53019,53019,53019,53019,53020,53020,53020,53020,53026,53026,53026,53026,53039,53039,53039,53039,53040,53040,53040,53040,53041,53041,53041,53041,53042,53042,53042,53042,53043,53043,53043,53043,53044,53044,53044,53044,53048,53048,53048,53048,53049,53049,53049,53049,53050,53050,53050,53050,53051,53051,53051,53051,53052,53052,53052,53052,52317,52318,52319,52320,52321,52322,52323,52324,52035,52036,52041,52042,52043,52044,52045,52046,52047,52048,52049,52050,52051,52052,52053,52054,52056,52057,52058,52060,52061,52062,52063,52064,52065,52068,52069,52085,52086,52088,52089,52090';
        $products = Product::select('id', 'sku', 'sap_sku')
            ->whereIn('id', explode(',', $ids_txt))
            ->get();

        DB::beginTransaction();
        foreach ($products as $product) {
            $img = \ProductImage::where('product_id', $product->id)->where('cover', 1)->first();
            if ($img) {
                $data = ['default_img' => $img->id];
            } else {
                $data = ['default_img' => null, 'is_out_of_production' => 1];
            }
            DB::table('products')->where('id', $product->id)->update($data);
            $this->getConsole()->line("Product $product->id updated");
        }
        DB::commit();
        return;
        $commands = [];
        $sql = [];
        $ids = [];

        $rows = DB::select("select * from images where updated_at >= '2018-05-08' and ISNULL(updated_by)");
        foreach ($rows as $row) {
            $file = public_path('assets/products/' . $row->filename);
            $file_bkp = public_path('assets/bkp/' . $row->filename);
            $commands[] = "cp $file $file_bkp";
            $commands[] = "rm $file";
            $ids[] = $row->product_id;
        }

        \File::put(storage_path('images_sql.txt'), implode(',', $ids));
        \File::put(storage_path('images_command.txt'), implode(PHP_EOL, $commands));
    }


    function check_redirect_domains()
    {
        $checks = "morellato.asia
morellato.at
morellato.ch
morellato.cn
morellato.co.nz
morellato.co.uk
morellato.com
morellato.com.au
morellato.com.es
morellato.com.ru
morellato.com.tw
morellato.es
morellato.fr
morellato.hk
morellato.it
morellato.kr
morellato.my
morellato.ph
morellato.sg
morellato.tw
morellato.xxx
morellato-mea.com
morellatostore.at
morellatostore.be
morellatostore.biz
morellatostore.cn
morellatostore.co.at
morellatostore.co.in
morellatostore.co.uk
morellatostore.com
morellatostore.com.cn
morellatostore.com.es
morellatostore.com.pt
morellatostore.de
morellatostore.es
morellatostore.eu
morellatostore.fr
morellatostore.gr
morellatostore.in
morellatostore.info
morellatostore.it
morellatostore.lu
morellatostore.mobi
morellatostore.net
morellatostore.nl
morellatostore.org
morellatostore.us";

        $finds = explode(PHP_EOL, $checks);
        $registered = DB::table('import_domains')->lists('domain');
        foreach ($finds as $find) {
            if (in_array($find, $registered)) {
                $this->getConsole()->info("$find ok");
            } else {
                $this->getConsole()->comment("$find not found");
            }
        }

    }


    function address_trackings()
    {
        $console = $this->getConsole();

        $rows = \AddressTracking::orderBy('order_id')->get();

        $many = count($rows);
        $console->line("There are [$many] records...");

        DB::beginTransaction();
        $counter = 0;
        foreach ($rows as $row) {
            $counter++;
            $order_id = $row->order_id;
            $order_shipping_number = (string)config('gls.geo.loc') . $row->shipping_number;
            DB::table('orders')->where('id', $order_id)->update(['shipping_number' => $order_shipping_number]);
            $console->line("Setting [$order_shipping_number] to order [$order_id] - $counter/$many");
        }
        DB::commit();
    }


    function bind_families()
    {
        $console = $this->getConsole();
        $charm_category = 31;
        $attribute = \Attribute::whereCode('family')->first();
        $rows = Product::withCategories([$charm_category])->orderBy('sku')->get();

        DB::beginTransaction();
        foreach ($rows as $row) {

            $import = DB::table('import_excel_jewels')->where('sku', $row->sku)->first();
            if ($import and $import->family != '') {
                $console->line("Found product $row->sku | $import->family");
                $option = \AttributeOption::rows('it')->whereAttributeId($attribute->id)->whereName($import->family)->first();
                if ($option) {
                    $row->addAttribute($attribute->id, [$option->id]);
                    $console->info("-> INSERTED");
                }
            }
        }
        DB::commit();
    }


    function extract_charms_images()
    {
        $charm_category = 31;

        $files = [];
        $cmd = 'wget https://www.morellato.com/assets/products/';
        $commands = [];

        $rows = Product::withCategories([$charm_category])->orderBy('sku')->get();
        foreach ($rows as $row) {
            $images = $row->getImages(false);
            foreach ($images as $image) {
                $files[] = $image->filename;
                $commands[] = $cmd . $image->filename;
            }
        }

        print_r($files);
        audit(implode(PHP_EOL, $commands));
    }


    function extract_bases_images()
    {
        $files = [];
        $cmd = 'wget https://www.morellato.com/assets/products/';
        $commands = [];

        $rows = Product::onlyBases()->orderBy('sku')->get();
        foreach ($rows as $row) {
            $images = $row->getImages(false);
            $counter = 1;
            foreach ($images as $image) {
                if ($counter == 1) {
                    $files[] = $image->filename;
                    $commands[] = $cmd . $image->filename;
                }
                $counter++;
            }
        }

        print_r($files);
        audit(implode(PHP_EOL, $commands));
    }


    function make_transparent_charms()
    {
        $console = $this->getConsole();
        $charm_category = 31;
        $rows = Product::withCategories([$charm_category])
            ->orderBy('sku')->get();

        foreach ($rows as $row) {
            $images = $row->getImages(false);
            $many = count($images);
            $console->line("Processing [$many] total items");

            if (!empty($images)) {
                $image = $images[0];
                $this->make_transparent_img($image->filename, $row->sku);
                $console->info("DONE with $row->sku");
            }
        }
    }


    private function make_transparent_img($name, $sku)
    {
        $file = str_replace('.jpg', null, $name);
        $filePath = public_path('assets/products/' . $file . '.jpg');
        $root = public_path('assets/products/droplets');
        if (!is_dir($root)) {
            mkdir($root, 0775);
        }
        $destPath = $root . '/' . $sku . '.png';


        $im = imagecreatefromjpeg($filePath);

        $bg_color = 16777215;
        imagecolortransparent($im, $bg_color);

        imagepng($im, $destPath, 9);

        imagedestroy($im);

    }


    function builder_setup_products()
    {
        $collections = config('builder.collections');
        $charm_category = config('builder.droplet_categories');
        DB::beginTransaction();
        audit_watch();
        //take all charms
        $rows = Product::withCollections($collections)->withCategories($charm_category)->get();
        foreach ($rows as $row) {
            $conditions = ['product_id' => $row->id];
            $data = [
                'product_id' => $row->id,
                'attachment' => 'center',
                'type' => 'droplet',
                'blueprint_id' => null,
            ];
            BuilderProduct::updateOrCreate($conditions, $data);
            $this->getConsole()->line("Product $row->sku inserted as DROPLET");
        }
        //take all bases
        $base_categories = [16, //bracciali
            6, //collane
            2, 3, 8, 9, 10, 11, 12, 19, 20, 21, 22, 25, 44, 51, 75, 78, 82, 84, 85, 86, 102, 111, 112, 113, 114, 115 //watches
        ];
        $rows = Product::withCollections($collections)->withCategories($base_categories)->get();
        foreach ($rows as $row) {
            $conditions = ['product_id' => $row->id];
            $data = [
                'product_id' => $row->id,
                'attachment' => null,
                'type' => 'base',
                'blueprint_id' => 1,
            ];
            BuilderProduct::updateOrCreate($conditions, $data);
            $this->getConsole()->line("Product $row->sku inserted as BASE");
        }
        DB::commit();
    }

    /**
     * @return ConfiguratorRepository
     */
    private function getConfiguratorRepository()
    {
        return \App::make(ConfiguratorRepository::class);
    }

    function builder_test_repository()
    {
        audit_watch();
        $repository = $this->getConfiguratorRepository();
        //$repository->getBases();
    }


    function builder_import_products()
    {
        $console = $this->getConsole();
        $rows = DB::table('import_excel_builder')->get();
        $notFounds = [];
        DB::beginTransaction();
        //audit_watch();
        foreach ($rows as $row) {
            $product = Product::whereSku($row->sku)->first();
            if (is_null($product)) {
                $console->error("Warning: product $row->sku not found");
                $notFounds[] = $row->sku;
                continue;
            }
            if ($row->type == 'base') {
                $conditions = ['product_id' => $product->id];
                $blueprint = BuilderBlueprint::whereCode($row->config_pa)->first();
                $data = [
                    'product_id' => $product->id,
                    'attachment' => null,
                    'type' => 'base',
                    'blueprint_id' => $blueprint->id,
                ];
                BuilderProduct::updateOrCreate($conditions, $data);
                $this->getConsole()->line("Product $row->sku inserted as BASE");
            }
            if ($row->type == 'droplet') {
                $conditions = ['product_id' => $product->id];
                switch ($row->attachment) {
                    case 'T':
                        $attachment = 'top';
                        break;
                    case 'B':
                        $attachment = 'bottom';
                        break;
                    case 'C':
                    default:
                        $attachment = 'center';
                        break;
                }
                $data = [
                    'product_id' => $product->id,
                    'attachment' => $attachment,
                    'type' => 'droplet',
                    'blueprint_id' => null,
                ];
                BuilderProduct::updateOrCreate($conditions, $data);
                $this->getConsole()->line("Product $row->sku inserted as DROPLET");
            }
        }
        DB::commit();
        audit(implode(PHP_EOL, $notFounds));
    }


    function builder_bind_families()
    {
        $console = $this->getConsole();

        $attribute = \Attribute::whereCode('family')->first();
        $rows = Product::onlyDroplets()->orderBy('sku')->get();

        DB::beginTransaction();
        foreach ($rows as $row) {

            $import = DB::table('import_excel_builder')->where('sku', $row->sku)->first();
            if ($import and $import->family != '') {
                $console->line("Found product $row->sku | $import->family");
                $option = \AttributeOption::rows('it')->whereAttributeId($attribute->id)->whereName($import->family)->first();
                if ($option) {
                    $row->addAttribute($attribute->id, [$option->id]);
                    $console->info("-> INSERTED");
                } else {
                    $console->error("Warning: family $import->family not found!");
                }
            }
        }
        DB::commit();
    }


    function builder_setup_slots()
    {

        /*A  x=117 y= 630
B  x=272 y= 692
C  x=393 y= 715
D  x=496 y=720,5
E   x=600 y=715
F  x=723 y= 692
G  x=876  y= 630
*/
        $mode_natural = true;
        $width = 160;
        $height = 160;


        $slot1 = [
            'position' => 1,
            'x' => $mode_natural ? 40 : 37,
            'y' => 550,
            'width' => $width,
            'height' => $height,
        ];

        $slot2 = [
            'position' => 2,
            'x' => $mode_natural ? 160 : 192,
            'y' => $mode_natural ? 600 : 612,
            'width' => $width,
            'height' => $height,
        ];

        $slot3 = [
            'position' => 3,
            'x' => $mode_natural ? 290 : 313,
            'y' => $mode_natural ? 630 : 635,
            'width' => $width,
            'height' => $height,
        ];

        $slot4 = [
            'position' => 4,
            'x' => $mode_natural ? 420 : 416,
            'y' => 640,
            'width' => $width,
            'height' => $height,
        ];

        $slot5 = [
            'position' => 5,
            'x' => $mode_natural ? 550 : 520,
            'y' => $mode_natural ? 630 : 635,
            'width' => $width,
            'height' => $height,
        ];

        $slot6 = [
            'position' => 6,
            'x' => $mode_natural ? 680 : 643,
            'y' => $mode_natural ? 600 : 612,
            'width' => $width,
            'height' => $height,
        ];

        $slot7 = [
            'position' => 7,
            'x' => $mode_natural ? 800 : 796,
            'y' => 550,
            'width' => $width,
            'height' => $height,
        ];

        $slots = [
            $slot1,
            $slot2,
            $slot3,
            $slot4,
            $slot5,
            $slot6,
            $slot7,
        ];

        $model1 = BuilderBlueprint::find(1);
        $model1->params = compact('slots');
        $model1->save();

        unset($slots);

        $slots = [
            $slot1,
            $slot2,
            $slot6,
            $slot7,
        ];

        $model2 = BuilderBlueprint::find(2);
        $model2->params = compact('slots');
        $model2->save();
    }


    public function test_builder_payload()
    {
        $model = BuilderPayload::find(6);
        //$model->makeImage();
        $model->makePdf();
    }


    public function builder_set_exceptions()
    {

        $products = DB::table('products')->whereIn('sku', [
            'SCZ551W',
            'SCZ554W',
            'SCZ617W',
            'SCZ618W',
            'SCZ647W',
            'SCZ650W',
            'SCZ664W',
            'SCZ665W',
            'SCZ700W',
            'SCZ703W',
            'SCZ706W',
            'SCZ708W',
            'SCZ710W',
            'SCZ711W',
            'SCZ712W',
            'SCZ767W',
            'SCZ768W',
            'SCZ769W',
            'SCZ770W',
            'SCZ775W',
            'SCZ777W',
            'SCZ778W',
            'SCZ877W',
            'SCZ881W',
            'SCZ882W',
            'SCZ883W',
            'SCZ884W',
            'SCZ886W',
            'SCZ901W',
            'SCZ902W',
            'SCZ903W',
            'SCZ905W',
            'SCZ906W',
            'SCZ907W',
            'SCZ908W',
            'SCZ910W',
            'SCZ912',
            'SCZ915W',
            'SCZ916W',
            'SCZ918W',
            'SCZ920W',
            'SCZ940W',
            'SCZ952W',
            'SCZ953W',
            'SCZ956W',
            'SCZ960W',
            'SCZ961W',
            'SCZ417W',
            'SCZ418W',
            'SCZ440W',
            'SCZ445W',
            'SCZ912W',
        ])->lists('id');
        $params = ['doubleSize' => true, 'offsetTop' => 40];
        $rows = BuilderProduct::whereIn('product_id', $products)->get();
        foreach ($rows as $row) {
            $row->params = $params;
            $row->save();
        }


        $products = DB::table('products')->whereIn('sku', [
            'SAJT01',
            'SAJT19',
            'SAJT23',
            'SAJT25',
        ])->lists('id');
        $params = ['doubleSize' => true, 'offsetTop' => 0];
        $rows = BuilderProduct::whereIn('product_id', $products)->get();
        foreach ($rows as $row) {
            $row->params = $params;
            $row->save();
        }

    }


    public function builder_rename_files()
    {
        $root = public_path("assets/products/builder/raw/");
        $files = \File::allFiles($root);
        foreach ($files as $file) {
            audit($file);
            $pathName = $file->getPathName();
            $fileName = $file->getFilename();

            if (\Str::contains($pathName, '_8.png')) {

                $newFile = $root . str_replace('_8.PNG', '.png', strtoupper($fileName));
                echo $newFile . PHP_EOL;
                \File::move($pathName, $newFile);
            }

            if (\Str::contains($pathName, '_1.png')) {

                $newFile = $root . str_replace('_1.PNG', '.png', strtoupper($fileName));
                echo $newFile . PHP_EOL;
                \File::move($pathName, $newFile);
            }
        }
    }


    public function builder_missing_top()
    {
        $ids = BuilderProduct::where('type', 'droplet')->where('attachment', 'top')->whereNull('params')->lists('product_id');

        $skus = Product::whereIn('id', $ids)->orderBy('sku')->lists('sku');
        //rint_r($skus);
        echo implode($skus, PHP_EOL);
    }


    public function export_products_attributes()
    {
        $queries = [];
        $rows = DB::table('products_attributes')->where('attribute_id', config('builder.family_attribute_id'))->get();
        foreach ($rows as $row) {
            $pattern = "insert into products_attributes(product_id, attribute_id, attribute_val) values ($row->product_id, $row->attribute_id, $row->attribute_val);";
            echo $pattern . PHP_EOL;
            $queries[] = $pattern;
        }

        $rows = DB::table('products_attributes_position')->where('attribute_id', config('builder.family_attribute_id'))->get();
        foreach ($rows as $row) {
            $pattern = "insert into products_attributes_position(product_id, attribute_id, position) values ($row->product_id, $row->attribute_id, $row->position);";
            echo $pattern . PHP_EOL;
            $queries[] = $pattern;
        }

        file_put_contents(base_path('sources/configuratore.sql'), implode(PHP_EOL, $queries));
    }

    public function affiliate_test_model()
    {
        $model = Affiliate::find(2);
        $console = $this->getConsole();
        $user_id = $model->user->id;
        $shops = $model->shops;
        $console->line("USER ID => $user_id");
        $console->line("SHOPS COUNT => " . count($shops));
    }

    public function test_validator()
    {
        $rules = [
            'postcode_it' => 'required|zip:IT',
            'postcode_fr' => 'required|zip:FR',
            'postcode_hu' => 'required|zip:HU', //ungheria
            'postcode_mt' => 'required|zip:MT', //malta
            'postcode_nl' => 'required|zip:NL', //olanda
            'postcode_pt' => 'required|zip:PT', //portogallo
            'postcode_cz' => 'required|zip:CZ', //Repubblica Ceca
            'postcode_gb' => 'required|zip:GB', //Regno Unito
            'postcode_in' => 'required|zip:IN', //India
            'postcode_at' => 'required|zip:AT', //Austria
            'postcode_hr' => 'required|zip:HR', //Croazia
            'postcode_pl' => 'required|zip:PL', //Polonia
            'postcode_ro' => 'required|zip:RO', //Romania
            'postcode_ch' => 'required|zip:CH', //Svizzera
            'postcode_us' => 'required|zip:US', //Stati Uniti
            'postcode_lv' => 'required|zip:LV', //LV
            'postcode_jp' => 'required|zip:JP', //Japan
            'postcode_ca' => 'required|zip:CA', //Canada
            'vat' => 'required|vat:IT',
        ];

        $data = [
            'postcode_it' => '00052',
            'postcode_fr' => '00052',
            'postcode_hu' => '0052',
            'postcode_mt' => 'KKR1236',
            'postcode_nl' => '2512dv', //'[1-9]{1}\\d{3}[ ]?[A-Z]{2}' /^[1-9][0-9]{3}[\s]?[A-Za-z]{2}$/i
            'postcode_pt' => '7005-610',
            'postcode_cz' => '377 01',
            'postcode_gb' => 'DD2 5PY',
            'postcode_in' => '110 030',
            'postcode_at' => '4784',
            'postcode_hr' => '10000',
            'postcode_pl' => '01-355',
            'postcode_ro' => '700622',
            'postcode_ch' => '4410',
            'postcode_us' => '33195-6503',
            'postcode_lv' => 'LV-4501',
            'postcode_jp' => '770-0003',
            'postcode_ca' => 'J7V 7P2',
            'vat' => '05024270281',
        ];

        $validator = \Validator::make($data, $rules);
        if ($validator->fails()) {
            audit($validator->errors());
            $this->getConsole()->error('VALIDATION FAILED');
        } else {
            $this->getConsole()->info('VALIDATION SUCCESS');
        }
    }


    function fix_sirv()
    {
        $console = $this->getConsole();
        $products = Product::withSource('local')
            ->where('cnt_images', 2)
            //->where('id', 54073)
            ->select('id', 'sku', 'cnt_images')->get();

        $getFileSize = function ($file) {
            $fileName = public_path('assets/products/' . $file);
            if (file_exists($fileName)) {
                $size = filesize($fileName);
                return $size;
            }
            return -1;
        };

        foreach ($products as $product) {

            $images = \ProductImage::where('product_id', $product->id)->get();
            $files = [];
            foreach ($images as $image) {
                $files[] = $image->filename;
            }
            $console->comment("Checking images for product [$product->id]");
            $file1 = $files[0];
            $file2 = $files[1];
            $fileSize1 = $getFileSize($file1);
            $fileSize2 = $getFileSize($file2);

            if ($fileSize1 == $fileSize2) {
                $console->line("$file1 and $file2 have identical size...");

                $wrong = $images[0];
                $correct = $images[1];

                $wrong->cover = 0;
                $wrong->position = 99;
                $wrong->var = 99;
                $wrong->version = 99;
                $wrong->active = 0;
                $wrong->save();

                $correct->cover = 1;
                $correct->position = 1;
                $correct->save();

                \DB::table('products')->where('id', $product->id)->update(['default_img' => $correct->id, 'cnt_images' => 1]);
                $console->info('-> DONE');
            }
        }
    }


    function test_cache()
    {
        audit_watch();
        $console = $this->getConsole();
        $ids = [52577, 52578, 52597, 52596];
        foreach ($ids as $id) {
            $obj = Product::find($id);
            $console->line("Testing product $obj->sku");
            \ProductHelper::generateImagesSetsCacheForProduct($obj);
        }
    }

    function test_metadata()
    {
        audit_watch();
        $console = $this->getConsole();
        $id = 52577;
        $obj = Product::find($id);
        $console->line("Testing product $obj->sku");
        \ProductHelper::handleMetadata($obj);
        $id = 52578;
        $obj = Product::find($id);
        $console->line("Testing product $obj->sku");
        \ProductHelper::handleMetadata($obj);
    }

    function test_mssql()
    {
        $console = $this->getConsole();
        $code = 836;
        $conn = \Core::getNegoziandoConnection();
        $results = $conn->getProductsByShop($code);
        $total = count($results);
        $console->info("Found $total rows for store $code");
        audit($results);
    }

    function test_order_totals()
    {
        $order_ids = [24078, 24079, 24080];
        foreach ($order_ids as $order_id) {
            $order = \Order::find($order_id);
            $this->getConsole()->comment("Calculating totals for order $order_id");
            $order->updateTotals();
        }
    }

    function test_products_wanted()
    {
        $this->getConsole()->comment("Executing products wanted");
        \ProductHelper::setMostWantedProducts();
    }

    function test_products_states()
    {
        $this->getConsole()->comment("Executing products states");
        \ProductHelper::updateProductsStates();
    }

    function extract_product_prices()
    {
        $env = \App::environment();
        $isLocal = $env == 'local';
        $console = $this->getConsole();
        $products = DB::connection('cloud')
            ->table('products')
            ->orderBy('id', 'desc')
            //->take(10)
            ->get();
        $many = count($products);
        $console->line("Found $many products in the cloud...");
        $queries = [];

        if ($isLocal) {

        } else {
            DB::beginTransaction();
        }

        foreach ($products as $product) {
            $fields = [
                'price' => $product->price,
                'sell_price_wt' => $product->sell_price_wt,
                'sell_price' => $product->sell_price,
                'price_nt' => $product->price_nt,
            ];

            if ($isLocal) {
                $query = "UPDATE products SET ";
                foreach ($fields as $key => $value) {
                    $query .= "$key = '$value', ";
                }
                $query = rtrim($query, ', ');
                $query .= " WHERE id=$product->id;";
                //$console->line($query);
                $queries[] = $query;
            } else {
                DB::table('products')->where('id', $product->id)->update($fields);
            }

        }
        $console->info('PRODUCTS DONE');

        $combinations = DB::connection('cloud')
            ->table('products_combinations')
            ->orderBy('id', 'desc')
            //->take(10)
            ->get();
        $many = count($combinations);
        $console->line("Found $many combinations in the cloud...");

        foreach ($combinations as $combination) {
            $fields = [
                'buy_price' => $combination->buy_price,
                'price' => $combination->price,
            ];

            if ($isLocal) {
                $query = "UPDATE products_combinations SET ";
                foreach ($fields as $key => $value) {
                    $query .= "$key = '$value', ";
                }
                $query = rtrim($query, ', ');
                $query .= " WHERE id=$combination->id;";

                $queries[] = $query;
            } else {
                DB::table('products_combinations')->where('id', $combination->id)->update($fields);
            }


        }

        if ($isLocal) {
            \File::put(base_path('sources/logs/sql/reimport.sql'), implode(PHP_EOL, $queries));
        } else {
            DB::commit();
        }

        $console->info('DONE');
    }


    function repair_secure_keys()
    {
        $console = $this->getConsole();
        $rows = DB::select("select secure_key, count(id) as cnt from customers  GROUP BY secure_key HAVING cnt > 1 ORDER BY cnt desc");
        DB::beginTransaction();
        foreach ($rows as $row) {
            $console->comment("Fixing secure_key $row->secure_key, with $row->cnt records");
            $customers = DB::table('customers')->where('secure_key', $row->secure_key)->get();
            foreach ($customers as $customer) {
                $secure_key = \Format::secure_key();
                $console->line("$customer->id => $secure_key");
                DB::table('customers')->where('id', $customer->id)->update(compact('secure_key'));
            }
        }
        DB::commit();
    }


    function repair_b2b_orders()
    {
        $console = $this->getConsole();
        /*$query = "select customer_id, count(id) as cnt from orders where ISNULL(parent_id) group by customer_id HAVING cnt > 1  order by cnt desc";
        $rows = DB::select($query);
        foreach ($rows as $row) {
            $subQuery = "select count(distinct(shipping_address_id)) as cnt from orders where customer_id=$row->customer_id";
            $results = DB::selectOne($subQuery);
            if ($results->cnt > 1) {
                $console->line("$row->customer_id");
            }
        }*/
        $ids = [214, 91, 150, 2332, 128, 152, 4377, 223, 8465, 2389, 5282, 2080, 3911];
        $write = true;
        foreach ($ids as $id) {
            $console->line("== Fixing customer $id ==");
            $orders = DB::table('orders')->where('customer_id', $id)->get();
            foreach ($orders as $order) {
                $shipping = DB::table('address')->find($order->shipping_address_id);
                $console->line("Searching customer with name $shipping->firstname $shipping->lastname");
                $customers = DB::table('customers')->where('active', 1)->where('lastname', $shipping->lastname)->where('firstname', $shipping->firstname)->get();
                $many = count($customers);
                $console->comment("Found ($many) customers");
                foreach ($customers as $customer) {
                    if ($customer->id != $id) {
                        $console->info("Customer to repair => ($customer->name) Customer: $customer->id | Order:$order->id | Shipping: $order->shipping_address_id | Billing: $order->billing_address_id");
                        if ($write) {
                            DB::table('address')->where('id', $order->shipping_address_id)->update(['customer_id' => $customer->id]);
                            DB::table('address')->where('id', $order->billing_address_id)->update(['customer_id' => $customer->id]);
                            DB::table('orders')->where('id', $order->id)->update(['customer_id' => $customer->id]);
                        }
                    }
                }
            }
        }
    }


    function test_remote_api()
    {
        $console = $this->getConsole();
        /**
         * @var $repository RemoteRepository
         */
        $repository = app('services\Remote\Repositories\RemoteRepository');

        $route = 'api/remote/ping';
        $console->comment("Testing API at $route - Single mode (GET)");

        //test direct call
        $response = $repository->toTenant('BS')->get($route)->response();
        $console->info('Response OK');
        $console->info($response);

        //test broadcast
        $console->comment("Testing API at $route - Broadcast mode (POST)");
        $responses = $repository->broadcast()->post($route)->response();
        print_r($responses);

        $route = 'api/remote/json';
        $payload = [
            'foo' => 'bar',
            'bar' => 'foo'
        ];
        $console->comment("Testing API at $route - Single mode (GET)");

        //test direct call
        $response = $repository->setParams($payload)->toTenant('BS')->get($route)->response();
        $console->info('Response OK');
        print_r($response);

        //test broadcast
        $console->comment("Testing API at $route - Broadcast mode (POST)");
        $responses = $repository->setParams($payload)->broadcast()->post($route)->response();
        print_r($responses);
    }


    function test_queue()
    {
        $console = $this->getConsole();
        $console->line('Pushing a new TestJob onto the queue ' . (string)config('cache.prefix'));
        \Queue::push('services\Jobs\TestJob',
            [
                'message' => "This is a test messages created on " . date('Y-m-d H:i:s')
            ],
            config('cache.prefix')
        );
        $console->info('Job successfully pushed');
    }


    function check_db_products()
    {
        $console = $this->getConsole();
        $file = base_path('sources/check_products.txt');
        if (file_exists($file)) {
            $skus = explode(PHP_EOL, file_get_contents($file));
            $rows = DB::connection('live')->table('products')->whereIn('sku', $skus)->select('id', 'sku')->get();
            $ids = [];
            foreach ($rows as $row) {
                $ids[$row->sku] = $row->id;
            }
            foreach ($skus as $sku) {
                $check = isset($ids[$sku]);
                if ($check) {
                    $console->info($sku);
                } else {
                    $console->error($sku);
                }
            }
        }
    }

    function remove_import_products()
    {
        $console = $this->getConsole();
        $file = base_path('sources/remove_products.txt');
        if (file_exists($file)) {
            $skus = explode(PHP_EOL, file_get_contents($file));
            DB::table('import_excel_jewels')->whereIn('sku', $skus)->delete();
        }
    }

    function test_ws2()
    {
        $str = 'a:2:{s:9:"ZT_RETURN";s:0:"";s:8:"ZT_ZVBFA";O:8:"stdClass":1:{s:4:"item";a:3:{i:0;O:8:"stdClass":41:{s:5:"MANDT";s:3:"100";s:5:"VBELV";s:10:"0002717157";s:5:"POSNV";s:6:"000100";s:5:"VBELN";s:10:"0083289221";s:5:"POSNN";s:6:"000010";s:7:"VBTYP_N";s:1:"J";s:5:"RFMNG";s:5:"1.000";s:5:"MEINS";s:2:"ST";s:5:"RFWRT";s:4:"0.00";s:5:"WAERS";s:0:"";s:7:"VBTYP_V";s:1:"C";s:5:"PLMIN";s:1:"+";s:5:"TAQUI";s:0:"";s:5:"ERDAT";s:10:"2018-09-10";s:5:"ERZET";s:8:"11:45:00";s:5:"MATNR";s:10:"FO.FTW2103";s:5:"BWART";s:3:"601";s:5:"BDART";s:0:"";s:5:"PLART";s:0:"";s:5:"STUFE";s:2:"01";s:5:"LGNUM";s:0:"";s:5:"AEDAT";s:10:"0000-00-00";s:5:"FKTYP";s:0:"";s:5:"BRGEW";s:5:"0.000";s:5:"GEWEI";s:0:"";s:5:"VOLUM";s:5:"0.000";s:5:"VOLEH";s:0:"";s:5:"FPLNR";s:0:"";s:5:"FPLTR";s:6:"000000";s:9:"RFMNG_FLO";s:22:"1.0000000000000000E+00";s:9:"RFMNG_FLT";s:22:"1.0000000000000000E+00";s:5:"VRKME";s:2:"ST";s:5:"ABGES";s:22:"0.0000000000000000E+00";s:5:"SOBKZ";s:0:"";s:5:"SONUM";s:0:"";s:5:"KZBEF";s:0:"";s:5:"NTGEW";s:5:"0.000";s:6:"LOGSYS";s:0:"";s:5:"WBSTA";s:0:"";s:5:"CMETH";s:0:"";s:5:"MJAHR";s:4:"0000";}i:1;O:8:"stdClass":41:{s:5:"MANDT";s:3:"100";s:5:"VBELV";s:10:"0083289221";s:5:"POSNV";s:6:"000010";s:5:"VBELN";s:10:"7600005827";s:5:"POSNN";s:6:"000100";s:7:"VBTYP_N";s:1:"M";s:5:"RFMNG";s:5:"1.000";s:5:"MEINS";s:2:"ST";s:5:"RFWRT";s:4:"0.00";s:5:"WAERS";s:3:"EUR";s:7:"VBTYP_V";s:1:"J";s:5:"PLMIN";s:1:"+";s:5:"TAQUI";s:0:"";s:5:"ERDAT";s:10:"2018-09-10";s:5:"ERZET";s:8:"13:26:10";s:5:"MATNR";s:10:"FO.FTW2103";s:5:"BWART";s:0:"";s:5:"BDART";s:0:"";s:5:"PLART";s:0:"";s:5:"STUFE";s:2:"02";s:5:"LGNUM";s:0:"";s:5:"AEDAT";s:10:"0000-00-00";s:5:"FKTYP";s:1:"L";s:5:"BRGEW";s:5:"0.000";s:5:"GEWEI";s:0:"";s:5:"VOLUM";s:5:"0.000";s:5:"VOLEH";s:0:"";s:5:"FPLNR";s:0:"";s:5:"FPLTR";s:6:"000000";s:9:"RFMNG_FLO";s:22:"1.0000000000000000E+00";s:9:"RFMNG_FLT";s:22:"1.0000000000000000E+00";s:5:"VRKME";s:2:"ST";s:5:"ABGES";s:22:"0.0000000000000000E+00";s:5:"SOBKZ";s:0:"";s:5:"SONUM";s:0:"";s:5:"KZBEF";s:0:"";s:5:"NTGEW";s:5:"0.000";s:6:"LOGSYS";s:0:"";s:5:"WBSTA";s:0:"";s:5:"CMETH";s:0:"";s:5:"MJAHR";s:4:"0000";}i:2;O:8:"stdClass":41:{s:5:"MANDT";s:3:"100";s:5:"VBELV";s:10:"0083289221";s:5:"POSNV";s:6:"000000";s:5:"VBELN";s:0:"";s:5:"POSNN";s:6:"000000";s:7:"VBTYP_N";s:1:"8";s:5:"RFMNG";s:5:"0.000";s:5:"MEINS";s:0:"";s:5:"RFWRT";s:4:"0.00";s:5:"WAERS";s:0:"";s:7:"VBTYP_V";s:0:"";s:5:"PLMIN";s:0:"";s:5:"TAQUI";s:0:"";s:5:"ERDAT";s:10:"0000-00-00";s:5:"ERZET";s:8:"00:00:00";s:5:"MATNR";s:12:"L19580081714";s:5:"BWART";s:0:"";s:5:"BDART";s:0:"";s:5:"PLART";s:0:"";s:5:"STUFE";s:2:"00";s:5:"LGNUM";s:0:"";s:5:"AEDAT";s:10:"0000-00-00";s:5:"FKTYP";s:0:"";s:5:"BRGEW";s:5:"0.000";s:5:"GEWEI";s:0:"";s:5:"VOLUM";s:5:"0.000";s:5:"VOLEH";s:0:"";s:5:"FPLNR";s:0:"";s:5:"FPLTR";s:6:"000000";s:9:"RFMNG_FLO";s:22:"0.0000000000000000E+00";s:9:"RFMNG_FLT";s:22:"0.0000000000000000E+00";s:5:"VRKME";s:0:"";s:5:"ABGES";s:22:"0.0000000000000000E+00";s:5:"SOBKZ";s:0:"";s:5:"SONUM";s:0:"";s:5:"KZBEF";s:0:"";s:5:"NTGEW";s:5:"0.000";s:6:"LOGSYS";s:0:"";s:5:"WBSTA";s:0:"";s:5:"CMETH";s:0:"";s:5:"MJAHR";s:4:"0000";}}}}';
        $obj = unserialize($str);
        audit($obj, __METHOD__);
    }

    function test_feats()
    {
        $console = $this->getConsole();
        $console->line("Testing Fees");
        if (feats()->enabled('fees')) {
            $console->info('ENABLED');
        } else {
            $console->comment('DISABLED');
        }
    }

    function test_order_reference()
    {
        $tests = [
            'KRS105299',
            'KRS105297-NG971',
            'KRS105297-01',
            'KRS105270-LC',
            'KRS105158-WM',
            'KRS105058-B',
            'KRS104795-W3683',
            'KRS104308-D',
            'KRS103834-01-C',
            'KRS104075-W3951-',
            'KRS102958-C55',
            'KRS102958-C18',
            'KRS86005-01-F',
        ];

        foreach ($tests as $test) {
            $str = $test . ' => ' . \OrderManager::increaseReference($test);
            $this->getConsole()->line($str);
        }
    }

    function test_receipt_pdf()
    {
        $order_ids = explode(',', '24082,24083,24080,24085,24086,24087,24089,24090,24091');
        $order_ids = explode(',', '24090,24091');
        //$order_ids = explode(',', '24091');
        $orders = \Order::whereIn('id', $order_ids)->get();
        foreach ($orders as $order) {
            $order->createFeeDocumentFile();
            $this->getConsole()->line("PDF generated for order $order->id");
        }
    }

    function test_rma_details()
    {
        $rma = \Rma::find(99);
        $details = $rma->getProductsMatrixDetailsForNewOrder();
        print_r($details);

        if (!empty($details)) {
            $newOrder = \OrderManager::createOrderWithDetails($rma->getOrder(), $details);

            if (is_null($newOrder)) {
                throw new \Exception('Could not create the new order');
            }
        }
    }

    function test_receipt_email()
    {
        $id = 24082;
        $order = \Order::find($id);
        $this->getConsole()->line("Sending email attachment for order $order->id");
        \OrderManager::sendFeeAttachmentByEmail($order, false);
    }

    function test_order_placed()
    {
        $id = $this->getConsole()->ask("Please provide the Order ID");
        $order = \Order::find($id);

        /* @var $orderRepository OrderRepository */
        $orderRepository = app('services\Repositories\OrderRepository');
        $orderRepository->setOrder($order)->orderPlaced();
    }

    function test_order_update_totals()
    {
        $id = 24092;
        /* @var $order \Order */
        $order = \Order::find($id);
        $order->updateTotals();
    }

    function test_week()
    {
        $days = [
            '2018-09-17', //monday
            '2018-09-18', //tueday
            '2018-09-19', //wednesday
            '2018-09-20', //thursday
            '2018-09-21', //friday
            '2018-09-22', //saturday
            '2018-09-23', //sunday
            '2018-09-24', //monday
        ];

        $console = $this->getConsole();

        foreach ($days as $day) {
            $date = Carbon::parse($day);
            $console->comment($day);
            $console->line("dayHuman => " . $date->format('l'));
            $console->line("dayOfWeek => " . $date->dayOfWeek);
            $console->line("weekOfYear => " . $date->weekOfYear);
            $console->comment('------');
        }
    }

    function test_sap_submit()
    {
        audit_watch();
        $rows = \Order::submittableToSap()->get();
    }

    function test_encrypt()
    {
        $password = '6-+pcD2y+*sNn"AR';
        $encrypter = new Encrypter(config('app.key'));
        $secret = $encrypter->encrypt($password);
        $plain = $encrypter->decrypt($secret);
        $this->getConsole()->line('SECRET');
        $this->getConsole()->line($secret);
        $this->getConsole()->line('PLAIN');
        $this->getConsole()->line($plain);
    }

    function test_invoice_increment()
    {
        $console = $this->getConsole();
        $invoice_number = \Core::getInvoiceNumber();
        $console->line("Current invoice_number index: $invoice_number");
        \Core::setInvoiceNumber($invoice_number);
        $invoice_number = \Core::getInvoiceNumber();
        $console->line("After invoice_number index: $invoice_number");
    }

    function fix_empty_orders()
    {
        $query = "SELECT id, order_id, created_at, deleted_at, TIME_TO_SEC(TIMEDIFF(deleted_at, created_at)) as diff 
from order_details 
where !ISNULL(deleted_at) 
and deleted_by = 0 
and YEAR(deleted_at)=2018 
and order_id NOT in (select order_id from order_details where ISNULL(deleted_at))
having diff <= 60 
order by order_id desc;";

        $console = $this->getConsole();

        $rows = DB::select($query);
        $many = count($rows);

        $console->comment("Found [$many] rows to process");

        $counter = 0;

        $orders_updated = [];

        foreach ($rows as $row) {
            $counter++;
            $console->comment("Processing row $counter/$many");
            $order = \Order::find($row->order_id);
            $order_details_id = $row->id;

            if ($order) {
                $console->line("Found match with DETAIL: $order_details_id | ORDER: $order->id");
                DB::table('order_details')->where(['id' => $order_details_id])->update(['deleted_at' => null]);
                $order->updateTotals();
                $console->info('=> DONE');
                $orders_updated[$order->id] = $order->reference;
            }
        }

        print_r($orders_updated);
    }


    function test_b2b_import()
    {
        /* @var StrapsCustomerImporter $importer */
        $importer = app('services\Morellato\Database\StrapsCustomerImporter');
        $importer->make();
    }

    function test_b2b_customers_mailer()
    {
        /* @var StrapsCustomerImporter $importer */
        $importer = app('services\Morellato\Database\StrapsCustomerImporter');
        $importer->sendMailToCustomers();
    }

    function b2b_customers_csv()
    {
        /* @var StrapUserRepository $repository */
        $repository = app('services\Strap\Repositories\StrapUserRepository');
        $repository->generateUsersList();
        $this->getConsole()->info('-> DONE');
    }

    function test_ws2_response()
    {
        /* @var OrderFlowHandler $repository */
        $repository = \App::make('services\Morellato\Soap\Handler\OrderFlowHandler', [$this->getConsole()]);
        $repository->testResponse('KRS112522');
        /*$repository->testResponse('KRS76231');
        $repository->testResponse('KRS88463');
        $repository->testResponse('KRS85509');*/
        $repository->testExportCsv();
        $this->getConsole()->info('-> DONE');
    }

    function test_gls_shipment()
    {
        $bda = '0083345737';
        $mode = 'shop';
        $repository = \App::make(TestOrderHandler::class);
        $repository->testShipment($bda, $mode);
    }

    function test_gls_carrier_shipment()
    {
        $carrier_shipping = '9580125136'; //mid
        $carrier_shipping = '9580126056'; //mid
        //$carrier_shipping = '9580125064'; //full
        $carrier_shipping = '9590058330'; //full
        $repository = \App::make(TestOrderHandler::class);
        $repository->setConsole($this->getConsole());
        $repository->attachConsole();
        $repository->testCarrierShipment($carrier_shipping);
    }

    function test_asset()
    {
        $folder = 'frontend';
        $commands = \Helper::publishAssets($folder);
        print_r($commands);
    }

    function test_map_combinations()
    {
        $id = 590;
        \ProductHelper::mapCombinationsAttributesToMaster($id);
        \ProductHelper::buildProductCache($id, true);
    }

    function test_order_manager()
    {
        $cart_id = 112389;
        \OrderManager::create($cart_id, false);
    }

    function test_cart_repository()
    {
        $cartRepository = new CartRepository();
        $cartRepository->getAbandonedCarts('2018-09-01');
    }

    function test_csvproduct_repository()
    {
        $csvRepository = new CsvProductsRepository();
        $csvRepository->getProducts();
    }

    function fix_bluespirit_products_desc()
    {
        $console = $this->getConsole();
        $bool = $console->confirm("Are you sure you want to execute this?");
        if ($bool !== true) {
            $console->line('Job skipped');
            return;
        }
        $console->line('Executing Job');
        $query = 'select * from products_lang where LENGTH(TRIM(sdesc)) > 0 and LENGTH(TRIM(ldesc)) = 0';
        $rows = DB::select($query);
        DB::beginTransaction();
        foreach ($rows as $row) {
            $where = ['product_id' => $row->product_id, 'lang_id' => $row->lang_id];
            DB::table('products_lang')->where($where)->update([
                'sdesc' => null,
                'ldesc' => $row->sdesc,
            ]);
            $console->line("Updated $row->product_id,$row->lang_id");
        }
        DB::commit();
    }

    function test_gift_card()
    {
        $amount = \Product::where('id', 55917)->pluck('sell_price');
        echo $amount;
    }

    function test_ip_resolver()
    {
        $console = $this->getConsole();
        $ips = [
            'newYork' => '161.185.160.93',
            'germany' => '2.16.192.56',
            'spain' => '4.69.153.169',
            'italyIpv6' => '2001:4c90:DFE1:63::FEFB',
            'SantaMonicaIpv6' => '2001:0:3238:DFE1:63::FEFB',
            'DallasIpv6' => '2607:f0d0:1002:51::4',
        ];
        foreach ($ips as $key => $ip) {
            $console->comment("Resolving record [$key] => $ip");
            $payload = \Site::ip2Location($ip);
            audit($payload, 'RESULT PAYLOAD');
            print_r($payload->toArray());
        }
    }

    function test_unique_id()
    {
        $matrix = [];
        $rows = DB::table('customers')->select(DB::raw('distinct(email)'))->take(5000)->orderBy('id', 'desc')->get();
        foreach ($rows as $row) {//3560529181
            $str = trim($row->email);
            $id = sprintf("%u", crc32(md5($str)));
            if (isset($matrix[$id])) {
                $this->getConsole()->error("Warning: collision for $id");
            } else {
                $this->getConsole()->info("$str => $id");
                $matrix[$id] = $str;
            }
        }
    }

    function test_unique_id_custom()
    {
        $rows = [
            'f.politi@icoa.it',
            'manuel.malaspina@selligent.com',
            'luca.pellegrini@commandersact.com',
            'diego.zerjal@commandersact.com',
            'v.derubeis@kronoshop.com',
        ];

        //md5
        $this->getConsole()->comment("MD5 UNIQUE IDS (string)");
        foreach ($rows as $row) {
            $str = trim($row);
            $id = md5($str);
            $this->getConsole()->info("$str => $id");
        }

        //md5-gnu
        $this->getConsole()->comment("MD5-GMP UNIQUE IDS (integers)");
        foreach ($rows as $row) {
            $str = trim($row);
            $id = sprintf("%u", crc32(md5($str)));
            $this->getConsole()->info("$str => $id");
        }

        //uuid
        $this->getConsole()->comment("UUID UNIQUE IDS (RFC 4211)");
        foreach ($rows as $row) {
            $str = trim($row);
            $id = UUID::v3(UUID::COMMON_NAMESPACE, $str);
            $this->getConsole()->info("$str => $id");
        }
    }

    function test_unique_uuid()
    {
        $matrix = [];
        $rows = DB::table('customers')->select(DB::raw('distinct(email)'))->take(5000)->orderBy('id', 'desc')->get();
        foreach ($rows as $row) {
            //oliva.angelo1975@gmail.com => e176f453-17fa-3554-b7dd-3b8493401a89
            $str = trim($row->email);
            $id = UUID::v3(UUID::COMMON_NAMESPACE, $str);
            if (isset($matrix[$id])) {
                $this->getConsole()->error("Warning: collision for $id");
            } else {
                $this->getConsole()->info("$str => $id");
                $matrix[$id] = $str;
            }
        }
    }


    function test_lucky_number()
    {
        $console = $this->getConsole();
        $name = $console->ask("Please give a name");
        $service = new MyLuckyNumber($name);
        $payload = $service->getPayload();
        print_r($payload);
    }


    function export_order_flows()
    {
        /** @var TestOrderFlow $test */
        $test = \App::make(TestOrderFlow::class);
        $test->setConsole($this->getConsole());
        $test->scanDirectory();
        $test->exportToCsv();
    }


    function test_order_flow()
    {
        audit_watch();
        /** @var TestOrderFlow $test */
        $test = \App::make(TestOrderFlow::class);
        $test->setConsole($this->getConsole());
        $test->scanSingleFile('BSO99787');
        $test->scanSingleFile('BSO99839');
        $test->scanSingleFile('BSO98808');
        $test->scanSingleFile('BSO98507');
    }

    function test_order_flow_call()
    {
        $ids = [25347, 25357, 25540, 25756];
        audit_watch();
        /** @var TestOrderFlow $test */
        $test = \App::make(TestOrderFlow::class);
        $test->setConsole($this->getConsole());
        $test->callWebServiceForCustomOrders($ids);
    }

    function test_best_shop_order_details()
    {
        //$order = \Order::find(25357);
        //$order = \Order::find(25347);
        $order = \Order::find(25540);
        $bestShop = \OrderHelper::findBestShopByOrderDetails($order);
        audit($bestShop, __METHOD__);
    }

    function test_order_collector()
    {
        $ids = [
            25347 => 'Z6',
            25357 => 'Z6',
            25540 => 'Z5',
            25756 => 'Z5',
        ];
        audit_watch();
        /** @var TestOrderFlow $test */
        $test = \App::make(TestOrderFlow::class);
        $test->setConsole($this->getConsole());
        $test->walkCollectorForCustomOrders($ids);
    }


    function test_canvas()
    {
        $tests = [
            '201901',
            '201902',
            '201903',
            '201904',
            '201905',
            '201906',
        ];
        foreach ($tests as $test) {
            $dates = \ProductHelper::parseCanvas($test);
            $this->getConsole()->line("$test => from: {$dates['new_from_date']} | to: {$dates['new_to_date']}");
        }
    }


    function test_ca_refund()
    {
        $repository = new TagCommanderOrderRepository();
        $repository->setStartDate('2018-08-19')->make(true);
    }


    function test_gls_order_handler_tracking()
    {
        $ids = [25347, 25357, 25540, 25756];
        $ids = [25756];
        \Config::set('gls.codes.online|customer', 8657);
        \Config::set('gls.codes.online|shop', 8657);
        print_r(\Config::get('gls.codes'));
        audit_watch();
        /** @var TestOrderHandler $test */
        $test = \App::make(TestOrderHandler::class);
        $test->setConsole($this->getConsole());
        $test->attachConsole();
        $test->testTrackingWithCustomerOrders($ids);
    }


    function test_site_plugins_product()
    {
        audit_watch();
        $markup = <<<HTML
<div class="container">
[blade:product class="product-col col-lg-3 col-md-3 col-sm-6 col-xs-6 flexitem s6 l3 m4 col" category="1" attribute="239,25" limit="2"][/blade:product]

@picture(/media/images/boxes/casio3.jpg)
@picture(/media/images/boxes/casio2.jpg,220)
@picture(/media/images/boxes/casio3.jpg,200,200)
@picture(/media/images/boxes/casio3.jpg"This is the alt")
@picture(/media/images/boxes/casio3.jpg,500"This is the alt")

</div>
HTML;

        $results = \Site::loadPlugins($markup);

        $this->getConsole()->line($results);

    }


    function test_json_importer()
    {
        $importer = \App::make(JsonImporter::class, [$this->getConsole()]);
        $skus = $this->getConsole()->ask('Please provide a list of SKUs (comma separated)');
        $data = explode(',', $skus);
        if (count($data) == 1 and trim($data[0]) == '') {
            $this->getConsole()->error("Insufficient data provided");
            return;
        }
        if (count($data) > 1) {
            foreach ($data as $sku) {
                if (trim($sku) == '') {
                    $this->getConsole()->error("Insufficient data provided");
                    return;
                }
            }
        }
        audit_watch();
        $importer->setLimitSkus($data);
        $importer->make();
    }


    function orphans_json_importer()
    {
        $skus = DB::table('products')->whereRaw("(isnull(brand_id) or ISNULL(collection_id) or ISNULL(default_category_id) or ISNULL(main_category_id)) and ISNULL(deleted_at)")->orderBy('created_at', 'desc')->lists('sku');
        $many = count($skus);
        $this->getConsole()->comment("Found $many products");
        $importer = \App::make(JsonImporter::class, [$this->getConsole()]);
        $importer->setLimitSkus($skus);
        $importer->make();
    }


    function test_fork_order()
    {
        $order_id = $this->getConsole()->ask('Please provide a valid Order ID');

        audit_watch();
        $order = \Order::find($order_id);

        if ($order) {
            $this->getConsole()->comment("Forking Order $order->reference");
            \OrderManager::forkOrder($order);
        } else {
            $this->getConsole()->error("Could not find any Order with the given ID");
        }
    }


    function test_order_scope_slip_details()
    {
        audit_watch();
        $orders = \Order::withOrderSlipDetailType(OrderSlipDetail::TYPE_SOLD)->get();
    }


    function test_order_scope_customer_warning()
    {
        audit_watch();
        $orders = \Order::customerWarnableMissedPickup()->get();
    }


    function test_order_scope_customer_package_ready()
    {
        audit_watch();
        $orders = \Order::customerWarnablePackageReady()->get();
    }


    function test_order_scope_shop_warnable_carrier_shipment()
    {
        audit_watch();
        $orders = \Order::shopWarnableCarrierShipment()->get();
    }


    function test_order_scope_migrationable_to_working()
    {
        audit_watch();
        $orders = \Order::migrationableToWorking()->get();
    }


    function test_order_scope_submittable_to_sap()
    {
        audit_watch();
        $orders = \Order::submittableToSap()->get();
    }


    function test_order_scope_submittable_to_sap_order_flow()
    {
        audit_watch();
        $orders = \Order::submittableToSapOrderFlow()->get();
    }


    function test_order_scope_migrationable_to_closed()
    {
        audit_watch();
        $orders = \Order::migrationableToClosed()->get();
    }


    function test_order_tokens()
    {
        $console = $this->getConsole();
        $order_id = $console->ask('Please provide an order ID');
        /** @var Order $order */
        $order = Order::getObj($order_id);
        $tokens = $order->getTokens();
        audit($tokens, 'TOKENS');
    }


    function test_order_collector_consumer()
    {
        /** @var OrderCollectorService $collector */
        $collector = OrderCollectorService::getInstance();

        //simulate various labels for the Collection
        $order = Order::getObj(25770);
        $collector->pushWithLabel(OrderSlipDetail::REASON_SHOP_SHIPPED, $order);


        /** @var OrderFlowHandler $handler */
        $handler = \App::make(OrderFlowHandler::class, [$this->getConsole()]);
        $handler->executePostActions();
    }


    function test_order_eloquent_logger()
    {
        $order = Order::getObj(25770);
        $order->logger()->log('This is a custom log');
        $order->logger()->info(['foo' => 'bar'], 'This is a custom log with info markup');
    }


    function test_order_eloquent_logger_status()
    {
        /** @var Order $order */
        $order = Order::getObj(25770);
        $order->setStatus(\OrderState::STATUS_NEW, true, 'Test #1');
        $order->setPaymentStatus(\PaymentState::STATUS_COMPLETED);
        $order->setStatus(\OrderState::STATUS_SHIPPED, true, 'Test #2');

        $order = Order::getObj(25769);
        $order->setStatus(\OrderState::STATUS_SHIPPED, true, 'Test #3');
        $order->setStatus(\OrderState::STATUS_WORKING, true, 'Test #4');
    }


    function generate_google_auth_secret()
    {
        $g = new GoogleAuthenticator();
        $secret = $g->generateSecret();
        $this->getConsole()->info("Secret generated: $secret");
    }


    function test_google_auth_encrypt()
    {
        $token = 'U6Q34H4WHUNANAD7';
        $fa_token = 'eyJpdiI6IjZHNFNHWTJ6bFpUdG5xU0JzUk5IY1E9PSIsInZhbHVlIjoibWVpd0pUK1pzb01xVFAzS3RqZEdTXC9PT0puWEk2bFk4ZXZ0Y25uTEJUVXc9IiwibWFjIjoiNWVlMTY4N2FlNDZhYjUwODYxMDk4NGQ3N2MyZmE2ZjEzODQwN2E4YmFmMmEyNzVjNjJjZDk3ODg2ZWI4NzEwZCJ9';
        $encrypter = new Encrypter(config('app.key'));
        $secret = $encrypter->encrypt($token);
        $plain = $encrypter->decrypt($secret);
        $this->getConsole()->line('SECRET');
        $this->getConsole()->line($secret);
        $this->getConsole()->line('PLAIN');
        $this->getConsole()->line($plain);
    }


    function scp()
    {
        $console = $this->getConsole();
        $relativePath = $console->ask('Insert the relative path for the file');
        $path = base_path($relativePath);
        if (!file_exists($path)) {
            $console->error("Could not produce commands, the file at $path does not exist on this tenant");
            return;
        }

        $roots = [
            'KS' => 'f_politi_m_icoa_it@ecommerce.google:/workarea/www/production/l4/',
            'BS' => 'f_politi_m_icoa_it@bluespirit.google:/workarea/www/bluespirit/l4/',
            'MR' => 'f_politi_m_icoa_it@morellato.gce:/workarea/www/morellato/l4/',
            'SC' => 'f_politi_m_icoa_it@sector.gce:/workarea/www/sector/l4/',
            'PW' => 'f_politi_m_icoa_it@pw.gce:/workarea/www/philipwatch/l4/',
            'ST' => 'f_politi_m_icoa_it@cinturino.google:/workarea/www/cinturino/l4/',
            'LP' => 'f_politi_m_icoa_it@lps.google:/workarea/www/lepetitstory/l4/',
        ];

        $template = "pscp -i C:\\Users\\Fabio\\Documents\\PPK\\ecommerce.ppk %s %s";

        $path = str_replace('/', "\\", $path);

        foreach ($roots as $root) {
            $cmd = sprintf($template, $path, $root . $relativePath);
            $console->line($cmd);
        }
    }


    function lcp()
    {
        $console = $this->getConsole();
        $relativePath = $console->ask('Insert the relative path for the file');
        $path = base_path($relativePath);
        if (!file_exists($path)) {
            $console->error("Could not produce commands, the file at $path does not exist on this tenant");
            return;
        }

        $roots = [
            'BS' => 'D:/wamp/www/l4.bluespirit/',
            'MR' => 'D:/wamp/www/l4.morelweb/',
            'SC' => 'D:/wamp/www/l4.sector/',
            'PW' => 'D:/wamp/www/l4.philipwatch/',
            'ST' => 'D:/wamp/www/l4.cinturino/',
            'LP' => 'D:/wamp/www/l4.lepetitstory/',
        ];

        foreach ($roots as $root) {
            $destination = $root . $relativePath;
            $console->line("Copying $path => $destination");
            \File::copy($path, $destination);
        }
    }


    function test_mail_blacklist()
    {
        $emails = [
            '2231971625@qq.com',
            'fabio.politi.dev@gmail.com',
        ];

        $console = $this->getConsole();
        foreach ($emails as $email) {
            $console->line("Testing email $email");
            $valid = \Utils::isEmail($email);
            if ($valid) {
                $console->info('-> OK');
            } else {
                $console->error('-> KO');
            }
        }
    }


    function test_order_status_change_shipped()
    {
        $console = $this->getConsole();
        $customer = \Customer::where('email', 'like', '%politi%')->first();
        /** @var \Order $order */
        $order = \Order::where('customer_id', $customer->id)
            ->withoutStatus(\OrderState::STATUS_SHIPPED)
            ->withCarrier(\Carrier::TYPE_NATIONAL_DELIVERY)
            ->orderBy('id', 'desc')
            ->first();
        $carrier = $order->carrierName();
        $console->comment("Found Order $order->id for customer $customer->email and carrier $carrier");
        $console->line("Current status: " . $order->statusName(false));
        $console->comment('Setting status to STATUS_SHIPPED');
        $order->setStatus(\OrderState::STATUS_SHIPPED);
    }


    function test_product_scope_trends()
    {
        audit_watch();
        /** @var Product[] $products */
        $products = Product::withTrends([19, 39])->take(5)->get();
        foreach ($products as $product) {
            $trends = $product->getTrendsCodesAttribute();
            $this->getConsole()->line($trends);
        }
    }


    function test_customer_accessors()
    {
        $console = $this->getConsole();
        $id = $console->ask('Customer ID?');
        /** @var \Customer $customer */
        $customer = \Customer::find($id);
        if ($customer) {
            $attributes = [
                'name',
                'card_code',
                'card_barcode_url',
                'card_html',
                'account_panel_url',
            ];
            foreach ($attributes as $attribute) {
                $value = $customer->getAttribute($attribute);
                $console->line("[$attribute] => $value");
            }
        } else {
            $console->error('Customer not found');
        }
    }


    function test_order_sendmail_with_status()
    {
        $console = $this->getConsole();
        $id = $console->ask('Order ID?');
        /** @var \Order $order */
        $order = \Order::find($id);
        if ($order) {
            $status = $order->statusName();
            $status_code = \OrderState::statusToString($order->status);
            $console->line("Testing order[$order->reference] with status: $status and status_code: $status_code");
            $test = $order->handleStatusUpdateEmail(true);
            ($test) ? $console->info('POSITIVE') : $console->error('NEGATIVE');

            $email = $order->getCustomer()->email;

            $change_status = $console->confirm("Should I force the status to SHIPPED? (Warning: an email will be sent to $email)");
            if ($change_status) {
                \Config::set('mail.pretend', false);
                $console->line("Changing status to SHIPPED...");
                $order->setStatus(\OrderState::STATUS_SHIPPED);
                $console->info('DONE');
            }
        } else {
            $console->error('Order not found');
        }
    }


    function test_photoslurp_module_json()
    {
        $console = $this->getConsole();
        $ids_str = $console->ask('Insert module(s) separated by a comma(,)');
        $ids = explode(',', $ids_str);
        foreach ($ids as $id) {
            $module = \Module::getObj($id);
            $frontend_module = new \Frontend\Module($module->id);
            $console->line("Generating JSON for module [$module->id] $module->name");
            $photoslurp = new PhotoslurpPlugin();
            $jsCode = $photoslurp->makeJson($frontend_module);
            print $jsCode . PHP_EOL;
        }
    }


    function test_product_price_rule()
    {
        $console = $this->getConsole();
        /** @var \PriceRule $priceRule */
        $priceRule = \PriceRule::where('repeatable', 1)->orderBy('id', 'desc')->first();
        $console->line("Testing price rule #{$priceRule->id}");
        /** @var Product $product */
        $product = Product::withPriceRule($priceRule->id)->orderBy('id', 'desc')->first();
        $console->line("Testing on product #{$product->id} - {$product->sku}");
        //\Product::forgetPriceRulesById($product->id);
        $console->comment('Calling setPrices');
        $product->setPrices();

        $attributes = [
            "price_official",
            "price_final",
            "price_saving",
            "price_label",
            "price_official_raw",
            "price_saving_raw",
            "price_final_raw",
            "price_offer",
            "price_currency_id",
        ];
        foreach ($attributes as $attribute) {
            $console->info("$attribute => " . $product->getAttribute($attribute));
        }
        audit($product->toArray(), __METHOD__);
    }

    function test_product_related_images()
    {
        $console = $this->getConsole();
        $product_id = $console->ask('Enter product id');
        $product = Product::getObj($product_id);
        if (null === $product) {
            $console->error('Cannot find any product');
            exit();
        }

        audit_watch();
        \ProductHelper::getRelatedImagesForProducts($product_id);
    }

    function test_intervention_image_logo()
    {
        $console = $this->getConsole();
        $config = \Cfg::getMultiple("DEFAULT_LOGO,DEFAULT_LOGO_ALT,DEFAULT_LOGO_TITLE");

        $logo = $config['DEFAULT_LOGO'];

        $imagePath = public_path() . $logo;
        $console->line($imagePath);

        $img = \Image::make($imagePath);
        $h = $img->height();
        $w = $img->width();
        $ext = $img->extension;
    }

    function test_intervention_image_webp()
    {
        $console = $this->getConsole();


        $imagePath = public_path('media/user.jpg');
        $console->line($imagePath);

        /** @var Image $img */
        $img = \Image::make($imagePath);
        $height = $img->height();
        $width = $img->width();
        $ext = $img->extension;
        $mime = $img->mime();
        print_r(compact('width', 'height', 'ext', 'mime'));
        $img->sharpen(10)->save(base_path('sources/test_images/user.webp'), 80, 'webp');

        $savedImg = \Image::make(base_path('sources/test_images/user.webp'));
        $height = $img->height();
        $width = $img->width();
        $ext = $img->extension;
        $mime = $img->mime();
        print_r(compact('width', 'height', 'ext', 'mime'));
    }

    protected function test_uuid()
    {
        $console = $this->getConsole();
        $max = 1000;
        $items = [];
        for ($i = 0; $i < $max; $i++) {
            //$uuid = \Utils::uuid(true);
            $uuid = \Format::secure_key();
            $console->line("$i => $uuid");
            if (isset($items[$uuid])) {
                $items[$uuid]++;
            } else {
                $items[$uuid] = 1;
            }
        }

        $total = array_sum($items);

        if ($total === $max) {
            $console->info('LOOKS GOOD TO ME');
        } else {
            $console->error('WARNING: THERE ARE DUPLICATES');
        }
    }

    protected function test_gls_address_api()
    {
        audit(__METHOD__);
        $console = $this->getConsole();
        /** @var AddressXmlReader $handler */
        $handler = app(AddressXmlReader::class);
        $handler->test();
        $console->info('DONE');
    }

    protected function test_order_tracking_from_order_history()
    {
        $console = $this->getConsole();
        audit_watch();

        $console->info('Getting random OrderHistories');
        $rows = \OrderHistory::onlyOrder()->take(3)->orderByRaw('rand()')->get();
        foreach ($rows as $row) {
            $console->line("Testing OrderHistory [$row->id]");
            $order_tracking = OrderTracking::fromOrderHistory($row);
            print_r($order_tracking->toArray());
        }

        $console->info('Getting OrderHistories from Order children');
        $order_childred_ids = Order::onlyChildren()->take(3)->orderByRaw('rand()')->lists('id');
        $rows = \OrderHistory::onlyOrder()->withOrder($order_childred_ids)->take(3)->orderByRaw('rand()')->get();
        foreach ($rows as $row) {
            $console->line("Testing OrderHistory [$row->id]");
            $order_tracking = OrderTracking::fromOrderHistory($row);
            print_r($order_tracking->toArray());
        }

        $console->info('Getting OrderHistories from Order with shops');
        $order_childred_ids = Order::withDeliveryStore()->withAvailabilityStore()->take(3)->orderByRaw('rand()')->lists('id');
        $rows = \OrderHistory::onlyOrder()->withOrder($order_childred_ids)->take(3)->orderByRaw('rand()')->get();
        foreach ($rows as $row) {
            $console->line("Testing OrderHistory [$row->id]");
            $order_tracking = OrderTracking::fromOrderHistory($row);
            print_r($order_tracking->toArray());
        }
    }

    protected function test_order_tracking_from_soft_event()
    {
        $console = $this->getConsole();
        audit_watch();
        $order = Order::whereYear('created_at', '=', date('Y') - 1)->orderByRaw('rand()')->first();
        $console->line("Migrating soft status for order: $order->id");
        $order->setSoftStatus(
            OrderState::STATUS_DELIVERED,
            'Migrazione automatica in base a meccaniche GLS con matrice: 906.online.shop',
            OrderTracking::byGls(OrderTracking::REASON_SHOP_CONFIRM)
        );
    }
}