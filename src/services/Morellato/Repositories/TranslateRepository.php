<?php

namespace services\Morellato\Repositories;

use DB, App, Config, Utils;
use Illuminate\Filesystem\Filesystem;
use Str;
use Lexicon;
use Attribute;
use AttributeOption;
use Category;
use Collection;
use Brand;
use Page;
use Module;
use Product;
use Trend;
use Menu;
use services\Morellato\Grabbers\GoogleTranslateGrabber;
use services\Morellato\Grabbers\GoogleTranslateNodeGrabber;


class TranslateRepository extends Repository
{

    /**
     * @var Filesystem
     */
    protected $files;

    /**
     * @var GoogleTranslateGrabber
     */
    protected $grabber;


    protected $lang = 'it';

    protected $translate_lang = ['en', 'es', 'fr'];

    public function __construct(Filesystem $files, GoogleTranslateNodeGrabber $grabber)
    {
        $this->files = $files;
        $this->grabber = $grabber;
    }


    function interactive()
    {
        $console = $this->getConsole();
        $text = $console->ask('Please provide a text to translate');
        $from = $console->choice('Please the source language', ['it', 'en', 'es', 'fr', 'de'], 'it');
        $console->comment("Translating '$text' from [$from]");
        $translations = [];
        foreach ($this->translate_lang as $lang) {
            $console->line("Translating into [$lang]");
            $results = $this->grabber->reset()->addItem($text)->translate($lang);
            $translations[$lang] = ['name' => $results[0]];
        }
        print_r($translations);
    }


    function non_interactive()
    {
        $console = $this->getConsole();
        $text = '<div style="text-align: justify;">Gli<strong> orecchini da uomo</strong> sono un accessorio che d&agrave; un tocco di carattere e stile a diversi tipi di look. Sebbene oggi siano accessori prettamente <strong>femminili</strong>, la storia dice che non &egrave; stato sempre cos&igrave;, e oggi non &egrave; affatto difficile incontrare uomini che li indossano. Nella societ&agrave; occidentale gli <strong>orecchini da uomo</strong> venivano abitualmente indossati all&#39;orecchio sinistro, seguendo una tradizione gi&agrave; presente nel XIX secolo nella marina mercantile inglese, mentre si indossavano sul destro nella marina militare, sempre in Inghilterra. I pirati portavano l&#39;orecchino per &quot;catturare la luce&quot;. Nei luoghi con scarsa visibilit&agrave;, per esempio sotto coperta, l&#39;<strong>orecchino in oro</strong> rifletteva la luce di una qualsiasi fonte e il pirata poteva essere localizzato. Su <strong>Bluespirit</strong> potrete trovare un&rsquo;ampia gamma di modelli di <strong>orecchini da uomo</strong>, che riceverete completi di confezione originale e garanzia del prodotto. <strong>Bluespirit</strong> &egrave; infatti <strong>rivenditore ufficiale</strong> di tutti i marchi presenti nel proprio catalogo. La storia degli <strong>orecchini</strong> &egrave; un racconto millenario. Essi sono infatti <a href="https://www.bluespirit.com/gioielli-C4.htm"><strong>gioielli </strong></a>utilizzati dalla maggior parte delle civilt&agrave;. In Egitto ad esempio, nacquero principalmente come <strong>ornamento maschile</strong> per indicare un elevato status sociale. Solo successivamente incominciarono ad attrarre un pubblico pi&ugrave; vasto. L&rsquo;amore per gli orecchini non &egrave; solo una questione di stile e<strong> moda</strong>, ma racchiude in s&eacute; anche significati e simboli pi&ugrave; profondi. Oggi gli uomini indossano l&rsquo;orecchino per seguire uno stile particolare, ma in passato l&rsquo;<strong>orecchino maschile</strong> aveva significati molto profondi e poteva essere addirittura simbolo d&rsquo;amore. In passato infatti, condividere lo stesso orecchino con il proprio partner era un&rsquo;usanza legata alla credenza che vedeva uniti in un legame inscindibile l&rsquo;uomo e la donna che indossavano lo stesso orecchino. I <strong>gioielli </strong>che oggi invece vengono usati come simbolo d&rsquo;amore sono gli <strong>anelli di fidanzamento</strong> e le <strong>fedine</strong>. Per chi cerca uno stile pi&ugrave; casual, la<a href="https://www.bluespirit.com/orecchini-sector-sharp-uomo-C17B16N1497G1.htm"><strong> linea Sharp</strong></a> della <strong>Sector </strong>offre <strong>orecchini da uomo in acciaio</strong> e con cristalli, adatti a tutti coloro che sanno scegliere i dettagli che fanno la differenza. Per chi invece preferisce gli <strong>orecchini in oro</strong>, la collezione <strong>Bluespirit</strong> con la <a href="https://www.bluespirit.com/orecchini-bluespirit-b-classic-uomo-C17B139N1821G1.htm"><strong>linea B-Classic </strong></a>offre diversi modelli e soluzioni adatte ad ogni look. Che siano in oro, <strong>argento</strong> o <strong>acciaio</strong>, offriamo un&rsquo;ampia scelta di orecchini (e non solo) per lui e per lei, perfetti anche come<a href="https://www.bluespirit.com/idee-regalo.html"><strong> idee regalo</strong></a> per le occasioni speciali o solo per una semplice sorpresa. <strong>Bluespirit</strong> ti offre tante promozioni per acquistare <strong>orologi </strong>e<strong> gioielli</strong> ai prezzi pi&ugrave; convenienti del web.</div>';
        $translations = [];
        foreach ($this->translate_lang as $lang) {
            $console->line("Translating into [$lang]");
            $results = $this->grabber->reset()->addItem($text)->translate($lang);
            $translations[$lang] = ['name' => $results[0]];
        }
        print_r($translations);
        audit($translations);
    }


    public function files()
    {
        $files = [
            'ajax.php',
            'rma.php',
            'template.php',
        ];

        foreach ($files as $file) {
            $translations = include app_path("lang/{$this->lang}/$file");
            //print_r($translations);
            $this->grabber->reset();
            foreach ($translations as $key => $value) {
                if (is_string($value))
                    $this->grabber->addItem($value);
            }
            $target_lang = 'de';
            $results = $this->grabber->translate($target_lang);
            //print_r($results);
            $file_array = [];
            $index = 0;
            foreach ($translations as $key => $value) {
                if (isset($results[$index]))
                    $file_array[$key] = $results[$index];

                $index++;
            }
            $target_file = app_path("lang/{$target_lang}/$file");
            $this->files->put($target_file, '<?php return ' . var_export($file_array, true) . ";\n");
        }

    }


    public function lexicon()
    {

        $console = $this->getConsole();

        $rows = Lexicon::rows('it')
            ->whereIn('id', [486, 487, 488, 489, 490, 491])
            ->get();

        \Utils::watch();

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]");
            foreach ($this->translate_lang as $lang) {
                $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                $translations[$lang] = ['name' => $results[0]];
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    public function attributes()
    {
        $console = $this->getConsole();

        $rows = Attribute::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('attributes_lang');
        $avoid = ['name', 'slug', 'lang_id', 'attribute_id'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]");
            foreach ($this->translate_lang as $lang) {
                $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                $name = $results[0];
                $tooltip_label = null;
                if (strlen($row->tooltip_label) > 0) {
                    $tooltip_label = $this->grabber->reset()->addItem($row->tooltip_label)->translate($lang)[0];
                }
                $tooltip_value = null;
                if (strlen($row->tooltip_value) > 0) {
                    $tooltip_value = $this->grabber->reset()->addItem($row->tooltip_value)->translate($lang)[0];
                }
                $translations[$lang] = ['name' => $name, 'slug' => Str::slug($name)];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
                $translations[$lang]['tooltip_label'] = $tooltip_label;
                $translations[$lang]['tooltip_value'] = $tooltip_value;
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    public function attributes_options()
    {
        $console = $this->getConsole();

        $rows = AttributeOption::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('attributes_options_lang');
        $avoid = ['name', 'slug', 'lang_id', 'attribute_option_id'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]");
            foreach ($this->translate_lang as $lang) {
                $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                $name = mb_ucfirst($results[0]);
                $translations[$lang] = ['name' => $name, 'slug' => Str::slug($name)];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    public function brands()
    {
        $console = $this->getConsole();

        $rows = \Brand::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('brands_lang');
        $avoid = ['name', 'slug', 'lang_id', 'brand_id', 'h1', 'metatitle', 'metakeywords', 'metadescription'];
        $reparses = ['h1', 'metatitle', 'metakeywords', 'metadescription'];
        $htmls = [];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]($row->id)");
            foreach ($this->translate_lang as $lang) {
                $lang_id = $lang;
                $brand_id = $row->id;
                $lang_record = DB::table('brands_lang')->where(compact('lang_id', 'brand_id'))->first();

                //existing record
                if ($lang_record) {
                    $translations[$lang] = [];
                    foreach ($htmls as $html) {
                        if (\Str::length(trim($lang_record->$html)) == 0 and \Str::length(trim($row->$html)) > 0) {
                            //translate HTML desc
                            $results = $this->grabber->reset()->addItem($row->$html)->translate($lang);
                            $translations[$lang][$html] = ($results[0]);
                        }
                    }
                    foreach ($reparses as $reparse) {
                        if (\Str::length(trim($lang_record->$reparse)) == 0 and \Str::length($row->$reparse) > 0) {
                            $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                            $name = mb_ucfirst($results[0]);
                            $translations[$lang][$reparse] = $name;
                        }
                    }
                } else {
                    $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                    $name = mb_ucfirst($results[0]);
                    $translations[$lang] = ['name' => $name, 'slug' => Str::slug($name)];
                    foreach ($columns as $column) {
                        $translations[$lang][$column] = $row->$column;
                    }
                    foreach ($reparses as $reparse) {
                        if (strlen($row->$reparse) > 0) {
                            $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                            $name = mb_ucfirst($results[0]);
                            $translations[$lang][$reparse] = $name;
                        }
                    }
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    public function collections()
    {
        $console = $this->getConsole();

        $rows = \Collection::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('collections_lang');
        $avoid = ['name', 'slug', 'lang_id', 'collection_id', 'h1', 'metatitle', 'metakeywords', 'metadescription'];
        $reparses = ['h1', 'metatitle', 'metakeywords', 'metadescription'];
        $htmls = [];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]($row->id)");
            foreach ($this->translate_lang as $lang) {
                $lang_id = $lang;
                $collection_id = $row->id;
                $lang_record = DB::table('collections_lang')->where(compact('lang_id', 'collection_id'))->first();

                //existing record
                if ($lang_record) {
                    $translations[$lang] = [];
                    foreach ($htmls as $html) {
                        if (\Str::length(trim($lang_record->$html)) == 0 and \Str::length(trim($row->$html)) > 0) {
                            //translate HTML desc
                            $results = $this->grabber->reset()->addItem($row->$html)->translate($lang);
                            $translations[$lang][$html] = ($results[0]);
                        }
                    }
                    foreach ($reparses as $reparse) {
                        if (\Str::length(trim($lang_record->$reparse)) == 0 and \Str::length($row->$reparse) > 0) {
                            $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                            $name = mb_ucfirst($results[0]);
                            $translations[$lang][$reparse] = $name;
                        }
                    }
                } else {
                    $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                    $name = mb_ucfirst($results[0]);
                    $translations[$lang] = ['name' => $name, 'slug' => Str::slug($name)];
                    foreach ($columns as $column) {
                        $translations[$lang][$column] = $row->$column;
                    }
                    foreach ($reparses as $reparse) {
                        if (strlen($row->$reparse) > 0) {
                            $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                            $name = mb_ucfirst($results[0]);
                            $translations[$lang][$reparse] = $name;
                        }
                    }
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    /*public function categories()
    {
        $console = $this->getConsole();

        $rows = Category::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('categories_lang');
        $avoid = ['name', 'slug', 'lang_id', 'category_id', 'singular', 'h1', 'metatitle', 'metakeywords', 'metadescription'];
        $reparses = ['singular', 'h1', 'metatitle', 'metakeywords', 'metadescription'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);
        DB::beginTransaction();
        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]");
            foreach ($this->translate_lang as $lang) {
                $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                $name = mb_ucfirst($results[0]);
                $translations[$lang] = ['name' => $name, 'slug' => Str::slug($name)];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
                foreach ($reparses as $reparse) {
                    if (strlen($row->$reparse) > 0) {
                        $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                        $name = mb_ucfirst($results[0]);
                        $translations[$lang][$reparse] = $name;
                    }
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
        DB::commit();
    }*/


    public function categories()
    {
        $console = $this->getConsole();

        $rows = \Category::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('categories_lang');
        $avoid = ['name', 'slug', 'lang_id', 'category_id', 'h1', 'metatitle', 'metakeywords', 'metadescription'];
        $reparses = ['singular', 'h1', 'metatitle', 'metakeywords', 'metadescription'];
        $htmls = ['ldesc'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]($row->id)");
            foreach ($this->translate_lang as $lang) {
                $lang_id = $lang;
                $category_id = $row->id;
                $lang_record = DB::table('categories_lang')->where(compact('lang_id', 'category_id'))->first();

                //existing record
                if ($lang_record) {
                    $translations[$lang] = [];
                    foreach ($htmls as $html) {
                        if (\Str::length(trim($lang_record->$html)) == 0 and \Str::length(trim($row->$html)) > 0) {
                            //translate HTML desc
                            $results = $this->grabber->reset()->addItem($row->$html)->translate($lang);
                            $translations[$lang][$html] = ($results[0]);
                        }
                    }
                    foreach ($reparses as $reparse) {
                        if (\Str::length(trim($lang_record->$reparse)) == 0 and \Str::length($row->$reparse) > 0) {
                            $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                            $name = mb_ucfirst($results[0]);
                            $translations[$lang][$reparse] = $name;
                        }
                    }
                } else {
                    $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                    $name = mb_ucfirst($results[0]);
                    $translations[$lang] = ['name' => $name, 'slug' => Str::slug($name)];
                    foreach ($columns as $column) {
                        $translations[$lang][$column] = $row->$column;
                    }
                    foreach ($reparses as $reparse) {
                        if (strlen($row->$reparse) > 0) {
                            $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                            $name = mb_ucfirst($results[0]);
                            $translations[$lang][$reparse] = $name;
                        }
                    }
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    public function products()
    {
        $console = $this->getConsole();

        $rows = Product::rows('it')
            ->orderBy('product_id', 'desc')
            ->get();


        $columns = DB::getSchemaBuilder()->getColumnListing('products_lang');
        $avoid = ['lang_id', 'product_id'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);
        audit_watch();

        DB::beginTransaction();
        $reparses = ['sdesc', 'ldesc'];
        $reparses = [];

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name] - ID: $row->id");
            foreach ($this->translate_lang as $lang) {
                $translations[$lang] = [];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
                $name = \ProductHelper::craftProductName($row->id, $lang);
                $translations[$lang]['name'] = trim($name);
                $translations[$lang]['slug'] = Str::slug(trim($name));
                try {
                    foreach ($reparses as $reparse) {
                        if (strlen($row->$reparse) > 0) {
                            $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                            $name = mb_ucfirst($results[0]);
                            $translations[$lang][$reparse] = $name;
                        }
                    }
                } catch (\Exception $e) {
                    $this->getConsole()->error($e->getMessage());
                    DB::commit();
                    throw $e;
                }
            }
            $row->make($translations, false, false);
            DB::table('products_lang')
                ->where('product_id', $row->id)
                ->whereIn('lang_id', $this->translate_lang)
                ->update([
                    'h1' => null,
                    'attributes' => null,
                    'indexable' => null,
                    'metatitle' => null,
                    'metakeywords' => null,
                    'metadescription' => null,
                    'metadata' => $row->metadata,
                ]);
        }

        DB::commit();
    }


    public function tables()
    {
        $console = $this->getConsole();

        /*$rows = \StockReason::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('stock_reasons_lang');
        $avoid = ['name', 'slug', 'lang_id', 'stock_reason_id'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]");
            foreach ($this->translate_lang as $lang) {
                $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                $name = mb_ucfirst( $results[0] );
                $translations[$lang] = ['name' => $name];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
            }
            $row->make($translations);
        }*/


        $rows = \RmaOption::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('rma_options_lang');
        $avoid = ['name', 'slug', 'lang_id', 'rma_option_id'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]");
            foreach ($this->translate_lang as $lang) {
                $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                $name = mb_ucfirst($results[0]);
                $translations[$lang] = ['name' => $name];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
            }
            //print_r($translations);
            $row->make($translations);
        }


        $rows = \RmaState::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('rma_states_lang');
        $avoid = ['name', 'slug', 'lang_id', 'rma_state_id'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]");
            foreach ($this->translate_lang as $lang) {
                $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                $name = mb_ucfirst($results[0]);
                $translations[$lang] = ['name' => $name];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
            }
            //print_r($translations);
            $row->make($translations);
        }


        $rows = \PaymentState::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('payment_states_lang');
        $avoid = ['name', 'slug', 'lang_id', 'payment_state_id'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]");
            foreach ($this->translate_lang as $lang) {
                $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                $name = mb_ucfirst($results[0]);
                $translations[$lang] = ['name' => $name];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
            }
            //print_r($translations);
            $row->make($translations);
        }


        $rows = \OrderState::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('order_states_lang');
        $avoid = ['name', 'slug', 'lang_id', 'order_state_id'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]");
            foreach ($this->translate_lang as $lang) {
                $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                $name = mb_ucfirst($results[0]);
                $translations[$lang] = ['name' => $name];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    public function emails()
    {
        $console = $this->getConsole();

        $rows = \Email::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('emails_lang');
        $avoid = ['subject', 'lang_id', 'email_id'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        DB::beginTransaction();
        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->subject]");
            foreach ($this->translate_lang as $lang) {
                $results = $this->grabber->reset()->addItem($row->subject)->translate($lang);
                $name = mb_ucfirst($results[0]);
                $translations[$lang] = ['subject' => $name];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
        DB::commit();
    }


    public function image_groups()
    {
        $console = $this->getConsole();

        $rows = \GroupImage::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('images_groups_lang');
        $avoid = ['lang_id', 'image_group_id'];
        $columns = array_diff($columns, $avoid);

        DB::beginTransaction();
        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->group_image_id]");
            foreach ($this->translate_lang as $lang) {
                $translations[$lang] = [];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
            }
            //print_r($translations);
            $row->make($translations, false, false);
        }
        DB::commit();
    }


    public function images()
    {
        $console = $this->getConsole();

        $rows = \ProductImage::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('images_lang');
        $avoid = ['lang_id', 'product_image_id'];
        $columns = array_diff($columns, $avoid);

        DB::beginTransaction();
        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->product_image_id]");
            foreach ($this->translate_lang as $lang) {
                $translations[$lang] = [];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
            }
            //print_r($translations);
            $row->make($translations, false, false);
        }
        DB::commit();
    }


    public function magic_tokens()
    {
        $console = $this->getConsole();

        $rows = \MagicTokenTemplate::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('magic_tokens_templates_lang');
        $avoid = ['lang_id', 'magic_token_template_id'];
        $columns = array_diff($columns, $avoid);

        DB::beginTransaction();
        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->content]");
            foreach ($this->translate_lang as $lang) {
                $translations[$lang] = [];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
        DB::commit();
    }


    public function message_templates()
    {
        $console = $this->getConsole();

        $rows = \MessageTemplate::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('message_templates_lang');
        $avoid = ['name', 'slug', 'lang_id', 'message_template_id', 'message'];
        $reparses = ['message'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]");
            foreach ($this->translate_lang as $lang) {
                $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                $name = mb_ucfirst($results[0]);
                $translations[$lang] = ['name' => $name];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
                foreach ($reparses as $reparse) {
                    if (strlen($row->$reparse) > 0) {
                        $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                        $name = mb_ucfirst($results[0]);
                        $translations[$lang][$reparse] = $name;
                    }
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    public function payments()
    {
        $console = $this->getConsole();

        $rows = \Payment::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('payments_lang');
        $avoid = ['name', 'slug', 'lang_id', 'payment_id', 'carttext', 'aftertext'];
        $reparses = ['carttext', 'aftertext'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]");
            foreach ($this->translate_lang as $lang) {
                $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                $name = mb_ucfirst($results[0]);
                $translations[$lang] = ['name' => $name];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
                foreach ($reparses as $reparse) {
                    if (strlen($row->$reparse) > 0) {
                        $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                        $name = mb_ucfirst($results[0]);
                        $translations[$lang][$reparse] = $name;
                    }
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    public function seo_blocks()
    {
        $console = $this->getConsole();

        $rows = \SeoBlock::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('seo_blocks_lang');
        $avoid = ['lang_id', 'seo_block_id'];
        $columns = array_diff($columns, $avoid);
        $reparses = ['content'];

        $languages = $this->translate_lang;
        $stop_matrix = [];
        foreach ($languages as $language) {
            $stop_matrix[$language] = DB::table('seo_blocks_lang')->where('lang_id', $language)->lists('seo_block_id');
        }

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->content]");
            foreach ($this->translate_lang as $lang) {

                if (in_array($row->seo_block_id, $stop_matrix[$lang])) {
                    $this->getConsole()->comment("Skipping [$lang] for $row->seo_block_id");
                } else {
                    $translations[$lang] = [];
                    foreach ($columns as $column) {
                        $translations[$lang][$column] = $row->$column;
                    }
                    foreach ($reparses as $reparse) {
                        if (strlen($row->$reparse) > 0) {
                            $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                            $name = mb_ucfirst($results[0]);
                            $translations[$lang][$reparse] = $this->cleanupTranslateResult($name);
                        }
                    }
                }
            }
            $row->make($translations);
        }
    }


    public function seo_texts()
    {
        $console = $this->getConsole();

        $rows = \SeoText::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('seo_texts_lang');
        $avoid = ['lang_id', 'seo_text_id', 'h1', 'content', 'metatitle', 'metakeywords', 'metadescription'];
        $reparses = ['seo_text_id', 'h1', 'content', 'metatitle', 'metakeywords', 'metadescription'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);
        $this->translate_lang = $this->translate_lang + ['en'];
        $total_langs = count($this->translate_lang);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->id]");

            //check existing for all langs

            $totals = DB::table('seo_texts_lang')->whereIn('lang_id', $this->translate_lang)->where('seo_text_id', $row->seo_text_id)->count();
            if ($totals == $total_langs) {
                $console->comment("Skipping since it is completed");
                continue;
            }

            foreach ($this->translate_lang as $lang) {

                $translations[$lang] = [];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
                foreach ($reparses as $reparse) {
                    if (strlen($row->$reparse) > 0) {
                        $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                        $name = mb_ucfirst($results[0]);
                        $translations[$lang][$reparse] = $name;
                    }
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    public function menus()
    {
        $console = $this->getConsole();

        $rows = Menu::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('menus_lang');
        $avoid = ['name', 'slug', 'lang_id', 'menu_id', 'subtitle'];
        $reparses = ['subtitle'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);
        $specials = ['Lista autom.', 'Contenitore'];
        $cache = [];
        $total_langs = count($this->translate_lang);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]($row->id)");

            //check existing for all langs
            $totals = DB::table('menus_lang')->whereIn('lang_id', $this->translate_lang)->where('menu_id', $row->menu_id)->count();
            if ($totals == $total_langs) {
                $console->comment("Skipping since it is completed");
                continue;
            }

            foreach ($this->translate_lang as $lang) {
                $cache_key = $row->name . '|' . $lang;
                $given_name = null;
                if (isset($cache[$cache_key])) {
                    $given_name = $cache[$cache_key];
                }
                if (in_array($row->name, $specials)) {
                    $given_name = $row->name;
                }
                if (is_null($given_name)) {
                    $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                    $name = mb_ucfirst($results[0]);
                    $cache[$cache_key] = $name;
                } else {
                    $name = $given_name;
                }

                $translations[$lang] = ['name' => $name];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
                foreach ($reparses as $reparse) {
                    if (strlen($row->$reparse) > 0) {
                        $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                        $name = mb_ucfirst($results[0]);
                        $translations[$lang][$reparse] = $name;
                    }
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    public function carriers()
    {
        $console = $this->getConsole();

        $rows = \Carrier::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('carriers_lang');
        $avoid = ['name', 'slug', 'lang_id', 'carrier_id', 'carttext', 'delay'];
        $reparses = ['carttext', 'delay'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]");
            foreach ($this->translate_lang as $lang) {
                $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                $name = mb_ucfirst($results[0]);
                $translations[$lang] = ['name' => $name];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
                foreach ($reparses as $reparse) {
                    if (strlen($row->$reparse) > 0) {
                        $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                        $name = mb_ucfirst($results[0]);
                        $translations[$lang][$reparse] = $name;
                    }
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    public function banners()
    {
        $console = $this->getConsole();

        $rows = \Banner::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('banners_lang');
        $avoid = ['name', 'slug', 'lang_id', 'banner_id', 'input_alt'];
        $reparses = ['input_alt', 'input_textarea'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);
        $cache = [];

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->banner_id]");
            foreach ($this->translate_lang as $lang) {

                $translations[$lang] = [];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
                foreach ($reparses as $reparse) {
                    if (strlen($row->$reparse) > 0) {

                        $cache_key = $row->$reparse . '|' . $lang;
                        $given_name = null;
                        if (isset($cache[$cache_key])) {
                            $given_name = $cache[$cache_key];
                        }

                        if (is_null($given_name)) {
                            $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                            $name = mb_ucfirst($results[0]);
                        } else {
                            $name = $given_name;
                        }

                        $translations[$lang][$reparse] = $name;
                    }
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    public function pages()
    {
        $console = $this->getConsole();

        $rows = \Page::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('pages_lang');
        $avoid = ['name', 'slug', 'lang_id', 'page_id', 'h1', 'metatitle', 'metakeywords', 'metadescription'];
        $reparses = ['h1', 'metatitle', 'metakeywords', 'metadescription'];
        $htmls = ['ldesc', 'ldesc_mobile'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]");
            foreach ($this->translate_lang as $lang) {
                $lang_id = $lang;
                $page_id = $row->id;
                $lang_record = DB::table('pages_lang')->where(compact('lang_id', 'page_id'))->first();

                //existing record
                if ($lang_record) {
                    $translations[$lang] = [];
                    foreach ($htmls as $html) {
                        if (\Str::length(trim($lang_record->$html)) == 0 and \Str::length(trim($row->$html)) > 0) {
                            //translate HTML desc
                            $results = $this->grabber->reset()->addItem($row->$html)->translate($lang);
                            $translations[$lang][$html] = ($results[0]);
                        }
                    }
                    foreach ($reparses as $reparse) {
                        if (\Str::length(trim($lang_record->$reparse)) == 0 and \Str::length($row->$reparse) > 0) {
                            $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                            $name = mb_ucfirst($results[0]);
                            $translations[$lang][$reparse] = $name;
                        }
                    }
                } else {
                    $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                    $name = mb_ucfirst($results[0]);
                    $translations[$lang] = ['name' => $name, 'slug' => Str::slug($name)];
                    foreach ($columns as $column) {
                        $translations[$lang][$column] = $row->$column;
                    }
                    foreach ($reparses as $reparse) {
                        if (strlen($row->$reparse) > 0) {
                            $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                            $name = mb_ucfirst($results[0]);
                            $translations[$lang][$reparse] = $name;
                        }
                    }
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    public function trends()
    {
        $console = $this->getConsole();

        $rows = \Trend::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('trends_lang');
        $avoid = ['name', 'slug', 'lang_id', 'trend_id', 'h1', 'metatitle', 'metakeywords', 'metadescription'];
        $reparses = ['h1', 'metatitle', 'metakeywords', 'metadescription'];
        $htmls = [];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]($row->id)");
            foreach ($this->translate_lang as $lang) {
                $lang_id = $lang;
                $trend_id = $row->id;
                $lang_record = DB::table('trends_lang')->where(compact('lang_id', 'trend_id'))->first();

                //existing record
                if ($lang_record) {
                    $translations[$lang] = [];
                    foreach ($htmls as $html) {
                        if (\Str::length(trim($lang_record->$html)) == 0 and \Str::length(trim($row->$html)) > 0) {
                            //translate HTML desc
                            $results = $this->grabber->reset()->addItem($row->$html)->translate($lang);
                            $translations[$lang][$html] = ($results[0]);
                        }
                    }
                    foreach ($reparses as $reparse) {
                        if (\Str::length(trim($lang_record->$reparse)) == 0 and \Str::length($row->$reparse) > 0) {
                            $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                            $name = mb_ucfirst($results[0]);
                            $translations[$lang][$reparse] = $name;
                        }
                    }
                } else {
                    $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                    $name = mb_ucfirst($results[0]);
                    $translations[$lang] = ['name' => $name, 'slug' => Str::slug($name)];
                    foreach ($columns as $column) {
                        $translations[$lang][$column] = $row->$column;
                    }
                    foreach ($reparses as $reparse) {
                        if (strlen($row->$reparse) > 0) {
                            $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                            $name = mb_ucfirst($results[0]);
                            $translations[$lang][$reparse] = $name;
                        }
                    }
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    public function modules()
    {
        $console = $this->getConsole();

        $rows = \Module::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('modules_lang');
        $avoid = ['name', 'lang_id', 'module_id', 'input', 'input_alt', 'input_textarea', 'input_textarea_alt', 'input_text', 'input_text_alt'];
        $reparses = ['input', 'input_alt', 'input_textarea', 'input_textarea_alt', 'input_text', 'input_text_alt'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);
        $total_langs = count($this->translate_lang);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]");

            //check existing for all langs
            $totals = DB::table('modules_lang')->whereIn('lang_id', $this->translate_lang)->where('module_id', $row->module_id)->count();
            if ($totals == $total_langs) {
                $console->comment("Skipping since it is completed");
                continue;
            }

            foreach ($this->translate_lang as $lang) {
                $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                $name = mb_ucfirst($results[0]);
                $translations[$lang] = ['name' => $name];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
                foreach ($reparses as $reparse) {
                    if (strlen($row->$reparse) > 0) {
                        $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                        $name = mb_ucfirst($results[0]);
                        $translations[$lang][$reparse] = $name;
                    }
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    public function navs()
    {
        $console = $this->getConsole();

        $rows = \Nav::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('navs_lang');
        $avoid = ['name', 'slug', 'lang_id', 'nav_id', 'h1', 'metatitle', 'metakeywords', 'metadescription'];
        $reparses = ['h1', 'metatitle', 'metakeywords', 'metadescription'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]");
            foreach ($this->translate_lang as $lang) {
                $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                $name = mb_ucfirst($results[0]);
                $translations[$lang] = ['name' => $name, 'slug' => Str::slug($name)];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
                foreach ($reparses as $reparse) {
                    if (strlen($row->$reparse) > 0) {
                        $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                        $name = mb_ucfirst($results[0]);
                        $translations[$lang][$reparse] = $name;
                    }
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    public function months()
    {
        $console = $this->getConsole();

        $rows = DB::table("months")
            ->join('months_lang', 'id', '=', 'month_id')
            ->where('lang_id', '=', 'it')
            ->orderBy('position')->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('months_lang');
        $avoid = ['name', 'short', 'lang_id', 'month_id'];
        $columns = array_diff($columns, $avoid);
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]");
            foreach ($this->translate_lang as $lang) {
                $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                $name = mb_ucfirst($results[0]);
                $translations[$lang] = ['name' => $name, 'short' => substr($name, 0, 3)];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
                DB::table('months_lang')->insert([
                    'month_id' => $row->month_id,
                    'lang_id' => $lang,
                    'name' => $translations[$lang]['name'],
                    'short' => $translations[$lang]['short'],
                ]);
            }
            print_r($translations);

        }
    }


    public function cart_rules()
    {
        $console = $this->getConsole();

        $rows = \CartRule::rows('it')
            ->get();

        $columns = DB::getSchemaBuilder()->getColumnListing('cart_rules_lang');
        $avoid = ['name', 'description', 'image_content'];
        $reparses = ['carttext'];
        $columns = array_diff($columns, $avoid);
        $this->translate_lang = $this->translate_lang + ['en'];
        //print_r($columns);

        foreach ($rows as $row) {
            $translations = [];
            $console->line("Translating [$row->name]");
            foreach ($this->translate_lang as $lang) {
                $results = $this->grabber->reset()->addItem($row->name)->translate($lang);
                $name = mb_ucfirst($results[0]);
                $translations[$lang] = ['name' => $name];
                foreach ($columns as $column) {
                    $translations[$lang][$column] = $row->$column;
                }
                foreach ($reparses as $reparse) {
                    if (strlen($row->$reparse) > 0) {
                        $results = $this->grabber->reset()->addItem($row->$reparse)->translate($lang);
                        $name = mb_ucfirst($results[0]);
                        $translations[$lang][$reparse] = $name;
                    }
                }
            }
            //print_r($translations);
            $row->make($translations);
        }
    }


    public function fill_products()
    {

        $console = $this->getConsole();

        $products = Product::select('id')->get();
        $many = count($products);
        $console->comment("Found [$many] products to process");

        $counter = 0;

        foreach ($products as $product) {
            $counter++;
            $console->line("Processing product [$product->id] ($counter/$many)");

            DB::beginTransaction();
            try {
                \ProductHelper::buildProductCache($product->id, true);

                $seo_fields = \ProductHelper::generateSeo($product->id, true);

                foreach ($seo_fields as $locale => $data) {
                    $update = [
                        'metatitle' => $data['title'],
                        'h1' => $data['h1'],
                        'metakeywords' => $data['keywords'],
                        'metadescription' => $data['description'],
                    ];
                    DB::table('products_lang')->where('product_id', $product->id)->where('lang_id', $locale)->update($update);
                }
                $console->info("Updated SEO for product [$product->id]");

            } catch (\Exception $e) {
                $console->error($e->getMessage());
                $console->error($e->getTraceAsString());
            }
            DB::commit();

        }
    }


    public function desc_products()
    {

        $console = $this->getConsole();

        $products = \Product_Lang::where('lang_id', 'it')
            ->get();
        $many = count($products);
        $console->comment("Found [$many] products to process");

        $counter = 0;
        $languages = $this->translate_lang + ['en'];
        $fields = ['sdesc', 'ldesc'];

        foreach ($products as $product) {
            $counter++;
            $console->line("Processing product [$product->product_id] ($counter/$many)");

            DB::beginTransaction();
            try {
                foreach ($languages as $lang) {
                    $product_id = $product->product_id;
                    $lang_id = $lang;
                    $exists = DB::table('products_lang')->where(compact('product_id', 'lang_id'))->count();
                    $params = [];

                    foreach ($fields as $field) {
                        $value = trim($product->$field);
                        if (\Str::length($value) > 0) {
                            $results = $this->grabber->reset()->addItem($value)->translateHtml($lang);
                            $params[$field] = $results[0];
                        }
                    }

                    if (!empty($params)) {
                        if ($exists > 0) {
                            DB::table('products_lang')->where(compact('product_id', 'lang_id'))->update($params);
                            $console->info("Updated $product_id [$lang_id]");
                        } else {
                            $params['product_id'] = $product_id;
                            $params['lang_id'] = $lang_id;
                            DB::table('products_lang')->insert($params);
                            $console->info("Inserted $product_id [$lang_id]");
                        }
                    }

                }


            } catch (\Exception $e) {
                $console->error($e->getMessage());
                $console->error($e->getTraceAsString());
            }
            DB::commit();

        }
    }


    public function refill()
    {
        $console = $this->getConsole();

        $tables = [
            //'cart_rules_lang' => 'cart_rule_id',
            //'emails_lang' => 'email_id',
            //'lexicons_lang' => 'lexicon_id',
            //'menus_lang' => 'menu_id',
            //'modules_lang' => 'module_id',
            //'pages_lang' => 'page_id',
            //'sections_lang' => 'section_id',
            'trends_lang' => 'trend_id',
        ];

        foreach ($tables as $table => $pivot) {
            $console->comment("Processing table [$table]");
            $columns = DB::getSchemaBuilder()->getColumnListing($table);
            $rows = DB::table($table . '_copy')->where('lang_id', '<>', 'it')->orderBy($pivot)->get();
            DB::beginTransaction();
            foreach ($rows as $row) {
                $lang_id = $row->lang_id;

                $params = compact('lang_id');
                $params[$pivot] = $row->$pivot;
                $query_params = $params;
                foreach ($columns as $column) {
                    $params[$column] = $row->$column;
                }

                $record = DB::table($table)->where($query_params)->first();
                if ($record) { //existing record
                    DB::table($table)->where($query_params)->update($params);
                    $console->info("Updated {$params[$pivot]} [$lang_id]");
                } else { //non-existing
                    DB::table($table)->where($query_params)->insert($params);
                    $console->info("Inserted {$params[$pivot]} [$lang_id]");
                }
            }
            DB::commit();
        }
    }


    public function refill_images()
    {
        $console = $this->getConsole();
        $rows = DB::table('images')->whereNotIn('id', function ($query) {
            $query->select('product_image_id')->from('images_lang');
        })->get();

        $languages = $this->translate_lang + ['en', 'it'];
        $table = 'images_lang';

        DB::beginTransaction();
        foreach ($rows as $row) {
            foreach ($languages as $lang_id) {
                $query_params = [
                    'product_image_id' => $row->id,
                    'lang_id' => $lang_id
                ];
                $params = [
                    'product_image_id' => $row->id,
                    'lang_id' => $lang_id,
                    'legend' => null,
                    'published' => 1,
                ];
                $record = DB::table($table)->where($query_params)->first();
                if ($record) { //existing record
                    $console->info("Skipped {$row->id} [$lang_id]");
                } else { //non-existing
                    DB::table($table)->insert($params);
                    $console->info("Inserted {$row->id} [$lang_id]");
                }
            }
        }
        DB::commit();
    }


    public function republish()
    {
        $console = $this->getConsole();

        $languages = $this->translate_lang + ['en'];
        $tables = [
            'pages_lang' => 'page_id',
            'modules_lang' => 'module_id',
            'products_lang' => 'product_id'
        ];

        DB::beginTransaction();
        foreach ($tables as $table => $column) {
            $console->comment("Processing table [$table]");
            $ids = DB::table($table)->where('published', 1)->where('lang_id', 'it')->lists($column);
            DB::table($table)->whereIn('lang_id', $languages)->whereIn($column, $ids)->update(['published' => 1]);

            $ids = DB::table($table)->where('published', 0)->where('lang_id', 'it')->lists($column);
            DB::table($table)->whereIn('lang_id', $languages)->whereIn($column, $ids)->update(['published' => 0]);
        }
        DB::commit();
    }


    public function config()
    {
        $console = $this->getConsole();

        $rows = DB::table('config_lang')->where('lang_id', 'it')->get();
        $languages = $this->translate_lang + ['en'];

        DB::beginTransaction();
        foreach ($rows as $row) {
            $text = $row->prod_value;
            $cfg_key = $row->cfg_key;
            foreach ($languages as $lang_id) {
                $results = $this->grabber->reset()->addItem($text)->translate($lang_id);
                $prod_value = $results[0];

                $existing = DB::table('config_lang')->where(compact('cfg_key', 'lang_id'))->first();

                if ($existing) {
                    DB::table('config_lang')->where(compact('cfg_key', 'lang_id'))->update(compact('prod_value'));
                    $console->info("Updated $cfg_key ($lang_id");
                } else {
                    $params = compact('cfg_key', 'lang_id', 'prod_value');
                    DB::table('config_lang')->insert($params);
                    $console->info("Inserted $cfg_key ($lang_id");
                }
            }

        }
        DB::commit();
    }


    public function covers()
    {
        $rows = \ProductImage::where('cover', 1)->get();
        DB::beginTransaction();
        foreach ($rows as $row) {
            $this->getConsole()->line("Updating [$row->product_id]");
            DB::table('products')->where('id', $row->product_id)->update(['default_img' => $row->id]);
        }
        DB::commit();
    }


    public function fix_missing_images_lang()
    {
        $console = $this->getConsole();
        $query = "select * from images where id not in (select product_image_id from images_lang where lang_id='it')";
        $rows = DB::select($query);
        $many = count($rows);
        $counter = 0;
        DB::beginTransaction();
        foreach ($rows as $row) {
            $counter++;
            $console->line("Image $counter/$many");
            DB::table('images_lang')->insert([
                'product_image_id' => $row->id,
                'lang_id' => 'it',
                'legend' => null,
                'published' => 1,
            ]);
        }
        DB::commit();
    }


    public function test_grabber()
    {
        $str1 = "Attenzione: non è possibile procedere con l\'ordine per un problema interno; il carrello risulta essere vuoto! Per favore ricontrolla il tuo carrello ed esegui nuovamente il processo di acquisto.";
        $str2 = "Prodotti";
        $str3 = "Registrati :here";
        $translations = $this->grabber->addItem($str1)->addItem($str2)->addItem($str3)->translate('es');
        print_r($translations);
    }


    public function options_en()
    {
        $query = "select * from attributes_options_lang where lang_id='it' and attribute_option_id not in (select attribute_option_id from attributes_options_lang where attribute_option_id and lang_id='en')";
        $rows = DB::select($query);
        DB::beginTransaction();
        foreach ($rows as $row) {
            $this->getConsole()->line("Updating [$row->name]");
            $results = $this->grabber->reset()->addItem($row->name)->translate('en');
            $name = mb_ucfirst($results[0]);
            $slug = Str::slug($name);
            $data = [
                'attribute_option_id' => $row->attribute_option_id,
                'lang_id' => 'en',
                'name' => $name,
                'slug' => $slug,
            ];
            $option = DB::table('attributes_options_lang')->where(['lang_id' => 'en', 'attribute_option_id' => $row->attribute_option_id,])->first();
            if (is_null($option)) {
                DB::table('attributes_options_lang')->insert($data);
            }
        }
        DB::commit();
    }


    function collections_en()
    {
        $rows = DB::select("select * from collections_lang where lang_id='it' and collection_id not in (select collection_id from collections_lang where lang_id='en')");
        DB::beginTransaction();
        foreach ($rows as $row) {
            $data = [
                'lang_id' => 'en',
                'name' => $row->name,
                'collection_id' => $row->collection_id,
                'slug' => $row->slug,
                'published' => $row->published,
            ];
            DB::table('collections_lang')->insert($data);
        }
        DB::commit();
    }


    private function cleanupTranslateResult($text)
    {
        $searches = ['&quot;', ' before = ', ': ', 'avant = ', 'antes = ', 'anterior = ', 'después = ', 'après = '];
        $replaces = ['"', ' before=', ':', 'before=', 'before=', 'before=', 'after=', 'after='];
        $text = str_replace($searches, $replaces, $text);
        return trim($text);
    }


    function testWrap()
    {
        $content = '<a href="https://www.bluespirit.com"><img src="https://www.bluespirit.com/media/images/pages/img_bs_404_home.jpg" /></a>
<h1>4<i class="fa fa-life-bouy fa-spin fa-fw"></i>4</h1>
<h2>Pagina non trovata</h2>
<p><strong>Attenzione:</strong> la risorsa che stai cercando non esiste od è stata rimossa o spostata.</p>
<p>Per favore utilizza il motore di ricerca che trovi in alto a destra, oppure utilizza uno dei seguenti link suggeriti</p>
<ul>
    <li>[blade:link to="home"]Torna all\'homepage[/blade:link]</li>
    <li>[blade:link to="catalog"]Sfoglia il catalogo prodotti[/blade:link]</li>
    <li>[blade:link to="page-12"]Vai alla pagina dei contatti[/blade:link]</li>
    <li>[blade:link to="sitemap"]Consulta la nostra mappa del sito[/blade:link]</li>
    <li>[blade:link to="page-16"]Hai bisogno di informazioni? Clicca qui[/blade:link]</li>
</ul>';
        echo $this->grabber->wrapSpecialTags($content);
    }


    function testUnwrap()
    {
        $content = '<div class="inline sep-left">
<span class="notranslate">[blade:link to="page-177"]</span><i class="fa fa-map-marker"></i> Store <span class="notranslate">[/blade:link]</span>
</div>
<div class="inline">
<span class="hidden-sm">Numero verde</span><span class="hidden-lg hidden-md"><i class="fa fa-phone"></i></span> <strong>800 669 712</strong>
</div>';
        echo $this->grabber->unwrapSpecialTags($content);
    }
}