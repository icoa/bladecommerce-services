<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 01/08/2017
 * Time: 11:19
 */

namespace services\Morellato\Repositories;

use Carbon\Carbon;
use Core\Condition\RuleResolver;
use DB, App, Config, Utils;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Encryption\Encrypter;
use Illuminate\Filesystem\Filesystem;
use Laravel\File;
use Order;
use OrderHelper;
use OrderState;
use services\Bluespirit\Importers\Excel\ExcelImporter;
use services\Bluespirit\Importers\Excel\ExcelRepository;
use services\Membership\Models\Affiliate;
use services\Models\BuilderBlueprint;
use services\Models\BuilderPayload;
use services\Models\BuilderProduct;
use services\Models\OrderSlip;
use services\Models\OrderSlipDetail;
use services\Morellato\Gls\Xml\Reader\CarrierShipmentXmlReader;
use services\Morellato\Gls\Xml\Reader\ShipmentXmlReader;
use services\Remote\Repositories\RemoteRepository;
use services\Repositories\CartRuleRepository;
use services\Repositories\ConfiguratorRepository;
use services\Repositories\OrderFeedRepository;
use services\Repositories\OrderRepository;
use Str;
use services\IpResolver;
use services\Morellato\CsvImporters\AnagraficaImporter;
use services\Morellato\Geo\AddressResolver;
use services\Morellato\Grabbers\ImageGrabber;
use Product;
use League\Csv\Writer;
use League\Csv\Reader;
use services\Bluespirit\Importers\Excel\WatchesExcelImporter;
use services\Bluespirit\Importers\Excel\JewelsExcelImporter;
use services\Bluespirit\Importers\Excel\StrapsExcelImporter;
use Illuminate\Support\Facades\Schema;

class ToolsRepository extends Repository
{

    protected $files;
    protected $errors = [];

    public function __construct(Filesystem $files)
    {
        $this->files = $files;
    }


    private function _getCountry($code)
    {
        $row = \DB::table('countries')->where('iso_code', $code)->first();
        return $row;
    }

    private function _getState($code, $country_id)
    {
        $row = \DB::table('states')->where('iso_code', $code)->where('country_id', $country_id)->first();
        return $row;
    }


    private function runStates()
    {
        $rows = \DB::table('import_iso')->orderBy('country')->orderBy('state')->orderBy('name')->get();
        $now = date('Y-m-d H:i:s');

        \DB::beginTransaction();
        foreach ($rows as $row) {
            $country = $this->_getCountry($row->country);
            if ($country) {
                $state = $this->_getState($row->state, $country->id);
                if ($state) {
                    $this->info("Found state [$row->state | $row->name]");
                    \DB::table('states')->where('id', $state->id)->update(['name' => $row->name]);
                } else {
                    $this->comment("Non found state [$row->state | $row->name]");
                    $data = [
                        'name' => $row->name,
                        'iso_code' => $row->state,
                        'country_id' => $country->id,
                        'active' => 1,
                        'zone_id' => $country->zone_id,
                        'created_at' => $now,
                        'updated_at' => $now,
                    ];
                    \DB::table('states')->insert($data);
                }
            } else {
                $this->error("Country not found [$row->country]");
            }
        }
        \DB::commit();
    }


    protected function import_text()
    {
        $this->description('it');
        $this->description('en');
        print_r($this->errors);
    }

    protected function description($lang)
    {
        $this->getConsole()->comment("Importing product description");
        $sourceDir = storage_path('import/descriptions/' . $lang);
        $files = $this->files->allFiles($sourceDir);
        //print_r($files);
        $debug = false;
        DB::beginTransaction();
        foreach ($files as $file) {
            try {
                $ext = $file->getExtension();
                $fileName = $file->getFilename();
                $canImport = true;
                if ($debug) {
                    $canImport = false;
                    if ('TLJ933.json' == $fileName or 'TLJ933.txt' == $fileName) {
                        $canImport = true;
                    }
                }
                if ($canImport) {
                    $content = $this->files->get($sourceDir . '/' . $fileName);
                    if ($ext == 'txt') {
                        $this->importDescriptionHtml($fileName, $content, $lang);
                    }
                    if ($ext == 'json') {
                        $this->importDescriptionJson($fileName, $content, $lang);
                    }
                }
            } catch (\Exception $e) {
                $this->getConsole()->error($e->getMessage());
            }
        }
        DB::commit();
    }


    private function getProductByFilename($filename)
    {
        $sku = str_replace(['.json', '.txt'], '', $filename);
        $obj = Product::where('sku', $sku)->first();
        if ($obj)
            return $obj;

        $sku = str_replace('-', '/', $sku);
        $obj = Product::where('sku', $sku)->first();
        return $obj;
    }


    private function importDescriptionHtml($fileName, $content, $lang)
    {
        $product = $this->getProductByFilename($fileName);
        if ($product) {
            $this->getConsole()->line("Importing description for product [$fileName]");
            $content = trim($content);
            DB::table('products_lang')->where('product_id', $product->id)->where('lang_id', $lang)->update(['ldesc' => $content]);
            $this->getConsole()->info('-> DONE');
        } else {
            $this->getConsole()->error("No product found for [$fileName]");
            $this->errors[] = $fileName;
        }
    }


    private function importDescriptionJson($fileName, $content, $lang)
    {
        $product = $this->getProductByFilename($fileName);
        if ($product) {
            $this->getConsole()->line("Importing json for product [$fileName]");
            $content = trim($content);
            DB::table('products_lang')->where('product_id', $product->id)->where('lang_id', $lang)->update(['head' => $content]);
            $this->getConsole()->info('-> DONE');
        } else {
            $this->getConsole()->error("No product found for [$fileName]");
            $this->errors[] = $fileName;
        }
    }


    public function attributes_refactory()
    {
        $console = $this->getConsole();
        $rows = DB::table('products_attributes')->get();
        DB::beginTransaction();
        $counter = 0;
        $many = count($rows);
        $console->line("Found [$many] records");
        foreach ($rows as $row) {
            $counter++;
            DB::table('products_attributes')->where('id', $row->id)->update(['id' => $counter]);
            $console->line("Done ($counter/$many)");
        }
        DB::commit();


        $rows = DB::table('products_attributes_position')->get();
        DB::beginTransaction();
        $counter = 0;
        $many = count($rows);
        $console->line("Found [$many] records");
        foreach ($rows as $row) {
            $counter++;
            DB::table('products_attributes_position')->where('id', $row->id)->update(['id' => $counter]);
            $console->line("Done ($counter/$many)");
        }
        DB::commit();

    }


    public function images_cmd()
    {
        $console = $this->getConsole();
        $console->line(__METHOD__);
        $product_ids = \Product::where('source', 'morellato')->lists('id');
        $images = \ProductImage::whereIn('product_id', $product_ids)->get();
        $console->line(count($images));
        $lines = [];
        $sourceDir = '/workarea/www/bluespirit/l4/public/assets/products/';
        $targetDir = '/workarea/morellato/';
        foreach ($images as $img) {
            $cmd = "cp {$sourceDir}{$img->filename} {$targetDir}{$img->filename}";
            $lines[] = $cmd;
        }
        \File::put(storage_path('cmd_images.txt'), implode(PHP_EOL, $lines));
        $console->info('DONE');
    }


    public function clear_zombies()
    {
        /*delete from products_lang where product_id not in (select id from products);
delete from products_attributes where product_id not in (select id from products);
delete from products_attributes_position where product_id not in (select id from products);
delete from categories_products where product_id not in (select id from products);
delete from images where product_id not in (select id from products);
delete from images_lang where product_image_id not in (select id from images);*/
    }


    public function clear_images()
    {

        $root = public_path('assets/products/');
        $imageFiles = \File::allFiles($root);
        $names = [];
        foreach ($imageFiles as $imageFile) {
            $names[] = $imageFile->getFilename();
        }
        $files = DB::table('images')->orderBy('filename')->lists('filename');
        $diff = array_diff($names, $files);
        print_r($diff);
        return;
        $many = count($files);
        $this->getConsole()->line("Found [$many] to delete");
        foreach ($files as $file) {

        }
    }


    public function dump()
    {

        $dbName = config('database.connections.mysql.database');

        $tablesToDump = [
            'attributes',
            'attributes_lang',
            'attributes_options',
            'attributes_options_lang',
            'attributes_rules',
            'attributes_sets',
            'attributes_sets_content',
            'attributes_sets_content_cache',
            'attributes_sets_rules',
            'attributes_sets_rules_cache',
            'banners',
            'banners_lang',
            'brands',
            'brands_lang',
            'cache_products',
            'cache_products_sets',
            'carriers',
            'carriers_group',
            'carriers_lang',
            'cart_rules',
            'cart_rules_carrier',
            'cart_rules_combination',
            'cart_rules_country',
            'cart_rules_group',
            'cart_rules_lang',
            'cart_rules_product_rule',
            'cart_rules_product_rule_group',
            'cart_rules_product_rule_value',
            'categories',
            'categories_lang',
            'categories_products',
            'collections',
            'collections_lang',
            'countries',
            'countries_lang',
            'currencies',
            'delivery',
            'emails',
            'emails_lang',
            'images',
            'images_groups',
            'images_groups_lang',
            'images_lang',
            'images_sets',
            'images_sets_rules',
            'lexicons',
            'lexicons_lang',
            'instagrams',
            'magic_tokens',
            'magic_tokens_templates',
            'magic_tokens_templates_lang',
            'menu_types',
            'menus',
            'menus_lang',
            'message_templates',
            'message_templates_lang',
            'mi_shops',
            'module_positions',
            'module_templates',
            'module_types',
            'modules',
            'modules_lang',
            'navs',
            'navs_lang',
            'resellers',
            'products',
            'products_attributes',
            'products_attributes_lang',
            'products_attributes_position',
            'products_combinations',
            'products_combinations_attributes',
            'products_combinations_images',
            'products_lang',
            'products_relations',
            'products_specific_prices',
            'products_specific_stocks',
            'seo_blocks',
            'seo_blocks_lang',
            'seo_blocks_rules',
            'seo_texts',
            'seo_texts_lang',
            'seo_texts_rules',
            'override_rules',
            'pages',
            'pages_lang',
            'trends',
            'trends_lang',
            'trends_products',
            'payments',
            'payments_carrier',
            'payments_country',
            'payments_group',
            'payments_lang',
            'price_rules',
            'price_rules_country',
            'price_rules_currency',
            'price_rules_group',
            'price_rules_lang',
            'range_price',
            'range_weight',
            'order_states',
            'order_states_lang',
            'payment_states',
            'payment_states_lang',
            'rma_options',
            'rma_options_lang',
            'rma_states',
            'rma_states_lang',
            'sections',
            'sections_lang',


        ];

        /*$tablesToDump = [
            'address',
            'customers',
            'orders',
            'order_details',
            'order_history',
            'newsletter',
            'mi_orders_slips',
        ];*/

        $notList = implode("','", $tablesToDump);

        $tables = DB::select("SELECT table_name FROM information_schema.tables where table_schema='$dbName' and table_name not in ('$notList')");

        $cmd = 'D:/MariaDB10.0/bin/mysqldump.exe --port=4306 --add-drop-table --no-create-db -uwebmaster -potevi12! ';
        foreach ($tables as $table) {
            $cmd .= "--ignore-table=$dbName.$table->table_name ";
        }
        $cmd .= "$dbName > sources/dump.sql";
        \File::put(base_path('mysqldump.bat'), $cmd);
    }


    public function dump_exclude()
    {
        $dbName = 'kronoshop_temp';

        $tablesToExclude = [
            'address',
            'customers',
            'customers_groups',
            'customers_groups_lang',
            'customers_import',
            'config',
            'config_lang',
            'gift_cards',
            'gift_cards_movements',
            'google_analytics',
            'import_301',
            'import_affiliates_stocks',
            'import_domains',
            'import_entities_address',
            'import_entities_carriers',
            'import_entities_countries',
            'import_entities_currencies',
            'import_entities_customers',
            'import_entities_customers_groups',
            'import_entities_errors',
            'import_entities_newsletter',
            'import_entities_order_states',
            'import_entities_payment_states',
            'import_entities_payments',
            'import_entities_promotions',
            'import_entities_sales',
            'import_entities_sales_address',
            'import_entities_sales_products',
            'import_entities_shops',
            'import_entities_states',
            'import_excel_builder',
            'import_excel_jewels',
            'import_excel_straps',
            'import_excel_watches',
            'import_giftcards',
            'import_orders',
            'mi_address_responses',
            'mi_address_trackings',
            'mi_anagrafica',
            'mi_anagrafica_ng',
            'mi_attribute_alias',
            'mi_categorie',
            'mi_customers',
            'mi_errors',
            'mi_fidelity_logs',
            'mi_giacenza',
            'mi_giacenza_ng',
            'mi_listino',
            'mi_logs',
            'mi_migrations',
            'mi_orders',
            'mi_orders_logs',
            'mi_orders_messages',
            'mi_orders_slips',
            'mi_orders_slips_details',
            'mi_products_actions',
            'mi_sapsku',
            'mi_shops',
            'mi_traduzioni',
            'migrations',
            'newsletters',
            'options',
            'order_carriers',
            'order_cart_rules',
            'order_details',
            'order_history',
            'order_states',
            'order_states_lang',
            'orders',
            'payment_states',
            'payment_states_lang',
            'rma_details',
            'rma_options',
            'rma_options_lang',
            'rma_states',
            'rma_states_lang',
            'rmas',
            'stock_mvts',
            'stock_reasons',
            'stock_reasons_lang',
            'throttle',
            'transactions',
            'transactions_paypal',
            'transactions_safepay',
            'users',
            'users_groups',
            'wishlists',
            'wishlists_products',
        ];

        $notList = implode("','", $tablesToExclude);

        $query = "SELECT table_name FROM information_schema.tables where table_schema='$dbName' and table_name in ('$notList')";
        $tables = DB::select($query);

        $cmd = 'D:/MariaDB10.0/bin/mysqldump.exe --port=4306 --add-drop-table --no-create-db -uwebmaster -potevi12! ';
        foreach ($tables as $table) {
            $cmd .= "--ignore-table=$dbName.$table->table_name " . PHP_EOL;
        }
        $cmd .= "$dbName > sources/dump.sql";
        \File::put(base_path('mysqldump_exclude.bat'), $cmd);
    }


    public function check_images()
    {
        $files = DB::table('images')->orderBy('filename')->lists('filename');
        $root = public_path('assets/products/');
        $many = count($files);
        $this->getConsole()->comment("Found $many images that dont exist");
        $lines = [];
        foreach ($files as $file) {
            $realPath = $root . $file;
            if (!file_exists($realPath)) {
                $this->getConsole()->line($file);
                //$lines[] = "cp /workarea/www/production/l4/public/assets/products/$file /workarea/bluespirit/";
                $lines[] = "UPDATE images set md5=null where filename='$file';";
            }
        }
        \File::put(storage_path('cmd.txt'), implode(PHP_EOL, $lines));
    }


    public function countries()
    {
        $console = $this->getConsole();
        $console->line(__METHOD__);
        $rows = DB::table('countries_temp')->get();
        foreach ($rows as $row) {
            DB::table('countries')->where('iso_code', $row->iso)->update(['iso_code3' => $row->iso3, 'iso_number' => $row->numcode]);
        }
        $console->info('DONE');
    }


    public function import_customers()
    {
        $table = 'customers_import';
        $default = \Customer::getDefaultAttributes();
        $console = $this->getConsole();
        $default_group_id = 6;

        DB::beginTransaction();

        $move_to_default = true;
        if ($move_to_default) {
            DB::table('customers')
                ->where('default_group_id', $default_group_id)
                ->update(['default_group_id' => 3]);
        }

        $rows = DB::table($table)->where('email', '<>', '')->get();
        foreach ($rows as $row) {
            $data = $default;
            $data['active'] = 1;
            $data['firstname'] = trim($row->firstname);
            $data['lastname'] = trim($row->lastname);
            $data['email'] = trim($row->email);
            $data['name'] = $data['firstname'] . ' ' . $data['lastname'];
            $data['gender_id'] = $row->gender == 'F' ? 2 : 1;
            $data['last_passwd_gen'] = \Format::now();
            $data['passwd'] = \Hash::make(trim($row->password));
            $data['default_group_id'] = $default_group_id;
            if ($row->birthday != '' and $row->birthday != null) {
                $data['birthday'] = $row->birthday;
            }

            $exists = \Customer::where('email', $data['email'])->first();
            if ($exists) {
                $console->line("Customer {$data['email']} already exists");
                $exists->fill($data);
                $exists->save();
                $console->info('OK, Updated');
            } else {
                $console->line("Impporting Customer $row->email");
                $data['secure_key'] = \Format::secure_key();
                $customer = new \Customer($data);
                $customer->save();
                if ($customer->id > 0) {
                    $console->info('OK, Inserted');
                } else {
                    $console->error('Error, Not inserted');
                }
            }

        }

        DB::commit();

    }


    public function export_modules()
    {
        $ids = [385, 386, 387];
        $new_ids = [385, 386, 387];
        $columns = \Schema::getColumnListing('modules');
        //dd($columns);

        $columns_lang = \Schema::getColumnListing('modules_lang');

        $commands = [];

        $rows = \Module::whereIn('id', $ids)->get();
        $index = 0;
        foreach ($rows as $row) {
            $id = $new_ids[$index];
            $fields = $columns;
            $values = [];
            foreach ($columns as $column) {
                $v = $column == 'id' ? $id : $row->$column;
                if ($v === null)
                    $v = 'null';
                $values[] = $v;
            }
            $fieldsList = implode(', ', $fields);
            $valuesList = implode("', '", $values);
            $cmd = "insert into modules($fieldsList) values('$valuesList');";
            $commands[] = str_replace("'null'", 'null', $cmd);

            $lang_rows = \Module_Lang::where('module_id', $row->id)->get();
            foreach ($lang_rows as $lang_row) {
                $fields = $columns_lang;
                $values = [];
                foreach ($columns_lang as $column) {
                    $v = $column == 'module_id' ? $id : $lang_row->$column;
                    if ($v === null)
                        $v = 'null';
                    $values[] = $v;
                }
                $fieldsList = implode(', ', $fields);
                $valuesList = implode("', '", $values);
                $cmd = "insert into modules_lang($fieldsList) values('$valuesList');";
                $commands[] = str_replace("'null'", 'null', $cmd);
            }
            $index++;
        }

        \File::put(base_path('sources/export_modules.sql'), implode(PHP_EOL, $commands));
        $this->getConsole()->info('DONE');
    }


    public function storelocator()
    {
        $console = $this->getConsole();
        $console->line("Executing storelocator...");
        $rows = \Reseller::where(function ($query) {
            $query->whereNull('longitude')->orWhere('longitude', 0);
        })->orWhere(function ($query) {
            $query->whereNull('latitude')->orWhere('latitude', 0);
        })
            //->take(10)
            ->get();

        $many = count($rows);

        $console->line("Found $many records...");

        foreach ($rows as $row) {
            $resolved = $row->resolveAddress();
            if ($resolved) {
                $console->line("Resolved address for [$row->name] ($row->id)");
            } else {
                $console->comment("Could not resolve address for [$row->name] ($row->id)");
            }
        }
    }


    public function reset()
    {
        $console = $this->getConsole();

        $bool = $console->ask("Are you sure you want to reset all main tables?", 'N');
        $bool = strtoupper($bool);

        if ($bool != 'Y') {
            $console->line("Exiting...");
            return;
        }

        $console->line("Truncating all main tables...");

        $queries = [];

        $queries[] = "DELETE FROM products_lang WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM products_attributes WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM products_attributes_position WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM products_combinations WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM products_combinations WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM products_specific_prices WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM products_specific_stocks WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM categories_products WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM cache_products WHERE id NOT IN (select id from products);";
        $queries[] = "DELETE FROM products_combinations_attributes WHERE combination_id NOT IN (select id from products_combinations);";
        $queries[] = "DELETE FROM images WHERE product_id NOT IN (select id from products);";
        $queries[] = "DELETE FROM images_lang WHERE product_image_id NOT IN (select id from images);";
        $queries[] = "DELETE FROM seo_texts WHERE brand_id > 0 and brand_id NOT IN (select id from brands);";
        $queries[] = "DELETE FROM seo_texts WHERE category_id > 0 and category_id NOT IN (select id from categories);";
        $queries[] = "DELETE FROM seo_texts WHERE collection_id > 0 and collection_id NOT IN (select id from collections);";
        $queries[] = "DELETE FROM seo_texts WHERE trend_id > 0 and trend_id NOT IN (select id from trends);";
        $queries[] = "DELETE FROM seo_texts_lang WHERE seo_text_id NOT IN (select id from seo_texts);";
        $queries[] = "DELETE FROM seo_texts_rules WHERE seo_text_id NOT IN (select id from seo_texts);";
        $queries[] = "TRUNCATE TABLE address;";
        $queries[] = "TRUNCATE TABLE campaigns;";
        $queries[] = "TRUNCATE TABLE cart;";
        $queries[] = "TRUNCATE TABLE cart_cart_rules;";
        $queries[] = "TRUNCATE TABLE cart_product;";
        $queries[] = "TRUNCATE TABLE cart_rules;";
        $queries[] = "TRUNCATE TABLE cart_rules_carrier;";
        $queries[] = "TRUNCATE TABLE cart_rules_combination;";
        $queries[] = "TRUNCATE TABLE cart_rules_country;";
        $queries[] = "TRUNCATE TABLE cart_rules_group;";
        $queries[] = "TRUNCATE TABLE cart_rules_lang;";
        $queries[] = "TRUNCATE TABLE cart_rules_product_rule;";
        $queries[] = "TRUNCATE TABLE cart_rules_product_rule_group;";
        $queries[] = "TRUNCATE TABLE cart_rules_product_rule_value;";
        $queries[] = "TRUNCATE TABLE coupons;";
        $queries[] = "TRUNCATE TABLE customers;";
        $queries[] = "TRUNCATE TABLE customers_import;";
        $queries[] = "TRUNCATE TABLE fidelity_points;";
        $queries[] = "TRUNCATE TABLE import_excel_jewels;";
        $queries[] = "TRUNCATE TABLE import_excel_watches;";
        $queries[] = "TRUNCATE TABLE messages;";
        $queries[] = "TRUNCATE TABLE mi_address_responses;";
        $queries[] = "TRUNCATE TABLE mi_address_trackings;";
        $queries[] = "TRUNCATE TABLE mi_anagrafica;";
        $queries[] = "TRUNCATE TABLE mi_anagrafica_ng;";
        $queries[] = "TRUNCATE TABLE mi_categorie;";
        $queries[] = "TRUNCATE TABLE mi_errors;";
        $queries[] = "TRUNCATE TABLE mi_fidelity_logs;";
        $queries[] = "TRUNCATE TABLE mi_giacenza;";
        $queries[] = "TRUNCATE TABLE mi_giacenza_ng;";
        $queries[] = "TRUNCATE TABLE mi_listino;";
        $queries[] = "TRUNCATE TABLE mi_logs;";
        $queries[] = "TRUNCATE TABLE mi_migrations;";
        $queries[] = "TRUNCATE TABLE mi_orders;";
        $queries[] = "TRUNCATE TABLE mi_orders_messages;";
        $queries[] = "TRUNCATE TABLE mi_orders_slips;";
        $queries[] = "TRUNCATE TABLE mi_products_actions;";
        $queries[] = "TRUNCATE TABLE mi_sapsku;";
        $queries[] = "TRUNCATE TABLE mi_traduzioni;";
        $queries[] = "TRUNCATE TABLE newsletters;";
        $queries[] = "TRUNCATE TABLE order_carriers;";
        $queries[] = "TRUNCATE TABLE order_cart_rules;";
        $queries[] = "TRUNCATE TABLE order_details;";
        $queries[] = "TRUNCATE TABLE order_history;";
        $queries[] = "TRUNCATE TABLE orders;";
        $queries[] = "TRUNCATE TABLE price_rules;";
        $queries[] = "TRUNCATE TABLE price_rules_country;";
        $queries[] = "TRUNCATE TABLE price_rules_currency;";
        $queries[] = "TRUNCATE TABLE price_rules_group;";
        $queries[] = "TRUNCATE TABLE price_rules_product_rule;";
        $queries[] = "TRUNCATE TABLE price_rules_product_rule_group;";
        $queries[] = "TRUNCATE TABLE price_rules_product_rule_value;";
        $queries[] = "TRUNCATE TABLE profiles;";
        $queries[] = "TRUNCATE TABLE redirect_links;";
        $queries[] = "TRUNCATE TABLE rmas;";
        $queries[] = "TRUNCATE TABLE stock_mvts;";
        $queries[] = "TRUNCATE TABLE throttle;";
        $queries[] = "TRUNCATE TABLE transactions;";
        $queries[] = "TRUNCATE TABLE transactions_paypal;";
        $queries[] = "TRUNCATE TABLE transactions_safepay;";
        $queries[] = "TRUNCATE TABLE trends;";
        $queries[] = "TRUNCATE TABLE trends_lang;";
        $queries[] = "TRUNCATE TABLE trends_products;";
        $queries[] = "TRUNCATE TABLE vouchers;";
        $queries[] = "TRUNCATE TABLE wishlists;";
        $queries[] = "TRUNCATE TABLE wishlists_products;";

        //echo implode(PHP_EOL,$queries);
        //print_r($queries);
        audit(implode(PHP_EOL, $queries));

        $bool = $console->ask("Do you want to replace tokens for seo and entities?", 'N');
        $bool = strtoupper($bool);

        if ($bool != 'Y') {
            $console->line("Exiting...");
            return;
        }
        $queries = [];
        $searches = ['morellato.com', 'Morellato', 'morellato'];
        $replaces = ['morellatostraps.com', 'Cinturino by Morellato', 'Cinturino by Morellato'];

        /*
         * update products_lang set metatitle = replace(metatitle,'Kronoshop.com','Easybags.it');
update products_lang set metadescription = replace(metadescription,'Kronoshop.com','Easybags.it');
update products_lang set metakeywords = replace(metakeywords,'Kronoshop.com','Easybags.it');
update products_lang set h1 = replace(h1,'Kronoshop.com','Easybags.it');
update products_lang set ldesc = replace(ldesc,'Kronoshop.com','Easybags.it');
update products_lang set metatitle = replace(metatitle,'Kronoshop','Easybags');
update products_lang set metadescription = replace(metadescription,'Kronoshop','Easybags');
update products_lang set metakeywords = replace(metakeywords,'Kronoshop','Easybags');
update products_lang set h1 = replace(h1,'Kronoshop','Easybags');
update products_lang set ldesc = replace(ldesc,'Kronoshop','Easybags'); */

        $fields = [
            'metatitle',
            'metadescription',
            'metakeywords',
            'h1',
            'ldesc',
        ];

        $special_fields = [
            'seo_texts_lang' => [
                'metatitle',
                'metadescription',
                'metakeywords',
                'h1',
                'content',
            ],
            'seo_blocks_lang' => [
                'content',
            ],
            'lexicons_lang' => [
                'name',
            ],
            'modules_lang' => [
                'input_textarea',
                'input_textarea_alt',
                'input_text',
                'input_text_alt',
            ],
            'emails_lang' => [
                'subject',
                'body',
                'sender_name',
            ],
        ];

        $tables = [
            'brands_lang',
            'categories_lang',
            'collections_lang',
            'products_lang',
            'trends_lang',
            'seo_texts_lang',
            'seo_blocks_lang',
            'lexicons_lang',
            'modules_lang',
            'emails_lang',
        ];

        foreach ($tables as $table) {
            $table_fields = isset($special_fields[$table]) ? $special_fields[$table] : $fields;
            foreach ($table_fields as $field) {
                foreach ($searches as $i => $search) {
                    $replace = $replaces[$i];
                    $queries[] = "update $table set $field = replace($field, '$search', '$replace');";
                }
            }
        }
        audit(implode(PHP_EOL, $queries));


    }


    function text_attributes()
    {
        $console = $this->getConsole();
        $attributes = \Attribute::where('frontend_input', 'text')->get();
        foreach ($attributes as $attribute) {
            $console->line("Fixing $attribute->code");
            DB::beginTransaction();
            $query = "select *, count(product_id) as cnt from products_attributes where attribute_id=$attribute->id group by product_id having cnt>1 order by cnt desc, product_id desc";
            $rows = DB::select($query);
            $many = count($rows);
            if ($many > 0) {
                $console->line("Found $many duplicates");
                foreach ($rows as $row) {
                    $ids = DB::table('products_attributes')->where('attribute_id', $attribute->id)->where('product_id', $row->product_id)->orderBy('id', 'desc')->lists('id');
                    if (count($ids) > 1) {
                        $remove = $ids[0];
                        $console->line("Removing id [$remove] for $attribute->id | $row->product_id");
                        DB::table('products_attributes')->where('id', $remove)->delete();
                    }
                }
            }
            DB::commit();
        }
    }


    private function resolveCategory($category)
    {
        if ($category->parent_id == 0) {
            return $category->id;
        }
        return $this->resolveCategory(\Category::getObj($category->parent_id));
    }


    function remap_products_categories()
    {
        $console = $this->getConsole();
        $builder = \Product::orderBy('id', 'asc');
        //$builder->take(10);
        $products = $builder->get();

        $many = count($products);
        $counter = 0;
        DB::beginTransaction();
        foreach ($products as $product) {
            $counter++;
            $console->line("Optimizing product [$product->id] $counter / $many");
            $categories = \Category::whereIn('id', function ($query) use ($product) {
                $query->select('category_id')->from('categories_products')->where('product_id', $product->id);
            })->get();
            $default_category_id = null;
            $main_category_id = null;
            foreach ($categories as $category) {
                $parent_id = $this->resolveCategory($category);
                $console->line("Category: " . $category->id . " | parent: " . $parent_id);
                if (is_null($main_category_id)) {
                    $main_category_id = $parent_id;
                }
                if (is_null($default_category_id)) {
                    $default_category_id = $category->id;
                }
                $data = compact('main_category_id', 'default_category_id');
                print_r($data);
                DB::table('products')->where('id', $product->id)->update($data);
            }
        }
        DB::commit();
    }


    function en_collections()
    {
        $rows = DB::select("select * from collections_lang where lang_id='it' and collection_id not in (select collection_id from collections_lang where lang_id='en')");
        DB::beginTransaction();
        foreach ($rows as $row) {
            $data = [
                'lang_id' => 'en',
                'name' => $row->name,
                'collection_id' => $row->collection_id,
                'slug' => $row->slug,
                'published' => $row->published,
            ];
            DB::table('collections_lang')->insert($data);
        }
        DB::commit();
    }


    function links_for_301()
    {
        $console = $this->getConsole();
        $languages = \Core::getLanguages();

        $sourcePath = storage_path('morellato/csv/export/');
        $filename = $sourcePath . "links_" . date('Ymd') . '.csv';
        touch($filename);

        $console->line("Exporting links...");

        //we create the CSV into memory
        $csv = Writer::createFromPath($filename);

        //we insert the CSV header
        $csv->insertOne(['id', 'sku', 'lang_id', 'url']);

        $rows = Product::orderBy('id', 'desc')
            //->take(10)
            ->get(['id', 'sku']);

        foreach ($rows as $row) {
            foreach ($languages as $lang) {
                $link = \Link::to('product', $row->id, $lang);
                $data = [
                    'id' => $row->id,
                    'sku' => $row->sku,
                    'lang_id' => $lang,
                    'url' => $link,
                ];
                $csv->insertOne($data);
            }
            $console->line("Finished with $row->sku");
        }
    }


    function craft_products_names()
    {
        $console = $this->getConsole();
        $categories = [34, 23, 76, 83, 18, 5, 31];
        $languages = \Core::getLanguages();
        $rows = Product::orderBy('id', 'desc')
            ->whereIn('id', function ($query) use ($categories) {
                $query->select('product_id')->from('categories_products')->whereIn('category_id', $categories);
            })
            //->take(10)
            ->get();

        DB::beginTransaction();

        foreach ($rows as $row) {
            foreach ($languages as $lang) {
                $name = \ProductHelper::craftProductName($row, $lang);
                $console->line("Finished with $row->sku => [$lang] => $name");
                audit("$row->sku => [$lang] => $name");
                $slug = Str::slug($name);
                DB::table('products_lang')->where('product_id', $row->id)->where('lang_id', $lang)->update([
                    'name' => $name,
                    'slug' => $slug,
                ]);
            }
            $row->uncacheSimple();
        }
        DB::commit();
    }


    function create_map_301()
    {
        $tpl = "rewrite ^%s$ %s permanent;";

        $languages = \Core::getLanguages();

        foreach ($languages as $language) {
            $sourcePath = base_path("sources/301_$language.conf");
            $rows = DB::table('import_301')
                ->where('lang_id', $language)
                ->orderBy('source', 'desc')
                //->take(10)
                ->get();

            if (count($rows) == 0)
                continue;

            $lines = [];

            foreach ($rows as $row) {
                $lines[] = sprintf($tpl, trim($row->source), trim($row->target));
            }

            \File::put($sourcePath, implode(PHP_EOL, $lines));
            $this->getConsole()->info($language . ' => done');
        }
    }


    function create_map_301_general()
    {
        $tpl = "rewrite ^%s$ %s permanent;";

        $sourcePath = base_path("sources/301_general.conf");
        $rows = DB::table('import_301')
            ->whereNull('lang_id')
            ->orderBy('source', 'desc')
            //->take(10)
            ->get();

        $lines = [];

        foreach ($rows as $row) {
            $lines[] = sprintf($tpl, trim($row->source), trim($row->target));
        }

        \File::put($sourcePath, implode(PHP_EOL, $lines));
        $this->getConsole()->info('GENERAL => done');
    }


    function import_giftcards()
    {
        $console = $this->getConsole();
        $console->line("Importing gift cards...");
        $rows = DB::table('import_giftcards')->get();
        foreach ($rows as $row) {
            $console->line("Importing $row->code");
            $order = \Order::where('reference', $row->order_reference)->first();
            if ($order) {
                $order_id = $order->id;
                $code = $row->code;
                $orderDetail = \OrderDetail::where('order_id', $order_id)->first();
                if ($orderDetail) {
                    $order_detail_id = $orderDetail->id;
                    $product_id = $orderDetail->product_id;
                    $product = \Product::getObj($orderDetail->product_id);
                    $amount = $row->amount;
                    $original_amount = $product->buy_price;
                    $expires_at = $row->expires;
                    $name = $product->name;
                    $customer_name = $order->getCustomer()->name;
                    $data = compact('code', 'product_id', 'order_id', 'order_detail_id', 'amount', 'original_amount', 'expires_at', 'name', 'customer_name');
                    //audit($data);
                    print_r($data);
                    \GiftCard::updateOrCreate($data, ['code' => $code]);
                }
            } else {
                $console->error("Could not find order $row->order_reference");
            }
        }
    }


    function fix_images_en()
    {
        $rows = DB::select("select * from images_lang where lang_id='it' and `product_image_id`  not in (select `product_image_id`  from images_lang where lang_id='en')");
        foreach ($rows as $row) {
            try {
                DB::table('images_lang')->insert([
                    'product_image_id' => $row->product_image_id,
                    'lang_id' => 'en',
                    'legend' => '',
                    'published' => 1,
                ]);
                $this->getConsole()->line($row->product_image_id);
            } catch (\Exception $e) {
                $this->getConsole()->line($e->getMessage());
            }
        }
    }


    function redirect_domains()
    {
        $tpl = "server { 
	listen 80;
	server_name %s;	
	return 301 \$scheme://www.morellato.com%s;
}";

        $languages = \Core::getLanguages();

        $lines = [];
        $sourcePath = base_path("sources/domains.conf");

        foreach ($languages as $language) {

            $rows = DB::table('import_domains')
                ->where('lang_id', $language)
                ->orderBy('domain', 'asc')
                ->get();

            $domains = [];

            foreach ($rows as $row) {
                $target = $row->lang_id == 'it' ? null : "/" . $row->lang_id;
                $domains[] = $row->domain;
                $domains[] = 'www.' . $row->domain;
            }

            $lines[] = sprintf($tpl, trim(implode(' ', $domains)), trim($target));
            $this->getConsole()->info($language . ' => done');

        }

        \File::put($sourcePath, implode(PHP_EOL, $lines));

    }


    function address_trackings()
    {
        $console = $this->getConsole();

        $rows = \AddressTracking::orderBy('order_id')->get();

        $many = count($rows);
        $console->line("There are [$many] records...");

        DB::beginTransaction();
        $counter = 0;
        foreach ($rows as $row) {
            $counter++;
            $order_id = $row->order_id;
            $order_shipping_number = (string)config('gls.geo.loc') . $row->shipping_number;
            DB::table('orders')->where('id', $order_id)->update(['shipping_number' => $order_shipping_number]);
            $console->line("Setting [$order_shipping_number] to order [$order_id] - $counter/$many");
        }
        DB::commit();
    }


    function bind_families()
    {
        $console = $this->getConsole();
        $charm_category = 31;
        $attribute = \Attribute::whereCode('family')->first();
        $rows = Product::withCategories([$charm_category])->orderBy('sku')->get();

        DB::beginTransaction();
        foreach ($rows as $row) {

            $import = DB::table('import_excel_jewels')->where('sku', $row->sku)->first();
            if ($import and $import->family != '') {
                $console->line("Found product $row->sku | $import->family");
                $option = \AttributeOption::rows('it')->whereAttributeId($attribute->id)->whereName($import->family)->first();
                if ($option) {
                    $row->addAttribute($attribute->id, [$option->id]);
                    $console->info("-> INSERTED");
                }
            }
        }
        DB::commit();
    }


    function make_transparent_charms()
    {
        $console = $this->getConsole();
        $charm_category = 31;
        $rows = Product::withCategories([$charm_category])
            ->orderBy('sku')->get();

        foreach ($rows as $row) {
            $images = $row->getImages(false);
            $many = count($images);
            $console->line("Processing [$many] total items");

            if (!empty($images)) {
                $image = $images[0];
                $this->make_transparent_img($image->filename, $row->sku);
                $console->info("DONE with $row->sku");
            }
        }
    }


    private function make_transparent_img($name, $sku)
    {
        $file = str_replace('.jpg', null, $name);
        $filePath = public_path('assets/products/' . $file . '.jpg');
        $root = public_path('assets/products/droplets');
        if (!is_dir($root)) {
            mkdir($root, 0775);
        }
        $destPath = $root . '/' . $sku . '.png';


        $im = imagecreatefromjpeg($filePath);

        $bg_color = 16777215;
        imagecolortransparent($im, $bg_color);

        imagepng($im, $destPath, 9);

        imagedestroy($im);

    }


    function builder_setup_products()
    {
        $collections = config('builder.collections');
        $charm_category = config('builder.droplet_categories');
        DB::beginTransaction();
        audit_watch();
        //take all charms
        $rows = Product::withCollections($collections)->withCategories($charm_category)->get();
        foreach ($rows as $row) {
            $conditions = ['product_id' => $row->id];
            $data = [
                'product_id' => $row->id,
                'attachment' => 'center',
                'type' => 'droplet',
                'blueprint_id' => null,
            ];
            BuilderProduct::updateOrCreate($conditions, $data);
            $this->getConsole()->line("Product $row->sku inserted as DROPLET");
        }
        //take all bases
        $base_categories = [16, //bracciali
            6, //collane
            2, 3, 8, 9, 10, 11, 12, 19, 20, 21, 22, 25, 44, 51, 75, 78, 82, 84, 85, 86, 102, 111, 112, 113, 114, 115 //watches
        ];
        $rows = Product::withCollections($collections)->withCategories($base_categories)->get();
        foreach ($rows as $row) {
            $conditions = ['product_id' => $row->id];
            $data = [
                'product_id' => $row->id,
                'attachment' => null,
                'type' => 'base',
                'blueprint_id' => 1,
            ];
            BuilderProduct::updateOrCreate($conditions, $data);
            $this->getConsole()->line("Product $row->sku inserted as BASE");
        }
        DB::commit();
    }

    /**
     * @return ConfiguratorRepository
     */
    private function getConfiguratorRepository()
    {
        return \App::make(ConfiguratorRepository::class);
    }


    function builder_import_products()
    {
        $console = $this->getConsole();
        $rows = DB::table('import_excel_builder')->get();
        $notFounds = [];
        DB::beginTransaction();
        //audit_watch();
        foreach ($rows as $row) {
            $product = Product::whereSku($row->sku)->first();
            if (is_null($product)) {
                $console->error("Warning: product $row->sku not found");
                $notFounds[] = $row->sku;
                continue;
            }
            if ($row->type == 'base') {
                $conditions = ['product_id' => $product->id];
                $blueprint = BuilderBlueprint::whereCode($row->config_pa)->first();
                $data = [
                    'product_id' => $product->id,
                    'attachment' => null,
                    'type' => 'base',
                    'blueprint_id' => $blueprint->id,
                ];
                BuilderProduct::updateOrCreate($conditions, $data);
                $this->getConsole()->line("Product $row->sku inserted as BASE");
            }
            if ($row->type == 'droplet') {
                $conditions = ['product_id' => $product->id];
                $params = null;
                switch ($row->attachment) {
                    case 'T':
                        $attachment = 'top';
                        $params = ['doubleSize' => true, 'offsetTop' => 40];
                        break;
                    case 'B':
                        $attachment = 'bottom';
                        break;
                    case 'C':
                    default:
                        $attachment = 'center';
                        break;
                }
                $data = [
                    'product_id' => $product->id,
                    'attachment' => $attachment,
                    'type' => 'droplet',
                    'blueprint_id' => null,
                    'params' => $params,
                ];
                BuilderProduct::updateOrCreate($conditions, $data);
                $this->getConsole()->line("Product $row->sku inserted as DROPLET");
            }
        }
        DB::commit();
        audit(implode(PHP_EOL, $notFounds));
    }


    function builder_bind_families()
    {
        $console = $this->getConsole();

        $attribute = \Attribute::whereCode('family')->first();
        $rows = Product::onlyDroplets()->orderBy('sku')->get();

        DB::beginTransaction();
        foreach ($rows as $row) {

            $import = DB::table('import_excel_builder')->where('sku', $row->sku)->first();
            if ($import and $import->family != '') {
                $console->line("Found product $row->sku | $import->family");
                $option = \AttributeOption::rows('it')->whereAttributeId($attribute->id)->whereName($import->family)->first();
                if ($option) {
                    $row->addAttribute($attribute->id, [$option->id]);
                    $console->info("-> INSERTED");
                } else {
                    $console->error("Warning: family $import->family not found!");
                }
            }
        }
        DB::commit();
    }


    function builder_setup_slots()
    {

        /*A  x=117 y= 630
B  x=272 y= 692
C  x=393 y= 715
D  x=496 y=720,5
E   x=600 y=715
F  x=723 y= 692
G  x=876  y= 630
*/
        $mode_natural = true;
        $width = 160;
        $height = 160;


        $slot1 = [
            'position' => 1,
            'x' => $mode_natural ? 40 : 37,
            'y' => 550,
            'width' => $width,
            'height' => $height,
        ];

        $slot2 = [
            'position' => 2,
            'x' => $mode_natural ? 160 : 192,
            'y' => $mode_natural ? 600 : 612,
            'width' => $width,
            'height' => $height,
        ];

        $slot3 = [
            'position' => 3,
            'x' => $mode_natural ? 290 : 313,
            'y' => $mode_natural ? 630 : 635,
            'width' => $width,
            'height' => $height,
        ];

        $slot4 = [
            'position' => 4,
            'x' => $mode_natural ? 420 : 416,
            'y' => 640,
            'width' => $width,
            'height' => $height,
        ];

        $slot5 = [
            'position' => 5,
            'x' => $mode_natural ? 550 : 520,
            'y' => $mode_natural ? 630 : 635,
            'width' => $width,
            'height' => $height,
        ];

        $slot6 = [
            'position' => 6,
            'x' => $mode_natural ? 680 : 643,
            'y' => $mode_natural ? 600 : 612,
            'width' => $width,
            'height' => $height,
        ];

        $slot7 = [
            'position' => 7,
            'x' => $mode_natural ? 800 : 796,
            'y' => 550,
            'width' => $width,
            'height' => $height,
        ];

        $slots = [
            $slot1,
            $slot2,
            $slot3,
            $slot4,
            $slot5,
            $slot6,
            $slot7,
        ];

        $model1 = BuilderBlueprint::find(1);
        $model1->params = compact('slots');
        $model1->save();

        unset($slots);

        $slots = [
            $slot1,
            $slot2,
            $slot6,
            $slot7,
        ];

        $model2 = BuilderBlueprint::find(2);
        $model2->params = compact('slots');
        $model2->save();
    }


    public function test_builder_payload()
    {
        $model = BuilderPayload::find(6);
        //$model->makeImage();
        $model->makePdf();
    }


    public function builder_set_exceptions()
    {

        $products = DB::table('products')->whereIn('sku', [
            'SCZ551W',
            'SCZ554W',
            'SCZ617W',
            'SCZ618W',
            'SCZ647W',
            'SCZ650W',
            'SCZ664W',
            'SCZ665W',
            'SCZ700W',
            'SCZ703W',
            'SCZ706W',
            'SCZ708W',
            'SCZ710W',
            'SCZ711W',
            'SCZ712W',
            'SCZ767W',
            'SCZ768W',
            'SCZ769W',
            'SCZ770W',
            'SCZ775W',
            'SCZ777W',
            'SCZ778W',
            'SCZ877W',
            'SCZ881W',
            'SCZ882W',
            'SCZ883W',
            'SCZ884W',
            'SCZ886W',
            'SCZ901W',
            'SCZ902W',
            'SCZ903W',
            'SCZ905W',
            'SCZ906W',
            'SCZ907W',
            'SCZ908W',
            'SCZ910W',
            'SCZ912',
            'SCZ915W',
            'SCZ916W',
            'SCZ918W',
            'SCZ920W',
            'SCZ940W',
            'SCZ952W',
            'SCZ953W',
            'SCZ956W',
            'SCZ960W',
            'SCZ961W',
            'SCZ417W',
            'SCZ418W',
            'SCZ440W',
            'SCZ445W',
            'SCZ912W',
        ])->lists('id');
        $params = ['doubleSize' => true, 'offsetTop' => 40];
        $rows = BuilderProduct::whereIn('product_id', $products)->get();
        foreach ($rows as $row) {
            $row->params = $params;
            $row->save();
        }


        $products = DB::table('products')->whereIn('sku', [
            'SAJT01',
            'SAJT19',
            'SAJT23',
            'SAJT25',
        ])->lists('id');
        $params = ['doubleSize' => true, 'offsetTop' => 0];
        $rows = BuilderProduct::whereIn('product_id', $products)->get();
        foreach ($rows as $row) {
            $row->params = $params;
            $row->save();
        }

    }


    public function builder_rename_files()
    {
        $root = public_path("assets/products/builder/raw/");
        $files = \File::allFiles($root);
        foreach ($files as $file) {
            audit($file);
            $pathName = $file->getPathName();
            $fileName = $file->getFilename();

            if (\Str::contains($pathName, '_8.png')) {

                $newFile = $root . str_replace('_8.PNG', '.png', strtoupper($fileName));
                echo $newFile . PHP_EOL;
                \File::move($pathName, $newFile);
            }

            if (\Str::contains($pathName, '_1.png')) {

                $newFile = $root . str_replace('_1.PNG', '.png', strtoupper($fileName));
                echo $newFile . PHP_EOL;
                \File::move($pathName, $newFile);
            }
        }
    }


    public function builder_missing_top()
    {
        $ids = BuilderProduct::where('type', 'droplet')->where('attachment', 'top')->whereNull('params')->lists('product_id');

        $skus = Product::whereIn('id', $ids)->orderBy('sku')->lists('sku');
        //rint_r($skus);
        echo implode($skus, PHP_EOL);
    }


    public function export_products_attributes()
    {
        $queries = [];
        $rows = DB::table('products_attributes')->where('attribute_id', config('builder.family_attribute_id'))->get();
        foreach ($rows as $row) {
            $pattern = "insert into products_attributes(product_id, attribute_id, attribute_val) values ($row->product_id, $row->attribute_id, $row->attribute_val);";
            echo $pattern . PHP_EOL;
            $queries[] = $pattern;
        }

        $rows = DB::table('products_attributes_position')->where('attribute_id', config('builder.family_attribute_id'))->get();
        foreach ($rows as $row) {
            $pattern = "insert into products_attributes_position(product_id, attribute_id, position) values ($row->product_id, $row->attribute_id, $row->position);";
            echo $pattern . PHP_EOL;
            $queries[] = $pattern;
        }

        file_put_contents(base_path('sources/configuratore.sql'), implode(PHP_EOL, $queries));
    }


    function repair_secure_keys()
    {
        $console = $this->getConsole();
        $rows = DB::select("select secure_key, count(id) as cnt from customers  GROUP BY secure_key HAVING cnt > 1 ORDER BY cnt desc");
        DB::beginTransaction();
        foreach ($rows as $row) {
            $console->comment("Fixing secure_key $row->secure_key, with $row->cnt records");
            $customers = DB::table('customers')->where('secure_key', $row->secure_key)->get();
            foreach ($customers as $customer) {
                $secure_key = \Format::secure_key();
                $console->line("$customer->id => $secure_key");
                DB::table('customers')->where('id', $customer->id)->update(compact('secure_key'));
            }
        }
        DB::commit();
    }


    public function export_orders()
    {
        $console = $this->getConsole();
        $input = $console->ask('Please insert an Order ID or a comma separated values of Order IDs');
        $ids = explode(',', $input);

        $payloads = [];
        $orders = DB::table('orders')->whereIn('id', $ids)->get();
        foreach ($orders as $order) {
            //fetch the customer
            $customer = DB::table('customers')->find($order->customer_id);
            $cart = DB::table('cart')->find($order->cart_id);
            $order_histories = DB::table('order_history')->where('order_id', $order->id)->get();
            $order_details = DB::table('order_details')->where('order_id', $order->id)->get();
            $addresses = [];
            $cart_products = [];
            $transactions = [];
            $transactions_safepay = [];
            $transactions_paypal = [];
            if ($customer) {
                $addresses = DB::table('address')->where('customer_id', $customer->id)->get();
            }
            if ($cart) {
                $cart_products = DB::table('cart_product')->where('cart_id', $cart->id)->get();
            }
            $transactions = DB::table('transactions')->where('order_id', $order->id)->get();
            foreach ($transactions as $transaction) {
                if ($transactions->module == 'safepay')
                    $transactions_safepay = DB::table('transactions_safepay')->where('transaction_ref', $transaction->id)->get();

                if ($transactions->module == 'paypal')
                    $transactions_paypal = DB::table('transactions_paypal')->where('transaction_ref', $transaction->id)->get();
            }

            $order->relations = compact(
                'customer',
                'cart',
                'order_histories',
                'order_details',
                'addresses',
                'cart_products',
                'transactions',
                'transactions_safepay',
                'transactions_paypal'
            );
            $payloads[] = $order;
        }

        \File::put(storage_path('export_orders.json'), json_encode($payloads, JSON_PRETTY_PRINT | JSON_FORCE_OBJECT));
        $this->getConsole()->info('DONE');
    }

    public function import_orders()
    {
        $console = $this->getConsole();
        $filePath = storage_path('export_orders.json');
        if (!\File::exists($filePath)) {
            $console->error("No json file found");
            return;
        }
        $answer = $console->choice("Are you sure to import orders from json file?", ['No', 'Yes'], 0);
        if ($answer == 'No') {
            $console->line('OK, exiting...');
            return;
        }
        $console->comment('Importing orders from json file...');
        DB::beginTransaction();
        try {
            $orders = json_decode(\File::get($filePath));
            audit($orders);
            foreach ($orders as $order) {
                $order_id = null;
                $cart_id = null;
                $customer_id = null;
                $shipping_address_id = null;
                $billing_address_id = null;

                //CART
                if (isset($order->relations->cart)) {
                    $console->line('Importing [cart] entity...');
                    $cart = $order->relations->cart;
                    unset($cart->id);
                    $params = (array)$cart;
                    $cart_id = DB::table('cart')->insertGetId($params);
                    if ($cart_id > 0) {
                        $console->info("Entity [cart] created with id => $cart_id");
                    } else {
                        throw new \Exception("Could not create a cart");
                    }
                }

                //CART PRODUCTS
                if (isset($order->relations->cart_products) and $cart_id > 0) {
                    $console->line('Importing [cart_products] entities...');
                    $rows = $order->relations->cart_products;
                    foreach ($rows as $row) {
                        unset($row->id);
                        $row->cart_id = $cart_id;
                        $params = (array)$row;
                        $cart_product_id = DB::table('cart_product')->insertGetId($params);
                        if ($cart_product_id > 0) {
                            $console->info("Entity [cart_product] created with id => $cart_product_id");
                        } else {
                            throw new \Exception("Could not create a cart_product");
                        }
                    }
                }

                //CUSTOMER
                if (isset($order->relations->customer)) {
                    $console->line('Importing [customer] entity...');
                    $customer = $order->relations->customer;
                    unset($customer->id);
                    //try to resolve an existing customer
                    $existingCustomer = \Customer::whereEmail($customer->email)->whereActive(1)->first();
                    if ($existingCustomer) {
                        $console->comment("Found an existing customer with id $existingCustomer->id");
                        $customer_id = $existingCustomer->id;

                        $existingShippingAddress = DB::table('address')->where(['customer_id' => $customer_id, 'billing' => 0])->orderBy('id', 'desc')->first();
                        if ($existingShippingAddress) {
                            $shipping_address_id = $existingShippingAddress->id;
                            $console->comment("Found an existing shipping address with id $shipping_address_id");
                        }

                        //check if the importing order has different shipping/billing address
                        if ($order->billing_address_id > 0 and $order->billing_address_id) {
                            $existingBillingAddress = DB::table('address')->where(['customer_id' => $customer_id, 'billing' => 1])->orderBy('id', 'desc')->first();
                            if ($existingBillingAddress) {
                                $billing_address_id = $existingBillingAddress->id;
                                $console->comment("Found an existing billing address with id $billing_address_id");
                            }
                        } else {
                            $billing_address_id = 0;
                        }
                    } else {
                        $params = (array)$customer;
                        $customer_id = DB::table('customers')->insertGetId($params);
                        if ($customer_id > 0) {
                            $console->info("Entity [customer] created with id => $customer_id");
                        } else {
                            throw new \Exception("Could not create a customer");
                        }
                    }
                } //customer

                //SHIPPING ADDRESS
                if (is_null($shipping_address_id) and $customer_id > 0) {
                    foreach ($order->relations->addresses as $address) {
                        if ($address->billing == 0) {
                            $console->line('Importing [shipping_address] entity...');
                            unset($address->id);
                            $address->customer_id = $customer_id;
                            $params = (array)$address;
                            $shipping_address_id = DB::table('address')->insertGetId($params);
                            if ($shipping_address_id > 0) {
                                $console->info("Entity [shipping_address] created with id => $shipping_address_id");
                            } else {
                                throw new \Exception("Could not create a shipping address");
                            }
                        }
                    }
                }

                //BILLING ADDRESS
                if (is_null($billing_address_id) and $customer_id > 0) {
                    foreach ($order->relations->addresses as $address) {
                        if ($address->billing == 1) {
                            $console->line('Importing [billing_address] entity...');
                            unset($address->id);
                            $address->customer_id = $customer_id;
                            $params = (array)$address;
                            $billing_address_id = DB::table('address')->insertGetId($params);
                            if ($billing_address_id > 0) {
                                $console->info("Entity [billing_address] created with id => $billing_address_id");
                            } else {
                                throw new \Exception("Could not create a billing address");
                            }
                        }
                    }
                    if (is_null($billing_address_id))
                        $billing_address_id = 0;
                }

                //ORDER
                $newOrder = clone $order;
                unset($newOrder->relations);
                unset($newOrder->id);
                $newOrder->customer_id = $customer_id;
                $newOrder->shipping_address_id = $shipping_address_id;
                $newOrder->billing_address_id = $billing_address_id;
                $newOrder->cart_id = $cart_id;
                $newOrder->reference = \OrderManager::generateReference($cart_id);
                $params = (array)$newOrder;
                $order_id = DB::table('orders')->insertGetId($params);
                if ($order_id > 0) {
                    $console->info("Entity [order] created with id => $order_id");
                } else {
                    throw new \Exception("Could not create an order");
                }


                //ORDER HISTORIES
                if (isset($order->relations->order_histories) and $order_id > 0) {
                    $console->line('Importing [order_histories] entities...');
                    $rows = $order->relations->order_histories;
                    foreach ($rows as $row) {
                        unset($row->id);
                        $row->order_id = $order_id;
                        $params = (array)$row;
                        $order_history_id = DB::table('order_history')->insertGetId($params);
                        if ($order_history_id > 0) {
                            $console->info("Entity [order_history] created with id => $order_history_id");
                        } else {
                            throw new \Exception("Could not create a order_history");
                        }
                    }
                }

                //ORDER DETAILS
                if (isset($order->relations->order_details) and $order_id > 0) {
                    $console->line('Importing [order_details] entities...');
                    $rows = $order->relations->order_details;
                    foreach ($rows as $row) {
                        unset($row->id);
                        $row->order_id = $order_id;
                        $params = (array)$row;
                        $order_detail_id = DB::table('order_details')->insertGetId($params);
                        if ($order_detail_id > 0) {
                            $console->info("Entity [order_details] created with id => $order_detail_id");
                        } else {
                            throw new \Exception("Could not create a order_details");
                        }
                    }
                }

                //TRANSACTIONS
                if (isset($order->relations->transactions) and $order_id > 0) {
                    $transaction_id = null;
                    $console->line('Importing [transactions] entities...');
                    $rows = $order->relations->transactions;
                    foreach ($rows as $row) {
                        unset($row->id);
                        $row->order_id = $order_id;
                        $row->cart_id = $cart_id;
                        $params = (array)$row;
                        $transaction_id = DB::table('transactions')->insertGetId($params);
                        if ($transaction_id > 0) {
                            $console->info("Entity [transactions] created with id => $transaction_id");
                        } else {
                            throw new \Exception("Could not create a transactions");
                        }
                    }

                    if ($transaction_id and isset($order->relations->transactions_safepay) and !empty($order->relations->transactions_safepay)) {
                        $console->line('Importing [transactions_safepay] entities...');
                        $rows = $order->relations->transactions_safepay;
                        foreach ($rows as $row) {
                            unset($row->id);
                            $row->transaction_ref = $transaction_id;
                            $params = (array)$row;
                            $transaction_safepay_id = DB::table('transactions_safepay')->insertGetId($params);
                            if ($transaction_safepay_id > 0) {
                                $console->info("Entity [transactions_safepay] created with id => $transaction_safepay_id");
                            } else {
                                throw new \Exception("Could not create a transactions_safepay");
                            }
                        }
                    }

                    if ($transaction_id and isset($order->relations->transactions_paypal) and !empty($order->relations->transactions_paypal)) {
                        $console->line('Importing [transactions_paypal] entities...');
                        $rows = $order->relations->transactions_paypal;
                        foreach ($rows as $row) {
                            unset($row->id);
                            $row->transaction_ref = $transaction_id;
                            $params = (array)$row;
                            $transaction_paypal_id = DB::table('transactions_paypal')->insertGetId($params);
                            if ($transaction_paypal_id > 0) {
                                $console->info("Entity [transactions_paypal] created with id => $transaction_paypal_id");
                            } else {
                                throw new \Exception("Could not create a transactions_paypal");
                            }
                        }
                    }
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            audit_exception($e);
            $console->error($e->getMessage());
            DB::rollBack();
            $console->comment('Transaction has been rolled back');
        }

    }


    function rebind_cover_images()
    {
        $this->getConsole()->comment("Fetching rows...");
        $rows = \ProductImage::where('cover', 1)
            ->where('var', '>', 1)
            ->whereNotNull('md5')
            ->get();

        $many = count($rows);
        $this->getConsole()->comment("Found $many images...");

        foreach ($rows as $row) {
            $cover = \ProductImage::where('product_id', $row->product_id)
                ->where('cover', 0)
                ->where('var', 1)
                ->whereNotNull('md5')
                ->first();

            if ($cover) {
                $this->getConsole()->info("Found a valid 'cover' for product [$row->product_id]");
                $images = \ProductImage::where('product_id', $row->product_id)
                    ->whereNotNull('md5')
                    ->orderBy('var')
                    ->get();

                $position = 0;
                foreach ($images as $image) {
                    $position++;
                    $image->cover = 0;
                    $image->position = $position;
                    $image->save();
                }

                $cover->cover = 1;
                $cover->save();
                DB::table('products')->where('id', $row->product_id)->update(['default_img' => $cover->id]);
                $this->getConsole()->info('-> DONE');
            } else {
                $this->getConsole()->line("Could not find a valid 'cover' for product [$row->product_id]");
            }
        }
    }


    function rebind_cover_products()
    {
        $rows = \ProductImage::where('cover', 1)->get();
        DB::beginTransaction();
        foreach ($rows as $row) {
            $this->getConsole()->line("Updating [$row->product_id]");
            DB::table('products')->where('id', $row->product_id)->update(['default_img' => $row->id]);
        }
        DB::commit();
    }


    public function canvas()
    {
        $console = $this->getConsole();
        $importer = \App::make('services\Morellato\Database\Importer', [$console]);
        $rows = DB::table('mi_anagrafica')
            ->select('sku', 'mastersku', 'ean', 'canvas')
            //->take(10)
            ->get();
        $many = count($rows);
        $console->line("Found [$many] products to process...");
        $counter = 0;
        DB::beginTransaction();
        foreach ($rows as $row) {
            $counter++;
            if ($row->canvas != '') {
                $canvasData = $importer->parseCanvas($row->canvas);
                $console->line("Handling canvas [$row->canvas] for product [$row->sku] ($counter/$many)");
                $flags = \ProductHelper::checkNewFlag((object)$canvasData);
                $canvasData = array_merge($canvasData, $flags);
                $builder = DB::table('products')->where('sku', $row->sku);
                if (strlen(trim($row->ean)) > 0) {
                    $builder->where('ean13', $row->ean);
                }
                $builder->update($canvasData);
                DB::table('cache_products')->where('sku', $row->sku)->update($flags);
                $console->info('=> UPDATED');
            }
        }
        DB::commit();
    }


    public function excel_canvas()
    {
        $importers = [
            WatchesExcelImporter::class,
            JewelsExcelImporter::class,
            StrapsExcelImporter::class,
        ];
        $console = $this->getConsole();
        DB::beginTransaction();
        foreach ($importers as $importer) {
            try {
                /** @var ExcelImporter $importer */
                $importer = \App::make($importer);
                $table = $importer->getTable();
                if (Schema::hasTable($table)) {
                    $console->comment("Evaluating canvass for table [$table]");
                    $products = $importer->getProducts();
                    $many = count($products);
                    $counter = 0;
                    foreach ($products as $product) {
                        $counter++;
                        if ($product->canvas != '') {
                            $canvasData = $importer->parseCanvas($product->canvas);
                            $console->line("Parsing canvas [$product->canvas] for product [$product->sku] ($counter/$many)");
                            $flags = \ProductHelper::checkNewFlag((object)$canvasData);
                            $canvasData = array_merge($canvasData, $flags);
                            $builder = DB::table('products')->where('sku', $product->sku);
                            if (strlen(trim($product->ean)) > 0) {
                                $builder->where('ean13', $product->ean);
                            }
                            $builder->update($canvasData);
                            DB::table('cache_products')->where('sku', $product->sku)->update($flags);
                            $console->info('=> UPDATED');
                        }
                    }
                }
            } catch (\Exception $e) {
                $console->error($e->getMessage());
            }
        }
        DB::commit();
    }


    public function sapsku()
    {
        $rows = DB::table('mi_sapsku')->get();
        DB::beginTransaction();
        foreach ($rows as $row) {
            DB::table('products')->where('ean13', $row->ean)->update(['sap_sku' => $row->sapsku]);
            DB::table('products_combinations')->where('ean13', $row->ean)->update(['sap_sku' => $row->sapsku]);
            DB::table('order_details')->where('product_ean13', $row->ean)->update(['product_sap_reference' => $row->sapsku]);
        }
        DB::commit();
    }


    public function product_slug()
    {
        $console = $this->getConsole();
        $rows = DB::table('products_lang')->get();
        $count = count($rows);
        $console->comment("Found $count rows");
        $counter = 0;
        DB::beginTransaction();
        $ids = [];
        foreach ($rows as $row) {
            $counter++;
            $console->line("Updating row $counter/$count");
            $slug = trim(Str::slug($row->name));
            $lang_id = $row->lang_id;
            $product_id = $row->product_id;
            DB::table('products_lang')->where(compact('product_id', 'lang_id'))->update(compact('slug'));
            $ids[] = $row->product_id;
        }
        DB::commit();

        $ids = array_unique($ids);

        foreach ($ids as $id) {
            $console->line("Purging cache for product $id");
            $product = new Product(compact('id'));
            $product->uncacheSimple();
        }
    }


    public function product_seo()
    {
        $rows = Product::inStocks()->select('id')->get();
        $count = count($rows);
        $this->getConsole()->comment("Found $count rows");
        $counter = 0;
        DB::beginTransaction();
        foreach ($rows as $row) {
            $counter++;
            $this->getConsole()->line("Updating row $counter/$count");
            $seo_fields = \ProductHelper::generateSeo($row->id, true);
            try {
                foreach ($seo_fields as $locale => $data) {
                    $update = [
                        'metatitle' => $data['title'],
                        'h1' => $data['h1'],
                        'metakeywords' => $data['keywords'],
                        'metadescription' => $data['description'],
                    ];
                    DB::table('products_lang')->where('product_id', $row->id)->where('lang_id', $locale)->update($update);
                }
                $this->getConsole()->info("Updated SEO for product [$row->id]");
            } catch (\Exception $e) {
                $this->getConsole()->error($e->getMessage());
                $this->getConsole()->error("Could not bind SEO fields for product [$row->id]: " . $e->getMessage());
            }
        }
        DB::commit();
    }


    public function import_orders_slips_from_csv()
    {
        $file = storage_path('orders_slips.csv');

        if (!file_exists($file)) {
            $this->getConsole()->error("Could not import since file does not exist");
            return;
        }

        $this->getConsole()->info("Reading file $file");

        $reader = Reader::createFromPath($file);
        $reader->setDelimiter(';');
        $reader->setNewline("\r\n");
        $reader->setEnclosure('"');
        $results = $reader->fetchAll();

        DB::beginTransaction();

        $messages = [];

        try {
            foreach ($results as $result) {
                list($delivery_id, $tracking_id) = $result;
                $shipping_number = $delivery_id;
                $carrier_shipping = str_replace('L1', '', $tracking_id);
                $withdrawal_id = $carrier_shipping;

                $this->getConsole()->line("Delivery: $delivery_id | Tracking: $tracking_id");

                $orderSlip = OrderSlip::where('delivery_id', $delivery_id)->first();

                if ($orderSlip) {
                    try {
                        DB::table('mi_orders_slips')->where('delivery_id', $delivery_id)->update(compact('withdrawal_id'));
                    } catch (\Exception $e) {

                    }
                    $order = \Order::find($orderSlip->order_id);
                    if ($order) {
                        $deleted_by = 1;
                        $this->getConsole()->info("Updated order [$order->id] with shipping_number [$delivery_id] and carrier_shipping [$carrier_shipping]");
                        $messages[] = "OK - Updated order [$order->id] with shipping_number [$delivery_id] and carrier_shipping [$carrier_shipping]";
                        DB::table('orders')->where('id', $order->id)->update(compact('shipping_number', 'carrier_shipping', 'deleted_by'));
                    } else {
                        $this->getConsole()->comment("Could not find any order with BDA $delivery_id");
                        $messages[] = "WARN - Could not find any order with BDA $delivery_id";
                    }
                } else {
                    $this->getConsole()->comment("Could not find any slip for BDA $delivery_id");
                    $messages[] = "WARN - Could not find any slip for BDA $delivery_id";
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            $this->getConsole()->error($e->getMessage());
            DB::rollBack();
        }

        $name = (string)config('app.project') . '_import_orders_slips_from_csv.log';

        $destFile = storage_path("logs/$name");

        file_put_contents($destFile, implode(PHP_EOL, $messages));

    }


    public function generate_coupons()
    {
        $console = $this->getConsole();
        $cart_rule_id = $console->ask("What is the cart rule's id?");
        $cp_len = $console->ask('Maximum length of the code?');
        $cp_total = $console->ask('How many coupons You want to generate?');
        $cp_prefix = $console->ask('Please provide a shared code prefix (could be empty).');
        $cp_suffix = $console->ask('Please provide a shared code suffix (could be empty).');

        $params = [
            'len' => $cp_len,
            'total' => $cp_total,
            'prefix' => $cp_prefix,
            'suffix' => $cp_suffix,
            'cart_rule_id' => $cart_rule_id,
        ];

        $console->comment("Are these info valid?");
        foreach ($params as $key => $value) {
            $console->line("$key: $value");
        }

        $continue = $console->confirm("Are You sure to continue with batch generation?");

        if ($continue) {
            try {
                $repository = new CartRuleRepository();
                $repository->setConsole($console);
                $repository->generateCoupons($params);
            } catch (\Exception $e) {
                $console->error($e->getMessage());
            }
        }
    }


    public function generate_coupons_simple()
    {
        $console = $this->getConsole();
        $cart_rule_id = $console->ask("What is the cart rule's id?");
        $cp_len = $console->ask('Maximum length of the code?');
        $cp_total = $console->ask('How many coupons You want to generate?');
        $cp_prefix = $console->ask('Please provide a shared code prefix (could be empty).');
        $cp_suffix = $console->ask('Please provide a shared code suffix (could be empty).');

        $params = [
            'len' => $cp_len,
            'total' => $cp_total,
            'prefix' => $cp_prefix,
            'suffix' => $cp_suffix,
            'cart_rule_id' => $cart_rule_id,
        ];

        $console->comment("Are these info valid?");
        foreach ($params as $key => $value) {
            $console->line("$key: $value");
        }

        $continue = $console->confirm("Are You sure to continue with batch generation?");

        if ($continue) {
            try {
                $repository = new CartRuleRepository();
                $repository->setConsole($console);
                $repository->generateCouponsSimple($params);
            } catch (\Exception $e) {
                $console->error($e->getMessage());
            }
        }
    }


    public function csv_coupons()
    {
        $console = $this->getConsole();
        $cart_rule_id = $console->ask("What is the cart rule's id?");
        try {
            $repository = new CartRuleRepository();
            $repository->setConsole($console);
            $repository->exportCsvForCoupons($cart_rule_id);
        } catch (\Exception $e) {
            $console->error($e->getMessage());
        }
    }


    public function ean()
    {
        $console = $this->getConsole();
        $confirmed = $console->confirm("Are You sure to import custom EANs and CANVASes?");
        if ($confirmed == false)
            return;

        DB::beginTransaction();

        try {
            $rows = DB::table('import_excel_eans')->get();
            $many = count($rows);
            $console->comment("Found $many records to process...");
            $counter = 0;
            foreach ($rows as $row) {
                $counter++;
                $console->line("Processing record $counter/$many");
                $data = [];
                if (strlen(trim($row->canvas)) > 0) {
                    $data = array_merge($data, \ProductHelper::parseCanvas($row->canvas));
                }
                if (strlen(trim($row->ean)) > 0) {
                    $data['ean13'] = trim($row->ean);
                }
                if (!empty($data)) {
                    DB::table('products')->where('sku', $row->sku)->orWhere('sap_sku', $row->sku)->update($data);
                    $console->info("Product $row->sku successfully updated");
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            $console->error($e->getMessage());
            DB::rollBack();
        }
    }


    public function ean_fill()
    {
        $console = $this->getConsole();
        $confirmed = $console->confirm("Are You sure to import custom EANs and CANVASes?");
        if ($confirmed == false)
            return;

        DB::beginTransaction();

        try {
            $rows = DB::table('import_excel_eans')->get();
            $many = count($rows);
            $console->comment("Found $many records to process...");
            $counter = 0;
            foreach ($rows as $row) {
                $counter++;
                $console->line("Processing record $counter/$many");
                $data = [];
                if (strlen(trim($row->canvas)) > 0) {
                    $data['canvas'] = trim($row->canvas);
                }
                if (strlen(trim($row->ean)) > 0) {
                    $data['ean'] = trim($row->ean);
                }
                if (!empty($data)) {
                    if (isset($data['canvas'])) {
                        DB::table('import_excel_watches')->where('sku', $row->sku)->where('canvas', '')->update(['canvas' => $data['canvas']]);
                        DB::table('import_excel_jewels')->where('sku', $row->sku)->where('canvas', '')->update(['canvas' => $data['canvas']]);
                    }
                    if (isset($data['ean'])) {
                        DB::table('import_excel_watches')->where('sku', $row->sku)->where('ean', '')->update(['ean' => $data['ean']]);
                        DB::table('import_excel_jewels')->where('sku', $row->sku)->where('ean', '')->update(['ean' => $data['ean']]);
                    }
                    $console->info("Product $row->sku successfully updated");
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            $console->error($e->getMessage());
            DB::rollBack();
        }
    }


    public function check_wrong_delivery()
    {
        $write = true;
        $console = $this->getConsole();
        $shipmentXmlReader = \App::make('services\Morellato\Gls\Xml\Reader\ShipmentXmlReader');
        if ($write) {
            $orderHandler = \App::make('services\Morellato\Gls\Handler\OrderHandler');
            $orderHandler->setConsole($console);
        }
        $rows = DB::table('mi_orders_slips')
            ->where('withdrawal_id', 'G')
            //->where('delivery_id', '0083474740')
            //->where('order_id', '30554')
            ->get();
        foreach ($rows as $row) {
            $console->comment("Parsing order $row->order_id");
            $order_id = $row->order_id;
            $delivery_id = $row->delivery_id;
            $shop_record = OrderSlipDetail::where('order_id', $order_id)->where('type', OrderSlipDetail::TYPE_CARRIER)->first();
            if (is_null($shop_record)) {
                $console->error("Warning: cannot find any shop_record for this order");
                continue;
            }
            if ($delivery_id != $shop_record->vbelv) {
                $console->info("Changing delivery $delivery_id => {$shop_record->vbelv}");
                $delivery_id = $shop_record->vbelv;
                $console->line("Resolving tracking for BDA [$delivery_id]");
                if ($write) {
                    DB::table('orders')->where('id', $order_id)->update(['shipping_number' => $delivery_id]);
                    DB::table('mi_orders_slips')->where('order_id', $order_id)->update(['delivery_id' => $delivery_id]);
                    $orderHandler->trackOrder($order_id);
                } else {
                    $order = \Order::find($order_id);
                    $shipmentXmlReader->setModeByOrder($order)->setBda($delivery_id);
                    $responseData = $shipmentXmlReader->getResponse();
                    print_r($responseData);
                }
            }
        }
    }


    public function bs_dump_sql_products()
    {
        $table_str = 'attributes
attributes_lang
attributes_options
attributes_options_lang
attributes_rules
attributes_sets
attributes_sets_content
attributes_sets_content_cache
attributes_sets_rules
attributes_sets_rules_cache
brands
brands_lang
cache_products
cache_products_sets
categories
categories_lang
categories_products
collections
collections_lang
images
images_groups
images_groups_lang
images_lang
images_sets
images_sets_rules
products
products_attributes
products_attributes_lang
products_attributes_position
products_carriers
products_combinations
products_combinations_attributes
products_combinations_images
products_lang';

        $tables = explode(PHP_EOL, $table_str);
        $tables_list = implode(' ', $tables);
        $cmd = "mysqldump --add-drop-table --no-create-db --single-transaction --quick --lock-tables=false -u bluespirit -pXXXXXX bluespirit_prod $tables_list > export_products.sql";
        $this->getConsole()->line($cmd);
    }


    public function bs_dump_sql_non_products()
    {
        $table_str = 'banners
banners_lang
cart_rules
cart_rules_carrier
cart_rules_combination
cart_rules_country
cart_rules_group
cart_rules_lang
cart_rules_product_rule
cart_rules_product_rule_group
cart_rules_product_rule_value
customers_groups
customers_groups_lang
magic_tokens
magic_tokens_templates
magic_tokens_templates_lang
menu_types
menus
menus_lang
modules_lang
modules
navs
navs_lang
seo_blocks
seo_texts
price_rules
price_rules_country
price_rules_currency
price_rules_group
price_rules_lang
price_rules_product_rule
price_rules_product_rule_group
price_rules_product_rule_value
seo_blocks
seo_blocks_lang
seo_blocks_rules
seo_texts
seo_texts_lang
seo_texts_rules
trends
trends_products';

        $tables = explode(PHP_EOL, $table_str);
        $tables_list = implode(' ', $tables);
        $cmd = "mysqldump --add-drop-table --no-create-db --single-transaction --quick --lock-tables=false -u bluespirit -pXXXXXX bluespirit_prod $tables_list > export_other.sql";
        $this->getConsole()->line($cmd);
    }


    public function bs_dump_multilang()
    {
        $table_str = 'attributes
attributes_lang
attributes_options
attributes_options_lang
attributes_rules
attributes_sets
attributes_sets_content
attributes_sets_content_cache
attributes_sets_rules
attributes_sets_rules_cache
banners
banners_lang
brands
brands_lang
cache_products
cache_products_sets
carriers
carriers_lang
cart_rules
cart_rules_carrier
cart_rules_combination
cart_rules_country
cart_rules_group
cart_rules_lang
cart_rules_product_rule
cart_rules_product_rule_group
cart_rules_product_rule_value
categories
categories_lang
categories_products
collections
collections_lang
config_lang
countries
countries_lang
customers_groups
customers_groups_lang
days
days_lang
emails
emails_lang
genders
genders_lang
images
images_groups
images_groups_lang
images_lang
images_sets
images_sets_rules
languages
lexicons
lexicons_lang
magic_tokens
magic_tokens_templates
magic_tokens_templates_lang
menu_types
menus
menus_lang
message_templates
message_templates_lang
modules
modules_lang
months
months_lang
navs
navs_lang
order_states
order_states_lang
pages
pages_lang
payment_states
payment_states_lang
payments
payments_lang
peoples
peoples_lang
price_rules
price_rules_country
price_rules_currency
price_rules_group
price_rules_lang
price_rules_product_rule
price_rules_product_rule_group
price_rules_product_rule_value
products
products_attributes
products_attributes_lang
products_attributes_position
products_carriers
products_combinations
products_combinations_attributes
products_combinations_images
products_lang
rma_options
rma_options_lang
rma_states
rma_states_lang
sections
sections_lang
seo_blocks
seo_blocks_lang
seo_blocks_rules
seo_texts
seo_texts_lang
seo_texts_rules
stock_reasons
stock_reasons_lang
taxes
taxes_lang
trends
trends_lang';

        $tables = explode(PHP_EOL, $table_str);
        $tables_list = implode(' ', $tables);
        $cmd = "mysqldump --add-drop-table --no-create-db --single-transaction --quick --lock-tables=false -u bluespirit -pXXXXXX bluespirit_prod $tables_list > export_ml.sql";
        $this->getConsole()->line($cmd);
    }


    public function add_ean_to_index()
    {
        $console = $this->getConsole();
        $rows = \Product::where('ean13', '!=', '')->select('id', 'ean13')->get();
        $many = count($rows);
        $counter = 0;
        DB::beginTransaction();
        foreach ($rows as $row) {
            $counter++;
            $console->line("Product $counter/$many");
            DB::statement("UPDATE cache_products SET indexable_it = CONCAT(indexable_it, ' $row->ean13') WHERE id = $row->id");
        }
        DB::commit();
    }


    public function rebuild_products_cache()
    {
        $console = $this->getConsole();

        $products = Product::orderBy('id', 'desc')->get();
        $many = count($products);
        $console->comment("Found [$many] products to process");

        $counter = 0;
        $limit_commit = 100;

        foreach ($products as $product) {
            if ($counter === 0 || $counter % $limit_commit === 0) {
                if ($counter > 0) {
                    DB::commit();
                    $console->info('Committed chunks');
                }
                DB::beginTransaction();
            }
            $counter++;
            $console->line("Processing product [$product->id] ($counter/$many)");
            try {
                \ProductHelper::buildProductCache($product, false);
            } catch (\Exception $e) {
                $console->error($e->getMessage());
                $console->error($e->getTraceAsString());
            }

        }
        DB::commit();
        $console->info('Committed chunks');
    }


    public function repair_orders_with_error_package_ready()
    {
        $console = $this->getConsole();
        $orders = \Order::withStatus(OrderState::STATUS_READY_PICKUP)->withHistoryStates(OrderState::STATUS_CLOSED)->get();
        foreach ($orders as $order) {
            $counter = 0;
            $history_states = \OrderHistory::where('order_id', $order->id)->where('type', \OrderHistory::TYPE_ORDER)->orderBy('created_at', 'desc')->get();
            foreach ($history_states as $history_state) {
                $counter++;
                if ($counter == 2 and $history_state->status_id == OrderState::STATUS_CLOSED) {
                    $console->line("Found order $order->reference [$order->id]");
                    $order->setStatus(OrderState::STATUS_CLOSED);
                    $console->info("Order migrate to STATUS_CLOSED");
                }
            }
        }
    }


    public function repair_orders_with_error_cancelled()
    {
        $console = $this->getConsole();
        /** @var \Order[] $orders */
        $orders = \Order::withStatus([OrderState::STATUS_READY_PICKUP, OrderState::STATUS_READY])->withHistoryStates(OrderState::STATUS_CANCELED)->get();
        foreach ($orders as $order) {
            $counter = 0;
            $history_states = \OrderHistory::where('order_id', $order->id)->where('type', \OrderHistory::TYPE_ORDER)->orderBy('created_at', 'desc')->get();
            foreach ($history_states as $history_state) {
                $counter++;
                if ($counter == 2 and $history_state->status_id == OrderState::STATUS_CANCELED) {
                    $console->line("Found order $order->reference [$order->id]");
                    $order->setStatus(OrderState::STATUS_CANCELED);
                    $console->info("Order migrate to STATUS_CANCELED");
                }
            }
        }
    }


    public function repair_orders_flag_warning()
    {
        $console = $this->getConsole();
        $min_date = Carbon::now()->addDays(-90)->format('Y-m-d');
        /** @var \Order[] $orders */
        $orders = \Order::
        withStatus([OrderState::STATUS_READY_PICKUP, OrderState::STATUS_READY])
            ->withWarnableFlag()
            ->where('created_at', '<=', $min_date)
            ->get();
        foreach ($orders as $order) {
            $console->line("Found order $order->reference [$order->id]");
            \DB::table('orders')->where('id', $order->id)->update(['flag_warnable' => 1]);
            $console->info('Flag warning disabled');
        }
    }


    public function remove_new_for_products()
    {
        $console = $this->getConsole();
        $filePath = storage_path('import/products_remove_new.txt');
        if (!file_exists($filePath)) {
            $console->error("Could not find any source TXT file at $filePath");
        }

        $lines = explode(PHP_EOL, file_get_contents($filePath));
        $many = count($lines);
        $console->comment("Found $many products to process");
        $counter = 0;
        DB::beginTransaction();
        try {
            foreach ($lines as $id) {
                $id = (int)trim($id);
                if ($id > 0) {
                    $counter++;
                    $console->line("Processing product $id ($counter/$many)");
                    $data = [
                        'is_new' => 0,
                        'new_from_date' => null,
                        'new_to_date' => null,
                    ];
                    DB::table('products')->where(compact('id'))->update($data);
                    DB::table('cache_products')->where(compact('id'))->update(['is_new' => 0]);
                }
            }
            $console->info('-> DONE');
            DB::commit();
        } catch (\Exception $e) {
            $console->error($e->getMessage());
            DB::rollBack();
        }
    }


    public function switch_prices_to_it()
    {
        $console = $this->getConsole();
        $country_id = 10; //ITA
        DB::beginTransaction();
        $ids = Product::lists('id');

        try {
            foreach ($ids as $id) {
                //find the first price_rule
                $conditions = [
                    'country_id' => $country_id,
                    'product_id' => $id,
                ];
                $rule = DB::table('products_specific_prices')->where($conditions)->first();
                if ($rule) {
                    $attributes = \ProductHelper::parsePrices($rule->reduction_value);
                    $price = $attributes['price'];
                    $console->comment("Updating product[$id] with price $price");
                    DB::table('products')->where('id', $id)->update($attributes);
                    DB::table('cache_products')->where('id', $id)->update(compact('price'));
                }
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }


    public function purge_broken_products_images()
    {

        $console = $this->getConsole();

        /** @var \ProductImage[] $images */
        $images = \ProductImage::whereIn('product_id', static function ($query) {
            $query->select('id')->from('products')->whereNull('deleted_at')->where('is_out_of_production', 0);
        })
            ->orderBy('product_id', 'desc')
            ->orderBy('position', 'asc')
            ->get();

        $counter = 0;
        $many = count($images);

        /** @var \ProductImage[] $broken_images */
        $broken_images = [];

        foreach ($images as $image) {
            $counter++;
            $fullPath = $image->full_path;
            $console->line("Checking image #{$image->id} at [$fullPath] ($counter/$many)");
            if ($image->fileExists()) {
                if ($image->isImage()) {
                    $console->info("LGTM => Image is valid!");
                } else {
                    $console->error("FAILED => Attention: the file is not a proper image!");
                    $broken_images[] = $image;
                }
            } else {
                $console->comment("SKIPPED => File does not exist");
            }
        }

        if (count($broken_images)) {
            $many = count($broken_images);
            $console->error("I HAVE FOUND $many BROKEN IMAGES!");
            foreach ($broken_images as $broken_image) {
                $console->line($broken_image->full_path);
            }
            $confirm = $this->getConsole()->confirm('Do You want to purge all broken images?');
            if ($confirm) {
                foreach ($broken_images as $broken_image) {
                    try {
                        $console->line("Purging image $broken_image->full_path");
                        $broken_image->purgeFromDisk();
                        $console->info('SUCCESS');
                    } catch (\Exception $e) {
                        $console->error('ERROR => ' . $e->getMessage());
                    }
                }
            }
        } else {
            $console->info('ALL PERFECT => There are no broken images!');
        }
    }


    public function product()
    {
        $console = $this->getConsole();
        $id = $console->ask('Please provide a product ID');

        $choice = $console->choice('What payload You want to see?', [
            'getObj',
            'getPublicObj',
            'getFullProduct',
        ]);
        $console->line($choice);

        audit_watch();
        $product = \Product::$choice($id);

        print_r($product->toArray());
    }


    public function fix_ws2_case_x()
    {
        $console = $this->getConsole();
        // ordini annullati, con riga in stato X
        /** @var Order[] $orders */
        $orders = Order::
        withStatus(OrderState::STATUS_CANCELED)
            ->withOrderSlipDetailType([OrderSlipDetail::TYPE_SHOP_NOT_FOUND])
            ->withOrderSlipDetailType([OrderSlipDetail::TYPE_CANCELLED])
            ->orderBy('id', 'desc')
            ->get();

        $many = count($orders);
        $console->comment("Found $many orders");
        $counter = 0;
        $rows = [['OrderId', 'OrderReference', 'OrderStatus', 'PaymentStatus', 'AvailabilityStore', 'BestShop']];
        foreach ($orders as $order) {
            $counter++;
            $console->line("Processing order $order->reference ($order->id) [$counter/$many]");
            //ok, now find the best shop
            $best_shop = OrderHelper::findBestShopByOrderDetails($order);
            if ($best_shop !== '' and $best_shop !== null) {
                $console->info("Found new best-shop: $best_shop");
            } else {
                $console->error('Cannot find a best-shop for this order');
            }
            $shop = $order->getAvailabilityShop();
            $rows[] = [
                $order->id,
                $order->reference,
                $order->statusName(),
                $order->paymentStatusName(),
                ($shop) ? $shop->code : null,
                $best_shop,
            ];
        }

        $sourcePath = storage_path('morellato/csv/export');
        $filename = $sourcePath . "/orders_with_X_" . date('Ymd') . '.csv';
        touch($filename);

        $console->line("Exporting into CSV...");

        //we create the CSV into memory
        $csv = Writer::createFromPath($filename);

        foreach ($rows as $row) {
            $csv->insertOne($row);
        }
    }


    public function silent_orders()
    {
        $console = $this->getConsole();
        // ordini annullati, con riga in stato X
        /** @var Order[] $orders */
        $orders = Order::
        withOrderSlipDetailReason([OrderSlipDetail::REASON_NOT_HANDLED])
            ->withOrderSlipDetailType([OrderSlipDetail::TYPE_CANCELLED])
            ->orderBy('id', 'desc')
            ->get();

        $many = count($orders);
        $console->comment("Found $many orders");
        $counter = 0;
        $rows = [['OrderId', 'OrderReference', 'OrderStatus', 'PaymentStatus']];
        foreach ($orders as $order) {
            $counter++;
            $console->line("Processing order $order->reference ($order->id) [$counter/$many]");
            $rows[] = [
                $order->id,
                $order->reference,
                $order->statusName(),
                $order->paymentStatusName(),
            ];
        }

        $sourcePath = storage_path('morellato/csv/export');
        $filename = $sourcePath . "/orders_silent_" . date('Ymd') . '.csv';
        touch($filename);

        $console->line("Exporting into CSV...");

        //we create the CSV into memory
        $csv = Writer::createFromPath($filename);

        foreach ($rows as $row) {
            $csv->insertOne($row);
        }
    }


    public function blocked_orders()
    {
        $console = $this->getConsole();
        // ordini annullati, con riga in stato X
        /** @var Order[] $orders */
        $orders = Order::
        withStatus(OrderState::STATUS_WORKING)
            ->withOrderSlipDetailReason([
                OrderSlipDetail::REASON_NOT_HANDLED,
                OrderSlipDetail::REASON_CANCELLED_NOT_PICKED,
                OrderSlipDetail::REASON_CANCELLED_NOT_PICKED_ALT,
                OrderSlipDetail::REASON_CANCELLED_NOT_FOUND,
                OrderSlipDetail::REASON_CANCELLED_NOT_FOUND_ALT
            ])
            ->orderBy('id', 'desc')
            ->get();

        $many = count($orders);
        $console->comment("Found $many orders");
        $counter = 0;
        $rows = [['OrderId', 'OrderReference', 'OrderStatus', 'PaymentStatus']];
        foreach ($orders as $order) {
            $counter++;
            $console->line("Processing order $order->reference ($order->id) [$counter/$many]");
            $rows[] = [
                $order->id,
                $order->reference,
                $order->statusName(),
                $order->paymentStatusName(),
            ];
        }

        $sourcePath = storage_path('morellato/csv/export');
        $filename = $sourcePath . "/orders_blocked_" . date('Ymd') . '.csv';
        touch($filename);

        $console->line("Exporting into CSV...");

        //we create the CSV into memory
        $csv = Writer::createFromPath($filename);

        foreach ($rows as $row) {
            $csv->insertOne($row);
        }
    }


    public function order_without_shops()
    {
        $console = $this->getConsole();
        /** @var Order[] $orders */
        $orders = Order::migrationableToWorking()->orderBy('id', 'desc')->get();

        $many = count($orders);

        $console->line("Found $many orders");

        foreach ($orders as $order) {

            try {
                $console->line("Order $order->id - $order->reference");

                $products = $order->getProducts();

                $order_availability_mode = $order->availability_mode;
                $order_availability_shop = (string)$order->availability_shop_id;

                $checksums = [
                    Order::MODE_LOCAL => 0,
                    Order::MODE_SHOP => 0,
                    Order::MODE_ONLINE => 0
                ];
                $checksums_keys = array_keys($checksums);
                $shops = [];

                foreach ($products as $product) {
                    $availability_mode = $product->availability_mode;
                    $availability_shop_id = (string)$product->availability_shop_id;
                    $warehouse = $product->warehouse;

                    if (in_array($availability_mode, $checksums_keys, true)) {
                        $checksums[$availability_mode]++;
                    }
                    if ($availability_mode === Order::MODE_STAR) {
                        foreach ($checksums as $key => $value) {
                            $checksums[$key]++;
                        }
                    }
                    if ($availability_mode === Order::MODE_SHOP and $availability_shop_id !== '') {
                        $pivot = (int)array_get($shops, $availability_shop_id, 0);
                        $pivot++;
                        array_set($shops, $availability_shop_id, $pivot);
                    }
                    if ($availability_mode === Order::MODE_SHOP and $availability_shop_id === '') {
                        array_set($shops, 'XXX', 1);
                    }
                }

                $total = count($products);
                $console->comment('Order_availability_mode: ' . $order_availability_mode . ' - Order_availability_shop: ' . $order_availability_shop);
                $console->line('Products: ' . $total);
                //print_r($checksums);
                //print_r($shops);
                //decision making
                if ($order_availability_mode === Order::MODE_SHOP and $order_availability_shop === '') {
                    if ($total === $checksums[Order::MODE_SHOP] and count($shops) === 1) {
                        $given_shop = array_keys($shops)[0];
                        if ($given_shop !== 'XXX') {
                            $console->info('***** THIS ORDER SHOULD BE SET TO => ' . $given_shop);
                            DB::table('orders')->where('id', $order->id)->update(['availability_shop_id' => $given_shop]);
                        }
                    }
                }
            } catch (\Exception $e) {
                audit_exception($e);
                $console->error($e->getMessage());
            }
        }
    }


    public function report_orders_best_shops()
    {
        $console = $this->getConsole();
        $feeder = new OrderFeedRepository();
        $feeder->setConsole($console);
        $query = Order::where('created_at', '>=', '2019-11-15');
        $feeder->bestShops($query);
    }


    public function report_orders_working_30_nov()
    {
        $console = $this->getConsole();
        $feeder = new OrderFeedRepository();
        $feeder->setConsole($console);
        $query = Order::where('created_at', '>=', '2019-11-01');
        $feeder->working($query);
    }


    public function report_orders_working_without_slips()
    {
        $console = $this->getConsole();
        $feeder = new OrderFeedRepository();
        $feeder->setConsole($console);
        $query = Order::where('created_at', '>=', '2019-11-01');
        $feeder->workingWithoutSlips($query);
    }


    public function report_orders_with_duplicated_transactions()
    {
        $console = $this->getConsole();

        $query = "SELECT transaction_id, count(id) as aggregate from transactions GROUP BY transaction_id HAVING aggregate > 1 order by aggregate desc";
        $duplicates = DB::select($query);

        $errors = [];

        foreach ($duplicates as $duplicate) {
            $transaction_id = $duplicate->transaction_id;
            $times = $duplicate->aggregate;

            /** @var Order[] $orders */
            $orders = Order::where('created_at', '>=', '2019-11-01')
                ->whereIn('id', function ($query) use ($transaction_id) {
                    $query->select('order_id')->from('transactions')->where('transaction_id', $transaction_id);
                })
                ->orderBy('id', 'desc')
                //->take(20)
                ->get();


            foreach ($orders as $order) {
                $console->line("Order [$order->id] has duplicated transaction [$transaction_id] ($times)");
                $order_amount = (float)\Format::money($order->total_order);
                $order_id = $order->id;
                $order_transactions = DB::table('transactions')->where(compact('transaction_id', 'order_id'))->get();
                foreach ($order_transactions as $order_transaction) {
                    $transaction_amount = (float)\Format::money($order_transaction->amount);
                    $console->comment("Order total: $order_amount | Transaction amount: $transaction_amount");
                    if ($transaction_amount != $order_amount) {
                        $console->error("Transaction: $transaction_id for order $order_id is invalid!");
                        $errors[] = compact('transaction_id', 'order_id', 'order_amount', 'transaction_amount');
                    } else {
                        $console->info("Transaction: $transaction_id for order $order_id is valid!");
                    }
                }
            }
        }


        $header = [
            'Data',
            'N_Ordine',
            'ID_Ordine',
            'Status_Ordine',
            'Status_Pagamento',
            'Importo',
            'Url',
            'Master',
            'Tipo_Ordine',
            'Transaction_ID',
            'Gateway',
            'Transaction_Amount',
            'Transaction_Date',
            'Transaction_Status',
            'Transaction_Status_Date',
        ];

        $export_rows = [$header];

        foreach ($errors as $error) {
            /** @var Order $order */
            $order = Order::getObj($error['order_id']);
            $order_id = $error['order_id'];
            $transaction_id = $error['transaction_id'];
            $transaction = DB::table('transactions')->where(compact('transaction_id', 'order_id'))->first();

            $export_row = [
                'Data' => $order->created_at->format('Y-m-d'),
                'N_Ordine' => $order->reference,
                'ID_Ordine' => $order->id,
                'Status_Ordine' => $order->statusName(),
                'Status_Pagamento' => $order->paymentStatusName(),
                'Importo' => $order->total_order,
                'Url' => $order->getBackendLinkAttribute(),
                'Master' => $order->isMaster() ? 'SI' : 'NO',
                'Tipo_Ordine' => $order->availability_mode,
                'Transaction_ID' => $transaction_id,
                'Gateway' => $transaction->module,
                'Transaction_Amount' => $transaction->amount,
                'Transaction_Date' => $transaction->created_at,
                'Transaction_Status' => $transaction->status,
                'Transaction_Status_Date' => $transaction->response_at,
            ];
            $export_rows[] = $export_row;
        }

        $platform = config('app.project');

        $sourcePath = storage_path('morellato/csv/export');
        $filename = $sourcePath . "/{$platform}_orders_with_duplicated_transactions_" . date('Ymd') . '.csv';
        if (file_exists($filename))
            unlink($filename);

        touch($filename);

        $console->line("Exporting into CSV...");

        //we create the CSV into memory
        $csv = Writer::createFromPath($filename);
        $csv->setDelimiter(';');
        $csv->setEncodingFrom('UTF-8');


        foreach ($export_rows as $row) {
            $csv->insertOne($row);
        }

        $console->info("File is available at: $filename");
    }


    public function master_orders_auto_status()
    {
        $console = $this->getConsole();
        $feeder = new OrderFeedRepository();
        $feeder->setConsole($console);
        $query = Order::where('created_at', '>=', '2019-11-01');
        $feeder->masterOrdersAutoStatus($query);
    }


    public function export_orders_for_gls()
    {
        $console = $this->getConsole();
        $feeder = new OrderFeedRepository();
        $feeder->setConsole($console);
        $query = Order::where('created_at', '>=', '2019-11-01');
        $feeder->gls($query);
    }

    public function apply_price_rule()
    {
        audit_watch();
        $console = $this->getConsole();
        $id = $console->ask('Price rule ID');
        $priceRule = \PriceRule::getObj(($id));
        if (null === $priceRule) {
            $console->error('Cannot find any model with given ID');
            return;
        }

        $rules = $priceRule->getRuleExceptions();

        $rr = new RuleResolver();
        $rr->setDebug(true);
        $rr->setRules($priceRule->conditions);
        $product_ids = $rr->bindProducts();

        $real_ids = \Product::whereIn('id', $product_ids)->lists('id');
        print_r($real_ids);
    }

    public function upsert_builder_products()
    {
        DB::beginTransaction();
        BuilderProduct::whereYear('deleted_at', '=', 2020)->restore();
        BuilderProduct::whereIn('product_id', function ($query) {
            $query->select('id')->from('products')->whereNull('deleted_at')->where(function ($subquery) {
                $subquery->where('is_out_of_production', 1)->orWhere('is_soldout', 1);
            });
        })->delete();
        DB::commit();
    }

    public function insert_missing_combinations()
    {
        $console = $this->getConsole();

        /** @var Product[] $products */
        $products = Product::whereIn('id', function ($query) {
            $query->select('product_id')->from('products_combinations');
        })
            ->orderBy('id', 'desc')
            ->get();

        $many = count($products);
        $counter = 0;
        $console->info("Found $many products with combinations");

        $variant_attribute = 'size';
        $attribute_model = \Attribute::whereCode($variant_attribute)->first();
        /** @var \Attribute $attribute */
        $attribute = \Attribute::getObj($attribute_model->id);

        foreach ($products as $product) {
            $counter++;

            $console->line("Handling product $counter/$many");

            $combination = \ProductCombination::where('product_id', $product->id)->where(function ($query) use ($product) {
                $query->where('sku', $product->sku)->orWhere('sap_sku', $product->sap_sku)->orWhere('sku', $product->sap_sku);
            })->first();

            if (null === $combination) {
                $console->comment("Found missing combination for $product->sku ($product->id)");

                $product_variant_options = DB::table('products_attributes')->where(['product_id' => $product->id, 'attribute_id' => $attribute->id])->lists('attribute_val');
                if (empty($product_variant_options)) {
                    $console->error('Could not find any variant for combination in context product');
                    continue;
                }

                /** @var \AttributeOption $attribute_option */
                $attribute_option = \AttributeOption::getObj($product_variant_options[0]);
                if (null === $attribute_option) {
                    $console->error('Could not find any AttributeOption for selected variant in context product');
                    continue;
                }

                $data = [
                    'name' => $attribute->name . ': ' . $attribute_option->uname,
                    'product_id' => $product->id,
                    'sku' => $product->sku,
                    'sap_sku' => $product->sap_sku,
                    'ean13' => $product->ean13,
                    'weight' => $product->weight,
                    'buy_price' => $product->price,
                    'price' => 0,
                ];

                $combination_id = \ProductCombination::insertGetId($data);

                $pivot_data = [
                    'attribute_id' => $attribute->id,
                    'option_id' => $attribute_option->id,
                    'combination_id' => $combination_id,
                ];

                DB::table('products_combinations_attributes')->insert($pivot_data);

                $console->info("Combination successfully created with id $combination_id");

            } else {
                $console->line("Found combination $combination->sku ($combination->id). Skipping insert");
            }
        }
    }


    public function fix_misplaced_images()
    {
        $console = $this->getConsole();
        /** @var ImageGrabber $grabber */
        $grabber = \App::make(ImageGrabber::class, [$console]);
        $tmpStoragePath = config('ftp.imageTmpStorage') . '/';
        $publicStoragePath = public_path('assets/products/');
        $copyPath = storage_path('logs/');

        $copy = false;
        $fix = true;

        $images = DB::table('images')
            ->whereRaw("id <> (REPLACE(filename,'.jpg','') * 1) and date(created_at) >= '2020-02-01'")
            //->where('product_id', 71764)
            ->orderBy('product_id', 'desc')
            ->orderBy('position', 'asc')
            ->get();

        $many = count($images);
        $console->info("Found $many images to fix...");
        $counter = 0;

        $products = [];
        foreach ($images as $image) {
            if (isset($products[$image->product_id])) {
                $products[$image->product_id][] = $image;
            } else {
                $products[$image->product_id] = [$image];
            }
        }
        $products_meta = Product::whereIn('id', array_keys($products))->get(['id', 'sku', 'sap_sku']);
        $skus = [];
        foreach ($products_meta as $meta) {
            $skus[$meta->id] = \Utils::getSapSku($meta->sku, $meta->sap_sku);
        }
        $many_products = count($products);
        $console->info("...grouped in $many_products products");

        foreach ($products as $product_id => $items) {
            $counter++;
            $sku = $skus[$product_id];
            $console->comment("Fixing product $product_id [$sku] ($counter/$many_products)");
            $vars = $grabber->getVarsBySku($sku);
            print_r($items);

            $many = count($vars);
            $console->comment("Found [$many] vars for product [$sku] ($counter/$many_products)");

            if (!empty($vars)) {

                foreach ($vars as $var) {

                    $imageFilename = null;

                    $media = DB::connection('repository')
                        ->table('media')
                        ->where('code', $sku)
                        ->where('var', $var)
                        ->whereIn('CONTENT_TYPE', ['image/jpeg', 'image/jpg'])
                        ->select('FILENAME', 'SIZE', 'UPLOADED', 'CONTENT_TYPE', 'ENCODED_FILENAME', 'VAR', 'VERSION')
                        ->orderBy('VERSION', 'DESC')
                        ->first();

                    $fileType = $grabber->getExtension($media->CONTENT_TYPE);
                    $mediaFilename = $media->FILENAME;

                    $console->table(
                        ['FILENAME', 'SIZE', 'CONTENT_TYPE', 'ENCODED_FILENAME', 'VAR', 'VERSION'],
                        [[$media->FILENAME, $media->SIZE, $media->CONTENT_TYPE, $media->ENCODED_FILENAME, $media->VAR, $media->VERSION]]
                    );

                    $resourceName = $tmpStoragePath . $media->FILENAME;
                    $existingFile = null;
                    $existingIdFile = null;
                    $selectedItem = null;

                    foreach ($items as $item) {
                        if ((int)$item->var === (int)$var) {
                            $existingFile = $publicStoragePath . $item->filename;
                            $existingIdFile = $publicStoragePath . $item->id . '.jpg';
                            $selectedItem = $item;
                        }
                    }

                    if (file_exists($resourceName)) {
                        $console->info("Resource File exists at $resourceName");
                    } else {
                        $console->error("Resource File does not exist at $resourceName");
                    }

                    if ($existingFile and file_exists($existingFile)) {
                        $console->info("Current File exists at $existingFile");
                    } else {
                        $console->error("Current File does not exist at $existingFile");
                    }

                    if ($copy) {
                        copy($resourceName, $copyPath . last(explode('/', $resourceName)));
                        copy($existingFile, $copyPath . last(explode('/', $existingFile)));
                        copy($existingIdFile, $copyPath . last(explode('/', $existingIdFile)));
                    }

                    if ($existingIdFile and file_exists($existingIdFile)) {
                        $console->info("Maybe File exists at $existingIdFile");
                        $params = [
                            'size' => filesize($existingIdFile),
                            'md5' => md5_file($existingIdFile),
                            'time' => filemtime($existingIdFile),
                        ];
                        print_r($params);
                        if ($fix && $selectedItem) {
                            DB::table('images')->where('id', $selectedItem->id)->update([
                                'filename' => last(explode('/', $existingIdFile)),
                                'updated_at' => date('Y-m-d H:i:s'),
                            ]);
                            $console->info("=> IMAGE $selectedItem->id FIXED");
                        }
                    } else {
                        $console->error("Maybe File does not exist at $existingIdFile");
                    }

                }
            } else {
                $msg = "No images found for [$sku] in the repository";
                $console->error($msg);
            }
        }
    }


    public function fix_products_combinations_without_size_mapping()
    {
        $console = $this->getConsole();

        //create mapping for size options
        $mapping = [];
        $attribute = \Attribute::whereCode('size')->first();
        /** @var \AttributeOption[] $options */
        $options = \AttributeOption::where('attribute_id', $attribute->id)->rows()->get();
        foreach ($options as $option) {
            if ($option->uname === $option->name) {
                $mapping[$option->uname] = $option->id;
            } else {
                $mapping[$option->name] = $option->id;
                $mapping[$option->uname] = $option->id;
            }
        }
        /** @var \ProductCombination[] $combinations */
        $combinations = \ProductCombination::whereNotIn('id', function ($query) {
            $query->select('combination_id')->from('products_combinations_attributes');
        })
            ->orderBy('product_id')
            ->orderBy('id')
            ->get();

        foreach ($combinations as $combination) {
            $value = trim(str_replace(['Misura:', 'Misura :', 'Size:', 'Size :'], '', $combination->name));
            if (isset($mapping[$value])) {
                $console->info("Found value {$mapping[$value]} for {$combination->name}({$combination->id})");
                $data = [
                    'attribute_id' => $attribute->id,
                    'option_id' => $mapping[$value],
                    'combination_id' => $combination->id,
                ];
                DB::table('products_combinations_attributes')->insert($data);
            } else {
                $console->error("NOT Found value for {$combination->name}({$combination->id})");
            }
        }
    }

    public function scp_move_files()
    {
        $files = [
            'vendor/bladecommmerce/services/src/services/Morellato/Repositories/ToolsRepository.php',
            'vendor/bladecommmerce/services/src/services/Morellato/Soap/Response/OrderFlowResponse.php',
        ];
        $console = $this->getConsole();
        $command = 'pscp -i [PPK] [SOURCE_FILE] [USER]@[HOST]:[ROOT]/[TARGET_FILE]';
        $servers = json_decode(file_get_contents(base_path('servers.json')));
        $ppk = $servers->config->ppk;
        $user = $servers->config->user;
        foreach ($servers->items as $server) {
            $data = compact('ppk', 'user');
            $data['host'] = $server->host;
            $data['root'] = $server->root;
            foreach ($files as $file) {
                $data['source_file'] = base_path() . '/' . $file;
                $data['target_file'] = $file;
                $cmd = str_replace(array_map(static function ($key) {
                    return '[' . strtoupper($key) . ']';
                }, array_keys($data)), array_values($data), $command);
                $console->line($cmd);
                shell_exec($cmd);
            }
        }
    }


    public function merge_trends()
    {
        $console = $this->getConsole();
        $trend_id = $console->ask('Please provide the target trend id');
        $trend = \Trend::find($trend_id);
        if (null === $trend) {
            $console->error('Could not find any trend with given ID');
            return;
        }

        $source_list = $console->ask('Please provide the source trend ids, comma separated');
        $source_ids = explode(',', $source_list);
        foreach ($source_ids as $id) {
            $trend = \Trend::find($id);
            if (null === $trend) {
                $console->error("Could not find any trend with ID [$id]");
                return;
            }
        }

        $product_ids = DB::table('trends_products')->whereIn('trend_id', $source_ids)->lists('product_id');

        DB::beginTransaction();
        foreach ($product_ids as $product_id) {
            try {
                DB::table('trends_products')->insert(compact('trend_id', 'product_id'));
            } catch (\Exception $e) {
                $console->comment($e->getMessage());
            }
        }
        DB::commit();

        $console->info('DONE');
    }

    public function csv_orders_gls()
    {
        $dates = [
            [Carbon::parse('2019-01-01'), Carbon::parse('2019-10-31'), 'shop', 'shop'],
            [Carbon::parse('2019-01-01'), Carbon::parse('2019-10-31'), 'shop', 'customer'],
            [Carbon::parse('2019-01-01'), Carbon::parse('2019-10-31'), 'online', 'shop'],
            [Carbon::parse('2019-01-01'), Carbon::parse('2019-10-31'), 'online', 'customer'],


            [Carbon::parse('2019-11-01'), Carbon::parse('2019-12-31'), 'shop', 'shop'],
            [Carbon::parse('2019-11-01'), Carbon::parse('2019-12-31'), 'shop', 'customer'],
            [Carbon::parse('2019-11-01'), Carbon::parse('2019-12-31'), 'online', 'shop'],
            [Carbon::parse('2019-11-01'), Carbon::parse('2019-12-31'), 'online', 'customer'],
        ];
        foreach ($dates as $period) {
            $this->export_csv_orders_gls($period[0], $period[1], $period[2], $period[3]);
        }
    }

    private function export_csv_orders_gls(Carbon $from, Carbon $to, $source, $destination)
    {
        audit_watch();
        $console = $this->getConsole();
        $start = $from->format('Y-m-d');
        $end = $to->format('Y-m-d');
        $name = config('remote.internal.from');

        $sourcePath = storage_path('morellato/csv/export/');
        $filename = $sourcePath . "{$name}_orders_gls_{$source}--{$destination}_{$start}--{$end}.csv";
        if(file_exists($filename)){
            unlink($filename);
        }
        touch($filename);
        $start_time = $start . ' 00:00:00';
        $end_time = $end . ' 23:59:59';

        $console->line("Exporting orders into file [$filename]...");

        //we create the CSV into memory
        $csv = Writer::createFromPath($filename);
        $csv->setDelimiter(';');
        $csv->setNewline("\r\n");

        //we insert the CSV header
        $csv->insertOne(['id', 'reference', 'sales_id', 'delivery_id', 'withdrawal_id', 'created_at', 'source', 'destination']);

        /** @var Builder $builder */
        $builder = Order::with('order_slip')
            ->available()
            ->withHistoryStates(OrderState::STATUS_WORKING, [$start_time, $end_time])
            ->orderBy('created_at', 'desc');

        if ($source === 'online') {
            $builder->withoutAvailabilityStore();
        }

        if ($source === 'shop') {
            $builder->withAvailabilityStore();
        }

        if ($destination === 'shop') {
            $builder->withDeliveryStore();
        }

        if ($destination === 'customer') {
            $builder->withoutDeliveryStore();
        }

        /** @var Order[] $orders */
        $orders = $builder->get();

        //audit($orders->toArray());
        $lines = [];
        foreach ($orders as $order) {
            $console->line("Exporting order $order->reference ($order->id)");
            $lines[] = [
                $order->id,
                $order->reference,
                (isset($order->order_slip)) ? $order->order_slip->sales_id : null,
                (isset($order->order_slip)) ? $order->order_slip->delivery_id : null,
                (isset($order->order_slip)) ? $order->order_slip->withdrawal_id : null,
                $order->created_at,
                $source,
                $destination,
            ];
        }
        $csv->insertAll($lines);
    }
}