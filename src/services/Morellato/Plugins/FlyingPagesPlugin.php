<?php

namespace services\Morellato\Plugins;

use Plugins\Plugin;
use services\Traits\TraitAttributes;
use Illuminate\Support\Str;
use Frontend\Module;
use Event;
use services\Traits\TraitLocalDebug;
use Request;

class FlyingPagesPlugin extends Plugin
{

    use TraitAttributes;
    use TraitLocalDebug;

    /**
     * @var bool
     */
    protected $debug = false;

    /**
     * @var bool
     */
    protected $localDebug = false;

    /**
     * @var bool
     */
    protected $shouldRender = false;

    /**
     * @var string
     */
    protected $config_namespace = 'flyingPages';

    /**
     * @var array
     */
    protected $scopeWhiteList = [
        'home',
        'page',
        'section',
        'catalog',
        'product',
        'promo',
        'sitemap',
    ];

    /**
     * @var array
     */
    protected $settings = [];

    /**
     * QuickLinksPlugin constructor.
     */
    function __construct()
    {
        $this->debug = config('app.debug');
        $this->debug = false;
        $this->setupFrontend();
    }

    function registerEvents()
    {
        parent::registerEvents();
        Event::listen('frontend.template.afterFooter', "services\\Morellato\\Plugins\\FlyingPagesPlugin@onFrontendFooter");
    }

    /**
     * Print javascript on frontend footer
     */
    function onFrontendFooter()
    {
        $this->route();
        $code = $this->render();
        return $code;
    }

    /**
     * Homepage Tag
     * Category / Listing Tag
     * Product Tag
     * Page Tag
     * Section Tag
     * Promo Tag
     * Nav Tag
     */
    protected function route()
    {
        $scopeName = $this->getParam('scopeName');

        $this->audit($scopeName, 'scopeName');
        //$shouldRender = in_array($scopeName, $this->scopeWhiteList);
        $shouldRender = true;
        $this->audit($shouldRender, 'shouldRender');

        $this->shouldRender = $shouldRender;
    }


    /**
     * @return string
     * @throws \Exception
     */
    protected function render()
    {
        $tag = $this->getTag();

        $this->audit($tag, __METHOD__);

        return $tag;
    }


    /**
     * @return string
     * @throws \Exception
     */
    protected function getTag()
    {
        if (false == $this->shouldRender)
            return '';

        $options = $this->getTagConfig();
        $this->audit($options, 'OPTIONS');

        $ignore = json_encode($options['ignore_keywords']);

        $script_uri = \Site::rootify('themes/admin/assets/js/echo/shared/flying-pages.min.js');

        $code = <<<CODE
window.addEventListener("load", () => {
    console.log('Flying Pages activated!');
    flyingPages({
        delay: {$options['delay']},
        ignoreKeywords: $ignore,
        maxRPS: {$options['max_rps']},
        hoverDelay: {$options['hover_delay']}
    });
});
CODE;

        $tag = <<<TAG
<!-- FlyingPages setup -->
<script defer src="$script_uri"></script>
<script>
$code
</script>
TAG;

        $string = (string)$tag;
        return $string;
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function getTagConfig()
    {
        $options = (array)$this->config(null); //auto-read the 'plugins_settings.flyingPages array'
        $siteUrl = config('app.url');

        $options['origins'] = true;

        /*$ignores = array(
            // Do not preload feed links.
            preg_quote('/feed/', '/'),
            preg_quote('javascript:;', '/'),
            preg_quote('/?_theme=mobile', '/'),
            preg_quote('/?_theme=desktop', '/'),

            // Do not preload self, including self with hash.
            '^https?:\/\/[^\/]+' . preg_quote(Request::fullUrl(), '/') . '(#.*)?$', // phpcs:ignore

            // Don't pre-fetch links to the admin since they could be nonce links.
            '^' . preg_quote("$siteUrl/", '/') . '(admin|themes|rest|assets|static|player|packages|cache)',

            // Don't pre-fetch links to PHP, CSV, TXT, XML, JSON files
            '^' . preg_quote($siteUrl, '/') . '[^?#]+\.(php|csv|txt|xml|json)',
        );*/
        $ignores = [
          'feed',
          'javascript',
          '_theme',
          'admin',
          'themes',
          'rest',
          'assets',
          'static',
          'player',
          'packages',
          'cache',
        ];

        //ignore all links to 'Nav' controllers, except few ones
        $ids = $this->getInvalidNavsLink();
        foreach ($ids as $id) {
            $ignores[] = ltrim(\Link::to('nav', $id), '/') ;
        }

        $options['ignore_keywords'] = array_merge($ignores, $options['ignore_keywords']);

        return $options;
    }

    /**
     * @return array
     */
    protected function getInvalidNavsLink()
    {
        $key = $this->config_namespace . '-invalid-nav-ids';
        if (\Cache::has($key))
            return \Cache::get($key);

        $invalidNavs = ['cart', 'checkout', 'order', 'confirm_order', 'wishlist', 'account', 'search', 'compare', 'register', 'login'];
        $ids = \Nav::whereIn('shortcut', $invalidNavs)->lists('id');
        \Cache::put($key, $ids, 60 * 6);
        return $ids;
    }

    /**
     * @param $object
     * @return string
     */
    private function toJson($object)
    {
        $flags = $this->debug ? JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK : JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK;
        return (is_array($object) or is_array($object)) ? json_encode($object, $flags) : null;
    }


}