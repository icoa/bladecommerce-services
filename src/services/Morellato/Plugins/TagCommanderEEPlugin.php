<?php

namespace services\Morellato\Plugins;


use Carbon\Carbon;
use Illuminate\Support\Facades\Event;

use Analytics;
use Plugins\Plugin;
use Product;
use services\Morellato\Plugins\CommanderActPlugin;
use Exception;
use services\Traits\TraitAttributes;
use services\Traits\TraitLocalDebug;
use View;
use services\TagCommander\TagCommanderOrderRepository;

class TagCommanderEEPlugin extends Plugin
{

    use TraitLocalDebug;
    use TraitAttributes;

    public $list = null;
    protected $localDebug = false;
    protected $debug = false;

    function registerEvents()
    {
        parent::registerEvents();

        $className = self::class;

        //FRONTEND
        Event::listen('frontend.template.afterHead', "$className@onFrontendHead");
        Event::listen('frontend.template.afterFooter', "$className@onFrontendFooter");
        Event::listen('frontend.product.afterList', "$className@onProductAfterList");
        Event::listen('frontend.cart.addProduct', function (&$product, $qty) {
            return $this->onCartAddProduct($product, $qty);
        });
        Event::listen('frontend.cart.updateProduct', function (&$product, $qty) {
            return $this->onCartUpdateProduct($product, $qty);
        });
        Event::listen('frontend.cart.removeProduct', function (&$product, &$qty) {
            return $this->onCartRemoveProduct($product, $qty);
        });


        //BACKEND
        //Event::listen('admin.template.afterHead', "$className@onBackendHead");
        Event::listen('admin.template.afterFooter', "$className@onBackendFooter");
    }


    function onBackendHead()
    {
        $this->audit(__METHOD__);
        //Analytics::ga('set', 'nonInteraction', true);
        //return (Analytics::isActive() AND $this->isBackendValidScope()) ? Analytics::render() : null;
    }

    function onBackendFooter()
    {
        $this->audit(__METHOD__);
        $this->debug = config('app.debug');
        return (Analytics::isActive() AND $this->isBackendValidScope()) ? $this->renderBackendFooter() : null;
    }

    private function isBackendValidScope()
    {
        $scope = \AdminTpl::getScope();
        $this->audit($scope, __METHOD__, 'SCOPE');
        $valid = ['DashboardController', 'Echo1\Admin\DashboardController'];
        return (in_array($scope, $valid));
    }

    protected function getBackendTag()
    {
        $repository = new TagCommanderOrderRepository();
        //$this->audit($this->debug, 'debug');
        //$rendering = ($this->debug) ? $repository->setStartDate(Carbon::now()->addYear(-1))->make(true) : $repository->make();
        $rendering = $repository->make();

        if (is_null($rendering)) {
            $speech = $repository->getSpeech();
            return "<!-- RECORDED SPEECH:\n$speech -->";
        }
        $track = $rendering;
        $debug = $this->debug;
        $tag = View::make('plugins.tagCommander', compact('track', 'debug'));
        $string = (string)$tag;
        $speech = $repository->getSpeech();
        return "<!-- RECORDED SPEECH:\n$speech -->\n{$string}\n";
    }

    protected function renderBackendFooter()
    {
        $script = $this->getBackendTag();
        $className = self::class;
        return "<!-- START $className -->\n{$script}\n<!-- EOF $className -->";
    }


    function onFrontendHead()
    {
        $this->audit(__METHOD__);
        return Analytics::isActive() ? Analytics::renderSimple() : null;
    }

    function onFrontendFooter()
    {
        $this->audit(__METHOD__);

        if (Analytics::isActive() == false) {
            return null;
        }

        $this->setList();

        $js = [];
        $listeners = [
            'onCartAdd' => 'MBG.onCartAdd',
            'onCartUpdate' => 'MBG.onCartUpdate',
            'onCartDelete' => 'MBG.onCartDelete',
        ];

        $scope = \FrontTpl::getScopeName();

        //FUNNEL CART + CHECKOUT
        if ($scope == 'checkout' OR $scope == 'cart') {
            $step = \CartManager::getStepInt();
            $stepName = \CartManager::getCheckoutStep();
            switch ($stepName) {
                case 'cart':
                    $option = "Cart #" . \CartManager::getTransactionId();
                    break;
                case 'auth':
                case 'reauth':
                    $option = false;
                    break;
                case 'shipping':
                    $option = "Guest checkout: " . (\CartManager::isGuestCheckout() ? 'YES' : 'NO');
                    break;
                case 'carrier':
                    //$option = "Carrier: ".\CartManager::getCarrierName();
                    $option = false;
                    break;
                case 'confirm':
                    $option = "Payment: " . \CartManager::getPaymentName();
                    break;
            }
            $json = $this->toJson(['step' => $step, 'option' => $option]);
            $js[] = "MBG.addCheckout($json);";

            $listeners['onCartOptionUpdate'] = 'MBG.onCartOptionUpdate';
            $listeners['onCheckoutOptionUpdate'] = 'MBG.onCartOptionUpdate';
            $listeners['onCheckoutStep'] = 'MBG.onCheckoutStep';
        }

        //KEEP THIS FOR ORDER FLAGS
        if ($scope == 'confirm_order') {
            $order = \FrontTpl::getData('order');
            $paymentState = \FrontTpl::getData('paymentState');
            $payment = ($order) ? $order->getPayment() : null;
            if ($payment and $payment->online == 1) {
                if ($paymentState and $paymentState->failed == 0) {
                    $wrappedOrder = $this->wrapOrder($order);
                    if ($wrappedOrder) {
                        $order->setSendFlag('purchase');
                    }
                }
            }
        }

        //PROMOTIONS
        if ($scope == 'promotions') {
            $promotions = \FrontTpl::getData('promotions');
            if ($promotions) {
                $wrappedPromotions = [];
                foreach ($promotions as $promo) {
                    $wrappedPromotions[] = $this->wrapPromotion($promo);
                }
                $json = $this->toJson($wrappedPromotions);
                $js[] = "MBG.addPromotions($json);";
            }
        }

        if ($scope == 'catalog') {
            $js[] = 'MBG.afterCatalogLoad();';
        }


        $listnersStr = 'Shared.listeners({';
        foreach ($listeners as $func => $listener) {
            $listnersStr .= "'$func':$listener,";
        }
        $listnersStr = rtrim($listnersStr, ",") . '});';

        $currency = \FrontTpl::getData('currency_code');

        $jsPlain = implode(PHP_EOL, $js);

        $str = <<<STR
<!-- Tag Commander Enhanced Ecommerce -->
<script type="text/javascript">
    var f = function(){
        try{
            var MBG = CommandersActEventLib;
            MBG.setCurrency('$currency');
            MBG.init(false);
            $listnersStr
        }catch(e){console.log(e);}
        $jsPlain
    };
    if(window.jQuery){
        jQuery(document).ready(f);
    }else{
        defer_functions.push(f);
    }
</script>
<!-- EOF :: Tag Commander Enhanced Ecommerce -->
STR;

        return $str;
    }


    function setList()
    {
        if (isset($this->list))
            return $this->list;

        $scope = \FrontTpl::getScopeName();
        $scope_id = \FrontTpl::getScopeId();
        if (in_array($scope, ['product', 'product_offer', 'catalog'])) {
            $pathInfo = \Request::getPathInfo();
            $pathInfo = str_replace(['/', '.htm'], '', $pathInfo);
            $tokens = explode('-', $pathInfo);
            array_pop($tokens);
            $pathInfo = implode('-', $tokens);
            $this->list = $pathInfo;
        } else {
            $this->list = $scope;
        }
    }


    function onProductAfterList($product, $counter)
    {
        $product_id = $product->id;
        $this->audit($product->id, __METHOD__);
        $this->setList();

        if (Analytics::isActive()) {
            if ($product instanceof Product)
                $product = $product->toArray();

            $wrappedProduct = $this->wrapProduct($product, [], $counter + 1, true);
            $json = $this->toJson($wrappedProduct);
            $js = "<template defer='js' id='pp_$product_id'>$json</template>";
            return $js;
        }
    }


    function onCartAddProduct(\Product &$product, $qty)
    {
        $this->audit(__METHOD__);
        if (!isset($product->already_full) or (isset($product->already_full) AND $product->already_full == false)) {
            $product->setFullData();
        }
        $productArray = is_array($product) ? $product : $product->toArray();
        $productArray = $this->wrapProduct($productArray, ['cart_quantity' => $qty], 0, true);
        return $productArray;
    }


    function onCartUpdateProduct(\Product &$product, $qty)
    {
        $this->audit(__METHOD__);
        if (!isset($product->already_full) or (isset($product->already_full) AND $product->already_full == false)) {
            $product->setFullData();
        }
        $productArray = is_array($product) ? $product : $product->toArray();
        $productArray = $this->wrapProduct($productArray, ['cart_quantity' => $qty], 0, true);
        return $productArray;
    }


    function onCartRemoveProduct(\Product &$product, &$qty)
    {
        $this->audit(__METHOD__);
        if (!isset($product->already_full) or (isset($product->already_full) AND $product->already_full == false)) {
            $product->setFullData();
        }
        $productArray = is_array($product) ? $product : $product->toArray();
        $productArray = $this->wrapProduct($productArray, ['cart_quantity' => $productArray['cart_quantity']], 0, true);
        $qty = $productArray->product_qty;
        return $productArray;
    }


    private function toJson($object)
    {
        return json_encode($object, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);
    }


    /**
     * wrap products to provide a standard products information for google analytics script
     */
    public function wrapProducts($products, $extras = array(), $full = false)
    {
        $result_products = array();
        if (!is_array($products))
            return;


        foreach ($products as $index => $product) {
            if ($product instanceof Product)
                $product = $product->toArray();


            $result_products[] = $this->wrapProduct($product, $extras, $index, $full);
        }

        return $result_products;
    }


    /**
     * wrap product to provide a standard product information for google analytics script
     */
    public function wrapProduct($product, $extras, $index = 0, $full = false)
    {
        $ga_product = '';

        try {
            if (is_array($product)) {

                $remove_attributes = [
                    'updated_at',
                    'created_at',
                ];

                foreach ($remove_attributes as $remove_attribute) {
                    if (isset($product[$remove_attribute]))
                        unset($product[$remove_attribute]);
                }

                $product = new Product($product);
            }

            $variant = null;
            if (isset($product->attributes_small))
                $variant = $product->attributes_small;
            elseif (isset($extras['attributes_small']))
                $variant = $extras['attributes_small'];

            $product_qty = 1;
            if (isset($extras['qty']))
                $product_qty = $extras['qty'];
            elseif (isset($extras['cart_quantity']))
                $product_qty = $extras['cart_quantity'];

            $product->variant = $variant;

            if ($full) {
                $ga_product = CommanderActPlugin::getProductPayload($product, $product_qty);
                $ga_product->list = $this->list;
            }
        } catch (Exception $e) {
            audit_exception($e, __METHOD__);
        }

        return $ga_product;
    }


    /**
     * Return a detailed transaction for Google Analytics
     */
    public function wrapOrder($order)
    {
        if (is_numeric($order)) {
            $order = \Order::getObj($order);
        }

        if (!is_object($order)) {
            return null;
        }

        $data = array(
            'id' => $order->reference,
            'affiliation' => \Cfg::get('SITE_TITLE'),
            'revenue' => number_format($order->total_order, 2),
            'shipping' => number_format($order->total_shipping, 2),
            'tax' => number_format($order->total_taxes, 2)
        );
        if ($order->coupon_code != '') {
            $data['coupon'] = $order->coupon_code;
        }
        return $data;
    }


    /**
     * Return a detailed promotion for Google Analytics
     */
    public function wrapPromotion($promo, $extra = [])
    {
        if (is_numeric($promo)) {
            $promo = \PriceRule::getObj($promo);
        }

        if (!is_object($promo)) {
            return null;
        }

        $data = array(
            'id' => 'PROMO_' . $promo->id,
            'name' => $promo->name,
        );

        $eligible = ['creative', 'position'];
        foreach ($eligible as $key => $field) {
            if (isset($extra[$key])) {
                $data->$key = $field;
            }
        }

        return $data;
    }

}