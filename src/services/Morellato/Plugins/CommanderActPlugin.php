<?php
/**
 *
 * CommanderAct Plugin
 * General Scopes Blocks
 * [Environment, User, Website tree, Search results, Product info, Basket/order]
 *
 * How eCommerce 'scopes' are connected to 'blocks'
 * SCOPE            BLOCKS
 * Homepage         [Environment, User]
 * Catalog          [Environment, User, Website tree, Product info]
 * Search           [Environment, User, Website tree, Search results, Product info]
 * Product          [Environment, User, Website tree, Product info]
 * Cart             [Environment, User, Product info, Basket/order]
 * Checkout         [Environment, User, Product info, Basket/order]
 * Order confirm    [Environment, User, Product info, Basket/order]
 * Register         [Environment, User]
 * Register confirm [Environment, User]
 * Other account    [Environment, User]
 * Newsletter       [Environment, User]
 * Newsletter conf  [Environment, User]
 * Trends page      [Environment, User]
 * Store locator    [Environment, User]
 * Error pages      [Environment, User]
 * Simple pages     [Environment, User]
 *
 */

namespace services\Morellato\Plugins;

use Carbon\Carbon;
use Plugins\Plugin;
use Event;
use services\Traits\TraitAttributes;
use Illuminate\Support\Str;
use CartManager;
use View;
use Core;
use FrontTpl;
use Site;
use Product;

class CommanderActPlugin extends Plugin
{

    use TraitAttributes;

    protected $debug = false;
    protected $hasCart = false;
    protected $hasOrder = false;
    protected $hasSearch = false;

    protected $taxRate;
    protected $taxIncluded = false;

    protected $platformSuffix = null;

    protected $isMultiContainer = false;

    protected $bootstrapped = false;

    function __construct()
    {
        $this->taxRate = \Core::getDefaultTaxRate();
        $this->platformSuffix = config('plugins_settings.selligent.suffix', null);

        if (strlen($this->platformSuffix) != 2)
            $this->platformSuffix = null;
    }

    function registerEvents()
    {
        parent::registerEvents();
        Event::listen('frontend.template.afterHead', "services\\Morellato\\Plugins\\CommanderActPlugin@onFrontendHead");
        Event::listen('frontend.template.afterFooter', "services\\Morellato\\Plugins\\CommanderActPlugin@onFrontendFooter");
    }

    function bootstrap(){
        if(true === $this->bootstrapped)
            return;

        //$this->debug = config('app.debug');
        $this->isMultiContainer = config('plugins_settings.commanderAct.head_snippet_uri') !== null;
        $this->setupFrontend();
        $this->setGlobalAttributes();
        $this->setUserAttributes();
        $this->route();

        $this->bootstrapped = true;
    }

    function onFrontendHead()
    {
        $this->bootstrap();
        $code = null;
        if($this->isMultiContainer)
        {
            $code = $this->renderHead();
        }
        return $code;
    }

    /**
     * Print javascript on frontend footer
     */
    function onFrontendFooter()
    {
        $this->bootstrap();
        $code = $this->render();
        return $code;
    }

    /**
     * @param $name
     */
    protected function setPagename($name)
    {
        $this->setAttribute('env_template', $name);
    }


    /***
     * Set the global parameters for order
     */
    protected function setGlobalAttributes()
    {
        /***
         * env_template    Page template name
         * env_work    Working environnement
         * env_country    Site country (ISO_3166-1_alpha-2)
         * env_language    Site language (ISO 639-1)
         * env_device    Device (desktop, tablet, mobile)
         * env_currency    Currency code (ISO code 4217)
         * env_sitename    Site Name (Morellato, Sector, etc..)
         */
        $attributes = [];
        $country = Core::getCountry();

        $attributes['env_work'] = config('plugins_settings.commanderAct.env', 'prod');
        $attributes['env_country'] = $country ? $country->iso_code : null;
        $attributes['env_language'] = Str::upper($this->getParam('lang'));
        $attributes['env_device'] = FrontTpl::getDevice()[0];
        $attributes['env_sitename'] = config('plugins_settings.commanderAct.sitename', config('app.project'));
        $attributes['env_currency'] = $this->getParam('currency');

        $this->setAttributes($attributes);

    }

    /***
     * Set the global parameters for order
     */
    protected function setUserAttributes()
    {
        /***
         * user_id    Visitor ID
         * user_gender    User gender
         * user_age    User age in years
         * user_postalcode    User postal code
         * user_address    User address
         * user_firstname    User firstname
         * user_lastname    User lastname
         * user_email    Email
         * user_newcustomer    New customer (1st order)
         */
        $scopeName = $this->getParam('scopeName');
        $user = \FrontUser::get();
        if ($scopeName == 'confirm_order') {
            $order = $this->getOrder();
            $customer = ($order) ? $order->getCustomer() : null;
            if ($customer) {
                $address = $order->getShippingAddress();
                $user = $customer;
                if ($address) {
                    $user->postcode = $address->postcode;
                    $address_full = str_replace('<br>', ' -', $address->fullAddress());
                    $user->address_full = $address_full;
                }
            }
        }
        $attributes = [];

        if ($user and $user->segmentazione == 1) {
            $email = md5(trim(Str::lower($user->email)));
            $attributes['user_id'] = $user->id;
            $attributes['user_gender'] = $user->gender_id > 0 ? ($user->gender_id == 1 ? 'm' : 'f') : null;
            $attributes['user_age'] = $user->birthday ? Carbon::parse($user->birthday)->diffInYears(Carbon::now(), true) : null;
            $attributes['user_firstname'] = trim($user->firstname);
            $attributes['user_lastname'] = trim($user->lastname);
            $attributes['user_email'] = $email;
            $attributes['user_email_md5'] = $email;
            $attributes['user_email_hash'] = $attributes['user_email_md5'];
            $attributes['user_plcy_comunicazione'] = $user->comunicazione;
            $attributes['user_plcy_segmentazione'] = $user->segmentazione;

            $attributes['user_postalcode'] = isset($user->postcode) ? (string)$user->postcode : '';
            $attributes['user_address'] = isset($user->address_full) ? $user->address_full : '';
        } else {
            $attributes['user_id'] = '';
            $attributes['user_gender'] = '';
            $attributes['user_age'] = '';
            $attributes['user_firstname'] = '';
            $attributes['user_lastname'] = '';
            $attributes['user_email'] = '';
            $attributes['user_email_md5'] = '';
            $attributes['user_email_hash'] = '';
            $attributes['user_plcy_comunicazione'] = 0;
            $attributes['user_plcy_segmentazione'] = 0;
            $attributes['user_postalcode'] = '';
            $attributes['user_address'] = '';
            $attributes['user_newcustomer'] = '';
        }

        $this->setAttributes($attributes);
    }

    /**
     *
     */
    protected function route()
    {
        $scope = $this->getParam('scope');
        $scopeName = $this->getParam('scopeName');

        if ($this->debug)
            audit(compact('scope', 'scopeName'), __METHOD__);

        $rendering = 'default';
        switch ($scope) {
            case 'product':
            case 'home':
            case 'catalog':
                $rendering = $scope;
                break;
            case 'list':
                if ($scopeName == 'search-results') {
                    $rendering = 'search';
                }
                break;
        }

        switch ($scopeName) {
            case 'cart':
            case 'checkout':
            case 'register':
            case 'register_activated':
            case 'confirm_order':
            case 'product_offer':
            case 'wishlist':
                $rendering = $scopeName;
                break;

            case 'account':
            case 'order':
                $rendering = 'account';
                break;
        }
        $method = Str::camel('onRender_' . $rendering);

        if ($this->debug)
            audit($method, __METHOD__);

        if (method_exists($this, $method)) {
            $this->$method($rendering);
        }
    }

    /**
     * @param $object
     * @return string
     */
    private function toJson($object)
    {
        $flags = $this->debug ? JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT : JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;
        return json_encode($object, $flags);
    }

    /**
     * @return string
     */
    protected function render()
    {
        if ($this->debug)
            audit($this->getAttributes(), __METHOD__);

        return $this->getTag();
    }

    /**
     * @return string
     */
    protected function renderHead()
    {
        if ($this->debug)
            audit($this->getAttributes(), __METHOD__);

        $track = $this->toJson($this->getAttributes());
        $debug = $this->debug;
        $multi = $this->isMultiContainer;
        $tag = View::make('plugins.commanderAct_head', compact('track', 'debug', 'multi'));
        $string = (string)$tag;
        return $string;
    }


    /**
     * @return string
     */
    protected function getTag()
    {
        $track = $this->toJson($this->getAttributes());
        $debug = $this->debug;
        $multi = $this->isMultiContainer;
        $tag = View::make('plugins.commanderAct', compact('track', 'debug', 'multi'));
        $string = (string)$tag;
        return $string;
    }

    /**
     * @param Product $product
     * @param int $qty
     * @return \stdClass
     */
    public function getProductNode(Product $product, $qty = 1)
    {
        //audit($product->toArray(), __METHOD__);
        $item = new \stdClass();
        $item->product_id = $product->id;
        $item->product_id_variation = ($product->combination) ? $product->combination->id : '';
        $item->product_id_context = ($this->platformSuffix) ? $product->id . $this->platformSuffix : '';
        $item->product_sku = $product->sku;
        $item->product_name = $product->name;
        $item->product_name_variation = ($product->combination) ? $product->combination->sku : '';
        $official = $this->money($product->price_official_raw);
        $sell = isset($product->cart_item_price) ? $product->cart_item_price : $product->price_final_raw;
        $sell = $this->money($sell);
        $item->product_unitprice = \Format::currency($sell, false, 'default', true);
        $discount = $official - $sell;
        $discount = $this->money($discount);
        $item->product_discount = \Format::currency($discount, false, 'default', true);
        $item->product_url = $product->link_absolute;
        $item->product_url_img = Site::img($product->defaultImg, true);
        $item->product_qty = (int)$qty;
        $item->product_brand = $product->brand_name;
        $item->product_brand_id = $product->brand_id;
        $item->product_collection = $product->collection_name;
        $item->product_collection_id = $product->collection_id;
        $categories = $product->getCategories();
        $categoryBlock = $this->getCategoryNode($categories, 'product');
        foreach ($categoryBlock as $key => $value) {
            $item->$key = $value;
        }
        //custom fields
        $defs = config('plugins_settings.commanderAct.product_custom_fields', []);
        if (!empty($defs)) {
            foreach ($defs as $key => $field) {
                $item->$key = isset($product->$field) ? $product->$field : null;
            }
        }
        return $item;
    }

    /**
     * @param array $categories
     * @param string $root
     * @return array
     */
    protected function getCategoryNode(array $categories, $root = 'page')
    {
        $categoryLimit = 3;
        $categoryBlock = [
            $root . '_cat1_name' => '',
            $root . '_cat1_id' => '',
            $root . '_cat2_name' => '',
            $root . '_cat2_id' => '',
            $root . '_cat3_name' => '',
            $root . '_cat3_id' => '',
        ];
        foreach ($categories as $index => $category) {
            if ($index < $categoryLimit) {
                $i = $index + 1;
                $categoryBlock["{$root}_cat{$i}_name"] = $category->name;
                $categoryBlock["{$root}_cat{$i}_id"] = $category->id;
            }
        }
        return $categoryBlock;
    }

    /**
     * @param \Cart|null $cart
     * @param \Order|null $order
     * @return array
     */
    protected function getOrderCustomFields(\Cart $cart = null, \Order $order = null)
    {
        $item = is_null($order) ? $cart : $order;
        $defs = config('plugins_settings.commanderAct.order_custom_fields', []);
        $data = [];
        if (!empty($defs)) {
            foreach ($defs as $key => $field) {
                $data[$key] = method_exists($item, $field) ? $item->$field() : null;
            }
        }
        return $data;
    }

    /**
     * @param $rendering
     */
    protected function onRenderHome($rendering)
    {
        $this->setPagename('homepage');
    }

    /**
     * @param $rendering
     */
    protected function onRenderDefault($rendering)
    {
        $scope_id = $this->getParam('scopeId');
        $this->setPagename('other_generic');
        $custom_scopes = config("plugins_settings.commanderAct.custom_scopes", []);
        foreach ($custom_scopes as $id => $custom_scope) {
            if ($id == $scope_id) {
                $this->setPagename($custom_scope);
            }
        }
    }

    /**
     * @param $rendering
     */
    protected function onRenderRegister($rendering)
    {
        $this->setPagename('myaccount_form');
    }

    /**
     * @param $rendering
     */
    protected function onRenderRegisterActivated($rendering)
    {
        $this->setPagename('myaccount_confirmation');
    }

    /**
     * @param $rendering
     */
    protected function onRenderCatalog($rendering)
    {
        $this->setPagename('category');

        $categories = [];
        $category = $this->getCatalogCategory();
        if ($category) {
            //audit($category, 'CATEGORY');
            $ancestors = [];
            $category->mapAncestors($ancestors);

            if (!empty($ancestors)) {
                foreach ($ancestors as $ancestor) {
                    $categories[] = $ancestor;
                }
            }
            $categories[] = $category;
            //audit($categories, 'ALL CATEGORIES');
        }
        $this->setAttributes($this->getCategoryNode($categories));

        $brand = $this->getCatalogBrand();
        $this->setAttribute('page_cat_brand_name', $brand ? $brand->name : '');
        $this->setAttribute('page_cat_brand_id', $brand ? $brand->id : '');

        $collection = $this->getCatalogCollection();
        $this->setAttribute('page_cat_collection_name', $collection ? $collection->name : '');
        $this->setAttribute('page_cat_collection_id', $collection ? $collection->id : '');

        $gender = $this->getGenderCollection();
        $this->setAttribute('page_cat_gender_name', $gender ? $gender->uname : '');

        $products = $this->getCatalogProducts();

        $items = [];
        $limit = config("plugins_settings.commanderAct.limit", 100);
        $counter = 0;

        foreach ($products as $product) {
            if ($counter < $limit) {
                $item = $this->getProductNode($product);
                $items[] = $item;
            }
            $counter++;
        }

        $this->setAttribute('product_array', $items);
    }

    /**
     * @param $rendering
     */
    protected function onRenderSearch($rendering)
    {
        $this->onRenderCatalog($rendering);
        $this->setPagename('internal_search');
        $q = $this->getCatalogSearchQuery();
        $this->setAttribute('search_keywords', $q);
        $this->setAttribute('search_filters', [$this->getCatalogOrderBy() . '|' . $this->getCatalogOrderDir()]);
    }

    /**
     * @param $rendering
     */
    protected function onRenderProduct($rendering)
    {
        $this->setPagename('product');

        $product = $this->getProduct();
        if (is_null($product))
            return;

        $item = $this->getProductNode($product);
        if ($item) {
            //clone all 'product_cat*' fields into tree structure
            $tree = [];
            for ($i = 1; $i <= 3; $i++) {
                $tree["page_cat{$i}_name"] = $item->{"product_cat{$i}_name"};
                $tree["page_cat{$i}_id"] = $item->{"product_cat{$i}_id"};
            }
            $this->setAttributes($tree);
        }

        $exportable_items = [
            'page_cat_brand_name' => 'product_brand',
            'page_cat_brand_id' => 'product_brand_id',
            'product_name' => 'product_name',
            'page_cat_gender_name' => 'product_gender',
            'page_cat_collection_id' => 'product_collection_id',
            'page_cat_collection_name' => 'product_collection',
        ];

        $items = [$item];

        foreach($exportable_items as $key => $value){
            if(isset($item->{$value})){
                $this->setAttribute($key, $item->{$value});
            }
        }

        $this->setAttribute('product_array', $items);
    }

    /**
     * @param $rendering
     */
    protected function onRenderProductOffer($rendering)
    {
        $this->onRenderProduct($rendering);
    }

    /**
     * @param $rendering
     */
    protected function onRenderCart($rendering)
    {
        $this->setPagename('funnel_basket');
        $cart = $this->getCart();
        $total = $this->getCartTotal();
        $discount = $this->getCartDiscount();
        $products = $this->getCartProducts();

        $data = [];

        $data['basket_id'] = $cart ? $cart->id : '';
        $data['basket_recovery_url'] = $cart ? \CartManager::getAbandonedUrl() : '';
        $data['order_amount'] = $total ? $this->money($total) : '';
        $data['order_discount'] = $discount ? $this->money($discount) : '';
        $data['order_payment_methods'] = $cart ? $cart->getPaymentEntityModule() : '';

        $items = [];
        $qty = 0;
        foreach ($products as $product) {
            //audit($product, __METHOD__);
            $item = $this->getProductNode($product);
            $item->product_qty = $product->cart_quantity;
            $qty += $product->cart_quantity;
            $items[] = $item;
        }

        $data['order_products_number'] = $qty;
        $this->setAttributes($data);
        $this->setAttribute('product_array', $items);
        try {
            $order_custom_fields = $this->getOrderCustomFields($cart);
            if (!empty($order_custom_fields)) {
                $this->setAttributes($order_custom_fields);
            }
        } catch (\Exception $e) {
            audit($e->getMessage(), __METHOD__);
            audit($e->getTraceAsString(), __METHOD__);
        }

    }

    /**
     * @param $rendering
     */
    protected function onRenderCheckout($rendering)
    {
        $this->onRenderCart($rendering);
        $this->setPagename('funnel_step2');
    }

    /**
     * @param $rendering
     */
    protected function onRenderWishlist($rendering)
    {
        $this->setPagename('myaccount_other');

    }

    /**
     * @param $rendering
     */
    protected function onRenderAccount($rendering)
    {
        $this->setPagename('myaccount_other');
    }


    /**
     * @param $rendering
     */
    protected function onRenderConfirmOrder($rendering)
    {
        $this->setPagename('funnel_confirmation');

        $order = $this->getOrder();
        $paymentState = $this->getPaymentState();

        $items = [];
        $qty = 0;

        if ($order and $paymentState and $paymentState->failed == 0) {

            $data = [];

            $data['basket_id'] = $order->cart_id;
            $data['order_id'] = $order->reference;
            $data['order_amount'] = \Format::float($this->money($order->total_order));
            $data['order_discount'] = \Format::float($this->money($order->total_discounts));
            $data['order_payment_methods'] = $order->getPaymentEntityModule();

            $customer = \FrontUser::get();
            $lastOrder = null;
            if ($customer) {
                $lastOrder = $customer->getLastOrder();
            }
            $user_newcustomer = 1;
            if ($lastOrder and $lastOrder->id == $order->id) {
                $user_newcustomer = 0;
            }
            $data['user_newcustomer'] = $user_newcustomer;

            $products = $order->getProducts();
            $size = count($products);
            if ($size > 0) {
                foreach ($products as $index => $p) {
                    $obj = $p->product;
                    $obj->setFullData();
                    if ($p->product_combination_id > 0) {
                        $obj->combination = \ProductCombination::getObj($p->product_combination_id);
                    }

                    $item = $this->getProductNode($obj);
                    $item->product_qty = $p->product_quantity;
                    $qty += $p->product_quantity;

                    $items[] = $item;
                }
            }

            $data['order_products_number'] = $qty;
            $this->setAttributes($data);
            $this->setAttribute('product_array', $items);

            try {
                $order_custom_fields = $this->getOrderCustomFields(null, $order);
                if (!empty($order_custom_fields)) {
                    $this->setAttributes($order_custom_fields);
                }
            } catch (\Exception $e) {
                audit($e->getMessage(), __METHOD__);
                audit($e->getTraceAsString(), __METHOD__);
            }
        }
    }

    /**
     * @param $value
     * @return float
     */
    protected function money($value)
    {
        return $this->taxIncluded ? $value : \Core::untax($value, $this->taxRate);
    }

    /**
     * @param Product $product
     * @param $qty
     * @return mixed
     */
    static function getProductPayload(Product $product, $qty)
    {
        $instance = new static();
        return $instance->getProductNode($product, $qty);
    }

}