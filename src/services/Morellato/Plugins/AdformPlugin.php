<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 24/10/2017
 * Time: 11:56
 */

namespace services\Morellato\Plugins;

use Plugins\Plugin;
use Event;
use services\Traits\TraitAttributes;
use Illuminate\Support\Str;
use CartManager;
use View;

class AdformPlugin extends Plugin
{

    use TraitAttributes;

    protected $debug;

    function registerEvents()
    {
        parent::registerEvents();
        Event::listen('frontend.template.afterFooter', "services\\Morellato\\Plugins\\AdformPlugin@onFrontendFooter");
    }

    /**
     * Print javascript on frontend footer
     */
    function onFrontendFooter()
    {
        $this->debug = config('app.debug');
        $this->setupFrontend();
        $this->setGlobalAttributes();
        $this->route();
        $code = $this->render();
        return $code;
    }


    /***
     * Set the global parameters for order
     */
    protected function setGlobalAttributes()
    {
        /***
         * Nome Variabile         Valore
         * sv1                    user id if logged in
         * sv2                    Device: "M" for Mobile or "D" for Desktop
         * sv3                    page url
         * sv4                    Negozio (es. Magenta)
         * sv6                    value
         * sv7                    true o false (newsletter registration)
         * sv9                    Brand
         */
        $attributes = [];
        $user = \FrontUser::get();

        $attributes['sv1'] = ($user) ? $user->id : null;
        $attributes['sv2'] = ($this->getParam('mobile')) ? 'M' : 'D';
        $attributes['sv3'] = $this->getParam('url');
        $attributes['sv4'] = null;
        $attributes['sv6'] = $this->getParam('currency');
        $attributes['sv7'] = ($user) ? $user->hasNewsletter() : false;
        $attributes['sv9'] = null;

        $this->setAttribute('pm', config("plugins_settings.adform.pm"));
        $this->setAttribute('order', $attributes);

    }


    protected function route()
    {
        $scope = $this->getParam('scope');
        $scopeName = $this->getParam('scopeName');
        //audit(compact('scope', 'scopeName'), __METHOD__);
        $rendering = 'default';
        switch ($scope) {
            case 'product':
            case 'home':
            case 'catalog':
                $rendering = $scope;
                break;
            case 'list':
                if ($scopeName == 'search-results') {
                    $rendering = 'search';
                }
                break;
        }

        switch ($scopeName) {
            case 'cart':
            case 'checkout':
            case 'register':
            case 'register_activated':
            case 'confirm_order':
            case 'product_offer':
            case 'wishlist':
                $rendering = $scopeName;
                break;

            case 'account':
            case 'order':
                $rendering = 'account';
                break;
        }
        $method = Str::camel('onRender_' . $rendering);
        //audit($method, __METHOD__);
        if (method_exists($this, $method)) {
            $this->$method($rendering);
        }
    }

    /**
     * @param $object
     * @return string
     */
    private function toJson($object)
    {
        $flags = $this->debug ? JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK : JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK;
        return json_encode($object, $flags);
    }

    /**
     * @return string
     */
    protected function render()
    {
        //audit($this->getAttributes(), __METHOD__);

        $pagename = $this->getAttribute('pagename');

        $this->setAttribute('divider', ("@encodeURIComponent('|')@"));
        $this->setAttribute('pagename', ("@encodeURIComponent('$pagename')@"));
        $user = \FrontUser::get();

        $this->setAttribute('mailmd5', ($user) ? md5($user->email) : '');
        $this->setAttribute('mailsha256', ($user) ? hash('sha256', $user->email) : '');

        //https://track.adform.net/Serving/TrackPoint/?pm=1025122&ADFPageName=IT%7CProductPage&ADFdivider=|
        $pixelParams = [];
        $pixelParams['pm'] = $this->getAttribute('pm');
        $pixelParams['ADFPageName'] = urlencode($pagename);
        $pixelParams['ADFdivider'] = '|';

        $pixel = "https://track.adform.net/Serving/TrackPoint/?" . http_build_query($pixelParams);

        $tag = $this->getTag($pixel);

        // audit($tag, __METHOD__);
        // audit($this->getAttributes(), __METHOD__);

        return $tag;
    }


    /**
     * @param $pixel
     * @return string
     */
    protected function getTag($pixel)
    {
        $track = $this->toJson($this->getAttributes());
        $debug = $this->debug;
        $tag = View::make('plugins.adform', compact('pixel', 'track', 'debug'));
        $string = (string)$tag;
        //resolve some json and urlencode quirks
        return str_replace(['"@', '@"'], '', $string);
    }


    /**
     * @param $name
     */
    protected function setPagename($name)
    {
        $lang = Str::upper($this->getParam('lang'));
        $this->setAttribute('pagename', "$lang|$name");
    }

    /**
     * @param $product
     * @return \stdClass
     */
    protected function getProductNode($product)
    {
        $item = new \stdClass();
        $item->productid = $product->id;
        $item->productname = $product->name;
        $item->productsales = \Format::currency($product->price_final_raw, false, 'default', true);
        $item->categoryname = $product->default_category_id > 0 ? $product->category_name : $product->main_category_name;
        $item->categoryid = $product->default_category_id > 0 ? $product->default_category_id : $product->main_category_id;
        return $item;
    }


    protected function onRenderHome($rendering)
    {
        $this->setPagename('HomePage');
    }

    protected function onRenderDefault($rendering)
    {
        $this->setPagename('CmsPage');
    }

    protected function onRenderRegister($rendering)
    {
        $this->setPagename('Registration');
    }

    protected function onRenderRegisterActivated($rendering)
    {
        $this->setPagename('TYP_Registration');
    }

    protected function onRenderCatalog($rendering)
    {
        $conditions = \Catalog::get('conditions');
        $brand = $this->getCatalogBrand();
        if ($brand) {
            $this->setAttribute('order.sv9', $brand->name);
        }
        $titles = [];
        foreach ($conditions as $condition) {
            if (isset($condition->target)) {
                $titles[] = $condition->target;
            }
        }
        //audit($conditions, __METHOD__);
        $this->setPagename('CategoryPage|' . implode('-', $titles));
        $weight = 1;
        $this->setAttribute('order.weight', $weight);
        $products = $this->getCatalogProducts();

        $items = [];
        $limit = config("plugins_settings.adform.limit", 100);
        $counter = 0;

        foreach ($products as $product) {
            if ($counter < $limit) {
                $item = $this->getProductNode($product);
                $item->step = 1;

                $items[] = $item;
            }
            $counter++;
        }

        $this->setAttribute('order.itms', $items);
    }

    protected function onRenderSearch($rendering)
    {
        $this->onRenderCatalog($rendering);
        $this->setPagename('Search');
    }

    protected function onRenderProduct($rendering)
    {
        $this->setPagename('ProductPage');
        $weight = 5;
        $step = 1;

        $product = $this->getProduct();
        if (is_null($product))
            return;

        $item = $this->getProductNode($product);
        $item->weight = $weight;
        $item->step = $step;
        $itms = [$item];

        if (isset($product->brand_name)) {
            $this->setAttribute('order.sv9', $product->brand_name);
        }

        $this->setAttribute('order.itms', $itms);
    }


    protected function onRenderProductOffer($rendering)
    {
        $this->onRenderProduct($rendering);
        $this->setPagename('ProductPage_inofferta');
    }


    protected function onRenderCart($rendering)
    {
        $this->setPagename('Cart');
        $this->setAttribute('order.weight', 50);
        $products = $this->getCartProducts();
        $size = count($products);
        $items = [];
        foreach ($products as $product) {
            $item = $this->getProductNode($product);
            $item->step = 2;
            $item->productcount = $product->cart_quantity;

            $items[] = $item;
        }
        $this->setAttribute('order.svn1', $size);
        $this->setAttribute('order.itms', $items);
    }


    protected function onRenderCheckout($rendering)
    {
        $this->onRenderCart($rendering);
        $this->setPagename('Checkout');

        $carrier = CartManager::getCarrierName();
        $payment = CartManager::getPaymentName();
        $country = CartManager::getDeliveryCountry();
        $countryName = $country ? $country->name : null;
        $gift = CartManager::hasGift();
        $streaming = CartManager::hasStreaming();
        $promo = CartManager::getCoupon();
        $store = CartManager::getDeliveryStore();
        $storeName = ($store) ? $store->name : null;

        $this->setAttribute('order.sv4', $storeName);
        $this->setAttribute('order.sv5', $promo);
        $this->setAttribute('order.sv10', $gift);
        $this->setAttribute('order.sv11', $streaming);
        $this->setAttribute('order.sv12', $countryName);
        $this->setAttribute('order.sv13', $carrier);
        $this->setAttribute('order.sv14', $payment);
    }


    protected function onRenderWishlist($rendering)
    {
        $this->setPagename('WhishList');
        $weight = 1;
        $products = $this->getWishlistProducts();

        $items = [];

        foreach ($products as $product) {
            $item = $this->getProductNode($product);
            $item->step = 1;

            $items[] = $item;
        }

        $this->setAttribute('order.weight', $weight);
        $this->setAttribute('order.itms', $items);
    }


    protected function onRenderAccount($rendering)
    {
        $this->setPagename('Account');
    }


    protected function onRenderConfirmOrder($rendering)
    {
        $this->setPagename('ThankYouPage');

        $order = $this->getOrder();
        $paymentState = $this->getPaymentState();

        $items = [];

        if ($order and $paymentState and $paymentState->failed == 0) {

            $carrier = $order->carrierName();
            $payment = $order->payment;
            $shippingAddress = $order->getShippingAddress();
            $countryName = ($shippingAddress) ? $shippingAddress->countryName() : null;

            $gift = $order->hasGift();
            $streaming = $order->hasStreaming();

            $this->setAttribute('order.orderid', $order->reference);
            $this->setAttribute('order.sales', \Format::float($order->total_order));
            $this->setAttribute('order.sv10', $gift);
            $this->setAttribute('order.sv11', $streaming);
            $this->setAttribute('order.sv12', $countryName);
            $this->setAttribute('order.sv13', $carrier);
            $this->setAttribute('order.sv14', $payment);

            $products = $order->getProducts();
            $size = count($products);
            if ($size > 0) {
                foreach ($products as $index => $p) {
                    $obj = $p->product;
                    $obj->setFullData();

                    $item = $this->getProductNode($obj);
                    $item->productsales = \Format::currency($p->cart_price_tax_incl, false, 'default', true);
                    $item->step = 3;
                    $item->productcount = $p->product_quantity;

                    $items[] = $item;
                }
            }

            $this->setAttribute('order.svn1', $size);
            $this->setAttribute('order.itms', $items);
        }
    }


}