<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 24/10/2017
 * Time: 11:56
 */

namespace services\Morellato\Plugins;

use Plugins\Plugin;
use Event;
use services\Traits\TraitAttributes;
use Illuminate\Support\Str;
use CartManager;
use View;

class CriteoPlugin extends Plugin
{

    use TraitAttributes;

    protected $debug;

    function registerEvents()
    {
        parent::registerEvents();
        Event::listen('frontend.template.afterFooter', "services\\Morellato\\Plugins\\CriteoPlugin@onFrontendFooter");
    }

    /**
     * Print javascript on frontend footer
     */
    function onFrontendFooter()
    {
        $this->debug = config('app.debug');
        $this->setupFrontend();
        $this->setGlobalAttributes();
        $this->route();
        $code = $this->render();
        return $code;
    }


    protected function setGlobalAttributes()
    {
        $attributes = [];
        $user = \FrontUser::get();
        $attributes['account'] = config('plugins_settings.criteo.account');
        $attributes['email'] = ($user) ? md5($user->email) : '';

        $this->setAttributes($attributes);
    }

    /**
     * Homepage Tag
     * Category / Listing Tag
     * Product Tag
     * Basket / Cart Tag
     * Sales Tag
     */
    protected function route()
    {
        $scope = $this->getParam('scope');
        $scopeName = $this->getParam('scopeName');
        //audit(compact('scope', 'scopeName'), __METHOD__);
        $rendering = null;
        switch ($scope) {
            case 'product':
            case 'home':
            case 'catalog':
                $rendering = $scope;
                break;
            case 'list':
                if ($scopeName == 'search-results') {
                    $rendering = 'search';
                }
                break;
        }

        switch ($scopeName) {
            case 'cart':
            case 'checkout':
            case 'confirm_order':
                $rendering = $scopeName;
                break;
        }

        if (is_null($rendering))
            return;

        $method = Str::camel('onRender_' . $rendering);
        //audit($method, __METHOD__);
        if (method_exists($this, $method)) {
            $this->$method($rendering);
        }
    }

    /**
     * @param $object
     * @return string
     */
    private function toJson($object)
    {
        $flags = $this->debug ? JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK : JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK;
        return (is_array($object) or is_array($object)) ? json_encode($object, $flags) : null;
    }

    /**
     * @return string
     */
    protected function render()
    {
        $tag = $this->getTag();

        //audit($tag, __METHOD__);

        return $tag;
    }


    /**
     * @return string
     */
    protected function getTag()
    {
        //$track = $this->toJson($this->getAttributes());
        $account = $this->getAttribute('account');
        $email = $this->getAttribute('email');
        $event = $this->getAttribute('event');
        $item = $this->getAttribute('item');
        $order_id = $this->getAttribute('order_id');
        $debug = $this->debug;
        $tag = View::make('plugins.criteo', compact('account', 'email', 'event', 'item', 'order_id', 'debug'));
        $string = (string)$tag;
        return $string;
    }


    /**
     * @param $name
     */
    protected function setEventName($name)
    {
        $this->setAttribute('event', $name);
    }

    /**
     * @param $product
     * @return \stdClass
     */
    protected function getProductNode($product)
    {
        $item = new \stdClass();
        $item->id = (string)$product->id;
        $item->price = \Format::currency($product->price_final_raw, false, 'default', true);
        return $item;
    }


    protected function onRenderHome($rendering)
    {
        $this->setEventName('viewHome');
    }


    protected function onRenderCatalog($rendering)
    {
        $this->setEventName('viewList');

        $products = $this->getCatalogProducts();

        $items = [];

        foreach ($products as $product) {
            $items[] = (string)$product->id;
        }

        $strItems = "['" . implode("','", $items) . "']";

        $this->setAttribute('item', $strItems);
    }

    protected function onRenderSearch($rendering)
    {
        $this->onRenderCatalog($rendering);

    }

    protected function onRenderProduct($rendering)
    {
        $this->setEventName('viewItem');

        $product = $this->getProduct();
        if (is_null($product))
            return;

        $this->setAttribute('item', "'$product->id'");
    }


    protected function onRenderCart($rendering)
    {
        $this->setEventName('viewBasket');

        $products = $this->getCartProducts();
        $items = [];
        foreach ($products as $product) {
            $item = $this->getProductNode($product);
            $item->quantity = $product->cart_quantity;

            $items[] = $item;
        }

        $this->setAttribute('item', $this->toJson($items));
    }


    protected function onRenderCheckout($rendering)
    {
        return $this->onRenderCart($rendering);
    }


    protected function onRenderConfirmOrder($rendering)
    {
        $this->setEventName('trackTransaction');

        $order = $this->getOrder();
        $paymentState = $this->getPaymentState();

        $items = [];

        if ($order and $paymentState and $paymentState->failed == 0) {

            $order_id = $order->cart_id;



            $products = $order->getProducts();
            $size = count($products);
            if ($size > 0) {
                foreach ($products as $index => $p) {
                    $obj = $p->product;
                    $obj->setFullData();

                    $item = $this->getProductNode($obj);

                    $item->price = \Format::currency($p->cart_price_tax_incl, false, 'default', true);
                    $item->quantity = $p->product_quantity;

                    $items[] = $item;
                }
            }

            $this->setAttribute('order_id', $order_id);
            $this->setAttribute('item', $this->toJson($items));
        }
    }


}