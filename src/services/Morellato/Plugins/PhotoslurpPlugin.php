<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 24/10/2017
 * Time: 11:56
 */

namespace services\Morellato\Plugins;

use Plugins\Plugin;
use services\Traits\TraitAttributes;
use Illuminate\Support\Str;
use Frontend\Module;
use Event;
use View;

class PhotoslurpPlugin extends Plugin
{

    use TraitAttributes;

    protected $debug;

    function __construct()
    {
        $this->debug = config('app.debug');
        $this->setupFrontend();
    }

    function registerEvents()
    {
        parent::registerEvents();
        Event::listen('frontend.template.afterFooter', "services\\Morellato\\Plugins\\PhotoslurpPlugin@onFrontendFooter");
    }

    /**
     * Print javascript on frontend footer
     */
    function onFrontendFooter()
    {
        $this->route();
        $code = $this->render();
        return $code;
    }

    /**
     * Homepage Tag
     * Category / Listing Tag
     * Product Tag
     * Basket / Cart Tag
     * Sales Tag
     */
    protected function route()
    {
        $scopeName = $this->getParam('scopeName');

        $rendering = $scopeName == 'confirm_order' ? 'confirm_order' : null;

        if (is_null($rendering))
            return;

        $method = Str::camel('onRender_' . $rendering);

        if (method_exists($this, $method)) {
            $this->$method($rendering);
        }
    }

    /**
     * @param $rendering
     */
    protected function onRenderConfirmOrder($rendering)
    {

        $order = $this->getOrder();
        $paymentState = $this->getPaymentState();

        $items = [];

        /**
         * var photoSlurpTrackingSettings = {
         * products: {
         * 'sku1':  {count:1, price:20, currency: 'eur'},
         * 'sku2':  {count:1, price:10.99, currency: 'eur'}
         * },
         * albumId: 123
         * };
         */

        if ($order and $paymentState and $paymentState->failed == 0) {

            $products = $order->getProducts();
            $size = count($products);
            if ($size > 0) {
                foreach ($products as $index => $p) {
                    $obj = $p->product;

                    $item = $this->getProductNode($obj);

                    $item->price = \Format::currency($p->cart_price_tax_incl, false, 'default', true);
                    $item->count = $p->product_quantity;

                    $items[$obj->id] = $item;
                }
            }

            $this->setAttribute('settings.orderId', $order->reference);
            $this->setAttribute('settings.albumId', $this->getAlbumId());
            $this->setAttribute('settings.products', $items);
        }
    }

    /**
     * @param $product
     * @return \stdClass
     */
    protected function getProductNode($product)
    {
        $item = new \stdClass();
        $item->price = \Format::currency($product->price_final_raw, false, 'default', true);
        $item->currency = \Str::lower($this->getParam('currency'));
        return $item;
    }

    /**
     * @return string
     */
    protected function render()
    {
        $tag = $this->getTag();

        //audit($tag, __METHOD__);

        return $tag;
    }


    /**
     * @return string
     */
    protected function getTag()
    {
        //$track = $this->toJson($this->getAttributes());
        $settings = $this->toJson($this->getAttribute('settings'));
        if(is_null($settings))
            return '';
        $debug = $this->debug;
        $tag = View::make('plugins.photoslurp', compact('settings', 'debug'));
        $string = (string)$tag;
        return $string;
    }

    /**
     * @param Module $module
     * @return string
     */
    function makeJson(Module $module)
    {
        $data = [];
        $data['albumId'] = $this->getAlbumId();
        $data['widgetType'] = (string)$module->getParam('widgetType');
        $data['pageType'] = (string)$module->getParam('pageType');
        $data['submitText'] = (string)$module->getParam('submitText');
        $data['addPhotosImg'] = (string)$module->getParam('addPhotosImg');
        $data['photosAlign'] = (string)$module->getParam('photosAlign');
        $data['noteAddPicsText'] = (string)$module->getParam('noteAddPicsText');
        $data['ctaButton'] = (string)$module->getParam('ctaButton');
        $data['tocLink'] = (string)$module->getParam('tocLink');
        $data['shopThisLookText'] = (string)$module->getParam('shopThisLookText');
        $data['loadMoreText'] = (string)$module->getParam('loadMoreText');
        $data['postedByText'] = (string)$module->getParam('postedByText');
        $data['viewAndShopText'] = (string)$module->getParam('viewAndShopText');
        $data['theme'] = (string)$module->getParam('theme');
        $data['productType'] = (string)$module->getParam('productType');
        $data['pageLimit'] = (int)$module->getParam('page_limit', 12);
        $data['thumbOverlay'] = (bool)$module->getParam('thumbOverlay');
        $data['showSubmit'] = (bool)$module->getParam('showSubmit');
        $data['randomOrder'] = (bool)$module->getParam('randomOrder');
        $data['socialIcons'] = (bool)$module->getParam('socialIcons');
        $data['enableGA'] = (bool)$module->getParam('enableGA');
        $data['inStockOnly'] = (bool)$module->getParam('inStockOnly');
        $data['strictProducts'] = (bool)$module->getParam('strictProducts');
        $data['rightsClearedOnly'] = (bool)$module->getParam('rightsClearedOnly');
        $data['assignedOnly'] = (bool)$module->getParam('assignedOnly');
        $data['utmParams'] = (bool)$module->getParam('utmParams');
        $data['varyingThumbSizes'] = (bool)$module->getParam('varyingThumbSizes');
        $data['autoscrollLimit'] = (int)$module->getParam('autoscrollLimit', 1);
        $data['initDelay'] = (int)$module->getParam('initDelay', 0);
        $data['emptyThreshold'] = (int)$module->getParam('emptyThreshold', 0);
        $visibleProducts = (int)$module->getParam('visibleProducts');
        if ($visibleProducts) {
            $data['visibleProducts'] = $visibleProducts;
        }

        $collection = $module->getParam('collection');
        if(strlen(trim($collection)) > 0)
            $data['collection'] = explode(',', $module->getParam('collection'));

        $data['widgetId'] = md5(config('app.url') . $data['widgetType']);
        $data['lang'] = $this->getLang();

        if((bool)$module->getParam('enableProductId', false)){
            $data['productId'] = '';
            if ($this->getParam('scope') == 'product') {
                $product = $this->getProduct();
                if (!is_null($product)) {
                    $data['productId'] = (int)$product->id;
                }
            }
        }

        //remove falsy props
        foreach ($data as $key => $value){
            if(is_string($value) and strlen(trim($value)) === 0){
                unset($data[$key]);
            }
            if(is_integer($value) and $value === 0){
                unset($data[$key]);
            }
            /*if(is_bool($value) and $value === false){
                unset($data[$key]);
            }*/
        }


        $this->setAttributes($data);

        return $this->toJson($data);
    }

    /**
     * @return null|string
     */
    function getLang(){
        return \Core::getLang();
        $lang = null;
        switch ($this->getParam('lang')){
            default:
            case 'it':
                $lang = 'it_IT';
                break;
            case 'en':
                $lang = 'en_GB';
                break;
            case 'es':
                $lang = 'es_ES';
                break;
            case 'fr':
                $lang = 'fr_FR';
                break;
            case 'de':
                $lang = 'de_DE';
                break;
        }
        return $lang;
    }

    /**
     * @return mixed
     */
    function getUsername()
    {
        return config('plugins_settings.photoslurp.username');
    }

    /**
     * @return mixed
     */
    function getAlbumId()
    {
        return config('plugins_settings.photoslurp.albumId');
    }

    /**
     * @param $object
     * @return string
     */
    private function toJson($object)
    {
        $flags = $this->debug ? JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT | JSON_NUMERIC_CHECK : JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK;
        return (is_array($object) or is_array($object)) ? json_encode($object, $flags) : null;
    }


}