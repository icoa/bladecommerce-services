<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 17/02/2017
 * Time: 10:08
 */

namespace services\Morellato\Soap\Response;

use Config;
use DB;
use Utils;
use Exception;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use services\Morellato\Loggers\DatabaseOrderLogger as Logger;
use services\Morellato\Soap\Service\OrderService;

class OrderResponse extends AbstractOrderResponse
{

    protected $storage_path = 'morellato/orders/response';

    function make()
    {
        $this->saveResponse();
        $returnNodes = isset($this->response['RETURN']) ? $this->response['RETURN']->item : null;
        if (is_array($returnNodes)) {
            $this->purgeMessages();
            foreach ($returnNodes as $node) {
                $this->addMessage($node);
            }
        } elseif (is_object($returnNodes)) {
            $this->purgeMessages();
            if(isset($returnNodes->item)){
                if(is_array($returnNodes->item)){
                    foreach ($returnNodes->item as $node) {
                        $this->addMessage($node);
                    }
                }
                if(is_object($returnNodes->item)){
                    $this->addMessage($returnNodes->item);
                }
            }else{
                $this->addMessage($returnNodes);
            }
        }

        $delivery_id = $this->response['DELIVDOCUMENTS'];
        $sales_id = $this->response['SALESDOCUMENT'];
        $deliveryExists = \OrderHelper::isValidGlsCode($delivery_id);
        $salesExists = \OrderHelper::isValidGlsCode($sales_id);
        //there no delivery and sales; internal error
        if ($deliveryExists === false AND $salesExists === false) {
            throw new Exception("Response for order [$this->order_id] has no delivery and sales numbers", self::ORDER_ERROR);
        }
        //save the (incomplete/full) order slip
        $this->setOrderSlip($delivery_id, $sales_id);
        //there is no delivery but the sales has been created; throw the exception (probably will send an email or report)
        if ($deliveryExists === false AND $salesExists === true) {
            throw new Exception("Response for order [$this->order_id] has no delivery number", self::ORDER_WARNING);
        }
        //so far so good...
        $this->setOrderShippingNumber($delivery_id);
    }

}