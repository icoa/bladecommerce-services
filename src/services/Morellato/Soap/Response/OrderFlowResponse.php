<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 17/02/2017
 * Time: 10:08
 */

namespace services\Morellato\Soap\Response;

use Config;
use DB;
use Illuminate\Support\Collection;
use services\OrderCollectorService;
use services\Traits\TraitLocalDebug;
use Utils;
use Exception;
use OrderDetail;
use services\Models\OrderSlipDetail;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use services\Morellato\Loggers\DatabaseOrderLogger as Logger;
use services\Morellato\Soap\Service\OrderService;

class OrderFlowResponse extends AbstractOrderResponse
{
    use TraitLocalDebug;

    protected $localDebug = false;

    protected $storage_path = 'morellato/orders/flows/response';

    /**
     * @var Collection
     */
    protected $slips;

    /**
     * @param $response
     * @return array
     */
    static function getItems($response)
    {
        $data = [];
        if (isset($response['ZT_ZVBFA']) and isset($response['ZT_ZVBFA']->item)) {
            $items = $response['ZT_ZVBFA']->item;

            if (is_array($items)) {
                $data = $items;
            } else {
                $data = [$items];
            }
        }
        return $data;
    }

    function make()
    {
        $this->saveResponse();
        $response = $this->response;
        $this->slips = Collection::make([]);
        if (isset($response['ZT_ZVBFA']) and isset($response['ZT_ZVBFA']->item)) {
            $items = $response['ZT_ZVBFA']->item;
            //so far so good...
            if (is_array($items)) {
                foreach ($items as $item) {
                    $this->parseItem($item);
                }
            } else {
                $this->parseItem($items);
            }
        } else {
            $this->console->error("The SAP response could not be parsed: node ZT_ZVBFA is empty!");
            throw new Exception("The SAP response could not be parsed: node ZT_ZVBFA is empty!");
        }
        $this->handleUpsertedSlipDetails();
    }


    protected function parseItem($item)
    {
        try {
            $this->collectItem($item);
        } catch (Exception $e) {
            audit_exception($e, __METHOD__);
            audit_error($item, __METHOD__, 'Item');
        }
    }


    protected function collectItem($item)
    {
        $slip = new OrderSlipDetail();
        $slip->setAttributesByOrderFlowItem($item);
        $slip->setOrderId($this->order_id);

        try {

            $slip->findOrderDetail();
            $result = $slip->upsertPayload();

            if ($result)
                $this->slips->push($result);

        } catch (Exception $e) {
            audit_exception($e, __METHOD__);
        }
    }


    protected function handleUpsertedSlipDetails()
    {
        if ($this->slips->isEmpty()) {
            $this->console->comment("There are not upserted slips, exiting now");
            return;
        }

        $this->audit($this->slips->toArray(), __METHOD__, 'SLIP MODELS');

        /** @var OrderSlipDetail $slip */
        foreach ($this->slips as $slip) {
            switch ($slip->getType()) {

                case OrderSlipDetail::TYPE_SHIPPING:
                    try {
                        $this->saveShippingNumber($slip);
                    } catch (Exception $e) {
                        audit_exception($e, __METHOD__);
                    }
                    break;

                case OrderSlipDetail::TYPE_INVOICE:
                    try {
                        $this->saveInvoiceNumber($slip);
                    } catch (Exception $e) {
                        audit_exception($e, __METHOD__);
                    }
                    break;

                case OrderSlipDetail::TYPE_SHIPPED:
                    try {
                        $this->saveWarehouseNumber($slip);
                    } catch (Exception $e) {
                        audit_exception($e, __METHOD__);
                    }
                    break;

                case OrderSlipDetail::TYPE_SOLD:
                    try {
                        $this->saveOrderReceipt($slip);
                    } catch (Exception $e) {
                        audit_exception($e, __METHOD__);
                    }
                    break;

                case OrderSlipDetail::TYPE_CARRIER:
                    try {
                        $this->saveCarrierShipping($slip);
                    } catch (Exception $e) {
                        audit_exception($e, __METHOD__);
                    }
                    break;

                case OrderSlipDetail::TYPE_CARRIER_SHOP:
                    try {
                        $this->saveCarrierShipping($slip, true);
                    } catch (Exception $e) {
                        audit_exception($e, __METHOD__);
                    }
                    break;

                case OrderSlipDetail::TYPE_CANCELLED:
                    try {
                        $this->handleCancelledItem($slip);
                    } catch (Exception $e) {
                        audit_exception($e, __METHOD__);
                    }
                    break;

                case OrderSlipDetail::TYPE_PACKAGE_READY:
                    try {
                        $this->handlePackageReady($slip);
                    } catch (Exception $e) {
                        audit_exception($e, __METHOD__);
                    }
                    break;

                case OrderSlipDetail::TYPE_RITIRED_ONLY:
                    try {
                        $this->handleRitiredOnly($slip);
                    } catch (Exception $e) {
                        audit_exception($e, __METHOD__);
                    }
                    break;

                case OrderSlipDetail::TYPE_SHOP_NOT_FOUND:
                    try {
                        $this->handleShopNotFound($slip);
                    } catch (Exception $e) {
                        audit_exception($e, __METHOD__);
                    }
                    break;

                case OrderSlipDetail::TYPE_CANCELLED_NOT_FOUND:
                    try {
                        $this->handleShopCancelledNotFound($slip);
                    } catch (Exception $e) {
                        audit_exception($e, __METHOD__);
                    }
                    break;
            }
        }
    }

    /**
     * Se ha valore "J", nelle colonne VBELN / POSNN  puoi trovare il numero della consegna / riga consegna
     * e nella colonna  VBELV / POSNV  il numero dell'ordine / riga ordine
     *
     * @param $slip
     * @throws Exception
     */
    protected function saveShippingNumber(OrderSlipDetail $slip)
    {
        $number = $slip->getNumber();
        //check if 'NEG'
        if(\OrderHelper::isNegCode($number)){
            //do not save any data or throw exception;
            return;
        }
        $this->audit("saveShippingNumber [$number]");
        $this->console->line("saveShippingNumber [$number]");
        if (!\OrderHelper::isValidGlsCode($number)) {
            throw new Exception('The SAP response has an invalid DELIVERY_ID');
        }
        $order_id = $this->order_id;
        DB::table('orders')->where('id', $order_id)->update(['shipping_number' => $number]);
        DB::table('mi_orders_slips')->where('order_id', $order_id)->update(['delivery_id' => $number]);
        $this->logger->log("Shipping number [$number] as been attached to order [$order_id]", 'order_polling');
        $this->console->info("Shipping number [$number] as been attached to order [$order_id]");
        $this->orderService->getOrder()->logger()->log("Shipping number [$number] attached to order", 'ORDER_FLOW');
    }

    /**
     * Se ha valore "M", nelle colonne VBELN / POSNN  puoi trovare il numero della fattura / riga fattura
     * al cliente finale e nelle colonne VBELV / POSNV il numero della consegna / riga consegna
     *
     * @param $slip
     * @throws Exception
     */
    protected function saveInvoiceNumber(OrderSlipDetail $slip)
    {
        $number = $slip->getNumber();
        $this->audit("saveInvoiceNumber [$number]");
        $this->console->line("saveInvoiceNumber [$number]");
        if (strlen($number) <= 0) {
            throw new Exception('The SAP response has an invalid INVOICE_ID');
        }
        $order_id = $this->order_id;
        DB::table('orders')->where('id', $order_id)->update(['invoice_code' => $number]);
        DB::table('mi_orders_slips')->where('order_id', $order_id)->update(['invoice_id' => $number]);
        $this->logger->log("Invoice number [$number] as been attached to order [$order_id]", 'order_polling');
        $this->console->info("Invoice number [$number] as been attached to order [$order_id]");
        $this->orderService->getOrder()->logger()->log("Invoice number [$number] attached to order", 'ORDER_FLOW');
    }

    /**
     * Se ha valore "R", nelle colonne VBELN / POSNN puoi trovare il numero del documento di
     * scarico di magazzino / riga documento e nelle colonne  VBELV / POSNV  il numero della consegna / riga consegna.
     * Se è presente questa riga significa che la merce è già stata spedita
     *
     * @param $slip
     * @throws Exception
     */
    protected function saveWarehouseNumber(OrderSlipDetail $slip)
    {
        $number = $slip->getNumber();
        $this->audit("saveWarehouseNumber [$number]");
        $this->console->line("saveWarehouseNumber [$number]");
        /*if (strlen($number) <= 0) {
            throw new Exception("The SAP response has an invalid WAREHOUSE_ID");
        }*/
        $orderDetail = $slip->getOrderDetail();
        if ($orderDetail) {
            $this->audit("Setting status SHIPPED to orderDetail $orderDetail->id", __METHOD__);
            $orderDetail->setStatus(OrderDetail::STATUS_SHIPPED);
        }
        $this->saveWithdrawalId($number);
    }

    /**
     * @param $withdrawal_id
     */
    protected function saveWithdrawalId($withdrawal_id)
    {
        if (!\OrderHelper::isValidGlsCode($withdrawal_id)) {
            return;
        }
        //remove the first 2 chars
        $withdrawal_id = substr($withdrawal_id, 2);
        $order_id = $this->order_id;
        DB::table('orders')->where('id', $order_id)->update(['carrier_shipping' => $withdrawal_id]);
        DB::table('mi_orders_slips')->where('order_id', $order_id)->update(['withdrawal_id' => $withdrawal_id]);
        $this->orderService->getOrder()->logger()->log("Carrier number [$withdrawal_id] attached to order", 'ORDER_FLOW');
        $this->logger->log("Carrier shipping number [$withdrawal_id] as been attached to order [$order_id]", 'order_polling');
        $this->console->info("Carrier shipping number [$withdrawal_id] as been attached to order [$order_id]");
    }

    /**
     * Se ha valore “8” nella colonna VBELN puoi trovare il numero della consegna
     * e nella colonna MATNR il numero del ritiro da parte del corriere.
     * Nella colonna ERDAT è presente la data di ritiro richiesta.
     *
     * Se ha valore “9” nella colonna VBELN puoi trovare il numero della consegna
     * e nella colonna MATNR il numero del ritiro da parte del corriere.
     * Nella colonna ERDAT è presente la data di creazione del ritiro.
     * In questo caso il destinatario è il negozio di ritiro.
     *
     *
     * @param $slip
     * @param $shop
     * @throws Exception
     */
    protected function saveCarrierShipping(OrderSlipDetail $slip, $shop = false)
    {
        $delivery_id = $slip->getDelivery();
        if (\OrderHelper::isValidGlsCode($delivery_id)) {
            $order_id = $this->order_id;
            DB::table('orders')->where('id', $order_id)->update(['shipping_number' => $delivery_id]);
            DB::table('mi_orders_slips')->where('order_id', $order_id)->update(['delivery_id' => $delivery_id]);
            $this->orderService->getOrder()->logger()->log("Shipping number [$delivery_id] attached to order", 'ORDER_FLOW');
        }
        $shipping = $slip->getCarrierShipping();
        //check if 'NEG'
        if(\OrderHelper::isNegCode($shipping)){
            //do not save any data or throw exception;
            return;
        }
        $this->audit("saveCarrierShipping [$shipping]");
        $this->console->line("saveCarrierShipping [$shipping]");
        if (!\OrderHelper::isValidGlsCode($shipping)) {
            throw new Exception("The SAP response has an invalid CARRIER_SHIPPING");
        }

        $this->saveWithdrawalId($shipping);

        /** @var OrderCollectorService $collector */
        $collector = OrderCollectorService::getInstance();
        $collector->pushWithLabel(($shop) ? OrderSlipDetail::REASON_SHOP_SHIPPED : OrderSlipDetail::REASON_SHIPPED, $this->orderService->getOrder());

        $orderDetail = $slip->getOrderDetail();
        if ($orderDetail) {
            $this->audit("Setting status SHIPPED/SHOP_SHIPPED to orderDetail $orderDetail->id", __METHOD__);
            $orderDetail->setStatus(($shop) ? OrderDetail::STATUS_SHOP_SHIPPED : OrderDetail::STATUS_SHIPPED);
        }
    }

    /**
     * Se ha valore "C" , nelle colonne VBELV / POSNV puoi trovare il numero dell’ordine di vendita
     * e nella colonna LGNUM la causale di annullamento della posizione.
     * (Z5 quando la merce è stata rifiutata,
     * Z6 quando l’ordine è annullato es. per merce mancante,
     * Z8 quando l’ordine aveva consegna da Deufol e, per l’impossibilità dell’evasione (es. articolo o corredo mancante), viene manualmente annullato)
     * Se è presente questa riga significa che la riga ordine è stata annullata
     *
     * @param $slip
     * @throws Exception
     */
    protected function handleCancelledItem(OrderSlipDetail $slip)
    {
        $reason = $slip->getReason();
        $this->console->line("handleCancelledItem [$reason]");
        if (strlen($reason) <= 0) {
            throw new Exception('The SAP response has an invalid REASON');
        }

        /** @var OrderCollectorService $collector */
        $collector = OrderCollectorService::getInstance();

        $status = null;
        switch ($reason) {
            case OrderSlipDetail::REASON_UNPICKED: //Z5
            case OrderSlipDetail::REASON_CANCELLED_NOT_PICKED: //RRR
                $status = OrderDetail::STATUS_SHOP_UNPICKED;
                //further actions: set Order status to CANCELLED_SAP, collect the Order for notification
                $collector->pushWithLabel(OrderSlipDetail::REASON_UNPICKED, $this->orderService->getOrder());
                break;

            case OrderSlipDetail::REASON_NOT_FOUND: //Z6
            case OrderSlipDetail::REASON_CANCELLED_NOT_PICKED_ALT: //ANN
                $status = OrderDetail::STATUS_SHOP_NOT_FOUND;
                //further actions: pick another best shop for the order, collect the Order for notification
                $collector->pushWithLabel(OrderSlipDetail::REASON_NOT_FOUND, $this->orderService->getOrder());
                break;

            case OrderSlipDetail::REASON_CANCELLED: //Z8
                $status = OrderDetail::STATUS_CANCELLED;
                //further actions: collect the Order for notification
                $collector->pushWithLabel(OrderSlipDetail::REASON_CANCELLED, $this->orderService->getOrder());
                break;

            case OrderSlipDetail::REASON_NOT_HANDLED: //ZC
            case OrderSlipDetail::REASON_NOT_HANDLED_ALT: //INA
                $status = null;
                $collector->pushWithLabel(OrderSlipDetail::REASON_NOT_HANDLED, $this->orderService->getOrder());
                break;

            case OrderSlipDetail::REASON_RMA_STORE: //RES
                $status = null;
                $collector->pushWithLabel(OrderSlipDetail::REASON_RMA_STORE, $this->orderService->getOrder());
                break;

            case OrderSlipDetail::REASON_CANCELLED_NOT_FOUND: //NNN = DO NOTHING
                $status = null;
                //$collector->pushWithLabel(OrderSlipDetail::REASON_RMA_STORE, $this->orderService->getOrder());
                break;
            /*case OrderSlipDetail::REASON_CANCELLED_NOT_PICKED_ALT:
                $status = OrderDetail::STATUS_SHOP_UNPICKED;
                //further actions: set Order status to CANCELLED_SAP, collect the Order for notification
                $collector->pushWithLabel(OrderSlipDetail::REASON_UNPICKED, $this->orderService->getOrder());
                break;*/
        }


        $orderDetail = $slip->getOrderDetail();
        if ($orderDetail and $status) {
            $this->audit("Setting status $status to orderDetail $orderDetail->id", __METHOD__);
            $orderDetail->setStatus($status);
        }
    }


    /**
     * (tracciamento consegna del prodotto da DEUFOL O NEGOZIO al CLIENTE)
     * Se ha valore “7” nella colonna VBELN puoi trovare il numero della consegna e
     * nella colonna MATNR il numero dello scontrino di vendita.
     * Nella colonna ERDAT è presente la data di emissione dello scontrino.     *
     *
     * @param $slip
     * @throws Exception
     */
    protected function saveOrderReceipt(OrderSlipDetail $slip)
    {
        /** @var OrderCollectorService $collector */
        $collector = OrderCollectorService::getInstance();
        $receipt = $slip->getReceipt();
        $order_id = $this->order_id;

        if (strlen($receipt) > 0) {
            DB::table('orders')->where('id', $order_id)->update(['receipt' => $receipt]);
            $this->orderService->getOrder()->logger()->log("Receipt number [$receipt] attached to order", 'ORDER_FLOW');
            $collector->pushWithLabel(OrderSlipDetail::REASON_SHOP_SOLD, $this->orderService->getOrder());
        }

        $orderDetail = $slip->getOrderDetail();
        if ($orderDetail) {
            $status = OrderDetail::STATUS_SHOP_SOLD;
            $this->audit("Setting status $status to orderDetail $orderDetail->id", __METHOD__);
            $orderDetail->setStatus($status);
        }
    }


    /**
     * Se ha valore "P", il pacco è pronto per il ritiro in store a cura del cliente;
     * Internamente quando si verifica questa casistica dovremmo aggiornare lo status ordine
     * da 'consegnato' a 'pronto per il ritiro' ed inviare la prima mail di
     * pacco pronto per il ritiro al cliente con tutte le indicazioni dello store
     *
     * @param $slip
     * @throws Exception
     */
    protected function handlePackageReady(OrderSlipDetail $slip)
    {
        /** @var OrderCollectorService $collector */
        $collector = OrderCollectorService::getInstance();
        $collector->pushWithLabel(OrderSlipDetail::REASON_PACKAGE_READY, $this->orderService->getOrder());

        $orderDetail = $slip->getOrderDetail();
        if ($orderDetail) {
            $status = OrderDetail::STATUS_SHOP_SHIPPED;
            $this->audit("Setting status $status to orderDetail $orderDetail->id", __METHOD__);
            $orderDetail->setStatus($status);
        }
    }


    /**
     * Se ha valore "1", il pacco è stato ritirato in store
     *
     * @param $slip
     * @throws Exception
     */
    protected function handleRitiredOnly(OrderSlipDetail $slip)
    {
        /** @var OrderCollectorService $collector */
        $collector = OrderCollectorService::getInstance();
        $collector->pushWithLabel(OrderSlipDetail::REASON_SHOP_RETIRED, $this->orderService->getOrder());

        $orderDetail = $slip->getOrderDetail();
        if ($orderDetail) {
            $status = OrderDetail::STATUS_SHOP_SOLD;
            $this->audit("Setting status $status to orderDetail $orderDetail->id", __METHOD__);
            $orderDetail->setStatus($status);
        }
    }

    /**
     * Se ha valore "X", nelle colonne VBELN / POSNN significa che l’ordine su Negoziando
     * è stato indicato come “non trovato”. Se il flusso di “non trovato” viene poi
     * correttamente completato in sede con l’annullamento dell’ordine,
     * verrà inserito il motivo di rifiuto (Z6/Z5) nella riga del flusso che presenta il valore “C”.
     *
     * @param $slip
     * @throws Exception
     */
    protected function handleShopNotFound(OrderSlipDetail $slip)
    {
        $reason = $slip->getReason();
        /** @var OrderCollectorService $collector */
        $collector = OrderCollectorService::getInstance();

        switch ($reason) {
            case OrderSlipDetail::REASON_CANCELLED_NOT_FOUND:
            case OrderSlipDetail::REASON_CANCELLED_NOT_FOUND_ALT:
                $collector->pushWithLabel(OrderSlipDetail::REASON_SHOP_NOT_FOUND, $this->orderService->getOrder());

                $status = OrderDetail::STATUS_NOT_FOUND;

                $orderDetail = $slip->getOrderDetail();
                if ($orderDetail and $status) {
                    $this->audit("Setting status $status to orderDetail $orderDetail->id", __METHOD__);
                    $orderDetail->setStatus($status);
                }
                break;
        }
    }

    /**
     * @param OrderSlipDetail $slip
     */
    protected function handleShopCancelledNotFound(OrderSlipDetail $slip)
    {
        /** @var OrderCollectorService $collector */
        $collector = OrderCollectorService::getInstance();
        $collector->pushWithLabel(OrderSlipDetail::REASON_CANCELLED_NOT_FOUND, $this->orderService->getOrder());

        $status = OrderDetail::STATUS_NOT_FOUND;

        $orderDetail = $slip->getOrderDetail();
        if ($orderDetail and $status) {
            $this->audit("Setting status $status to orderDetail $orderDetail->id", __METHOD__);
            $orderDetail->setStatus($status);
        }
    }


}