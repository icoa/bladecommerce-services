<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 17/02/2017
 * Time: 10:08
 */

namespace services\Morellato\Soap\Response;

use Config;
use DB;
use services\Morellato\Soap\Service\AbstractOrderService;
use Utils;
use Exception;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use services\Morellato\Loggers\DatabaseOrderLogger as Logger;
use services\Morellato\Soap\Service\OrderService;

class AbstractOrderResponse
{
    const ORDER_OK = 200;
    const ORDER_WARNING = 300;
    const ORDER_NOT_FOUND = 404;
    const ORDER_ERROR = 500;

    protected $console;
    protected $files;
    protected $logger;
    protected $response;
    protected $orderService;
    protected $order_id;
    protected $storage_path;

    function __construct(AbstractOrderService $orderService, Command $console, Filesystem $files, Logger $logger)
    {
        $this->console = $console;
        $this->files = $files;
        $this->logger = $logger;
        $this->orderService = $orderService;
        $this->order_id = $orderService->getOrderId();
    }

    function setResponse($response){
        $this->response = $response;
        return $this;
    }


    function saveResponse(){
        $file = $this->orderService->getOrderReference().'.log';
        $path = storage_path($this->storage_path) . '/' . $file;
        $this->files->put($path,serialize($this->response));
    }

    function make(){

    }

    protected function addMessage($node){
        $data = [
            'order_id' => $this->order_id,
            'message_type' => $node->TYPE,
            'message_id' => $node->ID,
            'message_number' => $node->NUMBER,
            'message_text' => $node->MESSAGE,
            'message_parameter' => $node->PARAMETER,
            'created_at' => date('Y-m-d H:i:s')
        ];
        DB::table('mi_orders_messages')->insert($data);
    }


    protected function purgeMessages(){
        DB::table('mi_orders_messages')->where('order_id',$this->order_id)->delete();
    }


    protected function setOrderSlip($delivery_id, $sales_id){
        $delivery_id = \OrderHelper::isValidGlsCode($delivery_id) ? $delivery_id : null;
        $sales_id = \OrderHelper::isValidGlsCode($sales_id) ? $sales_id : null;
        $data = [
            'order_id' => $this->order_id,
            'delivery_id' => $delivery_id,
            'sales_id' => $sales_id,
            'created_at' => date('Y-m-d H:i:s')
        ];
        //upsert the data
        $many = DB::table('mi_orders_slips')->where('order_id',$this->order_id)->count();
        if($many > 0){
            DB::table('mi_orders_slips')->where('order_id',$this->order_id)->update($data);
        }else{
            DB::table('mi_orders_slips')->insert($data);
        }
    }


    protected function setOrderShippingNumber($delivery_id){
        $delivery_id = \OrderHelper::isValidGlsCode($delivery_id) ? $delivery_id : null;
        DB::table('orders')->where('id',$this->order_id)->update(['shipping_number' => $delivery_id]);
    }

}