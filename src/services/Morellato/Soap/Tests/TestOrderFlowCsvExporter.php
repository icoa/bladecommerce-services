<?php

namespace services\Morellato\Soap\Tests;

use SplFileObject;
use League\Csv\Writer;

/**
 * Class TestOrderFlowCsvExporter
 * @package services\Morellato\Soap\Tests
 */
class TestOrderFlowCsvExporter
{

    const CSV_DELIMITER = ";";
    const CSV_NEWLINE = "\r\n";

    /**
     * @param array $headers
     * @param array $lines
     * @return string
     */
    function make(array $headers, array $lines)
    {
        $rows = array_merge([$headers], $lines);

        $targetFilePath = storage_path('morellato/csv/export/ORDERS_WS2.csv');

        $writer = Writer::createFromPath(new SplFileObject($targetFilePath, 'w'), 'w');
        $writer->setEncodingFrom('UTF-8');
        $writer->setDelimiter(self::CSV_DELIMITER); //the delimiter will be the tab character
        $writer->setNewline(self::CSV_NEWLINE); //use windows line endings for compatibility with some csv libraries
        $writer->insertAll($rows);
        return $targetFilePath;
    }
}