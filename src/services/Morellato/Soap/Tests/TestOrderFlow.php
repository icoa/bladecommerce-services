<?php

namespace services\Morellato\Soap\Tests;

use Config;
use DB;
use Illuminate\Support\Collection;
use services\Models\OrderSlipDetail;
use services\Morellato\Soap\Handler\OrderFlowHandler;
use services\OrderCollectorConsumer;
use services\OrderCollectorService;
use services\Traits\TraitConsole;
use Utils;
use Exception;
use Order;
use Illuminate\Filesystem\Filesystem;
use Symfony\Component\Finder\SplFileInfo;
use services\Morellato\Soap\Response\OrderFlowResponse;

class TestOrderFlow
{
    use TraitConsole;

    protected $files;
    protected $fields = ['ORDER', 'VBTYP_N', 'VBELN', 'MATNR', 'VBELV', 'POSNV', 'LGNUM'];
    protected $lines = [];

    /**
     * TestOrderFlow constructor.
     * @param Filesystem $files
     */
    function __construct(Filesystem $files)
    {
        $this->files = $files;
    }

    function scanDirectory()
    {
        $fields = $this->fields;
        /** @var SplFileInfo[] $files */
        $files = $this->files->allFiles(storage_path('morellato/orders/flows/response'));

        foreach ($files as $file) {
            $fileName = $file->getFilename();
            $order = explode('.', $fileName)[0];
            $content = $this->files->get($file->getPathname());
            $this->getConsole()->comment("Testing file [$fileName]");
            $data = unserialize($content);
            //audit('-----------------');
            $items = OrderFlowResponse::getItems($data);
            $lines = $this->itemsToTable($items, $order);
            $this->pushLines($lines);
            $this->getConsole()->table($fields, $lines);
            //audit($items);
        }
    }

    /**
     * @param array $items
     * @param $order
     * @return array
     */
    function itemsToTable(array $items, $order)
    {
        $fields = $this->fields;
        $lines = [];
        foreach ($items as $item) {
            $row = [];
            $item->ORDER = $order;
            foreach ($fields as $field) {
                $row[$field] = isset($item->$field) ? $item->$field : null;
            }
            $lines[] = $row;
        }
        return $lines;
    }

    /**
     * @param array $lines
     */
    function pushLines(array $lines)
    {
        $this->lines = array_merge($this->lines, $lines);
    }

    /**
     *
     */
    function exportToCsv()
    {
        $this->getConsole()->comment("Exporting data to CSV file...");
        if (empty($this->lines)) {
            $this->getConsole()->error('No lines could be parsed; CSV generation skipped');
            return;
        }
        $exporter = new TestOrderFlowCsvExporter();
        $fileName = $exporter->make($this->fields, $this->lines);
        $this->getConsole()->info("CSV file successfully generated to [$fileName]");
    }

    /**
     * @param $reference
     * @throws Exception
     */
    function scanSingleFile($reference)
    {
        $filePath = storage_path("morellato/orders/flows/response/$reference.log");
        if (!$this->files->exists($filePath))
            throw new Exception("File [$filePath] does not exist");

        $order = Order::where('reference', $reference)->first();

        $content = $this->files->get($filePath);
        $this->getConsole()->comment("Testing file [$filePath]");
        $data = unserialize($content);
        //audit('-----------------');
        $items = OrderFlowResponse::getItems($data);
        $slips = Collection::make([]);
        foreach ($items as $item) {
            $slip = new OrderSlipDetail();
            $slip->setAttributesByOrderFlowItem($item);
            $slips->push($slip);

            if ($order) {
                $slip->setOrderId($order->id);
                $slip->findOrderDetail();
                $slip->upsertPayload();
            }


        }
        print_r($slips->toArray());
    }


    function callWebServiceForCustomOrders($ids)
    {
        /** @var OrderFlowHandler $handler */
        $handler = \App::make('services\Morellato\Soap\Handler\OrderFlowHandler', [$this->getConsole()]);
        $handler->setSimulateSoapCall(true);
        $rows = Order::whereIn('id', $ids)->get();
        foreach ($rows as $row) {
            try {
                $this->getConsole()->line("Processing order [$row->id]");
                $handler->sendOrder($row->id);
            } catch (Exception $e) {
                audit_exception($e, __METHOD__);
            }
        }

        $collector = OrderCollectorService::getInstance();
        $orders = $collector->toArray();
        audit($orders, 'OrderCollectorService::orders');
    }


    function walkCollectorForCustomOrders($schema)
    {
        $collector = OrderCollectorService::getInstance();
        foreach ($schema as $order_id => $label) {
            $payload = Order::getObj($order_id);
            $collector->pushWithLabel($label, $payload);
        }
        $consumer = new OrderCollectorConsumer($collector);
        $consumer->takeActionsAfterOrderFlow();
    }
}