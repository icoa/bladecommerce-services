<?php

namespace services\Morellato\Soap\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Order;
use Payment;
use OrderState;
use PaymentState;
use Illuminate\Database\Eloquent\Builder;
use services\Models\OrderSlipDetail;
use services\Morellato\Soap\Service\AbstractOrderService;
use Carrier;
use services\Models\OrderSlip;
use services\Models\CustomerEmailWarning;
use Config;
use DB;
use MorellatoShop;
use Exception;

/**
 * Trait TraitOrderSap
 * @package services\Morellato\Soap\Traits
 * @mixin Order
 */
trait TraitOrderSap
{
    /**
     * @return HasOne
     */
    function order_slip()
    {
        return $this->hasOne(OrderSlip::class);
    }

    /**
     * @return HasMany
     */
    function order_slip_details()
    {
        return $this->hasMany(OrderSlipDetail::class);
    }

    /**
     * @return bool
     */
    function isMorellato()
    {
        $start_id = Config::get('soap.order.startId');
        if ($this->id <= $start_id)
            return false;

        if ($this->availability_mode == 'master')
            return false;

        if (feats()->sapReceipts())
            return true;

        $query = "select count(id) as aggregate from products where source='morellato' and id in (select product_id from order_details where order_id={$this->id})";
        $result = DB::selectOne($query);
        return $result->aggregate;
    }


    /**
     * @return array|static[]
     */
    function getMorellatoMessages()
    {
        return DB::table('mi_orders_messages')->where('order_id', $this->id)->orderBy('id', 'desc')->get();
    }

    /**
     * @return OrderSlip
     */
    function getMorellatoSlip()
    {
        return OrderSlip::where('order_id', $this->id)->first();
    }

    /**
     * @return bool
     */
    function hasShop()
    {
        return (($this->availability_mode == 'shop' or $this->availability_mode == 'mixed') and $this->availability_shop_id != '');
    }

    /**
     * @return MorellatoShop|null
     */
    function getMorellatoShop()
    {
        if ($this->hasShop()) {
            $code = $this->availability_shop_id;
            $model = MorellatoShop::getByCode($code);
            if ($model) {
                return $model;
            }
        }
        return null;
    }

    /**
     * @return null|string
     */
    function getSapXmlFile()
    {
        $folder = storage_path('morellato/orders/');
        $filename = $this->reference . '.xml';
        $path = $folder . $filename;
        if (\File::exists($path)) {
            return $path;
        }
        return null;
    }

    /**
     * Selects all orders that can me viable for SAP
     *
     * @param Builder $builder
     */
    function scopeForSap(Builder $builder)
    {
        $this->scopeWithBasicAvailability($builder);
        $start_id = config('soap.order.startId');
        $builder->where('id', '>', $start_id);
    }

    /**
     * Selects all orders that can me viable for SAP
     *
     * @param Builder $builder
     */
    function scopeWithoutSlips(Builder $builder)
    {
        $builder->whereNotIn('id', function ($query) {
            $query->select('order_id')->from('mi_orders_slips');
        });
    }

    /**
     * Selects all orders that can me submitted to SAP
     *
     * @param Builder $builder
     */
    function scopeSubmittableToSap(Builder $builder)
    {
        $order_status_ok = [OrderState::STATUS_WORKING];

        $payment_status_offline = [
            PaymentState::STATUS_PENDING_BANKTRANSFER,
            PaymentState::STATUS_PENDING_RIBA,
            PaymentState::STATUS_PENDING_CASH,
            PaymentState::STATUS_PENDING_SHOP
        ];

        $payment_status_online = [PaymentState::STATUS_COMPLETED];

        $invalid_modes = [Order::MODE_MASTER, Order::MODE_OFFLINE, Order::MODE_LOCAL];

        //is SAP receipts are enabled, then 'offline' and 'local' are OK to be sent
        if (feats()->sapReceipts()) {
            $invalid_modes = [Order::MODE_MASTER, Order::MODE_OFFLINE];
        }

        $membership_enabled = \Core::membership();

        $builder
            ->orderBy('id', 'desc')
            ->forSap()
            ->withoutAvailabilityModes($invalid_modes)
            ->withoutSlips()
            ->where(function ($subQuery) use ($order_status_ok, $payment_status_offline, $payment_status_online) {
                $subQuery->where(function ($nestQuery) use ($order_status_ok, $payment_status_offline, $payment_status_online) {
                    $nestQuery->whereIn('status', $order_status_ok)->whereIn('payment_status', $payment_status_offline);
                })->orWhere(function ($nestQuery) use ($order_status_ok, $payment_status_offline, $payment_status_online) {
                    $nestQuery->whereIn('status', $order_status_ok)->whereIn('payment_status', $payment_status_online);
                });
            });

        //if SAP receipts are enabled, than even 'affiliate' orders can be submitted
        if ($membership_enabled and !feats()->sapReceipts()) {
            $builder->whereNull('affiliate_id');
        }
    }


    /**
     * Selects all orders that can me submitted to SAP OrderFlow WebService
     *
     * @param Builder $builder
     */
    function scopeSubmittableToSapOrderFlow(Builder $builder)
    {
        $days = -1 * (int)config('soap.order.flow_min_days', 40);
        $date = Carbon::yesterday()->addDays($days)->format('Y-m-d');
        $invalid_modes = [Order::MODE_MASTER, Order::MODE_OFFLINE, Order::MODE_LOCAL];
        //is SAP receipts are enabled, then 'offline' and 'local' are OK to be sent
        if (feats()->sapReceipts()) {
            $invalid_modes = [Order::MODE_MASTER, Order::MODE_OFFLINE];
        }

        $invalid_order_states = [
            OrderState::STATUS_NEW,
            OrderState::STATUS_CANCELED,
            OrderState::STATUS_CANCELED_AFFILIATE,
            OrderState::STATUS_CANCELED_SAP,
            OrderState::STATUS_CLOSED,
            OrderState::STATUS_REFUNDED,
            OrderState::STATUS_REFUNDED_PARTIALLY,
            OrderState::STATUS_INVOICE,
            OrderState::STATUS_OUTOFSTOCK,
            OrderState::STATUS_DELIVERED,
            OrderState::STATUS_PENDING,
            OrderState::STATUS_PENDING_DELIVERY,
            //OrderState::STATUS_SHIPPED,
            //OrderState::STATUS_READY_PICKUP, //this is a stop
        ];

        $invalid_payment_states = [
            PaymentState::STATUS_PENDING_RIBA,
            PaymentState::STATUS_PENDING_BANKTRANSFER,
            PaymentState::STATUS_PENDING_CREDITCARD,
            PaymentState::STATUS_PENDING_PAYPAL,
            //PaymentState::STATUS_PENDING_SHOP,
            PaymentState::STATUS_ERROR,
            PaymentState::STATUS_EXPIRED,
            PaymentState::STATUS_USER_CANCELED,
            PaymentState::STATUS_REVERSED,
            PaymentState::STATUS_REFUNDED_PARTIALLY,
            PaymentState::STATUS_REFUNDED,
        ];

        $membership_enabled = \Core::membership();

        $builder
            ->orderBy('id', 'desc')
            ->forSap()
            ->where('created_at', '>=', $date)
            ->withoutStatus($invalid_order_states)
            ->withoutPaymentStatus($invalid_payment_states)
            ->withoutAvailabilityModes($invalid_modes);

        //if SAP receipts are enabled, than even 'affiliate' orders can be submitted
        if ($membership_enabled and !feats()->sapReceipts()) {
            $builder->whereNull('affiliate_id');
        }
    }

    /**
     * Selects all orders that can be migrated to 'WORKING' status from 'NEW' status
     *
     * @param Builder $builder
     */
    function scopeMigrationableToWorking(Builder $builder)
    {
        $valid_carriers = [
            Carrier::TYPE_NATIONAL_DELIVERY,
            Carrier::TYPE_VIRTUAL_DELIVERY,
            Carrier::TYPE_B2B_NATIONAL_DELIVERY,
            Carrier::TYPE_B2B_SHOP_WITHDRAWAL,
            Carrier::TYPE_SHOP_WITHDRAWAL,
        ];
        //These are all payments types that require 'Payment success' payment state
        $payments_requiring_success_payment_status = [
            Payment::TYPE_CREDITCARD,
            Payment::TYPE_PAYPAL,
            Payment::TYPE_GIFTCARD,
            Payment::TYPE_SHOP_ANTICIPATED,
            //Payment::TYPE_BANKTRANSFER,
            //Payment::TYPE_RIBA,
        ];

        //These are all payments types that DO not require 'Payment success' payment state
        $payments_not_requiring_success_payment_status = [
            Payment::TYPE_CASH => PaymentState::STATUS_PENDING_CASH,
            Payment::TYPE_WITHDRAWAL => PaymentState::STATUS_PENDING_SHOP,
            Payment::TYPE_SHIPPING_POINT => PaymentState::STATUS_PENDING_SHOP,
        ];

        $invalid_modes = [Order::MODE_MASTER, Order::MODE_OFFLINE, Order::MODE_LOCAL];

        $membership_enabled = \Core::membership();

        $builder
            ->orderBy('id', 'desc')
            ->forSap()
            ->withStatus(OrderState::STATUS_NEW)
            ->withCarrier($valid_carriers)
            ->withoutAvailabilityModes($invalid_modes)
            ->withoutSlips()
            ->where(function ($subQuery) use ($payments_requiring_success_payment_status, $payments_not_requiring_success_payment_status) {
                $subQuery->where(function ($nestQuery) use ($payments_requiring_success_payment_status) {
                    $nestQuery->where('payment_status', PaymentState::STATUS_COMPLETED)->whereIn('payment_id', $payments_requiring_success_payment_status);
                });

                foreach ($payments_not_requiring_success_payment_status as $payment_id => $payment_status) {
                    $subQuery->orWhere(function ($nestQuery) use ($payment_id, $payment_status) {
                        $nestQuery->where(compact('payment_id', 'payment_status'));
                    });
                }
            });

        //if SAP receipts are enabled, than even 'affiliate' orders can be submitted
        if ($membership_enabled and !feats()->sapReceipts()) {
            $builder->whereNull('affiliate_id');
        }
    }

    /**
     * Selects all orders that can be migrated to 'CLOSED' status
     *
     * @param Builder $builder
     */
    function scopeMigrationableToClosed(Builder $builder)
    {
        $date = Carbon::now()->addDay(-1)->format('Y-m-d H:i:s');

        $valid_carriers = [
            Carrier::TYPE_NATIONAL_DELIVERY,
        ];

        $valid_states = [
            OrderState::STATUS_DELIVERED,
        ];

        $invalid_modes = [Order::MODE_MASTER, Order::MODE_OFFLINE, Order::MODE_LOCAL];

        $membership_enabled = \Core::membership();

        $builder
            ->where('created_at', '<=', $date)
            ->orderBy('id', 'desc')
            ->forSap()
            ->withStatus($valid_states)
            ->withCarrier($valid_carriers)
            ->withoutAvailabilityModes($invalid_modes)
            ;

        //if SAP receipts are enabled, than even 'affiliate' orders can be submitted
        if ($membership_enabled and !feats()->sapReceipts()) {
            $builder->whereNull('affiliate_id');
        }
    }

    /**
     * @param string $givenDocType
     * @return mixed|null
     */
    function getSapDocTypeCode($givenDocType = null)
    {
        $code = null;
        if ($this->isStarling() or $this->isReceipt()) {
            $code = config('soap.doc_types.receipt', 'ZWE4');
        } else {
            if ($givenDocType == 'ZWE1') {
                $code = $this->isInvoiceRequired() ? config('soap.doc_types.invoice', 'ZWE1') : config('soap.doc_types.receipt', 'ZWE4');
            }
            //further check is the order is still valid for receipt
            if ($this->isValidForReceipt()) {
                $code = config('soap.doc_types.receipt', 'ZWE4');
            }
        }
        return $code;
    }

    /**
     * @return bool
     */
    function hasSapInvoice()
    {
        $slip = $this->getMorellatoSlip();
        if ($slip) {
            return ((string)$slip->invoice_id) != '';
        }
        return false;
    }

    /**
     * @return string|null
     */
    function getSapInvoice()
    {
        $slip = $this->getMorellatoSlip();
        if ($slip) {
            return ((string)$slip->invoice_id != '') ? $slip->invoice_id : null;
        }
        return null;
    }

    /**
     * @return bool
     */
    function hasSapDelivery()
    {
        $slip = $this->getMorellatoSlip();
        if ($slip) {
            $delivery_id = (string)$slip->delivery_id;
            return \OrderHelper::isValidGlsCode($delivery_id);
        }
        return false;
    }

    /**
     * @return string|null
     */
    function getSapDelivery()
    {
        $slip = $this->getMorellatoSlip();
        if ($slip) {
            $delivery_id = (string)$slip->delivery_id;
            return \OrderHelper::isValidGlsCode($delivery_id) ? $delivery_id : null;
        }
        return null;
    }

    /**
     * @return bool
     */
    function hasSapSale()
    {
        $slip = $this->getMorellatoSlip();
        if ($slip) {
            return ((string)$slip->sales_id) != '';
        }
        return false;
    }

    /**
     * @return string|null
     */
    function getSapSale()
    {
        $slip = $this->getMorellatoSlip();
        if ($slip) {
            return ((string)$slip->sales_id != '') ? $slip->sales_id : null;
        }
        return null;
    }

    /**
     * @return string
     */
    function getSapPaymentCustomerCode()
    {
        $payment_id = $this->payment_id;
        $payment_code = config('soap.payments_codes.' . $payment_id);
        if ($payment_code == null) {
            $payment_code = config('soap.order.shopDelivery.orderHeaderIn.PARTN_NUMB');
        }
        return $payment_code;
    }

    /**
     * @return string
     */
    function getSapScenary()
    {
        $orderService = new AbstractOrderService($this->id, $this);
        return $orderService->getScenary();
    }


    /**
     * @param bool $debug
     * @return array
     */
    function statusCanBeMigratedToWorking($debug = false)
    {
        /**
         * An 'order' can be migrated to 'WORKING' if:
         *
         * check product 'offline'
         * check quantities for 'online'
         * check real-time quantities for 'shop'
         * check if conditions for 'PDV' are met
         */

        $console = \Registry::console();

        $products = $this->getProducts();

        $messages = [];

        $order_availability_mode = $this->availability_mode;
        $order_availability_shop = $this->getAvailabilityShop();
        $order_delivery_store = $this->getDeliveryStore();
        $reference = $this->reference;

        //audit(compact('order_availability_mode', 'order_availability_shop', 'order_delivery_store'), __METHOD__, 'Requirements');

        $infos = [];

        foreach ($products as $product) {
            $info = new \stdClass();
            $availability_mode = $product->availability_mode;
            $availability_shop_id = $product->availability_shop_id;
            $warehouse = $product->warehouse;
            $shop = \MorellatoShop::getByCode($availability_shop_id);
            $shop_name = ($shop) ? $shop->name : 'PDV NOT FOUND';
            $quantity = $product->product_quantity;
            $combination = $product->product_combination_id > 0 ? \ProductCombination::getObj($product->product_combination_id) : null;
            $product_obj = \Product::getObj($product->product_id);
            if ($debug) {
                $product_obj->sku = $product->product_reference;
                $product_obj->sap_sku = $product->product_sap_reference;
            }
            $sku = \Utils::getSapSku($product_obj->sku, $product_obj->sap_sku);

            $info->sku = $sku;
            $info->quantity = $quantity;
            $info->shop = $shop;


            if ($console) {
                $console->line("Checking product SKU: $sku | AVAILABILITY_MODE: $availability_mode");
            }

            //if the availability is 'offline', try to find an "online" quantity...
            //...if the "online" quantity is not found, try to find a "shop" quantity with a valid "shop";
            //Warning: the shop must be shared for all "products" in the same order;
            //if any of the quantities is still 'offline', than the 'order' cannot be migrated
            if ($availability_mode == Order::MODE_OFFLINE) {
                $info->availability = Order::MODE_OFFLINE;
                $hasOnlineQuantities = \ProductHelper::checkOnlineQuantities($product_obj, $quantity, $combination);
                if ($hasOnlineQuantities) {
                    //audit("Offline quantities is now online");
                    $messages[] = "Product $sku was 'offline' but I found a valid 'online' stock";
                    $info->availability = Order::MODE_ONLINE;
                } else {
                    $hasShopQuantities = \ProductHelper::findShopQuantities($product_obj, $quantity, $combination, $this->id);
                    if ($hasShopQuantities) {
                        //audit("Offline quantities is now shop");
                        $messages[] = "Product $sku was 'offline' but I found a valid 'shop' stock";
                        $info->availability = Order::MODE_SHOP;
                    } else {
                        //audit("Offline quantities is still offline");
                        $messages[] = "Product $sku was 'offline' and still I cannot find an available stock (online or shop)";
                    }
                }
            }

            //if the availability is 'online', check if the quantity is still available
            if ($availability_mode == Order::MODE_ONLINE) {
                $info->availability = Order::MODE_ONLINE;
                $hasOnlineQuantities = \ProductHelper::checkOnlineQuantities($product_obj, $quantity, $combination);
                if ($hasOnlineQuantities) {
                    //audit("Online quantities are confirmed");
                    $messages[] = "Product $sku is 'online' in '$warehouse' and stocks are still valid";
                } else {
                    //audit("Online quantities are NOT confirmed");
                    $messages[] = "Product $sku is 'online' but I cannot confirm an available online stock";
                    $hasShopQuantities = \ProductHelper::findShopQuantities($product_obj, $quantity, $combination, $this->id);
                    if ($hasShopQuantities) {
                        //audit("Online quantities is now shop");
                        $messages[] = "Product $sku was 'online' but I found a valid 'shop' stock";
                        $info->availability = Order::MODE_SHOP;
                    } else {
                        //audit("Online quantities are now offline");
                        $messages[] = "Product $sku is online and I cannot find an available 'shop' stock";
                        $info->availability = Order::MODE_OFFLINE;
                    }
                }
            }

            //if the availability is 'shop', check if the quantity is still available in the selected shop
            if ($availability_mode == Order::MODE_SHOP) {
                $info->availability = Order::MODE_SHOP;
                $hasStillQuantities = \ProductHelper::checkShopQuantities($availability_shop_id, $product_obj, $quantity, $combination, $this->id);
                if ($hasStillQuantities) {
                    //audit("Shop quantities are confirmed");
                    $messages[] = "Product $sku is 'shop' with availability in '$shop_name' and stocks are still valid";
                } else {
                    //audit("Shop quantities are NOT confirmed");
                    $messages[] = "Product $sku is 'shop' with availability in '$shop_name' but I cannot confirm an available 'shop' stock on the same PDV";
                    $info->availability = Order::MODE_OFFLINE;
                }
            }

            if (in_array($availability_mode, [Order::MODE_LOCAL, Order::MODE_STAR])) {
                $info->availability = $availability_mode;
            }

            $infos[] = $info;
        }

        $checksums = [
            Order::MODE_LOCAL => 0,
            Order::MODE_SHOP => 0,
            Order::MODE_ONLINE => 0
        ];

        //populate checksums
        foreach ($infos as $info) {
            if (array_key_exists($info->availability, $checksums))
                $checksums[$info->availability] = 1;
        }

        $valid = true;
        //if the availability flags are not all the same, then the order is 'mixed' and should be checked manually
        if (array_sum($checksums) > 1)
            $valid = false;

        foreach ($infos as $info) {
            //if we found at least an 'offline' item, the order cannot be migrated
            if ($info->availability == Order::MODE_OFFLINE) {
                $valid = false;
            }
        }

        if ($valid == false) {
            $messages[] = "Warning: the order '$reference' is now in MIXED or OFFLINE mode; this order cannot be SAFELY migrated and should be managed manually!";
        }

        if ($this->hasSapIssues()) {
            $valid = false;
            $messages = array_merge($messages, $this->getSapIssues());
        }

        if ($console) {
            foreach ($messages as $message) {
                $console->comment($message);
            }
            $console->info("Dumping \$infos...");
            foreach ($infos as $info) {
                $console->line("SKU: $info->sku | QUANTITY: $info->quantity | AVAILABILITY: $info->availability");
            }
            $console->info("Dumping \$checksums...");
            print_r($checksums);
            $console->info("Checksum is: " . array_sum($checksums));
        }

        return compact('valid', 'messages');
    }


    /**
     * @return array
     */
    function getSapIssues()
    {
        $messages = [];
        $registry_key = 'order_sap_issues_' . $this->id;
        if (\Registry::has($registry_key))
            return \Registry::get($registry_key, []);

        //if the current order status is one of the "negative" status, no issues should be raised
        if ($this->hasCurrentStatus([
            OrderState::STATUS_CANCELED,
            OrderState::STATUS_CANCELED_AFFILIATE,
            OrderState::STATUS_REFUNDED_PARTIALLY,
            OrderState::STATUS_REFUNDED,
        ])
        ) {
            return $messages;
        }

        $reference = $this->reference;
        $order_availability_mode = $this->availability_mode;
        $payment = $this->getPayment();
        $deliveryStore = $this->getDeliveryStore();
        $availabilityStore = $this->getAvailabilityShop();
        $products = $this->getProducts();

        //if the invoice is required, check if we have at least a valid shipping address or billing address with a Tax/Vat code
        if ($this->isInvoiceRequired()) {
            $passed = false;
            $shippingAddress = $this->getShippingAddress();
            if ($shippingAddress and $shippingAddress->hasFiscalCode()) {
                $passed = true;
            }

            $billingAddress = $this->getBillingAddress();
            if ($billingAddress and $billingAddress->hasFiscalCode()) {
                $passed = true;
            }

            if (false === $passed) {
                $messages[] = "Order $reference could not be submitted to SAP, since the invoice is required but there is not a valid FISCAL CODE in any of the addresses provided";
            }
        }

        //if the shipping method is "ritiro al negozio" a valid 'withdrawal shop' must be present
        if ($payment->module == Payment::MODULE_SHOP_PICK) {
            if (is_null($deliveryStore)) {
                $messages[] = "Order $reference could not be submitted to SAP, since no withdrawal shop has been selected for shipment method 'Pick in store'";
            }
        }

        //if the availability mode is "shop" a valid 'availability shop' must be present
        if ($this->availability_mode == Order::MODE_SHOP) {
            if (is_null($availabilityStore)) {
                $messages[] = "Order $reference could not be submitted to SAP, since no availability shop has been selected for availability mode 'shop'";
            }
        }

        //if the are at least on product with 'offline' availability
        foreach ($products as $product) {
            $availability_mode = $product->availability_mode;
            //skip mode_star
            if ($availability_mode == Order::MODE_STAR) {
                continue;
            }
            if ($availability_mode == Order::MODE_OFFLINE) {
                $messages[] = "Order $reference could not be submitted to SAP, since product [$product->product_reference] has availability mode set to 'offline'";
            }
            //check if the availability_mode of the order reflects any availability_mode given by the products
            if ($order_availability_mode != $availability_mode) {
                $messages[] = "Order $reference could not be submitted to SAP, since product [$product->product_reference] has availability mode set to '$availability_mode' while order mode is set to '$order_availability_mode'";
            }
        }

        \Registry::set($registry_key, $messages);

        return $messages;
    }

    /**
     * @return bool
     */
    function hasSapIssues()
    {
        return !empty($this->getSapIssues());
    }

    /**
     * @return null|string
     */
    function getOrderFlowResponseFile()
    {
        $filePath = storage_path("morellato/orders/flows/response/{$this->reference}.log");
        return file_exists($filePath) ? $filePath : null;
    }

    /**
     * Increase order reference, taking post actions (reset slips)
     */
    function doActionIncrease()
    {
        $model = Order::find($this->id);
        $reference = \OrderManager::increaseReference($model->reference);
        $model->reference = $reference;
        $model->invoice_code = null;
        $model->shipping_number = null;
        $model->tracking_url = null;
        $model->carrier_shipping = null;
        $model->save(['timestamps' => false]);
        \DB::table('mi_orders_slips')->where('order_id', $model->id)->delete();
        $model->logger()->log("Order increased with new reference $reference", 'GENERAL');
    }

    /**
     * Selects all orders with a given order slip details reason
     *
     * @param Builder $builder
     * @param string|array $type
     */
    function scopeWithOrderSlipDetailReason(Builder $builder, $type)
    {
        if (!is_array($type))
            $type = [$type];

        $type_str = "'" . implode("','", $type) . "'";

        $builder->whereRaw("EXISTS
    (SELECT count(mi_orders_slips_details.id) as cnt
     FROM mi_orders_slips_details
     WHERE mi_orders_slips_details.order_id = orders.id 
     AND mi_orders_slips_details.lgnum IN ($type_str)
     HAVING cnt > 0)");
    }

    /**
     * Selects all orders without a given order slip details reason
     *
     * @param Builder $builder
     * @param string|array $type
     */
    function scopeWithoutOrderSlipDetailReason(Builder $builder, $type)
    {
        if (!is_array($type))
            $type = [$type];

        $type_str = "'" . implode("','", $type) . "'";

        $builder->whereRaw("EXISTS
    (SELECT count(mi_orders_slips_details.id) as cnt
     FROM mi_orders_slips_details
     WHERE mi_orders_slips_details.order_id = orders.id 
     AND mi_orders_slips_details.lgnum IN ($type_str)
     HAVING cnt = 0)");
    }

    /**
     * Selects all orders with a given order slip details type
     *
     * @param Builder $builder
     * @param string|array $type
     */
    function scopeWithOrderSlipDetailType(Builder $builder, $type)
    {
        if (!is_array($type))
            $type = [$type];

        $type_str = "'" . implode("','", $type) . "'";

        $builder->whereRaw("EXISTS
    (SELECT count(mi_orders_slips_details.id) as cnt
     FROM mi_orders_slips_details
     WHERE mi_orders_slips_details.order_id = orders.id 
     AND mi_orders_slips_details.type IN ($type_str)
     HAVING cnt > 0)");
    }

    /**
     * Selects all orders without a given order slip details type
     *
     * @param Builder $builder
     * @param string|array $type
     */
    function scopeWithoutOrderSlipDetailType(Builder $builder, $type)
    {
        if (!is_array($type))
            $type = [$type];

        $type_str = "'" . implode("','", $type) . "'";

        $builder->whereRaw("EXISTS
    (SELECT count(mi_orders_slips_details.id) as cnt
     FROM mi_orders_slips_details
     WHERE mi_orders_slips_details.order_id = orders.id 
     AND mi_orders_slips_details.type IN ($type_str)
     HAVING cnt = 0)");
    }

    /**
     * Selects all orders with a given order detail status
     *
     * @param Builder $builder
     * @param string|array $status
     */
    function scopeWithOrderDetailStatus(Builder $builder, $status)
    {
        if (!is_array($status))
            $status = [$status];

        $status_str = "'" . implode("','", $status) . "'";

        $builder->whereRaw("EXISTS
    (SELECT count(order_details.id) as cnt
     FROM order_details
     WHERE order_details.order_id = orders.id
     AND ISNULL(order_details.deleted_at) 
     AND order_details.status IN ($status_str)
     HAVING cnt > 0)");
    }

    /**
     * Selects all orders without a given order details status
     *
     * @param Builder $builder
     * @param string|array $status
     */
    function scopeWithoutOrderDetailStatus(Builder $builder, $status)
    {
        if (!is_array($status))
            $status = [$status];

        $status_str = "'" . implode("','", $status) . "'";

        $builder->whereRaw("EXISTS
    (SELECT count(order_details.id) as cnt
     FROM order_details
     WHERE order_details.order_id = orders.id
     AND ISNULL(order_details.deleted_at) 
     AND order_details.status IN ($status_str)
     HAVING cnt = 0)");
    }

    /**
     * @param null $type
     * @return array|OrderSlipDetail[]
     */
    function getOrderSlipDetails($type = null)
    {
        if (is_null($type)) {
            $type = [];
        } else {
            $type = is_array($type) ? $type : [$type];
        }
        if (!isset($this->order_slip_details)) {
            $this->load('order_slip_details');
        }
        $rows = [];
        try {
            //audit($this->order_slip_details, __METHOD__, 'DETAILS');
            $rows = $this->order_slip_details->filter(function ($obj) use ($type) {
                if (empty($type) or in_array($obj->type, $type)) {
                    return $obj;
                }
                return null;
            });
            //audit($rows, __METHOD__, 'ROWS');
        } catch (Exception $e) {
            audit($e->getMessage(), __METHOD__);
        }
        return $rows;
    }

    /**
     * @return null|string
     */
    function getShippingNumberFromOrderSlipDetails()
    {
        $order_slip_details = $this->getOrderSlipDetails(OrderSlipDetail::TYPE_SHIPPING);
        if (empty($order_slip_details))
            return null;

        foreach ($order_slip_details as $order_slip_detail) {
            if ($order_slip_detail->hasShipping())
                return $order_slip_detail->getShipping();
        }

        return null;
    }

    /**
     * @return null|string
     */
    function getCarrierShippingNumberFromOrderSlipDetails()
    {
        $order_slip_details = $this->getOrderSlipDetails([OrderSlipDetail::TYPE_CARRIER, OrderSlipDetail::TYPE_CARRIER_SHOP]);
        if (empty($order_slip_details))
            return null;

        foreach ($order_slip_details as $order_slip_detail) {
            if ($order_slip_detail->hasCarrierShipping())
                return $order_slip_detail->getCarrierShipping();
        }

        return null;
    }

    /**
     * @return float
     */
    function getSapDiscount()
    {
        return $this->fixed_discounts + $this->total_discounts + $this->total_discounts_extra;
    }




    /**
     * @param bool $debug
     * @return array
     */
    function statusCanBeMigratedToClosed($debug = false)
    {
        /**
         * An 'order' can be migrated to 'CLOSED' if:
         *
         * the current status id 'DELIVERED'
         * the carrier is 'STANDARD_DELIVERY'
         */

        $console = \Registry::console();
        $reference = $this->reference;
        $statusName = $this->statusName(false);
        $carrierName = $this->carrierName();

        $messages = [];

        $check_states = [
            OrderState::STATUS_DELIVERED,
        ];

        $check_carriers = [
            Carrier::TYPE_NATIONAL_DELIVERY,
        ];

        $resolve_state = $this->hasCurrentStatus($check_states);
        $resolve_carrier = $this->hasCarrier($check_carriers);

        if(false === $resolve_state)
            $messages[] = "Order [$reference] cannot be migrated to CLOSED, since its current status($statusName) is not legit.";

        if(false === $resolve_carrier)
            $messages[] = "Order [$reference] cannot be migrated to CLOSED, since its current carrier($carrierName) is not legit.";

        $valid = (true === $resolve_state and true === $resolve_carrier);

        if ($console) {
            foreach ($messages as $message) {
                $console->comment($message);
            }
        }

        return compact('valid', 'messages');
    }
}