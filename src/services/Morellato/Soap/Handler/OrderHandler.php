<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 15/02/2017
 * Time: 11:03
 */

namespace services\Morellato\Soap\Handler;

use Config;
use DB;
use services\Morellato\Soap\Exceptions\OrderHasNoValidProductsForSap;
use Utils;
use Exception;
use Mail;
use services\Morellato\Soap\Connection\SoapWrapper;
use services\Morellato\Soap\Service\OrderService;
use services\Morellato\Soap\Response\OrderResponse;


use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use services\Morellato\Loggers\DatabaseOrderLogger as Logger;

class OrderHandler
{

    /**
     * @var SoapWrapper
     */
    protected $soapWrapper;
    protected $console;
    protected $files;
    protected $logger;

    protected $saveFile = true;
    protected $saveMigration = true;
    protected $simulateSoapCall = false;


    function __construct(Command $console, Filesystem $files, Logger $logger, SoapWrapper $soapWrapper)
    {
        $this->console = $console;
        $this->files = $files;
        $this->logger = $logger;
        $this->soapWrapper = $soapWrapper;
    }

    public function make()
    {
        if(feats()->switchDisabled(\Core\Cfg::SERVICE_SEND_ORDER_WS1)){
            return;
        }
        if (Config::get('soap.orderFlow.production', true)) {
            $this->console->line("Fetching orders...");
            $this->fetchOrders();
        } else {
            $this->console->line("Simulating order...");
            $order_ids = [24051, 24050, 24049, 24048, 24047, 24046, 24041, 24043];
            //$order_ids = [24047,24046];
            foreach ($order_ids as $order_id) {
                //$this->sendOrder($order_id);
                //$this->sendFakeOrder($order_id);
                $this->makeXml($order_id);
            }
        }
    }

    public function forceOrder($order_id)
    {
        /** @var \Order $order */
        $order = \Order::find($order_id);
        if ($order == null) {
            throw new Exception("Could not find any order with ID: $order_id");
        }
        if ($order->hasSapIssues()) {
            $this->console->error("Order could not be submitted to SAP, since has issues");
            print_r($order->getSapIssues());
        } else {
            $this->sendOrder($order->id);
        }
    }


    public function createXml($order_id)
    {
        $order = \Order::find($order_id);
        if ($order == null) {
            throw new Exception("Could not find any order with ID: $order_id");
        }
        $this->makeXml($order_id);
    }


    private function saveXmlFile($xml, $reference)
    {
        $schema = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:sap-com:document:sap:rfc:functions" xmlns:ns2="http://www.sap.com/webas/630/soap/features/session/"><SOAP-ENV:Header><ns2:Session><enableSession>true</enableSession></ns2:Session></SOAP-ENV:Header><SOAP-ENV:Body>
        <ns1:Z_BAPI_SO_CREATEFROMDAT2 xmlns:ns1="urn:sap-com:document:sap:rfc:functions">
        {content}
        </ns1:Z_BAPI_SO_CREATEFROMDAT2>
        <ns1:BAPI_TRANSACTION_COMMIT xmlns:ns1="urn:sap-com:document:sap:rfc:functions">
      		<WAIT>X</WAIT>
    	</ns1:BAPI_TRANSACTION_COMMIT>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
XML;
        $fileContent = str_replace('{content}', $xml, $schema);

        $folder = storage_path('morellato/orders/');
        $filename = $reference . '.xml';

        $this->console->info("Soap Xml request saved as [{$folder}{$filename}]");

        $this->files->put($folder . $filename, $fileContent);
    }


    private function fetchOrders()
    {
        /** @var \Order[] $rows */
        $rows = \Order::submittableToSap()->get();

        $many = count($rows);

        $this->logger->log("Found [$many] orders to process", 'order_builder');
        $this->console->line("Found [$many] orders to process");

        foreach ($rows as $row) {
            try {
                $this->console->line("Processing order [$row->id]");
                $this->logger->log("Processing order ID [$row->id]", 'order_builder');
                if (!$row->hasSapIssues()) {
                    $this->sendOrder($row->id);
                }
            } catch (OrderHasNoValidProductsForSap $e) {
                $msg = $e->getMessage();
                audit_error($msg . " - Order: $row->id", __METHOD__);
            } catch (Exception $e) {
                $msg = $e->getMessage() . " - Order: $row->id";
                $this->logger->error($msg, 'order_builder');
                $this->console->error($msg);
                audit_exception($e, $msg);
            }
        }
    }


    private function sendOrder($order_id)
    {
        $orderService = new OrderService($order_id);

        $xml = null;
        $this->console->comment("Sending order [$order_id]");

        if (!$this->simulateSoapCall) {
            $orderService->make();
            $xml = $orderService->toXml();
            if ($this->saveFile and $xml) {
                $reference = $orderService->getOrderReference();
                $this->saveXmlFile($xml, $reference);
            }
            $this->console->comment("XML generated for order [$order_id]");
            $server = config('soap.location');
            $this->console->info("Sending order to SAP server [$server]");
            $this->soapWrapper->trace(true);
            $this->console->line("XML request for order [$order_id] successfully crafted, contacting SAP Web Service...");
            $response = $this->soapWrapper->call('Z_BAPI_SO_CREATEFROMDAT2', $xml);
        } else {
            $response = $this->getFakeResponse($orderService);
        }

        Utils::log($response, 'SOAP RESPONSE');
        $this->handleSoapResponse($orderService, $response);

        if ($this->saveMigration) {
            $this->migrateOrder($orderService);
        }
    }


    private function handleSoapResponse(OrderService $orderService, $response)
    {
        try {
            $orderResponse = new OrderResponse($orderService, $this->console, $this->files, $this->logger);
            $orderResponse->setResponse($response)->make();
            $order_id = $orderService->getOrderId();
            $this->console->info("SAP response for order [$order_id] processed");
        } catch (Exception $e) {
            switch ($e->getCode()) {
                case OrderResponse::ORDER_WARNING: //sales id is set, no delivery id
                    $this->logger->error("SAP response has no delivery id - sending warning email", 'order_builder');
                    $this->sendWarningEmail($orderService);
                    return;
                    break;

                case OrderResponse::ORDER_ERROR: //no sales id and no delivery - internal error
                    $this->logger->error("SAP response has no delivery id and sales id - sending error email", 'order_builder');
                    $this->sendErrorEmail($orderService);
                    break;

                default: //all OK

                    break;
            }
            Utils::error($e->getCode(), __METHOD__);
            Utils::error($e->getMessage(), __METHOD__);
            Utils::error($e->getTraceAsString(), __METHOD__);
            Utils::error($response, __METHOD__ . '::Response');
        }

    }


    /**
     * Save order migration data
     *
     * @param OrderService $orderService
     */
    private function migrateOrder($orderService)
    {
        $data = $orderService->getMigrationData();
        $data['created_at'] = date('Y-m-d H:i:s');
        DB::table('mi_orders')->insert($data);
    }


    private function sendFakeOrder($order_id)
    {
        $orderService = new OrderService($order_id);
        $this->sendWarningEmail($orderService);
        //$this->sendErrorEmail($orderService);
    }


    /**
     * Read a Fake Response
     *
     * @param OrderService $orderService
     * @return mixed
     */
    private function getFakeResponse(OrderService $orderService)
    {
        $file = $orderService->getOrderReference() . '.log';
        $path = storage_path('morellato/orders/response/') . $file;
        return unserialize($this->files->get($path));
    }


    /**
     * Make only the XML
     *
     * @param int $order_id
     * @return mixed
     */
    private function makeXml($order_id)
    {
        $orderService = new OrderService($order_id);

        $xml = null;

        try {
            $orderService->make();
            $xml = $orderService->toXml();

            if ($this->saveFile and $xml) {
                $reference = $orderService->getOrderReference();
                $this->saveXmlFile($xml, $reference);
            }
        } catch (Exception $e) {
            $this->console->error("Could not generate XML for order $order_id");
            $this->console->error($e->getMessage());
        }

    }


    /**
     * Send the warning email
     *
     * @param OrderService $orderService
     * @return mixed
     */
    private function sendWarningEmail($orderService)
    {
        $actuallySend = Config::get('soap.warning_emails.send', false);
        if ($actuallySend == false)
            return;

        Config::set('mail.pretend', false);

        $recipient_external = Config::get('soap.warning_emails.recipient_external');
        $recipient_internal = Config::get('soap.warning_emails.recipient_internal');

        $order = $orderService->getOrder();
        $messages = $order->getMorellatoMessages();
        $slip = $order->getMorellatoSlip();
        $reason = "L'ordine non è pervenuto con un delivery ID valido.";

        Mail::send('emails.morellato.warning', compact('order', 'messages', 'reason', 'slip'), function ($mail) use ($recipient_external, $recipient_internal) {
            $mail->from(Config::get('soap.warning_emails.from', 'info@kronoshop.com'));
            $mail->subject('Notifica di ricezione ordine con delivery mancante');
            $mail->to($recipient_external);
            $mail->cc($recipient_internal);
            $mail->bcc('f.politi@m.icoa.it');
        });
    }


    /**
     * Send the error email
     *
     * @param OrderService $orderService
     * @return mixed
     */
    private function sendErrorEmail($orderService)
    {
        $actuallySend = Config::get('soap.warning_emails.send', false);
        if ($actuallySend == false)
            return;

        Config::set('mail.pretend', false);

        $recipient_external = Config::get('soap.warning_emails.recipient_external');
        $recipient_internal = Config::get('soap.warning_emails.recipient_internal');

        $order = $orderService->getOrder();
        $messages = $order->getMorellatoMessages();
        $reason = "SAP non ha risposto con un Delivery ID e un Sales ID - errore grave!";


        Mail::send('emails.morellato.warning', compact('order', 'messages', 'reason'), function ($mail) use ($recipient_external, $recipient_internal) {
            $mail->from(Config::get('soap.warning_emails.from', 'info@kronoshop.com'));
            $mail->subject('Notifica di ricezione ordine con errore grave');
            $mail->to($recipient_external);
            $mail->cc($recipient_internal);
            $mail->bcc('f.politi@m.icoa.it');
        });
    }
}