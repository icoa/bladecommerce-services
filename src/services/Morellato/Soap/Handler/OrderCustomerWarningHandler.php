<?php

namespace services\Morellato\Soap\Handler;

use Config;
use services\Models\CustomerEmailWarning;
use Exception;
use Cfg;
use Email;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use services\Morellato\Loggers\DatabaseOrderLogger as Logger;

use Order;
use services\Traits\TraitLocalDebug;
use Carbon\Carbon;

class OrderCustomerWarningHandler extends OrderWarningHandler
{

    /**
     * @return string
     */
    function getType()
    {
        return self::TYPE_MISSED_PICKUP;
    }

    /**
     * @return string
     */
    static function getDescription()
    {
        return "Sends an email to the customer, who has not picked up yet products in the shop.";
    }

    /**
     * @return Order[]
     */
    protected function getOrders()
    {
        return Order::customerWarnableMissedPickup()->get();
    }

    /**
     * @param Order $order
     * @return bool
     */
    protected function orderCanBeWarned(Order $order)
    {
        $pretend = $this->isPretend();
        $days = \Cfg::get('CUSTOMER_WARNING_MINIMUM_DAYS', 7);
        $type = CustomerEmailWarning::TYPE_SHOP_PICKUP;
        $check = $order->hasValidTimeOffsetForCustomerWarning($type, $days);
        if ($pretend) {
            $this->console->line("=> CUSTOMER_WARNING_MINIMUM_DAYS: $days");
            //get total warnings by type
            $totals = CustomerEmailWarning::withType($type)
                ->withOrder($order->id)
                ->count();
            $this->console->line("=> TOTAL WARNING OF TYPE[{$type}]: $totals");
            ($check) ? $this->console->info("****>> ALL CONDITIONS ARE SATISFIED [OrderCustomerWarningHandler]") : $this->console->comment("****>> AT LEAST ONE CONDITION IS NOT SATISFIED [OrderCustomerWarningHandler]");
        }

        if(false === $check)
            return $check;

        //check if the latest history states is N days old
        $lastHistoryState = $order->getLastHistoryStatus();
        if($lastHistoryState){
            $created = Carbon::parse($lastHistoryState->created_at);
            $now = Carbon::now();
            $diff = $created->diffInDays($now);
            if($pretend){
                $this->console->line("=> LAST HISTORY DIFF: $diff");
            }
            if($diff < $days)
                $check = false;
        }

        if($pretend){
            ($check) ? $this->console->info("****>> ALL CONDITIONS ARE SATISFIED [OrderCustomerWarningHandler]") : $this->console->comment("****>> AT LEAST ONE CONDITION IS NOT SATISFIED [OrderCustomerWarningHandler]");
        }

        return $check;
    }

    /**
     * @param string $lang_id
     *
     * @return Email|null
     */
    protected function getEmail($lang_id)
    {
        return Email::getByCode('WARNING_SHOP_PICKUP', $lang_id);
    }

    /**
     * @return string
     */
    protected function getCustomerEmailWarningType()
    {
        return CustomerEmailWarning::TYPE_SHOP_PICKUP;
    }
}