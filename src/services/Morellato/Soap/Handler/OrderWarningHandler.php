<?php

namespace services\Morellato\Soap\Handler;

use Config;
use services\Models\CustomerEmailWarning;
use Exception;
use Cfg;
use Email;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use services\Morellato\Loggers\DatabaseOrderLogger as Logger;

use Order;
use services\Traits\TraitLocalDebug;

class OrderWarningHandler
{

    const TYPE_PACKAGE_READY = 'package';
    const TYPE_MISSED_PICKUP = 'pickup';
    const TYPE_SHOP_CARRIER = 'carrier';
    const TYPE_SHOP_CUSTOMER_CALL = 'call';

    protected $console;
    protected $files;
    protected $logger;
    protected $debug = true;
    protected $pretend = false;
    protected $queue = [];
    use TraitLocalDebug;
    protected $localDebug = false;


    /**
     * OrderCustomerWarningHandler constructor.
     * @param Command $console
     * @param Filesystem $files
     * @param Logger $logger
     */
    function __construct(Command $console, Filesystem $files, Logger $logger)
    {
        $this->console = $console;
        $this->files = $files;
        $this->logger = $logger;
        $this->debug = \App::environment() != 'live';
    }

    /**
     * @return string
     */
    static function getDescription()
    {
        return 'MISSING DESCRIPTION';
    }

    /**
     * @return string
     */
    function getType()
    {
        return self::TYPE_MISSED_PICKUP;
    }

    /**
     * @return boolean
     */
    public function isPretend()
    {
        return $this->pretend;
    }

    /**
     * @param boolean $pretend
     */
    public function setPretend($pretend)
    {
        $this->pretend = $pretend;
    }



    /**
     * @param string|null $type
     * @return bool
     */
    function isEnabled($type = null)
    {
        $type = is_null($type) ? $this->getType() : $type;
        $key = "order.warnings.{$type}";
        audit(compact('type', 'key'), __METHOD__);
        return (bool)config($key, true);
    }

    /**
     * @return OrderWarningHandler[]
     */
    static function options()
    {
        return [
            self::TYPE_PACKAGE_READY => OrderCustomerWarningPackageReadyHandler::class,
            self::TYPE_MISSED_PICKUP => OrderCustomerWarningHandler::class,
            self::TYPE_SHOP_CARRIER => OrderShopWarningCarrierShipment::class,
            self::TYPE_SHOP_CUSTOMER_CALL => OrderShopWarningCustomerHandler::class,
        ];
    }

    /**
     * @return array
     */
    static function switchForOptions()
    {
        return [
            self::TYPE_PACKAGE_READY => \Core\Cfg::SERVICE_CUSTOMER_WARNING_PACKAGE_READY,
            self::TYPE_MISSED_PICKUP => \Core\Cfg::SERVICE_CUSTOMER_WARNING_MISSED_PICKUP,
            self::TYPE_SHOP_CARRIER => \Core\Cfg::SERVICE_CUSTOMER_WARNING_SHOP_CARRIER,
            self::TYPE_SHOP_CUSTOMER_CALL => \Core\Cfg::SERVICE_CUSTOMER_WARNING_SHOP_CUSTOMER_CALL,
        ];
    }

    /**
     *
     */
    public function make()
    {
        $type = $this->getType();
        $this->console->line("Fetching order for type [$type]");
        $enabled = Cfg::get('SERVICE_CUSTOMER_WARNING');
        if ($enabled != '1') {
            $this->console->comment("Service [$type] not enabled globally - exiting now...");
            return;
        }
        $enabled = $this->isEnabled();
        if(false == $enabled){
            $this->console->comment("Service [$type] not enabled locally - exiting now...");
            return;
        }
        $this->fetchOrders();
    }

    /**
     * @param $order_id
     */
    public function single($order_id)
    {
        $order = Order::getObj($order_id);
        if ($order) {
            $response = $this->orderCanBeWarned($order);
            $this->queueOrder($order);
            $this->resolveQueue();
        } else {
            $this->console->error("Cannot find any Order with ID[$order_id]");
        }
    }

    /**
     * @return bool
     */
    protected function isLocalEnv()
    {
        return \App::environment() == 'local';
    }

    /**
     * @return Order[]
     */
    protected function getOrders()
    {
        return Order::customerWarnableMissedPickup()->get();
    }

    /**
     * @param Order $order
     * @return bool
     */
    protected function orderCanBeWarned(Order $order)
    {
        return $order->hasValidTimeOffsetForCustomerWarning(CustomerEmailWarning::TYPE_SHOP_PICKUP, \Cfg::get('CUSTOMER_WARNING_MINIMUM_DAYS', 7));
    }

    /**
     * @param string $lang_id
     *
     * @return Email|null
     */
    protected function getEmail($lang_id)
    {
        return Email::getByCode('WARNING_SHOP_PICKUP', $lang_id);
    }

    /**
     * @return string
     */
    protected function getCustomerEmailWarningType()
    {
        return CustomerEmailWarning::TYPE_SHOP_PICKUP;
    }

    /**
     *
     */
    protected function fetchOrders()
    {
        \Registry::set('console', $this->console);
        audit_watch();

        $orders = $this->getOrders();

        $many = count($orders);

        $mode = $this->debug ? 'DEMO' : 'LIVE';
        $this->console->comment("===== EXEC MODE: $mode =====");
        $this->console->comment("Found $many orders to check...");

        $counter = 0;
        foreach ($orders as $order) {
            $counter++;
            $this->console->comment("Checking order $order->reference[$order->id] ($counter/$many)");
            try {
                $response = $this->orderCanBeWarned($order);
                if ($response) {
                    $this->console->info("Order customer can be successfully warned!");
                    $this->queueOrder($order);
                } else {
                    $this->console->comment("Order customer should not be safely warned!");
                }
            } catch (Exception $e) {
                $this->console->error($e->getMessage());
                audit_exception($e, __METHOD__);
                audit_error($order, __METHOD__, 'Order');
            }
        }
        $this->resolveQueue();
    }

    /**
     * @param Order $order
     */
    protected function queueOrder(Order $order)
    {
        $this->queue[] = $order;
    }

    /**
     * @return string
     */
    protected function getRecipientKey()
    {
        return 'customer_email';
    }

    /**
     *
     */
    protected function resolveQueue()
    {
        if (empty($this->queue))
            return;

        $this->console->comment("Processing queue...");

        Config::set('mail.pretend', false);

        foreach ($this->queue as $order) {
            /* @var Order $order */

            $this->console->comment("Resolving order $order->reference[$order->id] from the queue");
            $data = $order->getTokens();

            $mail_recipient = $data[$this->getRecipientKey()];

            $recipient = (!$this->isLocalEnv()) ? $mail_recipient : 'f.politi@icoa.it';
            $store = $order->getDeliveryStore();
            $data['shop_details'] = $data['shipping_details'];
            if ($store) {
                $data['shop_details'] = $store->toHtml('template.reminders.html_email');
            }

            $data['order_details'] = "<h5><a href='{$data['customer_link']}'>{$data['order_number']} ({$data['order_date']})</a></h5><br>{$data['products_details']}";

            $this->audit($data, 'ORDER FULL DATA', __METHOD__);
            $this->console->comment("Sending email to [$mail_recipient]($recipient)");
            $email = $this->getEmail($order->lang_id);

            if($this->isPretend() === false){
                $email->send($data, $recipient);
                $order->logger()->log("Sent email {$email->code} to recipient $mail_recipient", 'EMAIL_SENT');

                //track the warning event
                $warning = new CustomerEmailWarning();
                $warning->fill([
                    'order_id' => $order->id,
                    'customer_id' => $order->customer_id,
                    'email_id' => $email->id,
                    'type' => $this->getCustomerEmailWarningType(),
                    'recipient' => $mail_recipient,
                ]);
                $warning->save();
                $this->console->info("Customer warning saved; move on...");
            }else{
                $this->console->info('Pretend mode is enabled - no real action taken');
            }

        }
    }
}