<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 15/02/2017
 * Time: 11:03
 */

namespace services\Morellato\Soap\Handler;

use Config;
use DB;
use services\Morellato\Soap\Response\OrderFlowResponse;
use services\Morellato\Soap\Service\AbstractOrderService;
use Utils;
use Exception;
use Order;
use services\Morellato\Soap\Connection\SoapWrapper;
use services\Morellato\Soap\Service\OrderFlowService;
use services\Morellato\Soap\Response\OrderResponse;
use SplFileObject;
use League\Csv\Writer;
use services\OrderCollectorConsumer;
use services\OrderCollectorService;


use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use services\Morellato\Loggers\DatabaseOrderLogger as Logger;

class OrderFlowHandler
{

    /**
     * @var SoapWrapper
     */
    protected $soapWrapper;
    protected $console;
    protected $files;
    protected $logger;

    protected $saveFile = true;
    protected $saveMigration = true;
    protected $simulateSoapCall = false;
    protected $stack = [];

    /**
     * OrderFlowHandler constructor.
     * @param Command $console
     * @param Filesystem $files
     * @param Logger $logger
     * @param SoapWrapper $soapWrapper
     */
    function __construct(Command $console, Filesystem $files, Logger $logger, SoapWrapper $soapWrapper)
    {
        $this->console = $console;
        $this->files = $files;
        $this->logger = $logger;
        $this->soapWrapper = $soapWrapper;
    }

    /**
     *
     */
    public function make()
    {
        if(feats()->switchDisabled(\Core\Cfg::SERVICE_PULL_ORDER_WS2)){
            return;
        }
        $this->fetchOrders();
        $this->executePostActions();
    }

    /**
     * @param $xml
     * @param $reference
     */
    private function saveXmlFile($xml, $reference)
    {
        $schema = <<<XML
<?xml version="1.0" encoding="UTF-8"?>
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:ns1="urn:sap-com:document:sap:rfc:functions" xmlns:ns2="http://www.sap.com/webas/630/soap/features/session/"><SOAP-ENV:Header><ns2:Session><enableSession>true</enableSession></ns2:Session></SOAP-ENV:Header><SOAP-ENV:Body>
        <ns1:Z_RV_ORDER_FLOW xmlns:ns1="urn:sap-com:document:sap:rfc:functions">
        {content}
        </ns1:Z_RV_ORDER_FLOW>
        <ns1:BAPI_TRANSACTION_COMMIT xmlns:ns1="urn:sap-com:document:sap:rfc:functions">
      		<WAIT>X</WAIT>
    	</ns1:BAPI_TRANSACTION_COMMIT>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
XML;
        $fileContent = str_replace('{content}', $xml, $schema);

        $folder = storage_path('morellato/orders/flows/');

        $filename = $reference . '.xml';

        $this->files->put($folder . $filename, $fileContent);
    }


    /**
     *
     */
    public function fetchOrders()
    {
        audit_watch();
        $rows = Order::submittableToSapOrderFlow()->get();

        $many = count($rows);

        $this->logger->log("Found [$many] orders to process", 'order_polling');
        $this->console->line("Found [$many] orders to process");

        foreach ($rows as $row) {
            try {
                $this->console->line("Processing order [$row->id]");
                $this->logger->log("Processing order ID [$row->id]", 'order_polling');
                $this->sendOrder($row->id);
            } catch (Exception $e) {
                audit_exception($e, __METHOD__);
                $msg = $e->getMessage();
                $this->logger->error($msg, 'order_polling');
            }
        }
    }

    /**
     *
     */
    public function executePostActions()
    {
        $this->console->comment("Taking actions after WS2");
        $consumer = new OrderCollectorConsumer(OrderCollectorService::getInstance());
        $consumer->takeActionsAfterOrderFlow();
    }


    /**
     * @param $order_id
     */
    public function sendOrder($order_id)
    {
        $orderService = new OrderFlowService($order_id);

        $xml = null;

        if (!$this->simulateSoapCall) {
            try {
                $orderService->make();
                if (!$orderService->has('number')){
                    $this->console->error("OrderService.Number not found, the order is not submittable to SAP WS2");
                    return;
                }

                $xml = $orderService->toXml();
                \Utils::log($xml, 'XML');
                $this->soapWrapper->trace(true);
                //$this->logger->log("XML request for order [$order_id] successfully crafted, contacting SAP Web Service...",'order_builder');
                $response = $this->soapWrapper->call('Z_RV_ORDER_FLOW', $xml);
            } catch (Exception $e) {
                $this->console->error($e->getMessage());
                audit_exception($e, __METHOD__, 'Order_id: ' . $order_id);
                return;
            }

        } else {
            $response = $this->getFakeResponse($orderService);
        }

        //Utils::log($response, 'SOAP RESPONSE');
        $this->handleSoapResponse($orderService, $response);
        if ($this->saveFile and $xml) {
            $reference = $orderService->getOrderReference();
            $this->saveXmlFile($xml, $reference);
        }
    }

    /**
     * @param OrderFlowService $orderService
     * @param $response
     */
    private function handleSoapResponse(OrderFlowService $orderService, $response)
    {
        try {
            $orderResponse = new OrderFlowResponse($orderService, $this->console, $this->files, $this->logger);
            $orderResponse->setResponse($response)->make();
            $order_id = $orderService->getOrderId();
            $this->logger->log("SAP polling response for order [$order_id] processed", 'order_polling');
        } catch (Exception $e) {
            //audit_exception($e, __METHOD__);
            //Utils::log($e->getCode(), __METHOD__);
            Utils::log($e->getMessage(), __METHOD__);
            $order_id = $orderService->getOrderId();
            $this->logger->error("SAP polling response for order [$order_id] got an error: " . $e->getMessage(), 'order_polling');
        }
    }


    /**
     * Read a Fake Response
     *
     * @param OrderFlowService $orderService
     * @return mixed
     */
    private function getFakeResponse(OrderFlowService $orderService)
    {
        $file = $orderService->getOrderReference() . '.log';
        $path = storage_path('morellato/orders/flows/response/') . $file;
        return unserialize($this->files->get($path));
    }


    /**
     * Make only the XML
     *
     * @param int $order_id
     * @return mixed
     */
    private function makeXml($order_id)
    {
        $orderService = new OrderFlowService($order_id);

        $xml = null;

        $orderService->make();
        $xml = $orderService->toXml();

        if ($this->saveFile and $xml) {
            $reference = $orderService->getOrderReference();
            $this->saveXmlFile($xml, $reference);
        }
    }


    public function testResponse($reference)
    {
        $file = $reference . '.log';
        $path = storage_path('morellato/orders/flows/response/') . $file;
        $data = unserialize($this->files->get($path));
        //audit($data, __METHOD__);
        $this->stack[$reference] = OrderFlowResponse::getItems($data);
    }

    public function testExportCsv()
    {
        //audit($this->stack, 'STACK');
        $headers = null;
        $rows = [];
        foreach ($this->stack as $reference => $items) {
            foreach ($items as $item) {
                $item = (array)$item;
                if (is_null($headers)) {
                    $headers = array_keys($item);
                    $headers = [array_merge(['ORDER'], $headers)];
                }
                $row = array_values($item);
                $row = array_merge([$reference], $row);
                $rows[] = $row;
            }
        }

        $rows = array_merge($headers, $rows);

        //audit($rows);

        $targetFilePath = storage_path('WebService2.csv');

        $writer = Writer::createFromPath(new SplFileObject($targetFilePath, 'w'), 'w');
        $writer->setEncodingFrom('UTF-8');
        $writer->setDelimiter(';');
        $writer->insertAll($rows);
        $this->console->info("CSV exported to $targetFilePath");
    }

    /**
     * @return boolean
     */
    public function isSimulateSoapCall()
    {
        return $this->simulateSoapCall;
    }

    /**
     * @param boolean $simulateSoapCall
     */
    public function setSimulateSoapCall($simulateSoapCall)
    {
        $this->simulateSoapCall = $simulateSoapCall;
    }


}