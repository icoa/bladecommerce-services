<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 15/02/2017
 * Time: 11:03
 */

namespace services\Morellato\Soap\Handler;

use Config;
use DB;
use services\Models\OrderTracking;
use Utils;
use Exception;
use Mail;
use Cfg;


use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use services\Morellato\Loggers\DatabaseOrderLogger as Logger;

use Order;
use OrderState;

class OrderStatusHandler
{

    protected $console;
    protected $files;
    protected $logger;
    protected $debug = false;
    protected $pretend = false;
    protected $queue = [];


    function __construct(Command $console, Filesystem $files, Logger $logger)
    {
        $this->console = $console;
        $this->files = $files;
        $this->logger = $logger;
        $this->debug = config('soap.status_handler_mode', 'demo') != 'live';
    }

    /**
     * @return bool
     */
    public function isDebug()
    {
        return $this->debug;
    }

    /**
     * @param bool $debug
     * @return OrderStatusHandler
     */
    public function setDebug($debug)
    {
        $this->debug = $debug;
        return $this;
    }

    /**
     * @return bool
     */
    public function isPretend()
    {
        return $this->pretend;
    }

    /**
     * @param bool $pretend
     * @return OrderStatusHandler
     */
    public function setPretend($pretend)
    {
        $this->pretend = $pretend;
        if ($this->pretend)
            $this->setDebug(true);

        return $this;
    }


    public function make($order_id = null)
    {
        \Registry::set('console', $this->console);
        $this->checkOrdersWithMissingShop();
        usleep(500);
        if(feats()->switchEnabled(\Core\Cfg::SERVICE_ORDER_STATUS_WORKING)){
            $this->fetchMigrationableToWorkingOrders($order_id);
        }
        if(feats()->switchEnabled(\Core\Cfg::SERVICE_ORDER_STATUS_CLOSED)){
            $this->fetchMigrationableToClosedOrders($order_id);
        }
        $this->resolveQueue();
    }


    private function fetchMigrationableToClosedOrders($order_id = null)
    {

        //audit_watch();
        /* @var $orders Order[] */
        $orders = $order_id > 0 ? Order::whereIn('id', [$order_id])->get() : Order::migrationableToClosed()->orderBy('id', 'desc')->get();

        $many = count($orders);

        $mode = $this->debug ? 'DEMO' : 'LIVE';
        $this->console->comment("===== fetchMigrationableToClosedOrders EXEC MODE: $mode =====");
        $this->console->comment("Found $many orders to process...");

        $counter = 0;
        foreach ($orders as $order) {
            $counter++;
            $this->console->comment("Switching status for order $order->reference[$order->id] ($counter/$many)");
            try {
                $response = $order->statusCanBeMigratedToClosed($this->debug);
                if ($response['valid']) {
                    $this->console->info("Order can be successfully migrated to CLOSED!");
                    if ($this->debug == false) {
                        $order->setStatus(
                            OrderState::STATUS_CLOSED,
                            true,
                            'Migrazione automatica per chiusura ordine',
                            OrderTracking::byInternal());
                    }
                } else {
                    $this->console->comment("Order cannot be safely migrated to CLOSED!");
                }
                $this->queueOrder($order, $response);
            } catch (Exception $e) {
                $this->console->error($e->getMessage());
                audit_exception($e, __METHOD__);
                audit_error($order, __METHOD__, 'Order');
            }
        }
    }


    private function fetchMigrationableToWorkingOrders($order_id = null)
    {
        //audit_watch();
        /* @var $orders Order[] */
        $orders = $order_id > 0 ? Order::whereIn('id', [$order_id])->get() : Order::migrationableToWorking()->orderBy('id', 'desc')->get();

        $many = count($orders);

        $mode = $this->debug ? 'DEMO' : 'LIVE';
        $this->console->comment("===== fetchMigrationableToWorkingOrders EXEC MODE: $mode =====");
        $this->console->comment("Found $many orders to process...");

        $counter = 0;
        foreach ($orders as $order) {
            $counter++;
            $this->console->comment("Switching status for order $order->reference[$order->id] ($counter/$many)");
            try {
                $response = $order->statusCanBeMigratedToWorking($this->debug);
                if ($response['valid']) {
                    $this->console->info('Order can be successfully migrated to WORKING!');
                    if ($this->debug == false) {
                        $order->setStatus(
                            OrderState::STATUS_WORKING,
                            true,
                            'Migrazione automatica per messa in lavorazione ordine',
                            OrderTracking::byInternal());
                    }
                } else {
                    $this->console->comment('Order cannot be safely migrated to WORKING!');
                }
                $this->queueOrder($order, $response);
            } catch (Exception $e) {
                $this->console->error($e->getMessage());
                audit_exception($e, __METHOD__);
                audit_error($order, __METHOD__, 'Order');
            }
        }
    }

    /**
     * @param Order $order
     * @param array $response
     */
    private function queueOrder(Order $order, array $response)
    {
        $order->response = $response;
        $this->queue[] = $order;
    }

    /**
     *
     */
    private function resolveQueue()
    {
        if (empty($this->queue))
            return;

        if (true === $this->pretend)
            return;

        $this->console->comment("Processing queue...");
        $rows = [];

        foreach ($this->queue as $order) {
            /* @var Order $order */
            $id = $order->id;
            $reference = $order->reference;
            $carrier = $order->carrierName();
            $payment = $order->payment()->name;
            $status = $order->statusName();
            $payment_status = $order->paymentStatusName();
            $url = $order->getBackendLinkAttribute();
            $response = $order->response;
            $rows[] = (object)compact('id', 'reference', 'carrier', 'payment', 'status', 'payment_status', 'url', 'response');
        }

        Config::set('mail.pretend', false);

        $recipient_external = Config::get('soap.order_status_emails.recipient_external');
        $recipient_internal = Config::get('soap.order_status_emails.recipient_internal');

        $this->console->comment("Sending email...");

        Mail::send('emails.morellato.orderstatus', compact('rows'), function ($mail) use ($recipient_external, $recipient_internal) {
            $mail->from(\Cfg::get('MAIL_GENERAL_ADDRESS'));
            $mail->subject("Blade report: ordini automatici 'In lavorazione'");
            $mail->to($recipient_external);
            $mail->cc($recipient_internal);
            $mail->bcc('f.politi@icoa.it');
        });
    }

    private function setAsWorking($order_id)
    {
        $order = Order::getObj($order_id);
        $order->setStatus(
            OrderState::STATUS_WORKING,
            true,
            'Migrazione automatica',
            OrderTracking::byInternal()
        );
        $this->console->info("Order [$order_id] setted to WORKING");
        $this->logger->log("Order ID [$order_id] automatically setted to WORKING", 'status_builder');
    }

    protected function checkOrdersWithMissingShop()
    {
        /** @var Order[] $orders */
        $orders = Order::migrationableToWorking()->orderBy('id', 'desc')->get();

        $many = count($orders);

        $this->console->line("Found $many orders");

        foreach ($orders as $order) {

            try{
                $this->console->line("Order $order->id - $order->reference");

                $products = $order->getProducts();

                $order_availability_mode = $order->availability_mode;
                $order_availability_shop = (string)$order->availability_shop_id;

                $checksums = [
                    Order::MODE_LOCAL => 0,
                    Order::MODE_SHOP => 0,
                    Order::MODE_ONLINE => 0
                ];
                $checksums_keys = array_keys($checksums);
                $shops = [];

                foreach ($products as $product) {
                    $availability_mode = $product->availability_mode;
                    $availability_shop_id = (string)$product->availability_shop_id;
                    $warehouse = $product->warehouse;

                    if (in_array($availability_mode, $checksums_keys, true)) {
                        $checksums[$availability_mode]++;
                    }
                    if ($availability_mode === Order::MODE_STAR) {
                        foreach ($checksums as $key => $value) {
                            $checksums[$key]++;
                        }
                    }
                    if ($availability_mode === Order::MODE_SHOP and $availability_shop_id !== '') {
                        $pivot = (int)array_get($shops, $availability_shop_id, 0);
                        $pivot++;
                        array_set($shops, $availability_shop_id, $pivot);
                    }
                    if ($availability_mode === Order::MODE_SHOP and $availability_shop_id === '') {
                        array_set($shops, 'XXX', 1);
                    }
                }

                $total = count($products);
                $this->console->comment('Order_availability_mode: ' . $order_availability_mode . ' - Order_availability_shop: ' . $order_availability_shop);
                $this->console->line('Products: ' . $total);
                //print_r($checksums);
                //print_r($shops);
                //decision making
                if ($order_availability_mode === Order::MODE_SHOP and $order_availability_shop === '') {
                    if ($total === $checksums[Order::MODE_SHOP] and count($shops) === 1) {
                        $given_shop = array_keys($shops)[0];
                        if($given_shop !== 'XXX'){
                            $this->console->info('***** THIS ORDER SHOULD BE SET TO => ' . $given_shop);
                            DB::table('orders')->where('id', $order->id)->update(['availability_shop_id' => $given_shop]);
                        }
                    }
                }
            }catch (\Exception $e){
                audit_exception($e);
                $this->console->error($e->getMessage());
            }
        }
    }
}