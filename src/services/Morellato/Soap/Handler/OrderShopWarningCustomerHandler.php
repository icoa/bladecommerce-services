<?php

namespace services\Morellato\Soap\Handler;

use Config;
use services\Models\CustomerEmailWarning;
use Exception;
use Cfg;
use Email;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use services\Morellato\Loggers\DatabaseOrderLogger as Logger;

use Order;
use services\Traits\TraitLocalDebug;

class OrderShopWarningCustomerHandler extends OrderWarningHandler
{

    /**
     * @return string
     */
    function getType()
    {
        return self::TYPE_SHOP_CUSTOMER_CALL;
    }

    /**
     * @return string
     */
    static function getDescription(){
        return "Sends an email to the shop, X days after the customer warning has been sent, in order to invite the shop to call the customer.";
    }

    /**
     * @return Order[]
     */
    protected function getOrders()
    {
        return Order::customerWarnableMissedPickup()->get();
    }

    /**
     * @param Order $order
     * @return bool
     */
    protected function orderCanBeWarned(Order $order)
    {
        $pretend = $this->isPretend();
        $hasMinimumDaysGap = $order->hasValidTimeOffsetForCustomerWarning(CustomerEmailWarning::TYPE_SHOP_PICKUP, \Cfg::get('CUSTOMER_WARNING_MINIMUM_DAYS', 7));
        $hasNoPreviousWarning = $order->hasExactNumberOfWarnings($this->getCustomerEmailWarningType(), 0);
        $hasMinimumShopPickupWarnings = $order->hasNumberOfWarnings(CustomerEmailWarning::TYPE_SHOP_PICKUP, 2);
        $test = (true == $hasMinimumDaysGap and true == $hasNoPreviousWarning and true == $hasMinimumShopPickupWarnings);

        if($pretend){
            ($hasMinimumDaysGap) ? $this->console->info("=> Order hasMinimumDaysGap of type SHOP_PICKUP") : $this->console->error("=> Order failed at hasMinimumDaysGap of type SHOP_PICKUP");
            ($hasNoPreviousWarning) ? $this->console->info("=> Order hasNoPreviousWarning of type SHOP_CUSTOMER_CALL") : $this->console->error("=> Order failed at hasNoPreviousWarning of type SHOP_CUSTOMER_CALL");
            ($hasMinimumShopPickupWarnings) ? $this->console->info("=> Order hasMinimumShopPickupWarnings of type SHOP_PICKUP") : $this->console->error("=> Order failed at hasMinimumShopPickupWarnings of type SHOP_PICKUP");
            ($test) ? $this->console->info("****>> ALL CONDITIONS ARE SATISFIED [OrderShopWarningCustomerHandler]") : $this->console->comment("****>> AT LEAST ONE CONDITION IS NOT SATISFIED [OrderShopWarningCustomerHandler]");
        }

        return $test;
    }

    /**
     * @param string $lang_id
     *
     * @return Email|null
     */
    protected function getEmail($lang_id)
    {
        return Email::getByCode('WARNING_SHOP_CLERKS', $lang_id);
    }

    /**
     * @return string
     */
    protected function getCustomerEmailWarningType()
    {
        return CustomerEmailWarning::TYPE_SHOP_CUSTOMER_CALL;
    }

    /**
     * @return string
     */
    protected function getRecipientKey()
    {
        return 'store_email';
    }
}