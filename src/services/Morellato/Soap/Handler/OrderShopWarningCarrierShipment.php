<?php

namespace services\Morellato\Soap\Handler;

use Config;
use services\Models\CustomerEmailWarning;
use Exception;
use Cfg;
use Email;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use services\Morellato\Loggers\DatabaseOrderLogger as Logger;

use Order;
use services\Traits\TraitLocalDebug;

class OrderShopWarningCarrierShipment extends OrderWarningHandler
{

    /**
     * @return string
     */
    function getType()
    {
        return self::TYPE_SHOP_CARRIER;
    }

    /**
     * @return string
     */
    static function getDescription(){
        return "Sends an email to the shop when a carrier is about to deliver some products in the shop.";
    }

    /**
     * @return Order[]
     */
    protected function getOrders()
    {
        return Order::shopWarnableCarrierShipment()->get();
    }

    /**
     * @param Order $order
     * @return bool
     */
    protected function orderCanBeWarned(Order $order)
    {
        $pretend = $this->isPretend();
        $test = $order->hasExactNumberOfWarnings(CustomerEmailWarning::TYPE_SHOP_TARGET_SHIPMENT, 0);
        if ($pretend) {
            if ($test) {
                $this->console->info("=> Order has never had email warnings with type = SHOP_TARGET_SHIPMENT, the new Warning can be sent");
            } else {
                $this->console->comment("=> Order has already had email warnings with type = SHOP_TARGET_SHIPMENT, the new Warning cannot be sent");
            }
            ($test) ? $this->console->info("****>> ALL CONDITIONS ARE SATISFIED [OrderShopWarningCarrierShipment]") : $this->console->comment("****>> AT LEAST ONE CONDITION IS NOT SATISFIED [OrderShopWarningCarrierShipment]");
        }
        return $test;
    }

    /**
     * @param string $lang_id
     *
     * @return Email|null
     */
    protected function getEmail($lang_id)
    {
        return Email::getByCode('WARNING_GLS_CLERKS', $lang_id);
    }

    /**
     * @return string
     */
    protected function getCustomerEmailWarningType()
    {
        return CustomerEmailWarning::TYPE_SHOP_TARGET_SHIPMENT;
    }

    /**
     * @return string
     */
    protected function getRecipientKey()
    {
        return 'store_email';
    }
}