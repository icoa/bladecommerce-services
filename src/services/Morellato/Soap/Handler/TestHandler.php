<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 15/02/2017
 * Time: 11:03
 */

namespace services\Morellato\Soap\Handler;

use Artisaninweb\SoapWrapper\SoapWrapper;
use services\Morellato\Soap\Request\GetConversionAmount;
use services\Morellato\Soap\Response\GetConversionAmountResponse;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use services\Morellato\Loggers\FileLogger as Logger;

class TestHandler
{
    /**
     * @var SoapWrapper
     */
    protected $soapWrapper;
    protected $console;
    protected $files;
    protected $logger;


    function __construct(Command $console, Filesystem $files, Logger $logger, SoapWrapper $soapWrapper)
    {
        $this->console = $console;
        $this->files = $files;
        $this->logger = $logger;
        $this->soapWrapper = $soapWrapper;
    }

    /**
     * Use the SoapWrapper
     */
    public function make()
    {
        $this->soapWrapper->add('Currency', function ($service) {
            $service
                ->wsdl('http://currencyconverter.kowabunga.net/converter.asmx?WSDL')
                ->trace(true)
                ->classmap([
                    GetConversionAmount::class,
                    GetConversionAmountResponse::class,
                ]);
        });

        // Without classmap
        $response = $this->soapWrapper->call('Currency.GetConversionAmount', [
            'CurrencyFrom' => 'USD',
            'CurrencyTo'   => 'EUR',
            'RateDate'     => date('Y-m-d'),
            'Amount'       => '1000',
        ]);

        var_dump($response);

        // With classmap
        $response = $this->soapWrapper->call('Currency.GetConversionAmount', [
            new GetConversionAmount('USD', 'EUR', date('Y-m-d'), '1000')
        ]);

        var_dump($response);
    }
}