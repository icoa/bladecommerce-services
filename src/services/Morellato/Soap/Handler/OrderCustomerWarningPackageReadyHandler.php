<?php

namespace services\Morellato\Soap\Handler;

use Config;
use services\Models\CustomerEmailWarning;
use Exception;
use Cfg;
use Email;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use services\Morellato\Loggers\DatabaseOrderLogger as Logger;

use Order;
use services\Traits\TraitLocalDebug;

class OrderCustomerWarningPackageReadyHandler extends OrderWarningHandler
{

    /**
     * @return string
     */
    function getType()
    {
        return self::TYPE_PACKAGE_READY;
    }

    /**
     * @return string
     */
    static function getDescription()
    {
        return "Sends an email to the customer when the package is ready to be picked up in the shop.";
    }

    /**
     * @return Order[]
     */
    protected function getOrders()
    {
        return Order::customerWarnablePackageReady()->get();
    }

    /**
     * @param Order $order
     * @return bool
     */
    protected function orderCanBeWarned(Order $order)
    {
        $pretend = $this->isPretend();
        $test = $order->hasExactNumberOfWarnings(CustomerEmailWarning::TYPE_PACKAGE_READY, 0);
        if ($pretend) {
            if ($test) {
                $this->console->info("=> Order has never had email warnings with type = PACKAGE_READY, the new Warning can be sent");
            } else {
                $this->console->comment("=> Order has already had email warnings with type = PACKAGE_READY, the new Warning cannot be sent");
            }
            ($test) ? $this->console->info("****>> ALL CONDITIONS ARE SATISFIED [OrderCustomerWarningPackageReadyHandler]") : $this->console->comment("****>> AT LEAST ONE CONDITION IS NOT SATISFIED [OrderCustomerWarningPackageReadyHandler]");
        }
        return $test;
    }

    /**
     * @param string $lang_id
     *
     * @return Email|null
     */
    protected function getEmail($lang_id)
    {
        return Email::getByCode('WARNING_SHOP_PACKAGE_READY', $lang_id);
    }

    /**
     * @return string
     */
    protected function getCustomerEmailWarningType()
    {
        return CustomerEmailWarning::TYPE_PACKAGE_READY;
    }
}