<?php
namespace services\Morellato\Soap\Exceptions;

use Exception;

class OrderHasNoValidProductsForSap extends Exception
{
    protected $message = 'Order has no valid products for SAP';
}