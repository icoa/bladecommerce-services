<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 01/03/2017
 * Time: 15:47
 */

namespace services\Morellato\Soap\Service;

use Order;
use View;

class AbstractOrderService
{
    /**
     * @var Order
     */
    protected $order;
    protected $products;
    protected $root;
    protected $xml;
    protected $xml_template;

    protected $hasDeliveryStore = false;
    protected $hasStorePayment = false;
    protected $hasShopAvailability = false;
    protected $hasDifferentStoreForWithdrawal = false;
    protected $hasSameStoreForWithdrawal = false;
    protected $isB2B = false;
    protected $isStrapB2B = false;
    /**
     * @var string
     */
    protected $scenary;

    function __construct($id, Order $order = null)
    {
        $this->order = is_null($order) ? Order::find($id) : $order;
        //audit("Building service for order $id ({$this->order->reference})");
        $products = $this->order->getProducts();
        foreach ($products as $key => $product) {
            //remove all non-submittable products
            if (!$product->isSubmittableToSap()) {
                unset($products[$key]);
            }
        }
        if (count($products) === 0) {
            audit("The order [{$this->order->reference}] has no products of type MORELLATO");
            return;
        }
        $this->products = $products;

        $this->root = [];

        $this->hasDeliveryStore = $this->getOrder()->hasDeliveryStore(); //customer want to pick up the order at a shop
        $this->hasStorePayment = $this->getOrder()->hasStorePayment(); //customer want to pay the order at a shop
        $this->hasShopAvailability = $this->getOrder()->hasShopAvailability();  //the order availability is in the shop
        $this->hasDifferentStoreForWithdrawal = $this->getOrder()->hasDifferentStoreForWithdrawal(); //the shop for pick up is different from the availability shop
        $this->hasSameStoreForWithdrawal = $this->getOrder()->hasSameStoreForWithdrawal(); //the shop for pick up matches the availability shop
        $this->isB2B = $this->getOrder()->isB2B(); //the workflow is B2B
        $this->isStrapB2B = $this->getOrder()->isStrapB2B(); //the workflow is B2B (Strap version)

        //if the payment method is 'postpone' treat the order as a standard 'store' workflow
        if ($this->getOrder()->hasStorePostponePayment()) {
            $this->isB2B = false;
        }

        if(\App::environment() !== 'live' and \App::runningInConsole()){
            $debug_keys = ['hasDeliveryStore', 'hasStorePayment', 'hasShopAvailability', 'hasDifferentStoreForWithdrawal', 'hasSameStoreForWithdrawal', 'isB2B', 'isStrapB2B'];
            foreach($debug_keys as $debug_key){
                audit($this->$debug_key, "Detecting [$debug_key]");
            }
        }

        $this->detectScenary();
        /*
        \Utils::log("__DETECTING_SCENARY__ => {$this->order->reference} | $this->scenary");
        \Utils::log("DETECTING (hasDeliveryStore) => " . ($this->hasDeliveryStore ? 'true' : 'false'));
        \Utils::log("DETECTING (hasStorePayment) => " . ($this->hasStorePayment ? 'true' : 'false'));
        \Utils::log("DETECTING (hasShopAvailability) => " . ($this->hasShopAvailability ? 'true' : 'false'));
        \Utils::log("DETECTING (hasDifferentStoreForWithdrawal) => " . ($this->hasDifferentStoreForWithdrawal ? 'true' : 'false'));
        \Utils::log("DETECTING (hasSameStoreForWithdrawal) => " . ($this->hasSameStoreForWithdrawal ? 'true' : 'false'));
        \Utils::log("DETECTING (isB2B) => " . ($this->isB2B ? 'true' : 'false'));
        \Utils::log("DETECTING (isStrapB2B) => " . ($this->isStrapB2B ? 'true' : 'false'));
        if (\App::runningInConsole()) {
            print("__DETECTING_SCENARY__ => {$this->order->reference} | $this->scenary") . PHP_EOL;
            print("DETECTING (hasDeliveryStore) => " . ($this->hasDeliveryStore ? 'true' : 'false')) . PHP_EOL;
            print("DETECTING (hasStorePayment) => " . ($this->hasStorePayment ? 'true' : 'false')) . PHP_EOL;
            print("DETECTING (hasShopAvailability) => " . ($this->hasShopAvailability ? 'true' : 'false')) . PHP_EOL;
            print("DETECTING (hasDifferentStoreForWithdrawal) => " . ($this->hasDifferentStoreForWithdrawal ? 'true' : 'false')) . PHP_EOL;
            print("DETECTING (hasSameStoreForWithdrawal) => " . ($this->hasSameStoreForWithdrawal ? 'true' : 'false')) . PHP_EOL;
            print("DETECTING (isB2B) => " . ($this->isB2B ? 'true' : 'false')) . PHP_EOL;
            print("DETECTING (isStrapB2B) => " . ($this->isStrapB2B ? 'true' : 'false')) . PHP_EOL;
        }*/
        //\Utils::log($this);
    }

    private function detectScenary()
    {
        //detect scenary
        /**
         * Available scenaries (default)
         * B  = payment is online; product is available in 01; shipment via carrier
         * C  = payment is online; product is available in 03; shipment via carrier
         * D  = payment is online; product is available in NG-W3; shipment via carrier
         * F  = payment is online; product is available in WM; shipment via carrier
         *
         * Available scenaries (omnichannel)
         * P  = payment is online; product is available in 01-WM; shipment in store
         * R  = payment is online; product is available in 03; shipment in store         *
         * S  = payment is online; product is available in NG-W3; shipment in store (stock shop == withdraw shop)
         * S2 = payment is online; product is available in NG-W3; shipment in store (stock shop != withdraw shop)
         *
         * Z  = payment is in shop; product is available in 01-WM; shipment in store
         * Y  = payment is in shop; product is available in 03; shipment in store
         * X  = payment is in shop; product is available in NG-W3; shipment in store (stock shop == withdraw shop)
         * X2 = payment is in shop; product is available in NG-W3; shipment in store (stock shop != withdraw shop)
         *
         * Available scenaries (B2B)
         * H1 = payment is anticipated; product is available in 01-WM; shipping via carrier
         * H2 = payment is anticipated; product is available in 03; shipping via carrier
         * H3 = payment is anticipated; product is available in NG-W3; shipping via carrier
         * H4 = payment is anticipated; product is available in 01-WM; shipping in store
         * H5 = payment is anticipated; product is available in 03; shipping in store
         * H6 = payment is anticipated; product is available in NG-W3; shipping in store
         */

        //Default scenaries
        $this->bindScenaryException('B', 'C', 'D', 'D', 'F'); //B-C-D-F

        //Omnichannel scenaries
        if ($this->hasDeliveryStore and $this->isB2B == false) {
            if ($this->hasStorePayment == false) { // P-R-S-S2
                $this->bindScenaryException('P', 'R', 'S', 'S2');
            } else { // X-X2-Y-Z
                $this->bindScenaryException('Z', 'Y', 'X', 'X2');
            }
        }

        //B2B scenaries
        if ($this->isB2B) {
            if ($this->hasDeliveryStore) { //H4-H5-H6
                $this->bindScenaryException('H4', 'H5', 'H6');
            } else { //H1-H2-H3
                $this->bindScenaryException('H1', 'H2', 'H3');
            }
        }
    }

    /**
     * @param $availableInMainWarehouse
     * @param $availableInAltWarehouse
     * @param $availableInShop
     * @param null $availableInDifferentShop
     * @param null $availableInOutlet
     */
    private function bindScenaryException($availableInMainWarehouse, $availableInAltWarehouse, $availableInShop, $availableInDifferentShop = null, $availableInOutlet = null)
    {
        $warehouse = $this->order->warehouse;
        if ($this->hasShopAvailability)
            $warehouse = 'NG';

        if ($this->order->availability_mode == 'online' and in_array($warehouse, ['NG', 'W3'])) {
            $warehouse = null;
        }

        switch ($warehouse) {
            case 'NG':
            case 'W3':
                $this->scenary = $availableInShop;
                if ($availableInDifferentShop and $this->hasDifferentStoreForWithdrawal) {
                    $this->scenary = $availableInDifferentShop;
                }
                break;
            case '03':
                $this->scenary = $availableInAltWarehouse;
                break;
            case 'WM':
                $this->scenary = $availableInMainWarehouse;
                if ($availableInOutlet) {
                    $this->scenary = $availableInOutlet;
                }
                break;
            default:
                $this->scenary = $availableInMainWarehouse;
                break;
        }
    }

    /**
     * @return string
     */
    function getScenary()
    {
        return $this->scenary;
    }

    function getOrderReference()
    {
        return $this->order->reference;
    }

    function getOrderAvailabilityMode()
    {
        return $this->order->availability_mode;
    }

    function getOrderAvailabilityShop()
    {
        return $this->order->availability_shop_id;
    }

    function getOrderId()
    {
        return $this->order->id;
    }

    /**
     * @return \Order
     */
    function getOrder()
    {
        return $this->order;
    }

    /**
     * @return mixed
     */
    function toXml()
    {
        $content = View::make($this->xml_template, $this->root)->render();
        $this->xml = $content;
        return $content;
    }

    /**
     * @param $value
     * @return string
     */
    protected function money($value)
    {
        $value = $value / 10;
        if ($value <= 0) {
            $value = 0.1;
        }
        return number_format($value, 3, '.', '');
    }

    /**
     * @param $value
     * @return string
     */
    protected function quantity($value)
    {
        return number_format($value, 1, '.', '');
    }

    /**
     * @param $value
     * @return string
     */
    protected function line($value)
    {
        return str_pad($value, 4, '0', STR_PAD_LEFT);
    }

    /**
     * @param $value
     * @return string
     */
    protected function itemNumber($value)
    {
        return str_pad($value, 6, '0', STR_PAD_LEFT);
    }

    /**
     * @param $value
     * @return string
     */
    protected function addressLink($value)
    {
        return str_pad($value, 10, '0', STR_PAD_LEFT);
    }

    /**
     * @param $key
     * @return mixed
     */
    public function has($key)
    {
        return array_get($this->root, $key);
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    public function get($key, $default = null)
    {
        return array_get($this->root, $key, $default);
    }
}