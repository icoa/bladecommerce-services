<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 15/02/2017
 * Time: 13:26
 */

namespace services\Morellato\Soap\Service;

use Order;
use Currency;
use Utils;
use Config;
use View;
use Illuminate\Support\Str;
use services\Morellato\Soap\Exceptions\OrderHasNoValidProductsForSap;

class OrderService extends AbstractOrderService
{
    /**
     * @var Order
     */
    protected $order;

    protected $products;
    protected $root;
    protected $xml;
    protected $common = [];
    protected $xml_template = 'xml.morellato.order';
    protected $doc_type;

    function __construct($id, Order $order = null)
    {
        parent::__construct($id, $order);
        $this->setCommonData();
    }


    function make()
    {
        if ($this->products == null or (count($this->products) == 0)) {
            throw new OrderHasNoValidProductsForSap("Order has no valid products for SAP");
        }
        $this->_orderConditionsIn();
        $this->_orderText();
        $this->_orderHeaderIn();
        $this->_orderItemsIn();
        $this->_orderPartners();
        $this->_orderSchedulesIn();
        $this->_orderSchedulesInx();
        $this->_partnerAddresses();
        $this->root['date'] = date('Y-m-d H:i:s');
        $this->root['scenary'] = $this->getScenary();
        $this->root['order_reference'] = $this->getOrder()->reference;
        $this->root['order_id'] = $this->getOrder()->id;
        //Utils::log($this->root, __CLASS__);
    }


    private function setCommonData()
    {
        $order = $this->order;
        $this->common['LANGU_ISO'] = Str::upper($order->lang_id);
        $this->common['LANGU'] = $this->common['LANGU_ISO'][0];
        $this->common['FORMAT_COL'] = '*';
        $this->common['CURRENCY'] = 'EUR';
    }

    private function getCommon($key, $default = null)
    {
        $key = Str::upper($key);
        return isset($this->common[$key]) ? $this->common[$key] : $default;
    }

    private function _orderConditionsIn()
    {
        $schema = [];
        /*
         * Requested vars:
         * ITM_NUMBER	progressivo (char 6, di 10 in 10)		000100
         * COND_TYPE	ZPVF (prezzo manuale)		ZPVF
         * COND_TYPE	ZTVF (Importo spesa trasporto)		ZTVF
         * COND_VALUE	 		2.900 (rappresenta 29,00 €)
         * */

        $counter = 100;

        $condType = 'ZTVF';
        //in the workflow is B2B Straps, than the COND_TYPE value is different
        if ($this->isStrapB2B) {
            $condType = null;
        }

        //first row: spesa trasporto + costi pagamento
        if($condType != null){
            $costs = $this->order->total_shipping + $this->order->total_payment;
            if ($costs > 0) {
                $item = new \stdClass();
                $condValue = $this->money($costs);
                $item->ITM_NUMBER = $this->itemNumber($counter);
                $item->COND_TYPE = $condType;
                $item->COND_VALUE = $condValue;

                $schema[] = $item;
            }
        }

        //discount row (fixed amount)
        $discount = $this->order->getSapDiscount();
        if($discount > 0){
            $item = new \stdClass();
            $condValue = $this->money($discount);
            $item->ITM_NUMBER = $this->itemNumber(0);
            $item->COND_TYPE = 'ZSAX';
            $item->COND_VALUE = $condValue;

            $schema[] = $item;
        }

        $condType = 'ZPVF';
        //in the workflow is B2B Straps, than the COND_TYPE value is different
        if ($this->isStrapB2B) {
            $condType = 'ZPRS';
        }
        //other row: products
        foreach ($this->products as $product) {
            $item = new \stdClass();
            $price = ($product->price_tax_incl > 0) ? $product->price_tax_incl : $product->cart_price_tax_incl;
            //handle free products
            if($price <= 0.2){
                $price = $product->product_sell_price_wt;
            }
            $condValue = $this->money($price);
            $item->ITM_NUMBER = $this->itemNumber($counter);
            $item->COND_TYPE = $condType;
            $item->COND_VALUE = $condValue;
            $schema[] = $item;
            $counter += 100;
        }

        $this->root['orderConditionsIn'] = $schema;
    }


    private function _orderText()
    {
        $schema = [];
        /*
         * Requested vars:
         * ITM_NUMBER	progressivo (char 6, di 10 in 10)		000100
         * TEXT_ID	identificativo testo (Z010)		Z010
            LANGU	lingua		I
            LANGU_ISO	lingua 		IT
            FORMAT_COL	formato

        Z006 = NOTE DI CONFEZIONAMENTO(per il personale che preleva il materiale)
        Z009 = note che vengono stampate nel documento di trasporto
        Z010 = note stampate su Fattura cliente finale
        Z013 = NOTE SU SEGNACOLLO(Etichetta pacco)
         * */

        $extraOrderNode = false;
        switch ($this->getScenary()) {
            case 'P':
            case 'R':
            case 'S':
            case 'S2':
                $extraOrderNode = 'Ritira cliente - Gia pagato online';
                break;

            case 'X':
            case 'X2':
            case 'Y':
            case 'Z':
                $extraOrderNode = 'Ritira cliente con pagamento in negozio';
                break;
        }

        //note ordine
        $counter = 100;
        if (strlen(trim($this->order->notes)) > 0 or $extraOrderNode != false) {
            $item = new \stdClass();
            $item->ITM_NUMBER = $this->itemNumber($counter);
            $item->TEXT_ID = 'Z009';
            $item->LANGU = $this->getCommon('LANGU');
            $item->LANGU_ISO = $this->getCommon('LANGU_ISO');
            $item->FORMAT_COL = $this->getCommon('FORMAT_COL');
            $item->TEXT_LINE = Str::upper($this->order->notes);
            $item->TEXT_LINE = str_replace('<BR>', ', ', $item->TEXT_LINE);
            if ($extraOrderNode)
                $item->TEXT_LINE .= ' - ' . $extraOrderNode;

            $item->TEXT_LINE = ltrim($item->TEXT_LINE, ' - ');
            $item->TEXT_LINE = $this->_escape($item->TEXT_LINE);
            $schema[] = $item;
            $counter += 100;
        }

        //confezione regalo + diretta
        $tokens = [
            'gift' => config('soap.gift', 'Nessun dettaglio di confezionamento')
        ];
        if (strlen(trim($this->order->gift_box)) > 0) {
            $tokens['gift'] = str_replace('XX', $this->order->gift_box, ' + Kit XX (http://bit.ly/KsKitXX)');
        }
        if (strlen(trim($this->order->direct_name)) > 0) {
            $tokens['direct'] = 'Diretta ' . $this->order->direct_name;
        }

        $item = new \stdClass();
        $item->ITM_NUMBER = $this->itemNumber($counter);
        $item->TEXT_ID = 'Z006';
        $item->LANGU = $this->getCommon('LANGU');
        $item->LANGU_ISO = $this->getCommon('LANGU_ISO');
        $item->FORMAT_COL = $this->getCommon('FORMAT_COL');
        $item->TEXT_LINE = implode(', ', $tokens);
        $item->TEXT_LINE = $this->_escape($item->TEXT_LINE);
        $schema[] = $item;
        $counter += 100;

        //if the order has delivery in store and the payment is in store, add a new node
        if ($this->getScenary() == 'X' or $this->getScenary() == 'X2') {
            $customer = $this->getOrder()->getCustomer();
            $customerName = $customer->name;
            $item = new \stdClass();
            $item->ITM_NUMBER = $this->itemNumber($counter);
            $item->TEXT_ID = 'Z013';
            $item->LANGU = $this->getCommon('LANGU');
            $item->LANGU_ISO = $this->getCommon('LANGU_ISO');
            $item->FORMAT_COL = $this->getCommon('FORMAT_COL');
            $item->TEXT_LINE = "Ordinato da: $customerName";
            $item->TEXT_LINE = $this->_escape($item->TEXT_LINE);
            $schema[] = $item;
            $counter += 100;
        }


        $this->root['orderText'] = $schema;
    }


    private function _orderHeaderIn()
    {
        /*
         PURCH_DATE	Data 		2017-01-01
         PMNTTRMS			WEB1 (bonifico)  WEB2 (PAY PALL)  WEB3 (Carta di credito)  CA (Contrassegno)
         PURCH_NO_C 	Ordine d'acquisto del cliente		S_Nr.riferimento   esempo : KRONO_00000001
         * */

        //static default data
        $schema = Config::get('soap.order.defaults.orderHeaderIn');
        //in case of B2B Straps workflow, replace the main schema
        if ($this->isStrapB2B) {
            $schema = Config::get('soap.order.strap.orderHeaderIn');
        }
        $schema['ORD_REASON'] = null; // default
        $schema['PURCH_DATE'] = $this->order->created_at->format('Y-m-d');

        //now the sales organization is dynamic
        $salesOrg = $this->getOrder()->getSalesOrg();
        $schema['SALES_ORG'] = $salesOrg;

        $bindParamsFromScenary = null;

        switch ($this->getScenary()) {
            case 'P':
            case 'R':
                $bindParamsFromScenary = 'soap.order.scenaries.P';
                break;

            case 'Y':
            case 'Z':
                $bindParamsFromScenary = 'soap.order.scenaries.Y';
                break;

            case 'X':
                $bindParamsFromScenary = 'soap.order.scenaries.X';
                break;

            case 'X2':
                $bindParamsFromScenary = 'soap.order.scenaries.X2';
                break;

            case 'S2':
                $bindParamsFromScenary = 'soap.order.scenaries.S2';
                break;

            case 'S':
                $bindParamsFromScenary = 'soap.order.scenaries.S';
                break;

            case 'H1':
            case 'H2':
                $bindParamsFromScenary = 'soap.order.scenaries.H1';
                break;

            case 'H3':
                $bindParamsFromScenary = 'soap.order.scenaries.H3';
                break;

            case 'H4':
            case 'H5':
                $bindParamsFromScenary = 'soap.order.scenaries.H4';
                break;

            case 'H6':
                $bindParamsFromScenary = 'soap.order.scenaries.H6';
                break;
        }
        if ($bindParamsFromScenary) {
            $params = Config::get($bindParamsFromScenary);
            if (is_array($params) and !empty($params)) {
                foreach ($params as $key => $value) {
                    $schema[$key] = $value;
                }
            }
        }

        $payment_id = $this->order->payment_id;

        $payment_code = Config::get('soap.payments.' . $payment_id);
        if ($payment_code == null) {
            //TODO
            //throw exception
        } else {
            $params = Config::get('soap.order.payments.' . $payment_code);
            if (is_array($params) and !empty($params)) {
                if (is_array($params) and !empty($params)) {
                    foreach ($params as $key => $value) {
                        $schema[$key] = $value;
                    }
                }
            }
        }
        $schema['PMNTTRMS'] = $payment_code;
        $schema['PURCH_NO_C'] = $this->order->reference;
        $schema['CURRENCY'] = $this->getCommon('CURRENCY');
        $schema['NAME'] = $this->getOrder()->receipt;

        //if 'fees' is enabled, than if the order required an 'invoice', the DOC_TYPE is always 'ZWE1', otherwise is 'ZWE4'
        $overrideDocType = $this->getOrder()->getSapDocTypeCode($schema['DOC_TYPE']);
        if (!is_null($overrideDocType) and $this->isStrapB2B === false and !$this->getOrder()->hasStorePayment()) {
            $schema['DOC_TYPE'] = $overrideDocType;
        }
        $this->doc_type = $schema['DOC_TYPE'];
        if($this->doc_type == config('soap.doc_types.receipt', 'ZWE4')){
            $schema['ORD_REASON'] = 'Z70';
        }

        $this->root['orderHeaderIn'] = (object)$schema;
    }


    private function _orderItemsIn()
    {
        $schema = [];
        /*
         * Requested vars:
         ITM_NUMBER	progressivo (char 6, di 10 in 10)		000100
            MATERIAL	Materiale		SYT15
            TARGET_QTY			0

         * */

        $counter = 100;

        /** @var \CartProduct[] $products */
        $products = $this->products;
        $sapReceiptEnabled = feats()->sapReceipts();

        //other row: products
        foreach ($products as $product) {
            $sku = Utils::getSapSku($product->product_reference, $product->product_sap_reference);
            //check if the sap sku is correct
            /** @var \Product $productObj */
            $productObj = \Product::getFullObj($product->product_id);
            if ($productObj) {
                if ($productObj->sap_sku != '') {
                    $sku = $productObj->sap_sku;
                }
            }
            $item = new \stdClass();
            $item->SHORT_TEXT = null;
            $item->ITM_NUMBER = $this->itemNumber($counter);
            if ($product->hasProductCombination()) {
                $combination = $product->getProductCombination();
                if ($combination) {
                    $sku = Utils::getSapSku($combination->sku, $combination->sap_sku);
                }
            }

            //SAP RECEIPT: if product is not 'Morellato', than the SKU is dummy, and the real SKU must be injected as SHORT_TEXT
            if($sapReceiptEnabled){
                audit("SAP RECEIPT ENABLED, Proceed to swap the SKUs", __METHOD__);
                if(!$product->isMorellato()){
                    $item->SHORT_TEXT = $productObj->getAttribute('brand_name') . ' - ' . Str::upper(trim($sku));
                    $sku = config('fees.sap_receipt_sku');
                }
            }

            $item->MATERIAL = Str::upper(trim($sku));
            $item->TARGET_QTY = $this->quantity($product->product_quantity);
            if ($product->availability_mode == 'shop' and ($product->availability_shop_id != '')) {
                $item->REC_POINT = $product->availability_shop_id;
                $item->SHIP_POINT = 350;
            } else {
                //if the product mode is 'unsolved' then we can bind any shop provided
                if ($product->availability_mode == 'unsolved' and strlen($product->availability_solvable_shops) > 0) {
                    $shop = explode(',', $product->availability_solvable_shops)[0];
                    $item->REC_POINT = $shop;
                    $item->SHIP_POINT = 350;
                }
            }
            if ($this->getScenary() == 'S' or $this->getScenary() == 'S2') {
                $item->SHIP_POINT = 350;
            }
            $price = ($product->price_tax_incl > 0) ? $product->price_tax_incl : $product->cart_price_tax_incl;
            //handle free products
            if($price <= 0.2){
                //$item->ITEM_CATEG = ($this->doc_type == 'ZWE4') ? 'ZOT2' : 'ZFRE';
                $item->ITEM_CATEG = 'ZFRE';
            }
            $schema[] = $item;
            $counter += 100;
        }

        $this->root['orderItemsIn'] = $schema;
    }


    private function _orderPartners()
    {
        $schema = [];
        /*
         Requested vars:
         PARTN_ROLE	Committente		AG
         PARTN_NUMB	Codice		0000162420 (contrassegno)  0000162431 (pagamento elettronico)  0000162432 (bonifico)
         ADDR_LINK	link codice indirizzo		0000000001
         * */
        $payment_id = $this->order->payment_id;
        $carrier_id = $this->order->carrier_id;
        $payment_code = Config::get('soap.payments_codes.' . $payment_id);
        if ($payment_code == null) {
            //TODO
            //throw exception
        }
        $carrier_code = Config::get('soap.carriers.' . $carrier_id);
        if ($carrier_code == null) {
            //default carrier code for Italy
            $carrier_code = '0010000195';
        }
        $customer = $this->getOrder()->getCustomer();

        switch ($this->getScenary()) {
            case 'P':
            case 'R':
            case 'S':
            case 'S2':
            case 'X':
            case 'X2':
            case 'H6':
            case 'H5':
            case 'H4':
                //if the order has delivery in store but no payment in store, then the first segment is the billing address, but the second segment is the address of the store

                $item = new \stdClass();
                $item->PARTN_ROLE = 'AG'; //billing
                $item->PARTN_NUMB = $payment_code;
                $item->ADDR_LINK = $this->addressLink(1);
                //in B2B Strap workflow, the PARTN_NUMB is the customer code
                if ($this->isStrapB2B and $customer) {
                    $item->PARTN_NUMB = $customer->customer_code;
                }
                $schema[] = $item;

                $store = $this->getOrder()->getDeliveryStore();
                if ($store) {
                    $item = new \stdClass();
                    $item->PARTN_ROLE = 'WE'; //shipping
                    $item->PARTN_NUMB = $payment_code;
                    $item->ADDR_LINK = $this->addressLink(2);
                    //in B2B Strap workflow, the PARTN_NUMB is the customer code
                    if ($this->isStrapB2B and $customer) {
                        $item->PARTN_NUMB = $customer->customer_code;
                    }
                    $schema[] = $item;
                }
                break;


            case 'Y':
            case 'Z':

                $item = new \stdClass();
                $item->PARTN_ROLE = 'AG'; //custom code
                $item->PARTN_NUMB = \Config::get('soap.order.shopDelivery.orderHeaderIn.PARTN_NUMB');
                $item->ADDR_LINK = '';
                //in B2B Strap workflow, the PARTN_NUMB is the customer code
                if ($this->isStrapB2B and $customer) {
                    $item->PARTN_NUMB = $customer->customer_code;
                }
                $schema[] = $item;

                $store = $this->getOrder()->getDeliveryStore();
                if ($store) {
                    $item = new \stdClass();
                    $item->PARTN_ROLE = 'WE'; //shipping
                    $item->PARTN_NUMB = $store->cd_internal;
                    $item->ADDR_LINK = $this->addressLink(1);
                    //in B2B Strap workflow, the PARTN_NUMB is the customer code
                    if ($this->isStrapB2B and $customer) {
                        $item->PARTN_NUMB = $customer->customer_code;
                    }
                    $schema[] = $item;
                }

                break;

            default:
                //standard SAP requirements

                $item = new \stdClass();
                $item->PARTN_ROLE = 'AG'; //billing
                $item->PARTN_NUMB = $payment_code;
                $item->ADDR_LINK = $this->addressLink(1);
                //in B2B Strap workflow, the PARTN_NUMB is the customer code
                if ($this->isStrapB2B and $customer) {
                    $item->PARTN_NUMB = $customer->customer_code;
                }
                $schema[] = $item;

                $item = new \stdClass();
                $item->PARTN_ROLE = 'WE'; //shipping
                $item->PARTN_NUMB = $payment_code;
                $item->ADDR_LINK = $this->addressLink(2);
                //in B2B Strap workflow, the PARTN_NUMB is the customer code
                if ($this->isStrapB2B and $customer) {
                    $item->PARTN_NUMB = $customer->customer_code;
                }
                $schema[] = $item;
                break;
        }


        $item = new \stdClass();
        $item->PARTN_ROLE = 'SP'; //carrier
        $item->PARTN_NUMB = $carrier_code;
        $item->ADDR_LINK = '';
        $schema[] = $item;

        $this->root['orderPartners'] = $schema;
    }


    private function _orderSchedulesIn()
    {
        $schema = [];
        /*
         Requested vars:
         ITM_NUMBER	progressivo (char 6, di 10 in 10)		000100
         SCHED_LINE	progressivo (char 4, di 1 in 1)		0001
         REQ_DATE	data consegna richiesta		20131120
         REQ_QTY	Quantità richiesta		1.0
         * */

        if ($this->order->delivery_date == null) {
            $time = strtotime("+2 days");
            $dayOfWeek = date('w', $time);
            if ($dayOfWeek == 0) {
                $time += 24 * 60 * 60;
            }
            if ($dayOfWeek == 6) {
                $time += 24 * 60 * 60 * 2;
            }
            $this->order->delivery_date = date('Y-m-d', $time);
        }
        $counter = 100;
        $line = 1;
        $deliveryDate = date('Ymd', strtotime($this->order->delivery_date));


        //other row: products
        foreach ($this->products as $product) {

            $item = new \stdClass();
            $item->ITM_NUMBER = $this->itemNumber($counter);
            $item->SCHED_LINE = $this->line($line);
            $item->REQ_DATE = $deliveryDate;
            $item->REQ_QTY = $this->quantity($product->product_quantity);
            $schema[] = $item;
            $counter += 100;
        }

        $this->root['orderSchedulesIn'] = $schema;
    }


    private function _orderSchedulesInx()
    {
        $schema = [];
        /*
         Requested vars:
         ITM_NUMBER	progressivo (char 6, di 10 in 10)		000100
         SCHED_LINE	progressivo (char 4, di 1 in 1)		0001
         REQ_DATE	data consegna richiesta		20131120
         REQ_QTY	Quantità richiesta		1.0
         * */

        if ($this->order->delivery_date == null) {
            //TODO
            //throw exception
        }
        $counter = 100;
        $line = 1;

        //other row: products
        foreach ($this->products as $product) {

            $item = new \stdClass();
            $item->ITM_NUMBER = $this->itemNumber($counter);
            $item->SCHED_LINE = $this->line($line);
            $item->REQ_DATE = 'X';
            $item->REQ_QTY = 'X';
            $schema[] = $item;
            $counter += 100;
        }

        $this->root['orderSchedulesInx'] = $schema;
    }


    private function _partnerAddresses()
    {
        $schema = [];
        /*
        Requested vars:
        ADDR_NO	link codice indirizzo		0000000001
        NAME	nome 		Nome cliente FATTURAZIONE
        NAME_2	nome 2  se c'è
        NAME_3	nome 3 - se c'è
        CITY	city		Città
        POSTL_COD1	cap		61110
        STREET	indirizo		Indirizzo
        STREET_NO	nr		5
        HOUSE_NO
        COUNTRY	paese		IT
        LANGU	lingua		I
        REGION	regione(provincia)		VE
        ADR_NOTES	nota indirizzo
        TEL1_NUMBR	nr tel		0610836796
        FAX_NUMBER	nr fax
        E_MAIL	indirizzo email		indirizzo e-mail
         * */

        $billingAddress = $this->getOrder()->getBillingAddress();
        $shippingAddress = $this->getOrder()->getShippingAddress();
        $customer = $this->getOrder()->getCustomer();
        $store = $this->getOrder()->getDeliveryStore();
        $scenary = $this->getScenary();

        //in this case, if the B2B Strap is ON, we want the scenary treated as 'D'
        if ($this->isStrapB2B) {
            $scenary = 'D';
        }

        switch ($scenary) {
            case 'P':
            case 'R':
                $schema[] = $this->getAddressItem(1, $billingAddress, $customer);
                $schema[] = $this->getStoreAddressItem(2, $store, $customer, true);
                break;

            case 'Y':
            case 'Z':
                $schema[] = $this->getStoreAddressItem(1, $store, $customer, true);
                break;

            case 'X2':
            case 'S2':
            case 'H6':
                $schema[] = $this->getAddressItem(1, $billingAddress, $customer);
                $schema[] = $this->getStoreFullAddressItem(2, $store);
                break;

            case 'H4':
            case 'H5':
                $schema[] = $this->getAddressItem(1, $billingAddress, $customer);
                $schema[] = $this->getStoreAddressItem(2, $store, $customer, true);
                break;

            case 'X':
            case 'S':
            default:
                //standard SAP requirements
                $schema[] = $this->getAddressItem(1, $billingAddress, $customer);
                $schema[] = $this->getAddressItem(2, $shippingAddress, $customer);
                break;
        }

        $this->root['partnerAddresses'] = $schema;
    }


    function getMigrationData()
    {
        return [
            'order_id' => $this->order->id,
            'xml' => $this->xml,
            'schema' => serialize($this->root),
        ];
    }


    private function getStoreAddressItem($index, $store, $customer, $shopAsFirst = false)
    {
        $item = new \stdClass();
        $item->ADDR_NO = $this->addressLink($index);
        $customerAddress = $this->_escape(Str::upper(trim($customer->name)));
        if ($shopAsFirst) {
            $item->NAME = 'C/O ' . $this->_escape(Str::upper(trim($store->name)));
            $item->NAME_2 = $customerAddress;
            $item->NAME_3 = $store->cd_neg;
        }
        $item->NAME_4 = '';

        $item->CITY = $this->_escape(Str::upper(trim($store->city)));
        $item->POSTL_COD1 = Str::upper(trim($store->cap));
        $item->STREET = $this->_escape(Str::upper(trim($store->address)));
        $item->HOUSE_NO = '';
        $item->STREET_NO = '';
        $item->COUNTRY = 'IT';
        $item->LANGU = $this->getCommon('LANGU');
        $item->REGION = Str::upper(trim($store->state));
        $item->ADR_NOTES = $this->_escape(Str::upper(trim($store->description)));
        $item->TEL1_NUMBR = Str::upper($this->_escape(trim($store->tel)));
        $item->FAX_NUMBER = Str::upper($this->_escape(trim($store->fax)));
        $item->E_MAIL = Str::upper($this->_escape(trim($customer->email)));

        //new request, the FAX_NUMBER must be the 'transaction' code
        try{
            $code = $this->getOrder()->getFeeTransactionCodeAttribute();
            $item->FAX_NUMBER = $code;
        }catch (\Exception $e){
            audit_exception($e, __METHOD__);
        }

        return $item;
    }


    private function getStoreFullAddressItem($index, $store)
    {
        $item = new \stdClass();
        $item->ADDR_NO = $this->addressLink($index);
        $item->NAME = $this->_escape(Str::upper(trim($store->name)));
        $item->NAME_2 = '';
        $item->NAME_3 = $store->cd_neg;
        $item->NAME_4 = '';

        $item->CITY = $this->_escape(Str::upper(trim($store->city)));
        $item->POSTL_COD1 = Str::upper(trim($store->cap));
        $item->STREET = $this->_escape(Str::upper(trim($store->address)));
        $item->HOUSE_NO = '';
        $item->STREET_NO = '';
        $item->COUNTRY = 'IT';
        $item->LANGU = $this->getCommon('LANGU');
        $item->REGION = Str::upper(trim($store->state));
        $item->ADR_NOTES = $this->_escape(Str::upper(trim($store->description)));
        $item->TEL1_NUMBR = Str::upper($this->_escape(trim($store->tel)));
        $item->FAX_NUMBER = Str::upper($this->_escape(trim($store->fax)));
        $item->E_MAIL = Str::upper($this->_escape(trim($store->email)));

        //new request, the FAX_NUMBER must be the 'transaction' code
        try{
            $code = $this->getOrder()->getFeeTransactionCodeAttribute();
            $item->FAX_NUMBER = $code;
        }catch (\Exception $e){
            audit_exception($e, __METHOD__);
        }

        return $item;
    }


    private function getAddressItem($index, $address, $customer)
    {
        $item = new \stdClass();
        $item->ADDR_NO = $this->addressLink($index);
        $item->NAME = $this->_escape(Str::upper(trim($address->name)));
        $item->CITY = $this->_escape(Str::upper(trim($address->city)));
        $item->POSTL_COD1 = Str::upper(trim($address->postcode));
        $item->STREET = $this->_escape(Str::upper(trim($address->address1)));
        $item->HOUSE_NO = Str::upper(trim($address->address2));
        $item->STREET_NO = '';
        $item->COUNTRY = Str::upper(trim($address->countryCode));
        $item->LANGU = $this->getCommon('LANGU');
        $item->REGION = Str::upper(trim($address->stateCode));
        $item->ADR_NOTES = $this->_escape(Str::upper(trim($address->extrainfo)));
        $item->TEL1_NUMBR = Str::upper($this->_escape(trim($address->phone)));
        $item->FAX_NUMBER = Str::upper($this->_escape(trim($address->fax)));
        $item->E_MAIL = Str::upper($this->_escape(trim($customer->email)));
        //cod. fiscale o partita iva
        $cf = '';
        if ($address->cf != '') {
            $cf = $address->cf;
        } else {
            if ($address->vat_number != '') {
                $cf = $address->vat_number;
            }
        }
        $item->NAME_3 = '';
        $item->NAME_4 = Str::upper($this->_escape(trim($cf)));

        //new request, the FAX_NUMBER must be the 'transaction' code
        try{
            $code = $this->getOrder()->getFeeTransactionCodeAttribute();
            $item->FAX_NUMBER = $code;
        }catch (\Exception $e){
            audit_exception($e, __METHOD__);
        }
        return $item;
    }

    private function _escape($string)
    {
        return htmlspecialchars($string, ENT_XML1 | ENT_QUOTES, 'UTF-8');
    }


}