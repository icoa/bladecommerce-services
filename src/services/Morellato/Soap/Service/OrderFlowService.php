<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 15/02/2017
 * Time: 13:26
 */

namespace services\Morellato\Soap\Service;

use Order;
use Currency;
use Utils;
use Config;
use View;
use Illuminate\Support\Str;

class OrderFlowService extends AbstractOrderService
{

    protected $order;
    protected $products;
    protected $root;
    protected $xml;
    protected $xml_template = 'xml.morellato.orderFlow';



    function make()
    {
        $slip = $this->order->getMorellatoSlip();
        if($slip){
            $this->root['number'] = $slip->sales_id;
            Utils::log($slip,__CLASS__.'::Slip');
            Utils::log($this->root,__CLASS__);
        }
    }




}