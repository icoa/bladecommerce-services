<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 15/02/2017
 * Time: 12:59
 */

namespace services\Morellato\Soap\Connection;

use SoapClient, SoapVar;
use Config;

class Client extends SoapClient
{
    /**
     * @var string
     */
    protected $wsdl;

    /**
     * Client constructor.
     *
     * @param string $wsdl
     * @param array $options
     * @param array $headers
     */
    public function __construct($wsdl, $options, array $headers = [])
    {

        $options['location'] = Config::get('soap.location');
        $options['uri'] = Config::get('soap.uri');
        $options['login'] = Config::get('soap.login');
        $options['password'] = Config::get('soap.password');
        \Utils::log($options, __METHOD__);
        parent::SoapClient($wsdl, $options);

        if (!empty($headers)) {
            $this->headers($headers);
        }
    }

    /**
     * Get all functions from the service
     *
     * @return mixed
     */
    public function getFunctions()
    {
        return $this->__getFunctions();
    }

    /**
     * Get the last request
     *
     * @return mixed
     */
    public function getLastRequest()
    {
        return $this->__getLastRequest();
    }

    /**
     * Get the last response
     *
     * @return mixed
     */
    public function getLastResponse()
    {
        return $this->__getLastResponse();
    }

    /**
     * Get the last request headers
     *
     * @return mixed
     */
    public function getLastRequestHeaders()
    {
        return $this->__getLastRequestHeaders();
    }

    /**
     * Get the last response headers
     *
     * @return mixed
     */
    public function getLastResponseHeaders()
    {
        return $this->__getLastResponseHeaders();
    }

    /**
     * Get the types
     *
     * @return mixed
     */
    public function getTypes()
    {
        return $this->__getTypes();
    }

    /**
     * Get all the set cookies
     *
     * @return mixed
     */
    public function getCookies()
    {
        return $this->__cookies;
    }

    /**
     * Set a new cookie
     *
     * @param string $name
     * @param string $value
     *
     * @return $this
     */
    public function cookie($name, $value)
    {
        $this->__setCookie($name, $value);

        return $this;
    }

    /**
     * Set the location
     *
     * @param string $location
     *
     * @return $this
     */
    public function location($location = '')
    {
        $this->__setLocation($location);

        return $this;
    }

    /**
     * Set the Soap headers
     *
     * @param array $headers
     *
     * @return $this
     */
    protected function headers(array $headers = [])
    {
        $this->__setSoapHeaders($headers);

        return $this;
    }

    /**
     * Do soap request
     *
     * @param string $request
     * @param string $location
     * @param string $action
     * @param string $version
     * @param string $one_way
     *
     * @return mixed
     */
    public function doRequest($request, $location, $action, $version, $one_way)
    {
        return $this->__doRequest($request, $location, $action, $version, $one_way);
    }

    /*public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        \Utils::log('XXXXXXXXXXXXX');
        \Utils::log($request,'REQUEST');
        \Utils::log($location,'LOCATION');
        \Utils::log($action,'ACTION');
        \Utils::log($version,'VERSION');
        return parent::__doRequest($request, $location, $action, $version, $one_way);
    }*/

    /**
     * Do a soap call on the webservice client
     *
     * @param string $function
     * @param array $params
     *
     * @return mixed
     */
    public function call($function, $params)
    {
        return call_user_func_array([$this, $function], $params);
    }

    /**
     * Allias to do a soap call on the webservice client
     *
     * @param string $function
     * @param string $xml
     * @param array $options
     * @param null $inputHeader
     * @param null $outputHeaders
     *
     * @return mixed
     */
    public function SoapCall($function,
                             $xml,
                             array $options = null,
                             $inputHeader = null,
                             &$outputHeaders = null
    )
    {

        $xmlVar = new SoapVar($xml, XSD_ANYXML);

        return $this->__soapCall($function, [$xmlVar], $options, $inputHeader, $outputHeaders);
    }
}
