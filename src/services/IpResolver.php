<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 07/06/2017
 * Time: 18:54
 */

namespace services;

use services\IpResolver\Adapters\Ip2LocationAdapter;
use services\IpResolver\Adapters\IpApiAdapter;
use services\IpResolver\IpPayload;
use Cache;
use services\Traits\TraitLocalDebug;

class IpResolver
{
    /**
     * @var IpResolver
     */
    private static $instance = null;

    const PROVIDER_IP2LOCATION = 'ip2location';
    const PROVIDER_IPAPI = 'ipApi';

    protected $config = [];
    protected $provider;
    protected $adapter;
    /** @var  Cache */
    protected $cache;
    use TraitLocalDebug;
    protected $localDebug = false;

    public function __construct()
    {
        $this->cache = Cache::driver(config('cache.iptables.store', 'redis'))->tags(['iptables']);
        $this->setProvider((string)config('template.ipResolver.provider', self::PROVIDER_IP2LOCATION));
    }

    /**
     * @return IpResolver
     */
    public static function getInstance()
    {
        if (self::$instance == null) {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }

    /**
     * @param string $provider
     * @return IpResolver
     */
    public function setProvider($provider)
    {
        if (!in_array($provider, [self::PROVIDER_IPAPI, self::PROVIDER_IP2LOCATION], true))
            throw new \RuntimeException("$provider is not a valid IpResolver provider!");

        $this->provider = $provider;
        $this->config = config('template.ipResolver.' . $this->provider, []);
        $this->audit($this->provider, __METHOD__, 'provider');
        $this->audit($this->config, __METHOD__, 'config');
        switch ($this->provider) {
            case self::PROVIDER_IP2LOCATION:
                $this->adapter = new Ip2LocationAdapter($this->config);
                break;

            case self::PROVIDER_IPAPI:
                $this->adapter = new IpApiAdapter($this->config);
                break;
        }
        return $this;
    }

    /**
     * @return $this
     */
    public function setFallbackProvider()
    {
        return $this->setProvider((string)config('template.ipResolver.fallback_provider', self::PROVIDER_IPAPI));
    }

    /**
     * @param string $key
     * @param \DateTime|int $minutes
     * @return IpPayload
     */
    public function put($key, $minutes = 360, $fallback = true)
    {
        $payload = $this->getPayload($key);

        if ($payload->isResolved()) {
            $this->audit($payload, __METHOD__, 'WRITE:' . $key);
            $this->cache->put($key, serialize($payload->toArray()), $minutes);
        } else {
            if($fallback === true){
                $this->setFallbackProvider();
                return $this->put($key, $minutes, false);
            }
            $this->audit($payload, __METHOD__, 'SKIPPED:' . $key);
        }

        return $payload;

    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function has($key)
    {
        return $this->cache->has($key);
    }

    /**
     * @param string $key
     *
     * @return null|IpPayload
     */
    public function get($key)
    {
        $isThere = $this->has($key);
        if (false === $isThere)
            return null;

        try {
            $data = unserialize($this->cache->get($key, []));
            if (!is_array($data))
                return null;

            if (empty($data))
                return null;

            $payload = new IpPayload();

            $payload->fill($data);

            $this->audit($payload, __METHOD__, 'READ:' . $key);

            return $payload;
        } catch (\Exception $e) {
            return null;
        }
    }

    public function flush()
    {
        $this->cache->flush();
    }

    /**
     * @param $ip
     * @return IpPayload
     */
    private function getPayload($ip)
    {
        return $this->adapter->fetchPayload($ip);
    }

    /**
     * @param $ip
     * @return IpPayload
     */
    public function resolve($ip)
    {
        if ($ip === '127.0.0.1') {
            $ip = '79.26.211.79';
        }
        $cachePayload = $this->get($ip);
        if (!is_null($cachePayload))
            return $cachePayload;

        $newPayload = $this->put($ip);
        return $newPayload;
    }

    /**
     * @return IpPayload
     */
    public function getEmptyPayload()
    {
        $payload = new IpPayload();
        return $payload;
    }


}