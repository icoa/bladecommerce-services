<?php

namespace services\GoogleMerchant\Commands;

use Illuminate\Console\Command;
use services\Morellato\CsvImporters\AnagraficaImporter;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;
use Illuminate\Filesystem\Filesystem;
use services\GoogleMerchantBuilder;

class GoogleMerchantBuild extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'gm:build';

    protected $files;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build the XML for GoogleMerchant';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $lang = $this->argument('lang');
        $locale = ($this->argument('locale'));
        $this->info("Executing Google Merchant Builder importer...");
        $builder = new GoogleMerchantBuilder($lang, $locale);
        $content = $builder->batch();
        $file = strtolower("googlemerchant_{$lang}-{$locale}.xml");
        $realpath = storage_path("xml/googlemerchant/{$file}");
        $this->info("File saved as [$realpath]");
        \File::put($realpath,$content);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('lang', InputArgument::REQUIRED, 'The main language'),
            array('locale', InputArgument::REQUIRED, 'The main locale'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}