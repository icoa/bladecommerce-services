<?php namespace services;


use Product;
use Category;
use SimpleXMLElement;
use Cfg;
use DB;
use Format;
use Currency;

class GoogleMerchantFeed
{

    public $xml;
    protected $channel;
    protected $config;
    protected $lang;
    protected $locale;
    protected $currency;
    protected $country;
    protected $qs;
    protected $debug = false;
    protected $cache = [];
    protected $customLabels = [];
    /**
     * @var \Illuminate\Console\Command|null
     */
    protected $console;

    function __construct($lang, $locale)
    {
        audit_watch();
        $this->config = GoogleMerchantConfig::load();
        $this->lang = $lang;
        $this->locale = $locale;
        $this->country = \Country::where('iso_code', $locale)->first();
        $this->currency = ($this->country) ? \Currency::getObj($this->country->currency_id) : \Core::getDefaultCurrency();
        \Core::setLang($lang);
        if (isset($this->config->{"custom_qs_" . $locale})) {
            $this->qs = trim($this->config->{"custom_qs_" . $locale});
        }
        $this->console = \Registry::console();
        $this->bootstrap();
    }

    function bootstrap(){
        //custom labels
        $rows = DB::table('google_custom_labels')->get();
        foreach ($rows as $row) {
            $this->customLabels[$row->node_product_id] = $row;
        }
        $categories = DB::table('google_categories')->where('locale', $this->locale)->get();
        foreach($categories as $category){
            $this->cache['cat' . $category->id] = $category->sdesc;
        }
        $categories = Category::rows($this->locale)->get();
        foreach($categories as $category){
            $this->cache['category' . $category->id] = $category;
        }
        //audit($this->cache, 'cache');
    }

    protected function DbToXml($string)
    {
        return htmlspecialchars($string, ENT_NOQUOTES, 'UTF-8');
    }

    public function getParam($param, $default = null)
    {
        if (isset($this->config->$param)) return $this->config->$param;
        return $default;
    }

    public function export()
    {
        $config = $this->config;
        $lang = $this->lang;
        $locale = $this->locale;
        $min_id = (int)$this->getParam('export_min_id', 0);
        $min_price = (float)$this->getParam('export_min_price', 0);
        $export_not_ean = (int)$this->getParam('export_not_ean', 1);
        $export_not_published = (int)$this->getParam('export_not_published', 1);
        $export_fp = (int)$this->getParam('export_fp', 1);


        $queryBuilder = Product::rows($lang)->where('id', '>', $min_id)->where('visibility', '!=', 'none');
        if ($min_price > 0) {
            $queryBuilder->where('price', '>=', $min_price);
        }
        if ($export_not_ean == 0) {
            $queryBuilder->where('ean13', '<>', '');
        }
        if ($export_not_published == 0) {
            $queryBuilder->where('published', 1);
        }
        if ($export_fp == 0) {
            $queryBuilder->where('is_out_of_production', 0);
        }

        //TODO: remove after CoronaVirus
        $queryBuilder->onlyAvailableNow()->soldout(0);

        if ($this->debug) $queryBuilder->take(30);

        $rows = $queryBuilder->orderBy('id', 'desc')->lists('id');

        $this->xml = '<?xml version="1.0" encoding="UTF-8"?><rss version="2.0" xmlns:g="http://base.google.com/ns/1.0"><channel>';


        $this->createCompanyNode();

        $many = count($rows);
        if($this->console)
            $this->console->comment("Found $many products");
        $counter = 0;

        foreach ($rows as $id) {
            $counter++;
            if($this->console)
                $this->console->comment("Adding product $id ($counter/$many)");
            $product = Product::getProductFeed($id, $this->lang);
            $this->createProductNode($product);
        }

        return $this->xml . '</channel></rss>';
    }

    private function createCompanyNode()
    {
        $cfg = Cfg::getGroup('general');
        $description = Cfg::get('META_DESCRIPTION');

        $this->addNode('title', $cfg['SHOP_NAME'], true);
        $this->addNode('description', $description, true);
        $this->addNode('link', \Site::root());

    }


    /*<g:image_link><![CDATA[https://prestashop-demo.businesstech.lan/1-cart_default/T-shirts-a-manches-courtes-delaves.jpg]]></g:image_link>
    <g:condition>new</g:condition>
    <g:additional_image_link>https://prestashop-demo.businesstech.lan/2-cart_default/T-shirts-a-manches-courtes-delaves.jpg</g:additional_image_link>
    <g:additional_image_link>https://prestashop-demo.businesstech.lan/3-cart_default/T-shirts-a-manches-courtes-delaves.jpg</g:additional_image_link>
    <g:additional_image_link>https://prestashop-demo.businesstech.lan/4-cart_default/T-shirts-a-manches-courtes-delaves.jpg</g:additional_image_link>
    <g:product_type><![CDATA[Femme &gt; Hauts &gt; T-shirts]]></g:product_type>
    <g:google_product_category><![CDATA[Apparel & Accessories]]></g:google_product_category>
    <g:quantity>1797</g:quantity>
    <g:availability>in stock</g:availability>
    <g:price>26.68 USD</g:price>
    <g:gtin>123456789123</g:gtin>
    <g:brand><![CDATA[Fashion Manufacturer]]></g:brand>
    <g:mpn><![CDATA[demo_1]]></g:mpn>
    <g:gender><![CDATA[female]]></g:gender>
    <g:age_group><![CDATA[adult]]></g:age_group>
    <g:color><![CDATA[Bleu]]></g:color>
    <g:color><![CDATA[Orange]]></g:color>
    <g:size><![CDATA[L]]></g:size>
    <g:size><![CDATA[M]]></g:size>
    <g:size><![CDATA[S]]></g:size>
    <g:material><![CDATA[Coton]]></g:material>*/

    private function createProductNode($product)
    {
        $export_sdesc = $this->getParam('export_sdesc', 0);
        $export_ldesc = $this->getParam('export_ldesc', 0);
        $export_attributes_desc = $this->getParam('export_attributes_desc', 1);
        $export_color = $this->getParam('export_color', 0);
        $export_color_attributes = $this->getParam('export_color_attributes', []);
        $export_size = $this->getParam('export_size', 0);
        $export_size_attributes = $this->getParam('export_size_attributes', []);
        $export_material = $this->getParam('export_material', 0);
        $export_materiale_attributes = $this->getParam('export_materiale_attributes', []);
        $export_gender = $this->getParam('export_gender', 0);
        $imageType = $this->getParam('image', 'default');
        $availability = $this->getParam('availability', 'real');
        if ($availability == 'fake') {
            $product->availabilityStatus = 'in_stock';
        }


        $this->xml .= '<item>';

        $title = $product->name;
        $title = str_replace([$product->sku, $product->sap_sku], '', $title);
        $title = trim(rtrim($title, ' - '));
        $title = \Str::lower($title);
        $title = ucfirst($title);

        $this->addNode('g:id', $product->id);
        $this->addNode('title', $title, true);

        $description = '';
        if ($export_sdesc) {
            $description .= $product->getShortDescription() . " ";
        }
        if ($export_ldesc AND $product->ldesc != '') {
            $description .= $product->ldesc . " ";
        }
        if ($export_attributes_desc AND $product->attributes != '') {
            $description .= $product->attributes . " ";
        }
        $description = trim($description);
        $description = \Utils::stripTags($description);
        if ($description != '') {
            $this->addNode('description', $description, true);
        }
        $link = $product->link_absolute;
        if ($this->qs) {
            $link .= "?" . $this->qs;
        }
        $this->addNode('link', $link);
        $this->addNode('g:condition', $product->condition);
        $this->addNode('g:availability', strtr($product->availabilityStatus, '_', ' '));

        if ($product->availabilityStatus == 'preorder') {
            $availability_days = (int)$product->availability_days;
            if ($availability_days > 0) {
                $date = date("c", strtotime("today +$availability_days days"));
                $this->addNode('g:availability_date', $date);
            }
        }

        $product->qty = $product->qty <= 0 ? 0 : (int)$product->qty;

        $this->addNode('g:quantity', $product->qty);


        $price = Format::currency($product->price_official_raw, false, $this->currency->id, true) . " " . $this->currency->iso_code;
        $this->addNode('g:price', $price);

        if ($product->price_final_raw < $product->price_official_raw) {
            $price = Format::currency($product->price_final_raw, false, $this->currency->id, true) . " " . $this->currency->iso_code;
            $this->addNode('g:sale_price', $price);
        }


        $defaultImg = isset($product->{$imageType . "Img"}) ? $product->{$imageType . "Img"} : $product->defaultImg;
        if ($defaultImg != '' AND $defaultImg != '/media/no.gif') {
            $this->addNode('g:image_link', \Site::rootify($defaultImg), true);
        }

        $images = $product->getImages();

        foreach ($images as $img) {
            $defaultImg = isset($img->{$imageType . "Img"}) ? $img->{$imageType . "Img"} : $img->defaultImg;

            if ($img->cover == 0 AND $defaultImg != '' AND $defaultImg != '/media/no.gif') {
                $this->addNode('g:additional_image_link', \Site::rootify($defaultImg), true);
            }
        }

        $categories = [];
        if(isset($product->related_entities) and isset($product->related_entities['categories'])){
            $categories = $product->related_entities['categories'];
        }else{
            \Helper::setCategoriesReverse($product->default_category_id, $categories);
        }
        if (count($categories)) {
            $categories = array_reverse($categories);
            $data = [];
            foreach ($categories as $c) {
                $cat = $this->getCategory($c);
                if ($cat) $data[] = $cat->name;
            }
            if (count($data)) {
                $this->addNode('g:product_type', (implode(' > ', $data)), true);
            }
        }
        $category_id = $product->default_category_id > 0 ? $product->default_category_id : $product->main_category_id;
        $google_category = $this->getGoogleCategory($category_id);
        if ($google_category) {
            $this->addNode('g:google_product_category', $google_category, true);
        }
        if (isset($product->brand_name)) {
            $this->addNode('g:brand', $product->brand_name, true);
        }

        $identifier_exists = true;
        if ($product->export_ean == 0) {
            $identifier_exists = false;
        } else {

            $ean = (trim($product->ean13) != '') ? str_pad(trim($product->ean13), 13, '0', STR_PAD_LEFT) : false;
            $upc = (trim($product->upc) != '') ? str_pad(trim($product->upc), 12, '0', STR_PAD_LEFT) : false;
            $sku = (trim($product->sku) != '') ? trim($product->sku) : false;

            $gtin = ($ean) ? $ean : $upc;

            if ($gtin) $this->addNode('g:gtin', $gtin);
            if ($sku) $this->addNode('g:mpn', $sku, true);
            if ($gtin == false AND $sku == false) {
                $identifier_exists = false;
            }
        }
        if (!$identifier_exists) {
            $this->addNode('g:identifier_exists', 'FALSE');
        }

        if(isset($product->related_entities) and isset($product->related_entities['option_ids'])){
            $attribute_ids = $product->related_entities['option_ids'];
        }else{
            $attribute_ids = $product->getProductAttributesIds();
        }

        if ($export_color and count($export_color_attributes)) {
            $string = $this->getAttributes($product->id, $attribute_ids, $export_color_attributes);
            if ($string != '') $this->addNode('g:color', $string, true);
        }
        if ($export_material and count($export_materiale_attributes)) {
            $string = $this->getAttributes($product->id, $attribute_ids, $export_materiale_attributes);
            if ($string != '') $this->addNode('g:material', $string, true);
        }
        if ($export_size and count($export_size_attributes)) {
            $string = $this->getAttributes($product->id, $attribute_ids, $export_size_attributes);
            if ($string != '') $this->addNode('g:size', $string, true);
        }
        if ($export_gender) {
            $string = $this->getGenderString($product->id, $attribute_ids);
            if ($string and $string != '') {
                if ($string != '') $this->addNode('g:gender', $string);
            }
        }


        //custom labels
        $node = isset($this->customLabels[$product->id]) ? $this->customLabels[$product->id] : null;
        for ($i = 0; $i < 5; $i++) {
            $createLabel = null;
            $behaviour = $this->getParam('attribute_label_' . $i, 0);
            //\Utils::log("BEHAVIOUR FOR attribute_label_{$i} => $behaviour", "PRODUCT ID: $product->id");
            if ($behaviour == '0') { //valore manuale
                if ($node AND isset($node->{"label_" . $i}) AND trim($node->{"label_" . $i}) != '') {
                    $createLabel = trim($node->{"label_" . $i});
                }
            } else { //valore automatico
                if (is_numeric($behaviour)) { //attributo
                    //\Utils::log('=== IS NUMERIC ===');
                    $createLabel = $this->getAttributes($product->id, $attribute_ids, [$behaviour]);
                } else {
                    //\Utils::log('=== IS STRING ===');
                    //proprietà prodotto
                    if (in_array($behaviour, ['sku', 'qty', 'id', 'ean13'])) {
                        if (strlen($product->$behaviour) > 0) {
                            $createLabel = (string)$product->$behaviour;
                        }
                    } else {
                        //custom properties with related model
                        switch ($behaviour) {
                            case 'brand_id':
                                $createLabel = $product->brand_name;
                                break;
                            case 'collection_id':
                                $createLabel = $product->collection_name;
                                break;
                            case 'default_category_id':
                                $createLabel = $product->category_name_singular;
                                break;
                            case 'main_category_id':
                                $createLabel = $product->main_category_name_singular;
                                break;
                            case 'is_new':
                                $createLabel = $product->is_new == 1 ? 'New' : null;
                                break;
                            case 'is_outlet':
                                $createLabel = $product->is_outlet == 1 ? 'Outlet' : null;
                                break;
                            case 'is_featured':
                                $createLabel = $product->is_featured == 1 ? 'Featured' : null;
                                break;
                        }

                    }

                    //\Utils::log($createLabel, "CREATE LABEL");
                }
            }
            if ($createLabel != null AND trim($createLabel) != '') {
                $this->addNode('g:custom_label_' . $i, $createLabel, true, true);
            }
        }


        $this->xml .= '</item>';
    }


    private function getAttributes($product_id, $attribute_ids, $matches)
    {
        $ids = array_intersect($attribute_ids, $matches);
        $string = '';
        if (count($ids)) {
            $data = [];
            $attr = implode(",", $ids);
            $query = "SELECT name FROM attributes_options_lang
            WHERE lang_id='$this->lang' AND attribute_option_id IN
            (select attribute_val from products_attributes where product_id=$product_id and attribute_id in ($attr))";
            $rows = DB::select($query);
            if (count($rows)) {
                foreach ($rows as $row) {
                    $data[] = $row->name == '' ? $row->name : $row->name;
                }
            }
            if (count($data)) {
                foreach ($data as $name) {
                    if (\Str::length($name) <= 40 AND \Str::length($string) <= 100 AND !\Str::contains($string, $name) AND \Str::lower($name) != 'canvas') {
                        $string .= $name . "/";
                    }
                }
                $string = rtrim($string, "/");
                $string = str_replace(' & ', '/', $string);
            }
        }

        return $string;
    }


    private function getGoogleCategory($id)
    {
        if (isset($this->cache['cat' . $id])) {
            return $this->cache['cat' . $id];
        }
        $obj = DB::table('google_categories')->where('category_id', $id)->where('locale', $this->locale)->first();
        if ($obj) {
            $this->cache['cat' . $id] = $obj->sdesc;
            return $obj->sdesc;
        }
        return null;
    }

    private function getCategory($id)
    {
        if (isset($this->cache['category' . $id])) {
            return $this->cache['category' . $id];
        }
        $obj = \Category::getObj($id, $this->lang);
        if ($obj) {
            $this->cache['category' . $id] = $obj;
            return $obj;
        }
        return null;
    }


    private function getGenderString($product_id, $attributes)
    {
        $gender_id = $this->getGenderId();
        if (in_array($gender_id, $attributes)) {
            $query = "SELECT uname FROM attributes_options WHERE id IN (select attribute_val FROM products_attributes where product_id=$product_id AND attribute_id=$gender_id)";
            $rows = DB::select($query);
            if (count($rows)) {
                foreach ($rows as $row) {
                    switch ($row->uname) {
                        case 'U':
                            return 'unisex';
                            break;
                        case 'M':
                            return 'male';
                            break;
                        case 'F':
                        case 'W':
                            return 'female';
                            break;
                    }
                }
            }
        }
        return null;
    }


    private function getGenderId()
    {
        if (isset($this->cache['gender'])) {
            return $this->cache['gender'];
        }
        $id = \Attribute::where('code', 'gender')->pluck('id');
        $this->cache['gender'] = $id;
        return $id;
    }


    private function addNode($prop, $value, $isCdata = false, $mandatory = false, $isBoolean = false)
    {
        if ($isBoolean) {
            $value = ($value === false or $value === 0) ? "false" : $value;
            $value = ($value === true or $value === 1) ? "true" : $value;
        }
        if (($value and \Str::length($value) > 0) or $mandatory) {
            if ($isCdata) $value = '<![CDATA[' . $value . ']]>';
            else $value = \Utils::xml_entities($value);
            $this->xml .= "<$prop>$value</$prop>";
        }
    }

    private function addAttribute(SimpleXMLElement $node, $prop, $value, $isBoolean = false, $mandatory = false)
    {
        if ($isBoolean) {
            $value = ($value === false or $value === 0) ? "false" : $value;
            $value = ($value === true or $value === 1) ? "true" : $value;
        }

        if (($value and strlen($value) > 0) or $mandatory) {
            $value = $this->DbToXml($value);
            $node->addAttribute($prop, $value);
        }
    }
}