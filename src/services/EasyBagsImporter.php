<?php
/**
 * Created by PhpStorm.
 * User: Nemesys
 * Date: 15/05/2015
 * Time: 14:05
 */

namespace services;

use Writer, DB;

class EasyBagsImporter {

    function run(){
        //$this->_determine();
        //$this->_images();
        $this->_newbrands();
    }

    private function _determine(){
        $killer_queries = [];

        $cats = "26,41,42,47,52,53,54,55,56";
        Writer::nl('CATEGORIES','warning');
        Writer::nl($cats);
        $products = [];

        $categories = \Category::rows('it')->whereIn('id',explode(",",$cats))->get();
        foreach($categories as $c){
            Writer::nl($c->name,'success');
        }
        $categories = \Category::rows('it')->whereNotIn('id',explode(",",$cats))->get();
        foreach($categories as $c){
            Writer::nl($c->name,'error');
        }
        $killer_queries[] = "DELETE FROM categories where id NOT IN ($cats);";
        $killer_queries[] = "DELETE FROM categories_lang where category_id NOT IN ($cats);";
        $killer_queries[] = "DELETE FROM categories_products where category_id NOT IN ($cats);";
        $killer_queries[] = "DELETE FROM google_categories where category_id NOT IN ($cats);";

        $query = "SELECT DISTINCT brand_id from products where deleted_at is null and id in (select product_id from categories_products where category_id in ($cats))";
        $brands = [];

        $rows = DB::select($query);
        foreach($rows as $row){
            $brands[] = $row->brand_id;
        }
        Writer::nl('BRANDS','warning');
        Writer::nl($brands);
        $brand_ids = implode(',',$brands);
        $killer_queries[] = "DELETE FROM brands where id NOT IN ($brand_ids);";
        $killer_queries[] = "DELETE FROM brands_lang where brand_id NOT IN ($brand_ids);";
        $killer_queries[] = "DELETE FROM collections where brand_id NOT IN (select id from brands);";
        $killer_queries[] = "DELETE FROM collections_lang where collection_id NOT IN (select id from collections);";

        //$products = \Product::whereIn('brand_id',$brands)->orWhere('')
        $brand_ids = implode(",",$brands);
        $query = "SELECT id from products WHERE brand_id IN ($brand_ids) and id in (select DISTINCT product_id from categories_products where category_id in ($cats))";
        $rows = DB::select($query);
        foreach($rows as $row){
            $products[] = $row->id;
        }
        Writer::nl('PRODUCTS','warning');
        Writer::nl($products);

        $product_ids = implode(',',$products);
        $killer_queries[] = "DELETE FROM products where id NOT IN ($product_ids);";
        $killer_queries[] = "DELETE FROM products_lang where product_id NOT IN ($product_ids);";
        $killer_queries[] = "DELETE FROM products_attributes where product_id NOT IN ($product_ids);";
        $killer_queries[] = "DELETE FROM products_attributes_position where product_id NOT IN ($product_ids);";
        $killer_queries[] = "DELETE FROM products_combinations where product_id NOT IN ($product_ids);";
        $killer_queries[] = "DELETE FROM images where product_id NOT IN ($product_ids);";

        foreach($killer_queries as $query){
            Writer::nl($query,'st1');
        }

        /*
#delete from attributes where id not in (select attribute_id from products_attributes)
#delete from attributes_lang where attribute_id not in (select id from attributes)
#delete from attributes_options where attribute_id  not in (select id from attributes)
#delete from attributes_options_lang where attribute_option_id not in (select id from attributes_options)
#delete from attributes_sets_content where attribute_id not in (select id from attributes)
#delete from attributes_sets_content_cache where attribute_id not in (select id from attributes)
#delete from attributes_sets_rules where target_type='CAT' and target_id NOT IN (select id from categories)
#delete from attributes_sets_rules where target_type='BRA' and target_id NOT IN (select id from brands)
#delete from attributes_sets_rules_cache where target_type='CAT' and target_id NOT IN (select id from categories)
#delete from attributes_sets_rules_cache where target_type='BRA' and target_id NOT IN (select id from brands)
#delete from images_lang where product_image_id not in (select id from images)
#delete from images_sets_rules where target_type='CAT' and target_id NOT IN (select id from categories)
#delete from images_sets_rules where target_type='BRA' and target_id NOT IN (select id from brands)
#delete from cache_products where id not in (select id from products)
#delete from cache_products_sets where product_id not in (select id from products)
#delete from cache_products_sets where image_set_id not in (select id from images_sets)
         * */
    }



    private function _images(){
        $source = "/workarea/www/production/l4/public/assets/products/";
        $target = "/workarea/www/easybags/l4/public/assets/products/";


        $cmd = '';

        $rows = DB::table('images')->get();
        foreach($rows as $row){
            $cmd .= "cp {$source}{$row->filename} {$target}{$row->filename}".PHP_EOL;
        }
        echo '<pre>';
        Writer::nl($cmd);
    }


    private function _newbrands(){
        //casio - 81
        //mc sterling - 80
        //morellato - 42
        $ids = DB::table('attributes')->lists('id');
        echo implode(',',$ids);
        /*
         * update products_lang set metatitle = replace(metatitle,'Kronoshop.com','Easybags.it');
update products_lang set metadescription = replace(metadescription,'Kronoshop.com','Easybags.it');
update products_lang set metakeywords = replace(metakeywords,'Kronoshop.com','Easybags.it');
update products_lang set h1 = replace(h1,'Kronoshop.com','Easybags.it');
update products_lang set ldesc = replace(ldesc,'Kronoshop.com','Easybags.it');
update products_lang set metatitle = replace(metatitle,'Kronoshop','Easybags');
update products_lang set metadescription = replace(metadescription,'Kronoshop','Easybags');
update products_lang set metakeywords = replace(metakeywords,'Kronoshop','Easybags');
update products_lang set h1 = replace(h1,'Kronoshop','Easybags');
update products_lang set ldesc = replace(ldesc,'Kronoshop','Easybags');

update categories_lang set metatitle = replace(metatitle,'Kronoshop.com','Easybags.it');
update categories_lang set metadescription = replace(metadescription,'Kronoshop.com','Easybags.it');
update categories_lang set metakeywords = replace(metakeywords,'Kronoshop.com','Easybags.it');
update categories_lang set h1 = replace(h1,'Kronoshop.com','Easybags.it');
update categories_lang set metatitle = replace(metatitle,'Kronoshop','Easybags');
update categories_lang set metadescription = replace(metadescription,'Kronoshop','Easybags');
update categories_lang set metakeywords = replace(metakeywords,'Kronoshop','Easybags');
update categories_lang set h1 = replace(h1,'Kronoshop','Easybags');

update brands_lang set metatitle = replace(metatitle,'Kronoshop.com','Easybags.it');
update brands_lang set metadescription = replace(metadescription,'Kronoshop.com','Easybags.it');
update brands_lang set metakeywords = replace(metakeywords,'Kronoshop.com','Easybags.it');
update brands_lang set h1 = replace(h1,'Kronoshop.com','Easybags.it');
update brands_lang set metatitle = replace(metatitle,'Kronoshop','Easybags');
update brands_lang set metadescription = replace(metadescription,'Kronoshop','Easybags');
update brands_lang set metakeywords = replace(metakeywords,'Kronoshop','Easybags');
update brands_lang set h1 = replace(h1,'Kronoshop','Easybags');

update collections_lang set metatitle = replace(metatitle,'Kronoshop.com','Easybags.it');
update collections_lang set metadescription = replace(metadescription,'Kronoshop.com','Easybags.it');
update collections_lang set metakeywords = replace(metakeywords,'Kronoshop.com','Easybags.it');
update collections_lang set h1 = replace(h1,'Kronoshop.com','Easybags.it');
update collections_lang set metatitle = replace(metatitle,'Kronoshop','Easybags');
update collections_lang set metadescription = replace(metadescription,'Kronoshop','Easybags');
update collections_lang set metakeywords = replace(metakeywords,'Kronoshop','Easybags');
update collections_lang set h1 = replace(h1,'Kronoshop','Easybags');
         * */
    }
}