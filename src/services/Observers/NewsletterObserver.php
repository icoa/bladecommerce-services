<?php

namespace services\Observers;

use services\Repositories\UserRepository;
use Newsletter;

class NewsletterObserver
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * CustomerObserver constructor.
     */
    public function __construct()
    {
        $this->userRepository = app(UserRepository::class);
    }

    public function saved(Newsletter $model)
    {
        if (isset($model->broadcast_events) and $model->broadcast_events == false)
            return;

        $isPristine = true;
        $checks = ['profile', 'marketing'];
        foreach ($checks as $check) {
            if ($model->isDirty($check))
                $isPristine = false;
        }
        if ($isPristine === false) {
            $this->userRepository->broadcastNewsletterGdprFlags($model);
        }

    }
}