<?php

namespace services\Observers;

use services\Repositories\UserRepository;
use Customer;
use Registry;
use services\TagCommander\CommanderActApiService;

class CustomerObserver
{
    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * CustomerObserver constructor.
     */
    public function __construct()
    {
        $this->userRepository = app(UserRepository::class);
    }

    public function saved(Customer $model)
    {
        if (isset($model->broadcast_events) and $model->broadcast_events == false)
            return;

        $isPristine = true;
        $checks = ['profile', 'marketing'];
        foreach ($checks as $check) {
            if ($model->isDirty($check))
                $isPristine = false;
        }
        if ($isPristine === false) {
            $this->userRepository->broadcastCustomerGdprFlags($model);
        }
    }

    public function deleted(Customer $model)
    {
        //audit($model->id, __METHOD__);
        if (true === config('plugins_settings.commanderAct.api.delete_customers', false)) {
            /** @var CommanderActApiService $service */
            $service = app(CommanderActApiService::class);
            $service->setCustomer($model);
            $service->deleteCustomer();
            $service->sendReport();
        }
    }
}