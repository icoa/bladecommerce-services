<?php

namespace services\Observers;

use services\Models\OrderTracking;
use OrderHistory;

class OrderHistoryObserver
{
    /**
     * @param OrderHistory $model
     */
    public function saved(OrderHistory $model)
    {
        if($model->type === OrderHistory::TYPE_PAYMENT)
            return;
        
        try{
            $order_tracking = OrderTracking::fromOrderHistory($model);
            $order_tracking->save();
        }catch (\Exception $e){
            audit_exception($e, __METHOD__);
        }
    }

    public function deleted(OrderHistory $model)
    {

    }
}