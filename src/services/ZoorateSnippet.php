<?php
/**
 * Created by PhpStorm.
 * User: f.politi
 * Date: 03/02/2016
 * Time: 11:44
 */

namespace services;
use FrontTpl;
use File;
use Utils;

class ZoorateSnippet
{
    protected $code;
    protected $scope;

    function __construct($code,$lang = null){
        $this->code = $code;
        if(php_sapi_name() == "cli"){
            $this->lang = $lang;
        }else{
            $this->scope = FrontTpl::getScope();
            $this->lang = FrontTpl::getLang();
            $this->lang = 'it';
        }
    }

    function content(){
        $content = null;
        if($this->scope == 'product'){
            $content = $this->_product();
            if(is_null($content) OR $content == '' OR \Str::contains($content,'DOCTYPE')){
                $content = $this->_default();
            }
        }else{
            $content = $this->_default();
        }
        return $content;
    }

    private function _product(){
        $obj = \FrontTpl::getData('model');
        $sku = \Str::upper(trim($obj->sku));
        if($sku != ''){
            return $this->_load_product($sku);
        }
        return null;
    }

    private function _default(){
        return $this->_load_default();
    }

    private function _load_product($sku){
        $cache_key = 'product-'.$sku.'.dat';
        $cached = $this->_getFile($cache_key);
        if($cached){
            return $cached;
        }
        return null;
    }

    public function fetch_product($sku){
        $sku = \Str::upper(trim($sku));
        $cache_key = 'product-'.$sku.'.dat';
        $remoteContent = $this->_fetch("http://white.zoorate.com/gen?w=wp&MerchantCode={$this->code}&t=microdata&version=2&sku={$sku}");
        if($remoteContent != null){
            $this->_setFile($cache_key,$remoteContent);
            return $remoteContent;
        }
        return null;
    }

    public function fetch_default(){
        $cache_key = 'default.dat';
        $remoteContent = $this->_fetch("http://white.zoorate.com/gen?w=wp&MerchantCode={$this->code}&t=microdata&version=2");
        if($remoteContent != null){
            $this->_setFile($cache_key,$remoteContent);
            return $remoteContent;
        }
        return null;
    }

    private function _load_default(){
        $cache_key = 'default.dat';
        $cached = $this->_getFile($cache_key);
        if($cached){
            return $cached;
        }
        return null;
    }

    private function _getFile($file){
        $path = storage_path('zanox/'.$this->lang);
        $filename = $path.'/'.$file;
        if(File::exists($filename)){
            $content = File::get($filename);
            if($content != '')return $content;
        }
        return null;
    }

    private function _setFile($file,$content){
        if($content == '' OR
            \Str::contains($content,'Bad Gateway') OR
            \Str::contains($content,'Internal Server Error') OR
            \Str::contains($content,'Bad Request')
        )
            return;

        $path = storage_path('zanox/'.$this->lang);
        if(!is_dir($path)){
            mkdir($path,0777,true);
        }
        $filename = $path.'/'.$file;
        if(File::exists($filename)){
            File::delete($filename);
        }
        File::put($filename,$content);
    }

    private function _fetch($url){
        //Utils::log("Starting fetching: $url",__METHOD__);
        try{
            $curl = curl_init();
            curl_setopt($curl, CURLOPT_URL, $url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HEADER, false);
            $data = curl_exec($curl);
            $info = curl_getinfo($curl);
            curl_close($curl);
            //Utils::log("Finished fetching: $url",__METHOD__);
            return $info['http_code'] == 200 ? $data : null;
        }catch(\Exception $e){
            return null;
        }

    }
}