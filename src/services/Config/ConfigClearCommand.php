<?php


namespace services\Config;


use Illuminate\Console\Command;
use File;

class ConfigClearCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'config:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove the configuration cache file';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        File::delete(storage_path('meta/config.php'));

        $this->info('Configuration cache cleared!');
    }
}
