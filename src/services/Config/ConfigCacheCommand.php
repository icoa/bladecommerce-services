<?php


namespace services\Config;

use Illuminate\Console\Command;
use File;
use Illuminate\Foundation\Application;
use LogicException;
use Throwable;
use Illuminate\Console\Application as ApplicationConsole;

class ConfigCacheCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'config:cache';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a cache file for faster configuration loading';

    /**
     * Execute the console command.
     *
     * @return void
     *
     * @throws \LogicException
     */
    public function fire()
    {
        $this->call('config:clear');

        $config = $this->getFreshConfiguration();

        $configPath = storage_path('meta/config.php');

        File::put(
            $configPath, '<?php return '.var_export($config, true).';'.PHP_EOL
        );

        try {
            require $configPath;
        } catch (Throwable $e) {
            File::delete($configPath);

            throw new LogicException('Your configuration files are not serializable.', 0, $e);
        }

        $this->info('Configuration cached successfully!');
    }

    /**
     * Boot a fresh copy of the application configuration.
     *
     * @return array
     */
    protected function getFreshConfiguration()
    {
        /** @var Application $app */
        $app = require base_path('bootstrap/start.php');

        //$app->boot();
        $app->make(ApplicationConsole::class)->boot();

        $avoid_domains = ['compile', 'workbench', 'amazon'];
        //require all config files
        $config_files = File::files(app_path('config'));
        foreach($config_files as $config_file){
            $config_file = last(explode('/', $config_file));
            if(str_contains($config_file, 'php')){
                $domain = str_replace('.php', '', $config_file);
                if(!in_array($domain, $avoid_domains, true)){
                    $null = config($domain);
                }
            }
        }

        $items = $app['config']->getItems();

        $env = $app->environment();
        $default_session_config = require app_path('config/session.php');
        $env_session_file = app_path("config/$env/session.php");
        if(file_exists($env_session_file)){
            $env_session_config = require $env_session_file;
            if(isset($env_session_file['driver'])){
                $default_session_config['driver'] = $env_session_file['driver'];
            }
        }
        //override session driver
        $items['*::session']['driver'] = $default_session_config['driver'];
        return $items;
    }
}
