<?php


namespace services\Config;


use Illuminate\Console\Command;
use File;

class ConfigTestCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'config:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test a given config key with dot notation';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $key = $this->ask('Provide a config key');
        $value = config($key, 'NOT FOUND');
        $this->comment("Config value for key '$key':");

        if (is_array($value)) {
            print_r($value);
            $this->line('');
        } elseif (is_string($value) || is_int($value)) {
            $this->info($value);
        } elseif (is_bool($value)) {
            $this->info(($value ? 'true' : 'false'));
        } else {
            $this->error('Cannot print out a valid value');
        }
    }
}
