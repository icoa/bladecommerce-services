<?php


namespace services\Cache;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Cookie;
use File;

class SessionMonsterServiceProvider extends ServiceProvider
{

    const BLADE_SESSION = 'X-Blade-Session';

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * @var Request
     */
    protected $request;

    /**
     * @var Response
     */
    protected $response;

    /**
     * @return \Illuminate\Foundation\Application
     */
    public function getApp()
    {
        return $this->app;
    }

    /**
     * @return Request
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * @return Response
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     *
     */
    private function sendDefaultHeaders()
    {
        //security header - sent by nginx
        //$this->getResponse()->headers->set('Referrer-Policy', 'no-referrer-when-downgrade');
        //$this->getResponse()->headers->set('Feature-Policy', "accelerometer 'none'; camera 'none'; geolocation 'none'; gyroscope 'none'; magnetometer 'none'; microphone 'none'; payment 'none'; usb 'none'");
    }

    /**
     * @return bool
     */
    private function swapOldCookies()
    {
        //remove old cookie if new cookie has been named
        try {
            $default_name = 'laravel_session';
            if (isset($_COOKIE[$default_name]) && config('session.cookie') !== $default_name) {
                // migrate cookie
                setcookie(config('session.cookie'), $_COOKIE[$default_name], time() + 3600, config('session.path'), config('session.domain'), config('session.secure'));
                setcookie($default_name, '', time() - 3600, config('session.path'), config('session.domain'), config('session.secure'));
                \Config::set('session.driver', 'array');
                return true;
            }
            // remove non-host cookie
            $default_name = 'blade_sess';
            if (isset($_COOKIE[$default_name]) && \App::environment() === 'live' && config('session.cookie') !== $default_name) {
                // migrate cookie
                setcookie(config('session.cookie'), $_COOKIE[$default_name], time() + 3600, config('session.path'), config('session.domain'), config('session.secure'));
                setcookie($default_name, '', time() - 3600, config('session.path'), config('session.domain'), config('session.secure'));
                \Config::set('session.driver', 'array');
                return true;
            }
        } catch (\Exception $e) {

        }

        return false;
    }

    /**
     *
     */
    private function sendXSession()
    {

        // send blade-session everytime in admin
        // send blade-session for auth-basic
        if ($this->getRequest()->is(
            'admin',
            'admin/*',
            'api',
            'api/*',
            'oauth/*',
            'payment/*',
            'files/*'
        )) {
            $this->getResponse()->headers->set(self::BLADE_SESSION, 1);
            return;
        }

        $has_cart = \Core::cartIsNotEmpty() !== null;
        $has_auth = \Core::customerIsLogged() !== null;

        if ($has_cart || $has_auth) {
            $this->getResponse()->headers->set(self::BLADE_SESSION, 1);
            $this->sendSfcCookie('avoid');
        } else {
            $this->getResponse()->headers->set(self::BLADE_SESSION, 0);
            $this->sendSfcCookie();
        }
    }

    /**
     * @param null $sfc_cookie_value
     */
    private function sendSfcCookie($sfc_cookie_value = null)
    {
        $name = config('session.secure') === true ? config('session.prefix') . 'sfc' : 'sfc';

        $send = true;

        if (null === $sfc_cookie_value) {
            $cookie = new Cookie(
                $name,
                '',
                time() - 3600,
                config('session.path'),
                config('session.domain'),
                config('session.secure')
            );

            // only reset cookie if cookie exists
            $send = isset($_COOKIE[$name]);
        } else {
            $cookie = new Cookie(
                $name,
                $sfc_cookie_value,
                $sfc_cookie_value ? time() + (60 * config('session.lifetime', 1440 * 1)) : time() - 3600,
                config('session.path'),
                config('session.domain'),
                config('session.secure')
            );
        }

        if ($send) {
            setcookie($cookie->getName(), $cookie->getValue(), $cookie->getExpiresTime(), $cookie->getPath(), $cookie->getDomain(), $cookie->isSecure(), $cookie->isHttpOnly());
        }
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->after(function (Request $request, $response) {

            /** @var Response $response */
            $this->response = $response;
            $this->request = $request;

            $this->sendDefaultHeaders();

            // do nothing on redirection
            if ($response->isSuccessful() === false || $response->isRedirection() || $response instanceof RedirectResponse) {
                return;
            }

            $swapped = $this->swapOldCookies();
            if ($swapped) {
                return;
            }

            $this->sendXSession();
        });
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return array();
    }

}