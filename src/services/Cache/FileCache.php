<?php

namespace services\Cache;

use Exception;
use RuntimeException;

// Our class
class FileCache
{
    private static $instance = null;
    private $root;

    private function __construct()
    {
        $root = storage_path('cache/files');
        $this->root = $root;
        if (!is_dir($root)) {
            if (!mkdir($root, 0775) && !is_dir($root)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $root));
            }
        }
    }

    /**
     * @return FileCache
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            $c = __CLASS__;
            self::$instance = new $c;
        }

        return self::$instance;
    }

    /**
     * @param string $key
     * @param mixed $data
     * @param int $ttl
     */
    function store($key, $data, $ttl = 1440)
    {
        $fileName = $this->getFileName($key);
        // Opening the file
        $h = fopen($fileName, 'wb');
        if (!$h) {
            throw new RuntimeException('Could not write to cache');
        }
        // Serializing along with the TTL
        $data = serialize(array(time() + $ttl, $data));
        if (fwrite($h, $data) === false) {
            throw new RuntimeException('Could not write to cache');
        }
        fclose($h);
    }

    /**
     * @param $key
     */
    function forget($key)
    {
        $fileName = $this->getFileName($key);
        if (file_exists($fileName))
            unlink($fileName);
    }

    /**
     * @param $key
     * @return string
     */
    private function getFileName($key)
    {
        return $this->root . '/' . $key;

    }

    /**
     * @param $key
     * @return mixed|null
     */
    public function fetch($key)
    {
        $filename = $this->getFileName($key);
        if (!file_exists($filename) || !is_readable($filename)) return null;

        $data = file_get_contents($filename);

        $data = @unserialize($data);
        if (!$data) {
            // Unlinking the file when unserializing failed
            unlink($filename);
            return null;
        }

        // checking if the data was expired
        if (time() > $data[0]) {
            // Unlinking
            unlink($filename);
            return null;
        }
        return $data[1];
    }

}
