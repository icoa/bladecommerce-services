<?php


namespace services\Cache;


use Illuminate\Console\Command;
use Product;
use Mainframe;
use Core;

class SocketCacheWorker
{

    protected $command;

    function __construct(Command $command)
    {
        \Config::set('elasticquent.expand_products', true);
        $this->command = $command;
        $this->languages = Mainframe::languagesCodes();
        // socket_worker.php
        // create TCP socket, bind to 11111
        $socket = socket_create(AF_INET, SOCK_STREAM, 0); //IPv4, TCP
        socket_bind($socket, '127.0.0.1', 11111);
        //listen to socket
        $command->line('Listening for incoming connections...');
        socket_listen($socket);
        while (true) {
            // accept incoming TCP connection
            $ret = socket_accept($socket);
            // read the stream
            $input = socket_read($ret, 1024);
            $command->line("Received: $input");
            $output = '';
            if($input === 'close'){
                socket_close($ret);
                return;
            }
            if (is_numeric($input)) {
                foreach ($this->languages as $language) {
                    Core::setLang($language);
                    Product::getProductFeed($input, $language, true);
                    $output .= "Feed generated for $input, $language" . PHP_EOL;
                }
            }
            // send result back to client
            $command->line($output);
            socket_write($ret, $output, strlen($output));
            socket_close($ret);
        }
    }
}