<?php


namespace services\Cache\Sfc;


use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Session;
use Symfony\Component\HttpFoundation\Cookie;
use File;

class SfcCache
{
    /**
     * @var array
     */
    protected $tokens = [];

    /**
     * @var bool
     */
    protected $enabled = false;

    /**
     * @var bool
     */
    protected $global = true;

    /**
     * @var string
     */
    protected $cache_path;

    /**
     * @var bool
     */
    protected $has_auth = false;

    /**
     * @var bool
     */
    protected $has_cart = false;

    protected $debug = false;

    /**
     * SfcCache constructor.
     */
    public function __construct()
    {
        $this->enabled = config('cache.static.enabled', false) === true;
        $this->cache_path = config('cache.static.path', storage_path('cache_static'));
        $this->cache_path = rtrim($this->cache_path, '/') . '/';
    }

    /**
     * @return bool
     */
    public function isGlobal()
    {
        return $this->global;
    }

    /**
     * @param bool $global
     */
    public function setGlobal($global)
    {
        $this->global = $global;
    }


    /**
     * @return bool
     */
    public function isEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return bool
     */
    public function isDisabled()
    {
        return !$this->enabled;
    }

    /**
     * @return string
     */
    public function getCachePath()
    {
        return $this->cache_path;
    }

    /**
     * @param $path
     */
    protected function deleteNative($path)
    {
        $command = "find $path -mindepth 1 -maxdepth 1 -type d -print0 | xargs -0 rm -R";
        shell_exec($command);
    }

    /**
     * @param $path
     */
    protected function delete($path)
    {
        $dirs = File::directories($path);
        foreach ($dirs as $dir) {
            File::deleteDirectory($dir, false);
        }
    }

    /**
     * Remove all content from cache
     */
    public function flush()
    {
        if ($this->isDisabled())
            return false;

        $path = $this->getCachePath();
        if (!is_dir($path)) {
            throw new \RuntimeException('Could not find any path at ' . $path);
        }

        if (\Core::isWin() === false) {
            $this->deleteNative($path);
        } else {
            $this->delete($path);
        }
        return true;
    }

    /**
     * @param Request $request
     * @return null|\Illuminate\Http\Response
     */
    public function handleRequest(&$request)
    {
        if ($request->ajax() || $request->isJson() || $request->isMethod('GET') === false) {
            return null;
        }

        $client_etag = $request->header('If-None-Match');

        if ($this->debug) {
            audit_report($client_etag, '$client_etag');
            audit_report($request->headers->all(), 'headers');
        }

        if (strlen($client_etag) >= 32) {
            $client_etag = substr(str_replace(['"', 'W/', 'W\/'], '', $client_etag), 0, 32);
            $response = null;
            $this->sendFingerPrint($request, $response);
            $server_etag = $this->getEtag();

            if ($this->debug)
                audit_track(compact('client_etag', 'server_etag'), __METHOD__);

            if ($client_etag === $server_etag) {
                $response = Response::create('', 304);
                $this->attachCacheHeaders($response);
                return $response;
            }
        }

        return null;
    }


    /**
     * @param \Illuminate\Http\Request $request
     * @param \Illuminate\Http\Response $response
     *
     * @return \Illuminate\Http\Response
     */
    public function handle(&$request, &$response)
    {
        // do nothing on redirection
        if ($response->isRedirection() or $response instanceof RedirectResponse) {
            return $response;
        }
        if ($this->debug) {
            audit_report('Passing redirection');
        }

        // do nothing if the response is not successfull
        if ($response->isSuccessful() === false) {
            return $response;
        }
        if ($this->debug) {
            audit_report('Passing statusCode');
        }

        //bust cart for testing
        if (\App::environment() !== 'live') {
            if ($request->has('forget_cart')) {
                Session::forget('blade_cart');
            }
        }

        if ($request->ajax() || $request->isJson() || $request->isMethod('GET') === false) {
            $this->sendCacheable($response, false);
            $this->sendFingerPrint($request, $response);
            return $response;
        }

        if ($this->debug) {
            audit_report('Passing request');
        }

        /** @var \services\Morellato\ResponseCache\CacheProfiles\CacheEcommerceRequests $strategy */
        $strategy = app(\services\Morellato\ResponseCache\CacheProfiles\CacheEcommerceRequests::class);
        $shouldCacheRequest = ($strategy) ? $strategy->shouldCacheRequest($request) : false;
        if ($strategy) {
            $this->sendCacheable($response, $shouldCacheRequest);
        }

        $this->sendFingerPrint($request, $response);

        if ($this->isDisabled()) {
            $this->attachCacheHeaders($response);
            return $response;
        }

        if ($this->debug) {
            audit_report($request->getQueryString(), 'Query-string');
        }
        if ((string)$request->getQueryString() !== '') {
            $shouldCacheRequest = false;
        }

        if ($shouldCacheRequest) {
            if ($this->debug) {
                audit_report('Caching response');
            }
            try {
                $this->saveResponse($request, $response);
                $this->attachCacheHeaders($response);
            } catch (\Exception $e) {
                audit_exception($e, __METHOD__);
            }
        }else{
            $this->attachCacheHeaders($response);
        }

        if ($this->debug) {
            audit_report('Returning response: final');
        }

        return $response;
    }

    /**
     * @param Response $response
     * @param bool $is_cacheable
     */
    protected function sendCacheable(&$response, $is_cacheable = true)
    {
        if ($this->debug) {
            audit_report('Sending cacheable');
        }
        $response->headers->set('X-Cacheable', $is_cacheable ? 1 : 0);
    }


    /**
     * @param Request $request
     * @param Response $response
     */
    protected function saveResponse(&$request, &$response)
    {
        if ($this->debug) {
            audit_report('Saving response');
        }
        $content = $response->getContent();
        $partials = [];

        $partials['theme'] = config('mobile', false) == true ? 'mobile' : 'frontend';

        $fp = $this->getFingerPrint();
        $partials['fp'] = $fp;

        $partials['path'] = ltrim($request->getPathInfo(), '/');

        $path = $this->getCachePath();
        $dirName = implode('/', $partials);
        $targetPath = $path . $dirName;

        $date = date('Y-m-d H:i:s');
        //audit($targetPath, 'creating target path');
        if (!is_dir($targetPath))
            File::makeDirectory($targetPath, 0777, true, true);

        File::put($targetPath . '/index.html', $content . "<!-- STATIC.{$partials['theme']} ({$fp}) ON [$date] -->", true);
        if ($this->debug) {
            audit_report("SAVED STATIC.{$partials['theme']} ({$fp}) ON [$date]");
        }

        // do not save global cache file if there is a session
        if (false === $this->isGlobal()) {
            return;
        }

        unset($partials['fp']);
        $globalDirName = implode('/', $partials);
        $globalTargetPath = $path . $globalDirName;

        if (!is_dir($globalTargetPath))
            File::makeDirectory($globalTargetPath, 0777, true, true);

        File::put($globalTargetPath . '/index.html', $content . "<!-- STATIC.{$partials['theme']} (GLOBAL) ON [$date] -->", true);

        if ($this->debug) {
            audit_report("STATIC.{$partials['theme']} (GLOBAL) ON [$date]");
        }
    }

    /**
     * @param Request $request
     * @param Response $response
     */
    protected function sendFingerPrint(&$request, &$response)
    {
        $has_cart = false;
        $has_auth = false;

        $headers = [];
        $session = Session::all();

        if (array_key_exists('blade_lang', $session)) {
            array_set($headers, 'X-Blade-Lang', $session['blade_lang']);
            $this->addToken('lang', $session['blade_lang']);
        }

        if (array_key_exists('blade_country', $session)) {
            array_set($headers, 'X-Blade-Country', $session['blade_country']);
            $this->addToken('country', $session['blade_country']);
        }

        if (array_key_exists('blade_currency', $session)) {
            array_set($headers, 'X-Blade-Currency', $session['blade_currency']);
            $this->addToken('currency', $session['blade_currency']);
        }

        if (array_key_exists('theme', $session)) {
            array_set($headers, 'X-Blade-Theme', $session['theme']);
            $this->addToken('theme', $session['theme']);
        }

        if ($cart_id = \Core::cartIsNotEmpty()) {
            $has_cart = true;
            array_set($headers, 'X-Blade-Cart', $cart_id);
            $this->addToken('cart', $cart_id);
        }

        if ($customer_id = \Core::customerIsLogged()) {
            $has_auth = true;
            array_set($headers, 'X-Blade-Auth', $customer_id);
            $this->addToken('auth', $customer_id);
        }

        $this->has_cart = $has_cart;
        $this->has_auth = $has_auth;

        $fingerPrint = $this->getFingerPrint();

        array_set($headers, 'X-Blade-FingerPrint', $fingerPrint);

        if ($has_auth || $has_cart) {
            $this->setGlobal(false);
        }

        if ($response) {
            foreach ($headers as $key => $value) {
                $response->headers->set($key, $value);
            }

            $name = config('session.secure') === true ? config('session.prefix') . 'bladefp' : 'bladefp';
            $cookie = new Cookie(
                $name,
                $fingerPrint,
                time() + (60 * config('session.lifetime', 1440 * 1)),
                config('session.path'),
                config('session.domain'),
                config('session.secure')
            );
            setcookie($cookie->getName(), $cookie->getValue(), $cookie->getExpiresTime(), $cookie->getPath(), $cookie->getDomain(), $cookie->isSecure(), $cookie->isHttpOnly());
        }
    }

    /**
     * @return string
     */
    private function getEtag()
    {
        $this->addToken('uri', \Request::fullUrl());
        return $this->getFingerPrint();
    }

    /**
     * @param Response $response
     */
    private function attachCacheHeaders(&$response)
    {
        $cacheControl = 'max-age=0, no-cache, private';
        if ($this->has_auth || $this->has_cart) {
            $cacheControl = 'max-age=0, must-revalidate, no-cache, no-store, private';
        }
        $etag = $this->getEtag();
        $response->headers->set('Cache-Control', $cacheControl, true);
        $response->setEtag($etag, true);
    }

    /**
     * @param $key
     * @param $token
     */
    private function addToken($key, $token)
    {
        $this->tokens[$key] = $key . '=' . $token;
    }

    /**
     * @param $key
     * @param null $default
     * @return mixed
     */
    private function getToken($key, $default = null)
    {
        return array_get($this->tokens, $key, $default);
    }

    /**
     * @return string
     */
    public function getFingerPrint()
    {
        return md5(implode(',', $this->tokens));
    }
}