<?php

namespace services\Cache\Commands;

use Illuminate\Console\Command;
use services\Cache\Sfc\SfcCache;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use File;

class ClearStaticCache extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'cache:static:clear';

    protected $socket;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove all files and directories from the static cache root';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        try {
            $done = app(SfcCache::class)->flush();
            if($done){
                $this->info('DONE');
            }else{
                $this->comment('NOT DONE');
            }
        } catch (\Exception $e) {
            $this->error($e->getMessage());
            audit_exception($e, __METHOD__);
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}