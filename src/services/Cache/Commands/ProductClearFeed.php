<?php

namespace services\Cache\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use File;

class ProductClearFeed extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'feed:clear';

    protected $socket;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Remove all static representations for products';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $path = storage_path('cache/files');
        $files = File::files($path);
        foreach($files as $file){
            unlink($file);
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}