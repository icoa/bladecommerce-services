<?php

namespace services\Cache\Commands;

use Illuminate\Console\Command;
use services\Cache\SocketCacheWorker;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Product;
use Mainframe;

class SocketListen extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'socket:listen';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new socket listener';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $worker = new SocketCacheWorker($this);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}