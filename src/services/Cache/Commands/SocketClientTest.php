<?php

namespace services\Cache\Commands;

use Illuminate\Console\Command;
use services\Cache\SocketCacheWorker;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Product;
use Mainframe;

class SocketClientTest extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'socket:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test the socket worker';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $socket = socket_create(AF_INET, SOCK_STREAM, 0);
        $result = socket_connect($socket, '127.0.0.1', 11111);
// send Hello to worker
        socket_write($socket, 'Hello', 5);
// wait for response from worker
        $result = socket_read($socket, 1024);
        $this->line($result);
        socket_close($socket);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}