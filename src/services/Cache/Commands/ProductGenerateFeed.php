<?php

namespace services\Cache\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Product;
use Mainframe;
use Core;

class ProductGenerateFeed extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'feed:product';

    protected $socket;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a static representation for a complete product feed in JSON';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        \Config::set('elasticquent.expand_products', true);
        $rows = Product::inStocks()->lists('id');
        $many = count($rows);
        $counter = 0;
        $use_socket = $this->option('socket');

        if ($use_socket) {
            $this->comment('Generating feed using socket');
            foreach ($rows as $id) {
                $counter++;
                $this->line("Sending $id ($counter/$many)");
                $this->connect();
                $this->send($id);
                $this->disconnect();
            }
        } else {
            $this->comment('Generating feed file-system');
            $languages = Mainframe::languagesCodes();
            $many *= count($languages);
            foreach ($languages as $language) {
                foreach ($rows as $id) {
                    $counter++;
                    $this->line("Sending $id ($counter/$many)");
                    Core::setLang($language);
                    Product::getProductFeed($id, $language, true);
                }
            }
        }
    }

    protected function connect()
    {
        $this->socket = socket_create(AF_INET, SOCK_STREAM, 0);
        socket_connect($this->socket, '127.0.0.1', 11111);
    }

    protected function disconnect()
    {
        socket_close($this->socket);
    }

    protected function send($id)
    {
        socket_write($this->socket, $id, strlen($id));
        // wait for response from worker
        $result = socket_read($this->socket, 1024);
        $this->line($result);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('socket', null, InputOption::VALUE_OPTIONAL, 'If the optimizer must process only relations binding.', null),
        );
    }

}