<?php


/*
 * This file is part of the Sonata Project package.
 *
 * (c) Thomas Rabaix <thomas.rabaix@sonata-project.org>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace services\Auth\GoogleAuthenticator;

use Illuminate\Encryption\Encrypter;
use Exception;

if (!function_exists('hash_equals')) {
    function hash_equals($str1, $str2)
    {
        if (strlen($str1) != strlen($str2)) {
            return false;
        } else {
            $res = $str1 ^ $str2;
            $ret = 0;
            for ($i = strlen($res) - 1; $i >= 0; $i--) $ret |= ord($res[$i]);
            return !$ret;
        }
    }
}

/**
 * @see https://github.com/google/google-authenticator/wiki/Key-Uri-Format
 */
final class GoogleAuthenticator implements GoogleAuthenticatorInterface
{

    const BOOT_NAME = 'bootGoogle2FaChallenge';
    const SESSION_KEY = '2fa_auth';

    /**
     * @var int
     */
    private $passCodeLength;

    /**
     * @var int
     */
    private $secretLength;

    /**
     * @var int
     */
    private $pinModulo;

    /**
     * @var \DateTimeInterface
     */
    private $instanceTime;

    /**
     * @var int
     */
    private $codePeriod;

    /**
     * @var int
     */
    private $periodSize = 30;

    /**
     * @var string
     */
    private $username = '';

    /**
     * @var string
     */
    private $secret;

    /**
     * @var Encrypter
     */
    private $encrypter;

    /**
     * GoogleAuthenticator constructor.
     * @param int $passCodeLength
     * @param int $secretLength
     * @param \DateTimeInterface|null $instanceTime
     * @param int $codePeriod
     */
    public function __construct($passCodeLength = 6, $secretLength = 10, \DateTimeInterface $instanceTime = null, $codePeriod = 30)
    {
        /*
         * codePeriod is the duration in seconds that the code is valid.
         * periodSize is the length of a period to calculate periods since Unix epoch.
         * periodSize cannot be larger than the codePeriod.
         */

        $this->passCodeLength = $passCodeLength;
        $this->secretLength = $secretLength;
        $this->codePeriod = $codePeriod;
        $this->periodSize = $codePeriod < $this->periodSize ? $codePeriod : $this->periodSize;
        $this->pinModulo = pow(10, $passCodeLength);
        $this->instanceTime = $instanceTime ? $instanceTime : new \DateTimeImmutable();
        $this->encrypter = new Encrypter(config('app.key'));
    }

    /**
     * @param string $secret
     * @param string $code
     * @param int $discrepancy
     * @return boolean
     */
    public function checkCode($secret, $code, $discrepancy = 1)
    {
        /**
         * Discrepancy is the factor of periodSize ($discrepancy * $periodSize) allowed on either side of the
         * given codePeriod. For example, if a code with codePeriod = 60 is generated at 10:00:00, a discrepancy
         * of 1 will allow a periodSize of 30 seconds on either side of the codePeriod resulting in a valid code
         * from 09:59:30 to 10:01:29.
         *
         * The result of each comparison is stored as a timestamp here instead of using a guard clause
         * (https://refactoring.com/catalog/replaceNestedConditionalWithGuardClauses.html). This is to implement
         * constant time comparison to make side-channel attacks harder. See
         * https://cryptocoding.net/index.php/Coding_rules#Compare_secret_strings_in_constant_time for details.
         * Each comparison uses hash_equals() instead of an operator to implement constant time equality comparison
         * for each code.
         */
        $periods = floor($this->codePeriod / $this->periodSize);

        $result = 0;
        for ($i = -$discrepancy; $i < $periods + $discrepancy; ++$i) {
            $dateTime = new \DateTimeImmutable('@' . ($this->instanceTime->getTimestamp() - ($i * $this->periodSize)));
            $result = hash_equals($this->getCode($secret, $dateTime), $code) ? $dateTime->getTimestamp() : $result;
        }

        return $result > 0;
    }

    /**
     * NEXT_MAJOR: add the interface typehint to $time and remove deprecation.
     *
     * @param string $secret
     * @param float|string|int|\DateTimeInterface|null $time
     * @return string
     */
    public function getCode($secret, /* \DateTimeInterface */
                            $time = null)
    {
        if (null === $time) {
            $time = $this->instanceTime;
        }

        if ($time instanceof \DateTimeInterface) {
            $timeForCode = floor($time->getTimestamp() / $this->periodSize);
        } else {
            @trigger_error(
                'Passing anything other than null or a DateTimeInterface to $time is deprecated as of 2.0 ' .
                'and will not be possible as of 3.0.',
                E_USER_DEPRECATED
            );
            $timeForCode = $time;
        }

        $base32 = new FixedBitNotation(5, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567', true, true);
        $secret = $base32->decode($secret);

        $timeForCode = str_pad(pack('N', $timeForCode), 8, \chr(0), STR_PAD_LEFT);

        $hash = hash_hmac('sha1', $timeForCode, $secret, true);
        $offset = \ord(substr($hash, -1));
        $offset &= 0xF;

        $truncatedHash = $this->hashToInt($hash, $offset) & 0x7FFFFFFF;

        return str_pad((string)($truncatedHash % $this->pinModulo), $this->passCodeLength, '0', STR_PAD_LEFT);
    }

    public function generateSecret()
    {
        return (new FixedBitNotation(5, 'ABCDEFGHIJKLMNOPQRSTUVWXYZ234567', true, true))
            ->encode(random_bytes($this->secretLength));
    }

    private function hashToInt($bytes, $start)
    {
        return unpack('N', substr(substr($bytes, $start), 0, 4))[1];
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return $this
     */
    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getSecret()
    {
        return $this->secret;
    }

    /**
     * @param $secret
     * @return $this
     */
    public function setSecret($secret)
    {
        $this->secret = $secret;
        return $this;
    }


    /**
     * @return bool
     */
    public function hasUsername()
    {
        return strlen(trim($this->username)) > 0;
    }

    /**
     * @return string
     */
    public function getIssuer()
    {
        return ucfirst(config('app.project', 'Blade'));
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getQrCodeUrl()
    {
        if (!$this->hasUsername()) {
            throw new Exception("Cannot generate a QrCode URI without a proper username");
        }
        return GoogleQrUrl::generate($this->getUsername(), $this->getSecret(), $this->getIssuer());
    }

    /**
     * @return string
     */
    public function getQrCodeImgTag()
    {
        $url = $this->getQrCodeUrl();
        return "<img src='$url' width='200' height='200'>";
    }

    /**
     * @return string
     */
    public function encryptSecret()
    {
        return $this->encrypter->encrypt($this->getSecret());
    }

    /**
     * @param $payload
     * @return mixed|string
     */
    public function decryptSecret($payload)
    {
        return $this->encrypter->decrypt($payload);
    }
}