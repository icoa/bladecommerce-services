<?php


namespace services\Auth\GoogleAuthenticator;


trait TraitTwoFactorAuth
{
    /**
     * @return bool
     */
    function is2FaRequired()
    {
        return config('auth.google_authenticator.enabled', false);
    }

    /**
     * @return bool
     */
    function shouldDisplay2FaWarning()
    {
        if ($this->is2FaRequired() === false)
            return false;

        if ($this->negoziando == 1)
            return false;

        if ($this->clerk == 1)
            return false;

        if ($this->has_2fa == 0)
            return true;

        return false;
    }

    /**
     * @return GoogleAuthenticator
     */
    function getGoogleAuthenticatorService()
    {
        $service = new GoogleAuthenticator();
        $service->setUsername($this->email);
        return $service;
    }

    /**
     * @return string
     */
    function generateGoogle2FaSecret()
    {
        return $this->getGoogleAuthenticatorService()->generateSecret();
    }

    /**
     * @return string
     */
    function bootGoogle2FaChallenge()
    {
        $session_secret = $secret = $this->getGoogle2FaSecretCodeChallenge();
        if (is_null($session_secret)) {
            $secret = $this->generateGoogle2FaSecret();
            $id = $this->id;
            $key = GoogleAuthenticator::BOOT_NAME . $id;
            \Session::put($key, $secret);
        }
        return $secret;
    }

    /**
     * @return string
     */
    function getGoogle2FaQrCodeImg()
    {
        $service = $this->getGoogleAuthenticatorService();
        $service->setSecret($this->getGoogle2FaSecretCodeChallenge());
        return $service->getQrCodeImgTag();
    }

    /**
     * @return string|null
     */
    function getGoogle2FaSecretCodeChallenge()
    {
        $id = $this->id;
        $key = GoogleAuthenticator::BOOT_NAME . $id;
        $secret = \Session::get($key);
        return $secret;
    }

    function delete2FaSecretCodeChallenge()
    {
        $id = $this->id;
        $key = GoogleAuthenticator::BOOT_NAME . $id;
        \Session::forget($key);
    }

    /**
     * @param $code
     * @return bool
     */
    function check2FaGoogleCodeChallenge($code)
    {
        $service = $this->getGoogleAuthenticatorService();
        $secret = $this->getGoogle2FaSecretCodeChallenge();
        return $service->checkCode($secret, $code);
    }

    /**
     *
     */
    function save2FaEncryptedToken()
    {
        $service = $this->getGoogleAuthenticatorService();
        $secret = $this->getGoogle2FaSecretCodeChallenge();
        $token = $service->setSecret($secret)->encryptSecret();
        $this->setAttribute('has_2fa', 1);
        $this->setAttribute('gauth_token', $token);
        $this->save();
        $this->delete2FaSecretCodeChallenge();
        $this->set2FaSessionToken();
    }

    /**
     * @return mixed|string
     */
    function get2FaDecryptedToken()
    {
        $service = $this->getGoogleAuthenticatorService();
        $token = $service->decryptSecret($this->getAttribute('gauth_token'));
        return $token;
    }

    /**
     * @return bool
     */
    function has2faEnabled()
    {
        $flag = $this->getAttribute('has_2fa') == 1;
        if (false === $flag)
            return false;

        $token = $this->get2FaDecryptedToken();
        return $flag and strlen($token) > 0;
    }

    /**
     *
     */
    function disable2Fa()
    {
        $this->setAttribute('has_2fa', 0);
        $this->setAttribute('gauth_token', null);
        $this->save();
    }

    /**
     * @param $code
     * @return bool
     */
    function check2FaCode($code)
    {
        $service = $this->getGoogleAuthenticatorService();
        $secret = $this->get2FaDecryptedToken();
        return $service->checkCode($secret, $code);
    }

    /**
     *
     */
    function set2FaSessionToken()
    {
        $gauth_token = $this->getAttribute('gauth_token');
        \Session::put(GoogleAuthenticator::SESSION_KEY, $gauth_token);
    }

    /**
     *
     */
    function forget2FaSessionToken()
    {
        \Session::forget(GoogleAuthenticator::SESSION_KEY);
    }

    /**
     * @return bool
     */
    function has2FaSessionToken()
    {
        $gauth_token = $this->getAttribute('gauth_token');
        return \Session::get(GoogleAuthenticator::SESSION_KEY) === $gauth_token;
    }

    /**
     * @return bool
     */
    function requires2FaCode()
    {
        if ($this->has2faEnabled()) {
            return !$this->has2FaSessionToken();
        }
        return false;
    }
}