<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 28/09/2016
 * Time: 12:58
 */

namespace services\Amazon;
use services\Amazon\Models\AmazonAttribute;

class AmazonRepository
{
    function getAttributesMapping(){
        $attributes = AmazonAttribute::where('type','select')->orderBy('displayName')->get();
        foreach($attributes as $attribute){
            $templateNames = $attribute->amazonTemplates()->orderBy('displayName')->lists('displayName');
            $attribute->fullName = implode(" / ",$templateNames);
            $attribute->menu = json_decode($attribute->options);
        }
        return $attributes;
    }
}