<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 20/09/2016
 * Time: 17:44
 */

namespace services\Amazon\Models;


class AmazonAttribute extends \Eloquent
{
    protected $table = 'amazon_attributes';

    public $timestamps = null;

    protected $fillable = [
        'name',
        'displayName',
        'xmlNode',
        'xmlType',
        'options',
    ];

    function amazonTemplates(){
        return $this->belongsToMany('services\Amazon\Models\AmazonTemplate', 'amazon_attributes_templates', 'attribute_id', 'template_id');
    }
}