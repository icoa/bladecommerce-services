<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 20/09/2016
 * Time: 17:44
 */

namespace services\Amazon\Models;


class AmazonCategoryTemplate extends \Eloquent
{
    protected $table = 'amazon_categories_templates';

    public $timestamps = null;
}