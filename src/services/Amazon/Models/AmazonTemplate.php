<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 20/09/2016
 * Time: 17:44
 */

namespace services\Amazon\Models;


class AmazonTemplate extends \Eloquent
{
    protected $table = 'amazon_templates';

    public $timestamps = null;

    protected $fillable = ['name','displayName','variationTheme'];

    function amazonAttributes(){
        return $this->belongsToMany('services\Amazon\Models\AmazonAttribute', 'amazon_attributes_templates', 'template_id', 'attribute_id');
    }
}