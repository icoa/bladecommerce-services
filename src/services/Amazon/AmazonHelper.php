<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 20/09/2016
 * Time: 16:25
 */

namespace services\Amazon;

use services\Amazon\Models\AmazonAttribute;
use services\Amazon\Models\AmazonCategoryTemplate;
use services\Amazon\Models\AmazonTemplate;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;
use DB;
use Writer;


class AmazonHelper
{
    static function categoriesTemplatesMapping()
    {
        $data = [];
        $rows = AmazonCategoryTemplate::get();
        foreach ($rows as $row) {
            $data[$row->category_id] = $row->template_id;
        }
        return $data;
    }

    static function templatesOptions()
    {
        $rows = AmazonTemplate::orderBy('name')->get();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        return ['' => '- Nessuna selezione -'] + $data;
    }

    static function setCategoriesTemplates($template)
    {
        AmazonCategoryTemplate::truncate();
        foreach ($template as $category_id => $template_id) {
            if ($template_id > 0) {
                AmazonCategoryTemplate::insert(compact('category_id', 'template_id'));
            }
        }
    }

    static function importNodes()
    {
        $rows = DB::select("select * from amazon_nodes_import where node_id not in (select node_IT from amazon_nodes)");
        foreach ($rows as $row) {

            $paths = explode("/", $row->path);
            $root = $paths[0];
            $node_root = null;
            switch ($root) {
                case 'Orologi':
                    $node_root = 'watches';
                    $row->path = str_replace('Orologi/', null, $row->path);
                    break;

                case 'Abbigliamento':
                    $node_root = 'apparel';
                    $row->path = str_replace('Abbigliamento/', null, $row->path);
                    break;

                case 'Gioielli':
                    $node_root = 'jewelry';
                    $row->path = str_replace('Gioielli/', null, $row->path);
                    break;

                case 'Scarpe e borse':
                    $node_root = 'shoes';
                    $row->path = str_replace('Scarpe e borse/', null, $row->path);
                    break;
            }

            $data = [
                'node_IT' => $row->node_id,
                'node_UK' => $row->node_id,
                'node_path' => $row->path,
                'node_root' => $node_root,
            ];

            \Writer::nl($data);

            DB::table('amazon_nodes')->insert($data);
        }
    }


    static function loadYaml()
    {
        try {
            $value = Yaml::parse(file_get_contents(storage_path('xml/amazon/FineRing.yaml')));
            \Writer::nl($value);
        } catch (ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
        }
    }


    static function importYaml()
    {
        $filename = 'Jewelry.yaml';
        $cache = self::loadAttributesCache();
        try {
            $data = Yaml::parse(file_get_contents(storage_path('xml/amazon/' . $filename)));
            $shared = $data['Shared'];

            //insert shared definitions
            foreach ($shared as $name => $node) {

                if (!isset($cache[$name])) {

                    $displayName = null;
                    $type = 'select';
                    $options = null;
                    $xmlType = 'StringNotNull';
                    if (isset($node['name'])) {
                        $displayName = $node['name'];
                    }
                    if (isset($node['type'])) {
                        $type = $node['type'];
                    }
                    if ($type == 'select') {
                        //$options = implode(PHP_EOL,$node['options']);
                        $options = json_encode($node['options']);
                    }
                    $xmlNode = studly_case($name);
                    $insert = compact("name", "displayName", "type", "xmlNode", "xmlType", "options");
                    Writer::nl($insert);

                    $cache[$name] = DB::table('amazon_attributes')->insertGetId($insert);
                }

            }

            //insert specific attributes and bind them to template
            unset($data['Shared']);
            //Writer::nl($cache);
            //Writer::nl($data);
            //Writer::nl(array_keys($data));
            //return;
            foreach ($data as $productType => $info) {
                Writer::nl("Importing [$productType]", 'success');
                $template = AmazonTemplate::whereRaw("name like '%$productType%'")->first();
                $template_id = $template->id;

                foreach ($info as $name => $node) {

                    if ($name == 'variation_theme') {
                        $template->variationTheme = json_encode($node);
                        $template->save();
                        continue;
                    }

                    $attribute_id = isset($cache[$name]) ? $cache[$name] : null;

                    //custom attributes
                    if (is_array($node)) {
                        $displayName = null;
                        $type = 'select';
                        $options = null;
                        $xmlType = 'StringNotNull';
                        if (isset($node['name'])) {
                            $displayName = $node['name'];
                        }
                        if (isset($node['type'])) {
                            $type = $node['type'];
                        }
                        if ($type == 'select') {
                            //$options = implode(PHP_EOL,$node['options']);
                            $options = json_encode($node['options']);
                        }
                        $xmlNode = studly_case($name);
                        $insert = compact("name", "displayName", "type", "xmlNode", "xmlType", "options");
                        $attribute_id = DB::table('amazon_attributes')->insertGetId($insert);
                        Writer::nl($insert, 'warning');
                    }

                    if ($attribute_id == null) {
                        Writer::nl("ATTRIBUTE NULL $productType => $name (template {$template->id})", 'error');
                    } else {
                        Writer::nl("Binding $productType => $name (template {$template->id})");
                        DB::table('amazon_attributes_templates')->insert(compact('attribute_id', 'template_id'));
                    }

                }
            }


            //\Writer::nl($value);
        } catch (ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
        }
    }

    static function loadAttributesCache()
    {
        $rows = DB::select("select * from amazon_attributes");
        $data = [];
        foreach ($rows as $row) {
            $data[$row->name] = $row->id;
        }
        return $data;
    }

    static function fetchTemplate()
    {
        $template = AmazonTemplate::find(55);
        $attributes = $template->amazonAttributes()->orderBy('displayName')->get();
        Writer::nl($attributes->toArray());
    }

    static function fetchAttribute()
    {
        $attribute = AmazonAttribute::find(1);
        $templates = $attribute->amazonTemplates()->orderBy('displayName')->get();
        Writer::nl($templates->toArray());
    }

    static function selectWeightUnit()
    {
        return [
            'KG' => 'Chilogrammi',
            'GR' => 'Grammi',
            'LB' => 'Libbre',
            'MG' => 'Milligrammi',
            'OZ' => 'Once',
        ];
    }

    static function selectDimensionUnit()
    {
        return [
            'CM' => 'Centimetri',
            'M' => 'Metri',
            'MM' => 'Millimetri',
            'FT' => 'Piedi',
            'IN' => 'Pollici',
        ];
    }

    static function selectConditionTypes()
    {
        return [
            'New' => 'Nuovo',
            'UsedGood' => 'Usato - Buone condizioni',
            'Refurbished' => 'Ricondizionato',
        ];
    }

    static function selectBulletPointStrategies()
    {
        return [
            'description|attributes|custom' => 'Descrizione breve, Attributi, Valori fissi',
            'attributes|custom' => 'Attributi, Valori fissi',
            'custom|attributes' => 'Valori fissi, Attributi',
            'description' => 'Descrizione breve',
            'attributes' => 'Attributi',
            'custom' => 'Valori fissi',
        ];
    }
}
