<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 19/09/2016
 * Time: 16:51
 */

namespace services\Amazon;

use File;
use Utils;
use DOMDocument;

class Handler
{

    protected $path;
    protected $definitionsPath;

    function __construct()
    {
        $this->path = storage_path('xml/amazon/');
        $this->definitionsPath = $this->path . 'definitions/';
    }

    function tellMe()
    {
        return "tellMe";
    }

    function parse()
    {
        $this->loadDefinition('Jewelry/FineOther');
    }

    function loadDefinition($definition)
    {
        $filename = $this->getXsdFile($definition);
        $obj = new \stdClass();
        if (File::exists($filename)) {
            echo "<pre>";
            $xmlContent = File::get($filename);
            $xml = simplexml_load_string($xmlContent);
            if ($xml->children()->VariationData->count() > 0) {
                $obj->VariationData = [];
                foreach ($xml->children()->VariationData as $variationData) {
                    foreach ($variationData->element as $node) {
                        $nodeObj = new \stdClass();
                        $attributes = $node->attributes();
                        $name = null;
                        foreach ($attributes as $key => $value) {
                            $nodeObj->$key = (string)$value;
                            if($key == 'name')$name = $nodeObj->name;
                        }
                        if ($node->enumeration->count() > 0) {
                            $nodeObj->values = [];
                            foreach ($node->enumeration as $enumeration) {
                                $nodeObj->values[] = (string)$enumeration['value'];
                            }
                        }
                        $obj->VariationData[$name] = $nodeObj;
                    }
                }
            }
            if ($xml->children()->element->count() > 0) {
                $obj->Elements = [];
                foreach ($xml->children()->element as $element) {
                    $nodeObj = new \stdClass();
                    $attributes = $element->attributes();
                    $name = null;
                    foreach ($attributes as $key => $value) {
                        $nodeObj->$key = (string)$value;
                        if($key == 'name')$name = $nodeObj->name;
                    }
                    if ($element->enumeration->count() > 0) {
                        $nodeObj->values = [];
                        foreach ($element->enumeration as $enumeration) {
                            $nodeObj->values[] = (string)$enumeration['value'];
                        }
                    }
                    $obj->Elements[$name] = $nodeObj;
                }
            }
        }


        print_r($obj);
        /*$json  = json_encode($ob);
        $data = json_decode($json, true);
        echo "<pre>";
        print_r($data);*/
    }


    function getXsdFile($definition)
    {
        return $this->definitionsPath . $definition . '.xml';
    }
}