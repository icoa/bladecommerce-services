<?php  namespace services\Amazon;

use services\FactoryConfig;
use Str;

class AmazonConfig extends FactoryConfig
{
    static $keyName = 'AMAZON_CONFIG';
    static $parsedData = null;

    static function stores(){
        return [
          'IT','UK','ES','DE','FR'
        ];
    }

    static function loadParsed(){
        if(static::$parsedData != null){
            return static::$parsedData;
        }
        $stores = static::stores();
        $datas = static::load();
        $storeData = [];
        foreach($stores as $store){
            $storeKey = 'shop_'.$store;
            $storeData[$store] = new \stdClass();
            foreach($datas as $key => $value){
                if(Str::startsWith($key,$storeKey)){
                    $innerKey = substr($key,8);
                    $storeData[$store]->$innerKey = $value;
                    unset($datas->$key);
                }
            }
        }
        $datas->shops = $storeData;
        static::$parsedData = $datas;
        return $datas;
    }

    static function enabledShops(){
        $data = static::loadParsed();
        $enabled = [];
        foreach($data->shops as $country => $shopData){
            if($shopData->enabled == 1){
                $enabled[] = $country;
            }
        }
        return $enabled;
    }
}