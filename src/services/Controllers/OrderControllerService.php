<?php

namespace services\Controllers;

use Order;
use File;

class OrderControllerService
{
    /**
     * @param Order $order
     * @return null|string
     */
    function parseOrderFlowResponseData(Order $order)
    {
        if (is_null($order))
            return null;

        $filePath = $order->getOrderFlowResponseFile();

        if ($filePath === null)
            return null;

        $mtime = File::lastModified($filePath);
        $date = date('d-m-Y H:i:s', $mtime);
        $incipit = "Ultima scrittura del file: $date" . PHP_EOL . PHP_EOL;

        $content = File::get($filePath);
        $data = unserialize($content);
        return '<textarea readonly style=\'width: 100%; height: 50vh; background-color: #fff; font-family: "Courier New", "courier"\'>' . $incipit . print_r($data, 1) . '</textarea>';
    }
}