<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 10/07/2019
 * Time: 12:32
 */

namespace services\Elastic\Controllers;

use BaseController;
use Input;
use Country;
use Core;
use Illuminate\Support\Str;
use Response;
use services\Elastic\Repositories\ElasticProductSearchRepository;

class ElasticProductSearchController extends BaseController
{

    protected $repository;

    /**
     * @return ElasticProductSearchRepository
     */
    private function getRepository(){
        $this->repository = isset($this->repository) ? $this->repository : new ElasticProductSearchRepository();
        return $this->repository;
    }

    public function getCountries(){
        $search = Input::get('q');
        $rows = Country::rows(Core::getLang())->where('name', 'like', "%$search%")->select(['id', 'name', 'iso_code'])->get();
        return Response::json($rows->toArray());
    }

    public function getProducts(){
        $search = trim(Input::get('q'));
        $campaign = (int)trim(Input::get('campaign'));
        $data = [];
        if(Str::length($search) == 0){
            return Response::json($data);
        }
        $products = $this->getRepository()->liveSearch($search)->getProducts();
        foreach ($products as $product){
            $payload = [
                'id' => $product->id,
                'name' => $product->name,
                'img' => $product->thumbImg,
                'img_retina' => $product->thumbImgRetina,
                'price' => $product->price_final,
                'url' => ($campaign > 0) ? $product->link_absolute . "?cmpCode=$campaign" : $product->link_absolute,
            ];
            $data[] = $payload;
        }
        return Response::json($data);
    }

}