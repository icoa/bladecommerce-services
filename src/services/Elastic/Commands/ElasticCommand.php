<?php

namespace services\Elastic\Commands;

use App;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use services\Elastic\Repositories\ElasticConsoleRepository;

class ElasticCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'elastic:run';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute a task from the Elasticsearch repository';

    /**
     * @var ElasticConsoleRepository
     */
    protected $repository;

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        /** @var ElasticConsoleRepository repository */
        $this->repository = App::make(ElasticConsoleRepository::class);
        $this->repository->setConsole($this);
        $this->line('Executing ElasticElasticConsoleRepository...');
        $this->repository->exec($this->argument('task'));
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('task', InputArgument::REQUIRED, 'The task to perform'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}