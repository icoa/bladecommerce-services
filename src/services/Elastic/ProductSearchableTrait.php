<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 08/07/2019
 * Time: 16:54
 */

namespace services\Elastic;

use Elasticquent\ElasticquentTrait;
use Carbon\Carbon;
use Core\Facades\CoreFacade as Core;
use services\Elastic\Exceptions\ElasticBladeNonMappableAttribute;
use services\Elastic\Exceptions\ElasticBladeUnrequiredAttributesMapping;
use Illuminate\Database\Eloquent\Builder;
use Event;
use Product;
use App;

trait ProductSearchableTrait
{

    use ElasticquentTrait;


    /**
     *
     */
    public static function bootProductSearchableTrait()
    {
        //audit(config('elasticquent.expand_products', false), __METHOD__);
        if(config('elasticquent.expand_products', false) === true){
            Event::listen('product.full.expand', function (&$product) {
                return $product->elasticExpandFullProduct();
            });
        }
    }

    public function elasticExpandFullProduct()
    {
        $entities = [];
        $entities['categories'] = $this->getIdsCategories();
        $entities['attributes'] = $this->getIdsAttributesMap();
        $entities['price_rules'] = $this->getIdsPriceRules();
        $entities['trends'] = $this->getTrendsIds();
        $entities['trends_names'] = $this->getTrendsNames($entities['trends']);

        $attributes_data = $this->getAttributesMultiIds();
        $entities['option_ids'] = $attributes_data['option_ids'];
        $entities['option_values'] = $attributes_data['option_values'];
        $entities['attributes_ids'] = $attributes_data['attributes_ids'];
        $entities['attributes_codes'] = $attributes_data['attributes_codes'];
        $entities['attributes_labels'] = $attributes_data['attributes_labels'];

        $this->setAttribute('related_entities', $entities);
    }

    /**
     * @param $name
     * @return bool
     */
    public function hasRelatedEntity($name)
    {
        $entities = isset($this->related_entities) ? $this->related_entities : $this->getAttribute('related_entities');
        return is_array($entities) and isset($entities[$name]);
    }

    /**
     * @param $name
     * @return mixed
     */
    public function getRelatedEntity($name)
    {
        if ($this->hasRelatedEntity($name))
            return $this->getAttribute('related_entities')[$name];

        return null;
    }

    /**
     * @param $name
     * @param \Closure $callback
     * @return mixed
     */
    public function getRelatedEntities($name, \Closure $callback)
    {
        if ($this->hasRelatedEntity($name))
            return $this->getAttribute('related_entities')[$name];

        return $callback();
    }

    /**
     * @return array
     */
    public function getIdsCategories()
    {
        $ids = \DB::table("categories_products")->where("product_id", $this->id)->lists("category_id");
        foreach ($ids as $id) {
            $categories[] = $id;
        }
        \ProductHelper::bindCategoriesAncestors($categories, $categories);
        if ($categories === null) {
            $msg = "Warning: product [$this->id] has no categories to match";
            audit_error($msg, __METHOD__);
            audit($msg, __METHOD__);
        }
        $categories = is_array($categories) ? array_unique($categories) : [];
        return $categories;
    }

    /**
     * @return array
     */
    public function getIdsAttributesMap()
    {

        $query = "select products_attributes.attribute_id,
products_attributes.attribute_val 
from products_attributes 
join attributes on products_attributes.attribute_id=attributes.id
join products_attributes_position on products_attributes.attribute_id=products_attributes_position.attribute_id
AND products_attributes.product_id=products_attributes_position.product_id  
where products_attributes.product_id=$this->id 
and isnull(attributes.deleted_at)
and attributes.frontend_input in ('select','multiselect')
order by products_attributes_position.position";
        $rows = \DB::select($query);
        $schema = [];
        foreach ($rows as $row) {
            if ($row->attribute_id != '' and $row->attribute_val != '')
                $schema[] = $row->attribute_id . '|' . $row->attribute_val;
        }
        return $schema;
    }

    /**
     * @return array
     */
    public function getIdsPriceRules()
    {
        $rules_ids = [];
        $rules = \DB::table('products_specific_prices')->where('product_id', $this->id)->get();
        $now = time();
        foreach ($rules as $rule) {
            $from_passed = false;
            $to_passed = false;
            if ($rule->date_from) {
                $rule_from = strtotime($rule->date_from);
                if ($now >= $rule_from) {
                    $from_passed = true;
                }
            }
            if ($rule->date_to) {
                $rule_to = strtotime($rule->date_to);
                if ($now <= $rule_to) {
                    $to_passed = true;
                }
            }
            //both given, the time has to in both limits
            if ($rule->date_from and $rule->date_to) {
                if ($from_passed and $to_passed)
                    $rules_ids[] = $rule->id;
            } else {
                //only date from given
                if (is_null($rule->date_to)) {
                    if ($from_passed)
                        $rules_ids[] = $rule->id;
                }

                //only date to given
                if (is_null($rule->date_from)) {
                    if ($to_passed)
                        $rules_ids[] = $rule->id;
                }
            }
            if ($rule->date_from == null AND $rule->date_to == null) {
                $rules_ids[] = $rule->id;
            }
        }
        return $rules_ids;
    }

    /**
     * @return array
     */
    function getAttributesMultiIds()
    {
        $results = [
            'attributes_ids' => [],
            'attributes_codes' => [],
            'attributes_labels' => [],
            'option_ids' => [],
            'option_values' => [],
        ];
        $metadata = $this->getAttributesMetadata();
        if (!isset($metadata['attributes'])) {
            return $results;
        }

        $blacklist = self::getSkippableAttributesForIndexing();
        $attributes = $metadata['attributes'];
        //$this->audit($attributes, __METHOD__);

        foreach ($attributes as $attribute) {
            if (!in_array($attribute->code, $blacklist)) {
                $results['attributes_ids'][] = (int)$attribute->id;
                $results['attributes_codes'][] = $attribute->code;
                $results['attributes_labels'][] = $attribute->label;
                $results['option_values'][] = $attribute->value;

                if (is_array($attribute->structure)) {
                    foreach ($attribute->structure as $option) {
                        $results['option_ids'][] = (int)$option->option_id;
                    }
                }
            }
        }

        unset($metadata, $blacklist, $attributes);

        return $results;
    }

    /**
     * @return array
     */
    public function getIndexableDocumentLabels()
    {

        $labels = [];

        $hydrate_fields = array(
            'sku',
            'ean13',
            'brand_name',
            'collection_name',
            'main_category_name',
            'main_category_name_singular',
            'category_name',
            'category_name_singular',
        );

        foreach ($hydrate_fields as $field) {
            $labels[] = $this->getAttribute($field);
        }

        //Current combination SKU and EAN
        if (true == $this->getAttribute('has_combinations')) {
            $combinations = $this->getCombinations();
            foreach ($combinations as $combination) {
                $labels[] = $combination->sku;
                if ($combination->ean13 != '')
                    $labels[] = $combination->ean13;
            }
        }

        $attributes_data = $this->getAttributesMultiIds();
        //$this->audit($attributes_data, 'ATTRIBUTES DATA');
        $labels = array_merge($labels, $attributes_data['option_values']);

        //Trend names
        $trend_names = $this->getRelatedEntities('trends_names', function () {
            return $this->getTrendsNames();
        });
        if (is_array($trend_names) and !empty($trend_names)) {
            $labels = array_merge($labels, $trend_names);
        }

        //Labels by inner status
        $status_injections = [
            'is_outlet' => 'lbl_badge_outlet',
            'is_new' => 'lbl_badge_new',
            'is_in_promotion' => 'lbl_badge_offer',
        ];

        foreach ($status_injections as $attribute => $lexicon_code) {
            if (true == $this->getAttribute($attribute)) {
                $labels[] = lex($lexicon_code);
            }
        }

        $labels = array_unique($labels);
        $return_labels = [];
        foreach($labels as $key => $label){
            if($label != '')
                $return_labels[] = (string)$label;
        }

        unset($hydrate_fields, $attributes_data, $trend_names, $status_injections, $labels);

        return $return_labels;
    }


    /**
     * ElasticSearch part
     */

    /**
     * Get Mapping Properties
     *
     * @return array
     */
    public function getMappingProperties()
    {
        return isset($this->mappingProperties) ? $this->mappingProperties : $this->bootMappingProperties();
    }

    /**
     * @return string
     */
    public function getElasticLanguageAnalyzer()
    {
        $lang = Core::getLang();
        $analyzers = [
            'it' => 'italian',
            'en' => 'english',
            'fr' => 'french',
            'es' => 'spanish',
            'de' => 'german',
        ];

        return (isset($analyzers[$lang])) ? $analyzers[$lang] : 'english';
    }

    /**
     * @return array
     */
    public function bootMappingProperties()
    {

        $mappings = [];
        $language_analyzer = $this->getElasticLanguageAnalyzer();

        $booleans = [
            'show_price',
            'indexed',
            'feed',
            'gift_enabled',
            'is_available',
            'is_new',
            'is_featured',
            'is_outlet',
            'is_out_of_production',
            'is_in_promotion',
            'is_soldout',
        ];
        foreach ($booleans as $field) {
            $mappings[$field] = [
                'type' => 'boolean',
            ];
        }


        $ids = [
            'id',
            'brand_id',
            'collection_id',
            'default_category_id',
            'main_category_id',
        ];
        foreach ($ids as $field) {
            $mappings[$field] = [
                'type' => 'long',
            ];
        }


        $timestamps = [
            'created_at',
            'updated_at',
        ];
        foreach ($timestamps as $field) {
            $mappings[$field] = [
                'type' => 'date',
                'format' => 'yyyy-MM-dd HH:mm:ss',
            ];
        }


        $dates = [
            'new_from_date',
            'new_to_date',
        ];
        foreach ($dates as $field) {
            $mappings[$field] = [
                'type' => 'date',
                'format' => 'yyyy-MM-dd',
            ];
        }


        $prices = [
            'price_official_raw',
            'price_final_raw',
        ];
        foreach ($prices as $field) {
            $mappings[$field] = [
                'type' => 'float',
            ];
        }


        $counters = [
            'wanted',
            'position',
            'qty',
            'attribute_gender',
            'availability_days',
        ];
        foreach ($counters as $field) {
            $mappings[$field] = [
                'type' => 'integer',
            ];
        }


        $constants = [
            'type',
            'lang_id',
            'condition',
            'visibility',
            'source',
        ];
        foreach ($constants as $field) {
            $mappings[$field] = [
                'type' => 'keyword',
            ];
        }


        $fulltexts = [
            'sku',
            'sap_sku',
            'ean13',
            'name',
            'brand_name',
            'collection_name',
            'full_category_name',
            'gender',
        ];
        foreach ($fulltexts as $field) {
            $mappings[$field] = [
                'type' => 'text',
                'fields' => [
                    'keyword' => [
                        'type' => 'keyword',
                        'ignore_above' => 256,
                    ]
                ],
            ];
        }

        $specials = [
            'labels'
        ];
        foreach ($specials as $field) {
            $mappings[$field] = [
                'type' => 'text',
                'fields' => [
                    'keyword' => [
                        'type' => 'keyword',
                        'ignore_above' => 1024,
                    ]
                ],
            ];
        }


        $arrays = [
            'attributes' => 'keyword',
            'categories' => 'long',
            'trends' => 'long',
            'price_rules' => 'long',
            'options' => 'long',
        ];
        foreach ($arrays as $field => $type) {
            $mappings[$field] = [
                'type' => $type,
            ];
        }


        $texts = [
            'sdesc',
            'ldesc',
            'indexable',
            'metadescription',
            'metakeywords',
        ];
        foreach ($texts as $field) {
            $mappings[$field] = [
                'type' => 'text',
                'analyzer' => $language_analyzer,
            ];
        }

        $this->setMappingProperties($mappings);

        return $mappings;
    }

    /**
     * @return mixed
     */
    function getIndexName()
    {
        return config('elasticquent.prefix') . '_' . \Core::getLang();
    }

    /**
     * @return string
     */
    function getTypeName()
    {
        return 'products';
    }


    /**
     * @return array
     * @throws ElasticBladeNonMappableAttribute
     * @throws ElasticBladeUnrequiredAttributesMapping
     */
    function getIndexDocumentData()
    {

        $mappings = $this->getMappingProperties();

        $hydrate_fields = array(
            'id',
            'name',
            'lang_id',
            'position',
            'type',
            'sku',
            'sap_sku',
            'ean13',
            'brand_id',
            'collection_id',
            'default_category_id',
            'main_category_id',
            'condition',
            'new_from_date',
            'new_to_date',
            'show_price',
            'indexed',
            'feed',
            'gift_enabled',
            'visibility',
            'qty',
            'availability_days',
            'is_available',
            'is_new',
            'is_featured',
            'is_outlet',
            'is_out_of_production',
            'is_in_promotion',
            'is_soldout',
            'source',
            'wanted',
            'price_official_raw',
            'price_final_raw',
            'gender',
            'attribute_gender',
            'brand_name',
            'collection_name',
            'full_category_name',
            'indexable',
            'sdesc',
            'ldesc',
            'metadescription',
            'metakeywords',
            'created_at',
            'updated_at',
        );

        $data = [];

        foreach ($hydrate_fields as $field) {

            if (!isset($mappings[$field]))
                throw new ElasticBladeNonMappableAttribute("Attribute $field is not contained in the mapping types");

            $data[$field] = $this->mutateAttributeByElasticMappingType($field);
        }

        //build relations

        $data['categories'] = $this->getRelatedEntities('categories', function () {
            return $this->getIdsCategories();
        });

        $data['attributes'] = $this->getRelatedEntities('attributes', function () {
            return $this->getIdsAttributesMap();
        });

        $data['price_rules'] = $this->getRelatedEntities('price_rules', function () {
            return $this->getIdsPriceRules();
        });

        $data['trends'] = $this->getRelatedEntities('trends', function () {
            return $this->getTrendsIds();
        });

        $data['options'] = $this->getRelatedEntities('option_ids', function () {
            $attributes_data = $this->getAttributesMultiIds();
            return $attributes_data['option_ids'];
        });

        $data['labels'] = $this->getIndexableDocumentLabels();

        $data_keys = array_keys($data);
        $mappings_keys = array_keys($mappings);

        $diff = array_diff($mappings_keys, $data_keys);
        if (!empty($diff)) {
            $list = implode(', ', $diff);
            throw new ElasticBladeUnrequiredAttributesMapping("Missing mapping attributes for elastic document: $list");
        }

        unset($mappings, $hydrate_fields, $data_keys, $mappings_keys, $diff);

        $this->audit($data, __METHOD__);

        return $data;
    }

    /**
     * @param $name
     * @return mixed
     */
    protected function mutateAttributeByElasticMappingType($name)
    {
        $attribute = $this->getAttribute($name);
        $mapping = $this->getMappingProperties()[$name];
        if ((is_object($attribute) or is_array($attribute)) and $mapping['type'] != 'date')
            return $attribute;

        switch ($mapping['type']) {
            case 'integer':
            case 'long':
                $attribute = (int)$attribute;
                break;

            case 'double':
            case 'float':
                $attribute = (float)\Format::float($attribute);
                break;

            case 'boolean':
                $attribute = ((int)$attribute === 1) ? true : false;
                break;

            case 'text':
                $attribute = (string)(trim($attribute));
                if (\Str::length($attribute) === 0) {
                    $attribute = null;
                } else {
                    $attribute = strip_tags($attribute);
                }
                break;

            case 'date':
                if ($attribute === '' or $attribute === null) {
                    $attribute = null;
                } else {
                    $format = array_get($mapping, 'format', 'yyyy-MM-dd HH:ii:ss');
                    $searches = ['yyyy', 'MM', 'dd', 'HH', 'mm', 'ss'];
                    $replaces = ['Y', 'm', 'd', 'H', 'i', 's'];
                    $format = str_replace($searches, $replaces, $format);

                    if (!$attribute instanceof Carbon) {
                        $attribute = Carbon::parse($attribute);
                    }
                    $attribute = (string)$attribute->format($format);
                }
                break;

            case 'keyword':
            default:

                break;
        }

        unset($mapping);

        return $attribute;
    }

    /**
     * Selects all products that has at least a cover
     *
     * @param Builder $builder
     */
    function scopeWithCover(Builder $builder)
    {
        $builder->where('default_img', '>', 0);
    }


    /**
     * Selects all products that has at least a cover
     *
     * @param Builder $builder
     * @param string $lang
     */
    function scopePublished(Builder $builder, $lang = 'default')
    {

        $lang = \Core::getLang($lang);

        $builder
            ->join('products_lang', 'products.id', '=', 'products_lang.product_id')
            ->where('products_lang.lang_id', $lang)
            ->where('products_lang.published', 1);
    }


    /**
     * Selects all products that can be indexed in Elastic search
     *
     * @param Builder $builder
     */
    function scopeElasticIndexable(Builder $builder)
    {
        $this->scopeInStocks($builder);
        $this->scopePublished($builder);
        $this->scopeWithCover($builder);
        $builder->whereNotNull('metadata')->whereNotNull('indexable');
    }

    /**
     * @param $id
     * @param \Illuminate\Console\Command $console
     * @return array
     */
    static function upsertProductToIndex($id, $console = null)
    {
        $debug = false;
        $simulate = false;
        $responses = [];
        $languages = \Core::getLanguages();
        foreach ($languages as $lang) {
            \Core::setLang($lang);

            //perform simulation
            if ($simulate) {
                $product = new Product(compact('id'));
                $response = $product->bulkUpsertIndexTest();
                if ($response !== null)
                    $responses[] = $response;
            } else {
                if($console)
                    $console->line("Indexing $id($lang)");
                /** @var \Product $product */
                $product = self::getFullProduct($id, $lang);
                if ($product) {
                    try {
                        $response = $product->bulkUpsertIndex();
                        if ($response !== null and true === $debug)
                            $responses[] = $response;
                    } catch (\Exception $e) {
                        audit($e->getMessage(), "ES Upserting of product [$product->id]");
                        audit_exception($e, "ES Upserting of product [$product->id]");
                        if($console)
                            $console->error($e->getMessage());
                    }
                } else {
                    $msg = "WARNING => ES Upserting of product [$id] has returned a NULL model";
                    audit($msg, __METHOD__);
                    audit_error($msg, __METHOD__);
                    if($console)
                        $console->error($msg);
                }
                unset($product);
            }
        }

        if ($debug)
            audit($responses, "ES RESPONSES FOR $id");

        return $responses;
    }

    /**
     * @param $limit
     */
    static function setElasticBulkOpsLimit($limit)
    {
        $product = new Product();
        $product->bulkOpsSetLimit($limit);
    }

    /**
     * @return array
     */
    static function getSkippableAttributesForIndexing()
    {
        return [
            'package',
            'guarantee',
            'water_resistant',
            'certificate_auth',
            'moments',
            'swiss_made',
        ];
    }
}