<?php


namespace services\Elastic\Repositories;

use Product;
use Illuminate\Support\Str;

class ElasticProductSearchRepository extends ElasticSearchRepository
{

    /**
     * @param $search
     * @return $this
     */
    public function liveSearch($search)
    {

        $boolQuery = [
            'filter' => [
                [
                    'term' => ['is_out_of_production' => false]
                ],
                [
                    'term' => ['is_soldout' => false]
                ]
            ],
            'must_not' => [
                [
                    'term' => ['visibility' => 'none']
                ]
            ],
            'must' => [
                'multi_match' => [
                    'query' => $search,
                    'fuzziness' => 'AUTO',
                    'fields' => [
                        'indexable^5',
                        'labels^4',
                        'sku^2',
                        'ean13^2',
                        'name',
                        'brand_name',
                    ],
                ]
            ]
        ];

        $standardSearch = [
            'body' => [
                'track_scores' => true,
                'size' => config('elasticquent.livesearch.limit', 10),
                'sort' => [
                    ['is_featured' => 'desc'],
                    '_score',
                    ['is_new' => 'desc'],
                    ['position' => 'asc'],
                ],
                'query' => [
                    'bool' => $boolQuery
                ],
            ],
        ];

        $boostedSearch = [
            'body' => [
                'track_scores' => true,
                'size' => config('elasticquent.livesearch.limit', 10),
                'sort' => [
                    ['_score' => 'desc'],
                    ['is_featured' => 'desc'],
                    ['is_new' => 'desc'],
                    ['position' => 'asc'],
                ],
                'query' => [
                    'function_score' => [
                        'query' => [
                            'bool' => $boolQuery
                        ],
                        'boost' => 5,
                        'functions' => [
                            [
                                'filter' => [
                                    'multi_match' => $boolQuery['must']['multi_match'],
                                ],
                                'weight' => 100
                            ],

                            [
                                'filter' => ['match' => ['full_category_name' => $search]],
                                'weight' => 25
                            ],
                        ],
                        'max_boost' => 1000,
                        'score_mode' => 'sum',
                        'boost_mode' => 'sum',
                        'min_score' => 50
                    ]
                ]

            ],
        ];

        $searchParams = $standardSearch;

        if(config('elasticquent.search.enable_weights', false) === true){
            $weights = config('elasticquent.search.weights', []);
            if (!empty($weights)) {
                foreach ($weights as $key => $branch) {
                    $target_field = ('categories' === $key) ? 'categories' : 'brand_id';
                    $rules = $branch['rules'];
                    $default = $branch['default'];
                    $collected_ids = [];
                    foreach ($rules as $rule) {
                        $collected_ids += $rule['match'];
                        foreach ($rule['match'] as $id) {
                            $boostedSearch['body']['query']['function_score']['functions'][] = [
                                'filter' => ['match' => [$target_field => $id]],
                                'weight' => $rule['weight']
                            ];
                        }
                    }
                }
            }
            $searchParams = $boostedSearch;
        }
        /*$boolQuery['should'] = [

            [
                'constant_score' => [
                    'filter' => ['match' => ['default_category_id' => 16]],
                    'boost' => 3,
                ],
            ],
            [
                'constant_score' => [
                    'filter' => ['match' => ['default_category_id' => 17]],
                    'boost' => 3,
                ],
            ],
            [
                'constant_score' => [
                    'filter' => ['match' => ['default_category_id' => 31]],
                    'boost' => 2,
                ],
            ],
            [
                'constant_score' => [
                    'filter' => ['match' => ['default_category_id' => 34]],
                    'boost' => 1,
                ],
            ],
            [
                'constant_score' => [
                    'filter' => ['match' => ['default_category_id' => 90]],
                    'boost' => 0,
                ],
            ],
            [
                'constant_score' => [
                    'filter' => ['match' => ['main_category_id' => 1]],
                    'boost' => 10,
                ],
            ],
        ];

        $shouldSearch = [
            'body' => [
                'track_scores' => true,
                'size' => config('elasticquent.livesearch.limit', 10),
                'sort' => [
                    '_score',
                    ['is_featured' => 'desc'],

                    ['is_new' => 'desc'],
                    ['position' => 'asc'],
                ],
                'query' => [
                    'bool' => $boolQuery
                ],
            ],
        ];*/

        audit(compact('searchParams'), __METHOD__);

        $results = Product::complexSearch($searchParams);
        $this->setResults($results);
        return $this;
    }

    /**
     * @param $search
     * @param array $params
     * @return $this
     */
    public function liveCatalogSearch($search, $params = [])
    {
        $size = array_get($params, 'size', 96);
        $from = array_get($params, 'from', 0);
        $sort_by = array_get($params, 'sort_by', 'is_new');
        $sort_dir = array_get($params, 'sort_dir', 'desc');
        if ($sort_by === 'price')
            $sort_by = 'price_final_raw';

        $sort_fragment = [
            [$sort_by => $sort_dir],
            ['is_featured' => 'desc'],
            '_score',
            ['position' => 'asc'],
        ];

        if ($sort_by == 'wanted') {
            $sort_fragment = [
                '_score',
                [$sort_by => $sort_dir],
                ['is_featured' => 'desc'],
                ['position' => 'asc'],
            ];
        }

        $query = [
            'body' => [
                'track_scores' => true,
                'from' => $from,
                'size' => $size,
                'sort' => $sort_fragment,
                'query' => [
                    'bool' => [
                        'filter' => [
                            [
                                'term' => ['is_out_of_production' => false]
                            ],
                            [
                                'term' => ['is_soldout' => false]
                            ]
                        ],
                        'must_not' => [
                            [
                                'term' => ['visibility' => 'none']
                            ]
                        ],
                        'must' => [
                            'multi_match' => [
                                'query' => $search,
                                'fuzziness' => 'AUTO',
                                'fields' => [
                                    'indexable^5',
                                    'labels^4',
                                    'sku^2',
                                    'ean13^2',
                                    'name',
                                    'brand_name',
                                    'sdesc',
                                    'ldesc'
                                ],
                            ]
                        ],
                        /*'should' => [
                            [
                                'constant_score' => [
                                    'filter' => [
                                        'match' => [
                                            'metakeywords' => [
                                                'query' => $search,
                                                'fuzziness' => 2
                                            ]
                                        ]
                                    ],
                                    'boost' => 80,
                                ],
                            ],
                            [
                                'constant_score' => [
                                    'filter' => [
                                        'match' => [
                                            'metakeywords' => [
                                                'query' => $search,
                                                'fuzziness' => 1
                                            ]
                                        ]
                                    ],
                                    'boost' => 10,
                                ],
                            ],
                            [
                                'constant_score' => [
                                    'filter' => [
                                        'match' => [
                                            'metakeywords' => [
                                                'query' => $search,
                                            ]
                                        ]
                                    ],
                                    'boost' => 10,
                                ],
                            ]
                        ]*/
                    ]
                ],
            ],
        ];

        //audit($query, __METHOD__, 'QUERY');

        $query_count = $query;
        array_forget($query_count, [
            'body.track_scores',
            'body.from',
            'body.size',
            'body.sort',
        ]);

        $count = Product::complexSearchCount($query_count);
        $this->setCount($count);

        $results = Product::complexSearch($query);
        $this->setResults($results);
        return $this;
    }

    /**
     * @param $search
     * @param array $fields
     * @param array $sort
     * @return $this
     */
    public function customSearch($search, $fields = [], $sort = [])
    {
        $sort_items = [];
        foreach ($sort as $sort_element) {
            if (Str::contains($sort_element, '|')) {
                $tokens = explode('|', $sort_element);
                $sort_items = [$tokens[0], $tokens[1]];
            } else {
                $sort_items = [$sort_element];
            }
        }
        $results = Product::complexSearch(
            [
                'body' => [
                    'track_scores' => true,
                    'size' => 100,
                    'sort' => $sort_items,
                    'query' => [
                        'bool' => [
                            'filter' => [
                                [
                                    'term' => ['is_out_of_production' => false]
                                ],
                                [
                                    'term' => ['is_soldout' => false]
                                ]
                            ],
                            'must' => [
                                'multi_match' => [
                                    'query' => $search,
                                    'fuzziness' => 'AUTO',
                                    'fields' => $fields,
                                ]
                            ]
                        ]
                    ],
                ],
            ]
        );
        $this->setResults($results);
        return $this;
    }

    /**
     * @return array
     */
    public function getProducts()
    {
        $rows = [];
        $hits = $this->getHits();
        foreach ($hits as $hit) {
            //audit($hit, 'HIT');
            if (isset($hit['id'])) {
                $product = Product::getFullProduct($hit['id']);
                if ($product)
                    $rows[] = $product;
            }
        }
        return $rows;
    }

    /**
     * @return array
     */
    public function getIds()
    {
        $rows = [];
        $hits = $this->getHits();
        foreach ($hits as $hit) {
            if (isset($hit['id'])) {
                $rows[] = $hit['id'];
            }
        }
        return $rows;
    }
}