<?php

namespace services\Elastic\Repositories;

use services\Elastic\ElasticBladeService;
use services\Morellato\Repositories\Repository;
use Product;
use Exception;
use Config;

class ElasticConsoleRepository extends Repository
{
    /**
     * @var array
     */
    protected $commands = [
        ['fresh', 'Destroy and create all the indexes'],
        ['count_all', 'Counts all documents in each index'],
        ['sample', 'Index 5 products at random'],
        ['index_all', 'Index all products in all indexes'],
        ['index_interactive', 'Index a given set of products'],
        ['ids', 'Check id there are orphans products, and then proceed to index them'],
        ['live_search_interactive', 'Performs a live search'],
        ['catalog_search_interactive', 'Performs a catalog search'],
        ['delete', 'Remove all deleted products from all indexes'],
        ['delete_interactive', 'Force a custom deletion on all indexes'],
        ['update_mapping', 'Update current index mapping'],
    ];

    /**
     * @var ElasticProductSearchRepository
     */
    protected $service;

    function __construct(ElasticProductSearchRepository $service)
    {
        $this->service = $service;
        Config::set('elasticquent.expand_products', true);
    }

    /**
     * @return ElasticProductSearchRepository
     */
    function getService()
    {
        return $this->service;
    }

    /**
     * Performs a fresh install
     */
    function fresh()
    {
        $console = $this->getConsole();
        $confirmed = $console->confirm('This will destroy all the indexes; are You sure to proceed?');

        if (false === $confirmed) {
            $console->comment('Ok, procedure skipped');
            return;
        }

        $languages = \Core::getLanguages();
        foreach ($languages as $lang) {
            \Core::setLang($lang);

            try {
                $console->comment("Destroying index [$lang]");
                Product::deleteIndex();
                $console->info('-> DONE');
            } catch (Exception $e) {
                $console->line($e->getMessage());
            }

            try {
                $console->comment("Recreate index [$lang] with custom mappings");
                Product::createIndex();
                $console->info('-> DONE');
            } catch (Exception $e) {
                $console->line($e->getMessage());
            }


            $product = new Product();
            $type = $product->getTypeName();
            $index = $product->getIndexName();
            $console->info("Created ElasticSearch resources, INDEX: $index | TYPE: $type");
        }
        $console->info('-> FINISHED');
    }


    public function sample()
    {
        $languages = \Core::getLanguages();
        $ids = Product::elasticIndexable()
            ->select('id')
            ->take(5)
            ->orderBy(\DB::raw('rand()'))
            ->lists('id');
        foreach ($ids as $id) {
            foreach ($languages as $lang) {
                \Core::setLang($lang);
                $this->getConsole()->comment("Indexing sample product [$id]($lang)");
                $product = Product::getFullProduct($id, $lang);
                $data = $product->getIndexDocumentData();
                print_r($data);
                $json = json_encode($data, JSON_PRETTY_PRINT);
                $this->getConsole()->line($json);
                $response = $product->upsertIndex();
                print_r($response);
            }
        }
    }


    public function index_interactive()
    {
        $console = $this->getConsole();
        $given = $console->ask('Please provide a single product id or a list comma separated');

        if ($given == '') {
            $console->error("Data not provided");
            return;
        }

        $ids = explode(',', $given);

        $ids = Product::elasticIndexable()
            ->select('id')
            ->whereIn('id', $ids)
            ->lists('id');

        $many = count($ids);
        Product::setElasticBulkOpsLimit($many);

        foreach ($ids as $id) {
            Product::forgetFullCache($id);
            $this->getConsole()->comment("Indexing product [$id]");
            Product::upsertProductToIndex($id, $console);
        }
    }


    public function index_all()
    {
        $console = $this->getConsole();
        $products = Product::elasticIndexable()
            ->select('id')
            //->where('id', 3101)
            //->take(22)
            ->orderBy('id', 'desc')
            ->get();

        $many = count($products);
        $counter = 0;

        Product::setElasticBulkOpsLimit($many);
        foreach ($products as $product) {
            $counter++;
            $this->getConsole()->line("Indexing product [$product->id] ($counter/$many)");
            //Product::forgetFullCache($product->id);
            Product::upsertProductToIndex($product->id, $console);
        }
    }


    public function count_all()
    {
        $languages = \Core::getLanguages();
        foreach ($languages as $lang) {
            \Core::setLang($lang);
            $results = Product::countIndexedDocuments();
            print_r($results);
        }
    }


    public function ids()
    {
        $languages = \Core::getLanguages();
        foreach ($languages as $lang) {
            \Core::setLang($lang);
            $results = Product::getAllDocumentIds();
            $many = count($results);
            $this->getConsole()->info("Found $many ids on index [$lang]");
            $this->getConsole()->line("Getting missing indexed products...");
            $ids = Product::elasticIndexable()
                ->whereNotIn('id', $results)
                ->select('id')
                ->orderBy('id', 'desc')
                ->lists('id');

            print_r($ids);

            array_map(function ($id) use ($lang) {
                $product = Product::getFullProduct($id, $lang);
                $response = $product->upsertIndex();
                print_r($response);
            }, $ids);
        }
    }


    protected function live_search_interactive()
    {
        $console = $this->getConsole();
        $search = $console->ask('Please provide a search string');
        $results = $this->service->liveSearch($search);
        $maxScore = $results->getMaxScore();
        $shards = $results->getShardsExplained();
        $totalHits = $results->total();
        $count = $results->count();
        $headers = ['TOTAL HITS', 'MAX SCORE', 'SHARDS', 'COUNT'];
        $line = [
            $totalHits, $maxScore, $shards, $count
        ];

        $console->table($headers, [$line]);
        $products = $results->getHits();
        print_r($products);
    }


    protected function catalog_search_interactive()
    {
        $console = $this->getConsole();
        $search = $console->ask('Please provide a search string');
        $results = $this->service->liveCatalogSearch($search);
        $maxScore = $results->getMaxScore();
        $shards = $results->getShardsExplained();
        $totalHits = $results->total();
        $count = $results->count();
        $headers = ['TOTAL HITS', 'MAX SCORE', 'SHARDS', 'COUNT'];
        $line = [
            $totalHits, $maxScore, $shards, $count
        ];

        $console->table($headers, [$line]);
        $products = $results->getHits();
        print_r($products);
    }

    protected function delete()
    {
        $console = $this->getConsole();
        $console->comment('Getting all trashed products...');
        /** @var Product[] $products */
        $products = Product::onlyTrashed()->get();
        $many = count($products);
        $counter = 0;
        foreach ($products as $product) {
            $counter++;
            $console->line("Deleting product #{$product->id} ($counter/$many)");
            try {
                $product->removeFromIndex();
                $console->info('REMOVED');
            } catch (Exception $e) {
                $console->error('ERROR: ' . $e->getMessage());
            }
        }
        /** @var Product[] $products */
        $products = Product::rows()->where('published', 0)->get();
        $many = count($products);
        $counter = 0;
        foreach ($products as $product) {
            $counter++;
            $console->line("Deleting unpublished product #{$product->id} ($counter/$many)");
            try {
                $product->removeFromIndex();
                $console->info('REMOVED');
            } catch (Exception $e) {
                $console->error('ERROR: ' . $e->getMessage());
            }
        }
    }

    protected function delete_interactive()
    {
        $console = $this->getConsole();
        $given = $console->ask('Please provide a single product id or a list comma separated');

        if ($given === '') {
            $console->error("Data not provided");
            return;
        }

        $ids = explode(',', $given);
        $products = Product::withTrashed()->whereIn('id', $ids)->get();
        $many = count($products);
        $counter = 0;
        foreach ($products as $product) {
            $counter++;
            $console->line("Deleting product #{$product->id} ($counter/$many)");
            try {
                $product->removeFromIndex();
                $console->info('REMOVED');
            } catch (Exception $e) {
                $console->error('ERROR: ' . $e->getMessage());
            }
        }

        $products = Product::rows()->where('published', 0)->whereIn('id', $ids)->get();
        $many = count($products);
        $counter = 0;
        foreach ($products as $product) {
            $counter++;
            $console->line("Deleting product #{$product->id} ($counter/$many)");
            try {
                $product->removeFromIndex();
                $console->info('REMOVED');
            } catch (Exception $e) {
                $console->error('ERROR: ' . $e->getMessage());
            }
        }
    }


    protected function get_mappings()
    {
        $console = $this->getConsole();
        $id = $console->ask('Please provide the product ID');
        $test = Product::find($id);
        if (is_null($test)) {
            $console->error("The given product does not exist!");
            return;
        }
        $product = Product::getFullProduct($id);
        $mappings = $product->getMappingProperties();
        print_r($mappings);
        audit($mappings);
    }

    /**
     * @param $id
     * @return Product
     */
    function getFreshProduct($id)
    {
        //\ProductHelper::buildProductCache($id);
        //$product = Product::find($id);
        //$product->uncache();
        return Product::getFullProduct($id);
    }

    protected function update_mapping()
    {
        $console = $this->getConsole();
        $confirmed = $console->confirm('Are You sure to proceed?');

        if (false === $confirmed) {
            $console->comment('Ok, procedure skipped');
            return;
        }

        $languages = \Core::getLanguages();
        foreach ($languages as $lang) {
            \Core::setLang($lang);
            $product = new Product();
            $mapping = $product->getMappingProperties();

            try {
                $console->comment("Updating mapping [$lang]");
                print_r($mapping);
                Product::putMapping(true);
                $console->info('-> DONE');
            } catch (Exception $e) {
                $console->line($e->getMessage());
            }

            $type = $product->getTypeName();
            $index = $product->getIndexName();
            $console->info("Updated ElasticSearch resources, INDEX: $index | TYPE: $type");
        }
        $console->info('-> FINISHED');
    }
}