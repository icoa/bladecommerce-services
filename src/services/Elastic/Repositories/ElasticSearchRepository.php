<?php


namespace services\Elastic\Repositories;

use Elasticquent\ElasticquentResultCollection;

class ElasticSearchRepository
{

    /**
     * @var ElasticquentResultCollection
     */
    protected $results;

    /**
     * @var array
     */
    protected $query;

    /**
     * @var int
     */
    protected $count = 0;

    /**
     * @return array
     */
    public function getQuery()
    {
        return $this->query;
    }

    /**
     * @param array $query
     */
    public function setQuery($query)
    {
        $this->query = $query;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param int $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }


    /**
     * @return ElasticquentResultCollection
     */
    public function getResults()
    {
        return $this->results;
    }

    /**
     * @param ElasticquentResultCollection $results
     */
    public function setResults($results)
    {
        $this->results = $results;
    }

    /**
     * @return int
     */
    public function total()
    {
        $totalHits = $this->getResults()->totalHits();
        return (int)$totalHits['value'];
    }

    /**
     * @return int
     */
    public function count()
    {
        $hits = $this->results->getHits();
        return count($hits['hits']);
    }

    /**
     * @return float
     */
    public function getMaxScore()
    {
        $maxScore = $hits = $this->results->maxScore();
        return (float)$maxScore;
    }

    /**
     * @return string
     */
    public function getTotalHits()
    {
        $totalHits = $this->getResults()->totalHits();
        return $totalHits['relation'] . ' ' . $totalHits['value'];
    }

    /**
     * @return string
     */
    public function getShardsExplained()
    {
        $shards = $this->results->getShards();
        return "Total => {$shards['total']} | Successful => {$shards['successful']}";
    }

    /**
     * @return ElasticquentResultCollection
     */
    public function getHits()
    {
        return $this->results;
    }
}