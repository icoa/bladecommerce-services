<?php

namespace services\Elastic;

use services\Traits\TraitLocalDebug;

use Input;
use services\Elastic\Repositories\ElasticProductSearchRepository;

class ElasticBladeService
{
    protected $markers;
    protected $filters;
    protected $lang;
    protected $products;
    protected $data = [];
    protected $conditions = [];
    protected $pagesize = 0;
    protected $default_redirect_all = 13;
    protected $start = 0;
    protected $total = 0;
    protected $totalOutlet = 0;
    protected $minprice = 0;
    protected $maxprice = 0;
    protected $page = 1;
    protected $sortField = 'position';
    protected $sortDirection = 'asc';
    protected $main_attributes = [];
    public $attributes = [];
    public $attributes_codes = [];
    public $search = false;
    public $q = false;
    protected $localDebug = false; //this must be false on production
    protected $pagination_data;
    protected $order_by;
    protected $order_dir;
    protected $repository;

    use TraitLocalDebug;

    function __construct()
    {
        $this->repository = new ElasticProductSearchRepository();
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }



    function setup()
    {
        $this->q = Input::get('q');
        $this->pagesize = (isset($this->pagesize) AND $this->pagesize > 0) ? $this->pagesize : \Cfg::get('PRODUCTS_PER_PAGE');
        if (\Config::get('mobile', false) === true) {
            $this->pagesize = $this->pagesize / 2;
        }

        $config_order_by = config('app.catalog.order_by', 'is_new');
        $config_order_dir = config('app.catalog.order_dir', 'desc');

        if (isset($this->order_by)) {
            $order_by = $this->order_by;
        } else {
            $order_by = Input::get('order_by', $config_order_by);
        }
        if (isset($this->order_dir)) {
            $order_dir = $this->order_dir;
        } else {
            $order_dir = Input::get('order_dir', $config_order_dir);
        }

        if (!in_array($order_by, ['is_new', 'sku', 'price', 'qty', 'wanted'])) {
            $order_by = $config_order_by;
        }
        if (!in_array($order_dir, ['asc', 'desc'])) {
            $order_dir = $config_order_dir;
        }

        $this->order_by = $order_by;
        $this->order_dir = $order_dir;
        if ($this->page == 1) {
            $page = (int)Input::get('page', 1);
            $this->page = ($page > 0) ? $page : 1;
        }
        $this->start = ($this->page - 1) * $this->pagesize;
    }

    /**
     * @return string
     */
    function getTerm()
    {
        return trim($this->q);
    }

    function run()
    {
        $params = [
            'size' => $this->pagesize,
            'from' => $this->start,
            'sort_by' => $this->getOrderBy(),
            'sort_dir' => $this->getOrderDir(),
        ];

        $search = $this->repository->liveCatalogSearch($this->getTerm(), $params);
        $this->total = $search->getCount();
        $this->products = $search->getIds();
    }

    /**
     * @return array
     */
    function getProducts()
    {
        return $this->products;
    }

    /**
     * @return array
     */
    function getResults()
    {
        return $this->getProducts();
    }

    function getOrderBy()
    {
        return $this->order_by;
    }

    function getOrderDir()
    {
        return $this->order_dir;
    }

    /**
     * @return \stdClass
     */
    function getScopeObj()
    {
        $scopeObj = new \stdClass();
        $title = trans('template.search_by', ['term' => $this->q]);
        $scopeObj->metatitle = $title;
        $scopeObj->h1 = $title;
        $scopeObj->ldesc = $title;
        $scopeObj->metadescription = $title;
        $scopeObj->metakeywords = $this->q;
        return $scopeObj;
    }

    function getCanonicalLink($lang = 'default', $scheme = 'relative')
    {
        return \Link::to('nav', 13);
    }

    function getPaginationLinkTpl()
    {
        $link = $this->getCanonicalLink();
        $link = str_replace('.htm', '_{:page}.htm', $link);
        $qs = \Request::getQueryString();
        if ($qs) {
            $link .= '?' . $qs;
        }
        return $link;
    }

    function getFirstPage($withQueryString = true)
    {
        $url = \Site::rootify(str_replace('_{:page}.htm', '.htm', $this->getPaginationLinkTpl()));
        if (!$withQueryString) {
            $tokens = explode('?', $url);
            $url = $tokens[0];
        }
        return $url;
    }

    function getPaginationData()
    {
        if (isset($this->pagination_data)) return $this->pagination_data;
        $p = (object)[
            'order_by' => $this->getOrderBy(),
            'order_dir' => $this->getOrderDir(),
            'total' => $this->total,
            'viewed' => $this->total,
            'page' => $this->page,
            'pagesize' => $this->pagesize,
        ];

        $p->from = (($p->page - 1) * $p->pagesize) + 1;
        $p->to = $p->from + $p->pagesize - 1;
        if ($p->to > $p->total) $p->to = $p->total;
        $p->max_pages = ceil($this->total / $this->pagesize);
        $p->template_url = $this->getPaginationLinkTpl();
        $p->firstpage = $this->getFirstPage();
        $p->pagination = \Utils::pagination($this->total, $this->pagesize, $this->page, 10);
        $p->current = str_replace('{:page}', $p->page, $p->template_url);
        $p->prev = ($p->page > 1) ? str_replace('{:page}', $p->page - 1, $p->template_url) : false;
        if ($p->page - 1 == 1) $p->prev = $p->firstpage;
        $p->next = ($p->page < $p->max_pages) ? str_replace('{:page}', $p->page + 1, $p->template_url) : false;
        if ($p->prev) {
            \FrontTpl::setPrevLink($p->prev);
        }
        if ($p->next) {
            \FrontTpl::setNextLink($p->next);
        }
        $p->order_key = $p->order_by . '|' . $p->order_dir;
        $this->pagination_data = $p;
        audit($p, __METHOD__);
        return $p;
    }
}