<?php


namespace services\Routing;


use Frontend\Router as FrontendRouter;

class BladeRouteController extends \Controller
{
    /**
     * @param $path
     * @return \Illuminate\Http\Response|string|\Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function path($path){
        $fr = new FrontendRouter($path);
        return $fr->route();
    }
}