<?php


namespace services\Routing;


use Illuminate\Console\Command;
use File;

class RouteClearCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'route:clear';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Clears the routing cache';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $app = app();

        $paths = [
            app_path('routes.php'),
            app_path('routes.admin.php'),
        ];

        foreach($paths as $path){
            if(\Core::isWin()){
                $path = str_replace('/', '\\', $path);
            }
            $app['router']->clearCache($path);
        }

        $this->info('Routing cache cleared!');
    }
}
