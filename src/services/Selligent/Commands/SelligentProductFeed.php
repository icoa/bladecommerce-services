<?php


namespace services\Selligent\Commands;


use Illuminate\Console\Command;
use services\Selligent\ProductFeedService;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SelligentProductFeed extends Command
{
    /**
     * @var ProductFeedService
     */
    protected $importer;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'feed:selligent_product';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create custom Selligent product feed';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->importer = new ProductFeedService();
        $this->importer->setConsole($this);
        $this->info("Executing SelligentProductFeed...");

        $lang = $this->argument('lang');
        $languages = \Mainframe::languagesCodes();
        if ($lang == 'all') {
            foreach ($languages as $language) {
                $this->comment("Generating feed for language [$language]");
                $this->importer->setLang($language);
                $this->importer->make();
            }
        } else {
            if (in_array($lang, $languages)) {
                $this->comment("Generating feed for language [$lang]");
                $this->importer->setLang($lang);
                $this->importer->make();
            }else{
                $this->error("Lang [$lang] is not supported!");
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(array('lang', InputArgument::REQUIRED, 'The required language'),);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}