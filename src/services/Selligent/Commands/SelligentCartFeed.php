<?php


namespace services\Selligent\Commands;


use Carbon\Carbon;
use Illuminate\Console\Command;
use services\Repositories\CartRepository;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class SelligentCartFeed extends Command
{
    /**
     * @var CartRepository
     */
    protected $repository;
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'feed:selligent_cart';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create custom Selligent cart reminder feed. You can pass the start date as first argument';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->repository = new CartRepository();
        $this->repository->setConsole($this);
        $this->info("Executing SelligentCartFeed...");

        $date = $this->argument('date');

        if ($date == '') {
            $date = Carbon::yesterday()->format('Y-m-d');
        }

        $this->comment("Exporting all abandoned carts from $date");

        $this->repository->getAbandonedCarts($date);
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(array('date', InputArgument::OPTIONAL, 'The optional start date (default to yesterday)'),);
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}