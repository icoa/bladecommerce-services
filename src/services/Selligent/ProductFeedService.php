<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 29/01/2019
 * Time: 13:39
 */

namespace services\Selligent;


use DB;
use Product;
use Customer;
use services\Traits\TraitConsole;
use SplFileObject;
use Exception;
use League\Csv\Writer;
use Carbon\Carbon;
use Illuminate\Support\Str;

/**
 * Class ProductFeedService
 * @package services\Selligent
 *
 *
 * ID | Page URL | img URL | Title (nome prodotto) | Sottotitolo (vuoto per ora) | Categoria I | Categoria II | Brand | Collezione | Genere | Prezzo Listino | Prezzo finale | Q.tá
 * | Descrizione | Misure | Canvass | Novitá | Outlet | Colore | Materiale | Simboli | NAV | Gruppi personalizzati (ancora da definire, lasciare vuoto)
 * - La categoria deve essere sia generale (Categoria l) che particolare (Categoria ll);
 * - per le cover 500 x 500
 * - le varianti dovrebbero rappresentare solo l'elemento di varianza (le misure degli anelli) del prodotto master come da esempio
 * - canvass deve essere espresso - NUOVO A PARTIRE DA -
 * - i booleani devono essere espressi come true o false
 * - nav deve essere espresso come multiselect 07,08
 * - gruppi personalizzati li stiamo ancora definendo
 *
 * ESEMPIO:
 * 57937KS | https://www.kronoshop.com/anello-morellato-tesori-saiw42012-P57937.htm | https://cdn.kronoshop.com/i/default/73518/anello-morellato-tesori-saiw42012@2x.jpg | ANELLO MORELLATO TESORI - SAIW42012 |
 * Sottotitolo (vuoto per ora) | Gioielli | Anelli | Brand | Tesori | Donna | 69,00 | 69,00 | 10 | Anello Morellato ...... | 12,14,16,18 | 01-07-2019 | 1 | 0 | bianco | Argento,Cristalli | | | |
 */
class ProductFeedService
{

    use TraitConsole;

    const CSV_DELIMITER = ';';
    const CSV_NEWLINE = "\r\n";

    protected $lang = 'default';
    protected $messages = [];

    /**
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * @param string $lang
     */
    public function setLang($lang)
    {
        $this->lang = $lang;
        \Core::setLang($lang);
    }

    /**
     * Switch connection to perform all queries on Cloud DB
     */
    private function switchConnection()
    {
        $console = $this->getConsole();
        if (\App::environment() == 'live') {
            $dbName = config('database.connections.cloud.database');
            $console->comment("Switching connection to cloud database [$dbName]...");
            DB::disconnect();
            DB::setDefaultConnection('cloud');
            DB::reconnect();
            $console->info('-> DONE');
        }
    }


    function make()
    {
        \Config::set('elasticquent.expand_products', true);
        //audit_watch();
        $console = $this->getConsole();
        //$this->switchConnection();
        $suffix = config('plugins_settings.selligent.suffix');
        if (Str::length($suffix) != 2) {
            $console->error('Selligent id suffix not provided in configuration file');
            return;
        }

        \Core::setLang($this->getLang());

        $ai_headers = [
            'id' => 'ID',
            'page_url' => 'Page URL',
            'image_url' => 'Image URL',
            'sku' => 'SKU',
            'name' => 'Title',
            'alt_name' => 'Subtitle',
            'main_category' => 'Category I',
            'default_category' => 'Category II',
            'brand' => 'Brand',
            'collection' => 'Collection',
            'gender' => 'Gender',
            'list_price' => 'List price',
            'sell_price' => 'Sell price',
            'quantity' => 'Quantity',
            'description' => 'Description',
            'sizes' => 'Sizes',
            'canvass' => 'Canvass',
            'is_new' => 'New',
            'is_outlet' => 'Outlet',
            'color' => 'Color',
            'material' => 'Material',
            'symbol' => 'Symbol',
            'nav' => 'Nav',
            'trends' => 'Custom groups',
        ];

        $headers = [
            'id' => 'WEBSITE_PRODUCT_ID',
            'page_url' => 'PAGE_URL',
            'image_url' => 'IMAGE_URL',
            'sku' => 'SKU',
            'name' => 'TITLE',
            'alt_name' => 'SUBTITLE',
            'main_category' => 'CATEGORY_I',
            'default_category' => 'CATEGORY_II',
            'brand' => 'BRAND',
            'collection' => 'COLLECTION',
            'gender' => 'GENDER',
            'list_price' => 'LIST_PRICE',
            'sell_price' => 'SELL_PRICE',
            'quantity' => 'QUANTITY',
            'description' => 'DESCRIPTION',
            'sizes' => 'SIZES',
            'canvass' => 'CANVASS',
            'is_new' => 'NEW_PRODUCT',
            'is_outlet' => 'OUTLET',
            'color' => 'COLOR',
            'material' => 'MATERIAL',
            'symbol' => 'SYMBOL',
            'nav' => 'NAV',
            'trends' => 'CUSTOM_GROUPS',
        ];

        $lines = [];
        $ai_lines = [];
        $platform = \Cfg::get('SHORTNAME');

        $rows = Product::whereIn('id', function ($query) {
            $query->select('id')->from('cache_products');
        })
            ->inStocks()
            ->onlyAvailableNow() // TODO: revert to false after CoronaVirus
            ->soldout(0)
            ->select('id')
            ->orderBy('id', 'desc')
            ->lists('id');

        $many = count($rows);

        $console->comment("Found ($many) products...");

        $counter = 0;

        $link_template = config('plugins_settings.selligent.product_link_decorator');
        $link_append = config('plugins_settings.selligent.product_link_append');
        $hasLinkTemplate = strlen(trim($link_template)) > 0;
        $hasLinkAppend = strlen(trim($link_append)) > 0;

        if(true === $hasLinkTemplate){
            //these operation create exactly 2 tokens
            $link_template_tokens = explode('%s', $link_template);
            if (true === $hasLinkAppend) {
                //replace first character with '&' instead of '?'
                $link_template_tokens[1][0] = '&';
            }
            $link_template = $link_template_tokens[0];
            $link_append_ai = $link_append . $link_template_tokens[1];
        }else{
            $link_append_ai = $link_append;
        }

        foreach ($rows as $row) {
            $counter++;
            $console->comment("Exporting product [$row] ($counter/$many)");
            /** @var Product $product */
            $product = Product::getProductFeed($row, $this->getLang(), true); // TODO: revert to false after CoronaVirus
            if ($product === null) {
                $console->error("Product [$row] is null!");
                continue;
            }

            $product_url = $hasLinkAppend ? $product->link_absolute . '?' . urlencode(substr($link_append, 1)) : $product->link_absolute;
            $ai_product_url = $product->link_absolute;
            $ai_page_url = ($hasLinkTemplate) ? $link_template . urlencode($ai_product_url) : $ai_product_url;
            $page_url = $product_url;
            $trends = $product->getTrendsCodesAttribute();

            $data = [
                'id' => $product->id . $suffix,
                'page_url' => $page_url,
                'image_url' => \Site::rootify($product->defaultImg),
                'sku' => $product->sku,
                'name' => $product->name,
                'alt_name' => null,
                'main_category' => $product->main_category_name,
                'default_category' => $product->category_name,
                'brand' => $product->brand_name,
                'collection' => $product->collection_name,
                'gender' => $product->gender,
                'list_price' => $product->price_official_raw,
                'sell_price' => $product->price_final_raw,
                'quantity' => $product->qty,
                'description' => $this->getProductDescription($product),
                'sizes' => $this->exportAttributesValues($product, ['size', 'size-cm']),
                'canvass' => $this->formatCanvass($product),
                'is_new' => $this->formatBoolean($product->is_new),
                'is_outlet' => $this->formatBoolean($product->is_outlet),
                'color' => $this->exportAttributesValues($product, ['color', 'strap_colour', 'case_colour', 'primary_colour', 'pearls_color', 'ink_colour', 'diamond_color', 'dial_colour', 'colore_pietra']),
                'material' => $this->exportAttributesValues($product, ['material', 'case_material', 'charms_material', 'strap_material', 'necklace_material']),
                'symbol' => $this->exportAttributesValues($product, ['symbol']),
                'nav' => $this->exportAttributesCodes($product, ['bs-nav']),
                'trends' => $trends,
            ];

            //audit($product, 'product');
            //audit($data, 'data');

            unset($product);
            //gc_collect_cycles();

            $lines[] = $data;
            $data['page_url'] = $ai_page_url;
            $ai_lines[] = $data;
        }


        //audit($headers, __METHOD__, 'HEADERS');
        //audit($lines, __METHOD__, 'LINES');

        $rows = array_merge([$headers], $lines);

        $targetFilePath = $this->getFilePath();

        $writer = Writer::createFromPath(new SplFileObject($targetFilePath, 'w'), 'w');
        $writer->setEncodingFrom('UTF-8');
        $writer->setDelimiter(self::CSV_DELIMITER); //the delimiter will be the tab character
        $writer->setNewline(self::CSV_NEWLINE); //use windows line endings for compatibility with some csv libraries
        $writer->insertAll($rows);

        $this->messages[] = 'Feed generated at => ' . \Site::rootify("/feeds/{$this->getLang()}_selligent_products");

        $rows = array_merge([$ai_headers], $ai_lines);

        $targetFilePath = $this->getAIFilePath();

        $writer = Writer::createFromPath(new SplFileObject($targetFilePath, 'w'), 'w');
        $writer->setEncodingFrom('UTF-8');
        $writer->setDelimiter(self::CSV_DELIMITER); //the delimiter will be the tab character
        $writer->setNewline(self::CSV_NEWLINE); //use windows line endings for compatibility with some csv libraries
        $writer->insertAll($rows);

        $this->messages[] = 'Feed generated at => ' . \Site::rootify("/feeds/{$this->getLang()}_selligent_products_ai");

        return $targetFilePath;
    }

    /**
     * @param $value
     * @return string
     */
    protected function formatBoolean($value)
    {
        return $value == 1 ? 'true' : 'false';
    }

    /**
     * @param Product $product
     * @return null|string
     */
    protected function formatCanvass(Product $product)
    {
        //if ($product->is_new == 0)
        //    return null;

        try {
            $date = Carbon::parse($product->new_from_date);
            return $date->format('d-m-Y');
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @param Product $product
     * @param $attribute_code
     * @return string
     */
    protected function exportAttributesValues(Product $product, $attribute_code)
    {
        $values = [];
        $fallback = function (&$values) use ($product, $attribute_code) {
            audit($attribute_code, 'calling fallback');
            $options = $product->getProductAttributesOptionsByCode($attribute_code);
            foreach ($options as $option) {
                $values[] = $option->name;
            }
        };
        try {
            if ($product->hasRelatedEntity('option_values') and $product->hasRelatedEntity('attributes_codes')) {
                foreach ($product->getRelatedEntity('attributes_codes') as $index => $code) {
                    if (in_array($code, $attribute_code, true)) {
                        $values[] = $product->related_entities['option_values'][$index];
                    }
                }
            } else {
                $fallback($values);
            }
        } catch (Exception $e) {
            $fallback($values);
        }
        return implode(',', $values);
    }

    /**
     * @param Product $product
     * @param $attribute_code
     * @return string
     */
    protected function exportAttributesCodes(Product $product, $attribute_code)
    {
        $values = [];
        $fallback = function (&$values) use ($product, $attribute_code) {
            audit($attribute_code, 'calling fallback');
            $options = $product->getProductAttributesOptionsByCode($attribute_code);
            foreach ($options as $option) {
                $values[] = $option->uname;
            }
        };
        try {
            if ($product->hasRelatedEntity('option_ids') and $product->hasRelatedEntity('attributes_codes')) {
                foreach ($product->getRelatedEntity('attributes_codes') as $index => $code) {
                    if (in_array($code, $attribute_code, true)) {
                        foreach ($product->getRelatedEntity('option_ids') as $option_id) {
                            $option = \AttributeOption::getPublicObj($option_id, $this->getLang());
                            if ($option) {
                                $values[] = $option->uname;
                            }
                        }
                    }
                }
            } else {
                $fallback($values);
            }
        } catch (Exception $e) {
            $fallback($values);
        }
        return implode(',', $values);
    }

    /**P
     * @param Product $product
     * @return string
     */
    protected function getProductDescription(Product $product)
    {
        if ($product->sdesc != '') {
            $description = $product->sdesc;
        } else {
            $description = "$product->name - $product->attributes";
        }
        $description = strip_tags($description);
        $description = html_entity_decode($description, ENT_QUOTES, 'UTF-8');
        $description = trim(preg_replace("/\s+/", ' ', $description));
        return $description;
    }


    /**
     * @return string
     */
    protected function getFilePath()
    {
        $filename = $this->getLang() . '_selligent_products.csv';
        $dir = storage_path('xml/selligent');
        if (!is_dir($dir)) {
            mkdir($dir, 0775);
        }
        return "$dir/$filename";
    }

    /**
     * This should be fetched with
     * es. http://morellato.local/feeds/it_selligent_products
     *
     * @param $file
     * @return string
     */
    static function getPathByFile($file)
    {
        return storage_path("xml/selligent/{$file}csv");
    }


    /**
     * @return string
     */
    protected function getAIFilePath()
    {
        $filename = $this->getLang() . '_selligent_products_ai.csv';
        $dir = storage_path('xml/selligent');
        if (!is_dir($dir)) {
            mkdir($dir, 0775);
        }
        return "$dir/$filename";
    }


    function __destruct()
    {
        foreach ($this->messages as $message) {
            $this->getConsole()->info($message);
        }
    }
}