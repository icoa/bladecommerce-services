<?php namespace services;




class GoogleMerchantBuilder
{
    protected $lang;
    protected $locale;
    protected $filename;

    function __construct($lang,$locale){
        $this->locale = $locale;
        $this->lang = $lang;
        $this->filename = strtolower('googlemerchant_' . "$lang-$locale" . ".xml");
    }


    function batch(){
        $gm = new GoogleMerchantFeed($this->lang,$this->locale);
        try{
            $xml = $gm->export();
            $dir = storage_path("xml/googlemerchant");
            if(!is_dir($dir))mkdir($dir);
            $file = $dir."/".$this->filename;
            \File::put($file,$xml);
            return $xml;
        }catch(\Exception $e){
            \Utils::error($e->getMessage(),__METHOD__);
            \Utils::error($e->getTraceAsString(),__METHOD__);
        }
        return null;
    }


}
