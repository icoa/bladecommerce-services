<?php


namespace services\Strap\Traits;

use Hash;


trait TraitStrapsB2BCustomer
{

    /**
     * Craft a semi-random password with a static pattern
     *
     * @return array
     */
    public function getStrapDefaultPassword()
    {
        $passwd = substr($this->email, 0, 2);
        $passwd .= substr($this->website, -3);
        $passwd .= substr($this->name, 0, 1) . '!';
        $passwd .= substr($this->customer_code, -2);
        $plain = $passwd;
        $md5 = md5($plain);
        $hash = Hash::make($plain);
        return compact('plain', 'md5', 'hash');
    }

    /**
     * @return bool
     */
    function isStrapB2B()
    {
        if(config('app.project') != 'cinturino')
            return false;

        $b2b_group = config('cinturino.b2b_group', 9);
        return $this->hasGroup($b2b_group);
    }

    /**
     * @return bool
     */
    function isStrapB2BDanger()
    {
        if(config('app.project') != 'cinturino')
            return false;

        $b2b_group = config('cinturino.b2b_group_danger', 9);
        return $this->hasGroup($b2b_group);
    }
}