<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 07/11/2018
 * Time: 11:48
 */
namespace services\Strap\Traits;

use Order;

/**
 * Trait TraitOrderStrap
 * @package services\Strap\Traits
 * @mixin Order
 */
trait TraitOrderStrap{

    /**
     * @return bool
     */
    function isStrapB2B(){
        if(config('app.project') != 'cinturino')
            return false;

        $customer = $this->getCustomer();
        if($customer){
            return $customer->isStrapB2B();
        }
        return false;
    }
}