<?php

namespace services\Strap\Repositories;

use services\Morellato\Repositories\Repository;
use Customer;
use Email;
use SplFileObject;
use Exception;
use League\Csv\Writer;
use Input;
use Country;
use State;
use Illuminate\Support\Str;
use services\IsoCodes\ZipCode;

class StrapUserRepository extends Repository
{
    const CSV_DELIMITER = ";";
    const CSV_NEWLINE = "\r\n";

    /**
     * @param Customer $customer
     * @param $errors
     */
    function customLoginErrors(Customer $customer, &$errors)
    {
        if (feats()->enabled('straps')) {
            $b2b_group_danger = config('cinturino.b2b_group_danger');
            if ($customer->default_group_id == $b2b_group_danger) {
                //$errors[] = "Il tuo account non è abilitato ad accedere";
                $errors[] = lex('msg_b2b_login_warning');
                try {
                    $this->sendLoginWarningEmail($customer);
                } catch (Exception $e) {
                    audit_exception($e, __METHOD__);
                }
            }
        }
    }

    /**
     * Generate CSV file
     */
    function generateUsersList()
    {
        $b2b_group_danger = config('cinturino.b2b_group_danger');
        $b2b_group = config('cinturino.b2b_group');

        $customers = Customer::withGroup([$b2b_group_danger, $b2b_group])->get();

        $rows = [
            ['CustomerCode', 'VAT', 'Name', 'Email', 'Password', 'Danger']
        ];

        foreach ($customers as $customer) {
            $passwords = $customer->getStrapDefaultPassword();
            $danger = $customer->default_group_id == $b2b_group_danger ? 'Y' : 'N';
            $rows[] = [$customer->customer_code, $customer->website, $customer->name, $customer->email, $passwords['plain'], $danger];
        }

        $targetFilePath = storage_path('b2b_customers.csv');

        $writer = Writer::createFromPath(new SplFileObject($targetFilePath, 'w'), 'w');
        $writer->setEncodingFrom('UTF-8');
        $writer->setDelimiter(self::CSV_DELIMITER); //the delimiter will be the tab character
        $writer->setNewline(self::CSV_NEWLINE); //use windows line endings for compatibility with some csv libraries
        $writer->insertAll($rows);
    }

    /**
     * @param Customer $customer
     */
    private function sendLoginWarningEmail(Customer $customer)
    {
        $email = Email::getByCode('LOGIN_WARNING');
        $email_data = [
            'CUSTOMER_EMAIL' => $customer->email,
            'CUSTOMER_NAME' => $customer->name,
        ];
        $email->setCustomer($customer)->send($email_data, config('cinturino.b2b_login_warnings', 'f.politi@icoa.it'));
    }


    /**
     * @return array
     */
    function getRequiredRegisterDataB2B()
    {
        $data = Input::all();
        $profile = Input::get('profile');
        $marketing = Input::get('marketing');

        $errors = [];

        if ($profile == null) {
            $errors[] = trans("ajax.personal_consent_data");
        }
        if ($marketing == null) {
            $errors[] = trans("ajax.personal_consent_marketing");
        }

        if (!\Utils::isEmail($data['email'])) {
            $errors[] = trans("ajax.email_must_valid");
        }

        $required = [
            'email' => trans("ajax.email_address"),
        ];

        $required['company'] = trans("ajax.company");
        $required['firstname'] = trans("ajax.firstname");
        $required['lastname'] = trans("ajax.lastname");

        $use_address = Input::get('use_address') == '1';

        if ($use_address) {

            $required += [
                'address1' => trans("ajax.address"),
                'address2' => trans("ajax.address2"),
                'postcode' => trans("ajax.postcode"),
                'city' => trans("ajax.city"),
                'country_id' => trans("ajax.country"),
                'state_id' => trans("ajax.state"),
                'phone' => trans("ajax.phone"),
                'vat' => trans("ajax.vat"),
                'rea' => trans("ajax.rea"),
            ];

        }

        if (isset($data['country_id'])) {
            $country = Country::getObj($data['country_id']);
            if ($country AND $country->hasStates() == false) {
                unset($required['state_id']);
            }
            if ($country and $country->iso_code and $data['postcode'] != '') {
                $valid = ZipCode::validate($data['postcode'], $country->iso_code);
                if ($valid === false) {
                    $errors[] = trans("validation.zip", ['attribute' => trans("ajax.postcode")]);
                }
            }
            if (is_null($country)) {
                if (isset($required['country_id']))
                    unset($required['country_id']);

                if (isset($required['state_id']))
                    unset($required['state_id']);
            }
        }

        foreach ($required as $key => $field) {
            if (trim($data[$key]) == '') {
                $errors[] = trans("ajax.mandatory_field", ['field' => $field]);
            }
        }

        $email = Str::lower(trim($data['email']));

        if (Str::length($email) > 0 and !$this->isEmailUnique($email)) {
            $errors[] = trans("ajax.email_used");
        }

        if (feats()->enabled('recaptcha') and count($errors) == 0) {
            $response = Input::get('g-recaptcha-response');
            $validRecaptcha = \Recaptcha::verifyResponse($response, 'register');
            if ($validRecaptcha == false) {
                $errors[] = trans("ajax.recaptcha");
            }
        }

        return $errors;
    }

    /**
     * @param $email
     * @return bool
     */
    protected function isEmailUnique($email, $id = null)
    {
        $builder = Customer::whereEmail($email)->where("active", 1);
        if ($id) {
            $builder->where('id', '!=', $id);
        }
        $customer = $builder->first();
        return is_null($customer);
    }

    function sendRegisterB2bEmail()
    {
        $email = Email::getByCode('REGISTER_B2B');
        $details = '<table cellpadding="4" cellspacing="4">';

        $fields = [
            'email' => trans("ajax.email_address"),
            'company' => trans("ajax.company"),
            'firstname' => trans('custom.b2b.register.firstname'),
            'lastname' => trans('custom.b2b.register.lastname'),
            'address1' => trans("ajax.address"),
            'address2' => trans("ajax.address2"),
            'postcode' => trans("ajax.postcode"),
            'city' => trans("ajax.city"),
            'country_id' => trans("ajax.country"),
            'state_id' => trans("ajax.state"),
            'phone' => trans("ajax.phone"),
            'vat' => trans("ajax.vat"),
            'cf' => trans("ajax.cf"),
            'rea' => trans("ajax.rea"),
            'extrainfo' => trans("ajax.extrainfo"),
            'phone_mobile' => trans("ajax.phone_mobile"),
            'profile' => 'Consenso Profilazione',
            'marketing' => 'Consenso Comunicazione',
        ];

        foreach ($fields as $key => $name) {
            $value = Input::get($key);

            if ($key == 'country_id') {
                $country = Country::getObj($value);
                if ($country)
                    $value = $country->name;
            }

            if ($key == 'state_id') {
                $state = State::getObj($value);
                if ($state)
                    $value = $state->name;
            }

            if (in_array($key, ['profile', 'marketing'])) {
                $value = ($value == 1) ? 'ACCONSENTO' : 'NON ACCONSENTO';
            }

            $value = Str::upper(trim($value));

            $details .= "<tr><th style='text-align: right' align='right'>$name</th><td>$value</td></tr>";
        }

        $details .= '</table>';

        \Config::set('mail.pretend', false);

        $email->send(compact('details'), config('cinturino.b2b_register_recipient', 'f.politi@icoa.it'));
    }
}