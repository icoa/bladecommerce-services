<?php  namespace services;


use Order;
use Utils;
use Cfg;
use DB;
use Format;


class FactoryConfig
{

    static $keyName = 'GOOGLE_REMARKETING_CONFIG';
    static $config;

    static function get($key,$default = null){
        $config = static::load();
        if(isset($config) AND isset($config->$key) AND $config->$key != null){
            return $config->$key;
        }
        return $default;
    }

    static function getByLang($lang,$key,$default = null){
        $config = static::load();
        if(isset($config) AND isset($config->{$key.'_'.$lang}) AND $config->{$key.'_'.$lang} != null){
            return $config->{$key.'_'.$lang};
        }
        return $default;
    }

    static function set($key,$value){
        $config = static::load();
        $config->$key = $value;
        \Utils::log($config,__METHOD__);
        static::save();
    }

    static function setMultiple($data){
        $config = static::load();
        foreach($data as $key => $value){
            $config->$key = $value;
        }
        \Utils::log($config,__METHOD__);
        static::save();
    }

    static function save(){
        $config = static::load();
        $keyName = static::getKeyName();
        Utils::log($keyName,"SAVING AS");
        Cfg::save($keyName,serialize($config));
        static::load(true);
    }

    static function load($forceReload = false){
        if(!$forceReload AND static::$config){
            return static::$config;
        }
        $keyName = static::getKeyName();
        Utils::log($keyName,"LOADING AS");
        $str = Cfg::get($keyName,null);
        if($str AND $str != ''){
            static::$config = unserialize($str);
        }else{
            static::$config =  new \stdClass();
        }
        return static::$config;
    }

    static function getKeyName(){
        return static::$keyName;
    }

    static function clean(){
        $keyName = static::getKeyName();
        Cfg::save($keyName,null);
    }

}