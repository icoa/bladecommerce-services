<?php

namespace services\Bluespirit\Commands;

use Illuminate\Console\Command;
use services\Bluespirit\Plugins\Instagram\InstagramRepository;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class ExcelImport extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'excel:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Execute the Excel importer. Option: --only-prices=1 will actually update only the prices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing Excel Importer...");

        $handler = \App::make('services\Bluespirit\Importers\Excel\ExcelRepository', [$this]);
        $onlyPrices = $this->option('only-prices') == '1';
        if ($onlyPrices) {
            $this->comment("Importing only prices...");
        }
        $handler->setSource($this->argument('source'))->setOnlyPrices($onlyPrices)->make();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('source', InputArgument::REQUIRED, 'The source to import from'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('only-prices', null, InputOption::VALUE_OPTIONAL, 'Tell the parser to update only prices for products and combinations.', 'no'),
        );
    }

}