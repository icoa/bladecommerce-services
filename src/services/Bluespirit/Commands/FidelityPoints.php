<?php

namespace services\Bluespirit\Commands;

use Illuminate\Console\Command;
use Core;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FidelityPoints extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'fidelity:points';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Perform an action to the points of the given fidelity card.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //testing vouchers
        //000574005742334 000656006562500 000636006361545
        $this->info("Executing Fidelity Points...");

        $code = $this->argument('code');
        $points = (int)$this->argument('points');
        $action = $this->option('action');

        $this->comment("Card: $code | Action: $action");
        if (!in_array($action, ['check', 'give', 'take'])) {
            $this->error("Unrecognized action [$action]. Please use 'check', 'give' or 'take'");
            return;
        }
        if (strlen($code) != 13) {
            $this->error("The code must be 13 chars sized.");
            return;
        }
        if (in_array($action, ['give', 'take']) and $points <= 0) {
            $this->error("The points must be a positive integer.");
            return;
        }

        $customer_code = null;
        $repository = fidelityRepository();
        $response = $repository
            ->setParam('card_code', $code)
            ->check();

        if ($response and isset($response->customer_code) and $response->customer_code != '') {
            $customer_code = $response->customer_code;
            $card_code = $response->card_code;
            $customer = fidelityCustomer(compact('customer_code', 'card_code'));
        } else {
            $this->error("Cound not find any fidelity Customer with the given code");
            return;
        }


        if ($action == 'check') {
            $points = $customer->getPoints();
            if (is_null($points)) {
                $this->error("Cannot determine remote fidelity customer");
            } else {
                $this->info("This card has $points points");
            }
        }

        if ($action == 'give') {
            $this->line("Adding $points points to this card...");
            $points = $customer->addPoints($points);
            if ($points) {
                $this->info("OK! => Now this card has $points points");
            } else {
                $this->error("Could not update points to this card");
            }
        }

        if ($action == 'take') {
            $this->line("Removing $points points to this card...");
            $points = $customer->usePoints($points);
            if ($points) {
                $this->info("OK! => Now this card has $points points");
            } else {
                $this->error("Could not update points to this card");
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('code', InputArgument::REQUIRED, 'The required card code'),
            array('points', InputArgument::OPTIONAL, 'The points to take or give'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('action', null, InputOption::VALUE_OPTIONAL, 'Determine the possible actions (check, give, take).', 'check'),
        );
    }

}