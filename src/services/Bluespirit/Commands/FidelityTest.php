<?php

namespace services\Bluespirit\Commands;

use Illuminate\Console\Command;
use services\Bluespirit\Plugins\Instagram\InstagramRepository;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class FidelityTest extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'fidelity:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test the connection with the fidelity rest web service.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing Fidelity Test...");

        $customer_code = 4397703402;
        $card_code = 9943977034023;
        $email = 'f.politi@m.icoa.it';

        /*
         v.derubeis
         [customer_code] => 4320957553
         [card_code] => 9943209575539

         f.politi
         [customer_code] => 4397703402
         [card_code] => 9943977034023

         d.bonanno
         [customer_code] => 4397703403
         [card_code] => 9943977034030

         * */

        $repository = fidelityRepository();
        $repository->setConsole($this);

        $this->comment("Testing function CHECK with card_code [$card_code]");
        $response = $repository->resetParams()->setParam('card_code', $card_code)->check();
        if (is_null($response)) {
            $this->error("Test failed");
        } else {
            print_r($response);
            $this->info("Test resolved successfully");
        }


        $this->comment("Testing function CHECK with email [$email]");
        $response = $repository->resetParams()->setParam('email', $email)->check();
        if (is_null($response)) {
            $this->error("Test failed");
        } else {
            print_r($response);
            $this->info("Test resolved successfully");
        }


        $this->comment("Testing function CHECK with customer_code [$customer_code]");
        $response = $repository->resetParams()->setParam('customer_code', $customer_code)->check();
        if (is_null($response)) {
            $this->error("Test failed");
        } else {
            print_r($response);
            $this->info("Test resolved successfully");
        }


        $this->comment("Testing function GET POINTS with customer_code [$customer_code]");
        $customer = fidelityCustomer(['customer_code' => $customer_code]);
        $points = $customer->getPoints();
        if (is_null($points)) {
            $this->error("Test failed");
        } else {
            $this->line("The customer has $points points");
            $this->info("Test resolved successfully");
        }

        return;

        $this->comment("Testing function ADD POINTS with customer_code [$customer_code]");
        $customer = fidelityCustomer(['customer_code' => $customer_code]);
        $points = $customer->addPoints(25);
        if (is_null($points)) {
            $this->error("Test failed");
        } else {
            $this->line("The customer now has $points points");
            $this->info("Test resolved successfully");
        }

        $this->comment("Testing function USE POINTS with customer_code [$customer_code]");
        $customer = fidelityCustomer(['customer_code' => $customer_code]);
        $points = $customer->usePoints(5);
        if (is_null($points)) {
            $this->error("Test failed");
        } else {
            $this->line("The customer now has $points points");
            $this->info("Test resolved successfully");
        }

        $this->comment("Testing function GET POINTS(forced) with customer_code [$customer_code]");
        $customer = fidelityCustomer(['customer_code' => $customer_code]);
        $points = $customer->getPoints(true);
        if (is_null($points)) {
            $this->error("Test failed");
        } else {
            $this->line("The customer has $points points");
            $this->info("Test resolved successfully");
        }

        $this->comment("Testing function LOAD with customer_code [$customer_code]");
        $customer = fidelityCustomer(['customer_code' => $customer_code]);
        $loaded = $customer->load();
        if ($loaded == false) {
            $this->error("Test failed");
        } else {
            $this->line((string)$customer);
            $this->info("Test resolved successfully");
        }

        if ($loaded) {
            $phonenumber = '+39 06 45445102';
            $this->comment("Testing function UPDATE with customer_code [$customer_code]");
            $customer->setAttribute('phonenumber', $phonenumber);
            $customer->setAttribute('email', 'f.politi@m.icoa.it');
            $customer->setAttribute('birthdate', '1983-01-14');
            $customer->setAttribute('province', 'RM');
            $customer->save();

            //reload the customer from the web service
            $customer->load();
            if ($customer->getAttribute('phonenumber') == $phonenumber) {
                $this->info("Test resolved successfully");
            } else {
                $this->error("Test has not been passed");
            }
        }

        $this->comment("Testing function CUSTOMERS_CHANGED...");
        $customers = $repository->resetParams()->all('2017-01-01');
        foreach ($customers as $customer) {
            print_r($customer->toArray());
        }

        $insert = $this->option('insert');
        if ($insert) {
            $this->comment("Inserting a new customer...");

            /*$email = 'f.politi@m.icoa.it';
            $params = [
                'email' => $email,
                'name' => 'Fabio',
                'surname' => 'Politi',
                'address' => 'Via Acerra, 40',
                'zipcode' => '00052',
                'city' => 'Cerveteri',
                'country' => 'ITA',
                'gender' => 'M',
            ];*/
            $email = 'd.bonanno@m.icoa.it';
            $params = [
                'email' => $email,
                'name' => 'Daniel',
                'surname' => 'Bonanno',
                'address' => 'L.go Sasso, 2',
                'zipcode' => '00052',
                'city' => 'Cerveteri',
                'country' => 'ITA',
                'gender' => 'M',
            ];

            $customer = fidelityCustomer();
            $customer->setAttributes($params);
            $response = $customer->save();
            $this->line('RESPONSE RETURNED');
            print_r($response);

            //reload the customer from the web service
            $customer->load();
            if ($customer->getAttribute('email') == $email) {
                $this->info("Test resolved successfully");
            } else {
                $this->error("Test has not been passed");
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('insert', null, InputOption::VALUE_OPTIONAL, 'Try to create a new customer', null),
            array('points', null, InputOption::VALUE_OPTIONAL, 'Try to create a transaction in points.', null),
        );
    }

}