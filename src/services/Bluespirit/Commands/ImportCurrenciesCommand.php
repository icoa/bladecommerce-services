<?php

namespace services\Bluespirit\Commands;

use Illuminate\Console\Command;
use services\Bluespirit\Importers\Currencies\CurrencyImporter;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ImportCurrenciesCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'currencies:import';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import multi-currency catalog';

    /**
     * @var CurrencyImporter
     */
    protected $importer;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->importer = app(CurrencyImporter::class);
        $this->importer->setConsole($this);
        $this->info("Executing Currencies importer...");
        $this->importer->make();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */

    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}
