<?php
namespace services\Bluespirit\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UpsertEntitiesCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'entities:upsert';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Insert entities data from temp tables';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info("Executing Upsert Entities...");
        $importer = \App::make('services\Bluespirit\Importers\Entities\UpsertEntities',[$this]);
        $importer->setMode($this->option('mode'));
        $importer->make();
        unset($importer);
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
		 array('mode', null, InputOption::VALUE_OPTIONAL, 'Mode of usability, by default is basic, advanced is possible.', 'basic'),
		);
	}

}
