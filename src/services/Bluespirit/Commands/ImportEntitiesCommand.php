<?php

namespace services\Bluespirit\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class ImportEntitiesCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'entities:import';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import excel entity files into temporary database tables.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info("Executing CSV importer...");
        $entities = Config::get('entities.file_names');

        foreach($entities as $entity){
            $importer = \App::make('services\Bluespirit\Importers\Entities\\'.$entity.'Entity',[$this]);
            $importer->setPath($this->argument('path'));
            $importer->make();
            unset($importer);
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */

	protected function getArguments()
	{
		return array(
			array('path', InputArgument::REQUIRED, 'path relative to the storage_path'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			// array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}
