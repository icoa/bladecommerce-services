<?php

namespace services\Bluespirit\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class EntitiesCheckerCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'entities:check';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Check entity tables .';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info("Executing Entities Checker...");
		
        $entities = Config::get('entities.file_names');

        foreach($entities as $entity){
            $importer = \App::make('services\Bluespirit\Importers\Entities\\'.$entity.'Entity',[$this]);
            $importer->checkEntity();
            unset($importer);
        }
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */

	protected function getArguments()
	{
		return array(
			
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(
			
		);
	}

}
