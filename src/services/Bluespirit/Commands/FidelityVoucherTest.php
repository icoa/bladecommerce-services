<?php

namespace services\Bluespirit\Commands;

use Illuminate\Console\Command;
use Core;

class FidelityVoucherTest extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'fidelity:test-voucher';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test the result for a fixed voucher code.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing Fidelity Voucher Test...");

        $voucher = '000942009420557';
        //$voucher = '9900488';
        $voucher = '000990009900488';
        try{
            $this->comment("Testing voucher [$voucher]");
            $conn = Core::getNegoziandoConnection();
            $conn->setConsole($this);
            $response = $conn->getVoucher($voucher);
            if(is_null($response)){
                $this->error("The voucher [$voucher] has not been found in Negoziando");
            }else{
                print_r($response);
                $this->info("-> OK, voucher [$voucher] is fine");
            }

            $this->line('Testing exec');
            $voucher = '000656006562500';
            $response = $conn->redeemVoucher($voucher);
            if($response){
                $this->info("Stored procedure executed");
            }else{
                $this->error("Cannot execute Stored procedure");
            }
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}