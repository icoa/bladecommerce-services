<?php

namespace services\Bluespirit\Commands;

use Illuminate\Console\Command;
use Core;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class FidelityVoucher extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'fidelity:voucher';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Perform an action to the given fidelity voucher.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //testing vouchers
        //000574005742334 000656006562500 000636006361545
        $this->info("Executing Fidelity Voucher...");

        $voucher = $this->argument('voucher');
        $action = $this->option('action');

        $this->comment("Voucher: $voucher | Action: $action");
        if (!in_array($action, ['check', 'use', 'redeem'])) {
            $this->error("Unrecognized action [$action]. Please use 'check', 'use' or 'redeem'");
            return;
        }
        if (strlen($voucher) != 15) {
            $this->error("The voucher must be 15 chars sized.");
            return;
        }

        $conn = Core::getNegoziandoConnection();
        $conn->setConsole($this);

        if ($action == 'check') {
            $response = $conn->getVoucher($voucher);
            if (is_null($response)) {
                $this->error("The voucher [$voucher] has not been found in Negoziando");
            } else {
                print_r($response);
                $this->info("-> OK, voucher [$voucher] is fine");
            }
        }

        if ($action == 'use') {
            $response = $conn->useVoucher($voucher);
            if ($response) {
                $this->info("Stored procedure [USE] executed");
            } else {
                $this->error("Cannot execute Stored procedure [USE]");
            }
        }

        if ($action == 'redeem') {
            $response = $conn->redeemVoucher($voucher);
            if ($response) {
                $this->info("Stored procedure [REDEEM] executed");
            } else {
                $this->error("Cannot execute Stored procedure [REDEEM]");
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('voucher', InputArgument::REQUIRED, 'The required voucher code'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('action', null, InputOption::VALUE_OPTIONAL, 'Determine the possible actions (check, use, revert).', 'check'),
        );
    }

}