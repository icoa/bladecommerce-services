<?php

namespace services\Bluespirit\Commands;

use Illuminate\Console\Command;
use services\Bluespirit\Plugins\Instagram\InstagramRepository;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class InstagramFetch extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'instagram:fetch';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run a service that download and cache instagram resources.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing Instagram Fetch...");
        $handler = \App::make('services\Bluespirit\Plugins\Instagram\InstagramRepository',[$this]);
        $handler->make();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}