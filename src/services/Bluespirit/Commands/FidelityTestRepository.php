<?php

namespace services\Bluespirit\Commands;

use Illuminate\Console\Command;
use services\Bluespirit\Negoziando\Fidelity\FidelityCustomerRepository;
use services\Bluespirit\Plugins\Instagram\InstagramRepository;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class FidelityTestRepository extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'fidelity:testrep';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Test the fidelity customer repository.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->info("Executing Fidelity Repository Test...");
        $task = $this->argument('task');

        $method = 'task_' . $task;
        if(method_exists($this, $method)){
            $this->$method();
        }else{
            $this->error("No task [$task] has been found...");
        }

    }


    private function task_test(){
        $this->comment('Executing task [test]');
        /*
         v.derubeis
         [customer_code] => 4320957553
         [card_code] => 9943209575539

         f.politi
         [customer_code] => 4397703402
         [card_code] => 9943977034023

         d.bonanno
         [customer_code] => 4397703403
         [card_code] => 9943977034030

         * */

        $customer_code = 4397703402;
        $card_code = 9943977034023;
        $email = 'f.politi@m.icoa.it';
        $customer_id = 1;

        $repository = new FidelityCustomerRepository();
        $repository->setCustomerId($customer_id);

        //check if Fidelity customer exists
        $this->comment("Check if Fidelity customer exists");
        $exists = $repository->exists();
        $this->line($exists ? "The fidelity customer exists" : "The fidelity customer DOES NOT exist");

        $this->comment("Insert a new Fidelity customer");
        try {
            $response = $repository->insertFidelityCustomer();
            print_r($response->getAttributes());
        } catch (\Exception $e) {
            $this->error($e->getMessage());
        }
    }



    private function task_sync_customers(){
        $this->comment('Executing task [sync_customers]');
        $repository = new FidelityCustomerRepository();
        $repository->setConsole($this);
        $repository->syncCustomers();
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('task', InputArgument::REQUIRED, 'The task to perform'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('insert', null, InputOption::VALUE_OPTIONAL, 'Try to create a new customer', null),
            array('points', null, InputOption::VALUE_OPTIONAL, 'Try to create a transaction in points.', null),
        );
    }

}