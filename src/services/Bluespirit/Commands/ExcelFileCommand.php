<?php

namespace services\Bluespirit\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ExcelFileCommand extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'excelFile:import';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Import excel files into temporary tables, if --source=watches it will import watches temp. table else the jewels temp. table.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
		$this->info("Executing Excel File Importer...");

        $handler = \App::make('services\Bluespirit\Importers\ExcelFile\ExcelFileRepository', [$this]);
        $handler->setSource($this->argument('source'))->make();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array(
		array('source', InputArgument::REQUIRED, 'The source to import from'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array(

		);
	}

}
