<?php

namespace services\Bluespirit\Frontend;

use HtmlObject\Element;
use HtmlObject\Image;
use stdClass;
use Banner;

class FrontendGridHelper
{
    protected $row_counter = 0;
    protected $col_counter = 0;
    protected $banner_counter = 0;
    protected $col_total = 0;

    protected $tree = [];

    protected $lazyLoad = true;


    function __construct($data, $default = [])
    {

        $obj = $data == '' ? $default : unserialize($data);

        $treeRows = [];
        if (is_array($obj)) {

            foreach ($obj as $row) {

                $this->row_counter++;

                $treeRow = new stdClass();
                $treeRow->id = $row->id;
                $treeRow->columns = [];
                $treeRow->counter = $this->row_counter;


                $blocks = $row->blocks;

                if (is_array($blocks)) {
                    foreach ($blocks as $block) {

                        $this->col_counter++;

                        $treeColumn = new stdClass();
                        $css = $this->get_classes($block);
                        $treeColumn->css = $css;
                        $treeColumn->banners = [];
                        $treeColumn->counter = $this->col_counter;



                        $boxes = $block->boxes;

                        if (is_array($boxes)) {
                            foreach ($boxes as $box) {
                                $this->banner_counter++;
                                $treeBanner = $this->getBanner($box->id);
                                $treeColumn->banners[] = $treeBanner;
                            }
                        }

                        $treeRow->columns[] = $treeColumn;

                    }
                    $this->col_total = count($blocks);
                }

                $total = count($treeRow->columns);
                $classes = ['row', "row-index-$treeRow->id", "row-with-$total", "row-item-" . $this->row_counter];
                $css = implode(' ', $classes);
                $treeRow->css = $css;

                $treeRows[] = $treeRow;

            }
        }

        $this->tree = $treeRows;
    }

    /**
     * @param bool $enabled
     * @return $this
     */
    function setLazyLoad($enabled = true){
        $this->lazyLoad = $enabled;
        return $this;
    }


    function getTree()
    {
        return $this->tree;
    }


    function getBanner($id)
    {
        $banner = Banner::getObj($id);
        if ($banner) {

            $banner->src = ($banner->image == "") ? "/media/no.gif" : $banner->image;

            $banner->altText = $banner->input;
            $banner->btnText = $banner->input_alt;
            $banner->html = $banner->input_textarea;
            if ($banner->altText == '') {
                $banner->altText = strip_tags($banner->html);
            }

            $meta = (object)\Site::imageMeta($banner->src);

            $ratio = 1.25;
            $defaultSrc = null;
            $smallWidth = null;
            $smallHeight = null;

            if ($meta->width > 0 AND $meta->height > 0) {
                $smallWidth = ceil($meta->width / $ratio);
                $smallHeight = ceil($meta->height / $ratio);
                $injectDefault = "/images/5a/$smallWidth/$smallHeight";
                $injectBanner = "/images/5a/$meta->width/$meta->height";
                if (\Str::contains($banner->src, '.gif')) {
                    $injectDefault = null;
                    $injectBanner = null;
                }
                $defaultSrc = $banner->src != '' ? \FrontTpl::img($banner->src, false, $injectDefault) : null;
                $banner->src = $banner->src != '' ? \FrontTpl::img($banner->src, false, $injectBanner) : null;
            }

            $simpleImg = Image::create($banner->src, $banner->altText, ['class' => 'img-responsive', 'width' => $meta->width, 'height' => $meta->height]);

            if ($defaultSrc) {
                $default = '<!--[if IE 9]><video style="display: none"><![endif]-->';
                if($this->lazyLoad){
                    $default .= "<source data-srcset='$defaultSrc' media='(max-width: 1200px)'>";
                    $default .= "<source data-srcset='$banner->src' media='(min-width: 1201px)'>";
                }else{
                    $default .= "<source srcset='$defaultSrc' media='(max-width: 1200px)'>";
                    $default .= "<source srcset='$banner->src' media='(min-width: 1201px)'>";
                }

                if($this->lazyLoad){
                    $default .= Image::create("data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==", $banner->altText, [
                        'class' => 'img-responsive unveil',
                        'width' => $meta->width,
                        'height' => $meta->height,
                        'data-src' => $banner->src,
                    ]);
                }else{
                    $default .= Image::create($banner->src, $banner->altText, [
                        'class' => 'img-responsive',
                        'width' => $meta->width,
                        'height' => $meta->height,
                    ]);
                }

                $banner->imgElement = Element::create('picture', $default, [
                    'class' => 'img-responsive',
                ]);

            } else {
                $banner->imgElement = Image::create($banner->src, $banner->altText, ['class' => 'img-responsive', 'width' => $meta->width, 'height' => $meta->height]);
            }

            if($this->lazyLoad){
                $banner->imgElement .= "<noscript>$simpleImg</noscript>";
            }

            $link = new \LinkResolver($banner->content);
            $banner->url = $link->getUrl();
            $params = unserialize($banner->params);
            $banner->css = $params->banner_css;
            $banner->prepend = $params->prepend;
            $banner->append = $params->append;


        }
        return $banner;
    }


    function get_classes($block)
    {
        $classes = [];
        if ($block->lg != '') {
            $classes[] = ($block->lg == 'hidden') ? "hidden-lg" : "col-lg-" . $block->lg;
        }
        if ($block->md != '') {
            $classes[] = ($block->md == 'hidden') ? "hidden-md" : "col-md-" . $block->md;
        }
        if ($block->sd != '') {
            $classes[] = ($block->sd == 'hidden') ? "hidden-sm" : "col-sm-" . $block->sd;
        }
        if ($block->xs != '') {
            $classes[] = ($block->xs == 'hidden') ? "hidden-xs" : "col-xs-" . $block->xs;
        }
        if ($block->css != '') {
            $classes[] = $block->css;
        }
        $classes[] = "col-item-" . $this->col_counter;
        return implode(" ", $classes);
    }

    function front_box($id)
    {
        $tpl = '';
        try {
            $banner = Banner::getObj($id);
            if ($banner->published == 0)
                return null;

            $params = unserialize($banner->params);
            $src = ($banner->image == "") ? "/media/no.gif" : $banner->image;

            if (!file_exists(public_path($banner->image))) {
                $src = "/media/no.gif";
            }

            $altText = $banner->input;
            $btnText = $banner->input_alt;
            $bannerText = $banner->input_textarea;
            $jsCode = $banner->input_textarea_alt;
            $box = Element::create("div", null, ['class' => 'box']);

            $meta = (object)\Site::imageMeta($src);

            if ($meta->width > 0 AND $meta->height > 0) {
                $inject = "/images/5a/$meta->width/$meta->height";
                if (\Str::contains($src, '.gif')) {
                    $inject = null;
                }
                $src = $src != '' ? FrontTpl::img($src, false, $inject) : null;
            }

            $img = Image::create($src, strip_tags($bannerText), ['class' => 'banner', 'width' => $meta->width, 'height' => $meta->height]);

            if (isset($params->banner_css) AND $params->banner_css != '') {
                $box->addClass($params->banner_css);
            }

            if (isset($params->banner_id) AND $params->banner_id != '') {
                $box->setAttribute("id", $params->banner_id);
            }

            $link = new \LinkResolver($banner->content);
            $anchor = $link->getAnchor();
            $anchor->addClass("overlay");
            $overlay = "";


            if ($bannerText != '' OR $btnText != '') {
                $overlay = <<<HTML
<span class="overlayContainer">
    <span class="overlayAligner">
        <span class="headline">$bannerText</span>
        <span class="ctabutton"><span>$btnText</span></span>
    </span>
</span>
HTML;
            }
            $anchor->nest($overlay);
            $box->nest($img);
            $box->nest($anchor);

            return $box;
        } catch (\Exception $e) {

        }

        return $tpl;
    }


    function block($block, $boxes = '')
    {

        $div = Element::create("div", $boxes, ['class' => self::get_classes($block)]);

        return $div;
    }

    function row($row_index, $content)
    {
        $total = $this->col_total;
        $classes = ['row', "row-index-$row_index", "row-with-$total", "row-item-" . $this->row_counter];
        $css = implode(' ', $classes);
        $tpl = <<<TPL
<div class="$css">
    <div class="block-content">
        <div class="custom-block">
            $content
        </div>
        <div class="clear clr"></div>
    </div>
</div>
TPL;

        return $tpl;
    }
}