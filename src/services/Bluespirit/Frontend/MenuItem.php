<?php

namespace services\Bluespirit\Frontend;


use Carbon\Carbon;
use HtmlObject\Element;
use HtmlObject\Image;
use HtmlObject\Traits\Tag;
use Underscore\Types\Arrays;
use Core\Param;

class MenuItem extends Param
{

    protected $obj;
    protected $type;
    protected $params;
    protected $parent;
    protected $url;
    protected $anchor;
    protected $submenu;
    public $elements;
    public $id;
    public $position;
    public $deep;

    function __construct($obj, $parent = null)
    {
        $this->obj = $obj;
        $this->id = $obj->id;
        $this->params = unserialize($this->obj->params);
        $this->parent = $parent;
        $this->position = [];
        $this->setup();
    }

    function setup()
    {
        $obj = $this->obj;
        $this->type = $obj->link_type;
        switch ($obj->link_type) {

            case 'container':
                $this->anchor = $this->getContainer();
                if ($obj->inherit_context AND $this->parent) {
                    $this->obj->link_params = $this->parent->obj->link_params;
                }
                break;

            case 'auto':

                break;

            default:
                $linkObj = new \LinkResolver($obj->link_params, false);
                if ($obj->inherit_context AND $this->parent) {
                    //\Utils::log($this->parent->obj->link_params,"HAS GLOBAL PARAMS");
                    $linkObj->setGlobalParams($this->parent->obj->link_params);
                }
                $linkObj->render();
                $anchor = $linkObj->getAnchor();
                $this->anchor = $anchor;
                break;
        }


    }

    function getLinkParams()
    {
        return $this->obj->link_params;
    }

    private function getContainer()
    {
        $params = $this->params;
        $container = Element::div("",['class' => 'content-column menu-column row-grid-1']);
        $responsive = $this->getParam("responsive", 0);
        $responsive = false;
        if ($responsive) {
            $container->addClass($params->responsive_lg);
            $container->addClass($params->responsive_lg);
            $container->addClass($params->responsive_md);
            $container->addClass($params->responsive_sm);
            $container->addClass($params->responsive_xs);
        }
        return $container;
    }


    function getType(){
        return $this->type;
    }


    private function getDomList()
    {
        $lang = \Core::getLang();
        $obj = $this->obj;
        $params = $this->params;
        $linkObj = new \LinkResolver($obj->link_params, false);
        if ($obj->inherit_context AND $this->parent) {
            //\Utils::log($this->parent->obj->link_params,"HAS GLOBAL PARAMS");
            $linkObj->setGlobalParams($this->parent->obj->link_params);
        }
        $linkObj->render();
        $items = $linkObj->getAnchor();
        $nodes = [];
        $counter = 0;
        $max_items = count($items);
        if($max_items > 0){
            foreach ($items as $anchor) {
                $counter++;
                if ($this->getParam("link_css")) {
                    $anchor->addClass($params->link_css);
                }

                if ($this->getParam("prepend_text_$lang")) {
                    $text = $this->getParam("prepend_text_$lang") . " " . $anchor->getValue();
                    $anchor->setValue($text);
                }
                if ($this->getParam("append_text_$lang")) {
                    $text = $anchor->getValue() . " " . $this->getParam("append_text_$lang");
                    $anchor->setValue($text);
                }

                $node = Element::li($anchor);
                if ($this->getParam("icon_css")) {
                    $icon = Element::create(\Config::get('mobile',false) === true ? 'i' : 'span', "", ['class' => $params->icon_css]);
                    $anchor->setValue($icon . $anchor->getValue());
                }

                if ($counter == 1 AND in_array('first', $this->position)) {
                    $node->addClass('first');
                }
                if ($counter == $max_items AND in_array('last', $this->position)) {
                    $node->addClass('last');
                }

                $nodes[] = $node;
            }
        }


        return implode("", $nodes);
    }

    function getAnchorHref()
    {
        return $this->anchor->getAttribute("href");
    }

    function getAnchorValue()
    {
        return $this->obj->name;
    }

    function getAnchorSubmenu()
    {
        if (is_array($this->elements) AND count($this->elements)) {
            $this->submenu = $this->getSubmenu();
        }
        return $this->submenu;
    }


    function getDomContainer(){
        $obj = $this->obj;
        $params = $this->params;
        $container = $this->anchor;

        if ($this->getParam("link_css")) {
            $container->addClass($params->link_css);
        }
        if ($this->getParam("link_id")) {
            $container->setAttribute("id", $params->link_id);
        }


        $subtitle = null;
        if ($obj->showtitle == 1) {
            $subtitle = Element::create($params->subtitle_tag, $obj->subtitle, ["class" => 'parent']);
            if($this->getParam('subtitle_css')){
                $subtitle->addClass($params->subtitle_css);
            }
        }
        $submenu = $this->getAnchorSubmenu();
        $dom = Element::create("div");
        if ($this->getParam("list_css")) {
            $dom->addClass($params->list_css);
        }
        if($subtitle){
            $dom->addClass('list-column');
        }
        if ($this->getParam("list_id")) {
            $dom->setAttribute("id", $params->list_id);
        }


        $subtitle_position = $this->getParam("subtitle_position", "before_link");

        if ($subtitle AND $subtitle_position == 'before_submenu') {
            $container->prependChild($subtitle);
        }
        if ($submenu) $dom->nest($submenu);

        if ($obj->content_type != '') {
            switch ($obj->content_type) {
                case 'html':
                    $content = Element::div($obj->content_html);
                    break;
                case 'image':
                    $meta = (object)\Site::imageMeta($obj->content_image);
                    $src = \FrontTpl::img($obj->content_image, false, "/images/5a/{$meta->width}/{$meta->height}");
                    //$content = Element::div(Element::img("", ["src" => \FrontTpl::img($obj->content_image), 'width' => $meta->width, 'height' => $meta->height]));
                    $content = Element::div(Element::p(Element::img("", ["alt" => $obj->name, "src" => '/media/ajax-loader.gif', "data-src" => $src, 'width' => $meta->width, 'height' => $meta->height, 'class' => 'defer'])));
                    $content->addClass('staticblock');
                    $content .= '<noscript>' . Element::img("", ["alt" => $obj->name, "src" => $src, 'width' => $meta->width, 'height' => $meta->height]) . '</noscript>';
                    break;
                case 'modulePosition':
                    //$content = Element::div("modulePosition: " . $obj->content_module_position); //TODO
                    $content =  \FrontTpl::position($obj->content_module_position, false); //TODO
                    break;
            }

            $dom->nest($content);

        }


        $container->nest($dom);
        if ($subtitle AND $subtitle_position == 'after_submenu') {
            $container->nest($subtitle);
        }
        return $container;
    }


    function getDom()
    {
        $obj = $this->obj;
        if ($this->type == 'auto') {
            return $this->getDomList();
        }
        if ($this->type == 'container') {
            $container = $this->getDomContainer();
            return $container;
        }

        $params = $this->params;
        $anchor = $this->anchor;
        $dom = Element::create("li");
        $subtitle = null;

        if ($this->getParam("list_css")) {
            $dom->addClass($params->list_css);
        }
        if ($this->getParam("list_id")) {
            $dom->setAttribute("id", $params->list_id);
        }
        if (count($this->position)) {
            $dom->addClass(implode(" ", $this->position));
        }

        if ($obj->showname == 1) {
            $anchor->setValue($obj->name);
            $anchor->setAttribute("title", $obj->name);
        }
        if ($this->getParam("link_css")) {
            $anchor->addClass($params->link_css);
        }
        if ($this->getParam("link_id")) {
            $anchor->setAttribute("id", $params->link_id);
        }

        if ($this->getParam("prepend")) {
            $dom->prependChild($params->prepend);
        }


        if ($obj->showtitle == 1) {
            $subtitle = Element::create($params->subtitle_tag, $obj->subtitle, ["class" => $params->subtitle_css, "id" => $params->subtitle_id]);
        }


        $subtitle_position = $this->getParam("subtitle_position", "before_link");
        if ($subtitle AND $subtitle_position == 'before_link') {
            $dom->nest($subtitle);
        }

        if ($this->getParam("icon_css")) {
            $icon = Element::create(\Config::get('mobile',false) === true ? 'i' : 'span', "", ['class' => $params->icon_css]);
            $anchor->setValue($icon . $anchor->getValue());
        }

        $submenu = $this->getAnchorSubmenu();


        $content = false;
        if ($obj->content_type != '') {
            switch ($obj->content_type) {
                case 'html':
                    $content = Element::div($obj->content_html);
                    break;
                case 'image':
                    $meta = (object)\Site::imageMeta($obj->content_image);
                    //$content = Element::div(Element::img("", ["src" => \FrontTpl::img($obj->content_image), 'width' => $meta->width, 'height' => $meta->height]));
                    $content = Element::div(Element::p(Element::img("", ["alt" => $obj->name, "src" => '/media/ajax-loader.gif', "data-src" => \FrontTpl::img($obj->content_image), 'width' => $meta->width, 'height' => $meta->height, 'class' => 'defer'])));
                    $content->addClass('staticblock');
                    $content .= '<noscript>' . Element::img("", ["alt" => $obj->name, "src" => \FrontTpl::img($obj->content_image), 'width' => $meta->width, 'height' => $meta->height]) . '</noscript>';
                    break;
                case 'modulePosition':
                    //$content = Element::div("modulePosition: " . $obj->content_module_position); //TODO
                    $content = Element::div( \FrontTpl::position($obj->content_module_position, false)); //TODO
                    break;
            }
            if ($obj->content_link == 1) {
                $anchor->nest($content);
            } else {
                $dom->nest($content);
            }
        }

        switch ($this->type) {
            case 'container':
                if ($subtitle AND $subtitle_position == 'before_submenu') {
                    $anchor->prependChild($subtitle);
                }
                if ($submenu) $anchor->nest($submenu);
                if ($subtitle AND $subtitle_position == 'after_submenu') {
                    $anchor->nest($subtitle);
                }

                $dom->nest($anchor);
                break;

            default:
                $dom->nest($anchor);

                if ($subtitle AND $subtitle_position == 'before_submenu') {
                    $dom->prependChild($subtitle);
                }
                if ($submenu) $dom->nest($submenu);
                if ($subtitle AND $subtitle_position == 'after_submenu') {
                    $dom->nest($subtitle);
                }

                break;
        }

        if ($subtitle AND $subtitle_position == 'after_link') {
            $dom->nest($subtitle);
        }

        if ($this->getParam("append")) {
            $dom->nest($params->append);
        }


        return $dom;
    }

    function getSubmenu()
    {
        //\Utils::log($this->type,"getSubmenu");
        $obj = $this->obj;
        $params = $this->params;

        $ul = Element::ul("", ["class" => "l" . $this->deep]);

        if ($this->getParam("submenu_css")) {
            $ul->addClass($params->submenu_css);
        }
        if ($this->getParam("submenu_id")) {
            $ul->setAttribute("id", $params->submenu_id);
        }
        $counter = 0;
        $total_elements = count($this->elements);

        $switched = false;
        $hasContainer = false;

        foreach ($this->elements as $el) {
            if($el->getType() == 'container'){
                $hasContainer = true;
                $ul->setElement("div");
                if($switched == false){

                    switch ($total_elements){
                        case 6:
                            $cssClass = 'row-col-' . $total_elements;
                            break;
                        default:
                            $cssClass = 'row-col-5';
                            break;
                    }
                    $ul->addClass('menu-row '.$cssClass);
                    $switched = true;
                }
            }
            $counter++;
            if ($counter == 1) {
                $el->position[] = 'first';
            }
            if ($counter == $total_elements) {
                $el->position[] = 'last';
            }
            $li = $el->getDom();
            $ul->nest($li);
        }
        if($this->deep == 1){
            $cls = !$hasContainer ? 'dropdown' : 'dropdown drop-grid-5-5';
            $wrap = Element::div($ul,['class' => $cls]);
        }else{
            $wrap = $ul;
        }

        return $wrap;
    }
}