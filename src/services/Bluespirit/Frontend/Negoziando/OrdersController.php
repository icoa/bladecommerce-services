<?php

namespace services\Bluespirit\Frontend\Negoziando;

use View,
    Input,
    Redirect,
    DB,
    Mainframe,
    Utils;

use Order;
use OrderState;

class OrdersController extends NegoziandoController
{
    protected $view = 'negoziando.orders.index';
    protected $name = 'Gestioni ordini';
    protected $route = '/negoziando/orders';

    protected function loadAssets()
    {
        parent::loadAssets();
        $this->toFooter('js/blade/negoziando/orders.js', 'ondemand');
    }

    public function getTable()
    {
        $lang_id = 'it';
        $shop_id = $this->repository->getShop()->id;
        $shop_code = $this->repository->getShop()->cd_neg;

        $pages = Order::leftJoin('customers', 'customer_id', '=', 'customers.id')
            ->where(function ($query) use ($shop_code, $shop_id) {
                $query->whereIn('orders.customer_id', function ($query) use ($shop_id) {
                    $query->select('id')->from('customers')->where('default_group_id', \Config::get('negoziando.b2b_group'))->where('shop_id', $shop_id);
                })->orwhereIn('orders.id', function ($query) use ($shop_code) {
                    $query->select('order_id')->from('order_details')->where('availability_shop_id', $shop_code);
                })->orWhere('delivery_store_id', $shop_id);
            })
            //->whereNotIn('availability_mode',['local'])
            ->whereNull('parent_id')
            ->groupBy('orders.id')
            ->select(
                'orders.id',
                'orders.reference',
                DB::raw("CONCAT_WS(' ',firstname,lastname,company) as customer_name"),
                'orders.status',
                'orders.payment',
                'orders.carrier_id',
                'orders.total_order',
                'orders.created_at',
                'orders.gift_box'
            );

        $order_states = Mainframe::selectOrderStates(false);
        $payment_states = Mainframe::selectPaymentStates(false);

        return \Datatables::of($pages)
            ->having_column(['customer_name'])
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('reference', function ($data) {
                $link = $this->action("getPreview", true, $data['id']);
                return "<strong><a href='$link'>{$data['reference']}</a></strong>";
            })
            ->edit_column('customer_id', function ($data) {
                $order = Order::getObj($data['id']);
                $customer = $order->getCustomer();
                $s = ($customer) ? $customer->getName() : "ND";
                return "<strong>{$s}</strong>";
            })
            ->edit_column('customer_name', function ($data) {
                //$order = \Order::getObj($data['id']);
                $s = $data['customer_name'];
                return $s;
            })
            ->edit_column('gift_box', function ($data) {
                $order = Order::getObj($data['id']);
                return $order->getActionsForClerk();
            })
            ->edit_column('carrier_id', function ($data) {
                $order = Order::getObj($data['id']);
                $customer = $order->getCarrier();
                $s = ($customer) ? $customer->name : "ND";
                return "<strong>{$s}</strong>";
            })
            ->edit_column('status', function ($data) use ($order_states) {
                $order = Order::getObj($data['id']);
                $status = $order->getStatus();
                $s = ($status) ? "<span class='label' style='background-color:{$status->color};font-size:13px' title='{$status->name}'>{$status->name}</span>" : "ND";

                return $s;
            })
            ->edit_column('total_order', function ($data) {
                return "<strong>" . \Format::currency($data['total_order'], true) . "</strong>";
            })
            ->edit_column('id', function ($data) {
                return $data['id'];
            })
            ->make();
    }

    function getPreview($id)
    {
        $this->view = 'negoziando.orders.preview';
        $order = Order::find($id);
        $this->setInnerScope('Dettagli ordine ' . $order->reference);

        return $this->render([
            'order' => $order,
            'obj' => $order,
        ]);
    }

    public function getCreate()
    {
        $this->setInnerScope('Nuovo ordine');
        $this->view = 'negoziando.orders.create';
        return $this->render([
            'controllerAction' => \Link::to('nav', 13) . '?b2b=1&splash=0&empty_cart=1',
            'back' => Input::get('source') == 'home' ? '/negoziando' : $this->action('getIndex'),
        ]);
    }
}