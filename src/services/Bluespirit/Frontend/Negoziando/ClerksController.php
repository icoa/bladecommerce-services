<?php

namespace services\Bluespirit\Frontend\Negoziando;

use URL;
use Input;
use Redirect;
use Email;

class ClerksController extends NegoziandoController
{

    protected $rules = array(
        'message' => 'required|min:20',
    );
    protected $messages = array();
    protected $friendly_names = array(
        'message' => 'Messaggio',
    );

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|string
     */
    function getIndex()
    {
        $this->setInnerScope('Pagina principale');
        $data = [
            'logout' => URL::action('services\Bluespirit\Frontend\Negoziando\AuthController@getLogout', $this->repository->getShop()->cd_neg)
        ];
        return $this->render($data);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|string
     */
    function getHelp()
    {
        $this->setInnerScope('Contatta Help-Desk');
        $this->view = 'negoziando.clerks.help';
        \AdminForm::populate([
            'shop_id' => $this->repository->getShop()->id,
            'shop_name' => $this->repository->getShop()->name,
            'user_id' => $this->repository->getUser()->id,
        ]);
        $data = [
            'source' => \Input::get('source'),
            'back' => $this->action('getIndex') . '?action=help',
            'controllerAction' => $this->action('postHelp', false),
        ];
        return $this->render($data);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|string
     */
    function getSent()
    {
        $this->setInnerScope('Messaggio inviato');
        $this->view = 'negoziando.clerks.sent';
        $data = [
            'back' => $this->action('getIndex') . '?action=help',
        ];
        return $this->render($data);
    }


    function postHelp()
    {
        $input = Input::all();
        audit($input, "INPUT ALL");

        //Apply validation rules
        $validation = \Validator::make($input, $this->rules, $this->messages);
        if (count($this->friendly_names) > 0) {
            $validation->setAttributeNames($this->friendly_names);
        }

        //Validate rules
        if ($validation->fails()) {
            $url = $this->action('getHelp');
            return Redirect::to($url)->exceptInput('_lang_fields', '_lang_fields[]')->withErrors($validation);
        }

        //send the email
        try {
            $email = Email::getByCode('B2B_CLERK_HELP', 'it');
            $email_data = [
                'shop_name' => Input::get('shop_name', 'N/D'),
                'user_id' => Input::get('user_id', 'N/D'),
                'message' => trim(nl2br(Input::get('message', 'N/D'))),
            ];
            $email->send($email_data, config('negoziando.help_clerks_recipients', 'f.politi@icoa.it'));

            $url = $this->action('getSent');
            return Redirect::to($url);
        } catch (\Exception $e) {
            audit_exception($e, __METHOD__);
            $url = $this->action('getHelp');
            $validation->getMessageBag()->add('internal_error', 'Si è verificato un problema interno; prego contattare il servizio tecnico');
            return Redirect::to($url)->exceptInput('_lang_fields', '_lang_fields[]')->withErrors($validation);
        }
    }
}