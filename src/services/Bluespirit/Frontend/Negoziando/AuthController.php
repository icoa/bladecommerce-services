<?php

namespace services\Bluespirit\Frontend\Negoziando;

use View,
    Input,
    Lang,
    Redirect,
    Sentry,
    Utils;
use FrontTpl;


class AuthController extends NegoziandoController
{
    protected $route = '/negoziando/auth';
    /**
     * @param null $code
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|string
     */
    function postLogin($code = null)
    {
        $this->code = $code;

        $credentials = array(
            'email' => Input::get('email'),
            'password' => Input::get('password')
        );

        if (feats()->enabled('recaptcha')) {
            $response = Input::get('g-recaptcha-response');
            $validRecaptcha = \Recaptcha::verifyResponse($response, 'login');
            if ($validRecaptcha == false) {
                return $this->redirectWithError(trans("ajax.recaptcha"));
            }
        }

        try {
            $user = Sentry::authenticate($credentials, Input::has('rememberme'));

            if ($user) {
                return Redirect::to('/negoziando');
            }
        } catch (\Cartalyst\Sentry\Users\LoginRequiredException $e) {
            return $this->redirectWithError("login_required");
        } catch (\Cartalyst\Sentry\Users\PasswordRequiredException $e) {
            return $this->redirectWithError("password_required");
        } catch (\Cartalyst\Sentry\Users\WrongPasswordException $e) {
            return $this->redirectWithError("password_wrong");
        } catch (\Cartalyst\Sentry\Users\UserNotFoundException $e) {
            return $this->redirectWithError("user_not_found");
        } catch (\Cartalyst\Sentry\Users\UserNotActivatedException $e) {
            return $this->redirectWithError("user_not_active");
        } // The following is only required if throttle is enabled
        catch (\Cartalyst\Sentry\Throttling\UserSuspendedException $e) {
            return $this->redirectWithError("user_suspended");
        } catch (\Cartalyst\Sentry\Throttling\UserBannedException $e) {
            return $this->redirectWithError("user_banned");
        } catch (\Exception $e) {

        }
        //return $this->redirectWithError("general_error");
    }

    /**
     * @param $code
     * @return \Illuminate\Http\RedirectResponse
     */
    public function getLogout($code)
    {
        $this->code = $code;

        Sentry::logout();

        return Redirect::to($this->route . '/login/' . $this->code);
    }

    /**
     * @param $errorMsg
     * @return \Illuminate\Http\RedirectResponse
     */
    private function redirectWithError($errorMsg)
    {
        $err = (Lang::get('login.' . $errorMsg) != '') ? Lang::get('login.' . $errorMsg) : $errorMsg;
        return Redirect::to($this->route . '/login/' . $this->code)->withErrors(array('login' => $err));
    }

    /**
     * @param null $code
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|string
     */
    function getLogin($code = null)
    {
        if (Sentry::check()) {
            return Redirect::to('/negoziando');
        }

        $this->setInnerScope('Login al sistema');
        $this->view = 'negoziando.clerks.login';

        $shop = $this->repository->getShopByCode($code);

        $data = [
            'action' => $this->route . '/login/' . $shop,
            'shop' => $shop,
            'emails' => $this->repository->getShopLoginEmails()
        ];
        return $this->render($data);
    }
}