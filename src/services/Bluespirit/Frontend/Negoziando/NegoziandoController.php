<?php

namespace services\Bluespirit\Frontend\Negoziando;

use View,
    Input,
    Lang,
    Redirect,
    Sentry,
    Utils;
use FrontendController;
use FrontTpl;


class NegoziandoController extends FrontendController
{

    protected $view = 'negoziando.clerks.index';
    protected $name = 'Area riservata commesse';
    protected $route = '/negoziando';
    protected $code;

    /**
     * @var ClerksRepository
     */
    protected $repository;

    function __construct(ClerksRepository $repository)
    {
        $this->repository = $repository;
        parent::__construct($repository);
        $this->loadAssets();

        View::share('route', $this->route);
    }

    protected function action($action, $urlify = true, $parameter = null)
    {
        $className = static::class;
        $r = "{$className}@{$action}";
        if ($urlify) {
            $r = ($parameter === null) ? \URL::action($r) : \URL::action($r, $parameter);
        } elseif ($parameter !== null) {
            $r = [$r, $parameter];
        }
        return $r;
    }

    /**
     * @param null $name
     */
    protected function setInnerScope($name = null)
    {
        $defaultName = 'Area riservata commesse';
        FrontTpl::addBreadcrumb('Home');
        FrontTpl::addBreadcrumb($defaultName, '/negoziando');
        if ($this->name != $defaultName)
            FrontTpl::addBreadcrumb($this->name, $this->route);

        if ($name)
            FrontTpl::addBreadcrumb($name);

        if (\Site::isB2B())
            FrontTpl::setBodyClass('component__clerks');

        FrontTpl::setMultipleData([
            'name' => $this->name . (($name) ? ' - ' . $name : null),
            'h1' => $this->name . (($name) ? ' - ' . $name : null),
            'robots' => 'noindex, follow',
        ]);
    }

    protected function loadAssets()
    {
        $this->toFooter('js/plugins/tables/jquery.dataTables.css', 'ondemand');
        $this->toFooter('js/plugins/tables/jquery.dataTables.min.js', 'ondemand');
        $this->toFooter('js/plugins/tables/jquery.dataTables.columnFilter.js', 'ondemand');
        $this->toFooter('js/plugins/tables/jquery.dataTables.colVis.js', 'ondemand');
    }

    /**
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|string
     */
    function getIndex()
    {
        $this->setInnerScope();
        $data = [

        ];
        return $this->render($data);
    }
}