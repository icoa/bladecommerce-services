<?php
namespace services\Bluespirit\Frontend\Negoziando;

use MorellatoShop;
use App\Models\User;
use Sentry;
use services\Repositories\UserRepository;
use Customer;
use Address;
use Input;

class ClerksRepository
{

    /**
     * @var MorellatoShop
     */
    protected $shop;

    /**
     * @var \Cartalyst\Sentry\Users\UserInterface|null
     */
    protected $user;

    /**
     * @var UserRepository
     */
    protected $userRepository;

    function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
        $this->user = Sentry::check() ? Sentry::getUser() : null;
        if ($this->user) {
            $this->shop = MorellatoShop::getObj($this->user->shop_id);
        }
    }

    /**
     * @param $code
     * @return MorellatoShop|null
     */
    function getShopByCode($code = null)
    {
        if (is_null($this->shop)) {
            $this->shop = MorellatoShop::getByCode($code);
        }
        return $this->shop;
    }

    /**
     * @return array
     */
    function getShopLoginEmails()
    {
        $emails = [];
        $shop = $this->getShopByCode();
        if ($shop) {
            $rows = User::where('negoziando', 1)->where('shop_id', $shop->id)->lists('email');
            foreach ($rows as $row) {
                $emails[$row] = $row;
            }
        }
        return $emails;
    }

    /**
     * @return \Cartalyst\Sentry\Users\UserInterface|null
     */
    function getUser()
    {
        return $this->user;
    }

    /**
     * @return MorellatoShop|null
     */
    function getShop()
    {
        return $this->shop;
    }


    function checkCardCode($card_code)
    {
        $fidelityCustomer = $this->userRepository->getFidelityCustomer(null, $card_code);
        if ($fidelityCustomer) {
            audit($fidelityCustomer->toArray(), __METHOD__ . '::fidelityCustomer');

            $customer = $fidelityCustomer->findLocalCustomer();

            $shop = $this->getShop();
            if ($shop) {
                if ($fidelityCustomer->getAttribute('store') != '') {
                    //if the fidelityCustomer has the 'store' attribute, but is different from the current logged shop, than we cannot 'sync' the platforms
                    if ($fidelityCustomer->getAttribute('store') != $shop->cd_neg) {
                        return false;
                    }
                }
                //if the fidelityCustomer has no 'store', try to set by this one
                $customer->shop_id = $shop->id;
                $fidelityCustomer->setAttribute('store', $shop->cd_neg);
            }

            $customerRepository = $this->userRepository->getFidelityCustomerRepository();
            audit('Syncing customer and fidelityCustomer');
            $data = $customerRepository->sync($customer, $fidelityCustomer);
            return $data['customer'];
        }
        return null;
    }

    /**
     * @return Customer
     */
    function createCustomer()
    {
        $customer = new Customer();
        $data = Input::only($customer->db_fields);
        $customer->setAttributes($data);
        $customer->save();
        $this->handle_address($customer);
        audit($customer->toArray(), __METHOD__ . '::CUSTOMER');

        //now create the fidelity customer
        $fidelityCustomer = $this->userRepository->getFidelityCustomer($customer->email);
        if (is_null($fidelityCustomer)) {
            $results = $this->userRepository->getFidelityCustomerRepository()->sync($customer);
            $customer = $results['customer'];
        } else {
            $results = $this->userRepository->getFidelityCustomerRepository()->sync($customer, $fidelityCustomer);
            $customer = $results['customer'];
        }
        audit($customer->toArray(), __METHOD__ . '::CUSTOMER EXPANDED');
        return $customer;
    }


    /**
     * @param Customer $customer
     * @return Customer
     */
    function updateCustomer(Customer $customer)
    {
        $data = Input::only([
            'firstname',
            'lastname',
            'email',
            'company',
            'gender_id',
            'birthday',
            'name',
        ]);
        $customer->fill($data);
        $customer->save();
        audit($customer->toArray(), __METHOD__ . '::CUSTOMER UPDATE');

        //now create the fidelity customer
        $fidelityCustomer = $this->userRepository->getFidelityCustomer($customer->email, $customer->card_code);
        if (is_null($fidelityCustomer)) {
            $results = $this->userRepository->getFidelityCustomerRepository()->sync($customer);
            $customer = $results['customer'];
        } else {
            $results = $this->userRepository->getFidelityCustomerRepository()->sync($customer, $fidelityCustomer);
            $customer = $results['customer'];
        }
        audit($customer->toArray(), __METHOD__ . '::CUSTOMER EXPANDED');
        return $customer;
    }


    /**
     * @param Customer $model
     * @return Address|null
     */
    private function handle_address(Customer $model)
    {
        $data = Input::all();
        if ($data['shipping'] == 1) {

            $obj = new Address();
            $firstname = strlen(trim($data['address_firstname'])) > 0 ? $data['address_firstname'] : $data['firstname'];
            $lastname = strlen(trim($data['address_lastname'])) > 0 ? $data['address_lastname'] : $data['lastname'];
            $company = strlen(trim($data['address_company'])) > 0 ? $data['address_company'] : $data['company'];

            $data = [
                'customer_id' => $model->id,
                'firstname' => $firstname,
                'lastname' => $lastname,
                'company' => $company,
                'people_id' => $data['people_id'],
                'address1' => ucfirst($data['address1']),
                'address2' => $data['address2'],
                'postcode' => $data['postcode'],
                'city' => ucfirst($data['city']),
                'country_id' => $data['country_id'],
                'state_id' => isset($data['state_id']) ? $data['state_id'] : null,
                'phone' => $data['phone'],
                'phone_mobile' => $data['phone_mobile'],
                'cf' => \Str::upper($data['cf']),
                'vat_number' => \Str::upper($data['vat_number']),
                'extrainfo' => $data['extrainfo'],
                'billing' => 0,
            ];

            $obj->setAttributes($data);
            $obj->save();

            return $obj;
        }

        return null;
    }
}