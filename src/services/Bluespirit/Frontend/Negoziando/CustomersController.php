<?php

namespace services\Bluespirit\Frontend\Negoziando;

use View,
    Input,
    DB,
    Redirect,
    Utils;
use Customer;
use Address;

class CustomersController extends NegoziandoController
{
    protected $view = 'negoziando.customers.index';
    protected $name = 'Gestione clienti';
    protected $route = '/negoziando/customers';

    protected $rules = array(
        'people_id' => 'required',
        'firstname' => 'required',
        'lastname' => 'required',
        'email' => 'required|email|unique:customers,email,NULL,id,active,1',
        'profile' => 'required',
        'marketing' => 'required',
        'day' => 'required',
        'month' => 'required',
        'year' => 'required',
    );
    protected $messages = array(
        'email.unique' => 'Il valore che hai fornito per il campo <b>email</b> è stato già usato. Probabilmente questo cliente è gia esistente. <a id="existing" href="/negoziando/customers?source=choose">Clicca qui per visualizzare il cliente</a>'
    );
    protected $friendly_names = array(
        'email' => 'Indirizzo Email',
        'people_id' => 'Tipologia',
        'firstname' => 'Nome',
        'lastname' => 'Cognome',
        'company' => 'Rag. sociale',
        'birthdate' => 'Data di nascita',
        'gender_id' => 'Sesso',
        'postcode' => 'C.A.P.',
        'country_id' => 'Nazione',
        'state_id' => 'Provincia',
        'address1' => 'Indirizzo',
        'address2' => 'N° civico',
        'city' => 'Città',
        'cf' => 'Codice fiscale',
        'card_code' => 'Codice tessera',
        'profile' => 'Secondo flag Acconsento/Non acconsento',
        'marketing' => 'Primo flag Acconsento/Non acconsento',
        'day' => 'Data di nascita (giorno)',
        'month' => 'Data di nascita (mese)',
        'year' => 'Data di nascita (anno)',
    );

    protected function loadAssets()
    {
        parent::loadAssets();
        $this->toFooter('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css', 'ondemand');
        $this->toFooter('https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js', 'ondemand');
        $this->toFooter('js/blade/negoziando/customers.js', 'ondemand');
    }

    protected function getDefaultValues()
    {
        return [
            'id' => 0,
            'default_group_id' => 5,
            'people_id' => 1,
            'guest' => 0,
            'active' => 1,
            'shop_id' => $this->repository->getShop()->id,
            'shop_group_id' => 1,
            'optin' => 1,
            'newsletter' => 1,
            'marketing' => null,
            'profile' => null,
            'shipping' => 0,
            'country_id' => \Core::getDefaultCountry()->id,
            'lang_id' => \Core::getLang(),
            'secure_key' => \Format::secure_key(),
            'ip_registration_newsletter' => $_SERVER['REMOTE_ADDR'],
        ];
    }

    function prepareData($model = null)
    {

        $data = Input::all();

        if ($data['id'] == 0) { //create
            $data['passwd'] = \Hash::make(\Str::random(6));
            $data['last_passwd_gen'] = \Format::now();
        }

        if ($model) {
            $this->rules['email'] = 'required|unique:customers,email,' . $model->id . ",id,active,1";
        }

        if ($data['people_id'] == 1) {
            $this->rules['firstname'] = 'required';
            $this->rules['lastname'] = 'required';
            $this->rules['gender_id'] = 'required';
        } else {
            $this->rules['company'] = 'required';
        }

        if ($data['shipping'] == 1) {
            $this->rules['address1'] = 'required';
            $this->rules['address2'] = 'required';
            $this->rules['postcode'] = 'required';
            $this->rules['city'] = 'required';
            $this->rules['country_id'] = 'required';
            $this->rules['state_id'] = 'required';
            $this->rules['cf'] = 'required';
        }

        $data['birthday'] = null;
        if ($data['year'] > 0 and $data['month'] > 0 and $data['day'] > 0) {
            if ($data['month'] < 10)
                $data['month'] = '0' . $data['month'];

            if ($data['day'] < 10)
                $data['day'] = '0' . $data['day'];

            $data['birthday'] = $data['year'] . '-' . $data['month'] . '-' . $data['day'];
        }


        $hasDate = ($data['year'] > 0 and $data['month'] > 0 and $data['day'] > 0);
        if (config('auth.customer.avoid_minors', false) === true and $hasDate and $data['people_id'] == 1) {
            $this->rules['birthday'] = 'olderThan';
        }

        $data['firstname'] = ucfirst($data['firstname']);
        $data['lastname'] = ucfirst($data['lastname']);
        $data['company'] = ucfirst($data['company']);
        $data['name'] = $data['people_id'] == 1 ? $data['firstname'] . " " . $data['lastname'] : $data['company'];

        Input::replace($data);
    }


    public function getTable()
    {
        $lang_id = 'it';
        $shop_id = $this->repository->getShop()->id;
        $email = Input::get('email');

        $pages = Customer::where('active', 1)->leftJoin("peoples_lang", "customers.people_id", "=", "peoples_lang.people_id")
            ->leftJoin("customers_groups_lang", "customers.default_group_id", "=", "customers_groups_lang.customer_group_id")
            ->where("customers_groups_lang.lang_id", $lang_id)
            ->where("peoples_lang.lang_id", $lang_id)
            ->groupBy("customers.id")
            ->select(
                'customers.id',
                DB::raw("CONCAT_WS(' ',customers.firstname,customers.lastname,customers.company) as customer_name"),
                'customers.email',
                'customers.card_code',
                'customers.created_at',
                'lastname',
                'firstname',
                'company',
                'customers.people_id'
            );

        if ($email) {
            $pages->where('customers.email', $email);
        } else {
            $pages->where("customers.shop_id", $shop_id);
        }

        $cartIsEmpty = \CartManager::isEmpty();

        return \Datatables::of($pages)
            ->having_column(['customer_name'])
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('customer_name', function ($data) {
                $link = $this->action('getEdit', true, $data['id']);
                $name = ($data["people_id"] == 2) ? $data['company'] : $data['firstname'] . " " . $data['lastname'];
                return "<strong><a href='$link'>{$name}</a></strong>";
            })
            ->edit_column('people_id', function ($data) use ($cartIsEmpty) {
                $customer = Customer::getObj($data['id']);
                if ($cartIsEmpty) {
                    $btn = "<a class='pull-right btn btn-warning' href='$customer->gateway'>LOGIN CON QUESTO UTENTE</a>";
                } else {
                    $btn = "<a class='pull-right btn btn-success' href='$customer->gateway'>PROCEDI CON L'ORDINE</a>";
                }

                return $btn;
            })
            ->remove_column('lastname')
            ->remove_column('firstname')
            ->remove_column('company')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    public function getIndex()
    {
        $this->setInnerScope();
        $data = [
            'back' => Input::get('source') == 'choose' ? $this->action('getChoose') : false,
            'create' => $this->action('getCreate'),
        ];
        return $this->render($data);
    }

    public function getCreate()
    {
        $this->setInnerScope('Nuovo cliente');
        $this->view = 'negoziando.customers.create';
        \AdminForm::populate($this->getDefaultValues());
        return $this->render([
            'controllerAction' => $this->action('postSave', false),
            'back' => Input::get('source') == 'choose' ? $this->action('getChoose') : $this->action('getIndex'),
            'customer' => null,
        ]);
    }

    public function getEdit($id)
    {
        $this->setInnerScope('Modifica cliente');
        $this->view = 'negoziando.customers.create';
        $customer = Customer::find($id);

        if ($customer) {
            $data = $customer->toArray();
            $data['day'] = $customer->day;
            $data['month'] = $customer->month;
            $data['year'] = $customer->year;
            $data['shipping'] = 0;
            \AdminForm::populate($data);
        }

        return $this->render([
            'controllerAction' => $this->action('postUpdate', false, $id),
            'back' => $this->action('getIndex'),
            'customer' => $customer,
        ]);
    }

    public function getRegistered($customer_id)
    {
        $this->setInnerScope('Cliente registrato');
        $this->view = 'negoziando.customers.registered';
        $customer = Customer::find($customer_id);
        return $this->render([
            'customer' => $customer,
        ]);
    }

    public function getPreview($customer_id)
    {
        $this->setInnerScope('Dettagli cliente');
        $this->view = 'negoziando.customers.preview';
        $customer = Customer::find($customer_id);
        if ($customer) {
            $customer->full();
        }
        return $this->render([
            'customer' => $customer,
            'back' => $this->action('getCheck'),
        ]);
    }

    public function getChoose()
    {
        $this->setInnerScope("Procedi con l'ordine");
        $this->view = 'negoziando.customers.choose';
        $after = '?source=choose';
        return $this->render([
            'check' => $this->action('getCheck') . $after,
            'list' => $this->action('getIndex') . $after,
            'create' => $this->action('getCreate') . $after,
        ]);
    }


    public function getCheck()
    {
        $this->setInnerScope("Verifica Codice tessera");
        $this->view = 'negoziando.customers.check';
        return $this->render([
            'controllerAction' => $this->action('postCheck', false),
            'back' => $this->action('getChoose'),
        ]);
    }

    public function postUpdate($id)
    {
        $customer = Customer::find($id);
        $this->prepareData($customer);
        $input = Input::all();

        //Apply validation rules
        $validation = \Validator::make($input, $this->rules, $this->messages);
        if (count($this->friendly_names) > 0) {
            $validation->setAttributeNames($this->friendly_names);
        }

        //Validate rules
        if ($validation->fails()) {
            $url = $this->action('getEdit', true, $id);
            return Redirect::to($url)->exceptInput('_lang_fields', '_lang_fields[]')->withErrors($validation);
        }

        //update the customer
        try {
            $customer = $this->repository->updateCustomer($customer);

            $url = $this->action('getIndex');
            return Redirect::to($url);
        } catch (\Exception $e) {
            audit_exception($e, __METHOD__);
            $url = $this->action('getEdit', true, $id);
            $validation->getMessageBag()->add('internal_error', 'Si è verificato un problema interno; prego contattare il servizio tecnico');
            return Redirect::to($url)->exceptInput('_lang_fields', '_lang_fields[]')->withErrors($validation);
        }
    }

    public function postSave()
    {
        $this->prepareData();
        $input = Input::all();
        $isFromOrder = Input::get('source') == 'choose';
        audit($input, "INPUT ALL");

        //Apply validation rules
        $validation = \Validator::make($input, $this->rules, $this->messages);
        if (count($this->friendly_names) > 0) {
            $validation->setAttributeNames($this->friendly_names);
        }

        //Validate rules
        if ($validation->fails()) {
            $url = $this->action('getCreate');
            if ($isFromOrder) {
                $url .= '?source=choose';
            }
            return Redirect::to($url)->exceptInput('_lang_fields', '_lang_fields[]')->withErrors($validation);
        }

        //create the customer
        try {
            $customer = $this->repository->createCustomer();

            $url = $isFromOrder ? $customer->gateway : $this->action('getRegistered', true, $customer->id);
            return Redirect::to($url);
        } catch (\Exception $e) {
            audit_exception($e, __METHOD__);
            $url = $this->action('getCreate');
            if ($isFromOrder) {
                $url .= '?source=choose';
            }
            $validation->getMessageBag()->add('internal_error', 'Si è verificato un problema interno; prego contattare il servizio tecnico');
            return Redirect::to($url)->exceptInput('_lang_fields', '_lang_fields[]')->withErrors($validation);
        }
    }

    public function postCheck()
    {
        $input = Input::all();
        $rules = [
            'card_code' => 'required|min:13|max:13'
        ];
        //Apply validation rules
        $validation = \Validator::make($input, $rules, $this->messages);
        if (count($this->friendly_names) > 0) {
            $validation->setAttributeNames($this->friendly_names);
        }

        //Validate rules
        if ($validation->fails()) {
            return Redirect::back()->exceptInput('_lang_fields', '_lang_fields[]')->withErrors($validation);
        }

        //check the code in the repository
        try {
            $back = $this->action('getChoose');
            $response = $this->repository->checkCardCode(Input::get('card_code'));
            if (is_null($response)) {
                $validation->getMessageBag()->add('card_code_not_found', "Non esiste alcun cliente registrato con questo numero tessera; assicurati di aver inserito correttamente il numero della tessera; <b>riprova o <a href='$back'>torna indietro</a> per continuare</b>");
                return Redirect::back()->exceptInput('_lang_fields', '_lang_fields[]')->withErrors($validation);
            }
            if ($response === false) { //the code exists, but it is related to another 'store'
                $validation->getMessageBag()->add('card_code_conflict', "Questa tessera BlueSpirit esiste, ma è associata ad un altro Punto Vendita BlueSpirit; impossibile continuare con l'ordine");
                return Redirect::back()->exceptInput('_lang_fields', '_lang_fields[]')->withErrors($validation);
            }
            //in this case "response" is a Customer object
            $url = $this->action('getPreview', true, $response->id);
            return Redirect::to($url);
        } catch (\Exception $e) {
            audit_exception($e, __METHOD__);
            $validation->getMessageBag()->add('internal_error', 'Si è verificato un problema interno; prego contattare il servizio tecnico');
            return Redirect::back()->exceptInput('_lang_fields', '_lang_fields[]')->withErrors($validation);
        }
    }
}