<?php


namespace services\Bluespirit\Frontend;

use Theme;
use Illuminate\Support\Str;

class Tabs
{

    protected $type;
    protected $open;
    protected $tabs = [];

    function __construct($type, $open)
    {
        $this->type = $type;
        $this->open = $open;
    }

    function addTab($title, $content){
        $item = new \stdClass();
        $item->content = trim($content);
        $item->title = $title;
        $this->tabs[] = $item;
    }

    function render(){
        $view = $this->type;
        if(!in_array($this->type, ['accordion', 'tabpanel'])){
            $view = 'accordion';
        }
        $key = Str::random(8);
        return Theme::partial('shortcodes.' . $view, ['items' => $this->tabs, 'open' => $this->open, 'key' => $key]);
    }
}