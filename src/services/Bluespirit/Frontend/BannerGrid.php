<?php
namespace services\Bluespirit\Frontend;

use Banner;
use HtmlObject\Image;
use HtmlObject\Element;

/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 15/09/2017
 * Time: 13:12
 */
class BannerGrid
{

    protected $obj;
    protected $scope = 'admin';

    function __construct($data, $default = [])
    {
        $this->obj = $data == '' ? $default : unserialize($data);
    }

    function setScope($scope){
        $this->scope = $scope;
    }

    function getBannerByIndex($index)
    {
        foreach ($this->obj as $item) {
            if ($item->index == $index) {
                \Utils::log("Banner $index found by index");
                $banner = $this->getBanner($item->id);
                $banner->item = $item;
                return $banner;
            }
        }
        return null;
    }


    function getBanner($id)
    {
        $banner = Banner::getObj($id);
        if ($banner) {
            \Utils::log("Banner OBJ $banner->id found");
            $banner->src = ($banner->image == "") ? "/media/no.gif" : $banner->image;

            $banner->altText = $banner->input;
            $banner->btnText = $banner->input_alt;
            $banner->html = $banner->input_textarea;
            if ($banner->altText == '') {
                $banner->altText = strip_tags($banner->html);
            }
            if($this->scope == 'admin'){
                $banner->htmlElement = Image::create($banner->src, null);
            }else{
                $meta = (object)\Site::imageMeta($banner->src);

                $ratio = 1.25;
                $defaultSrc = null;
                $smallWidth = null;
                $smallHeight = null;
                $injectDefault = null;

                if ($meta->width > 0 AND $meta->height > 0) {
                    $smallWidth = ceil($meta->width / $ratio);
                    $smallHeight = ceil($meta->height / $ratio);
                    $injectDefault = "/images/5a/$smallWidth/$smallHeight";
                    $injectBanner = "/images/5a/$meta->width/$meta->height";
                    if (\Str::contains($banner->src, '.gif')) {
                        $injectDefault = null;
                        $injectBanner = null;
                    }
                    $defaultSrc = $banner->src != '' ? \FrontTpl::img($banner->src, false, $injectDefault) : null;
                    $banner->src = $banner->src != '' ? \FrontTpl::img($banner->src, false, $injectBanner) : null;
                }

                if($defaultSrc){
                    $default = '';
                    if(null !== $injectDefault){
                        // do nothing on GIF
                        $default .= "<source srcset='{$defaultSrc}.webp' media='(max-width: 1200px)' type='image/webp'>";
                        $default .= "<source srcset='{$banner->src}.webp' media='(min-width: 1201px)' type='image/webp'>";
                    }
                    $default .= "<source srcset='$defaultSrc' media='(max-width: 1200px)' type='image/jpg'>";
                    $default .= "<source srcset='$banner->src' media='(min-width: 1201px)' type='image/jpg'>";

                    $default .= Image::create($defaultSrc, $banner->altText, [
                        'class' => 'img-responsive',
                        'width' => $smallWidth,
                        'height' => $smallHeight,
                    ]);

                    $banner->imgElement = Element::create('picture', $default, [
                        'class' => 'img-responsive',
                    ]);

                }else{
                    $banner->imgElement = Image::create($banner->src, $banner->altText, ['class' => 'img-responsive', 'width' => $meta->width, 'height' => $meta->height]);
                }


                $link = new \LinkResolver($banner->content);
                $banner->url = $link->getUrl();
                $params = unserialize($banner->params);
                $banner->banner_css = $params->banner_css;
            }

        }
        return $banner;
    }

    function adminBox($index)
    {
        $banner = $this->getBannerByIndex($index);
        $html = '';
        if ($banner) {
            $html = <<<HTML
<div class="box draggable ui-draggable ui-sortable" data-id="{$banner->id}" data-css="{$banner->item->css}" data-index="{$banner->item->index}">
    <div class="portlet-header">
        <ul class="icons">
            <li><a data-action="edit-box" href="#" title="Impostazioni"><i class="font-cog"></i></a></li>
        </ul>
    </div>
    {$banner->htmlElement}
</div>
HTML;

        }
        return $html;
    }
}