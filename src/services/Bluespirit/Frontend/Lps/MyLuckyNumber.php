<?php

namespace services\Bluespirit\Frontend\Lps;

use Illuminate\Support\Str;
use services\Traits\TraitLocalDebug;


class MyLuckyNumber
{

    use TraitLocalDebug;

    /**
     * @var bool
     */
    protected $localDebug = false;

    /**
     * @var array
     */
    protected $matrix = [
        1 => ['a', 'j', 's'],
        2 => ['b', 'k', 't'],
        3 => ['c', 'l', 'u'],
        4 => ['d', 'm', 'v'],
        5 => ['e', 'n', 'w'],
        6 => ['f', 'o', 'x'],
        7 => ['g', 'p', 'y'],
        8 => ['h', 'q', 'z'],
        9 => ['i', 'r'],
    ];

    /**
     * @var string
     */
    protected $name;

    /**
     * MyLuckyNumber constructor.
     * @param $name
     */
    function __construct($name)
    {
        $name = Str::lower(trim($name));
        $name = str_replace(' ', '', $name);
        $name = e($name);
        $this->name = $name;
        $this->audit($name, 'Given name');
    }

    /**
     * @return array
     */
    function getPayload()
    {
        $number = $url = $success = false;
        $errors = $this->getErrors();
        if (empty($errors)) {
            $number = $this->getTotal();
            if ($number > 0) {
                $url = \Link::to('trend', $number);
                $success = true;
            }
        }
        $payload = compact('number', 'url', 'success', 'errors');
        $this->audit($payload, 'Payload');
        return $payload;
    }

    /**
     * @return array
     */
    protected function getErrors()
    {
        $errors = [];
        if (Str::length($this->name) <= 2) {
            $errors[] = trans('lps.name_too_short');
        }
        if (preg_match('/[^A-Za-z]+/', $this->name)) {
            $errors[] = trans('lps.invalid_chars');
        }
        return $errors;
    }

    /**
     * @return int
     */
    protected function getTotal()
    {
        $total = 0;
        $letters = $this->strToArray($this->name);
        foreach ($letters as $letter) {
            $number = $this->getNumberByLetter($letter);
            $this->audit($number, "letter: $letter");
            $total += $number;
        }
        $this->audit($total, __METHOD__, 'total');
        return $this->getUserNumber($total);
    }

    /**
     * @param $letter
     * @return int
     */
    protected function getNumberByLetter($letter)
    {
        foreach ($this->matrix as $number => $letters) {
            if (in_array($letter, $letters)) {
                return $number;
            }
        }
        return 0;
    }

    /**
     * @param $total
     * @return int
     */
    protected function getUserNumber($total)
    {
        if ($total < 10)
            return $total;

        $user_number = 0;
        $digits = $this->strToArray($total);
        foreach ($digits as $digit) {
            $user_number += (int)$digit;
        }
        return $this->getUserNumber($user_number);
    }

    /**
     * @param $str
     * @return array
     */
    protected function strToArray($str)
    {
        $str = (string)$str;
        $array = [];
        for ($i = 0; $i < Str::length($str); $i++) {
            $array[$i] = $str[$i];
        }
        return $array;
    }
}