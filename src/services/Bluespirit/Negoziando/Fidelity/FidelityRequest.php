<?php
namespace services\Bluespirit\Negoziando\Fidelity;

use DB, Utils, Config, Exception;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use services\Bluespirit\Negoziando\Fidelity\Exceptions\InternalErrorException;
use services\Traits\TraitConsole;
use services\Traits\TraitParams;

use Carbon\Carbon;

/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 10/10/2017
 * Time: 18:00
 */
class FidelityRequest
{

    use TraitConsole;
    use TraitParams;

    protected $debug = false;
    protected $files;
    protected $client;

    protected $uri;
    protected $token;
    protected $method = 'get';
    protected $action;

    protected $resourceBody;
    protected $resourceMime;

    /**
     * FidelityRequest constructor.
     * @param Filesystem $files
     * @param Client $client
     */
    function __construct(Filesystem $files, Client $client)
    {

        $this->files = $files;
        $this->client = $client;
        $this->uri = Config::get('negoziando.rest.endpoint');
        $this->token = Config::get('negoziando.rest.token');
    }


    /**
     * @param $method
     * @return $this
     */
    function setMethod($method)
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return string
     */
    function getMethod()
    {
        return $this->method;
    }

    /**
     * @param $action
     * @return $this
     */
    function setAction($action)
    {
        $this->action = $action;
        return $this;
    }

    /**
     * @return string
     */
    protected function makeUri()
    {
        return $this->uri . $this->action;
    }


    /**
     * @throws Exception
     */
    function fetch()
    {
        $params = $this->getParams();
        $method = $this->getMethod();
        $uri = $this->makeUri();

        if($this->debug){
            audit('------------------------------');
            audit("Calling Negoziando API");
            audit($uri, 'URI', __METHOD__);
            audit($method, 'METHOD', __METHOD__);
            audit($params, 'PARAMS', __METHOD__);
        }

        if ($method == 'get') {
            $params['token'] = $this->token;
            $uri = $uri . '?' . http_build_query($params);
            $res = $this->client->get($uri);
        } else {
            $uri = $uri . '?token=' . $this->token;
            $res = $this->client->$method($uri, ['json' => $params]);
        }

        $mime = null;
        $statusCode = $res->getStatusCode();

        if ($statusCode == 200) {
            $contentType = $res->getHeader('Content-Type');
            if ($contentType) {
                $mime = explode(';', $contentType)[0];
            }
        } else {
            throw new Exception("Negoziando B2B has responded with a status code of [$statusCode]");
        }

        $this->resourceBody = $res->getBody();
        $this->resourceMime = $mime;

        if (strlen($this->resourceBody) <= 0) {
            throw new Exception("Negoziando B2B has responded with an empty body");
        }
        if ($mime != 'application/json') {
            throw new Exception("Negoziando B2B has responded with a MIME different than json");
        }
    }


    /**
     * @return mixed
     * @throws InternalErrorException
     */
    function getResponse()
    {
        $body = json_decode($this->resourceBody);
        if($this->debug)
            audit($body, __METHOD__);

        if ($body) {
            if (isset($body->error)) {
                throw new InternalErrorException($body->error->message, $body->error->code);
            }
            return $body->data;
        }
        return null;
    }

}