<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 04/12/2017
 * Time: 18:12
 */

namespace services\Bluespirit\Negoziando\Fidelity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Voucher extends Model
{

    const STATUS_USED = 'used';
    const STATUS_REDEEMED = 'redeemed';

    protected $table = 'vouchers';
    protected $fillable = ['code', 'cart_id', 'amount', 'shop_code', 'card_code', 'status'];
    use SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $timestamps = true;

}