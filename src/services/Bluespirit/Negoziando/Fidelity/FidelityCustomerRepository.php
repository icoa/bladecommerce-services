<?php

namespace services\Bluespirit\Negoziando\Fidelity;

use DateTime;
use Customer;
use Address;
use Country;
use services\Morellato\Loggers\DatabaseFidelityLogger as DatabaseLogger;
use State;
use App;
use Illuminate\Support\Str;
use services\Bluespirit\Negoziando\Fidelity\Exceptions\FidelityCustomerNotFoundException;
use services\Bluespirit\Negoziando\Fidelity\Exceptions\CustomerNotFoundException;
use services\Traits\TraitConsole;
use services\Traits\TraitParams;

class FidelityCustomerRepository
{

    use TraitParams;
    use TraitConsole;

    /**
     * @var Customer
     */
    protected $customer;

    /**
     * @var FidelityCustomer
     */
    protected $fidelityCustomer;

    /**
     * @var FidelityRepository
     */
    protected $repository;

    /**
     * @var bool
     */
    protected $debug = false; //this must be false in production

    /**
     * @var DatabaseLogger
     */
    protected $logger;


    /**
     * FidelityCustomerBridge constructor.
     */
    public function __construct()
    {
        $this->repository = $this->repository();
        $this->logger = new DatabaseLogger();
    }


    /**
     * return FidelityRepository
     */
    private function repository()
    {
        return App::make('services\Bluespirit\Negoziando\Fidelity\FidelityRepository');
    }


    /**
     * @param Customer $customer
     */
    public function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
        if ($customer->email != '')
            $this->setParam('email', $customer->email);

        if ($customer->card_code != '')
            $this->setParam('card_code', $customer->card_code);

        if ($customer->customer_code != '')
            $this->setParam('customer_code', $customer->customer_code);
    }


    /**
     * @param $customer_id
     */
    public function setCustomerId($customer_id)
    {
        $this->setCustomer(Customer::find($customer_id));
    }


    /**
     * @param FidelityCustomer $fidelityCustomer
     */
    public function setFidelityCustomer(FidelityCustomer $fidelityCustomer)
    {
        $this->fidelityCustomer = $fidelityCustomer;
    }


    /**
     * @return bool
     */
    public function exists()
    {
        if ($this->hasParam('email')) {
            $this->repository->setParam('email', $this->getParam('email'));
        }
        if ($this->hasParam('card_code')) {
            $this->repository->setParam('card_code', $this->getParam('card_code'));
        }
        if ($this->hasParam('customer_code')) {
            $this->repository->setParam('customer_code', $this->getParam('customer_code'));
        }
        $response = $this->repository->resetParams()->setParams($this->getParams())->check();
        if (is_null($response)) {
            return false;
        }
        $this->setParam('card_code', $response->card_code);
        $this->setParam('customer_code', $response->customer_code);
        return true;
    }


    /**
     * @return null|FidelityCustomer
     */
    public function pull()
    {
        if ($this->hasParam('customer_code')) {
            $customer = fidelityCustomer(['customer_code' => $this->getParam('customer_code')]);
            $loaded = $customer->load();
            if ($loaded == false) {
                return null;
            } else {
                $this->setFidelityCustomer($customer);
                return $customer;
            }
        }
        return null;
    }


    /**
     * @return Customer
     * @throws FidelityCustomerNotFoundException
     */
    public function insertCustomer()
    {
        //insert a new customer into the DB, starting from a fidelityCustomer
        if (is_null($this->fidelityCustomer)) {
            throw new FidelityCustomerNotFoundException('No fidelity customer found to create a new customer');
        }
        $customer = new Customer();
        $address = new Address();

        $customerAttributes = $this->fidelityCustomer->getCustomerAttributes();
        $addressAttributes = $this->fidelityCustomer->getAddressAttributes();

        $customer->fill($customerAttributes);
        $address->fill($addressAttributes);

        if ($this->debug) {
            audit($customerAttributes, 'CUSTOMER ATTRIBUTES');
            audit($addressAttributes, 'ADDRESS ATTRIBUTES');
        } else {
            $customer->save();
            $address->customer_id = $customer->id;
            $address->save();
        }

        $this->setCustomer($customer);

        return $customer;
    }


    /**
     * @return Customer
     * @throws CustomerNotFoundException
     * @throws FidelityCustomerNotFoundException
     */
    public function updateCustomer()
    {
        //update a customer into the DB, starting from a fidelityCustomer
        if (is_null($this->fidelityCustomer)) {
            throw new FidelityCustomerNotFoundException('No fidelity customer found to update a customer');
        }

        if (is_null($this->customer)) {
            throw new CustomerNotFoundException('No internal customer found to update a customer');
        }

        $customerAttributes = $this->fidelityCustomer->getCustomerAttributes();
        $addressAttributes = $this->fidelityCustomer->getAddressAttributes();

        $this->customer->fill($customerAttributes);

        unset($customerAttributes['newsletter']);
        $address = $this->customer->getShippingAddress();
        if (is_null($address)) {
            $address = new Address($addressAttributes);
        }

        if ($this->debug) {
            audit($customerAttributes, 'CUSTOMER ATTRIBUTES');
            audit($addressAttributes, 'ADDRESS ATTRIBUTES');
        } else {
            $this->customer->save();
            $address->customer_id = $this->customer->id;
            $address->save();
        }

        return $this->customer;
    }


    /**
     * @return FidelityCustomer
     * @throws CustomerNotFoundException
     */
    public function insertFidelityCustomer()
    {
        if (is_null($this->customer)) {
            throw new CustomerNotFoundException('No internal customer found to insert a new fidelity customer');
        }
        $address = $this->customer->getShippingAddress();
        $fidelityCustomer = new FidelityCustomer();
        $fidelityCustomer->setCustomerAttributes($this->customer->getAttributes(), ($address) ? $address->getAttributes() : []);
        if ($this->debug) {
            audit($fidelityCustomer->getAttributes(), 'FIDELITY CUSTOMER ATTRIBUTES');
        } else {
            $fidelityCustomer->insert();
        }
        $this->fidelityCustomer = $fidelityCustomer;
        return $fidelityCustomer;
    }


    /**
     * @return FidelityCustomer
     * @throws CustomerNotFoundException
     * @throws FidelityCustomerNotFoundException
     */
    public function updateFidelityCustomer()
    {
        if (is_null($this->customer)) {
            throw new CustomerNotFoundException('No internal customer found to update a fidelity customer');
        }
        if (is_null($this->fidelityCustomer)) {
            throw new FidelityCustomerNotFoundException('No fidelity customer found to update a fidelity customer');
        }
        $address = $this->customer->getShippingAddress();
        $this->fidelityCustomer->setCustomerAttributes($this->customer->getAttributes(), ($address) ? $address->getAttributes() : []);
        if($this->debug){
            audit($this->fidelityCustomer->toArray(), __METHOD__);
        }else{
            $this->fidelityCustomer->update();
        }

        return $this->fidelityCustomer;
    }

    /**
     * @param Customer $customer
     * @param FidelityCustomer $fidelityCustomer
     * @param array $attributes
     * @return Customer
     */
    public function upsertCustomer(Customer $customer, FidelityCustomer $fidelityCustomer, $attributes = [])
    {
        $attributes['card_code'] = $this->getParam('card_code', $fidelityCustomer->getCardCode());
        $attributes['customer_code'] = $this->getParam('customer_code', $fidelityCustomer->getCustomerCode());

        $defaults_attributes = [
            'active' => ['check' => 0, 'default' => 1],
            'secure_key' => ['check' => -1, 'default' => \Format::secure_key()],
            'passwd' => ['check' => null, 'default' => \Hash::make(md5(time()))],
            'default_group_id' => ['check' => 3, 'default' => config('negoziando.b2b_group')],
        ];

        foreach ($defaults_attributes as $key => $descriptor) {
            if (isset($attributes[$key]) and $attributes[$key] == $descriptor['check']) { //auto-enable the customer created by Bluespirit card
                $attributes[$key] = $descriptor['default'];
            }
            if(!isset($attributes[$key]) and !in_array($key, ['passwd'])){
                $attributes[$key] = $descriptor['default'];
            }
        }
        $customer->fill($attributes);
        audit($attributes, __METHOD__);
        if ($this->debug) {
            audit($attributes, __METHOD__);
        } else {
            $customer->save();
            $this->logger->log("Customer [$customer->id] saved with card_code: $customer->card_code and customer_code: $customer->customer_code", 'customer', $customer->id);
        }
        return $customer;
    }


    /**
     * @param Address $address
     * @param $attributes
     * @return $address
     */
    public function upsertAddress(Address $address, $attributes)
    {
        $attributes['customer_id'] = isset($this->customer->id) ? $this->customer->id : null;
        $address->fill($attributes);
        if ($this->debug) {
            audit($attributes, __METHOD__);
        } else {
            $address->save();
            $this->logger->log("Address [$address->id] saved for customer [$address->customer_id]", 'address', $attributes['customer_id']);
        }
        return $address;
    }


    /**
     * @param FidelityCustomer $fidelityCustomer
     * @param array $attributes
     * @param array $address_attributes
     * @return FidelityCustomer
     */
    public function upsertFidelityCustomer(FidelityCustomer $fidelityCustomer, $attributes = [], $address_attributes = [])
    {
        $customer_id = (isset($this->customer) and isset($this->customer->id)) ? $this->customer->id : null;
        $fidelityCustomer->setCustomerAttributes($attributes, $address_attributes);
        if ($this->debug) {
            audit($fidelityCustomer->getAttributes(), __METHOD__);
        } else {
            audit($fidelityCustomer->getAttributes(), __METHOD__);
            $code = $fidelityCustomer->getAttribute('code');
            $this->logger->log("Fidelity customer upserted for customer_code: $code", 'customer', $customer_id);
            $fidelityCustomer->save();
        }
        return $fidelityCustomer;
    }

    /**
     * @return array
     */
    protected function getCustomerSyncableAttributes()
    {
        return [
            'name',
            'firstname',
            'lastname',
            'birthday',
            'email',
            'gender_id',
            'people_id',
            'shop_id',
        ];
    }

    /**
     * @return array
     */
    protected function getAddressSyncableAttributes()
    {
        return [
            'firstname',
            'lastname',
            'postcode',
            'city',
            'phone',
            'phone_mobile',
            'cf',
            'vat_number',
            'country_id',
            'state_id',
            'address1',
            'address2',
        ];
    }

    /**
     * @param Customer $customer
     * @param FidelityCustomer $fidelityCustomer
     * @return array
     */
    public function sync(Customer $customer, FidelityCustomer $fidelityCustomer = null)
    {
        $this->customer = $customer;
        $fidelityCustomer = is_null($fidelityCustomer) ? new FidelityCustomer() : $fidelityCustomer;
        audit($fidelityCustomer->toArray(), __METHOD__.'::fidelityCustomer');
        $customerAttributes = $fidelityCustomer->getCustomerAttributes();
        $addressAttributes = $fidelityCustomer->getAddressAttributes();
        $localCustomerAttributes = $customer->getAttributes();
        $address = $customer->getShippingAddress();
        $address = is_null($address) ? new Address() : $address;
        $localAddressAttributes = ($address) ? $address->getAttributes() : [];

        audit($customerAttributes, __METHOD__.'::$customerAttributes');
        audit($addressAttributes, __METHOD__.'::$addressAttributes');
        audit($localCustomerAttributes, __METHOD__.'::$localCustomerAttributes');
        audit($localAddressAttributes, __METHOD__.'::$localAddressAttributes');

        $syncable = $this->getCustomerSyncableAttributes();

        $customerDiff = $this->diff($localCustomerAttributes, $customerAttributes, $syncable);
        audit($customerDiff, __METHOD__.'::$customerDiff');
        if (!empty($customerDiff['diff1']) or $customer->customer_code == '') {
            //local customers must be upserted
            $customer = $this->upsertCustomer($customer, $fidelityCustomer, $customerDiff['merge']);
        }

        $syncable = $this->getAddressSyncableAttributes();

        $addressDiff = $this->diff($localAddressAttributes, $addressAttributes, $syncable);
        audit($addressDiff, __METHOD__.'::$addressDiff');

        if (!empty($addressDiff['diff1'])) {
            //local address must be upserted
            $this->upsertAddress($address, $addressDiff['merge']);
        }

        if (!empty($customerDiff['diff2']) or !empty($addressDiff['diff2'])) {
            $fidelityCustomer = $this->upsertFidelityCustomer($fidelityCustomer, $customerDiff['merge'], $addressDiff['merge']);
            if($fidelityCustomer){
                $card_code = $fidelityCustomer->getCardCode();
                $customer_code = $fidelityCustomer->getCustomerCode();
                if($customer and $card_code and $customer_code){
                    $customer->card_code = $card_code;
                    $customer->customer_code = $customer_code;
                    $customer->save();
                }
            }
        }

        return compact('customer', 'fidelityCustomer');
    }

    /**
     * @param $obj1
     * @param $obj2
     * @param $keys
     * @param int $prefer
     * @return array
     */
    protected function diff($obj1, $obj2, $keys, $prefer = 1)
    {
        $diff1 = [];
        $diff2 = [];
        $merge = [];
        if (!empty($obj1) or !empty($obj2)) {
            foreach ($keys as $key) {
                $val1 = isset($obj1[$key]) ? $obj1[$key] : null;
                $val2 = isset($obj2[$key]) ? $obj2[$key] : null;
                if (is_string($val1)) {
                    $val1 = $key == 'email' ? Str::lower($val1) : Str::upper($val1);
                    $val1 = trim($val1);
                }
                if (is_string($val2)) {
                    $val2 = $key == 'email' ? Str::lower($val2) : Str::upper($val2);
                    $val2 = trim($val2);
                }

                $merge[$key] = ($prefer === 1) ? $val1 : $val2;

                if ($val1 !== null and $val2 !== null) {
                    if ($this->nullish($val1) and !$this->nullish($val2)) {
                        $diff1[$key] = $val2;
                        if ($prefer == 1) {
                            $merge[$key] = $val2;
                        }
                        continue;
                    }
                    if (!$this->nullish($val1) and $this->nullish($val2)) {
                        $diff2[$key] = $val1;
                        if ($prefer == 2) {
                            $merge[$key] = $val1;
                        }
                        continue;
                    }
                    if ($val1 !== $val2) {
                        if ($val2 != '')
                            $diff1[$key] = $val2;

                        if ($val1 != '')
                            $diff2[$key] = $val1;
                    }
                }
                if ($val1 === null and $val2 !== null) {
                    if ($val2 != '') {
                        $diff1[$key] = $val2;
                        if ($prefer == 1) {
                            $merge[$key] = $val2;
                        }
                    }
                    continue;
                }
                if ($val1 !== null and $val2 === null) {
                    if ($val1 != '') {
                        $diff2[$key] = $val1;
                        if ($prefer == 2) {
                            $merge[$key] = $val1;
                        }
                    }
                    continue;
                }
            }
        }

        if (!empty($merge)) {
            foreach ($merge as $key => $value) {
                if ($this->nullish($value)) {
                    unset($merge[$key]);
                }
            }
        }

        return compact('diff1', 'diff2', 'merge');
    }


    /**
     * @param $value
     * @return bool
     */
    protected function nullish($value)
    {
        return ($value === null or trim($value) == '');
    }


    /**
     *
     */
    public function syncCustomers()
    {
        $console = $this->getConsole();
        $customers = Customer::whereActive(1)
            ->whereNull('customer_code')
            //->whereIn('id', [1, 2, 3])
            ->where('default_group_id', '!=', 5)
            //->take(10)
            ->get();
        $many = count($customers);

        $console->line("Found [$many] customers so far...");
        foreach ($customers as $customer) {
            $this->resetParams();
            $this->setCustomer($customer);
            $console->line("Check if Fidelity customer [$customer->email] exists");
            $exists = $this->exists();
            if ($exists) {
                $customer_code = $this->getParam('customer_code');
                $card_code = $this->getParam('card_code');
                $console->info("The fidelity customer exists");
                $console->info("Customer code: $customer_code | Card code: $card_code");

                $fidelityCustomer = $this->pull();
                if ($fidelityCustomer) {
                    print_r($fidelityCustomer->toArray());
                    $this->sync($customer, $fidelityCustomer);
                }
            } else {
                $console->comment("The fidelity customer DOES NOT exist");
                $fidelityCustomer = $this->insertFidelityCustomer();
                $this->setParam('card_code', $fidelityCustomer->getCardCode());
                $this->setParam('customer_code', $fidelityCustomer->getCustomerCode());
                $this->upsertCustomer($customer, $fidelityCustomer);
            }
        }
    }


}