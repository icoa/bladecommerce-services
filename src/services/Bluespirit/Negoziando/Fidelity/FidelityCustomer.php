<?php

namespace services\Bluespirit\Negoziando\Fidelity;

use Exception;
use services\Bluespirit\Negoziando\Fidelity\Exceptions\InternalErrorException;
use App;
use DateTime;
use Country;
use State;
use Customer;
use Illuminate\Support\Str;
use services\Morellato\Loggers\DatabaseFidelityLogger as DatabaseLogger;
use services\Bluespirit\Negoziando\Fidelity\FidelityPoint;

use services\Traits\TraitAttributes;

class FidelityCustomer
{

    use TraitAttributes;

    protected $customer_code;

    protected $card_code;

    public $exists = false;

    /**
     * @var FidelityRepository
     */
    protected $repository;

    /**
     * @var DatabaseLogger
     */
    protected $logger;

    /**
     * @var Customer
     */
    protected $customer;

    /**
     * FidelityCustomer constructor.
     * @param $attributes
     */
    public function __construct($attributes = [])
    {
        $this->repository = $this->repository();
        $this->setAttributes($attributes);
        $this->logger = new DatabaseLogger();
    }

    /**
     * @param Customer $customer
     * @return $this
     */
    function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @return Customer
     */
    function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return bool
     */
    function hasCustomer(){
        if(is_null($this->customer))
            return false;

        if(isset($this->customer->id) and $this->customer->id > 0)
            return true;

        return false;
    }

    /**
     * @return int|null
     */
    function getCustomerId()
    {
        return ($this->customer) ? $this->customer->id : null;
    }

    /**
     * @param $name
     * @param $value
     * @return $this
     */
    function setAttribute($name, $value)
    {
        if ($name == 'code' or $name == 'customer_code') {
            $name = 'code';
            $this->setCustomerCode($value);
        }

        if ($name == 'card_code') {
            $this->setCardCode($value);
        }

        if ($name == 'birthdate') {
            $value = str_replace('.', ':', $value);
        }

        if ($name == 'email') {
            $value = \Str::lower($value);
        } else {
            $value = \Str::upper($value);
        }

        $this->attributes[$name] = trim($value);
        return $this;
    }

    /**
     * @param $customer_code
     * @return $this
     */
    public function setCustomerCode($customer_code)
    {
        $this->customer_code = $customer_code;
        $this->repository->setCustomerCode($this->customer_code);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCustomerCode()
    {
        return $this->customer_code;
    }

    /**
     * @param $card_code
     * @return $this
     */
    public function setCardCode($card_code)
    {
        $this->card_code = $card_code;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCardCode()
    {
        return $this->card_code;
    }

    /**
     * return FidelityRepository
     */
    private function repository()
    {
        return App::make('services\Bluespirit\Negoziando\Fidelity\FidelityRepository');
    }

    /**
     * @param bool $force
     * @return int|null
     */
    public function getPoints($force = false)
    {
        if ($force == false and $this->hasAttribute('points')) {
            return (int)$this->getAttribute('points');
        }
        $points = $this->repository->points();
        if (!is_null($points)) {
            $this->setAttribute('points', $points);
        }
        return $points;
    }


    /**
     * @param $points
     * @param $reason_code
     * @param null $order_reference
     * @param null $order_id
     * @return int|null
     */
    protected function setPoints($points, $reason_code, $order_reference = null, $order_id = null)
    {
        $date = date('Y-m-d');
        $time = date('His');
        $params = compact('points', 'reason_code', 'order_reference', 'date', 'time');
        $points = $this->repository->setParams($params)->transaction();
        if (!is_null($points)) {
            $this->setAttribute('points', $points);
            if(!$this->hasCustomer()){
                $this->findLocalCustomer();
            }
            $customer_id = $this->getCustomerId();
            $message = "Customer [$customer_id] updated points to ({$params['points']}). Total points is ($points).";
            if ($params['order_reference']) {
                $message .= " Order reference was {$params['order_reference']}";
            }
            $this->logger->log($message, 'points', $customer_id);

            FidelityPoint::create([
                'points' => $params['points'],
                'total_points' => $points,
                'customer_id' => $customer_id,
                'order_id' => $order_id,
                'card_code' => $this->getCardCode(),
                'customer_code' => $this->getCustomerCode(),
            ]);
        }
        return $points;
    }

    /**
     * @param $points
     * @return int|null
     */
    public function usePoints($points)
    {
        return $this->setPoints(-1 * abs($points), '005');
    }

    /**
     * @param $points
     * @return int|null
     */
    public function addPoints($points)
    {
        return $this->setPoints(1 * abs($points), '005');
    }

    /**
     * @param $points
     * @param $reference
     * @param null $order_id
     * @return int|null
     */
    public function addPointsForOrder($points, $reference, $order_id = null)
    {
        return $this->setPoints(1 * abs($points), '030', $reference, $order_id);
    }

    /**
     * @param $points
     * @param $reference
     * @param null $order_id
     * @return int|null
     */
    public function usePointsForOrder($points, $reference, $order_id = null)
    {
        return $this->setPoints(-1 * abs($points), '030', $reference, $order_id);
    }

    /**
     * @return array|null
     */
    public function update()
    {
        $response = $this->repository->resetParams()->setParams($this->getAttributes())->update();
        if (is_array($response) or is_object($response)) {
            $this->setAttributes($response);
            return (array)$response;
        }
        return null;
    }


    /**
     * @return array|null
     */
    public function insert()
    {
        $response = $this->repository->resetParams()->setParams($this->getAttributes())->insert();
        if (is_array($response) or is_object($response)) {
            audit($response, __METHOD__);
            $this->setAttributes($response);
            $this->exists = true;
            return (array)$response;
        }
        return null;
    }


    /**
     * @return array|null
     */
    public function save()
    {
        return ($this->exists) ? $this->update() : $this->insert();
    }


    /**
     *
     */
    public function load()
    {
        $this->resetAttributes();
        $response = $this->repository->setCustomerCode($this->customer_code)->resetParams()->load();
        if (is_array($response) or is_object($response)) {
            $this->setAttributes($response);
            $this->exists = true;
            return true;
        }
        return false;
    }


    /**
     * @return array
     */
    public function toArray()
    {
        return $this->getAttributes();
    }


    /**
     * @return string
     */
    function __toString()
    {
        return print_r($this->toArray(), 1);
    }


    /**
     * @param $iso
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    protected function getCountryByIso($iso)
    {
        return Country::where('iso_code3', $iso)->first();
    }


    /**
     * @param $iso
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    protected function getStateByIso($iso)
    {
        return State::where('iso_code', $iso)->first();
    }


    /**
     * @param $date
     * @return bool
     */
    function validateDate($date)
    {
        $d = DateTime::createFromFormat('Y-m-d', $date);
        return $d && $d->format('Y-m-d') === $date && $date != '1900-01-01';
    }


    /**
     * @return array
     */
    protected function getDefaultCustomerAttributes()
    {
        return [
            'shop_group_id' => 1,
            'shop_id' => 1,
            'default_group_id' => 1,
            'lang_id' => 'it',
            'risk_id' => 1,
            'company' => '',
            'siret' => null,
            'ape' => null,
            'newsletter' => 1,
            'optin' => 1,
            'people_id' => 1,
            'guest' => 0,
            'deleted' => 0,
            'ip_registration_newsletter' => '127.0.0.1',
            'secure_key' => \Format::secure_key(),
        ];
    }

    /**
     * @return array
     */
    protected function getDefaultAddressAttributes()
    {
        return \Address::getDefaultAttributes();
    }


    /**
     * @return array
     */
    public function getCustomerAttributes()
    {
        $attributes = $this->getAttributes();
        //audit($attributes, __METHOD__);

        //translate the fidelity customer attributes into customer
        /*
         *  [code] => 1000109566
            [surname] => QUERCIOLI
            [name] => CRISTINA
            [address] => VIA PUGILE, 11
            [zipcode] => 57124
            [city] => LIVORNO
            [province] => LI
            [country] => ITA
            [taxcode] =>
            [phonenumber] =>
            [birthdate] => 1965-01-11 00:00:00
            [email] =>
            [mobilenumber] =>
            [gender] => F
         *
         * */
        $customerAttributes = $this->getDefaultCustomerAttributes();
        $customerAttributes['gender_id'] = null;
        if (isset($attributes['gender'])) {
            if ($attributes['gender'] == 'M')
                $customerAttributes['gender_id'] = 1;

            if ($attributes['gender'] == 'F')
                $customerAttributes['gender_id'] = 2;
        }
        $customerAttributes['email'] = isset($attributes['email']) ? $attributes['email'] : null;
        $customerAttributes['firstname'] = isset($attributes['name']) ? $attributes['name'] : null;
        $customerAttributes['lastname'] = isset($attributes['surname']) ? $attributes['surname'] : null;
        $customerAttributes['name'] = trim($customerAttributes['firstname'] . ' ' . $customerAttributes['lastname']);
        if ($customerAttributes['lastname'] == 'COMPANY') {
            $customerAttributes['firstname'] = null;
            $customerAttributes['lastname'] = null;
            $customerAttributes['company'] = $attributes['name'];
            $customerAttributes['name'] = $attributes['name'];
            $customerAttributes['people_id'] = 2;
        }

        $customerAttributes['birthday'] = null;
        if (isset($attributes['birthdate']) and $attributes['birthdate'] != '') {
            $date = substr($attributes['birthdate'], 0, 10);
            if ($this->validateDate($date)) {
                $customerAttributes['birthday'] = $date;
            }
        }

        //attach 'shop_id' parameter
        if (isset($attributes['store'])) {
            $shop = \MorellatoShop::getByCode($attributes['store']);
            if ($shop) {
                $customerAttributes['shop_id'] = $shop->id;
            }
        }

        return $customerAttributes;
    }


    /**
     * @return array
     */
    public function getAddressAttributes()
    {
        $attributes = $this->getAttributes();
        $addressAttributes = $this->getDefaultAddressAttributes();
        $country = isset($attributes['country']) ? $this->getCountryByIso($attributes['country']) : null;
        $state = isset($attributes['province']) ? $this->getStateByIso($attributes['province']) : null;

        $addressAttributes['firstname'] = isset($attributes['name']) ? $attributes['name'] : null;
        $addressAttributes['lastname'] = isset($attributes['surname']) ? $attributes['surname'] : null;
        $addressAttributes['postcode'] = isset($attributes['zipcode']) ? $attributes['zipcode'] : null;
        $addressAttributes['city'] = isset($attributes['city']) ? $attributes['city'] : null;
        $addressAttributes['phone'] = isset($attributes['phonenumber']) ? $attributes['phonenumber'] : null;
        $addressAttributes['phone_mobile'] = isset($attributes['mobilenumber']) ? $attributes['mobilenumber'] : null;
        $addressAttributes['cf'] = isset($attributes['taxcode']) ? $attributes['taxcode'] : null;
        $addressAttributes['vat_number'] = isset($attributes['taxcode']) ? $attributes['taxcode'] : null;
        $addressAttributes['country_id'] = ($country) ? $country->id : 10;
        $addressAttributes['state_id'] = ($state) ? $state->id : null;

        $addressAttributes['address1'] = isset($attributes['address']) ? $attributes['address'] : null;
        $addressAttributes['address2'] = null;
        if (isset($attributes['address']) and Str::contains($attributes['address'], ',')) {
            $commaPos = strpos($attributes['address'], ',');
            $addressAttributes['address1'] = substr($attributes['address'], 0, $commaPos);
            $addressAttributes['address2'] = substr($attributes['address'], $commaPos + 1);
        }

        if ($addressAttributes['lastname'] == 'COMPANY') {
            $addressAttributes['firstname'] = null;
            $addressAttributes['lastname'] = null;
            $addressAttributes['company'] = $attributes['name'];
            $addressAttributes['name'] = $attributes['name'];
            $addressAttributes['people_id'] = 2;
        }

        if ($addressAttributes['address1'] == '' or $addressAttributes['city'] == '')
            return [];

        return $addressAttributes;
    }


    /**
     * @param array $customerAttributes
     * @param array $addressAttributes
     */
    public function setCustomerAttributes(array $customerAttributes, array $addressAttributes)
    {

        audit($customerAttributes, __METHOD__ . ' => customerAttributes');
        audit($addressAttributes, __METHOD__ . ' => addressAttributes');

        $attributes = [];
        $attributes['gender'] = null;
        if (isset($customerAttributes['gender_id'])) {
            if ($customerAttributes['gender_id'] == 1)
                $attributes['gender'] = 'M';

            if ($customerAttributes['gender_id'] == 2)
                $attributes['gender'] = 'F';
        }

        $attributes['name'] = array_get($customerAttributes, 'firstname');
        $attributes['surname'] = array_get($customerAttributes, 'lastname');

        if ($customerAttributes['people_id'] == 2) {
            $attributes['name'] = array_get($customerAttributes, 'name');
            $attributes['surname'] = 'company';
        }

        $attributes['email'] = array_get($customerAttributes, 'email');
        $attributes['birthdate'] = array_get($customerAttributes, 'birthday');

        if (!empty($addressAttributes)) {

            $country_code = 'ITA';
            $country = Country::find(array_get($addressAttributes, 'country_id'));
            if ($country)
                $country_code = $country->iso_code3;

            $state = State::find(array_get($addressAttributes, 'state_id'));
            $province = ($state) ? $state->iso_code : null;

            $address = array_get($addressAttributes, 'address2') != '' ? array_get($addressAttributes, 'address1') . ', ' . array_get($addressAttributes, 'address2') : array_get($addressAttributes, 'address1');
            $taxcode = array_get($addressAttributes, 'vat_number') != '' ? array_get($addressAttributes, 'vat_number') : array_get($addressAttributes, 'cf');

            $attributes['address'] = $address;
            $attributes['city'] = array_get($addressAttributes, 'city');
            $attributes['zipcode'] = array_get($addressAttributes, 'postcode');
            $attributes['phonenumber'] = array_get($addressAttributes, 'phone');
            $attributes['mobilenumber'] = array_get($addressAttributes, 'phone_mobile');
            $attributes['taxcode'] = $taxcode;
            $attributes['country'] = $country_code;
            $attributes['province'] = $province;
        }

        //attach 'store' parameter
        if (isset($customerAttributes['shop_id']) and $customerAttributes['shop_id'] > 0) {
            $shop = \MorellatoShop::getObj($customerAttributes['shop_id']);
            if ($shop) {
                $attributes['store'] = $shop->cd_neg;
            }
        }

        $this->setAttributes($attributes);
    }

    /**
     * @return Customer
     */
    public function findLocalCustomer()
    {
        $attributes = $this->getAttributes();
        audit($attributes, __METHOD__ . '::attributes');
        $card_code = $this->getCardCode();
        $builder = Customer::where('active', 1)->orderBy('id', 'desc');
        $builder->where(function ($query) use ($card_code, $attributes) {
            if ($card_code and Str::length($card_code) == 13) {
                $query->orWhere('card_code', $card_code);
            }
            if (isset($attributes['email']) and filter_var($attributes['email'], FILTER_VALIDATE_EMAIL)) {
                $query->orWhere('email', $attributes['email']);
            }
        });

        $customer = $builder->first();

        if ($customer) {
            $this->setCustomer($customer);
        } else {
            $customer = new Customer();
        }
        return $customer;
    }
}