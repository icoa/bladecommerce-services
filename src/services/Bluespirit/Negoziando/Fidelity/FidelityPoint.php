<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 04/12/2017
 * Time: 18:12
 */

namespace services\Bluespirit\Negoziando\Fidelity;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

class FidelityPoint extends Model
{
    use \Echo1\Blameable;
    protected $blameable = array("created", "updated", "deleted");
    protected $isBlameable = true;

    protected $table = 'fidelity_points';
    protected $fillable = [
        'customer_id',
        'order_id',
        'points',
        'total_points',
        'card_code',
        'customer_code',
    ];
    use SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $timestamps = true;

}