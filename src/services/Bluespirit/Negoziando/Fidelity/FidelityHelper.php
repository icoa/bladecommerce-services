<?php

namespace services\Bluespirit\Negoziando\Fidelity;

use Order;
use OrderState;
use services\Bluespirit\Negoziando\Fidelity\Voucher;

class FidelityHelper
{

    /**
     * @var int
     */
    protected $points = 0;

    /*
     * @var int
     */
    protected $limit_points;

    /**
     * @var \services\Morellato\Database\Sql\Connection
     */
    protected $connection;

    public function __construct()
    {
        $this->limit_points = config('negoziando.card.limit_points', 500);
    }

    public function setup()
    {
        if(feats()->switchEnabled(\Core\Cfg::SERVICE_FIDELITY_CARD_POINTS)){
            $this->connection = \Core::getNegoziandoConnection();
        }
    }

    /**
     * @param $points
     * @return $this
     */
    public function setPoints($points)
    {
        $this->points = $points;
        return $this;
    }

    /**
     * @return int
     */
    public function getPoints()
    {
        return $this->points;
    }

    /**
     * @return int
     */
    public function getRemainingPoints()
    {
        return $this->limit_points - $this->points;
    }

    /**
     * @return int
     */
    public function getLimitPoints()
    {
        return $this->limit_points;
    }

    /**
     * @return int
     */
    public function getProgress()
    {
        return floor($this->points / $this->limit_points * 100);
    }

    /**
     * @param Order $order
     * @return float|int
     */
    public function getPointsForOrder(Order $order)
    {
        $total = 0;
        if ($order) {

            $total = $this->getExtraPointsFromOrder($order);

            if($order->hasOwnState(OrderState::STATUS_REFUNDED_PARTIALLY)){
                $total += $order->total_products;
                return floor($total);
            }

            $rmas = $order->getRma();
            foreach($rmas as $rma){
                $products = $rma->getProducts();
                foreach($products as $product){
                    /** @var \OrderDetail $detail */
                    $detail = isset($product->detail) ? $product->detail : null;
                    if($detail){
                        $total += $detail->total_price_tax_incl;
                    }
                }
            }
            return floor($total);
        }
        return $total;
    }

    /**
     * @param Order $order
     * @return int
     */
    public function getExtraPointsFromOrder(Order $order){
        $points = 0;
        if($order){
            $products = $order->getProducts(true);
            foreach($products as $product){
                $points += isset($product->product) ? (int)$product->product->additional_shipping_cost : 0;
            }
        }
        return $points;
    }

    /**
     * @param Order $order
     * @return int
     */
    public function getUsedPointsForOrder(Order $order){
        if ($order) {
            $record = FidelityPoint::where('order_id', $order->id)->orderBy('id')->first();
            return ($record) ? abs($record->points) : 0;
        }
        return 0;
    }

    /**
     * @param $voucher
     * @return null|mixed
     */
    public function getVoucher($voucher)
    {
        $this->setup();
        $result = $this->connection->getVoucher($voucher);
        return $result;
    }

    /**
     * @param $voucher
     * @return bool
     */
    public function useVoucher($voucher)
    {
        $this->setup();
        return $this->connection->useVoucher($voucher);
    }

    /**
     * @param $voucher
     * @return bool
     */
    public function redeemVoucher($voucher)
    {
        $this->setup();
        return $this->connection->redeemVoucher($voucher);
    }

    /**
     * @param Order $order
     * @return Voucher[]|mixed
     */
    public function getVouchersForOrder(Order $order)
    {
        return Voucher::where('cart_id', $order->cart_id)->get();
    }
}