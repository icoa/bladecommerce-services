<?php

namespace services\Bluespirit\Negoziando\Fidelity;


use services\Morellato\Repositories\Repository;
use Order;
use OrderState;
use Exception;
use services\Morellato\Loggers\DatabaseOrderLogger as DatabaseLogger;

class FidelityOrderRepository extends Repository
{
    /**
     * @var bool|mixed
     */
    protected $enabled = false;

    /**
     * @var FidelityHelper
     */
    protected $fidelityHelper;

    /**
     * @var DatabaseLogger
     */
    protected $logger;

    function __construct(FidelityHelper $fidelityHelper, DatabaseLogger $logger)
    {
        $this->fidelityHelper = $fidelityHelper;
        $this->logger = $logger;
        $this->enabled = config('negoziando.fidelity_enabled', false);
    }

    /**
     * @param Order $order
     * @param $status
     */
    public function orderStatusChanged(Order $order, $status)
    {
        switch ($status) {
            case OrderState::STATUS_REFUNDED_PARTIALLY:
            case OrderState::STATUS_REFUNDED:
                $this->orderRefunded($order);
                break;

            case OrderState::STATUS_CLOSED:
                $this->orderClosed($order);
                break;

            case OrderState::STATUS_CANCELED:
                $this->orderCancelled($order);
                break;
        }
    }

    /**
     * @param Order $order
     * @param $status
     */
    public function orderPaymentStatusChanged(Order $order, $status)
    {

    }

    /**
     * @param Order $order
     */
    function orderPlaced(Order $order)
    {
        audit('removeUsedPointsToFidelityCard', __METHOD__);
        $this->removeUsedPointsToFidelityCard($order);
    }

    /**
     * @param Order $order
     */
    public function orderClosed(Order $order)
    {

        if ($order->isMaster()) { //if the order is a master, only the vouchers are processed, while points aren't
            $this->setVouchersAsUsed($order);
        } elseif ($order->isChild()) { //if the order is a child, only the points are processed, while vouchers aren't
            $this->addPointsToFidelityCard($order);
        } else {
            // in this case both points and vouchers are processed
            $this->addPointsToFidelityCard($order);
            $this->setVouchersAsUsed($order);
        }

    }

    public function orderCancelled(Order $order)
    {

        if ($order->isMaster()) { //if the order is a master, only the vouchers are processed, while points aren't
            $this->setVouchersAsRedeemed($order);
        } elseif ($order->isChild()) { //if the order is a child, only the points are processed, while vouchers aren't
            $this->retakePointsToFidelityCard($order);
        } else {
            // in this case both points and vouchers are processed
            $this->retakePointsToFidelityCard($order);
            $this->setVouchersAsRedeemed($order);
        }

    }

    public function orderRefunded(Order $order)
    {

    }


    /**
     * Add points to the fidelity card according to the given order total
     * @param Order $order
     */
    protected function addPointsToFidelityCard(Order $order)
    {
        audit(__METHOD__);
        $order_id = $order->id;

        $customer = $order->getCustomer();

        //check if the order has been cancelled or closed in the past
        $cancelledStates = $order->historyStates('O', OrderState::STATUS_CANCELED);
        $closedStates = $order->historyStates('O', OrderState::STATUS_CLOSED);

        try {
            if (is_null($customer))
                throw new Exception("No valid customer found for order [$order_id] in addPointsToFidelityCard");

            $points = $this->fidelityHelper->getPointsForOrder($order);
            audit("Adding [$points] for order [$order->reference]", __METHOD__);
            if ($points <= 0) {
                audit("0 points are meaningless", __METHOD__);
                return;
            }

            //if the order has been closed more than one time, check if the order has also been cancelled; if so we can proceed
            if (count($closedStates) > 1) {
                if (count($cancelledStates) <= 0) {
                    $message = "Order [{$order->reference}] has not previous 'cancelled' states, so skip adding points";
                    $this->logger->info($message, 'fidelity', $order_id);
                    return;
                }
            }

            $fidelityCustomer = $customer->getFidelityCustomer();
            if ($fidelityCustomer) {
                $response = $fidelityCustomer->addPointsForOrder($points, $order->reference, $order_id);
                if ($response) {
                    $message = "Order [{$order->reference}] has added ($points) points to fidelity card";
                    $this->logger->info($message, 'fidelity', $order_id);
                } else {
                    throw new Exception("No valid negoziando transaction executed for order [$order_id] in addPointsToFidelityCard");
                }
            } else {
                throw new Exception("No valid fidelityCustomer found for order [$order_id] in addPointsToFidelityCard");
            }

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), 'fidelity', $order_id);
        }
    }


    /**
     * @param Order $order
     */
    protected function retakePointsToFidelityCard(Order $order)
    {
        audit(__METHOD__);
        $order_id = $order->id;

        $customer = $order->getCustomer();

        //check if the order has been cancelled or closed in the past
        $closedStates = $order->historyStates('O', OrderState::STATUS_CLOSED);

        try {
            if (is_null($customer))
                throw new Exception("No valid customer found for order [$order_id] in retakePointsToFidelityCard");

            $points = $this->fidelityHelper->getPointsForOrder($order);
            audit("Removing [$points] for order [$order->reference]", __METHOD__);
            if ($points <= 0) {
                audit("0 points are meaningless", __METHOD__);
                return;
            }
            $fidelityCustomer = $customer->getFidelityCustomer();
            if ($fidelityCustomer) {

                //take points from the customer only when the order, in history, has been closed at least one time
                if (count($closedStates) <= 0) {
                    $message = "Order [{$order->reference}] was never closed, so points are not taken again for this customer";
                    $this->logger->info($message, 'fidelity', $order_id);
                    return;
                }

                $response = $fidelityCustomer->usePointsForOrder($points, $order->reference, $order_id);
                if ($response) {
                    $message = "Order [{$order->reference}] has removed ($points) points to fidelity card";
                    $this->logger->info($message, 'fidelity', $order_id);
                } else {
                    throw new Exception("No valid negoziando transaction executed for order [$order_id] in retakePointsToFidelityCard");
                }
            } else {
                throw new Exception("No valid fidelityCustomer found for order [$order_id] in retakePointsToFidelityCard");
            }

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), 'fidelity', $order_id);
        }
    }


    /**
     * @param Order $order
     */
    protected function setVouchersAsUsed(Order $order)
    {
        audit(__METHOD__);

        $order_id = $order->id;

        $customer = $order->getCustomer();
        try {
            if (is_null($customer))
                throw new Exception("No valid customer found for order [$order_id] in setVouchersAsUsed");

            $vouchers = $this->fidelityHelper->getVouchersForOrder($order);
            audit($vouchers->toArray(), __METHOD__);
            foreach ($vouchers as $voucher) {
                $response = $this->fidelityHelper->useVoucher($voucher->code);
                if (!$response) {
                    //if something is wrong with the operation, create an error
                    throw new Exception("Voucher ($voucher->code) in order [$order_id] cannot be setted as used");
                } else {
                    $message = "Voucher ($voucher->code) in order [$order_id] correctly setted as used";
                    $this->logger->info($message, 'fidelity', $order_id);
                    $voucher->status = 'used';
                    $voucher->save();
                }
            }

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), 'fidelity', $order_id);
        }
    }


    /**
     * @param Order $order
     */
    protected function setVouchersAsRedeemed(Order $order)
    {
        audit(__METHOD__);

        $order_id = $order->id;

        $customer = $order->getCustomer();
        try {
            if (is_null($customer))
                throw new Exception("No valid customer found for order [$order_id] in setVouchersAsRedeemed");

            $vouchers = $this->fidelityHelper->getVouchersForOrder($order);
            audit($vouchers->toArray(), __METHOD__);
            foreach ($vouchers as $voucher) {
                $response = $this->fidelityHelper->redeemVoucher($voucher->code);
                if (!$response) {
                    //if something is wrong with the operation, create an error
                    throw new Exception("Voucher ($voucher->code) in order [$order_id] cannot be setted as redeemed");
                } else {
                    $message = "Voucher ($voucher->code) in order [$order_id] correctly setted as redeemed";
                    $this->logger->info($message, 'fidelity', $order_id);
                    //if operation is successfull, delete the internal voucher
                    $voucher->status = 'redeemed';
                    $voucher->save();
                }
            }

        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), 'fidelity', $order_id);
        }
    }


    /**
     * Remove points to card as determined by the customer
     * @param Order $order
     */
    protected function removeUsedPointsToFidelityCard(Order $order)
    {
        $order_id = $order->id;
        $cart = $order->getCart();
        $customer = $order->getCustomer();
        try {
            if (is_null($cart))
                throw new Exception("No valid cart found for order [$order_id] in removeUsedPointsToFidelityCard");

            if (is_null($customer))
                throw new Exception("No valid customer found for order [$order_id] in removeUsedPointsToFidelityCard");

            $points = $cart->getPoints();
            if ($points > 0) {
                $fidelityCustomer = $customer->getFidelityCustomer();
                if ($fidelityCustomer) {
                    $response = $fidelityCustomer->usePointsForOrder($points, $order->reference, $order->id);
                    if ($response) {
                        $message = "Order [{$order->reference}] has removed ($points) points from fidelity card";
                        $this->logger->info($message, 'fidelity', $order_id);
                    } else {
                        throw new Exception("No valid negoziando transaction executed for order [$order_id] in removeUsedPointsToFidelityCard");
                    }
                } else {
                    throw new Exception("No valid fidelityCustomer found for order [$order_id] in removeUsedPointsToFidelityCard");
                }
            }
        } catch (Exception $e) {
            $this->logger->error($e->getMessage(), 'fidelity', $order_id);
        }

    }
}