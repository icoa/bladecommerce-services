<?php

namespace services\Bluespirit\Negoziando\Fidelity;

use Order;

/**
 * Trait TraitOrderFidelity
 * @package services\Bluespirit\Negoziando\Fidelity
 * @mixin Order
 */
trait TraitOrderFidelity
{
    /**
     * @return int|number
     */
    public function getFidelityUsedPoints()
    {
        $cart = $this->getCart();
        return ($cart) ? $cart->getPoints() : 0;
    }

    /**
     * @return float|int
     */
    public function getFidelityUsedPointsAmount()
    {
        $cart = $this->getCart();
        return ($cart) ? $cart->getPointsAmount() : 0;
    }

    /**
     * @return bool
     */
    public function hasFidelityPoints()
    {
        return $this->getFidelityUsedPoints() > 0;
    }

    /**
     * @return null|FidelityPoint
     */
    public function getFidelityPointRecord()
    {
        return FidelityPoint::where('order_id', $this->id)->orderBy('id', 'desc')->first();
    }
}