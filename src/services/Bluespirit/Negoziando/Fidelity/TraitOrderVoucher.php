<?php

namespace services\Bluespirit\Negoziando\Fidelity;

use Order;
use Illuminate\Database\Eloquent\Collection;

/**
 * Trait TraitOrderVoucher
 * @package services\Bluespirit\Negoziando\Fidelity
 * @mixin Order
 */
trait TraitOrderVoucher
{
    /**
     * @return bool
     */
    function hasVouchers(){
        return Voucher::where('cart_id', $this->cart_id)->count() > 0;
    }

    /**
     * @return Collection|Voucher[]
     */
    function getVouchers(){
        return Voucher::where('cart_id', $this->cart_id)->get();
    }

    /**
     * @return integer
     */
    function getTotalUsedVouchersAmount(){
        return Voucher::where('cart_id', $this->cart_id)->where('status', Voucher::STATUS_USED)->sum('amount');
    }
}