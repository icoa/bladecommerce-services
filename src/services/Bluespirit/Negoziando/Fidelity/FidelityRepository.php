<?php

namespace services\Bluespirit\Negoziando\Fidelity;

use Exception;
use services\Bluespirit\Negoziando\Fidelity\Exceptions\InternalErrorException;
use Illuminate\Support\Collection;
use services\Traits\TraitConsole;
use services\Traits\TraitParams;

class FidelityRepository
{

    use TraitConsole;
    use TraitParams;

    protected $request;

    protected $customer_code;

    /**
     * FidelityCustomer constructor.
     * @param FidelityRequest $request
     */
    public function __construct(FidelityRequest $request)
    {
        $this->request = $request;
    }

    /**
     * @param string $customer_code
     * @return $this
     */
    public function setCustomerCode($customer_code)
    {
        $this->customer_code = $customer_code;
        return $this;
    }

    /**
     * @return int|null
     */
    public function check()
    {
        try {
            $this->request->resetParams()->setParams($this->getParams());
            $this->request->setAction('check_customer')->fetch();
            $response = $this->request->getResponse();
            if ($response and isset($response->customer_code))
                return $response;

            return null;
        } catch (InternalErrorException $e) {
            $this->log($e->getMessage());
            return null;
        } catch (Exception $e) {
            $this->log($e->getMessage());
            return null;
        }
    }

    /**
     * @return int|null
     */
    public function points()
    {
        try {
            $this->request->resetParams()->setAction('points/' . $this->customer_code)->fetch();
            $response = $this->request->getResponse();
            if ($response and isset($response->points))
                return $response->points;

            return 0;
        } catch (InternalErrorException $e) {
            return null;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @return int|null
     */
    public function transaction(){
        try {
            $this->request->setMethod('post');
            $this->request->resetParams()
                ->setParams($this->getParams())
                ->setParam('customer_code', $this->customer_code)
                ->setAction('loyalty_transaction')
                ->fetch();
            $response = $this->request->getResponse();
            if ($response and isset($response->points))
                return $response->points;

            return 0;
        } catch (InternalErrorException $e) {
            return null;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @return bool|null
     */
    public function update()
    {
        try {
            $this->request->setMethod('put');
            $this->request->resetParams()->setAction('customer/' . $this->customer_code);
            $params = $this->getParams();

            //do not pass 'customer_code' and 'customer_code'
            if(isset($params['customer_code']))
                unset($params['customer_code']);

            if(isset($params['customer_code']))
                unset($params['customer_code']);

            audit($params, 'Performing customer update');

            $this->request->setParams($params)->fetch();
            $response = $this->request->getResponse();
            if ($response and isset($response->customer_code))
                return $response;

            return false;
        } catch (InternalErrorException $e) {
            return null;
        } catch (Exception $e) {
            return null;
        }
    }


    /**
     * @return bool|null
     */
    public function insert()
    {
        try {
            $this->request->setMethod('post');
            $this->request->setAction('customer');
            $this->request->resetParams()->setParams($this->getParams())->fetch();
            $response = $this->request->getResponse();
            if ($response and isset($response->customer_code)) {
                $this->customer_code = $response->customer_code;
                return $response;
            }

            return false;
        } catch (InternalErrorException $e) {
            return null;
        } catch (Exception $e) {
            return null;
        }
    }

    /**
     * @return bool|mixed|null
     */
    public function load()
    {
        try {
            $this->request->setMethod('get');
            $this->request->resetParams()->setAction('customer/' . $this->customer_code)->fetch();
            $response = $this->request->getResponse();
            audit($response,__METHOD__);

            if ($response and isset($response->code)) {
                if ($response->code == $this->customer_code) {
                    $this->setParams((array)$response);
                    return $response;
                }
            }

            return false;
        } catch (InternalErrorException $e) {
            $this->log($e->getMessage());
            return null;
        } catch (Exception $e) {
            $this->log($e->getMessage());
            return null;
        }
    }


    /**
     * @param $from
     * @param null $to
     * @return Collection
     */
    public function all($from, $to = null)
    {
        $to = is_null($to) ? date('Y-m-d H:i:s') : $to;
        $fromTime = strtotime($from);
        $toTime = strtotime($to);
        $from = str_replace(date('P', $fromTime), '', date('c', $fromTime));
        $to = str_replace(date('P', $toTime), '', date('c', $toTime));
        $this->log("From: $from");
        $this->log("To: $to");
        $items = new Collection();
        try {
            $this->request->setMethod('get');
            $this->request->setAction('customers_changed');
            $this->request->resetParams()->setParams(['From_Date' => $from, 'To_Date' => $to])->fetch();
            $response = $this->request->getResponse();
            if ($response) {
                foreach ($response as $customer_data) {
                    $items->push(new FidelityCustomer($customer_data));
                }
            }
        } catch (InternalErrorException $e) {
            $this->log($e->getMessage());
        } catch (Exception $e) {
            $this->log($e->getMessage());
        }
        return $items;
    }
}