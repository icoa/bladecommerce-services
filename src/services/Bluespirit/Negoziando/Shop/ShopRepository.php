<?php


namespace services\Bluespirit\Negoziando\Shop;

use MorellatoShop;
use services\Morellato\Repositories\Repository;
use Illuminate\Support\Str;
use DB;

class ShopRepository extends Repository
{

    protected $rules = [
        'BLU' => 'blu',
        'MOR' => 'mor',
        'JOY' => 'joy',
        '*Amante' => 'dam',
    ];

    /**
     *
     */
    public function assignGmapMarkers()
    {
        /** @var MorellatoShop[] $shops */
        $shops = MorellatoShop::get();

        $console = $this->getConsole();

        DB::beginTransaction();
        foreach ($shops as $shop) {
            $console->comment("Assigning icon marker for shop [$shop->name]");
            $this->processRule($shop);
        }
        DB::commit();
    }

    /**
     * @param MorellatoShop $shop
     * @return MorellatoShop|void
     */
    protected function processRule(MorellatoShop $shop)
    {
        $name = Str::upper(trim($shop->name));
        $resolved_icon = null;

        foreach ($this->rules as $pattern => $icon) {

            // search first 3 chars
            if (strlen($pattern) === 3) {
                if ($pattern === substr($name, 0, 3)) {
                    $resolved_icon = $icon;
                }
            }

            // search entire pattern
            if ($pattern[0] === '*') {
                if (Str::contains($name, substr($pattern, 1))) {
                    $resolved_icon = $icon;
                }
            }
        }

        if ($shop->ie === $resolved_icon) {
            return;
        }

        $shop->ie = $resolved_icon;
        $shop->save(['timestamps' => false]);
        return $shop;
    }
}