<?php

namespace services\Bluespirit\Negoziando\Repositories;

use services\Models\OrderTracking;
use services\Morellato\Repositories\Repository;
use Order;
use OrderState;
use PaymentState;
use Payment;
use OrderManager;
use Exception;
use Carrier;

class NegoziandoOrderRepository extends Repository
{
    /**
     * @param Order $order
     * @param $status
     */
    public function orderStatusChanged(Order $order, $status)
    {

    }

    /**
     * @param Order $order
     * @param $status
     */
    public function orderPaymentStatusChanged(Order $order, $status)
    {

    }

    /**
     * @param Order $order
     */
    function orderPlaced(Order $order)
    {
        //when an order is placed, if the order has shop withdrawal and show availability in the same shop, close the order immediately
        if($order->hasOwnState(OrderState::STATUS_CLOSED)){
            return;
        }
        if ($order->hasShopAvailability() and $order->hasDeliveryStore() and $order->hasPayment(Payment::TYPE_SHOP_ANTICIPATED)) {
            audit("Order hasShopAvailability and hasDeliveryStore", __METHOD__);
            try {
                $availability_shop = $order->getAvailabilityShop();
                $delivery_shop = $order->getDeliveryStore();
                if ($availability_shop and $delivery_shop) {
                    audit("availability_shop: $availability_shop->id | delivery_shop:$delivery_shop->id", __METHOD__);
                    if ($availability_shop->id == $delivery_shop->id) {
                        $order->setStatus(
                            OrderState::STATUS_CLOSED,
                            true,
                            'Chiusura automatica per uguaglianza negozio di ritiro e giacenza',
                            OrderTracking::byInternal()
                        );
                    }
                }
            } catch (Exception $e) {
                audit_exception($e, __METHOD__);
            }
        }
        if($order->hasOwnState(OrderState::STATUS_NEW) and (bool)config('negoziando.order_placed_auto_working_status', false) === true){
            if($order->isMaster()){
                $childrens = $order->getChildren();
                foreach($childrens as $children){
                    if($this->shouldMigrate($children)) {
                        $children->setStatus(
                            OrderState::STATUS_WORKING,
                            true,
                            'Migrazione automatica ordine figlio',
                            OrderTracking::byInternal()
                        );
                    }
                }
            }else{
                if($this->shouldMigrate($order)){
                    $order->setStatus(
                        OrderState::STATUS_WORKING,
                        true,
                        'Migrazione automatica ordine',
                        OrderTracking::byInternal()
                    );
                }
            }
        }
    }

    /**
     * @param Order $order
     * @return bool
     */
    function shouldMigrate(Order $order){
        $valid_carriers = [
            Carrier::TYPE_NATIONAL_DELIVERY,
            Carrier::TYPE_VIRTUAL_DELIVERY,
            Carrier::TYPE_B2B_NATIONAL_DELIVERY,
            Carrier::TYPE_B2B_SHOP_WITHDRAWAL,
            Carrier::TYPE_SHOP_WITHDRAWAL,
        ];
        //These are all payments types that require 'Payment success' payment state
        $payments_requiring_success_payment_status = [
            Payment::TYPE_CREDITCARD,
            Payment::TYPE_PAYPAL,
            Payment::TYPE_GIFTCARD,
            Payment::TYPE_SHOP_ANTICIPATED,
        ];

        //These are all payments types that DO not require 'Payment success' payment state
        $payments_not_requiring_success_payment_status = [
            Payment::TYPE_CASH => PaymentState::STATUS_PENDING_CASH,
            Payment::TYPE_WITHDRAWAL => PaymentState::STATUS_PENDING_SHOP,
            Payment::TYPE_SHIPPING_POINT => PaymentState::STATUS_PENDING_SHOP,
        ];

        if($order->hasCarrier($valid_carriers)){
            if($order->hasPayment($payments_requiring_success_payment_status) and $order->hasCurrentPaymentStatus(PaymentState::STATUS_COMPLETED)){
                return true;
            }
            foreach($payments_not_requiring_success_payment_status as $payment => $payment_status){
                if($order->hasPayment($payment) and $order->hasCurrentPaymentStatus($payment_status)){
                    return true;
                }
            }
        }
        return false;
    }
}