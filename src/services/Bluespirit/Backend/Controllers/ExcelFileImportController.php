<?php

namespace services\Bluespirit\Backend\Controllers;

use Customer;
use BackendController;
use DB;
use Input;
use AdminAction;
use services\Repositories\ExcelFileImportRepository;
use Session;
use Notification;
use services\Bluespirit\Importers\ExcelFile\JewelsExcelFileImporter;
use services\Bluespirit\Importers\ExcelFile\WatchesExcelFileImporter;
use Validator;

class ExcelFileImportController extends BackendController
{

    public $component_id = 84;
    public $title = 'Importazione catalogo Excel';
    public $page = 'excel_import.index';
    public $pageheader = 'Importazione catalogo Excel';
    public $iconClass = 'font-columns';
    public $model = 'Customer';    
    protected $watchesExcelImporter;
    protected $jewelsExcelImporter;
    
    protected $rules = array(
        'card_code' => 'required',
        'customer_code' => 'required',
        'company' => 'required_if:people_id,2',
        'firstname' => 'required_if:people_id,1',
        'lastname' => 'required_if:people_id,1',
        'people_id' => 'required',
    );
    protected $friendly_names = array(
        'people_id' => 'Tipologia',
        'firstname' => 'Nome',
        'lastname' => 'Cognome',
        'company' => 'Rag. sociale',
        'card_code' => 'Numero carta',
        'customer_code' => 'Codice cliente',
    );
    
    protected $customerColumns = array('firstname','lastname','email','gender');
    

    protected $jewelsExcel = array(
        'SKU',
        'SAPSKU',	
        'MASTERSKU',	
        'EAN',
        'BRAND',	
        'CATEGORY',
        'COLLECTION',
        'GENDER',
        'size1',
        'size2',
        'size3',
        'nav',
        'model',
        'material',
        'material_weight',	
        'pearl',
        'pearl_origin',
        'pearl_size',
        'pearl_color',
        'pearl_shape',
        'stone',
        'stone_color',	
        'stone_cut',
        'stone_karats',
        'diamond_purity',
        'diamond_range',
        'shape',
        'price',
        'BOX',
        'WARRANTY'
    );
    protected $watchesExcel = array(
        'SKU',
        'SAPSKU',	
        'EAN',
        'BRAND',	
        'CATEGORY',
        'COLLECTION',
        'GENDER',
        'case_material',
        'case_size',
        'case_thickness',
        'finishing',
        'case_colour',
        'glass',
        'strap_material',	
        'strap_colour',
        'closure',
        'movement',
        'DATA_sino',
        'swiss_made',
        'CORONA_A_VITE_sino',
        'FONDELLO_A_VITE_sino',	
        'functions',
        'water_resistant',
        'price',
        'BOX',
        'WARRANTY'
    );
    
    /**
     * @var ExcelFileImportRepository
     */
    protected $repository;

    function __construct(ExcelFileImportRepository $repository,WatchesExcelFileImporter $watchesExcelImporter, JewelsExcelFileImporter $jewelsExcelImporter)
    {
        parent::__construct();
        $this->className = __CLASS__;
        $this->repository = $repository;        
        $this->watchesExcelImporter = $watchesExcelImporter;
        $this->jewelsExcelImporter = $jewelsExcelImporter;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Importazione catalogo Excel');
        $this->toFooter("js/echo/excel_import.js?v=4");
        $groups = \CustomerGroup::rows('it')->get();

        $customer_groups=array();
        
        foreach ($groups as $key => $value) {
            $customer_groups[$value->customer_group_id] = $value->name;
        }
        $view = array('customer_groups'=>$customer_groups);

        $this->toolbar_default();
        return $this->render($view);
    }

    function postIndex(){

        $this->addBreadcrumb('Importazione catalogo Excel');
        $this->toFooter("js/echo/excel_import.js?v=4");

        $groups = \CustomerGroup::rows('it')->get();

        $customer_groups=array();
        
        foreach ($groups as $key => $value) {
            $customer_groups[$value->customer_group_id] = $value->name;
        }

        $file = Input::file('excel_file');

        $customer_group = Input::get('customer_group');

        $errors = $this->repository->setExcelColumnValidation($this->customerColumns)->checkExcelFile($file);

        if($errors){
            Session::flash('error','la struttura del catalogo excel è errata !');
            $view = array('customer_groups'=>$customer_groups);
        
            return $this->render($view);
        }

        $report = $this->repository->import_customers($file,$customer_group);
        
        $view = array('customer_groups'=>$customer_groups,'report' => $report);

        $this->toolbar_default();
        return $this->render($view);

    }
      
    public function getImport()
    {
        $this->addBreadcrumb('Importazione catalogo Excel');
        $this->page = 'excel_import.import';
        $this->pageheader = 'Importa i prodotti catalog';
        $this->toFooter("js/echo/excel_import.js?v=4");


        $view = array();

        $this->toolbar_search();
        return $this->render($view);
    } 
    
    function postImport(){

        $this->addBreadcrumb('Importazione catalogo Excel');
        $this->page = 'excel_import.import';
        $this->pageheader = 'Importa i prodotti catalog';
        $this->toFooter("js/echo/excel_import.js?v=4");
        
        $type = Input::get('type');
        $file = Input::file('excel_file');
        $input = Input::all();
        $input['extension'] = strtolower($file->getClientOriginalExtension());
        
        $rules = [
                'type' => 'required',
                'excel_file' => 'required|max:950',
                'extension' => 'required|in:xlsx'
            ];
        
        $validator = Validator::make($input,$rules);
        
        if($validator->fails()){
            
            Session::flash('error',json_encode($validator->errors()->all()));
            $view = array();
            
            return $this->render($view);
        }
        
        if(!$type){
                  
            $errors = $this->repository->setExcelColumnValidation($this->jewelsExcel)->checkExcelFile($file);
        
            if($errors){
                Session::flash('error','la struttura del catalogo excel è errata !');
                $view = array();
            
                return $this->render($view);
            }
            
            $this->jewelsExcelImporter->setFromCommand(false);
            $report = $this->jewelsExcelImporter->setFile($file)->import();
            // dd($report);
        }else{
            $errors = $this->repository->setExcelColumnValidation($this->watchesExcel)->checkExcelFile($file);
    
            if($errors){
                Session::flash('error','la struttura del catalogo excel è errata !');
                $view = array();
            
                return $this->render($view);
            }
            
            $this->watchesExcelImporter->setFromCommand(false);
            $report = $this->watchesExcelImporter->setFile($file)->import();
        }
        
        $view = array('report' => $report);
        $this->toolbar_search();
        return $this->render($view);

    }
    function toolbar_search()
    {
        global $actions;
        $index = $this->action("getIndex", TRUE);
        $actions = array(
            new AdminAction('Indietro', $index, 'font-arrow-left'),
        );
        return $actions;
    }
    
    function toolbar_default(){
        global $actions;
        $import = $this->action("getImport", TRUE);
        $actions = array(
            new AdminAction('Importa i prodotti catalog', $import, 'font-plus'),
        );
        return $actions;
    }

}
