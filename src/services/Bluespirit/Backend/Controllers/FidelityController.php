<?php

namespace services\Bluespirit\Backend\Controllers;

use Customer;
use BackendController;
use DB;
use Input;
use AdminAction;
use services\Bluespirit\Negoziando\Fidelity\Voucher;
use services\Repositories\UserRepository;

class FidelityController extends BackendController
{

    public $component_id = 84;
    public $title = 'Gestione Fidelity';
    public $page = 'fidelity.index';
    public $pageheader = 'Gestione Fidelity';
    public $iconClass = 'font-columns';
    public $model = 'Customer';
    protected $rules = array(
        'card_code' => 'required',
        'customer_code' => 'required',
        'company' => 'required_if:people_id,2',
        'firstname' => 'required_if:people_id,1',
        'lastname' => 'required_if:people_id,1',
        'people_id' => 'required',
    );
    protected $friendly_names = array(
        'people_id' => 'Tipologia',
        'firstname' => 'Nome',
        'lastname' => 'Cognome',
        'company' => 'Rag. sociale',
        'card_code' => 'Numero carta',
        'customer_code' => 'Codice cliente',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Gestione Fidelity');
        $this->toFooter("js/echo/fidelity.js?v=5");
        $view = array();
        $this->toolbar_default();
        return $this->render($view);
    }

    public function getTrash()
    {

    }

    public function getCreate()
    {

    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/fidelity.js");
        $this->page = 'fidelity.create';
        $this->pageheader = 'Dettagli Cliente';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable()
    {
        \Utils::watch();
        $lang_id = 'it';

        $pages = Customer::leftJoin("peoples_lang", "customers.people_id", "=", "peoples_lang.people_id")
            ->leftJoin("customers_groups_lang", "customers.default_group_id", "=", "customers_groups_lang.customer_group_id")
            ->leftJoin("mi_shops", "customers.shop_id", "=", "mi_shops.id")
            ->where("customers_groups_lang.lang_id", $lang_id)
            ->where("peoples_lang.lang_id", $lang_id)
            ->where("customers.card_code", '<>', '')
            ->groupBy("customers.id")
            ->select(
                'customers.id',
                'customers.people_id',
                DB::raw("CONCAT_WS(' ',customers.firstname,customers.lastname,customers.company) as customer_name"),
                'customers.default_group_id',
                'customers.email',
                'customers.card_code',
                'customers.customer_code',
                'customers.shop_id',
                'customers.siret',
                'customers.active',
                'customers.created_at',
                'customers.updated_at',
                'customers.lastname',
                'customers.firstname',
                'customers.company',
                'customers_groups_lang.name as groupname',
                'peoples_lang.name as peoplename',
                'mi_shops.name as shopname'
            );

        return \Datatables::of($pages)
            ->having_column(['customer_name'])
            ->edit_column('updated_at', function ($data) {
                return \Format::datetime($data['updated_at']);
            })
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('shop_id', function ($data) {
                $v = ($data['default_group_id'] != 3) ? $data['shopname'] : '-';
                return "<strong>$v</strong>";
            })
            ->edit_column('default_group_id', function ($data) {
                $v = $data['groupname'];
                return "<strong>$v</strong>";
            })
            ->edit_column('customer_name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                $name = ($data["people_id"] == 2) ? $data['company'] : $data['firstname'] . " " . $data['lastname'];
                $btn = "<a href='/admin/fidelity/search?card_code={$data['card_code']}' title=\"Esegue un'interrogazione live sul database di Negoziando\" class='btn btn-mini pull-right'>Live query</a>";
                return "<strong><a href='$link'>{$name}</a></strong>$btn";
            })
            ->edit_column('active', function ($data) {
                return $this->boolean($data, 'active');
            })
            ->edit_column('people_id', function ($data) {
                $v = $data['peoplename'];
                return "<strong>$v</strong>";
            })
            ->edit_column('email', function ($data) {
                $v = $data['email'];
                return "<a href='mailto:$v'>$v</a>";
            })
            ->remove_column('lastname')
            ->remove_column('firstname')
            ->remove_column('company')
            ->remove_column('groupname')
            ->remove_column('peoplename')
            ->remove_column('shopname')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    public function getTabletrash()
    {

    }

    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }

        \Input::replace($_POST);

    }


    function _after_update($model)
    {

    }

    function getSearch()
    {
        $this->toFooter("js/echo/fidelity.js");
        $this->page = 'fidelity.search';
        $this->pageheader = 'Ricerca Cliente su Negoziando';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar_search();

        $repository = app(UserRepository::class);
        $email = trim(Input::get('email'));
        $card_code = trim(Input::get('card_code'));
        $customer_code = trim(Input::get('customer_code'));
        $fidelityCustomer = null;
        $points = 0;

        if ($email != '' or $card_code != '' or $customer_code != '') {
            $fidelityCustomer = $repository->getFidelityCustomer($email, $card_code, $customer_code);
            $points = $fidelityCustomer ? $fidelityCustomer->getPoints() : null;
        }

        $view = compact('email', 'card_code', 'customer_code', 'fidelityCustomer', 'points');

        return $this->render($view);
    }


    function toolbar_search()
    {
        global $actions;
        $index = $this->action("getIndex", TRUE);
        $actions = array(
            new AdminAction('Indietro', $index, 'font-arrow-left'),
        );
        return $actions;
    }

    function getVoucher()
    {
        $this->toFooter("js/echo/fidelity.js");
        $this->page = 'fidelity.voucher';
        $this->pageheader = 'Ricerca Voucher su Negoziando';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar_search();

        $voucher = (string)trim(Input::get('voucher'));

        $response = null;
        if ($voucher !== '' and feats()->switchEnabled(\Core\Cfg::SERVICE_FIDELITY_CARD_VOUCHER)) {
            $conn = \Core::getNegoziandoConnection();
            $response = $conn->getVoucher($voucher);
        }

        $view = compact('voucher', 'response');

        return $this->render($view);
    }

    function getVoucherList()
    {
        $this->page = 'fidelity.voucher_list';
        $this->pageheader = 'Dettaglio utilizzo Voucher';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar_search();

        $rows = Voucher::orderBy('created_at')->get();

        $view = compact('rows');

        return $this->render($view);
    }

    function toolbar_default()
    {
        global $actions;
        $search = $this->action("getSearch", TRUE);
        $voucher = $this->action("getVoucher", TRUE);
        $voucherList = $this->action("getVoucherList", TRUE);
        $actions = array(
            new AdminAction('Ricerca negoziando (clienti)', $search, 'font-search'),
            new AdminAction('Ricerca negoziando (voucher)', $voucher, 'font-search'),
            new AdminAction('Storico utilizzo (voucher)', $voucherList, 'font-user'),
        );
        return $actions;
    }

    function postCommit()
    {
        $card_code = Input::get('card_code');
        $points = Input::get('points');
        $action = Input::get('action');

        //validation
        $errors = [];

        if (strlen($card_code) != 13) {
            $errors[] = "Il numero della carta non risulta essere valido";
        }
        if (!in_array($action, ['give', 'take'])) {
            $errors[] = "Azione da eseguire non risulta essere valida";
        }
        if (!is_numeric($points)) {
            $errors[] = "Valore dei punti non risulta essere valido";
        }

        if (!empty($errors)) {
            return \Response::json(['success' => false, 'msg' => implode('<br>', $errors)]);
        }

        try {
            $repository = fidelityRepository();
            $response = $repository
                ->setParam('card_code', $card_code)
                ->check();

            if ($response and isset($response->customer_code) and $response->customer_code != '') {
                $customer_code = $response->customer_code;
                $card_code = $response->card_code;
                $customer = fidelityCustomer(compact('customer_code', 'card_code'));
            } else {
                throw new \Exception("Cound not find any fidelity Customer with the given code");
            }

            if ($action == 'give') {
                $customer->addPoints($points);
            }

            if ($action == 'take') {
                $customer->usePoints($points);
            }

            return \Response::json(['success' => true, 'msg' => "Transazione eseguita correttamente, attendere caricamento pagina"]);
        } catch (\Exception $e) {
            return \Response::json(['success' => false, 'msg' => $e->getMessage()]);
        }
    }

}
