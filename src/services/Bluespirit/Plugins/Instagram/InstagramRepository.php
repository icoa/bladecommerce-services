<?php

/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 20/09/2017
 * Time: 15:54
 */

namespace services\Bluespirit\Plugins\Instagram;

use Illuminate\Support\Facades\Config;
use Illuminate\Console\Command;
use Module;
use DB;

class InstagramRepository
{

    protected $client;
    protected $console;
    protected $position;

    function __construct(Command $console, InstagramClient $client)
    {
        $this->client = $client;
        $this->console = $console;
    }

    function make()
    {
        $this->position = DB::table('instagrams')->max('position');
        $module = Module::where('mod_type','instagram')->first();
        if ($module) {
            $data = $module->content;
            $blocks = explode('---', $data);

            foreach ($blocks as $block) {
                $block = trim($block);
                $tokens = explode(PHP_EOL, $block);
                $url = $tokens[0];
                $img = $tokens[1];
                $this->upsertResource($url, $img);
            }
        }else{
            $this->console->error("No valid module found");
        }
    }


    /**
     * @param $url
     * @param $img
     */
    protected function upsertResource($url, $img)
    {
        $this->console->comment("Importing resource [$url] [$img]");

        $existing = DB::table('instagrams')->where('resource', $img)->first();
        if (!is_null($existing)) {
            $this->console->line("Resource [$img] has been skipped");
            return;
        }
        try {
            $filename = $this->client->setUrl($img)->fetchResource()->saveResource();
            $this->position++;
            $data = [
                'position' => $this->position,
                'url' => $url,
                'resource' => $img,
                'filename' => $filename,
            ];
            \Utils::log($data, __METHOD__);
            DB::table('instagrams')->insert($data);
            $this->console->info("Resource has been saved as [$filename]");
        } catch (\Exception $e) {
            $this->console->error($e->getMessage());
        }

    }
}