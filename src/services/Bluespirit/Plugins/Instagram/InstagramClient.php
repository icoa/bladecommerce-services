<?php

/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 20/09/2017
 * Time: 15:53
 */

namespace services\Bluespirit\Plugins\Instagram;

use DB, Utils, Config, Exception;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Image;
use Carbon\Carbon;


class InstagramClient
{

    protected $files;
    protected $client;

    protected $resourceUrl;
    protected $resourceBody;
    protected $resourceMime;
    protected $resourceName;

    protected $publicStoragePath;

    /**
     * InstagramClient constructor.
     * @param Filesystem $files
     * @param Client $client
     */
    function __construct(Filesystem $files, Client $client)
    {
        $this->files = $files;
        $this->client = $client;
        $this->publicStoragePath = public_path('assets/instagram/');

        if (!$this->files->isDirectory($this->publicStoragePath)) {
            $this->files->makeDirectory($this->publicStoragePath, 775, true);
        }
    }

    /**
     * @param $url
     * @return $this
     */
    function setUrl($url)
    {
        $this->resourceUrl = $url;
        //getting filename
        $tokens = explode('/', $url);
        $this->setFilename(end($tokens));
        return $this;
    }

    /**
     * @param $name
     * @return $this
     */
    function setFilename($name){
        if(Str::contains($name, '?')){
            $name = explode('?', $name)[0];
        }
        $this->resourceName = $name;
        return $this;
    }

    /**
     * @throws Exception
     */
    function fetchResource()
    {
        if (is_null($this->resourceUrl) or $this->resourceUrl == '') {
            throw new \Exception("resourceUrl cannot be empty");
        }
        $res = $this->client->get($this->resourceUrl);

        $mime = null;
        $statusCode = $res->getStatusCode();

        if ($statusCode == 200) {
            $contentType = $res->getHeader('Content-Type');
            if ($contentType) {
                $mime = explode(';', $contentType)[0];
            }
        } else {
            throw new \Exception("Image downloader for resource [$this->resourceUrl] has responded with a status code of [$statusCode]");
        }

        $this->resourceBody = $res->getBody();
        $this->resourceMime = $mime;

        if (strlen($this->resourceBody) <= 0) {
            throw new \Exception("Image downloader for resource [$this->resourceUrl] has responded with an empty body");
        }

        return $this;
    }


    /**
     * @return string
     */
    function saveResource()
    {
        $filename = $this->resourceName;
        $this->files->put($this->publicStoragePath . $filename, $this->resourceBody);
        return $filename;
    }
}