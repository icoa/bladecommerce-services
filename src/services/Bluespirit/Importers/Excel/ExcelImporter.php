<?php
namespace services\Bluespirit\Importers\Excel;

/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 21/09/2017
 * Time: 18:19
 */

use Illuminate\Console\Command;
use DB, Utils, Config, Exception, Cache;
use Attribute, AttributeOption;
use Product, ProductCombination;
use Illuminate\Support\Str;
use services\Morellato\Grabbers\GoogleTranslateGrabber;

class ExcelImporter
{

    protected $console;
    protected $debug = true;
    protected $purge = true;
    protected $write = true;
    protected $deleteAllRelations = false;
    protected $cache = [];
    protected $errors = [];
    protected $combinations_errors = [];
    protected $table = 'table';

    protected $product_id;
    protected $product_combination_id;

    protected $product_name_rule;
    protected $erasable_attributes = [];

    public $onlyPrices = false;

    /**
     * @var GoogleTranslateGrabber
     */
    protected $translateGrabber;

    function __construct(GoogleTranslateGrabber $translateGrabber)
    {
        $this->product_id = DB::selectOne("SHOW TABLE STATUS WHERE name='products'")->Auto_increment;
        $this->product_combination_id = DB::table('products_combinations')->max('id') + 1;
        $this->translateGrabber = $translateGrabber;
    }

    public function run()
    {
        $names = array_values($this->mapping());
        $names = array_merge($names, array_keys($this->addAutomaticAttributes()));
        $this->erasable_attributes = \Attribute::whereIn('code', $names)->lists('id');
    }

    public function setConsole(Command $console)
    {
        $this->console = $console;
    }

    protected function languages()
    {
        return \Core::getLanguages();
    }

    /***
     * @return Command|null
     */
    public function getConsole()
    {
        return $this->console;
    }

    /**
     * @return string
     */
    public function getTable()
    {
        return $this->table;
    }

    protected function log($var, $message = null)
    {
        if ($this->debug)
            Utils::log($var, $message);
    }

    protected function cacheStore($tag, $key, $value)
    {
        $this->cache[$tag][Str::slug($key)] = $value;
    }

    protected function cacheRead($tag, $key, $default = null)
    {
        $key = Str::slug($key);
        return isset($this->cache[$tag][$key]) ? $this->cache[$tag][$key] : $default;
    }

    protected function nullish($str)
    {
        $str = trim($str);
        return ($str == '' or is_null($str) or $str == 'null' or $str == '.' or $str == '0' or str_contains(Str::upper($str), 'TO BE DEFINED'));
    }

    protected function sanitize($str)
    {
        $str = str_replace(['\\', ':', ';', '#', '?'], '', trim($str));
        return $str;
    }

    protected function warmUpCache()
    {
        if ($this->onlyPrices === true)
            return;

        Config::set('cache.driver', 'file');
        $cacheKey = 'excel_importer_cache_' . date('Y-m-d-H');
        if ($this->purge) {
            $this->log($cacheKey, 'Purging cache');
            Cache::forget($cacheKey);
        }
        if (Cache::has($cacheKey)) {
            $this->cache = Cache::get($cacheKey);
            $this->log($cacheKey, 'Getting cache');
        } else {
            $this->makeCategories();
            $this->makeBrands();
            $this->makeCollections();
            $this->makeAttributes();
            Cache::put($cacheKey, $this->cache, 60);
            $this->log($cacheKey, 'Saving cache');
        }

        $this->log($this->cache, 'cache');
    }

    /**
     * @return array|mixed|static[]
     */
    public function getProducts()
    {
        $rows = DB::table($this->table)
            ->where(function ($query) {
                $query->where('mastersku', '')->orWhereNull('mastersku');
            })
            ->where('category', '<>', '')
            ->where('gender', '<>', '')
            //->where('sku','P.77H705000100')
            //->take(10)
            ->get();
        return $rows;
    }

    protected function importProducts()
    {
        $console = $this->getConsole();
        $console->line(__METHOD__);

        Utils::watch();

        $rows = $this->getProducts();

        DB::beginTransaction();
        foreach ($rows as $row) {
            $console->line("Import product [$row->sku]");
            $product = $this->product($row);
            $this->importProduct($product);
        }
        DB::commit();
    }

    protected function importCombinations()
    {
        $console = $this->getConsole();
        $console->line(__METHOD__);

        $rows = DB::table($this->table)
            ->where(function ($query) {
                $query->where('mastersku', '<>', '')->orWhereNotNull('mastersku');
            })
            ->where('category', '<>', '')
            ->where('gender', '<>', '')
            //->take(10)
            ->get();

        DB::beginTransaction();
        foreach ($rows as $row) {
            if (trim($row->mastersku != '')) {
                $console->line("Import combination [$row->sku]");
                $this->importVariants($row->mastersku, 'size');
            }

        }
        DB::commit();
        $this->getConsole()->line("Founded mastersku errors:");
        $this->getConsole()->line(implode(PHP_EOL, $this->errors));
        audit($this->errors, 'ERRORS');
        $this->getConsole()->line("Founded combinations with missing size:");
        $this->getConsole()->line(implode(PHP_EOL, $this->combinations_errors));
    }

    protected function mapping()
    {
        return [

        ];
    }

    protected function loadAttribute($code)
    {
        $cached = $this->cacheRead('attribute_object', $code);
        if (!is_null($cached)) {
            return $cached;
        }
        $obj = Attribute::where('code', $code)->first();
        if (is_null($obj)) {
            //throw new Exception("Attribute with code [$code] has not been found");
            return $obj;
        }
        $this->cacheStore('attribute_object', $code, $obj);
        return $obj;
    }

    protected function loadProduct($sku)
    {
        if (isset($this->cache['products'][$sku])) {
            return $this->cache['products'][$sku];
        }
        $obj = Product::where('sku', $sku)->first();
        if (is_null($obj)) {
            throw new Exception("Product with SKU [$sku] has not been found");
        }
        $this->cache['products'][$sku] = $obj;
        return $obj;
    }

    protected function loadCombination($sku, $product_id)
    {
        if (isset($this->cache['product_combinations'][$sku])) {
            return $this->cache['product_combinations'][$sku];
        }
        $obj = ProductCombination::where('sku', $sku)->where('product_id', $product_id)->first();
        if (is_null($obj)) {
            throw new Exception("Combination with SKU [$sku] for product_id [$product_id] has not been found");
        }
        $this->cache['product_combinations'][$sku] = $obj;
        return $obj;
    }


    protected function makeCategories()
    {
        $values = DB::table($this->table)->select(DB::raw('distinct(category)'))->lists('category');

        foreach ($values as $val) {
            $tokens = explode(',', $val);
            foreach ($tokens as $value) {
                $name = $this->sanitize($value);
                $data = $this->resolveCategory($name);
                if (is_null($data)) {
                    $this->getConsole()->error("Cannot found any category for [$name]");
                    $this->errors[] = "Cannot found any category for [$name]";
                } else {
                    $this->cacheStore('categories', $name, $data);
                }
            }
        }
    }

    protected function resolveCategory($name, $parent_id = 0)
    {
        if ($this->nullish($name))
            return null;

        if ($name == 'SOLO TEMPO') {
            $name = 'JUST TIME';
        }

        $slug = Str::slug($name);

        $id = DB::table("categories_lang")->where("slug", $slug)->orWhere('singular', $name)->orWhere('name', $name)->pluck("category_id");
        $category = \Category::find($id);
        if ($category) {
            if ($category->parent_id != $parent_id) {
                $parent_id = $this->findCategoryAncestor($category->id, $category->parent_id);
            }
        }

        return $id > 0 ? compact('id', 'parent_id') : null;
    }


    protected function findCategoryAncestor($id, $parent_id)
    {
        if ($parent_id == 0) {
            return $id;
        }
        $category = \Category::find($parent_id);
        return $this->findCategoryAncestor($category->id, $category->parent_id);
    }


    protected function makeBrands()
    {
        $values = DB::table($this->table)->select(DB::raw('distinct(brand)'))->lists('brand');

        foreach ($values as $value) {
            $name = $this->sanitize($value);
            if (!$this->nullish($name)) {
                $id = $this->resolveBrand($name);
                if ($id) {
                    $this->cacheStore('brands', $name, $id);
                } else {
                    $this->cacheStore('brands', $name, $this->createBrand($name));
                }
            }
        }
    }

    protected function resolveBrand($name)
    {
        if ($this->nullish($name))
            return null;

        $lower = Str::lower($name);

        if ($lower == 'sector gioielli')
            $name = 'Sector';

        if ($lower == 'ice watch' or $lower == 'ice watches')
            $name = 'Ice-Watch';

        if ($lower == 'skagen')
            $name = 'Skagen Denmark';

        $slug = Str::slug($name);

        $id = DB::table("brands_lang")->where("slug", $slug)->pluck("brand_id");

        return $id > 0 ? $id : null;
    }

    protected function createBrand($name)
    {
        $name = ucfirst(Str::lower($name));
        $data = [];
        foreach ($this->languages() as $language) {
            $data[$language] = [
                'name' => $name,
                'slug' => Str::slug($name),
                'published' => 1,
            ];
        }
        if ($this->write) {
            $model = new \Brand();
            $model->make($data);
            $this->getConsole()->info("Created new brand [$name]");
            return $model->id;
        } else {
            $this->log($data, __METHOD__);
            return -1;
        }

    }

    protected function makeCollections()
    {
        $collections = DB::table($this->table)->select(DB::raw('distinct(collection)'), 'brand')->orderBy('brand')->orderBy('collection')->get();

        foreach ($collections as $collection) {
            $name = $this->sanitize($collection->collection);
            $brand = $this->sanitize($collection->brand);
            if (!$this->nullish($name)) {
                $id = $this->resolveCollection($name, $brand);
                if ($id) {
                    $this->cacheStore('collections', "$name-$brand", $id);
                } else {
                    $this->cacheStore('collections', "$name-$brand", $this->createCollection($name, $brand));
                }
            }
        }
    }

    protected function createCollection($name, $brand)
    {
        $name = ucfirst(Str::lower($name));
        $brand_id = null;
        try {
            $brand_id = $this->cacheRead('brands', $brand);
        } catch (\Exception $e) {
            $this->getConsole()->error("Try to fetch brand_id in createCollection has caused error for collection,brand ($name,$brand)");
            return null;
        }
        $data = [
            'brand_id' => $brand_id,
        ];

        foreach ($this->languages() as $language) {
            $data[$language] = [
                'name' => $name,
                'slug' => Str::slug($name),
                'published' => 1,
            ];
        }

        if ($this->write) {
            $model = new \Collection();
            $model->make($data);
            $this->getConsole()->info("Created new collection [$name] for brand [$brand]");
            return $model->id;
        } else {
            $this->log($data, __METHOD__);
            return -1;
        }
    }


    protected function resolveCollection($name, $brand)
    {
        if ($this->nullish($name))
            return null;

        $brand_id = null;
        try {
            $brand_id = $this->cacheRead('brands', $brand);
        } catch (\Exception $e) {
            $this->getConsole()->error("Try to fetch brand_id in resolveCollection has caused error for collection,brand ($name,$brand)");
            return null;
        }

        $slug = Str::slug($name);

        $id = DB::table("collections_lang")
            ->join('collections', 'collections.id', '=', 'collections_lang.collection_id')
            ->where("slug", $slug)
            ->where('brand_id', $brand_id)
            ->pluck("id");

        return $id > 0 ? $id : null;
    }


    protected function makeAttributes()
    {
        //get all mappings keys and store the attributes
        $mapping = $this->mapping();
        foreach ($mapping as $field => $attribute_code) {
            $this->getConsole()->line("Importing [$field] ($attribute_code)");
            $attribute = $this->loadAttribute($attribute_code);
            if (is_null($attribute)) {
                $this->getConsole()->error("No valid Attribute found for [$attribute_code]");
                continue;
            }

            $importableOptions = DB::table($this->table)->select(DB::raw("distinct($field) as field"))->orderBy('field')->get();

            switch ($attribute->frontend_input) {
                case 'select':
                case 'multiselect':
                    $this->parseOptionsSelect($importableOptions, $field, $attribute);
                    break;

                case 'boolean':
                    $this->parseOptionsBoolean($importableOptions, $field, $attribute);
                    break;

                default:
                case 'text':
                    $this->parseOptionsText($importableOptions, $field, $attribute);
                    break;
            }
        }
    }


    protected function parseOptionsSelect($options, $db_field, $attribute)
    {
        $aliases_rows = DB::table('mi_attribute_alias')->where('attribute', $attribute->code)->get();
        $aliases = [];
        foreach ($aliases_rows as $aliases_row) {
            $aliases[$aliases_row->alias] = ['attribute_id' => $aliases_row->attribute_id, 'attribute_option_id' => $aliases_row->attribute_option_id];
        }
        //$this->log($options,__METHOD__);
        $attribute_id = $attribute->id;
        foreach ($options as $row) {
            $values = [];
            if ($this->attributeNameCanBeSingle($db_field)) {
                $values[] = trim(Str::upper($row->field));
            } else {
                $delimiter = (Str::contains($row->field, '/') and !Str::contains($row->field, 'H/')) ? '/' : ',';
                $values = explode($delimiter, $row->field);
                foreach ($values as $key => $value) {
                    $values[$key] = trim(Str::upper($value));
                }
            }

            //now proceed to parsing values
            foreach ($values as $attribute_value) {
                if ($attribute_value != '' and $this->cacheRead($attribute->code, $attribute_value) == null) {
                    if (isset($aliases[$attribute_value])) {
                        $this->cacheStore($attribute->code, $attribute_value, $aliases[$attribute_value]);
                    } else {
                        $this->getConsole()->line("Proceed to process SELECT values for [$attribute_value] ($attribute->code)");
                        $attribute_option_id = $this->resolveAttributeOption($attribute_value, $attribute_id, $db_field);
                        if (!is_null($attribute_option_id)) {
                            $this->cacheStore($attribute->code, $attribute_value, compact('attribute_id', 'attribute_option_id'));
                        } else {
                            $this->getConsole()->error("Cannot bind or create new attribute option [$attribute_value] ($attribute->code)");
                        }
                    }
                }
            }

        }
    }


    protected function parseOptionsText($options, $db_field, $attribute)
    {
        $attribute_id = $attribute->id;
        foreach ($options as $row) {
            $attribute_value = trim($row->field);
            if (!$this->nullish($attribute_value)) {
                $attribute_option_id = ucfirst(Str::lower($attribute_value));
                $this->cacheStore($attribute->code, $attribute_value, compact('attribute_id', 'attribute_option_id'));
            }
        }
    }


    protected function parseOptionsBoolean($options, $db_field, $attribute)
    {
        $attribute_id = $attribute->id;
        foreach ($options as $row) {
            $attribute_value = trim($row->field);
            $option = Str::lower($attribute_value);
            if (in_array($option, ['si', 'sì', 'y', '1', 'yes', 's'])) {
                $attribute_option_id = 1; //true
                $this->cacheStore($attribute->code, $attribute_value, compact('attribute_id', 'attribute_option_id'));
            }
        }
    }


    protected function attributeNameCanBeSingle($name)
    {
        return in_array($name, [
            'size1',
            'size2',
            'size3',
            'material_weight',
            'pearl_size',
            'stone_karats',
            'diamond_purity',
            'diamond_range',
            'case_size',
            'case_thickness',
            'water_resistant',
            'easy_click',
            'cites',
        ]);
    }


    protected function attributeNotTranslatable($name)
    {
        return in_array($name, [
            'size1',
            'size2',
            'size3',
            'material_weight',
            'pearl_size',
            'stone_karats',
            'diamond_purity',
            'diamond_range',
            'case_size',
            'case_thickness',
            'water_resistant',
            'swiss_made',
            'easy_click',
            'cites',
        ]);
    }


    protected function resolveAttributeOption($option, $attribute_id, $db_field)
    {
        $name = Str::upper(trim($option));
        if ($db_field == 'water_resistance') { //water resistance
            $name = trim(str_replace(['ATM', 'AT', 'A.T.M.', 'A', 'A.T.'], '', $name));
        }
        if ($db_field == 'size1') { //ring size
            $name = trim(str_replace(['MM'], '', $name));
        }
        if ($db_field == 'size3') { //other sizes
            $name = trim(str_replace(['CM'], '', $name));
        }
        $slug = Str::slug($name);

        $result = DB::table('attributes_options')
            ->leftJoin('attributes_options_lang', 'attributes_options.id', '=', 'attributes_options_lang.attribute_option_id')
            ->where('attribute_id', $attribute_id)
            ->where(function ($query) use ($slug, $name) {
                $query->where(function ($query) use ($slug, $name) {
                    $query->where('slug', $slug)->orWhere('name', $name);
                });
                $query->orWhere(function ($query) use ($slug, $name) {
                    $query->where('uslug', $slug)->orWhere('uname', $name);
                });
            })
            ->first();

        if ($result and $result->id) {
            $this->getConsole()->comment("Attribute option found in database for [$option] ($attribute_id)");
            return $result->id;
        }

        return $this->createAttributeOption($name, $attribute_id, $db_field);
    }


    protected function createAttributeOption($attribute_value, $attribute_id, $db_field)
    {
        $console = $this->getConsole();
        $not_translatable = $this->attributeNotTranslatable($db_field);
        $uname = ucfirst(Str::lower($attribute_value));
        $uslug = Str::slug($uname);
        $data = compact('attribute_id', 'uname', 'uslug');
        $name = $uname;
        $slug = $uslug;

        foreach ($this->languages() as $language) {
            if ($language != 'it' and $not_translatable === false) {
                $console->line("Wait, translating [$uname]...");
                $results = $this->translateGrabber->reset()->addItem($uname)->translate($language);
                $translated_name = mb_ucfirst($results[0]);
                if (strlen($translated_name) > 0) {
                    $name = $translated_name;
                    $slug = Str::slug($name);
                }
            }
            $data[$language] = [
                'name' => $name,
                'slug' => $slug,
                'published' => 1,
            ];
        }

        $this->getConsole()->info("Created new attribute_option [$attribute_value] for attribute [$attribute_id]");
        if ($this->write) {
            $model = new \AttributeOption();
            $model->make($data);
            return $model->id;
        } else {
            $this->log($data, __METHOD__);
            return -1;
        }
    }


    protected function insertAction($product_id, $action, $from)
    {
        $table = 'mi_products_actions';

        $data = compact('product_id', 'action', 'from');
        $data['created_at'] = date('Y-m-d H:i:s');
        if ($this->write) {
            try {
                DB::table($table)->where('product_id', $product_id)->where('action', $action)->delete();
                DB::table($table)->insert($data);
            } catch (Exception $e) {

            }
        }
        return $data;
    }


    protected function craftProductName($product, $lang_id)
    {
        if (isset($product->relations['category'][0]['id']) and isset($product->relations['category'][0]['parent_id'])) {
            $rules = $this->product_name_rule[$lang_id];
            $tokens = [];
            foreach ($rules as $rule) {
                switch ($rule) {
                    case 'category':
                        $category = \Category::getObj($product->relations['category'][0]['id'], $lang_id);
                        if ($category->singular != '') {
                            $tokens[] = $category->singular;
                        } else {
                            $tokens[] = $category->name;
                        }
                        break;

                    case 'parent_category':
                        $category = \Category::getObj($product->relations['category'][0]['parent_id'], $lang_id);
                        if ($category) {
                            if ($category->singular != '') {
                                $tokens[] = $category->singular;
                            } else {
                                $tokens[] = $category->name;
                            }
                        } else {
                            $category = \Category::getObj($product->relations['category'][0]['id'], $lang_id);
                            if ($category->singular != '') {
                                $tokens[] = $category->singular;
                            } else {
                                $tokens[] = $category->name;
                            }
                        }
                        break;

                    case 'brand':
                        $tokens[] = $this->sanitize($product->brand);
                        break;

                    case 'collection':
                        $tokens[] = $this->sanitize($product->collection);
                        break;

                    case 'sku':
                        $tokens[] = '- ' . $this->sanitize($product->sku);
                        break;

                    default: //ATTRIBUTE HERE
                        //TODO: resolve dynamic attribute here
                        audit($rule, 'TODO: resolve dynamic attribute here');
                        break;
                }
            }
            return Str::upper(implode(' ', $tokens));
        } else {
            return null;
        }
    }


    protected function product($product)
    {
        unset($product->created_at);

        $product->sku = Str::upper(trim($this->sanitize($product->sku)));
        $product->sap_sku = Str::upper(trim($this->sanitize($product->sapsku)));
        $product->ean = Str::upper(trim($this->sanitize($product->ean)));
        $product->mastersku = Str::upper(trim($this->sanitize($product->mastersku)));
        $product->price = $this->price($product->price);
        if ($product->sku == $product->mastersku) {
            $product->mastersku = null;
        }
        if ($product->sap_sku == '')
            $product->sap_sku = $product->sku;

        if ($this->onlyPrices === true)
            return $product;

        if (strlen($product->canvas) == 6) {
            $canvas = $this->parseCanvas($product->canvas);
            $product->new_from_date = $canvas['new_from_date'];
            $product->new_to_date = $canvas['new_to_date'];
        }

        $collection = $this->sanitize($product->collection);
        $brand = $this->sanitize($product->brand);
        $category = $this->sanitize($product->category);
        $relations = [];
        $relations['brand'] = $this->cacheRead('brands', $brand);
        $relations['collection'] = $this->cacheRead('collections', "$collection-$brand");

        $tokens = explode(',', $category);
        $categories = [];
        foreach ($tokens as $token) {
            $cat = $this->sanitize($token);
            $resolvedCategory = $this->cacheRead('categories', $cat);
            if ($resolvedCategory) {
                $categories[] = $resolvedCategory;
            }
        }

        $relations['category'] = $categories;
        $this->log($relations, __METHOD__);

        $product->relations = $relations;

        $storage = [];

        $languages = $this->languages();

        foreach ($languages as $lang_id) {
            $name = $this->craftProductName($product, $lang_id);
            if ($this->nullish($name)) {
                throw new Exception("Translation data are incomplete for product [$product->sku]");
            }
            $product_translation = (object)[
                'name' => $name,
                'lang_id' => $lang_id,
            ];
            $storage[$lang_id] = $product_translation;
        }

        $product->translations = $storage;

        $attributes = [];

        $mapping = $this->mapping();
        foreach ($mapping as $field => $attribute_code) {
            $attribute = $this->loadAttribute($attribute_code);
            if (is_null($attribute)) {
                $this->getConsole()->error("No valid Attribute found for [$attribute_code]");
                continue;
            }

            if (isset($attributes[$attribute->code])) {
                $attributes[$attribute->code] = [];
            }

            switch ($attribute->frontend_input) {
                case 'select':
                case 'multiselect':
                    $values = [];
                    if ($this->attributeNameCanBeSingle($field)) {
                        $values[] = trim(Str::upper($product->$field));
                    } else {
                        $delimiter = (Str::contains($product->$field, '/') and !Str::contains($product->$field, 'H/')) ? '/' : ',';
                        $values = explode($delimiter, $product->$field);
                        foreach ($values as $key => $value) {
                            $values[$key] = trim(Str::upper($value));
                        }
                    }

                    //now proceed to parsing values
                    foreach ($values as $attribute_value) {
                        $attribute_relation = $this->cacheRead($attribute->code, $attribute_value);
                        if ($attribute_value != '' and $attribute_relation) {
                            $attributes[$attribute->code][] = $attribute_relation;
                        }
                    }
                    break;

                case 'boolean':
                case 'text':
                    $attribute_value = trim(Str::upper($product->$field));
                    $attribute_relation = $this->cacheRead($attribute->code, $attribute_value);
                    if ($attribute_relation) {
                        $attributes[$attribute->code][] = $attribute_relation;
                    }
                    break;
            }
        }

        //add Garanzia e Confezione if product category = 'Orologio'
        $attributes = array_merge($attributes, $this->addAutomaticAttributes());

        $product->attributes = $attributes;
        $product->translations = $storage;

        $this->log($product, __METHOD__);

        return $product;
    }


    private function parsePrices($price)
    {
        $price = $this->price($price);
        $price_nt = $this->price($price / 1.22);
        return [
            'price' => $price,
            'sell_price_wt' => $price,
            'sell_price' => $price_nt,
            'price_nt' => $price_nt,
        ];
    }

    private function price($val)
    {
        if ($val <= 0) return 0;
        $val = str_replace(',', '.', $val);
        return number_format((float)$val, 3, '.', '');
    }

    protected function getProductModelBySku($product, $morellatoOnly = true)
    {
        if ($morellatoOnly) {
            $model = \Product::where(function ($query) use ($product) {
                $query->where('sku', $product->sku)->orWhere('sap_sku', $product->sap_sku)->orWhere('sku', $product->sap_sku);
            })->where('source', 'morellato')->first();
        } else {
            $model = \Product::where(function ($query) use ($product) {
                $query->where('sku', $product->sku)->orWhere('sap_sku', $product->sap_sku)->orWhere('sku', $product->sap_sku);
            })->where(function ($query) use ($product) {
                $query->whereNull('source')->orWhere('source', 'local');
            })->first();
        }

        return $model;
    }


    protected function getProductCombinationBySku($product)
    {
        $combination = \ProductCombination::where(function ($query) use ($product) {
            $query->where('sku', $product->sku)->orWhere('sap_sku', $product->sap_sku)->orWhere('sku', $product->sap_sku);
        })->first();

        return $combination;
    }


    protected function importProduct($product)
    {
        $action = 'insert';
        $product_id = null;

        $oldModel = $this->getProductModelBySku($product, false);
        if ($oldModel and $oldModel->id > 0) {
            $action = 'update';
        }
        if ($oldModel and $oldModel->id > 0) {
            $model = $oldModel;
        } else {
            $model = $this->getProductModelBySku($product);
            if ($model)
                $action = 'update';
        }

        $this->getConsole()->line("Prepare to ($action) product with sku [$product->sku]");
        $prices = $this->parsePrices($product->price);

        if ($this->onlyPrices === true) {
            if (isset($model) and $model != null) {
                $data = [
                    'price' => $prices['price'],
                    'sell_price_wt' => $prices['sell_price_wt'],
                    'sell_price' => $prices['sell_price'],
                    'price_nt' => $prices['price_nt'],
                ];
                $this->getConsole()->line("Updating prices for product [$product->sku]");
                if ($this->write) {
                    DB::table('products')->where('id', $model->id)->update($data);
                }
                return $model->id;
            } else {
                $this->getConsole()->comment("No product found with sku [$product->sku] - cannot update prices");
            }
            return null;
        }

        $data = [
            'sku' => $product->sku,
            'sap_sku' => $product->sap_sku,
            'mastersku' => $product->mastersku,
            'ean13' => $product->ean,
            'price' => $prices['price'],
            'sell_price_wt' => $prices['sell_price_wt'],
            'sell_price' => $prices['sell_price'],
            'price_nt' => $prices['price_nt'],
            'brand_id' => $product->relations['brand'],
            'collection_id' => $product->relations['collection'],
            'default_category_id' => $product->relations['category'][0]['id'],
            'main_category_id' => $product->relations['category'][0]['parent_id'],
        ];
        $excel_source = (isset($product->source) and strlen($product->source) > 0) ? $product->source : 'morellato';
        if(isset($product->outlet) and $product->outlet == 1){
            $data['is_outlet'] = 1;
        }

        if (isset($product->new_from_date))
            $data['new_from_date'] = $product->new_from_date;

        if (isset($product->new_to_date))
            $data['new_to_date'] = $product->new_to_date;

        if ($data['main_category_id'] <= 0)
            $data['main_category_id'] = $data['default_category_id'];

        if ($oldModel and $oldModel->id > 0) {
            $data['is_out_of_production'] = 0;
            $data['cnt_attributes'] = 0;
            $data['cnt_categories'] = 0;
        }

        //bind translations to the data source model
        foreach ($product->translations as $locale => $translation) {
            unset($translation->lang_id);
            $translation->published = 1;
            $translation->slug = Str::slug($translation->name);
            $data[$locale] = $translation;
        }

        $this->log($data, 'DATA TO UPSERT');


        if ($action == 'insert') {
            //INSERT PRODUCT HERE
            $product_id = $this->product_id;
            $model = new \Product();
            $data['is_out_of_production'] = 0;
            $data['id'] = $product_id;
            $data['position'] = $product_id;
            $data['source'] = $excel_source;
            $attributes = $model->make($data, true);
            $this->log($attributes, 'PRODUCTS FILLABLE INSERT');
            if ($this->write) {
                $model->save();
            }

            $this->importProductAttributes($product, $product_id);
            $this->importProductCategories($product, $product_id);

            $this->getConsole()->info("Product [$product->sku] successfully inserted with id [$product_id]");
            $this->getConsole()->line('----------------- DONE -----------------');
            $this->insertAction($product_id, 'insert', 'excel');

            //update the general product id
            $this->product_id++;
        }

        //during the update only the main data are updated; in order to update the relations the records are deleted and re-inserted
        if ($action == 'update') {

            if (is_null($model)) {
                $model = $this->getProductModelBySku($product, false);
            }
            if (is_null($model)) {
                $this->getConsole()->error("Cannot find product to update ($product->sku)");
            }
            $product_id = $model->id;
            //now update the model
            $modelData = $model->toArray();
            $data = array_merge($modelData, $data);
            $data['cnt_attributes'] = 0;
            $data['cnt_categories'] = 0;
            $data['is_out_of_production'] = 0;
            if (strlen($data['source']) == 0) {
                $data['source'] = $excel_source;
            }
            $attributes = $model->make($data, true);
            $this->log($attributes, 'PRODUCTS UPDATE COMPLETE DATA');
            if ($this->write) {
                $model->save();
            }

            //re-insert all relations
            $this->clearProductAttributes($product_id);
            $this->importProductAttributes($product, $product_id);
            $this->importProductCategories($product, $product_id);

            $this->insertAction($product_id, 'update', 'excel');

            $this->getConsole()->info("Product [$product->sku] successfully updated with id [$product_id]");
            $this->getConsole()->line('----------------- DONE -----------------');
        }

        return $product_id;
    }


    protected function clearProductAttributes($product_id)
    {
        if ($this->write) {
            if ($this->deleteAllRelations) {
                DB::table('products_attributes')
                    ->where('product_id', $product_id)
                    ->delete();

                DB::table('products_attributes_position')
                    ->where('product_id', $product_id)
                    ->delete();
            } else {
                DB::table('products_attributes')
                    ->where('product_id', $product_id)
                    ->whereIn('attribute_id', $this->erasable_attributes)
                    ->delete();

                DB::table('products_attributes_position')
                    ->where('product_id', $product_id)
                    ->whereIn('attribute_id', $this->erasable_attributes)
                    ->delete();
            }

        }
    }


    protected function importProductAttributes($product, $product_id)
    {
        //now insert attributes

        $callback = function ($attribute, $counter, $name, $product_id) {
            $products_attributes = [
                'product_id' => $product_id,
                'attribute_id' => $attribute['attribute_id'],
                'attribute_val' => $attribute['attribute_option_id'],
            ];

            $this->log($products_attributes, 'PRODUCTS ATTRIBUTES INSERT => ' . $name);

            if ($this->write) {
                $exists = DB::table('products_attributes')
                    ->where('product_id', $products_attributes['product_id'])
                    ->where('attribute_id', $products_attributes['attribute_id'])
                    ->where('attribute_val', $products_attributes['attribute_val'])
                    ->first();
                if (is_null($exists)) {

                    //if single, remote any existing reference
                    if ($this->attributeNameCanBeSingle($name)) {
                        DB::table('products_attributes')
                            ->where('product_id', $products_attributes['product_id'])
                            ->where('attribute_id', $products_attributes['attribute_id'])
                            ->delete();
                    }

                    DB::table('products_attributes')->insert($products_attributes);
                }
            }
        };


        $callbackUpdatePosition = function ($attribute, $counter, $name, $product_id) {

            $products_attributes_position = [
                'product_id' => $product_id,
                'attribute_id' => $attribute['attribute_id'],
                'position' => $counter,
            ];

            $this->log($products_attributes_position, 'PRODUCTS ATTRIBUTES POSITION INSERT => ' . $name);

            if ($this->write) {
                $exists = DB::table('products_attributes_position')
                    ->where('product_id', $products_attributes_position['product_id'])
                    ->where('attribute_id', $products_attributes_position['attribute_id'])
                    ->first();
                if (is_null($exists)) {
                    DB::table('products_attributes_position')->insert($products_attributes_position);
                }
            }
        };

        $counter = 1;

        foreach ($product->attributes as $name => $attribute) {
            if (is_array($attribute) and !empty($attribute)) {

                if (isset($attribute['attribute_id'])) { //single attribute
                    $callback($attribute, $counter, $name, $product_id);
                    $callbackUpdatePosition($attribute, $counter, $name, $product_id);
                    $counter++;
                } else {
                    foreach ($attribute as $item) { //multiple attributes
                        $callback($item, $counter, $name, $product_id);
                    }
                    $callbackUpdatePosition($attribute[0], $counter, $name, $product_id);
                    $counter++;
                }
            }
        }
    }


    protected function importProductCategories($product, $product_id)
    {
        //now insert categories
        if (!empty($product->relations['category'])) {
            $categories = $product->relations['category'];
            foreach ($categories as $category) {
                $category_data = [
                    'category_id' => $category['id'],
                    'product_id' => $product_id,
                ];
                $this->log($category_data, 'PRODUCTS CATEGORIES INSERT');
                if ($this->write) {
                    try {
                        DB::table('categories_products')->insert($category_data);
                    } catch (\Exception $e) {

                    }
                }
            }
        }
    }


    function importVariants($mastersku, $variant)
    {
        $product_id = 0;
        $console = $this->getConsole();
        $this->log("Importing variants for mastersku: $mastersku");
        $masterProduct = \Product::where('sku', $mastersku)->orWhere('mastersku', $mastersku)->first();
        if ($masterProduct) {
            $product_id = $masterProduct->id;
        } else {
            $firstProduct = DB::table($this->table)->where('sku', $mastersku)->first();
            if ($firstProduct) {
                $console->comment("Default mastersku not found product [$mastersku]");
                $product = $this->product($firstProduct);
                $this->importProduct($product);
                $masterProduct = \Product::where('sku', $mastersku)->orWhere('mastersku', $mastersku)->first();
                if ($masterProduct) {
                    $product_id = $masterProduct->id;
                } else {
                    $this->getConsole()->error("Cannot find master product for masterSku [$mastersku]");
                    $this->errors[] = $mastersku;
                    return;
                }
            } else {
                $this->getConsole()->error("Cannot find master product for masterSku [$mastersku]");
                $this->errors[] = $mastersku;
                return;
            }
        }

        if ($this->onlyPrices === true and $product_id <= 0) {
            $this->getConsole()->error("When updating prices the Master product must be present...");
            return;
        }

        $fieldName = 'Misura';

        \Product::where('id', $product_id)->update(['is_out_of_production' => 0]);

        //Utils::watch();
        //variants
        $combinations = DB::table($this->table)
            ->where('mastersku', $mastersku)
            ->orderBy('sku')->get();

        $many = count($combinations);
        $this->getConsole()->comment("Found [$many] combinations for [$mastersku]");

        if ($this->onlyPrices === true) {

            foreach ($combinations as $combination) {

                $product = $this->product($combination);

                $data = [
                    'buy_price' => $product->price,
                ];

                $model = DB::table('products_combinations')->where('product_id', $product_id)->where(function ($query) use ($combination) {
                    $query->where('sku', $combination->sku)->orWhere('sap_sku', $combination->sap_sku)->orWhere('sku', $combination->sap_sku);
                })->first();

                if ($model) {
                    $product_combination_id = $model->id;
                    $this->getConsole()->comment("Updating variants prices for [$combination->sku]");

                    //UPDATE THE COMBINATION HERE
                    if ($this->write) {
                        if ($model and $model->id) {
                            DB::table('products_combinations')->where('id', $product_combination_id)->update($data);
                        }
                    }

                } else {
                    $this->getConsole()->error("Could not find combination [$combination->sku]");
                }
            }
            return;
        }

        //clear combinations
        if ($this->deleteAllRelations) {
            $old_combinations = DB::table('products_combinations')->where('product_id', $product_id)->lists('id');
            DB::table('products_combinations')->where('product_id', $product_id)->delete();
            DB::table('products_combinations_attributes')->whereIn('combination_id', $old_combinations)->delete();
        }

        //create variants (combinations)

        foreach ($combinations as $combination) {

            $product = $this->product($combination);

            $falsy = $this->nullish($product->size1);

            if ($falsy) {
                $this->combinations_errors[] = $product->sku;
                $this->getConsole()->error("Warning: $product->sku as falsy size variant");
                continue;
            }

            $attributes_options_mapping = [];
            $attributes_options_mapping[] = $this->cacheRead('size', $product->size1);

            $data = [
                'name' => $fieldName . ': ' . $product->size1,
                'product_id' => $product_id,
                'sku' => $product->sku,
                'sap_sku' => $product->sap_sku,
                'ean13' => $product->ean,
                'buy_price' => $product->price,
                'price' => 0,
            ];

            $action = 'insert';
            $count = DB::table('products_combinations')->where('product_id', $product_id)->where(function ($query) use ($combination) {
                $query->where('sku', $combination->sku)->orWhere('sap_sku', $combination->sap_sku)->orWhere('sku', $combination->sap_sku);
            })->count();
            if ($count > 0)
                $action = 'update';

            $this->log($action, "ImportAction for combination [$combination->sku]");
            $this->getConsole()->line("($action) ImportAction for combination [$combination->sku]");

            $product_combination_id = $this->product_combination_id;

            if ($action == 'insert') {
                $data['id'] = $product_combination_id;
                $data['quantity'] = 0;
                $this->log($data, 'PRODUCT COMBINATION INSERT DATA');

                //INSERT THE COMBINATION HERE
                if ($this->write) {
                    DB::table('products_combinations')->insert($data);
                }
                $this->product_combination_id++;
            }

            if ($action == 'update' || $action == 'skip') {
                $model = DB::table('products_combinations')->where('product_id', $product_id)->where(function ($query) use ($combination) {
                    $query->where('sku', $combination->sku)->orWhere('sap_sku', $combination->sap_sku)->orWhere('sku', $combination->sap_sku);
                })->first();
                if ($model and $model->id) {
                    $product_combination_id = $model->id;
                }
                $this->log($data, 'PRODUCT COMBINATION UPDATE DATA');
                //UPDATE THE COMBINATION HERE
                if ($this->write) {
                    if ($model and $model->id) {
                        DB::table('products_combinations')->where('id', $product_combination_id)->update($data);
                    } else {
                        DB::table('products_combinations')->insert($data);
                        $this->product_combination_id++;
                    }
                }
            }

            if (!empty($attributes_options_mapping)) {

                foreach ($attributes_options_mapping as $mapping) {
                    $combination_options = [
                        'combination_id' => $product_combination_id,
                        'option_id' => $mapping['attribute_option_id'],
                        'attribute_id' => $mapping['attribute_id'],
                    ];

                    $exists = DB::table('products_combinations_attributes')
                        ->where('combination_id', $combination_options['combination_id'])
                        ->where('option_id', $combination_options['option_id'])
                        ->where('attribute_id', $combination_options['attribute_id'])
                        ->first();

                    $this->log($combination_options, 'PRODUCT COMBINATION MAPPING');

                    if (is_null($exists)) {
                        //INSERT THE MAPPING HERE
                        if ($this->write) {
                            DB::table('products_combinations_attributes')->insert($combination_options);
                        }
                    }

                }
            }

        }

    }

    public function parseCanvas($canvas)
    {
        return \ProductHelper::parseCanvas($canvas);
    }

    protected function addAutomaticAttributes()
    {
        return [];
    }

    protected function finishing()
    {
        /**
         * Set all new imported products is_out_of_production=0
         * UPDATE products SET is_out_of_production=1;
         * UPDATE products SET is_out_of_production=0 WHERE DATE(updated_at)='2018-02-01';
         *
         * Delete all combinations that are not contained in the 'import_jewels'
         * select * from products_combinations where sku not in (select sku from import_excel_jewels where mastersku!='') and product_id in (select id from products where ISNULL(deleted_at) and is_out_of_production=0)
         */
    }


}