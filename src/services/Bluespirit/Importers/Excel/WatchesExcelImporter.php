<?php
namespace services\Bluespirit\Importers\Excel;

class WatchesExcelImporter extends ExcelImporter
{

    protected $table = 'import_excel_watches';
    protected $product_name_rule = [
        'it' => ['parent_category', 'brand', 'collection', 'sku'],
        'es' => ['parent_category', 'brand', 'collection', 'sku'],
        'fr' => ['parent_category', 'brand', 'collection', 'sku'],
        'en' => ['brand', 'collection', 'parent_category', 'sku'],
        'de' => ['brand', 'collection', 'parent_category', 'sku'],
    ];

    protected function mapping()
    {
        return [
            'nav' => 'bs-nav',
            'gender' => 'gender',
            'case_material' => 'case_material',
            'case_size' => 'case_size',
            'case_thickness' => 'case_thickness',
            'finishing' => 'finishing',
            'case_colour' => 'case_colour',
            'glass' => 'glass',
            'strap_material' => 'strap_material',
            'strap_colour' => 'strap_colour',
            'closure' => 'closure',
            'movement' => 'movement',
            'swiss_made' => 'swiss_made',
            'functions' => 'functions',
            'water_resistant' => 'water_resistant',
        ];
    }




    protected function addAutomaticAttributes(){
        $attributes = [];

        //garanzia 2 anni
        $attributes['guarantee'] = [[
            'attribute_id' => 13,
            'attribute_option_id' => 36,
        ]];

        //confezione originale
        $attributes['package'] = [[
            'attribute_id' => 12,
            'attribute_option_id' => 34,
        ]];

        return $attributes;
    }




    public function run()
    {
        $this->getConsole()->line(__METHOD__);
        parent::run();
        $this->warmUpCache();
        $this->importProducts();
    }




}