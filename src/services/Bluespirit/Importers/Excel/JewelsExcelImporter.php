<?php
namespace services\Bluespirit\Importers\Excel;



/***
 * Class JewelsExcelImporter
 * @package services\Bluespirit\Importers\Excel
 *
 * Import schema
 *
 *
 * excel                    |        Ecommerce
 * size1 (misura anelli)    |        size 19
 * size2 (misura bra-col)    |        size-cm 250
 * size3 (misura bra-col)    |        size-cm    250
 * nav (categoria menu)    |        bs-nav 215
 * model (modello)            |        modello 239
 * material (materiale)    |        material 17
 * material_weight (peso)    |        weight 149
 * pearl (perle)            |        pearls 42
 * pearl_origin (origine)    |        pearls_origin 87
 * pearl_size (diam. perle)|        pearls_diameter 88
 * pearl_color (col. perle)|        pearls_color 75
 * pearl_shape (forma perl)|        pearls_shape 33
 * stone (nome pietra)        |        stone 80
 * stone_color (col. pietr)|        colore_pietra 242
 * stone_cut (taglo pietra)|        stone_cut 106
 * stone_karats (caratura) |        carats 105
 * diamond_purity (purezza)|        diamond_purity 84
 * diamond_range (scala)    |        diamond_color 83
 * shape (forma)            |        forma 244
 */
class JewelsExcelImporter extends ExcelImporter
{


    protected $table = 'import_excel_jewels';
    protected $product_name_rule = [
        'it' => ['category', 'brand', 'collection', 'sku'],
        'es' => ['category', 'brand', 'collection', 'sku'],
        'fr' => ['category', 'brand', 'collection', 'sku'],
        'en' => ['brand', 'collection', 'category', 'sku'],
        'de' => ['brand', 'collection', 'category', 'sku'],
    ];

    protected function mapping()
    {
        return [
            'gender' => 'gender',
            'size1' => 'size',
            'size2' => 'size',
            'size3' => 'size-cm',
            'nav' => 'bs-nav',
            'model' => 'modello',
            'material' => 'material',
            'material_weight' => 'weight',
            'pearl' => 'pearls',
            'pearl_origin' => 'pearls_origin',
            'pearl_size' => 'pearls_diameter',
            'pearl_color' => 'pearls_color',
            'pearl_shape' => 'pearls_shape',
            'stone' => 'stone',
            'stone_color' => 'colore_pietra',
            'stone_cut' => 'stone_cut',
            'stone_karats' => 'carats',
            'diamond_purity' => 'diamond_purity',
            'diamond_range' => 'diamond_color',
            'shape' => 'forma',
            'treatment' => 'treatment',
            'symbol' => 'symbol',
        ];
    }




    protected function addAutomaticAttributes(){
        $attributes = [];

        //certificate_auth
        $attributes['certificate_auth'] = [[
            'attribute_id' => 20,
            'attribute_option_id' => 1,
        ]];

        //confezione originale
        $attributes['package'] = [[
            'attribute_id' => 12,
            'attribute_option_id' => 34,
        ]];

        return $attributes;
    }


    public function run()
    {
        $this->getConsole()->line(__METHOD__);
        parent::run();
        $this->warmUpCache();
        $this->importProducts();
        $this->importCombinations();
    }

}