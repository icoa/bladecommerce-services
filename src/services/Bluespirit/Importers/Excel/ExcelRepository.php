<?php
namespace services\Bluespirit\Importers\Excel;

use Illuminate\Support\Facades\Config;
use Illuminate\Console\Command;
use DB;

class ExcelRepository
{

    protected $console;
    protected $excelImporter;
    protected $watchesExcelImporter;
    protected $jewelsExcelImporter;
    protected $strapsExcelImporter;
    protected $onlyPrices = false;

    function __construct(Command $console, WatchesExcelImporter $watchesExcelImporter, JewelsExcelImporter $jewelsExcelImporter, StrapsExcelImporter $strapsExcelImporter)
    {
        $this->console = $console;
        $this->watchesExcelImporter = $watchesExcelImporter;
        $this->jewelsExcelImporter = $jewelsExcelImporter;
        $this->strapsExcelImporter = $strapsExcelImporter;
    }

    /**
     * @param $source
     * @return $this
     */
    function setSource($source)
    {
        switch ($source) {
            case 'watches':
                $this->excelImporter = $this->watchesExcelImporter;
                break;
            case 'jewels':
                $this->excelImporter = $this->jewelsExcelImporter;
                break;
            case 'straps':
                $this->excelImporter = $this->strapsExcelImporter;
                break;
        }

        $this->excelImporter->setConsole($this->console);
        return $this;
    }

    /**
     * @param bool $enabled
     * @return $this
     */
    function setOnlyPrices($enabled = false)
    {
        $this->onlyPrices = $enabled;
        return $this;
    }

    function make()
    {
        $this->excelImporter->onlyPrices = $this->onlyPrices;
        $this->excelImporter->run();
    }
}