<?php
namespace services\Bluespirit\Importers\Excel;

use Utils;
use DB;

/***
 * Class StrapsExcelImporter
 * @package services\Bluespirit\Importers\Excel
 *
 * Import schema
 *
 *
 */
class StrapsExcelImporter extends ExcelImporter
{


    protected $table = 'import_excel_straps';

    protected $product_name_rule = [
        'it' => ['category', 'brand', 'bs-nav', 'collection', 'color', 'sku'],
        'es' => ['category', 'brand', 'bs-nav', 'collection', 'color', 'sku'],
        'fr' => ['category', 'brand', 'bs-nav', 'collection', 'color', 'sku'],
        'en' => ['brand', 'bs-nav', 'collection', 'color', 'category', 'sku'],
        'de' => ['brand', 'bs-nav', 'collection', 'color', 'category', 'sku'],
    ];

    protected function mapping()
    {
        return [
            'gender' => 'gender',
            'size1' => 'size',
            'size2' => 'size',
            'size3' => 'size-cm',
            'nav' => 'bs-nav',
            'material' => 'material',
            'color' => 'color',
            'compatibility' => 'compatibility',
            'water_resistant' => 'water_resistant',
            'material_detail' => 'material_detail',
            'closure' => 'closure',
            'easy_click' => 'easy_click',
            'cites' => 'cites',
        ];
    }


    protected function addAutomaticAttributes()
    {
        $attributes = [];

        //confezione originale
        $attributes['package'] = [[
            'attribute_id' => 12,
            'attribute_option_id' => 1,
        ]];

        return $attributes;
    }


    protected function importProducts()
    {
        $console = $this->getConsole();
        $console->line(__METHOD__);

        Utils::watch();

        $rows = DB::table($this->table)
            ->where('category', '<>', '')
            ->where('gender', '<>', '')
            ->whereNull('mastersku')
            ->orderBy('sku')
            //->take(10)
            ->get();

        DB::beginTransaction();
        foreach ($rows as $row) {
            $console->line("Import product [$row->sku]");
            $product = $this->product($row);
            $this->importProduct($product);
        }
        DB::commit();

        $rows = DB::table($this->table)
            ->where('category', '<>', '')
            ->where('gender', '<>', '')
            ->groupBy('mastersku')
            ->orderBy('sku')
            //->take(10)
            ->get();

        DB::beginTransaction();
        foreach ($rows as $row) {
            $console->line("Import product [$row->sku]");
            $product = $this->product($row);
            $this->importProduct($product);
        }
        DB::commit();
    }


    public function prepare()
    {
        if($this->onlyPrices)
            return;

        $query = "UPDATE $this->table SET mastersku = SUBSTR(sku, 1, 14), brand = 'Morellato';";
        DB::statement($query);

        $query = "UPDATE $this->table SET material='PELLE WATER RESISTANT' WHERE material='pelli water resistant'";
        DB::statement($query);

        $query = "UPDATE $this->table SET material='PELLE LISCIA' WHERE material='pelli liscie'";
        DB::statement($query);

        $query = "UPDATE $this->table SET material='PELLE PREZIOSA' WHERE material='pelli preziose'";
        DB::statement($query);

        //re-group mastersku logical products
        $mastersku = null;
        $pivot = null;
        DB::beginTransaction();
        $rows = DB::table($this->table)->orderBy('mastersku')->orderBy('sku')->get();
        foreach ($rows as $row) {
            if ($mastersku != $row->mastersku) {
                $mastersku = $row->mastersku;
                $pivot = $row->sku;
                DB::table($this->table)->where('sku', $pivot)->update(['mastersku' => $pivot]);
            }else{
                DB::table($this->table)->where('mastersku', $mastersku)->update(['mastersku' => $pivot]);
            }
        }
        DB::commit();
    }

    public function run()
    {
        $this->getConsole()->line(__METHOD__);
        parent::run();
        $this->prepare();
        $this->warmUpCache();
        $this->importProducts();
        $this->importCombinations();
    }

}