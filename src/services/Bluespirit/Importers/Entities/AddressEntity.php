<?php
namespace services\Bluespirit\Importers\Entities;

use DB;

class AddressEntity extends ExcelEntityImporter
{
    protected $table = 'import_entities_address';
    protected $file_pattern = 'Blade_Import_Address';
    protected $skip_first_row = false;
    
    protected $headings = [
                'id',
                'country_id',
                'state_id',
                'customer_id',
                'people_id',
                'billing',
                'alias',
                'company',
                'lastname',
                'firstname',
                'address1',
                'address2',
                'postcode',
                'city',
                'other',
                'phone',
                'mobile_phone',
                'fax',
                'vat_number',
                'cf',
                'extrainfo',
                'created_at',
                'updated_at',
            ];
            
    protected $rules = [
                'id' => 'required',
                'country_id' => 'required',
                'state_id' => 'required',
                'customer_id' => 'required',
                'people_id' => 'required',
                'company' => 'required_if:people_id,2',
                'lastname' => 'required_if:people_id,1',
                'firstname' => 'required_if:people_id,1',
                'address1' => 'required',
                //'address2' => 'required',
                'postcode' => 'required',
                'city' => 'required',
                'phone' => 'required'
        ];
            
    protected function avoidDuplicate($obj){
        DB::table($this->table)->where('id',$obj->id)->delete();
    }
  
    public function checkEntity(){
        $this->console->comment("Checking Importer  " . static::class ." ...");
        $records =  DB::table($this->table)->get();

        $this->rules['country_id'] = 'required|exists:import_entities_countries,id';
        $this->rules['state_id'] = 'required|exists:import_entities_states,id';
        $this->rules['customer_id'] = 'required|exists:import_entities_customers,id';

        foreach($records as $record){
            $this->rules['state_id'] = ($record->state_id == 0) ? 'required' : 'required|exists:import_entities_states,id';
            $this->validate($record);
            continue;
        }
    }
    
    
    
}
