<?php
namespace services\Bluespirit\Importers\Entities;

use DB;
class CountriesEntity extends ExcelEntityImporter
{
    protected $table = 'import_entities_countries';
    protected $file_pattern = 'Blade_Import_Countries';
    protected $skip_first_row = false;
    protected $headings = [
                'id',
                'name',
                'currency_id',
                'iso_code',
                'iso_code3',
    ];
    
    protected $rules = [
        'id' => 'required',
        'currency_id' => 'required',
        'name' => 'required',
        'iso_code' => 'required|max:3'
    ];
            
    protected function avoidDuplicate($obj){
        DB::table($this->table)->where('id',$obj->id)->delete();
    }
    
    
    function checkEntity(){
        $this->console->comment("Checking Importer  " . static::class ." ...");
        $records =  DB::table($this->table)->get();
        $this->rule['currency_id'] = 'required|exists:import_entities_currencies,id';
        
        foreach($records as $record){
            $this->validate($record);
            continue;
        }
    }
}
