<?php
namespace services\Bluespirit\Importers\Entities;

use DB;
class CustomersGroupsEntity extends ExcelEntityImporter
{
    protected $table = 'import_entities_customers_groups';
    protected $file_pattern = 'Blade_Import_CustomersGroups';
    protected $skip_first_row = false;

    protected $headings = [
                'id',
                'name'
    ];
            
    protected $rules = [
         'id' => 'required',
         'name' => 'required'
    ];
    
    
    protected function avoidDuplicate($obj){
        DB::table($this->table)->where('id',$obj->id)->where('name',$obj->name)->delete();
    }
    
}
