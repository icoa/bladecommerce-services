<?php
namespace services\Bluespirit\Importers\Entities;

use DB;
class Sales_ProductsEntity extends ExcelEntityImporter
{
    protected $table = 'import_entities_sales_products';
    protected $file_pattern = 'Blade_Import_Sales_Products';
    protected $skip_first_row = false;
 
    protected $headings = [
            'order_id',
            'sku',
            'sapsku',
            'product_name',
            'product_quantity',
            'product_price',
            'reduction_percent',
            'reduction_amount',
            'product_ean13',
            'product_weight',
            'tax_rate',
            'total_price_tax_incl',
            'price_tax_incl',
            'availability_mode',
            'availability_shop_id',
            'warehouse',
            'created_at',
            'updated_at'
    ];
    
    protected $rules = [
        
            'order_id' => 'required',
            'sku' => 'required',
            //'sapsku' => 'required',
            'product_name' => 'required',
            'product_quantity' => 'required',
            'product_price' => 'required',
            //'product_ean13' => 'required',
            'product_weight' => 'required',
            'tax_rate' => 'required',
            'total_price_tax_incl' => 'required',
            'price_tax_incl' => 'required',
            //'availability_mode' => 'required'
    ];

    function checkEntity(){
        $this->console->comment("Checking Importer  " . static::class ." ...");
        $records =  DB::table($this->table)
            //->take(1000)
            ->get();

        $this->rules['order_id'] = 'required|exists:import_entities_sales,id';
        
        foreach($records as $record){
            $this->validate($record);
            continue;
        }
    }
    
    
            
}
