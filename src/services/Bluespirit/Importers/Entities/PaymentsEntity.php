<?php
namespace services\Bluespirit\Importers\Entities;

use DB;
class PaymentsEntity extends ExcelEntityImporter
{
    protected $table = 'import_entities_payments';
    protected $file_pattern = 'Blade_Import_Payments';
    protected $skip_first_row = false;

    protected $headings = [
                'id',
                'module',
                'name',
                'online',
                'additional_cost',
                'currency_id'
    ];
    
    protected $rules = [
                'id' => 'required',
                'module' => 'required',
                'name' => 'required',
                'online' => 'required|boolean',
                'additional_cost' => 'required',
                'currency_id' => 'required',
    ];
    
    protected function avoidDuplicate($obj){
        DB::table($this->table)->where('id',$obj->id)->where('module',$obj->module)->delete();
    }
    
    function checkEntity(){
        $this->console->comment("Checking Importer  " . static::class ." ...");
        $records =  DB::table($this->table)->get();
        $this->rule['currency_id'] = 'required|exists:import_entities_currencies,id';
        
        foreach($records as $record){
            $this->validate($record);
            continue;
        }
    }
}
