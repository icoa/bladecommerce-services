<?php

namespace services\Bluespirit\Importers\Entities;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Config;
use DB, Cache;
use Hash;
use File;

class UpsertEntities
{

    protected $console;
    protected $debug = true;
    protected $purge = true;
    protected $write = true;
    protected $deleteAllRelations = false;
    protected $cache = [];
    protected $errors = [];
    protected $combinations_errors = [];
    protected $table = 'table';
    protected $tables = 'tables';
    protected $external_relations;
    protected $tables_with_external_relations = [];
    protected $mode = 'basic';
    protected $logFile = '';
    protected $localDebug = false;

    function __construct(Command $console)
    {
        $this->console = $console;
        $this->tables = Config::get('entities.temp_tables');
        $this->external_relations = config('entities.external_relations', []);
        $this->tables_with_external_relations = config('entities.tables_with_external_relations', []);
        $this->startCache();
        $this->createLogFile();
    }

    public function make()
    {


        foreach ($this->tables as $table) {
            DB::beginTransaction();
            $this->table = 'import_entities_' . $table;
            $this->importEntities();
            DB::commit();
        }

    }

    function createLogFile()
    {
        $path = '/logs/entities/';
        $logPath = storage_path('logs/entities/');

        if (!File::isDirectory($logPath)) {
            File::makeDirectory($logPath, 777);
        }

        $this->logFile = $logPath . date('Ymd-His.') . 'upsert_entities.log';

    }

    protected function startCache()
    {
        $cacheKey = 'entities_importer_cache_relations';
        if (Cache::has($cacheKey)) {
            $this->cache = Cache::get($cacheKey);
            $this->console->comment($cacheKey . '  Getting cache');
        } else {
            Cache::put($cacheKey, $this->cache, 60);
            $this->console->comment($cacheKey . '  Saving cache');
        }
    }

    protected function cacheStore($tag, $key, $value)
    {
        $this->cache[$tag][$key] = $value;
    }

    protected function cacheRead($tag, $key, $default = null)
    {
        return isset($this->cache[$tag][$key]) ? $this->cache[$tag][$key] : $default;
    }

    protected function nullish($str)
    {
        $str = trim($str);
        return ($str == '' or is_null($str) or $str == 'null' or $str == '.' or $str == '0' or str_contains(Str::upper($str), 'TO BE DEFINED'));
    }

    protected function sanitize($str)
    {
        $str = str_replace(['\\', ':', ';', '#', '?'], '', trim($str));
        return $str;
    }

    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    protected function logError($msg)
    {
        File::append($this->logFile, $msg . PHP_EOL);
    }

    protected function parseLang($lang_id)
    {
        return substr($lang_id, 0, 2);
    }

    protected function parseLangByWebsite($website)
    {
        $lang_id = Str::lower(substr($website, -2, 2));
        if (!in_array($lang_id, ['it', 'es', 'de', 'fr', 'en']))
            $lang_id = 'it';

        return $lang_id;
    }

    protected function importEntities()
    {
        \Utils::watch();
        $this->console->line("Importing entities for table " . $this->table);
        $builder = DB::table($this->table);

        if (in_array($this->table, $this->tables_with_external_relations)) {
            $builder->whereIn('website', $this->external_relations);
        }
        if ($this->table == 'import_entities_address') {
            $builder->whereIn('customer_id', function ($query) {
                $query->select('id')->from('import_entities_customers')->whereIn('website', $this->external_relations);
            });
        }
        /*if ($this->table == 'import_entities_sales_products') {
            $builder->where('order_id', 2001);
        }*/
        /*if ($this->table == 'import_entities_sales') {
            //$builder->where('id', 36203);
            $builder->where('id', 2001);
        }*/
        if ($this->table == 'import_entities_sales_products') {
            $builder->whereIn('order_id', function ($query) {
                $query->select('id')->from('import_entities_sales')->whereIn('website', $this->external_relations);
            });
        }
        if ($this->localDebug) {
            $builder->take(20);
        }
        $rows = $builder->orderBy('id', 'desc')->get();

        $many = count($rows);
        $this->total = $many;
        $this->console->comment("Found [$many] records to process...");
        $counter = 0;

        $upsertMethod = $this->upsertMethod();
        foreach ($rows as $row) {
            $counter++;
            $this->console->line("Processing record $counter/$many");
            if ($upsertMethod) {
                $this->$upsertMethod($row);
            }
        }

        return true;
    }

    protected function upsertMethod()
    {
        $method = null;
        switch ($this->table) {
            case 'import_entities_customers_groups':
                return 'upsertCustomerGroupsTable';
                break;
            case 'import_entities_shops':
                if ($this->mode != 'basic') {
                    return 'upsertShopsTable';
                }
                break;
            case 'import_entities_customers':
                return 'upsertCustomersTable';
                break;
            case 'import_entities_newsletter':
                return 'upsertNewslettersTable';
                break;
            case 'import_entities_address':
                return 'upsertAddressTable';
                break;
            case 'import_entities_sales':
                return 'upsertSalesTable';
                break;
            case 'import_entities_sales_products':
                return 'upsertSalesProductsTable';
                break;
            case 'import_entities_states':
                return 'upsertStatesTable';
                break;
        }
        return $method;
    }

    protected function prepareUpsert($row)
    {
        switch ($this->table) {
            case 'import_entities_customers_groups':
                $this->upsertCustomerGroupsTable($row);
                break;
            case 'import_entities_shops':
                if ($this->mode != 'basic') {
                    $this->upsertShopsTable($row);
                }
                break;
            case 'import_entities_customers':
                $this->upsertCustomersTable($row);
                break;
            case 'import_entities_newsletter':
                $this->upsertNewslettersTable($row);
                break;
            case 'import_entities_address':
                $this->upsertAddressTable($row);
                break;
            case 'import_entities_sales':
                $this->upsertSalesTable($row);
                break;
            case 'import_entities_sales_products':
                $this->upsertSalesProductsTable($row);
                break;
            default:
                $this->console->comment('Entity Not Found');
                break;
        }
    }

    /**
     *  Input id of entity from relations
     *
     *  Returns id
     *
     **/

    private function getCountryId($id)
    {

        $cacheId = $this->cacheRead('countries', $id);
        if (isset($cacheId)) {
            return $cacheId;
        } else {
            $countryTemp = DB::table('import_entities_countries')->find($id);

            $row = \Country::where('iso_code', $countryTemp->iso_code)->first();

            if (isset($row)) {
                $this->cacheStore('countries', $id, $row->id);
                return $row->id;
            } else {
                return null;
            }
        }
    }

    private function getStatesId($id)
    {
        if ($id == 0)
            return null;

        $cacheId = $this->cacheRead('states', $id);
        if (isset($cacheId)) {
            return $cacheId;
        } else {

            $statesTemp = DB::table('import_entities_states')->find($id);

            $row = \State::where('iso_code', $statesTemp->iso_code)->first();

            if (isset($row)) {
                $this->cacheStore('states', $id, $row->id);
                return $row->id;
            } else {
                return null;
            }
        }
    }

    private function getShopsId($id)
    {

        $cacheId = $this->cacheRead('shops', $id);
        if (isset($cacheId)) {
            return $cacheId;
        } else {
            $shopsTemp = DB::table('import_entities_shops')->find($id);

            $row = DB::table('mi_shops')->where('cd_neg', $shopsTemp->cd_neg)->whereNull('deleted_at')->first();

            if (isset($row)) {
                $this->cacheStore('shops', $id, $row->id);
                return $row->id;
            } else {
                return null;
            }
        }
    }

    private function getCustomerId($id)
    {

        $cacheId = $this->cacheRead('customers', $id);
        if (isset($cacheId)) {
            return $cacheId;
        } else {
            $customersTemp = DB::table('import_entities_customers')->find($id);
            if (is_null($customersTemp))
                return null;

            $row = \Customer::where('email', $customersTemp->email)->where('active', 1)->first();

            if (isset($row)) {
                $this->cacheStore('customers', $id, $row->id);
                return $row->id;
            } else {
                return null;
            }
        }
    }

    private function getAddressId($id)
    {

        $cacheId = $this->cacheRead('address', $id);
        if (isset($cacheId)) {
            return $cacheId;
        } else {
            $addressTemp = DB::table('import_entities_address')->find($id);

            $row = \Address::where(DB::raw('md5(concat(customer_id,billing,address1,address2))'), md5($this->getCustomerId($addressTemp->customer_id) . $addressTemp->billing . $addressTemp->address1 . $addressTemp->address2))->first();
            if (isset($row)) {
                $this->cacheStore('address', $id, $row->id);
                return $row->id;
            } else {
                return null;
            }
        }
    }

    private function getCurrencyId($id)
    {

        $cacheId = $this->cacheRead('currencies', $id);
        if (isset($cacheId)) {
            return $cacheId;
        } else {
            $currenciessTemp = DB::table('import_entities_currencies')->find($id);

            $row = \Currency::where('iso_code', $currenciessTemp->iso_code)->first();

            if (isset($row)) {
                $this->cacheStore('currencies', $id, $row->id);
                return $row->id;
            } else {
                return null;
            }
        }
    }

    private function getCustomerGroupId($id)
    {

        $cacheId = $this->cacheRead('customerGroup', $id);
        if (isset($cacheId)) {
            return $cacheId;
        } else {
            $customerGroupTemp = DB::table('import_entities_customers_groups')->find($id);

            $row = DB::table('customers_groups_lang')->where('name', $customerGroupTemp->name)->first();

            if (isset($row)) {
                $this->cacheStore('customerGroup', $id, $row->customer_group_id);
                return $row->customer_group_id;
            } else {
                return null;
            }
        }
    }

    private function getCarrierId($id)
    {

        $cacheId = $this->cacheRead('carriers', $id);
        if (isset($cacheId)) {
            return $cacheId;
        } else {
            $carriersTemp = DB::table('import_entities_carriers')->find($id);
            $row = \Carrier::rows('it')->where('name', $carriersTemp->name)->first();
            if (isset($row)) {
                $this->cacheStore('carriers', $id, $row->carrier_id);
                return $row->carrier_id;
            } else {
                return null;
            }
        }
    }

    private function getSalesId($id)
    {

        $cacheId = $this->cacheRead('orders', $id);
        if (isset($cacheId)) {
            return $cacheId;
        } else {
            $salesTemp = DB::table('import_entities_sales')->find($id);

            $row = DB::table('orders')->where('reference', $salesTemp->reference)->first();

            if (isset($row)) {
                $this->cacheStore('orders', $id, $row->id);
                return $row->id;
            } else {
                return null;
            }
        }
    }

    private function getPaymentsId($id)
    {

        $cacheId = $this->cacheRead('payments', $id);
        if (isset($cacheId)) {
            return $cacheId;
        } else {
            $paymentsTemp = DB::table('import_entities_payments')->find($id);

            $row = DB::table('payments_relations')->where('module', $paymentsTemp->module)->first();

            if (isset($row)) {
                $this->cacheStore('payments', $id, $row->payment_id);
                return $row->payment_id;
            } else {
                return null;
            }
        }
    }

    private function getPaymentStatus($id)
    {

        $cacheId = $this->cacheRead('payment_status', $id);
        if (isset($cacheId)) {
            return $cacheId;
        } else {
            $paymentStatusTemp = DB::table('import_entities_payment_states')->find($id);

            $row = DB::table('payment_states_lang')->join('payment_states', 'payment_states_lang.payment_state_id', '=', 'payment_states.id')
                ->where('payment_states.deleted', 0)
                ->where('payment_states_lang.name', $paymentStatusTemp->name)
                ->select('payment_states.id')->first();

            if (isset($row)) {
                $this->cacheStore('payments', $id, $row->id);
                return $row->id;
            } else {
                return null;
            }
        }
    }

    private function getProduct($sku, $sapsku, $ean = null)
    {

        $product = \Product::where('sku', $sku)
            ->orWhere('sap_sku', $sapsku)
            ->orWhere('ean13', $ean)
            ->first();

        if (isset($product)) {
            return $product;
        } else {
            return null;
        }

    }

    private function getProductCombination($sku, $sapsku, $ean = null)
    {

        $product = \ProductCombination::where('sku', $sku)
            ->orWhere('sap_sku', $sapsku)
            ->orWhere('ean13', $ean)
            ->first();

        if (isset($product)) {
            return $product;
        } else {
            return null;
        }

    }

    /**
     * End of relations id retreival
     **/

    /**
     * Upsert different data
     **/
    private function checkRelations($relations, $data, $record_id, $tableName)
    {

        foreach ($relations as $relation) {
            if (!isset($data[$relation])) {
                $message = 'Unable to retrieve relation : ' . $relation . ' for record: ' . $record_id . ' for table ' . $tableName;
                $this->logError($message);
                return false;
            }
        }

        return true;
    }

    private function upsert($modelName, $data, $condition)
    {
        $model = app($modelName);
        $inserted = null;
        try {
            $inserted = $model->updateOrCreate($condition, $data);
        } catch (\Exception $e) {
            $this->console->error($e->getMessage());
            $this->logError($e->getMessage());
            \Utils::log($e->getTraceAsString());
        }
        return $inserted;
    }

    private function upsertLang($modelName, $data, $condition)
    {

        $this->console->comment('Upserting into ' . $modelName);

        $model = app($modelName);
        $modelLang = \App::Make('\\' . $modelName . '_Lang');

        $action = 'create';

        if ($this->mode == 'basic') {
            $inserted = $model::find($condition['id']);
            if (isset($inserted)) {
                $action = 'update';
            } else {
                $action = 'create';
            }
        }

        try {
            $relation = snake_case($modelName);
            if ($action = 'create') {
                $inserted = $model::create([]);
                $data[$relation . '_id'] = $inserted->id;
                $modelLang::create($data);
            } else {
                $modelLang::where($relation, $inserted->id)->first()->update($data);
            }

        } catch (Exception $e) {
            $this->errors[] = $e->getMessage();
        }

    }

    private function upsertAddressTable($row)
    {
        $data = [
            'country_id' => $this->getCountryId($row->country_id),
            'state_id' => null,
            'customer_id' => $row->customer_id,
            'people_id' => $row->people_id > 0 ? $row->people_id : 1,
            'billing' => $row->billing,
            'alias' => $row->alias,
            'company' => $this->sanitize($row->company),
            'lastname' => $this->sanitize($row->lastname),
            'firstname' => $this->sanitize($row->firstname),
            'address1' => trim($row->address1),
            'address2' => trim($row->address2),
            'postcode' => $this->sanitize($row->postcode),
            'city' => $this->sanitize($row->city),
            'other' => $row->other,
            'phone' => $row->phone,
            'phone_mobile' => $row->phone_mobile,
            'fax' => $row->fax,
            'vat_number' => $row->vat_number,
            'cf' => $row->cf,
            'extrainfo' => $row->extrainfo,
            'created_at' => $row->created_at,
            'updated_at' => $row->updated_at,
        ];

        $condition = ['customer_id' => $row->customer_id, 'address1' => $data['address1']];

        $this->upsert('Address', $data, $condition);
    }

    private function upsertCustomersTable($row)
    {
        $this->console->line("Importing " . $row->id);

        $lang_id = $this->parseLangByWebsite($row->website);
        $secure_key = md5($row->id);

        $data = [
            'import_id' => $row->id,
            'shop_id' => 1,
            'gender_id' => $row->gender_id,
            'default_group_id' => 3,
            'people_id' => $row->people_id,
            'company' => $row->company,
            'lastname' => $this->sanitize($row->lastname),
            'firstname' => $this->sanitize($row->firstname),
            'email' => $row->email,
            'passwd' => (trim($row->passwd)),
            'passwd_md5' => (trim($row->passwd)),
            'birthday' => $row->birthday == '0000-00-00' ? null : $row->birthday,
            'newsletter' => $row->newsletter,
            'ip_registration_newsletter' => $row->ip,
            'newsletter_date_add' => $row->newsletter_date_add,
            'active' => $row->active,
            'guest' => $row->guest,
            'created_at' => $row->created_at,
            'updated_at' => $row->updated_at,
        ];

        $data['name'] = $data['firstname'] . ' ' . $data['lastname'];
        $data['secure_key'] = $secure_key;
        $data['lang_id'] = $lang_id;

        $condition = ['id' => $row->id];

        $this->upsert('Customer', $data, $condition);

    }

    private function upsertCustomerGroupsTable($row)
    {

        $data = [
            'name' => $this->sanitize($row->name)
        ];
        if ($this->mode == 'basic') {
            $condition = ['id' => $row->id];
        } else {
            $condition = null;
        }

        $this->upsertLang('CustomerGroup', $data, $condition);

    }

    private function upsertNewslettersTable($row)
    {
        $this->console->line("Importing " . $row->email);

        $email = Str::lower(trim($row->email));
        $lang_id = $this->parseLang($row->lang_id);

        $data = [
            'customer_id' => $row->customer_id,
            'email' => $email,
            'lang_id' => $lang_id,
            'user_ip' => $row->user_ip,
            'created_at' => $row->created_at,
            'updated_at' => $row->updated_at,
        ];

        $condition = ['email' => $email];

        $this->upsert('Newsletter', $data, $condition);
    }


    private function insertLog($reason, $scope, $record_id, $obj)
    {
        DB::table('import_entities_errors')->insert([
            'reason' => $reason,
            'scope' => $scope,
            'record_id' => $record_id,
            'data' => serialize($obj),
            'event_at' => date('Y-m-d H:i:s'),
        ]);
    }


    private function upsertSalesProductsTable($row)
    {

        if (Str::contains($row->product_name, ['regalo', 'REGALO', 'Regalo'])) {
            $this->console->comment("Product REGALO skipped");
            return;
        }

        $product = $this->getProduct($row->sku, $row->sapsku, $row->product_ean13);
        $product_id = null;
        $combination_id = null;
        $product_buy_price = 0;
        $product_sell_price = 0;
        $product_sell_price_wt = 0;
        $product_item_price = 0;
        $product_item_price_tax_excl = 0;
        $reduction_percent = 0;
        $reduction_amount = 0;
        $reduction_amount_tax_incl = 0;
        $reduction_amount_tax_excl = 0;
        $product_price = 0;

        if (is_null($product_id)) {
            $combination = $this->getProductCombination($row->sku, $row->sapsku, $row->product_ean13);
            if ($combination) {
                $product = \Product::getObj($combination->product_id);
                $combination_id = $combination->id;
            }
        }

        if ($product) {
            $product_id = $product->id;
            $product_buy_price = $product->buy_price;
            $product_sell_price = $product->sell_price;
            $product_sell_price_wt = $product->sell_price_wt;
            $product_item_price = $product->sell_price_wt;
            $product_price = $product->sell_price_wt;
            $product_item_price_tax_excl = $product->sell_price;
        }

        $tax_rate = $row->tax_rate > 0 ? 1 + $row->tax_rate / 100 : 1;

        if (is_null($product)) {
            $reason = 'not_founded';
            $scope = 'products_sales';
            $this->insertLog($reason, $scope, $row->id, $row);
            $product_id = 0;
            $product_buy_price = $row->price_tax_incl;
            $product_sell_price = $row->price_tax_incl;
            $product_sell_price_wt = $row->price_tax_incl;
            $product_item_price = $row->price_tax_incl;
            $product_price = $row->price_tax_incl;
            $product_item_price_tax_excl = (float)\Core::untax($row->price_tax_incl, $tax_rate);
            //return;
        }

        $item_price = $row->total_price_tax_incl;
        $total_price = $row->price_tax_incl;

        $tax_name = "Imposta IVA standard ($row->tax_rate%)";

        $total_discounted_item_price = $item_price;
        $discounted_item_price = $item_price;

        if ($row->reduction_percent > 0) {
            $reduction_percent = $row->reduction_percent;
            $discounted_item_price = $item_price - ($item_price / 100 * $row->reduction_percent);
            $total_discounted_item_price = $discounted_item_price * $row->product_quantity;
        } elseif ($row->reduction_amount > 0) {
            $reduction_amount = $row->reduction_amount;
            $reduction_amount_tax_incl = $row->reduction_amount;
            $reduction_amount_tax_excl = (float)\Core::untax($reduction_amount_tax_incl, $tax_rate);
            $discounted_item_price = $item_price - $row->reduction_amount;
            $total_discounted_item_price = $discounted_item_price * $row->product_quantity;
        }
        //(float)\Core::untax($p->cart_item_price,$tax_rate)
        $total_price_tax_incl = $total_discounted_item_price;//discounted price * quantity
        $total_price_tax_excl = (float)\Core::untax($total_discounted_item_price, $tax_rate);// $total_price_tax_incl no taxes
        $cart_price_tax_incl = $discounted_item_price;//discounted price
        $cart_price_tax_excl = (float)\Core::untax($discounted_item_price, $tax_rate);//discounted price no taxes
        $price_tax_incl = $cart_price_tax_incl;
        $price_tax_excl = $cart_price_tax_excl;


        $data = [
            'id' => $row->id,
            'order_id' => $row->order_id,
            'product_id' => $product_id,
            'product_reference' => $row->sku,
            'product_sap_reference' => $row->sku,
            'product_combination_id' => $combination_id,
            'product_name' => $this->sanitize($row->product_name),
            'product_quantity' => $row->product_quantity,
            'product_quantity_in_stock' => $row->product_quantity,
            'product_price' => $product_price,
            'product_buy_price' => $product_buy_price,
            'product_sell_price' => $product_sell_price,
            'product_sell_price_wt' => $product_sell_price_wt,
            'product_item_price' => $product_item_price,
            'product_item_price_tax_excl' => $product_item_price_tax_excl,
            'total_price_tax_incl' => $total_price_tax_incl,
            'total_price_tax_excl' => $total_price_tax_excl,
            'cart_price_tax_incl' => $cart_price_tax_incl,
            'cart_price_tax_excl' => $cart_price_tax_excl,
            'price_tax_incl' => $price_tax_incl,
            'price_tax_excl' => $price_tax_excl,
            'reduction_percent' => $reduction_percent,
            'reduction_amount' => $reduction_amount,
            'reduction_amount_tax_incl' => $reduction_amount_tax_incl,
            'reduction_amount_tax_excl' => $reduction_amount_tax_excl,
            'product_ean13' => $row->product_ean13,
            'product_weight' => $row->product_weight,
            'tax_rate' => $tax_rate,
            'tax_name' => $tax_name,

            'availability_mode' => 'online',
            'availability_shop_id' => null,
            'warehouse' => '01',
            'created_at' => $row->created_at,
            'updated_at' => $row->updated_at,
        ];

        //audit($data, __METHOD__);
        //return;

        $condition = ['id' => $row->id];


        $this->upsert('OrderDetail', $data, $condition);

    }

    private function upsertShopsTable($row)
    {

        $data = [
            'name' => $row->name,
            'cd_neg' => $row->cd_neg,
            'cd_internal' => $row->cd_internal,
        ];

        if ($this->mode == 'basic') {
            $condition = ['id' => $row->id];
        } else {
            $condition = ['cd_neg' => $row->cd_neg];
        }

        $this->upsert('Shop', $data, $condition);

    }

    private function upsertSalesTable($row)
    {

        $customer_data = $this->upsertCustomerFromSales($row);
        $customer_id = $customer_data['customer_id'];
        $shipping_address_id = $customer_data['shipping_address_id'];
        $billing_address_id = $customer_data['billing_address_id'];
        $payment_id = $this->getPaymentIdByMethodCode($row->method_code);
        $paymentObj = \Payment::getObj($payment_id);

        $total_products = 0;
        $total_products_tax_incl = 0;
        $total_products_tax_excl = 0;

        $total_order = 0;
        $total_order_tax_incl = 0;
        $total_order_tax_excl = 0;
        $total_quantity = 0;

        $details = \OrderDetail::where('order_id', $row->id)->get();
        if (!empty($details)) {
            $total_shipping = 0;
            foreach ($details as $detail) {
                $total_quantity += $detail->product_quantity;
                $total_products += $detail->total_price_tax_incl;
                $total_products_tax_incl += $detail->total_price_tax_incl;
                $total_products_tax_excl += $detail->total_price_tax_excl;
                $total_order += $detail->total_price_tax_incl;
                $total_order_tax_incl += $detail->total_price_tax_incl;
                $total_order_tax_excl += $detail->total_price_tax_excl;
            }
            $total_taxes = $total_order_tax_incl - $total_order_tax_excl;
        } else {
            $total_quantity = $row->total_quantity;
            $total_taxes = $row->total_taxes;
            $total_order = $row->total_order;
            $total_order_tax_incl = $row->total_order;
            $total_order_tax_excl = $row->total_order;
            $total_products = $row->total_products;
            $total_products_tax_incl = $row->total_products;
            $total_products_tax_excl = $row->total_products;
            $total_shipping = $row->total_shipping;
        }

        $statusMapping = $this->getOrderStatuses($row, $payment_id);
        $status = $statusMapping['status'];
        $payment_status = $statusMapping['payment_status'];

        $data = [
            'id' => $row->id,
            'reference' => $row->reference,
            'carrier_id' => $this->getCarrierIdForTriboo($row->carrier_id, $row->lang_id),
            'payment_id' => $payment_id,
            'lang_id' => $this->parseLang($row->lang_id),
            'customer_id' => $customer_id,
            'currency_id' => 1,
            'status' => $status,
            'payment_status' => $payment_status,
            'secure_key' => md5($row->id),
            'shipping_address_id' => $shipping_address_id,
            'billing_address_id' => $billing_address_id,
            'send_confirm' => 1,
            'verified_address' => 1,
            'cart_rule_id' => 0,
            'delivery_store_id' => 0,
            'payment' => $paymentObj->name,
            'module' => $paymentObj->module,
            'gift' => $row->gift,
            'gift_message' => trim($row->gift_message),
            'notes' => $row->notes,
            'notes_internal' => $row->notes_internal,
            'total_products' => $total_products,
            'total_products_tax_incl' => $total_products_tax_incl,
            'total_products_tax_excl' => $total_products_tax_excl,
            'total_shipping' => $total_shipping,
            'total_payment' => $row->total_payment,
            'total_wrapping' => $row->total_wrapping,
            'total_order' => $total_order,
            'total_order_tax_incl' => $total_order_tax_incl,
            'total_order_tax_excl' => $total_order_tax_excl,
            'total_taxes' => $total_taxes,
            'total_weight' => $row->total_weight,
            'total_quantity' => $total_quantity,
            'delivery_date' => Carbon::parse($row->created_at)->addDays(3)->format('Y-m-d'),
            'coupon_code' => $row->coupon_code,
            'availability_mode' => 'online',
            'availability_shop_id' => null,
            'warehouse' => null,
            'shipping_number' => $row->sap_delivery_id,
            'created_at' => $row->created_at,
            'updated_at' => $row->updated_at,
        ];

        audit($data, __METHOD__);

        $condition = ['reference' => $row->reference];

        $order = $this->upsert('Order', $data, $condition);

        if ($order) {
            $order->setStatus($status, false);
            $order->setPaymentStatus($payment_status);
            $order->setOrderSlip($row->sap_delivery_id, $row->sap_sales_id);
        }

    }


    private function upsertStatesTable($row)
    {
        $this->console->line("Upserting $row->name");
        $country_id = $this->getCountryId($row->country_id);

        $name = trim($row->name);

        if ($country_id) {
            $this->console->info("Country ID: $country_id");
        } else {
            $this->console->error("Non found country: " . $row->country_id);
            return;
        }

        //italy
        if ($country_id == 10) {
            $state = \State::where('iso_code', $row->iso_code)->where('country_id', $country_id)->first();
            if ($state) {
                $state->alias = $row->name;
                $state->save();
            }
        } else {
            $state = \State::where(compact('name', 'country_id'))->first();
            if (!$state) {
                $data = [
                    'country_id' => $country_id,
                    'iso_code' => $row->iso_code,
                    'name' => $name,
                    'alias' => $name,
                    'zone_id' => 2,
                ];
                \State::create($data);
            }
        }
    }


    private function getPaymentIdByMethodCode($method_code)
    {
        $default = 10;
        $matrix = [
            'amazon' => 7, //import
            'banktransfer' => 3, //bonifico
            'free' => 8, //import
            'gestpay' => 2, //credit card
            'neteven' => 9, //import
            'paypal_express' => 1, //Paypal
            'purchaseorder' => 10, //import
        ];

        return isset($matrix[$method_code]) ? $matrix[$method_code] : $default;
    }

    private function getCarrierIdForTriboo($carrier_id, $lang_id)
    {
        $default = 8;
        $matrix = [
            11 => 4, //ritiro a negozio
            13 => ($lang_id == 'it_IT') ? 5 : 2, //spedizione standard se italia, spedizione estera altrimenti,
        ];

        return isset($matrix[$carrier_id]) ? $matrix[$carrier_id] : $default;
    }


    private function upsertCustomerFromSales($row)
    {
        audit(__METHOD__);
        $customer_id = $row->customer_id;
        $shipping_address_id = $row->shipping_address_id;
        $billing_address_id = $row->billing_address_id;

        $shippingAddress = $this->getSalesAddress($shipping_address_id);
        $billingAddress = $this->getSalesAddress($billing_address_id);

        if ($customer_id == 0) {
            //guest customer
            $guest = $this->upsertGuestCustomerFromBillingAddress($billingAddress ? $billingAddress : $shippingAddress, $row);
            if ($guest) {
                $customer_id = $guest->id;
            }
        }

        $shipping_address_id = $this->upsertAddressesForCustomer($customer_id, $shippingAddress, $row, true);
        $billing_address_id = $this->upsertAddressesForCustomer($customer_id, $billingAddress, $row, false);
        if (is_null($billing_address_id) and !is_null($shipping_address_id)) {
            $billing_address_id = $shipping_address_id;
        } elseif (!is_null($billing_address_id) and is_null($shipping_address_id)) {
            $shipping_address_id = $billing_address_id;
        }


        return compact('customer_id', 'shipping_address_id', 'billing_address_id');
    }

    private function getSalesAddress($id)
    {
        $row = DB::table('import_entities_sales_address')->find($id);
        if (is_null($row)) {
            return null;
        }
        return $row;
    }

    private function upsertGuestCustomerFromBillingAddress($row, $sale)
    {
        audit(__METHOD__);
        if (is_null($row)) {
            audit("ERROR - cannot find a valid sales_address");
            return null;
        }
        $email = Str::lower(trim($row->email));
        $existingCustomer = \Customer::where('email', $email)->first();
        if ($existingCustomer)
            return $existingCustomer;

        //create a new guest user
        $lang_id = $this->parseLang($sale->lang_id);
        $secure_key = md5($email);

        $newsletter = DB::table('import_entities_newsletter')->where('email', $email)->first() ? 1 : 0;

        $data = [
            'guest' => 1,
            'shop_id' => 1,
            'gender_id' => 1,
            'default_group_id' => 3,
            'people_id' => 1,
            'company' => null,
            'lastname' => $this->sanitize($row->lastname),
            'firstname' => $this->sanitize($row->firstname),
            'email' => $email,
            'passwd' => md5(str_random()),
            'passwd_md5' => md5(str_random()),
            'birthday' => null,
            'newsletter' => $newsletter,
            'ip_registration_newsletter' => null,
            'newsletter_date_add' => null,
            'active' => 1,
            'created_at' => $sale->created_at,
            'updated_at' => $sale->updated_at,
        ];

        $data['name'] = $data['firstname'] . ' ' . $data['lastname'];
        $data['secure_key'] = $secure_key;
        $data['lang_id'] = $lang_id;

        audit($data, __METHOD__);

        $customer = new \Customer($data);
        $customer->save(['timestamps' => false]);
        return $customer;
    }


    private function upsertAddressesForCustomer($customer_id, $row, $sale, $is_billing)
    {
        if (is_null($row))
            return null;

        $state_id = $this->getStatesIdByRegion($row->region, $row->country_id);

        $data = [
            'warehouse_id' => $row->id,
            'country_id' => $this->getCountryId($row->country_id),
            'state_id' => $state_id,
            'customer_id' => $customer_id,
            'people_id' => 1,
            'billing' => (int)$is_billing,
            'alias' => null,
            'company' => $this->sanitize($row->lastname),
            'lastname' => $this->sanitize($row->lastname),
            'firstname' => $this->sanitize($row->firstname),
            'address1' => trim($row->street),
            'address2' => trim($row->address2),
            'postcode' => $this->sanitize($row->postcode),
            'city' => $this->sanitize($row->city),
            'other' => $row->other,
            'phone' => $row->telephone,
            'phone_mobile' => $row->phone_mobile,
            'fax' => $row->fax,
            'vat_number' => $row->vat_number,
            'cf' => $row->cf,
            'extrainfo' => $row->extrainfo,
            'created_at' => $sale->created_at,
            'updated_at' => $sale->updated_at,
        ];

        audit($data, __METHOD__);

        $condition = ['warehouse_id' => $row->id];

        $address = $this->upsert('Address', $data, $condition);

        return $address->id;
    }


    private function getStatesIdByRegion($region, $outer_country_id)
    {
        $state = \State::where('name', $region)->first();
        return ($state) ? $state->id : null;
    }


    private function getOrderStatuses($row, $payment_id)
    {
        $status = 1;
        $payment_status = null;

        switch ($row->status) {

            case "ax_canceled":
                $status = \OrderState::STATUS_CANCELED;
                break;
            case "ax_closed":
                $status = \OrderState::STATUS_CLOSED;
                break;
            case "ax_processing":
                $status = \OrderState::STATUS_PENDING;
                break;
            case "ax_returned":
                $status = \OrderState::STATUS_REFUNDED;
                $payment_status = \PaymentState::STATUS_REFUNDED;
                break;
            case "ax_shipped":
                $status = \OrderState::STATUS_SHIPPED;
                break;
            case "axed":
                $status = \OrderState::STATUS_PENDING;
                break;
            case "axing":
                $status = \OrderState::STATUS_PENDING;
                break;
            case "canceled":
                $status = \OrderState::STATUS_CANCELED;
                break;
            case "gestpay_3d_verifying":
                $status = \OrderState::STATUS_PENDING;
                $payment_status = \PaymentState::STATUS_PENDING_CREDITCARD;
                break;
            case "gestpay_error":
                $status = \OrderState::STATUS_CANCELED;
                $payment_status = \PaymentState::STATUS_REFUSED;
                break;
            case "gestpay_rejected":
                $status = \OrderState::STATUS_CANCELED;
                $payment_status = \PaymentState::STATUS_REFUSED;
                break;
            case "payment_check_required":
                $status = \OrderState::STATUS_PENDING;
                $payment_status = \PaymentState::STATUS_REFUSED;
                break;
            case "pending":
                $status = \OrderState::STATUS_PENDING;
                break;
            case "sent":
                $status = \OrderState::STATUS_PENDING;
                break;

        }

        if ($payment_status == null and $status == \OrderState::STATUS_PENDING) {
            switch ($payment_id) {
                case \Payment::TYPE_BANKTRANSFER:
                    $payment_status = \PaymentState::STATUS_PENDING_BANKTRANSFER;
                    break;
                case \Payment::TYPE_CREDITCARD:
                    $payment_status = \PaymentState::STATUS_PENDING_CREDITCARD;
                    break;
                case \Payment::TYPE_PAYPAL:
                    $payment_status = \PaymentState::STATUS_PENDING_PAYPAL;
                    break;
                default:
                    $payment_status = \PaymentState::STATUS_EXPIRED;
                    break;
            }
        }
        if ($payment_status == null and $status == \OrderState::STATUS_CANCELED) {
            switch ($payment_id) {
                case \Payment::TYPE_BANKTRANSFER:
                    $payment_status = \PaymentState::STATUS_USER_CANCELED;
                    break;
                case \Payment::TYPE_CREDITCARD:
                    $payment_status = \PaymentState::STATUS_USER_CANCELED;
                    break;
                case \Payment::TYPE_PAYPAL:
                    $payment_status = \PaymentState::STATUS_REFUSED;
                    break;
                default:
                    $payment_status = \PaymentState::STATUS_USER_CANCELED;
                    break;
            }
        }
        if ($payment_status == null and ($status == \OrderState::STATUS_CLOSED or $status == \OrderState::STATUS_SHIPPED)) {
            switch ($payment_id) {
                case \Payment::TYPE_BANKTRANSFER:
                    $payment_status = \PaymentState::STATUS_COMPLETED;
                    break;
                case \Payment::TYPE_CREDITCARD:
                    $payment_status = \PaymentState::STATUS_COMPLETED;
                    break;
                case \Payment::TYPE_PAYPAL:
                    $payment_status = \PaymentState::STATUS_COMPLETED;
                    break;
                default:
                    $payment_status = \PaymentState::STATUS_COMPLETED;
                    break;
            }
        }
        if (is_null($payment_status)) {
            $payment_status = \PaymentState::STATUS_USER_CANCELED;
        }

        return compact('status', 'payment_status');
    }
}