<?php
namespace services\Bluespirit\Importers\Entities;

use DB;
class StatesEntity extends ExcelEntityImporter
{
    protected $table = 'import_entities_states';
    protected $file_pattern = 'Blade_Import_States';
    protected $skip_first_row = false;
    
    protected $headings = [
        'id',
        'name',
        'country_id',
        'iso_code',
    ];
    protected $rules = [
        'id' => 'required',
        'name' => 'required',
        'country_id' => 'required',
        'iso_code' => 'required'
    ];
            
    protected function avoidDuplicate($obj){
        DB::table($this->table)->where('id',$obj->id)->where('iso_code',$obj->iso_code)->where('country_id',$obj->country_id)->delete();
    }
    
    
    function checkEntity(){
        $this->console->comment("Checking Importer  " . static::class ." ...");
        $records =  DB::table($this->table)->get();
        $this->rule['country_id'] = 'required|exists:import_entities_countries,id';
        
        foreach($records as $record){
            $this->validate($record);
            continue;
        }
    }
    
            
}
