<?php
namespace services\Bluespirit\Importers\Entities;

use DB;
class PromotionsEntity extends ExcelEntityImporter
{
    protected $table = 'import_entities_promotions';
    protected $file_pattern = 'Blade_Import_Promotions';
    protected $skip_first_row = false;

    protected $headings = [
                'id',
                'date_from',
                'date_to',
                'code',
                'country_restriction',
                'carrier_restriction',
                'group_restriction',
                'reduction_type',
                'reduction_amount',
                'currency_id'
    ];
    
    protected $rules = [
                'id' => 'required',
                'date_from' => 'required',
                'date_to' => 'required',
                'code' => 'required',
                'country_restriction' => 'required',
                'reduction_type' => 'required',
                'reduction_amount' => 'required',
                'currency_id' => 'required'
    ];
    
    protected function avoidDuplicate($obj){
        DB::table($this->table)->where('id',$obj->id)->delete();
    }
        
    function checkEntity(){
        $this->console->comment("Checking Importer  " . static::class ." ...");
        
        $records =  DB::table($this->table)->get();
        
        $this->rule['currency_id'] = 'required|exists:import_entities_currencies,id';
        
        foreach($records as $record){
            $this->validate($record);
            continue;
        }
    }
    
            
}
