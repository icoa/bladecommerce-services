<?php
namespace services\Bluespirit\Importers\Entities;

use DB;

class CurrenciesEntity extends ExcelEntityImporter
{
    protected $table = 'import_entities_currencies';
    protected $file_pattern = 'Blade_Import_Currencies';
    protected $skip_first_row = false;

    protected $headings = [
            'id',
            'name',
            'iso_code',
    ];
    
    protected $rules = [
        'id' => 'required',
        'name' => 'required',
        'iso_code' => 'required|max:3'
    ];       
    
    protected function avoidDuplicate($obj){
        DB::table($this->table)->where('id',$obj->id)->where('name',$obj->name)->delete();
    }
            
}
