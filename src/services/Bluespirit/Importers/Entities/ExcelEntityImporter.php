<?php

namespace services\Bluespirit\Importers\Entities;

use DB;
use Utils;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Excel;
use Validator;
use File;

class ExcelEntityImporter
{
    protected $debug = false;
    protected $truncate = true;
    protected $update = false;
    //it indicates if the source file name must be inserted in every row as a separate field value
    protected $import_filename = false;
    
    protected $console;
    protected $files;
    protected $logFile;
    
    protected $file;
    protected $filename;
    protected $table = 'mi_missing_table';
    protected $counter = 0;
    protected $chunks = 250;
    protected $errors = [];
    protected $excelFiles = [];
    protected $file_pattern = '*';
    protected $utf_encode = false;
    protected $skip_first_row = true;
    protected $source = null;
    
    protected $path = '/';
    protected $timestamp = false;

    protected $headings = [

    ];
    
    protected $rules = [];
    
    function __construct(Command $console ,Filesystem $files )
    {
        $this->console = $console;
        $this->files = $files;
        $this->createLogFile();
    }

    function make()
    {
        $this->excelFiles = $this->files->glob($this->path. '/' .$this->file_pattern . '.*');
        
        $count = count($this->excelFiles);
        
        $this->createLogFile();
        
        $this->console->comment("Found {$count} Excel files to import...");
    
        //truncating the table
        if ($count > 0) {
          if ($this->truncate)
              DB::table($this->table)->truncate();
        
          foreach ($this->excelFiles as $excelFiles) {
              $this->setFile($excelFiles)->import();
          }
        
        }

        return $this;
    }
    
    function createLogFile(){
        $path = '/logs/entities/';
        $logPath = storage_path('logs/entities/');
        
        if(!File::isDirectory($logPath)){
            File::makeDirectory($logPath,777);
        }
        
        $this->logFile = $logPath . date('Ymd-His.'). $this->file_pattern. '.log';
        
    }
    
    function setFile($file)
    {
        $this->file = $file;

        return $this;
    }
    
    
    function import()
    {
        $this->console->comment("Importing file [$this->file] into table [$this->table]");

        $data = Excel::selectSheetsByIndex(0)->load($this->file, function($reader) {
            $reader->setDateFormat('Y-m-d H:i:s');
        })->get();
        
        
        // Excel::selectSheetsByIndex(0)->filter('chunk')->load($this->file)->chunk(250, function($results){
        //         $this->processRows($results);
        // },true);
        
        
        if(!empty($data) && $data->count() > 0){
            $this->processRows($data);
        }

        $this->finish();
        return $this;
    }

    function finish()
    {
        $doneFolder = $this->path.'/done';
        
        if (!File::isDirectory($doneFolder)) {
            File::makeDirectory($doneFolder);
        }
        
        $info = pathinfo($this->file);
        
        $dirname = $info['dirname'];
        $basename = $info['basename'];
        $newBasename = 'done/' . date('Ymd-His.').$basename;
        $oldFile = $dirname . '/' . $basename;
        $newFile = $dirname . '/' . $newBasename;
        $this->files->move($oldFile, $newFile);
        $this->console->info("Excel file [$basename] moved to [$newBasename]");

        if(method_exists($this,'onFinish')){
            $this->onFinish();
        }
    }


    protected function avoidDuplicate($obj){

    }

    function checkEntity(){
        $this->console->comment("Checking Importer  " . static::class ." ...");

        
        $records =  DB::table($this->table)->get();
        
        foreach($records as $record){
            $this->validate($record);
            continue;
        }
    }
    
    protected function canInsert($obj){

            return $this->validate($obj);
           
    }
    
    protected function processRows($rows)
    {
        $count = count($rows);
        $this->console->comment("Processing [$count] rows...");

        DB::beginTransaction();
        $counter = 0;

        foreach ($rows->toArray() as $row) {
            
            if($counter == 0 and $this->skip_first_row){
                $this->console->line('Skipping first row');
            }else{
                try {
                    $obj = new \stdClass();
                    foreach ($this->headings as $index => $heading) {

                        if (isset($row[$heading]))
                            $obj->$heading = $this->sanitize($row[$heading]);
                    }
                    if($this->import_filename)
                        $obj->imported_file = $this->filename;

                    // $obj->source = $this->source;



                    if($this->canInsert($obj)){
                        $this->avoidDuplicate($obj);
                        $this->insert($obj);
                        $this->counter++;
                    }else{
                        if($this->update){
                            $id = $this->getRecordId($obj);
                            if($id > 0){
                                $this->update($obj, $id);
                            }
                        }
                    }

                    unset($row);
                    unset($obj);
                    unset($id);

                } catch (\Exception $e) {
                    
                    $this->logError($e->getMessage());

                }
            }

            $counter++;

            $this->console->line("Record imported ($counter/$count)");
        }

        DB::commit();

        unset($rows);

        $this->console->comment("Transaction has been committed");
    }

    protected function insert($obj)
    {
        $values = (array)$obj;
        if($this->timestamp){
            $values['created_at'] = date('Y-m-d H:i:s',$obj->created_at);
            $values['updated_at'] = date('Y-m-d H:i:s',$obj->updated_at);
        }
        // $values['created_at'] = date('Y-m-d H:i:s');
        if ($this->debug) {
            Utils::log($values, __METHOD__);
        } else {
            DB::table($this->table)->insert($values);
        }
    }

    protected function update($obj,$id)
    {
        $values = (array)$obj;
        // $values['created_at'] = date('Y-m-d H:i:s');
        if ($this->debug) {
            Utils::log($values, __METHOD__);
        } else {
            DB::table($this->table)->where('id',$id)->update($values);
        }
    }


    protected function logError($msg)
    {
       File::append($this->logFile, $msg. PHP_EOL); 
    }

    protected function sanitize($str)
    {
        $str = str_replace(chr(0xC2) . chr(0xA0), '', $str);
        $str = (trim($str));
        if ($this->utf_encode) {
            $str = utf8_encode($str);
        }
        $str = str_replace(chr(0xC2) . chr(0xA0), '', $str);
        return $str;
    }

    public function setPath($path){
        $this->path = storage_path($path);
    }
    
    protected function validate($obj){
        
        $validator = Validator::make((array)$obj,$this->rules);
        
        if($validator->fails()){

            //audit($validator->errors());
            $errors = [];
            foreach($validator->errors()->toArray() as $field => $errorList){
                $errors[] = implode(' | ', $errorList) . " for value($field): " . $obj->$field;
            }
            
            //$message = stripslashes(html_entity_decode($this->sanitize(strip_tags(json_encode($validator->errors()->all()))))).' for table: '.$this->table.' for record id :'. $obj->id;
            $message = html_entity_decode($this->sanitize(strip_tags(implode(PHP_EOL, $errors))));
            
            $this->logError($message);
            return false;
            
        }
        return true;
    }

}
