<?php
namespace services\Bluespirit\Importers\Entities;

use DB;

class CustomersEntity extends ExcelEntityImporter
{
    protected $table = 'import_entities_customers';
    protected $file_pattern = 'Blade_Import_Customers';
    protected $skip_first_row = false;

    protected $headings = [
                'id',
                'shop_id',
                'gender_id',
                'people_id',
                'default_group_id',
                'company',
                'lastname',
                'firstname',
                'email',
                'passwd',
                'birthday',
                'newsletter',
                'ip',
                'newsletter_date_add',
                'active',
                'guest',
                'created_at',
                'updated_at'
    ];
    
    protected $rules = [
        'id' => 'required',
        'shop_id' => 'required',
        'gender_id' => 'required',
        'people_id' => 'required',
        'default_group_id' => 'required',
        'company' => 'required_if:people_id,2',
        'lastname' => 'required_if:people_id,1',
        'firstname' => 'required_if:people_id,1',
        'email' => 'required|email',
        'passwd' => 'required',
        'birthday' => 'required',
        'newsletter' => 'required|boolean',
        'active' => 'required|boolean',
        'guest' => 'required|boolean',
        'created_at' => 'required',
        'updated_at' => 'required'
        
    ];
              
    protected function avoidDuplicate($obj){
        DB::table($this->table)->where('id',$obj->id)->delete();
    }
    
    public function checkEntity(){
        $this->console->comment("Checking Importer  " . static::class ." ...");
        $records =  DB::table($this->table)->get();
        
        //$this->rules['shop_id'] = 'required|exists:import_entities_shops,id';
        $this->rules['default_group_id'] = 'required|exists:import_entities_customers_groups,id';
        
        foreach($records as $record){
            $this->validate($record);
            continue;
        }
    }
}
