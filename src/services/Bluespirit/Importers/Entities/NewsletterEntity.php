<?php
namespace services\Bluespirit\Importers\Entities;

use DB;
class NewsletterEntity extends ExcelEntityImporter
{
    protected $table = 'import_entities_newsletter';
    protected $file_pattern = 'Blade_Import_Newsletter';
    protected $skip_first_row = false;

    protected $headings = [
                'customer_id',
                'email',
                'lang_id',
                'user_ip',
                'created_at',
                'updated_at',
    ];
     
    protected $rules = [
            'customer_id' => 'required',
            'email' => 'required|email',
            'user_ip' => 'ip',
            'lang_id' => 'required',
            'created_at' => 'required',
            'updated_at' => 'required',
    ];       
            
            
    protected function avoidDuplicate($obj){
        DB::table($this->table)->where('email',strtolower(trim($obj->email)))->delete();
    }        
    
    public function checkEntity(){
        $this->console->comment("Checking Importer  " . static::class ." ...");
        $records =  DB::table($this->table)->get();
        foreach($records as $record){
            if($record->customer_id == 0){
                $this->rule['customer_id'] = 'required|exists:import_entities_customers,id';
            }else{
                $this->rule['customer_id'] = 'required';
            }
            $this->validate($record);
            continue;
        }
    }
    
    protected function insert($obj)
    {
        $values = (array)$obj;
        $values['email'] = strtolower(trim($obj->email));
        
        DB::table($this->table)->insert($values);

    }
            
            
}
