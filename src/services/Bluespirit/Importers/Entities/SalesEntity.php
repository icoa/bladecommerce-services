<?php
namespace services\Bluespirit\Importers\Entities;

use DB;
class SalesEntity extends ExcelEntityImporter
{
    protected $table = 'import_entities_sales';
    protected $file_pattern = 'Blade_Import_Sales';
    protected $skip_first_row = false;

    protected $headings = [
                'id',
                'reference',
                'carrier_id',
                'payment_id',
                'lang_id',
                'customer_id',
                'currency_id',
                'shipping_address_id',
                'billing_address_id',
                'payment_status',
                'gift',
                'gift_message',
                'notes',
                'notes_internal',
                'shipping_number',
                'total_products',
                'total_shipping',
                'total_payment',
                'total_wrapping',
                'total_order',
                'total_taxes',
                'total_weight',
                'total_quantity',
                'delivery_date',
                'coupon_code',
                'availability_mode',
                'availability_shop_id',
                'delivery_store_id',
                'warehouse',
                'created_at',
                'updated_at',
    ];
    
    protected $rules = [
            'id' => 'required',
            'reference' => 'required',
            'carrier_id' => 'required',
            //'payment_id' => 'required',
            'lang_id' => 'required',
            'customer_id' => 'required',
            'currency_id' => 'required',
            'shipping_address_id' => 'required',
            'billing_address_id' => 'required',
            'payment_status' => 'required',
            'gift' => 'required|boolean',
            //'shipping_number' => 'required',
            'total_products' => 'required',
            'total_shipping' => 'required',
            'total_payment' => 'required',
            'total_wrapping' => 'required',
            'total_order' => 'required',
            'total_taxes' => 'required',
            'total_weight' => 'required',
            'total_quantity' => 'required',
            //'delivery_date' => 'required',
            //'availability_mode' => 'required',
            //'availability_shop_id' => 'required',
            //'delivery_store_id' => 'required',
            //'warehouse' => 'required',
            //'created_at' => 'required',
            //'updated_at' => 'required',
    ];
    
    
    protected function avoidDuplicate($obj){
        DB::table($this->table)->where('id',$obj->id)->delete();
    }
    
    function checkEntity(){
        $this->console->comment("Checking Importer  " . static::class ." ...");
        $records =  DB::table($this->table)
            //->take(1000)
            ->get();
        
        $this->rules['carrier_id'] = 'required|exists:import_entities_carriers,id';
        //$this->rules['payment_id'] = 'required|exists:import_entities_payments,id';
        $this->rules['customer_id'] = 'required|exists:import_entities_customers,id';
        //$this->rules['currency_id'] = 'required|exists:import_entities_currencies,id';
        $this->rules['shipping_address_id'] = 'required|exists:import_entities_sales_address,id';
        $this->rules['billing_address_id'] = 'required|exists:import_entities_sales_address,id';
        $this->rules['payment_status'] = 'required|exists:import_entities_order_states,id';
        //$this->rule['availability_shop_id'] = 'required|exists:import_entities_shops,id';
        //$this->rule['delivery_store_id'] = 'required|exists:import_entities_shops,id';
        
        foreach($records as $record){
            $this->rules['customer_id'] = $record->customer_id > 0 ? 'required|exists:import_entities_customers,id' : 'required';
            $this->rules['shipping_address_id'] = ($record->shipping_address_id == 0 and $record->billing_address_id > 0) ? 'required' : 'required|exists:import_entities_sales_address,id';
            $this->validate($record);
            continue;
        }
    }
            
}
