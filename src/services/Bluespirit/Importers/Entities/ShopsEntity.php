<?php
namespace services\Bluespirit\Importers\Entities;

use DB;
class ShopsEntity extends ExcelEntityImporter
{
    protected $table = 'import_entities_shops';
    protected $file_pattern = 'Blade_Import_Shops';
    protected $skip_first_row = false;
    
    protected $headings = [
                'id',
                'name',
                'cd_neg',
                'cd_internal',

    ];
    
    protected $rules = [
            'id' => 'required',
            'name' => 'required',
            'cd_neg' => 'required',
            'cd_internal' => 'required',
    ];
    
    protected function avoidDuplicate($obj){
        DB::table($this->table)->where('cd_neg',$obj->cd_neg)->delete();
    }
    
    
            
}
