<?php


namespace services\Bluespirit\Importers\Currencies;


use services\Traits\TraitConsole;

use Registry;
use Country;
use Currency;
use ProductCombination;
use Product;
use ProductPrice;
use DB;
use Exception;

class CurrencyImporter
{
    use TraitConsole;

    protected $table = 'import_currencies';

    private function getMapping()
    {
        $fields = [
            'ITA_EUR',
            'ESP_EUR',
            'PRT_EUR',
            'DEU_EUR',
            'AUT_EUR',
            'NLD_EUR',
            'DNK_DKK',
            'GBR_GBP',
            'FIN_EUR',
            'SVN_EUR',
            'GRC_EUR',
            'MLT_EUR',
            'HRV_HRK',
            'LVA_EUR',
            'BEL_EUR',
            'FRA_EUR',
            'LUX_EUR',
            'EST_EUR',
            'SWE_SEK',
            'HUN_HUF',
            'POL_PLN',
            'IRL_EUR',
            'CZE_CZK',
            'CYP_EUR',
            'ROM_RON',
            'BGR_BGN',
            'LTU_EUR',
        ];

        $data = [];

        foreach ($fields as $field) {
            list($country_code, $currency_code) = explode('_', $field);
            $data[$field] = (object)['country' => $this->getCountry($country_code), 'currency' => $this->getCurrency($currency_code)];
        }

        return $data;
    }

    /**
     * @param $code
     * @return Country|null
     */
    private function getCountry($code)
    {
        $key = 'country_' . $code;
        if (Registry::has($key))
            return Registry::get($key);

        $country = Country::where('iso_code3', $code)->first();
        if ($country) {
            Registry::set($key, $country);
            return $country;
        }
        $this->getConsole()->error("Warning: cannot find country ($code)");
        return null;
    }

    /**
     * @param $code
     * @return Currency|null
     */
    private function getCurrency($code)
    {
        $key = 'currency_' . $code;
        if (Registry::has($key))
            return Registry::get($key);

        $currency = Currency::where('iso_code', $code)->first();
        if ($currency) {
            Registry::set($key, $currency);
            return $currency;
        }
        $this->getConsole()->error("Warning: cannot find currency ($code)");
        return null;
    }

    /**
     *
     */
    function make()
    {
        $data = $this->getMapping();
        foreach ($data as $field => $params) {

            if (is_null($params->country) or is_null($params->currency)) {
                $this->getConsole()->error("Could not import data, since country or currency are not mapped");
                return;
            }

            //audit($params->country->toArray(), "$field::country");
            //audit($params->currency->toArray(), "$field::currency");

        }

        DB::beginTransaction();
        try {
            $this->importRows();
            DB::commit();
        } catch (Exception $e) {
            audit($e->getTraceAsString());
            $this->getConsole()->error($e->getMessage());
            DB::rollBack();
        }
    }

    /**
     * @throws Exception
     */
    private function importRows()
    {
        $rows = DB::table($this->table)->get();

        $many = count($rows);

        $this->getConsole()->comment("Found $many rows to import...");

        $counter = 0;

        $matrix = $this->getMapping();

        foreach ($rows as $row) {
            $counter++;

            $this->getConsole()->comment("Importing line $counter/$many");

            $sku = $row->sku;

            $combination_id = 0;
            $product_id = 0;
            $country_id = 0;
            $reduction_currency = 0;
            $reduction_value = 0;
            $reduction_type = 'fixed_amount';

            $combination = ProductCombination::where(compact('sku'))->first();
            if ($combination) {
                $combination_id = $combination->id;
                $product_id = $combination->product_id;
            } else {
                $product = Product::where(compact('sku'))->first();
                if ($product)
                    $product_id = $product->id;
            }

            foreach ($matrix as $field => $params) {
                $country_id = $params->country->id;
                $reduction_currency = $params->currency->id;
                $reduction_value = $this->price($row->$field);

                if ($product_id == 0 or $country_id == 0 or $reduction_currency == 0 or $reduction_value == 0) {
                    $this->getConsole()->error("Could not insert price rule when importing SKU: $sku, Field: $field. Product_id: $product_id, Country_id : $country_id, Reduction_currency: $reduction_currency, Reduction_value: $reduction_value");
                    continue;
                }

                $payload = compact('product_id', 'combination_id', 'country_id', 'reduction_currency', 'reduction_value', 'reduction_type');

                $this->upsertProductPrice($payload);
            }
        }
    }

    /**
     * @param array $payload
     */
    private function upsertProductPrice(array $payload)
    {
        $attributes = $payload;
        unset($attributes['reduction_value']);

        $info = "Price: {$payload['reduction_value']} | Currency: {$payload['reduction_currency']} | Country: {$payload['country_id']}";

        $defaults = [
            'price_rule_id' => 0,
            'shop_id' => 1,
            'customer_group_id' => 0,
            'currency_id' => 0,
            'customer_id' => 0,
            'affiliate_id' => null,
            'description' => null,
            'price' => null,
            'from_quantity' => 1,
            'reduction_target' => 1,
            'reduction_tax' => 1,
            'free_shipping' => 0,
            'date_from' => null,
            'date_to' => null,
            'position' => 1,
            'stop_other_rules' => 1,
            'active' => 1,
            'global' => 1,
        ];

        $priceRule = ProductPrice::where($attributes)->first();
        if ($priceRule) {
            $this->getConsole()->comment("Updating price [$priceRule->id] => $info");
            $priceRule->fill($payload);
        } else {
            $this->getConsole()->info("Inserting price => $info");
            $priceRule = new ProductPrice($defaults);
            $priceRule->fill($payload);
        }
        $priceRule->save();
    }

    /**
     * @param $val
     * @return int|string
     */
    private function price($val)
    {
        if ($val <= 0) return 0;
        $val = str_replace(',', '.', $val);
        return number_format((float)$val, 3, '.', '');
    }
}