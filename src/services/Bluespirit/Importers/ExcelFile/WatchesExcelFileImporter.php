<?php
namespace services\Bluespirit\Importers\ExcelFile;
use DB;

class WatchesExcelFileImporter extends ExcelFileImporter
{

    protected $table = 'import_excel_watches';
    protected $file_pattern = 'Watches_Import';

    protected $headings = [
                    'sku',
                    'sapsku',
                    'mastersku',
                    'ean',
                    'brand',
                    'category',
                    'collection',
                    'gender',
                    'case_material',
                    'case_size',
                    'case_thickness',
                    'finishing' ,
                    'case_colour',
                    'glass',
                    'strap_material',
                    'strap_colour',
                    'closure',
                    'movement',
                    'swiss_made' ,
                    'functions',
                    'water_resistant',
                    'price',
                    'box',
                    'box2',
                    'warranty'
                ];
          
      protected $rules = [
        'sku' => 'required',
        'price' => 'required',
      ];
      
    
      public function run(){
        $this->getConsole()->line(__METHOD__);
        $this->make();
      }
      
      protected function avoidDuplicate($obj){
        DB::table($this->table)->where('sku',$obj->sku)->delete();
      }
}
