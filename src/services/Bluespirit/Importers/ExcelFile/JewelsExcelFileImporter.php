<?php
namespace services\Bluespirit\Importers\ExcelFile;
use DB;

class JewelsExcelFileImporter extends ExcelFileImporter
{
    protected $table = 'import_excel_jewels';
    protected $file_pattern = 'Jewels_Import';


    protected $headings = [
                'sku',
                'sapsku',
                'mastersku',
                'ean',
                'brand',
                'category',
                'collection',
                'gender',
                'size1',
                'size2',
                'size3',
                'nav',
                'model',
                'material',
                'material_weight',
                'pearl',
                'pearl_origin',
                'pearl_size',
                'pearl_color',
                'pearl_shape',
                'stone',
                'stone_color',
                'stone_cut',
                'stone_karats',
                'diamond_purity',
                'diamond_range',
                'shape',
                'price',
                'box',
                'box2',
                'warranty'
            ];
            
    protected $rules = [
      'sku' => 'required',
      'price' => 'required',
    ];
    
    public function run(){
      $this->getConsole()->line(__METHOD__);
      $this->make();
    }
    
    protected function avoidDuplicate($obj){
        DB::table($this->table)->where('sku',$obj->sku)->delete();
    }
    
}
