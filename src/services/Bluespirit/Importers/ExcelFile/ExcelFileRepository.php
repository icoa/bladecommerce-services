<?php
namespace services\Bluespirit\Importers\ExcelFile;

use Illuminate\Support\Facades\Config;
use Illuminate\Console\Command;
use DB;

class ExcelFileRepository
{

    protected $console;
    protected $excelImporter;
    protected $watchesExcelImporter;
    protected $jewelsExcelImporter;

    function __construct(Command $console, WatchesExcelFileImporter $watchesExcelImporter, JewelsExcelFileImporter $jewelsExcelImporter)
    {
        $this->console = $console;
        $this->watchesExcelImporter = $watchesExcelImporter;
        $this->jewelsExcelImporter = $jewelsExcelImporter;
    }

    /**
     * @param $source
     * @return $this
     */
    function setSource($source)
    {
        if ($source == 'watches') {
            $this->excelImporter = $this->watchesExcelImporter;
        } else {
            $this->excelImporter = $this->jewelsExcelImporter;
        }
        $this->excelImporter->setConsole($this->console);
        return $this;
    }

    function make()
    {
        $this->excelImporter->run();
    }
}
