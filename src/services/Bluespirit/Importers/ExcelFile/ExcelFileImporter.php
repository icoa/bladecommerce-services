<?php

namespace services\Bluespirit\Importers\ExcelFile;

use DB;
use Utils;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Excel;
use services\Morellato\Loggers\FileLogger as Logger;
use Validator;

class ExcelFileImporter
{
    protected $debug = false;
    protected $truncate = true;
    protected $update = false;
    //it indicates if the source file name must be inserted in every row as a separate field value
    protected $import_filename = false;

    protected $console;
    protected $files;
    // protected $logger;

    protected $file;
    protected $filename;
    protected $table = 'mi_missing_table';
    protected $counter = 0;
    protected $chunks = 250;
    protected $errors = [];
    protected $report = [];
    protected $excelFiles = [];
    protected $file_pattern = '*';
    protected $utf_encode = false;
    protected $skip_first_row = true;
    protected $source = null;
    protected $fromCommand = true;
    
    protected $rules = [
    
    ];

    protected $headings = [

    ];

    function __construct(Filesystem $files , Logger $logger )
    {
        $this->files = $files;
        $this->logger = $logger;
        $this->console = $this->getConsole();
    }

    function make()
    {
      $sourcePath = storage_path('uploads/excel');
      $this->excelFiles = $this->files->glob($sourcePath . '/' . $this->file_pattern . '_*');

      $count = count($this->excelFiles);
      if(isset($this->console)){
        $this->console->comment("Found {$count} Excel files to import...");
          
      }

      //truncating the table
      if ($count > 0) {
          if ($this->truncate)
              DB::table($this->table)->truncate();

          foreach ($this->excelFiles as $excelFiles) {
              $this->setFile($excelFiles)->import();
          }

      }

      return $this;
    }
    
    function setFromCommand($value){
        $this->fromCommand = $value;
    }
    
    function setFile($file)
    {
        
        $this->file = $file;

        return $this;
    }
    
    function import()
    {
        if(isset($this->console)){
            $this->console->comment("Importing file [$this->file] into table [$this->table]");
        }
        
        if(!$this->fromCommand){
            DB::table($this->table)->truncate();
        }

        try {
              $data = Excel::selectSheetsByIndex(0)->load($this->file,function($reader){
                  return $reader->ignoreEmpty()->get();
                  
              })->get();
                
                if(!empty($data) && $data->count() > 0){
                    $this->processRows($data);
                }
                             
                                
                // Excel::selectSheetsByIndex(0)->filter('chunk')->load($this->file)->chunk(250, function($results){
                //         $this->processRows($results);
                // },true);
        } catch (\Exception $e) {
            dd('He stato un errore con excel file');
        }
        
        if($this->fromCommand){
            
            $this->removeFile();
            
            return $this;
        }else{
            
            return $this->report;
        }
        
    }

    function finish()
    {
        if ($this->debug)
            return;

        $doneFolder = storage_path('morellato/csv/done');
        if (!$this->files->isDirectory($doneFolder)) {
            $this->files->makeDirectory($doneFolder);
        }

        $info = pathinfo($this->file);

        $dirname = $info['dirname'];
        $basename = $info['basename'];
        $newBasename = 'done/' . $basename;
        $oldFile = $dirname . '/' . $basename;
        $newFile = $dirname . '/' . $newBasename;
        $this->files->move($oldFile, $newFile);
        $this->console->info("CSV file [$basename] moved to [$newBasename]");

        if(method_exists($this,'onFinish')){
            $this->onFinish();
        }
    }


    protected function avoidDuplicate($obj){

    }

    protected function canInsert($obj){
        return true;
    }

    protected function getRecordId($obj){
        return 0;
    }


    protected function processRows($rows)
    {
        $count = count($rows);
        
        if(isset($this->console)){
            $this->console->comment("Processing [$count] rows...");
        }
        DB::beginTransaction();
        $counter = 0;

        foreach ($rows->toArray() as $row) {
            if(($counter == 0 and $this->skip_first_row) || (empty($row['sku']) && empty($row['price']) && empty($row['brand']) && empty($row['category']))){
                if(isset($this->console)){
                    $this->console->line('Skipping first row');
                }
            }else{
                try {
                    $obj = new \stdClass();
                    foreach ($this->headings as $index => $heading) {

                        if (isset($row[$heading]))
                            $obj->$heading = $this->sanitize($row[$heading]);
                    }
                    if($this->import_filename)
                        $obj->imported_file = $this->filename;

                    if($this->canInsert($obj)){
                        if($this->fromCommand || $this->validate($obj)){
                            $this->avoidDuplicate($obj);
                            $this->insert($obj);
                            $this->counter++;
                        }
                    }else{
                        if($this->update){
                            $id = $this->getRecordId($obj);
                            if($id > 0){
                                $this->update($obj, $id);
                            }
                        }
                    }

                    unset($row);
                    unset($obj);
                    unset($id);

                } catch (\Exception $e) {

                    $this->error($e->getMessage());

                }
            }

            $counter++;
            
            if(isset($this->console)){
                $this->console->line("Record imported ($counter/$count)");
            }
        }

        DB::commit();

        unset($rows);
                
        if(isset($this->console)){
            $this->console->comment("Transaction has been committed");
        }
        
    }

    protected function insert($obj)
    {
        $values = (array)$obj;
        if ($this->debug) {
            Utils::log($values, __METHOD__);
        } else {
            DB::table($this->table)->insert($values);
        }
    }

    protected function update($obj,$id)
    {
        $values = (array)$obj;
        if ($this->debug) {
            Utils::log($values, __METHOD__);
        } else {
            DB::table($this->table)->where('id',$id)->update($values);
        }
    }
    protected function validate($obj){
        $row = (array)$obj;
        $validator = Validator::make($row,$this->rules);
    
        if($validator->fails()){
            $this->report[] = array( 'data' => $row,'status' => 'error', 'errorMessage' => json_encode($validator->errors()->all()) );
            return false;
        }
        
        if(isset($obj->mastersku)){
            $sku = DB::table($this->table)->where('sku',$obj->mastersku)->first();
        
            if(!$sku){
                $this->report[] = array( 'data' => $row,'status' => 'error', 'errorMessage' => json_encode(['Il mastersku non si trova nella tabela']) );
                return false;
            }
        }
        
        $this->report[] = array( 'data' => $row,'status' => 'success');
        return true;
    }


    protected function error($msg)
    {
        $this->errors[] = $msg;
    }
    
    protected function sanitize($str)
    {
        $str = str_replace(chr(0xC2) . chr(0xA0), '', $str);
        $str = (trim($str));
        if ($this->utf_encode) {
            $str = utf8_encode($str);
        }
        $str = str_replace(chr(0xC2) . chr(0xA0), '', $str);
        return $str;
    }

    public function getConsole()
    {
        return $this->console;
    }
    public function setConsole(Command $console)
    {
        $this->console = $console;
    }

    public function removeFile(){
        
        if($this->fromCommand){
            unlink($this->file);
            $this->console->comment("File [$this->file] Removed.");
        }

        return $this;
    }


    public function restoreFiles()
    {
        $folder = storage_path('morellato/csv/done');
        $root = storage_path('morellato/csv/');
        $files = $this->files->files($folder);
        $many = count($files);
        $this->console->line("Found [$many] files to restore");
        foreach ($files as $file) {
            $basename = pathinfo($file)['basename'];
            $newFilename = $root . $basename;
            $this->files->move($file, $newFilename);
            $this->console->info("File [$basename] has been restored");
        }
        return $this;
    }
}
