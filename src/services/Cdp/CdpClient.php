<?php


namespace services\Cdp;

use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessTokenInterface;
use RuntimeException;
use League\OAuth2\Client\Provider\GenericProvider;
use League\OAuth2\Client\Token\AccessToken;
use services\Cdp\Exceptions\InvalidTokenException;

class CdpClient
{

    protected $endpoint = 'https://api-morellato-cdp.jc.neen.it';
    protected $token_endpoint = 'https://api-morellato-cdp.jc.neen.it/oauth/v2/token';

    /**
     * @var Client
     */
    protected $client;

    protected $token_file;

    /**
     * @var GenericProvider
     */
    protected $provider;


    public function __construct()
    {
        $this->token_file = storage_path('sessions/cdp_token.json');

        $this->provider = new GenericProvider([
            'clientId' => config('cdp.credentials.client_id'),    // The client ID assigned to you by the provider
            'clientSecret' => config('cdp.credentials.client_secret'),   // The client password assigned to you by the provider
            'urlAuthorize' => $this->endpoint,
            'urlAccessToken' => $this->token_endpoint,
            'urlResourceOwnerDetails' => $this->endpoint
        ]);

        try {
            $accessToken = $this->getToken();
            if (null === $accessToken) {
                throw new InvalidTokenException();
            }

            print_r($accessToken->jsonSerialize());
            // Using the access token, we may look up details about the
            // resource owner.
            /*$resourceOwner = $this->provider->getResourceOwner($accessToken);

            var_export($resourceOwner->toArray());*/

            // The provider provides a way to get an authenticated API request for
            // the service, using the access token; it returns an object conforming
            // to Psr\Http\Message\RequestInterface.
            $request = $this->provider->getAuthenticatedRequest(
                'GET',
                $this->endpoint,
                $accessToken
            );

            $data = [
                'state' => $this->provider->getState(),
                'getAuthorizationUrl' => $this->provider->getAuthorizationUrl(),
            ];
            print_r($data);
            echo $request->getBody();

        } catch (InvalidTokenException $e) {

            // Failed to get the access token
            exit($e->getMessage());

        }

    }

    /**
     * @return AccessToken|null
     * @throws IdentityProviderException
     */
    protected function getSavedToken()
    {
        if (file_exists($this->token_file)) {
            $json = json_decode(file_get_contents($this->token_file), true);
            $accessToken = new AccessToken($json);
            if ($accessToken->hasExpired()) {
                $newAccessToken = $this->provider->getAccessToken('refresh_token', [
                    'refresh_token' => $accessToken->getRefreshToken()
                ]);
                $this->saveToken($newAccessToken);
                return $newAccessToken;
            }
            return $accessToken;
        }
        return null;
    }

    /**
     * @param AccessTokenInterface $accessToken
     * @return AccessTokenInterface
     */
    protected function saveToken(AccessTokenInterface $accessToken)
    {
        $json = $accessToken->jsonSerialize();
        // save token into a json file
        file_put_contents($this->token_file, json_encode($json));
        return $accessToken;
    }

    /**
     * @return AccessToken|AccessTokenInterface|null
     */
    protected function getToken()
    {
        try {
            // return an existing token
            $existingAccessToken = $this->getSavedToken();
            if ($existingAccessToken) {
                return $existingAccessToken;
            }

            // Try to get an access token using the resource owner password credentials grant.
            $accessToken = $this->provider->getAccessToken('password', [
                'username' => config('cdp.credentials.username'),
                'password' => config('cdp.credentials.password')
            ]);

            // save token into a json file
            $this->saveToken($accessToken);
            return $accessToken;
        } catch (IdentityProviderException $e) {
            audit_exception($e, __METHOD__);
            // Failed to get the access token
            return null;
        }
    }


    /**
     * @param bool $with_cors
     * @return string
     */
    protected function getUri($with_cors = true)
    {
        $short = $this->getShort();
        if ($short === '')
            throw new UrlToShortNotProvided();

        if (false === filter_var($short, FILTER_VALIDATE_URL)) {
            throw new InvalidUrlToShort();
        }

        // check if the url has too many '?'
        if (count(explode('?', $short)) > 2) {
            throw new InvalidUrlToShort();
        }

        if (true === $with_cors) {
            if (false === \Core::isUrlStrictToSameHost($short)) {
                throw new InvalidUrlCorsPolicy();
            }
        } else {
            $host = parse_url($short, PHP_URL_HOST);
            if ($host !== self::CUTTLY_DOMAIN) {
                throw new InvalidUrlCorsPolicy('Given Url to stats does not belong to the configured Cuttly domain: ' . self::CUTTLY_DOMAIN);
            }
        }

        $params = [
            'KEY' => $this->access_key,
            'SHORT' => urlencode($short),
            'ALIAS' => $this->getAlias(),
        ];
        audit($params, __METHOD__, 'params');

        $uri = str_replace(array_map(static function ($key) {
            return "[$key]";
        }, array_keys($params)), array_values($params), $this->endpoint);

        if ($this->getAlias() === '') {
            $uri = str_replace('&name=', '', $uri);
        }

        return $uri;
    }

    /**
     * @param $status
     * @return bool
     */
    protected function isResponseStatusError($status)
    {
        return (int)$status !== 7;
    }

    /**
     * @return CuttlyShortPayload|CuttlyStatsPayload
     * @throws CuttlyApiException
     */
    public function fetchPayload()
    {
        $payload = new CuttlyShortPayload();

        $url = $this->getUri();
        audit($url, __METHOD__, 'URL');

        /** @var Response $response */
        $response = $this->client->get($url);
        $status = $response->getStatusCode();
        //audit($status, __METHOD__, 'STATUS');
        if ($status !== 200) {
            throw new RuntimeException('Cuttly service has not responded with a valid 200 OK Status code');
        }

        // Decode JSON response:
        $api_result = json_decode($response->getBody(), true);
        audit($api_result, __METHOD__, 'JSON RESPONSE');

        $params = $api_result['url'];
        if ($this->isResponseStatusError($params['status'])) {
            throw new CuttlyApiException('', (int)$params['status']);
        }

        $payload->fill($params);

        return $payload;
    }
}