<?php


namespace services\Cdp\Exceptions;
use Exception;

class InvalidTokenException extends Exception
{
    protected $message = 'Could not get a valid OAuth token';
}