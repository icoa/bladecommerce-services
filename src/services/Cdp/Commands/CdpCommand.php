<?php

namespace services\Cdp\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use services\Cdp\Repositories\CdpRepository;

class CdpCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'cdp:exec';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Performs some task to handle CDP web-service';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        /** @var CdpRepository $repository */
        $repository = app(CdpRepository::class);
        $repository->setConsole($this);
        $repository->test();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}