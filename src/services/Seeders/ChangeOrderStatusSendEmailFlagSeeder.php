<?php

namespace services\Seeders;

use Seeder;
use OrderState;
use DB;

class ChangeOrderStatusSendEmailFlagSeeder extends Seeder
{
    public function run()
    {
        DB::table('order_states')->whereNotIn('id', [OrderState::STATUS_SHIPPED])->update(['send_email' => 0, 'module_name' => null]);
        DB::table('order_states')->where('id', OrderState::STATUS_SHIPPED)->update(['send_email' => 1, 'module_name' => 'online_carrier']);
        $this->command->info('Order State send_email flag successfully seeder.');
    }
}