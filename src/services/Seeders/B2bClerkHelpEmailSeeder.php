<?php

namespace services\Seeders;

use Seeder;
use Email;
use DB;

class B2bClerkHelpEmailSeeder extends Seeder
{
    public function run()
    {
        $sampleRecord = Email::getByCode('ORDER_CONFIRM');
        $record = Email::where('code', 'B2B_CLERK_HELP')->first();
        if ($record) {
            $this->command->line('Emails table is already seeded.');
        } else {
            $data = [
                'code' => 'B2B_CLERK_HELP',
                'name' => 'Help-desk commesse B2B',
                'sdesc' => 'Messaggio inviato quando una commessa richiede help-desk nell\'area riservata commesse',
                'sender' => $sampleRecord->sender,
                'active' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $email_id = DB::table('emails')->insertGetId($data);
            $this->command->info('Emails table seeded!');

            $shared_data = [
                'shop_id' => 1,
                'subject' => 'B2B - Richiesta Help-desk commesse',
                'sender_name' => $sampleRecord->sender_name,
                'body' => '<p>Questo messaggio è stato inviato a seguito della richiesta di aiuto da parte di una commessa BlueSpirit</p><p><strong>Punto Vendita BlueSpirit:</strong> [SHOP_NAME]</p><p><strong>User ID:</strong> [USER_ID]</p><p><strong>Messaggio:</strong> [MESSAGE]</p>'
            ];

            $lang_translations = [
                'it' => $shared_data,
                'en' => $shared_data,
                'es' => $shared_data,
                'fr' => $shared_data,
                'de' => $shared_data,
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['email_id'] = $email_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('emails_lang')->insert($lang_data);
                $this->command->info("Emails Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }
}