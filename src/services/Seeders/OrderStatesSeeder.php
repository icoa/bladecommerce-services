<?php

namespace services\Seeders;

use Seeder;
use OrderState;
use DB;

class OrderStatesSeeder extends Seeder
{
    public function run()
    {
        //STATUS_PENDING_DELIVERY
        $this->addOrderState(OrderState::STATUS_PENDING_DELIVERY, [
            'delivery' => 1,
            'icon' => 'font-truck',
            'color' => '#673AB7',
        ], [
            'it' => 'In consegna',
            'en' => 'Delivering',
            'es' => 'En la entrega',
            'fr' => 'En livraison',
            'de' => 'In der Lieferung',
        ]);

        //STATUS_CANCELED_AFFILIATE
        $this->addOrderState(OrderState::STATUS_CANCELED_AFFILIATE, [
            'icon' => 'font-thumbs-down',
            'color' => '#D50000',
        ], [
            'it' => 'Annullato da affiliato',
            'en' => 'Cancelled by affiliate',
            'es' => 'Cancelado por el afiliado',
            'fr' => 'Annulé par affilié',
            'de' => 'Vom Affiliate storniert',
        ]);

        //STATUS_CANCELED_SAP
        OrderState::where('id', OrderState::STATUS_CANCELED_SAP)->delete();
        $this->addOrderState(OrderState::STATUS_CANCELED_SAP, [
            'icon' => 'font-remove',
            'color' => '#e62e00',
            'unremovable' => 1,
            'paid' => 1,
        ], [
            'it' => 'Annullato - non ritirato',
            'en' => 'Canceled - not withdrawn',
            'es' => 'Cancelado - no retirado',
            'fr' => 'Annulé - non retiré',
            'de' => 'Storniert - nicht zurückgezogen',
        ]);

        $shipped = OrderState::find(OrderState::STATUS_DELIVERED);
        $shipped->color = 'darkolivegreen';
        $shipped->icon = 'font-check';
        $shipped->save();

        //STATUS_REJECTED
        $this->addOrderState(OrderState::STATUS_REJECTED, [
            'icon' => 'font-thumbs-down',
            'color' => 'brown',
        ], [
            'it' => 'Respinto',
            'en' => 'Rejected',
            'es' => 'Rechazado',
            'fr' => 'Rejeté',
            'de' => 'Abgelehnt',
        ]);

        //STATUS_PICKUP_READY
        $this->addOrderState(OrderState::STATUS_READY_PICKUP, [
            'icon' => 'font-thumbs-up',
            'color' => '#77855C',
        ], [
            'it' => 'Pronto per il ritiro (in store)',
            'en' => 'Ready for withdrawal (in store)',
            'es' => 'Listo para la recogida (en tienda)',
            'fr' => 'Prêt pour la collecte (en magasin)',
            'de' => 'Abholbereit (im Laden)',
        ]);
    }

    /**
     * @param $id
     * @param array $data
     * @param array $translations
     */
    private function addOrderState($id, array $data, array $translations)
    {
        if (empty($data)) {
            $this->command->error("DATA provided is empty");
            return;
        }

        if (empty($translations)) {
            $this->command->error("TRANSLATION DATA provided is empty");
            return;
        }

        $defaults = [
            'invoice' => 0,
            'send_email' => 0,
            'module_name' => null,
            'unremovable' => 0,
            'hidden' => 0,
            'logable' => 0,
            'delivery' => 0,
            'shipped' => 0,
            'paid' => 0,
            'active' => 1,
            'deleted' => 0,
        ];

        $data = array_merge($defaults, $data);

        $record = OrderState::where('id', $id)->first();
        if ($record) {
            $this->command->info("OrderState [$id] already seeded");
            return;
        }
        $record = new OrderState();
        $record->fill($data);
        $record->id = $id;
        $record->save();
        $this->command->info("OrderState [$id] table seeded!");
        $order_state_id = $record->id;


        foreach ($translations as $lang_id => $name) {
            $lang_record = DB::table('order_states_lang')->where(compact('lang_id', 'order_state_id'))->first();
            if ($lang_record) {
                DB::table('order_states_lang')->where(compact('lang_id', 'order_state_id'))->update(compact('name'));
            } else {
                DB::table('order_states_lang')->insertGetId(compact('name', 'lang_id', 'order_state_id'));
            }
            $this->command->info("OrderState [$id] Lang ({$lang_id}) table seeded!");
        }
    }
}