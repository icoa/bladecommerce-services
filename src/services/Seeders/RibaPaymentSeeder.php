<?php

namespace services\Seeders;

use Seeder;
use Payment;
use PaymentState;
use Email;
use CustomerGroup;
use Country;
use DB;
use Exception;

class RibaPaymentSeeder extends Seeder
{
    public function run()
    {
        $module = 'riba';
        $record = Payment::where('module', $module)->first();
        $paymentState = PaymentState::where('module_name', $module)->first();
        $email = Email::where('code', 'RIBA_OK')->first();
        if (is_null($email) or is_null($paymentState)) {
            $this->command->comment('RiBa external requirement not satisfied. Please try to re-run this seeder');
            return;
        }
        if ($record) {
            $this->command->line('RiBa table is already seeded.');
        } else {

            DB::beginTransaction();

            try {
                $this->insert($module, $paymentState, $email);
                DB::commit();
            } catch (Exception $e) {
                DB::rollBack();
                $this->command->error($e->getMessage());
                audit_exception($e, __METHOD__);
            }

        }
    }


    private function insert($module, PaymentState $paymentState, Email $email)
    {
        $data = [
            'sdesc' => 'Tipo di pagamento Ri.Ba. per utenti B2B',
            'active' => config('app.project') == 'cinturino' ? 1 : 0,
            'price' => '0',
            'online' => '0',
            'is_default' => '0',
            'module' => $module,
            'payment_status' => $paymentState->id,
            'position' => 13,
            'logo' => '/media/images/payments/riba.gif',
            'carrier_restriction' => 0,
            'country_restriction' => 1,
            'group_restriction' => 1,
            'shop_restriction' => 0,
            'options' => null,
            'email_ok' => $email->id,
            'email_ko' => 0,
            'email_wait' => 0,
            'receipt' => 0,
            'created_by' => 1,
            'updated_by' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        $payment_id = DB::table('payments')->insertGetId($data);
        $this->command->info('Payments table seeded!');

        $text = '<p>Per i pagamenti effettuati con RIBA, i beni ordinati dal cliente saranno riservati per sette giorni dalla data di accettazione dell&rsquo;ordine, periodo nel quale il cliente dovr&agrave; inviare a Morellato.com la ricevuta di avvenuto pagamento via Mail all&#39;indirizzo info@morellato.com.</p>

<p>Il team di Morellato provveder&agrave; all&rsquo;invio di quanto ordinato solo all&rsquo;effettivo accredito della somma dovuta sui C/C di DIP s.p.a, tale accredito dovr&agrave; avvenire entro 15 giorni dalla data di accettazione dell&rsquo;ordine.&nbsp;Oltrepassata tale scadenze, l&rsquo;ordine verr&agrave; automaticamente annullato. La causale del RIBA dovra&#39; riportare necessariamente numero ordine data ordine nome e cognome cliente che ha effettuato l&rsquo;ordine.</p>

<p>Il RIBA dovr&agrave; essere intestato a :</p>

<p style="font-weight:bold;">Morellato s.p.a.<br />
FRATTE DI SANTA GIUSTINA IN COLLE (PDV), VIA COMMERCIALE 29,<br />
Banca: Cassa di Risparmio del Veneto<br />
<br />
IBAN: <span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;">IT 77 P 06225 62430 100000003601</span><br />
BIC: UNCRITMMOTC</p>';

        $lang_data = [
            'payment_id' => $payment_id,
            'lang_id' => 'it',
            'name' => 'RIBA - Ricevuta bancaria',
            'shop_id' => 1,
            'carttext' => $text,
            'aftertext' => $text,
        ];
        DB::table('payments_lang')->insert($lang_data);
        $this->command->info("RIBA Payments Lang ({$lang_data['lang_id']}) table seeded!");

        $text = 'The products ordered and paid with bank receipt in advance will be reserved for 7 days from the date of acceptance. In this period the customer has to send to Morellato the receipt of payments by email (at info@morellato.com).<br />
Morellato will ship the product ordered just after the credit entry on the Morellato\'s current account.<br />
The credit account has to take place within 15 days from the date of acceptance. Our staff will give warning the customer in the case of delay of credit entry. After this deadline, the order will be cancelled.<br />
The reason for payment must have the number of the order, date of order, name and surname of the customer that has made the order.<br />
<br />
Make the bank trasfer to:<br />
<p style="font-weight:bold;">Morellato s.p.a.<br />
FRATTE DI SANTA GIUSTINA IN COLLE (PDV), VIA COMMERCIALE 29,<br />
Banca: Cassa di Risparmio del Veneto<br />
<br />
IBAN: <span style="color: rgb(34, 34, 34); font-family: arial, sans-serif; font-size: 12.8px;">IT 77 P 06225 62430 100000003601</span><br />
BIC: UNCRITMMOTC</p>';

        $lang_data = [
            'payment_id' => $payment_id,
            'lang_id' => 'en',
            'name' => 'RIBA - Bank receipt',
            'shop_id' => 1,
            'carttext' => $text,
            'aftertext' => $text,
        ];
        DB::table('payments_lang')->insert($lang_data);
        $this->command->info("RIBA Payments Lang ({$lang_data['lang_id']}) table seeded!");

        $lang_data = [
            'payment_id' => $payment_id,
            'lang_id' => 'es',
            'name' => 'RIBA - Bank receipt',
            'shop_id' => 1,
            'carttext' => $text,
            'aftertext' => $text,
        ];
        DB::table('payments_lang')->insert($lang_data);
        $this->command->info("RIBA Payments Lang ({$lang_data['lang_id']}) table seeded!");

        $lang_data = [
            'payment_id' => $payment_id,
            'lang_id' => 'fr',
            'name' => 'RIBA - Bank receipt',
            'shop_id' => 1,
            'carttext' => $text,
            'aftertext' => $text,
        ];
        DB::table('payments_lang')->insert($lang_data);
        $this->command->info("RIBA Payments Lang ({$lang_data['lang_id']}) table seeded!");

        $lang_data = [
            'payment_id' => $payment_id,
            'lang_id' => 'de',
            'name' => 'RIBA - Bank receipt',
            'shop_id' => 1,
            'carttext' => $text,
            'aftertext' => $text,
        ];
        DB::table('payments_lang')->insert($lang_data);
        $this->command->info("RIBA Payments Lang ({$lang_data['lang_id']}) table seeded!");


        //now fill external requirements
        $countries = Country::where('iso_code', '!=', 'IT')->get();
        foreach ($countries as $country) {
            $country_id = $country->id;
            $existing = DB::table('payments_country')->where(compact('payment_id', 'country_id'))->first();
            if (is_null($existing))
                DB::table('payments_country')->insert(compact('payment_id', 'country_id'));
        }

        $group_id = config('cinturino.b2b_group', 5);
        $group_dng_id = config('cinturino.b2b_group_danger', 8);
        $groups = CustomerGroup::whereNotIn('id', [$group_id, $group_dng_id])->get();
        foreach ($groups as $group) {
            $group_id = $group->id;
            $existing = DB::table('payments_group')->where(compact('payment_id', 'group_id'))->first();
            if (is_null($existing))
                DB::table('payments_group')->insert(compact('payment_id', 'group_id'));
        }

        $this->command->info("Payment RIBA successfully seeded!");
    }
}