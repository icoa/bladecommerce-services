<?php

namespace services\Seeders;

use Seeder;
use Email;
use DB;

class StrapB2BRegisterEmailSeeder extends Seeder
{
    public function run()
    {
        $sampleRecord = Email::getByCode('ORDER_CONFIRM');
        $code = 'REGISTER_B2B';
        $record = Email::where('code', $code)->first();
        if ($record) {
            $this->command->line('REGISTER_B2B Emails table is already seeded.');
        } else {
            $data = [
                'code' => $code,
                'name' => 'Registrazione B2B',
                'sdesc' => 'Invio email con i dati del nuovo Rivenditore autorizzato',
                'sender' => $sampleRecord->sender,
                'active' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $email_id = DB::table('emails')->insertGetId($data);
            $this->command->info('REGISTER_B2B Emails table seeded!');

            $lang_translations = [
                'it' => [
                    'shop_id' => 1,
                    'subject' => 'Richiesta di registrazione Rivenditore autorizzato dal sito [SITE]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => "<p>Nuova richiesta di registrazione Rivenditore autorizzato dal sito <strong>[SITE]</strong>.</p><hr><p>Di seguito i dettagli dei dati forniti per la registrazione:</p>[DETAILS]"
                ],
                'en' => [
                    'shop_id' => 1,
                    'subject' => 'New B2B registration on website [SITE]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => "<p>A new registration request has been sent from <strong>[SITE]</strong> website.</p><hr><p>Here are the details:</p>[DETAILS]"
                ],
                'es' => [
                    'shop_id' => 1,
                    'subject' => 'New B2B registration on website [SITE]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => "<p>A new registration request has been sent from <strong>[SITE]</strong> website.</p><hr><p>Here are the details:</p>[DETAILS]"
                ],
                'fr' => [
                    'shop_id' => 1,
                    'subject' => 'New B2B registration on website [SITE]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => "<p>A new registration request has been sent from <strong>[SITE]</strong> website.</p><hr><p>Here are the details:</p>[DETAILS]"
                ],
                'de' => [
                    'shop_id' => 1,
                    'subject' => 'New B2B registration on website [SITE]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => "<p>A new registration request has been sent from <strong>[SITE]</strong> website.</p><hr><p>Here are the details:</p>[DETAILS]"
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['email_id'] = $email_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('emails_lang')->insert($lang_data);
                $this->command->info("REGISTER_B2B Emails Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }
}