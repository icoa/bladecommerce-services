<?php

namespace services\Seeders;

use Seeder;
use DB;

class ModulePositionSeeder extends Seeder
{

    private function upsertPosition($name)
    {
        $record = DB::table('module_positions')->where('name', $name)->first();
        if ($record) {
            $this->command->line("Module position $name is already seeded.");
        } else {
            $data = [
                'name' => $name,
                'active' => 1,
                'is_design' => 1,
                'row_index' => 0,
                'position' => 0,
                'width' => 12,
            ];
            DB::table('module_positions')->insert($data);
            $this->command->info("Module position $name is successfully seeded.");
        }
    }

    public function run()
    {
        $this->upsertPosition('mobile_header_search_footer');
        $this->upsertPosition('mobile_product_tabs_footer');
        $this->upsertPosition('mobile_catalog_grid_header');
        $this->upsertPosition('mobile_catalog_grid_footer');
        $this->upsertPosition('catalog_grid_footer');
        $this->upsertPosition('catalog_grid_header');
        $this->upsertPosition('mobile_after_header');
    }
}