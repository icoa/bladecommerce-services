<?php

namespace services\Seeders;

use Seeder;
use Sentry;
use DB;
use Exception;

class ApiOrdersSeeder extends Seeder
{
    public function run()
    {
        $this->group();
        $this->user();
    }

    private function group()
    {
        $permissions = '{"api":1}';
        $name = 'Api';
        $record = DB::table('groups')->where('name', $name)->first();
        if (is_null($record)) {
            $created_at = date('Y-m-d H:i:s');
            $updated_at = date('Y-m-d H:i:s');
            DB::table('groups')->insert(compact('name', 'permissions', 'created_at', 'updated_at'));
            $this->command->info("API group 'api' seeded");
        } else {
            DB::table('groups')->where('id', $record->id)->update(compact('name', 'permissions'));
            $this->command->line("API group 'api' already seeded");
        }
    }

    private function user()
    {
        $data = [
            'email' => config('api.e-one.email'),
            'password' => config('api.e-one.password'),
            'first_name' => 'E-One API',
            'last_name' => null,
            'activated' => 1,
            'shop_id' => null,
            'negoziando' => null,
            'clerk' => null,
            'affiliate_id' => null,
        ];
        $this->createApiUser($data);

        $data = [
            'email' => config('api.e1.email'),
            'password' => config('api.e1.password'),
            'first_name' => 'E-One API (alt.)',
            'last_name' => null,
            'activated' => 1,
            'shop_id' => null,
            'negoziando' => null,
            'clerk' => null,
            'affiliate_id' => null,
        ];
        $this->createApiUser($data);
    }

    private function isEnabled($email){
        return in_array(config('api.enabled'), $email, true);
    }

    private function createApiUser(array $data){
        $existingUser = null;
        try{
            $existingUser = Sentry::findUserByLogin($data['email']);
            if($existingUser && false === $this->isEnabled($data['email'])){
                $existingUser->activated = 0;
                $existingUser->save();
                $this->command->line("API user {$data['email']} has been disabled");
                return;
            }
        }catch (Exception $e){

        }

        if ($existingUser === null) {
            $user = Sentry::createUser($data);
            $user->addGroup(Sentry::findGroupByName('Api'));
            $this->command->info("API user {$data['email']} seeded");
        } else {
            $this->command->line("API user {$data['email']} already seeded");
        }
    }
}