<?php

namespace services\Seeders;

use Seeder;
use Email;
use DB;

class RibaEmailSeeder extends Seeder
{
    public function run()
    {
        $sampleRecord = Email::getByCode('ORDER_CONFIRM');
        $code = 'RIBA_OK';
        $record = Email::where('code', $code)->first();
        if ($record) {
            $this->command->line('RIBA Emails table is already seeded.');
        } else {
            $data = [
                'code' => $code,
                'name' => 'Email per esito pagamento RIBA',
                'sdesc' => 'E-mail inviata all\'utente dopo aver effettuato un ordine se il pagamento è tramite RIBA',
                'sender' => $sampleRecord->sender,
                'active' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $email_id = DB::table('emails')->insertGetId($data);
            $this->command->info('RIBA Emails table seeded!');

            $lang_translations = [
                'it' => [
                    'shop_id' => 1,
                    'subject' => 'Pagamento con RIBA per ordine [ORDER_NUMBER]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => '<span style="font-weight:bold;font-size:1.5em;">Pagamento con RIBA per ordine [ORDER_NUMBER]</span><br />
<br />
Gentile [CUSTOMER_NAME],<br />
La informiamo che i dati per il pagamento con RIBA di [ORDER_TOTAL] dell\'ordine numero [ORDER_NUMBER], effettuato in data [ORDER_DATE], sono i seguenti:<br />
<br />
<span style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;">MORELLATO s.p.a.</span><br style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;" />
<span style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;">FRATTE DI SANTA GIUSTINA IN COLLE (PDV), VIA COMMERCIALE 29,</span><br style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;" />
<span style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;">Banca: CASSA DI RISPARMIO DEL VENETO</span><br style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;" />
<span style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;">C/C : 00000003601</span><br style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;" />
<span style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;">IBAN: IT77P0622562430100000003601</span><br style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;" />
<span style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;">BIC: IBSPIT2P</span><br style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;" />
<span style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;">Note: Ordine N°[ORDER_NUMBER]</span><br style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;" />
<span style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;">La preghiamo di inviare la ricevuta di avvenuto pagamento all\'indirizzo mail </span><span style="color: rgb(38, 38, 38); font-family: Roboto, Arial, sans-serif;">customercare@morellatostraps.com </span><span style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;">. Per informazioni puo contattare il Servizio Clienti al Numero Verde </span>800.858.423<span style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;"> in orario 10.00-19.00, oppure scrivendo a </span><span style="color: rgb(38, 38, 38); font-family: Roboto, Arial, sans-serif;">customercare@morellatostraps.com </span><span style="color: rgb(38, 50, 56); font-family: Roboto, Arial, sans-serif;">. Cordialmente, </span>Lo Staff di Cinturino by Morellato.'
                ],
                'en' => [
                    'shop_id' => 1,
                    'subject' => 'Payment through bank receipt of the order number [ORDER_NUMBER]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => '<span style="font-weight:bold;font-size:1.5em;">Payment by bank receipt for order number [ORDER_NUMBER] </span><br />
<br />
Please take note of all the details for the payment by bank receipt of [ORDER_TOTAL] of the order number [ORDER_NUMBER], made [ORDER_DATE] :<br />
<br />
MORELLATO s.p.a.<br />
FRATTE DI SANTA GIUSTINA IN COLLE (PDV), VIA COMMERCIALE 29,<br />
Banca: CASSA DI RISPARMIO DEL VENETO<br />
C/C : 00000003601<br />
IBAN: IT77P0622562430100000003601<br />
BIC: IBSPIT2P<br />
Note: Order number [ORDER_NUMBER]<br />
<br />
We kindly ask you to send us back the payment receipt by bank transfer to the following e-mail address customercare@morellatostraps.com. For further information you can contact the Customer Care to the Free Number 800.858.423 Monday-Friday from 10:00 am to 7:00 pm, or by e-mail to customercare@morellatostraps.com.<br />
<br />
Kind Regards, <br />
<br />
Team Cinturino by Morellato'
                ],
                'es' => [
                    'shop_id' => 1,
                    'subject' => 'Payment through bank receipt of the order number [ORDER_NUMBER]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => '<span style="font-weight:bold;font-size:1.5em;">Payment by bank receipt for order number [ORDER_NUMBER] </span><br />
<br />
Please take note of all the details for the payment by bank receipt of [ORDER_TOTAL] of the order number [ORDER_NUMBER], made [ORDER_DATE] :<br />
<br />
MORELLATO s.p.a.<br />
FRATTE DI SANTA GIUSTINA IN COLLE (PDV), VIA COMMERCIALE 29,<br />
Banca: CASSA DI RISPARMIO DEL VENETO<br />
C/C : 00000003601<br />
IBAN: IT77P0622562430100000003601<br />
BIC: IBSPIT2P<br />
Note: Order number [ORDER_NUMBER]<br />
<br />
We kindly ask you to send us back the payment receipt by bank transfer to the following e-mail address customercare@morellatostraps.com. For further information you can contact the Customer Care to the Free Number 800.858.423 Monday-Friday from 10:00 am to 7:00 pm, or by e-mail to customercare@morellatostraps.com.<br />
<br />
Kind Regards, <br />
<br />
Team Cinturino by Morellato'
                ],
                'fr' => [
                    'shop_id' => 1,
                    'subject' => 'Payment through bank receipt of the order number [ORDER_NUMBER]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => '<span style="font-weight:bold;font-size:1.5em;">Payment by bank receipt for order number [ORDER_NUMBER] </span><br />
<br />
Please take note of all the details for the payment by bank receipt of [ORDER_TOTAL] of the order number [ORDER_NUMBER], made [ORDER_DATE] :<br />
<br />
MORELLATO s.p.a.<br />
FRATTE DI SANTA GIUSTINA IN COLLE (PDV), VIA COMMERCIALE 29,<br />
Banca: CASSA DI RISPARMIO DEL VENETO<br />
C/C : 00000003601<br />
IBAN: IT77P0622562430100000003601<br />
BIC: IBSPIT2P<br />
Note: Order number [ORDER_NUMBER]<br />
<br />
We kindly ask you to send us back the payment receipt by bank transfer to the following e-mail address customercare@morellatostraps.com. For further information you can contact the Customer Care to the Free Number 800.858.423 Monday-Friday from 10:00 am to 7:00 pm, or by e-mail to customercare@morellatostraps.com.<br />
<br />
Kind Regards, <br />
<br />
Team Cinturino by Morellato'
                ],
                'de' => [
                    'shop_id' => 1,
                    'subject' => 'Payment through bank receipt of the order number [ORDER_NUMBER]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => '<span style="font-weight:bold;font-size:1.5em;">Payment by bank receipt for order number [ORDER_NUMBER] </span><br />
<br />
Please take note of all the details for the payment by bank receipt of [ORDER_TOTAL] of the order number [ORDER_NUMBER], made [ORDER_DATE] :<br />
<br />
MORELLATO s.p.a.<br />
FRATTE DI SANTA GIUSTINA IN COLLE (PDV), VIA COMMERCIALE 29,<br />
Banca: CASSA DI RISPARMIO DEL VENETO<br />
C/C : 00000003601<br />
IBAN: IT77P0622562430100000003601<br />
BIC: IBSPIT2P<br />
Note: Order number [ORDER_NUMBER]<br />
<br />
We kindly ask you to send us back the payment receipt by bank transfer to the following e-mail address customercare@morellatostraps.com. For further information you can contact the Customer Care to the Free Number 800.858.423 Monday-Friday from 10:00 am to 7:00 pm, or by e-mail to customercare@morellatostraps.com.<br />
<br />
Kind Regards, <br />
<br />
Team Cinturino by Morellato'
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['email_id'] = $email_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('emails_lang')->insert($lang_data);
                $this->command->info("RIBA Emails Lang ({$lang_data['lang_id']}) table seeded!");
            }

        }
    }
}