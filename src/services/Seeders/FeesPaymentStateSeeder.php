<?php

namespace services\Seeders;

use Seeder;
use PaymentState;
use PaymentState_Lang;
use DB;

class FeesPaymentStateSeeder extends Seeder
{
    public function run()
    {
        $id = 9;
        $record = PaymentState::find($id);
        $lang_matrix = [
            'it' => 'Stornato',
            'en' => 'Reversed',
            'es' => 'Revertido',
            'fr' => 'Renversé',
            'de' => 'Umgekehrt',
        ];
        $record->active = 1;
        $record->color = '#7F00FF';
        $record->icon = 'font-remove-circle';
        $record->invoice = 0;
        $record->send_email = 0;
        $record->paid = 0;
        $record->save();
        $this->command->info('PaymentState table seeded!');

        $translations = PaymentState_Lang::where('payment_state_id', $id)->get();
        foreach ($translations as $translation) {
            DB::table('payment_states_lang')->where(['payment_state_id' => $id, 'lang_id' => $translation->lang_id])->update(['name' => $lang_matrix[$translation->lang_id]]);
            $this->command->info("PaymentState Lang ({$translation->lang_id}) table seeded!");
        }
    }
}