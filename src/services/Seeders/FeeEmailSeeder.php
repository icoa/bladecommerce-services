<?php

namespace services\Seeders;

use Seeder;
use Email;
use DB;

class FeeEmailSeeder extends Seeder
{
    public function run()
    {
        $sampleRecord = Email::getByCode('ORDER_CONFIRM');
        $code = 'FEES_ATTACHMENT';
        $record = Email::where('code', $code)->first();
        if ($record) {
            $this->command->line('Fees Emails table is already seeded.');
        } else {
            $data = [
                'code' => $code,
                'name' => 'Invio ricevuta fiscale al cliente',
                'sdesc' => 'Messaggio inviato al cliente con in allegato la ricevuta fiscale (o storno)',
                'sender' => $sampleRecord->sender,
                'active' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $email_id = DB::table('emails')->insertGetId($data);
            $this->command->info('Fees Emails table seeded!');

            $lang_translations = [
                'it' => [
                    'shop_id' => 1,
                    'subject' => '[SITE] - [DOCUMENT]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => "<p>Gentile [CUSTOMER_NAME],<br>in allegato trova la ricevuta fiscale relativa all'ordine <strong>[ORDER_REFERENCE]</strong>.</p><p>Si tratta di un documento in originale e non verrà effettuato nessun invio postale.</p><p>Il presente documento viene trasmesso tramite servizio di posta elettronica quindi è necessario da parte vostra stamparlo e conservarlo come da DPR 633/72, successive modifiche e da risoluzione delle Finanze prot. 450217 del 30 luglio 1990.</p>"
                ],
                'en' => [
                    'shop_id' => 1,
                    'subject' => '[SITE] - [DOCUMENT]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => "<p>Dear [CUSTOMER_NAME],<br>attached is the tax receipt for the order <strong>[ORDER_REFERENCE]</strong>.</p><p>This is an original document and will not be submitted no mailing.</p><p>This document is transmitted via e-mail service so it is necessary for you to print it and keep it as per Presidential Decree 633/72, subsequent amendments and resolution of Finance prot. 450217 of 30 July 1990.</p>"
                ],
                'es' => [
                    'shop_id' => 1,
                    'subject' => '[SITE] - [DOCUMENT]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => "<p>Estimado [CUSTOMER_NAME],<br>adjunto se encuentra el recibo de impuestos del pedido <strong>[ORDER_REFERENCE]</strong>.</p><p>Este es un documento original y no se enviará no hay envío.</p><p>Este documento se transmite por correo electrónico, por lo que es necesario que lo imprima y guarde según el Decreto Presidencial 633/72, enmiendas y resolución posteriores de Finance prot. 450217 del 30 de julio de 1990.</p>"
                ],
                'fr' => [
                    'shop_id' => 1,
                    'subject' => '[SITE] - [DOCUMENT]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => "<p>Cher [CUSTOMER_NAME], le document joint est le reçu fiscal pour la commande <strong>[ORDER_REFERENCE]</strong>.</p><p>Ce document est original et ne sera pas soumis. pas de mailing.</p><p>Ce document est transmis par e-mail, il est donc nécessaire de l’imprimer et de le conserver conformément au décret présidentiel 633/72, aux modifications ultérieures et à la résolution de Finance prot. 450217 du 30 juillet 1990.</p>"
                ],
                'de' => [
                    'shop_id' => 1,
                    'subject' => '[SITE] - [DOCUMENT]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => "<p>Sehr geehrte [CUSTOMER_NAME],<br>angefügt ist der Steuerbeleg für die Bestellung <strong>[ORDER_REFERENCE]</strong>.</p><p>Dies ist ein Originaldokument und wird nicht übermittelt keine Mailing.</p><p>Dieses Dokument wird per E-Mail-Service übertragen, so dass es notwendig ist, Sie zu drucken und es gemäß Präsidialverordnung 633/72, spätere Änderungen und Auflösung von Finance Prot zu halten. 450217 vom 30. Juli 1990.</p>"
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['email_id'] = $email_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('emails_lang')->insert($lang_data);
                $this->command->info("Fees Emails Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }
}