<?php

namespace services\Seeders;

use Seeder;
use Lexicon;
use DB;

class SoldoutLexiconSeeder extends Seeder
{
    public function run()
    {

        $record = Lexicon::where('code', 'msg_soldout')->first();
        if ($record) {
            $this->command->line('SoldoutLexicon table is already seeded.');
        } else {
            $data = [
                'code' => 'msg_soldout',
                'ldesc' => 'Messaggio visualizzato nella scheda prodotto in caso di Sold Out',
                'is_shortcode' => '0',
                'is_deep' => '0',
                'is_javascript' => '0',
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info('Lexicons table seeded!');

            $lang_translations = [
                'it' => [
                    'name' => '<p>Questo articolo purtroppo non è al momento disponibile per l\'acquisto online perchè le scorte sono esaurite;<br><br><strong>in alternativa ti consigliamo di dare un\'occhiata ai seguenti prodotti simili a <u>:name</u></strong></p>'
                ],
                'en' => [
                    'name' => '<p>This product is not available for online purchase, since is sold-out at the moment;<br><br><strong>alternatevely we suggest to take a look at the following items similar to <u>:name</u></strong></p>'
                ],
                'es' => [
                    'name' => '<p>Este producto no está disponible actualmente para la compra en línea, ya que está agotado;<br><br><strong>alternativas sugerimos que eche un vistazo a los siguientes artículos similares a <u>:name</u></strong></p>'
                ],
                'fr' => [
                    'name' => '<p>Ce produit n\'est actuellement pas disponible pour l\'achat en ligne, car il est complet;<br><br><strong>alternatives, nous suggérons de jeter un oeil aux éléments suivants similaires à <u>:name</u></strong></p>'
                ],
                'de' => [
                    'name' => '<p>Dieses Produkt ist derzeit nicht für den Online-Kauf verfügbar, da es ausverkauft ist;<br><br><strong>Wir empfehlen Ihnen einen Blick auf die folgenden ähnlichen Artikel wie <u>:name</u></strong></p>'
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }
}