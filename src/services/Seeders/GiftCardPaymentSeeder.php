<?php

namespace services\Seeders;

use Seeder;
use Payment;
use DB;

class GiftCardPaymentSeeder extends Seeder
{
    public function run()
    {

        $record = Payment::where('module', 'giftcard')->first();
        if ($record) {
            $this->command->line('GiftCardPayment table is already seeded.');
        } else {
            $data = [
                'sdesc' => 'Questo gateway viene auto-selezionato dal sistema solo in caso un carrello con una Gift Card azzera il totale ordine',
                'active' => '1',
                'price' => '0',
                'is_default' => '0',
                'module' => 'giftcard',
                'payment_status' => 2,
                'position' => 12,
                'carrier_restriction' => 0,
                'country_restriction' => 0,
                'group_restriction' => 0,
                'shop_restriction' => 0,
                'options' => null,
                'email_ok' => 0,
                'email_ko' => 0,
                'email_wait' => 0,
                'receipt' => 0,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $payment_id = DB::table('payments')->insertGetId($data);
            $this->command->info('Payments table seeded!');

            $lang_data = [
                'payment_id' => $payment_id,
                'lang_id' => 'it',
                'name' => 'Gift Card',
                'shop_id' => 1,
                'carttext' => '',
                'aftertext' => 'Le ricordiamo che l&#39;importo della Gift Card verr&agrave; decurtato solo per il parziale utilizzato in questo ordine.',
            ];
            DB::table('payments_lang')->insert($lang_data);
            $this->command->info("Payments Lang ({$lang_data['lang_id']}) table seeded!");

            $lang_data = [
                'payment_id' => $payment_id,
                'lang_id' => 'en',
                'name' => 'Gift Card',
                'shop_id' => 1,
                'carttext' => '',
                'aftertext' => 'We remind you that the amount of the Gift Card will be deducted only for the partial used in this order.',
            ];
            DB::table('payments_lang')->insert($lang_data);
            $this->command->info("Payments Lang ({$lang_data['lang_id']}) table seeded!");

            $lang_data = [
                'payment_id' => $payment_id,
                'lang_id' => 'es',
                'name' => 'Gift Card',
                'shop_id' => 1,
                'carttext' => '',
                'aftertext' => 'Le recordamos que el monto de la Tarjeta de regalo se deducir&aacute; solo por el parcial utilizado en este pedido.',
            ];
            DB::table('payments_lang')->insert($lang_data);
            $this->command->info("Payments Lang ({$lang_data['lang_id']}) table seeded!");

            $lang_data = [
                'payment_id' => $payment_id,
                'lang_id' => 'fr',
                'name' => 'Gift Card',
                'shop_id' => 1,
                'carttext' => '',
                'aftertext' => 'Nous vous rappelons que le montant de la Carte Cadeau ne sera d&eacute;duit que pour le montant partiel utilis&eacute; dans cette commande.',
            ];
            DB::table('payments_lang')->insert($lang_data);
            $this->command->info("Payments Lang ({$lang_data['lang_id']}) table seeded!");

            $lang_data = [
                'payment_id' => $payment_id,
                'lang_id' => 'de',
                'name' => 'Gift Card',
                'shop_id' => 1,
                'carttext' => '',
                'aftertext' => 'Wir weisen darauf hin, dass der Betrag der Geschenkkarte nur für die in dieser Bestellung verwendeten Teile abgezogen wird.',
            ];
            DB::table('payments_lang')->insert($lang_data);
            $this->command->info("Payments Lang ({$lang_data['lang_id']}) table seeded!");
        }
    }
}