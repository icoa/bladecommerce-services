<?php

namespace services\Seeders;

use Seeder;
use Lexicon;
use DB;

class CustomerGroupsSeeder extends Seeder
{
    public function run()
    {
        $this->add_lexicon();
        $this->add_groups();
        $this->add_widget();
    }

    public function add_groups()
    {
        DB::table('customers_groups')->where('id', 1)->update(['code' => 'VISITOR']);
        DB::table('customers_groups')->where('id', 2)->update(['code' => 'GUEST']);
        DB::table('customers_groups')->where('id', 3)->update(['code' => 'CUSTOMER']);

        $code = 'VODAFONE';
        $existing = \CustomerGroup::findByCode($code);
        if (null === $existing) {
            $model = new \CustomerGroup([
                'code' => $code,
                'redirect' => 'trend|1',
            ]);
            $model->save();

            $lang_translations = [
                'it' => [
                    'name' => 'Vodafone'
                ],
                'en' => [
                    'name' => 'Vodafone'
                ],
                'es' => [
                    'name' => 'Vodafone'
                ],
                'fr' => [
                    'name' => 'Vodafone'
                ],
                'de' => [
                    'name' => 'Vodafone'
                ],
            ];
            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['customer_group_id'] = $model->id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('customers_groups_lang')->insert($lang_data);
                $this->command->info("Customer Group Lang $code({$lang_data['lang_id']}) key seeded!");
            }
            $this->command->info("$code customer group seeded!");
        } else {
            $this->command->line("$code customer group is already seeded.");
        }
    }


    public function add_lexicon()
    {
        $this->insert_lexicon('login_title', 'Titolo pagina Login senza social', [
            'it' => [
                'name' => 'Sei registrato? Accedi al sito'
            ],
            'en' => [
                'name' => 'Are you registered? Login here'
            ],
            'es' => [
                'name' => '¿Estás registrado? Accedes al sitio'
            ],
            'fr' => [
                'name' => 'Êtes-vous inscrit? Accédez au site'
            ],
            'de' => [
                'name' => 'Bist du registriert? Zugriff auf die Website'
            ],
        ]);

        $this->insert_lexicon('msg_already_logged', 'Messaggio già loggato', [
            'it' => [
                'name' => 'Attenzione: sei già autenticato in questo sito'
            ],
            'en' => [
                'name' => 'Warning: you are already authenticated on this site'
            ],
            'es' => [
                'name' => 'Advertencia: ya está autenticado en este sitio'
            ],
            'fr' => [
                'name' => 'Attention: vous êtes déjà authentifié sur ce site'
            ],
            'de' => [
                'name' => ''
            ],
        ]);

        $this->insert_lexicon('msg_please_logout', 'Invito a sloggare', [
            'it' => [
                'name' => 'Per poter utilizzare questa pagina/sezione del sito è necessario effettuare il Logout oppure puoi tornare al pannello di controllo del tuo Account.'
            ],
            'en' => [
                'name' => 'In order to use this page/section of the site you must log out or you can return to the control panel of your account.'
            ],
            'es' => [
                'name' => 'Para utilizar esta página/sección del sitio, debe cerrar sesión o puede volver al panel de control de su cuenta.'
            ],
            'fr' => [
                'name' => 'Pour utiliser cette page/section du site, vous devez vous déconnecter ou vous pouvez retourner au panneau de contrôle de votre compte.'
            ],
            'de' => [
                'name' => ''
            ],
        ]);
    }


    private function insert_lexicon($code, $ldesc, $lang_translations)
    {
        $record = Lexicon::where('code', $code)->first();
        if ($record) {
            $this->command->line("$code lexicon key is already seeded.");
        } else {
            $data = [
                'code' => $code,
                'ldesc' => $ldesc,
                'is_shortcode' => '0',
                'is_deep' => '0',
                'is_javascript' => '0',
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info("$code Lexicon key seeded!");

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang $code({$lang_data['lang_id']}) key seeded!");
            }
        }
    }

    public function add_widget()
    {
        $id = 88;
        $existing = DB::table('widgets')->find($id);
        if ($existing === null) {
            $data = [
                'id' => $id,
                'parent_id' => 53,
                'name' => 'Short links',
                'controller' => 'services\Cuttly\Controllers\ShortLinksController@getIndex',
                'namedRoute' => null,
                'isNav' => 1,
                'active' => 1,
                'disabled' => 0,
                'iconImg' => null,
                'exact' => 0,
                'position' => 60,
            ];
            DB::table('widgets')->insert($data);
            $this->command->info('Widget for ShortLinks seeded!');
        } else {
            $this->command->line('Widget for ShortLinks is already seeded.');
        }
    }
}