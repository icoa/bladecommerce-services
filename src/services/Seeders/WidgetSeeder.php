<?php

namespace services\Seeders;

use Seeder;
use DB;

class WidgetSeeder extends Seeder
{
    public function run()
    {

        $record = DB::table('widgets')->find(84);
        if ($record) {
            $this->command->line('Widgets "Importazione catalogo Excel" is already seeded.');
        } else {
            $data = [
                'id' => 84,
                'parent_id' => 63,
                'name' => 'Importazione catalogo Excel',
                'controller' => 'services\Bluespirit\Backend\Controllers\ExcelFileImportController@getIndex',
                'namedRoute' => null,
                'isNav' => 0,
                'active' => 1,
                'disabled' => 0,
                'iconImg' => null,
                'exact' => 0,
                'position' => 100,
            ];
            DB::table('widgets')->insert($data);
            $this->command->info('Widgets table seeded!');
        }
        
        $record = DB::table('widgets')->find(85);
        if ($record) {
            $this->command->line('Widgets "Gestione Negozi Reseller" is already seeded.');
        } else {
            $data = [
                'id' => 85,
                'parent_id' => 63,
                'name' => 'Gestione Negozi Reseller',
                'controller' => 'ResellerController@getIndex',
                'namedRoute' => null,
                'isNav' => 0,
                'active' => 1,
                'disabled' => 0,
                'iconImg' => null,
                'exact' => 0,
                'position' => 100,
            ];
            DB::table('widgets')->insert($data);
            $this->command->info('Widgets table seeded!');
        }

        $record = DB::table('widgets')->find(86);
        if ($record) {
            $this->command->line('Widgets table is already seeded.');
        } else {
            $data = [
                'id' => 86,
                'parent_id' => 63,
                'name' => 'Gestione Affiliati',
                'controller' => 'services\Membership\Controllers\AffiliatesController@getIndex',
                'namedRoute' => null,
                'isNav' => 0,
                'active' => 1,
                'disabled' => 0,
                'iconImg' => null,
                'exact' => 0,
                'position' => 100,
            ];
            DB::table('widgets')->insert($data);
            $this->command->info('Widgets table seeded!');
        }
    }
}