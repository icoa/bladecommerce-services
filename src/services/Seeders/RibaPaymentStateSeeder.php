<?php

namespace services\Seeders;

use Seeder;
use PaymentState;
use PaymentState_Lang;
use DB;

class RibaPaymentStateSeeder extends Seeder
{
    public function run()
    {
        $template = 'riba';
        $record = PaymentState::where('module_name', $template)->first();
        if ($record) {
            $this->command->info("RiBa PaymentState already seeded");
            return;
        }
        $record = new PaymentState();
        $lang_matrix = [
            'it' => 'In attesa di pagamento con Ri.Ba.',
            'en' => 'Pending payment by Ri.Ba.',
            'es' => 'Pago pendiente por Ri.Ba.',
            'fr' => 'En attente de paiement par Ri.Ba.',
            'de' => 'Ausstehende Zahlung per Ri.Ba.',
        ];
        $record->invoice = 0;
        $record->send_email = 1;
        $record->module_name = $template;
        $record->color = '#AE46F0';
        $record->icon = 'font-certificate';
        $record->unremovable = 1;
        $record->hidden = config('app.project') == 'cinturino' ? 0 : 1;
        $record->logable = 0;
        $record->delivery = 0;
        $record->shipped = 0;
        $record->paid = 0;
        $record->failed = 0;
        $record->wait = 1;
        $record->active = config('app.project') == 'cinturino' ? 1 : 0;
        $record->deleted = 0;
        $record->save();
        $this->command->info('RiBa PaymentState table seeded!');
        $payment_state_id = $record->id;


        foreach ($lang_matrix as $lang_id => $name) {
            DB::table('payment_states_lang')->insertGetId(compact('name', 'lang_id', 'template', 'payment_state_id'));
            $this->command->info("RiBa PaymentState Lang ({$lang_id}) table seeded!");
        }
    }
}