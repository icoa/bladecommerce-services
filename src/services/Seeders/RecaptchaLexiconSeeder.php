<?php

namespace services\Seeders;

use Seeder;
use Lexicon;
use DB;

class RecaptchaLexiconSeeder extends Seeder
{
    public function run()
    {
        $code = 'msg_recaptcha';
        $record = Lexicon::where('code', $code)->first();
        if ($record) {
            $this->command->line("$code lexicon key is already seeded.");
        } else {
            $data = [
                'code' => $code,
                'ldesc' => 'Messaggio visualizzato accanto al widget di reCaptcha',
                'is_shortcode' => '0',
                'is_deep' => '0',
                'is_javascript' => '0',
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info("$code Lexicon key seeded!");

            $lang_translations = [
                'it' => [
                    'name' => 'Per la vostra sicurezza su questo sito è stata implementata una nuova procedura di verifica utente tramite servizio Google denominato "recaptcha"'
                ],
                'en' => [
                    'name' => 'Concerning your security this website has implemented a new user verify procedure through a Google service named "recaptcha"'
                ],
                'es' => [
                    'name' => 'Con respecto a su seguridad Esto sitio web ha implementado un nuevo procedimiento de verificación de usuario a través de un servicio de Google llamado "recaptcha"'
                ],
                'fr' => [
                    'name' => 'Concernant votre sécurité Le site Web a mis en œuvre une nouvelle procédure de vérification de l\'utilisateur par le biais d\'un service Google appelé "recaptcha"'
                ],
                'de' => [
                    'name' => 'In Übereinstimmung mit den neuen Sicherheitsanforderungen ist es erforderlich, den Benutzer über einen externen Dienst namens "recaptcha" von Google zu verifizieren'
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang $code({$lang_data['lang_id']}) key seeded!");
            }
        }


        $code = 'lbl_recaptcha';
        $record = Lexicon::where('code', $code)->first();
        if ($record) {
            $this->command->line("$code lexicon key is already seeded.");
        } else {
            $data = [
                'code' => $code,
                'ldesc' => 'Titolo area verifica di sicurezza',
                'is_shortcode' => '0',
                'is_deep' => '0',
                'is_javascript' => '0',
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info("$code Lexicon key seeded!");

            $lang_translations = [
                'it' => [
                    'name' => 'Verifica di sicurezza'
                ],
                'en' => [
                    'name' => 'Security verification'
                ],
                'es' => [
                    'name' => 'Verificación de seguridad'
                ],
                'fr' => [
                    'name' => 'Vérification de sécurité'
                ],
                'de' => [
                    'name' => 'Sicherheitsüberprüfung'
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang $code({$lang_data['lang_id']}) key seeded!");
            }
        }
    }
}