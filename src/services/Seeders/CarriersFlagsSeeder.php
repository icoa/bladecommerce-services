<?php

namespace services\Seeders;

use Seeder;
use DB;
use Carrier;

class CarriersFlagsSeeder extends Seeder
{
    public function run()
    {
        $ids = [
            Carrier::TYPE_B2B_NATIONAL_DELIVERY,
            Carrier::TYPE_NATIONAL_DELIVERY,
            Carrier::TYPE_INTERNATIONAL_DELIVERY,
        ];

        foreach($ids as $id){
            /** @var Carrier $carrier */
            $carrier = Carrier::find($id);
            if($carrier){
                if($carrier->need_range == 1){
                    $this->command->comment("Carrier id [$carrier->id] already changed");
                }else{
                    DB::table('carriers')->where('id', $id)->update(['need_range' => 1]);
                    $carrier->uncache();
                    $this->command->comment("Carrier id [$carrier->id] updated 'need_range'");
                }
            }
        }
    }
}