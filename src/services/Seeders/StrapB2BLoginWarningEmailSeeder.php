<?php

namespace services\Seeders;

use Seeder;
use Email;
use DB;

class StrapB2BLoginWarningEmailSeeder extends Seeder
{
    public function run()
    {
        $sampleRecord = Email::getByCode('ORDER_CONFIRM');
        $code = 'LOGIN_WARNING';
        $record = Email::where('code', $code)->first();
        if ($record) {
            $this->command->line('LOGIN_WARNING Emails table is already seeded.');
        } else {
            $data = [
                'code' => $code,
                'name' => 'Login Warning',
                'sdesc' => 'Invio email warning tentativo di Login per B2B pericolosi',
                'sender' => $sampleRecord->sender,
                'active' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $email_id = DB::table('emails')->insertGetId($data);
            $this->command->info('LOGIN_WARNING Emails table seeded!');

            $lang_translations = [
                'it' => [
                    'shop_id' => 1,
                    'subject' => 'Login rifiutato dal sito [SITE]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => "<p>Si è verificato un tentativo di accesso al sito <strong>[SITE]</strong>; il login è stato rifiutato perchè il cliente fa parte del gruppo <strong>B2B pericolosi</strong>,<br>qui di seguito trova le credenziali usate per accedere al sito <strong>[SITE]</strong><br><br>Username / Indirizzo email: <strong>[CUSTOMER_EMAIL]</strong><br>Ragione sociale: <strong>[CUSTOMER_NAME]</strong></p>"
                ],
                'en' => [
                    'shop_id' => 1,
                    'subject' => 'Login refused on website [SITE]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => "<p>An unwanted login has been refused on <strong>[SITE]</strong> website<br><br>

Username / Email address: <strong>[CUSTOMER_EMAIL]</strong><br>
Company: <strong>[CUSTOMER_NAME]</strong></p>"
                ],
                'es' => [
                    'shop_id' => 1,
                    'subject' => 'Login refused on website [SITE]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => "<p>An unwanted login has been refused on <strong>[SITE]</strong> website<br><br>

Username / Email address: <strong>[CUSTOMER_EMAIL]</strong><br>
Company: <strong>[CUSTOMER_NAME]</strong></p>"
                ],
                'fr' => [
                    'shop_id' => 1,
                    'subject' => 'Login refused on website [SITE]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => "<p>An unwanted login has been refused on <strong>[SITE]</strong> website<br><br>

Username / Email address: <strong>[CUSTOMER_EMAIL]</strong><br>
Company: <strong>[CUSTOMER_NAME]</strong></p>"
                ],
                'de' => [
                    'shop_id' => 1,
                    'subject' => 'Login refused on website [SITE]',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => "<p>An unwanted login has been refused on <strong>[SITE]</strong> website<br><br>

Username / Email address: <strong>[CUSTOMER_EMAIL]</strong><br>
Company: <strong>[CUSTOMER_NAME]</strong></p>"
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['email_id'] = $email_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('emails_lang')->insert($lang_data);
                $this->command->info("LOGIN_WARNING Emails Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }
}