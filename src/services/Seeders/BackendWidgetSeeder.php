<?php

namespace services\Seeders;

use Seeder;
use DB;

class BackendWidgetSeeder extends Seeder
{
    public function run()
    {
        $record = DB::table('widgets')->find(34);
        $controller = 'OrderStatesController@getIndex';
        $disabled = 0;
        DB::table('widgets')->where('id', $record->id)->update(compact('controller', 'disabled'));

        $record = DB::table('widgets')->find(3);
        $params = [
            'parent_id' => '5',
            'name' => 'Servizi interni/esterni',
            'controller' => '',
            'namedRoute' => 'settings.features',
            'active' => '1',
            'isNav' => '0',
            'disabled' => '0',
            'exact' => '0',
            'position' => '12',
        ];
        DB::table('widgets')->where('id', $record->id)->update($params);
        $this->command->info('Widgets settings.features seeder done!');


        $params = [
            'id' => '87',
            'parent_id' => '28',
            'name' => 'Gift Card',
            'controller' => 'GiftCardController@getIndex',
            'namedRoute' => null,
            'active' => '1',
            'isNav' => '0',
            'disabled' => '0',
            'exact' => '0',
            'position' => '50',
        ];
        $record = DB::table('widgets')->find($params['id']);
        if ($record) {
            DB::table('widgets')->where('id', $record->id)->update($params);
        } else {
            DB::table('widgets')->insert($params);
        }
        $this->command->info('Widgets GiftCardController seeder done!');


        // permissions
        DB::table('groups')->where('id', 1)->update([
            'permissions' => '{"admin":1, "export": 1, "feats": 1}',
        ]);
        $this->command->info('Backend permission seeder done!');
    }
}