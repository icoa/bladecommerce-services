<?php

namespace services\Seeders;

use Seeder;
use Lexicon;
use DB;

class StoreLocatorLexiconSeeder extends Seeder
{
    public function run()
    {
        $code = 'btn_storelocator';
        $record = Lexicon::where('code', $code)->first();
        if ($record) {
            $this->command->line("$code lexicon key is already seeded.");
        } else {
            $data = [
                'code' => $code,
                'ldesc' => 'Etichetta pulsante store-locator',
                'is_shortcode' => '0',
                'is_deep' => '0',
                'is_javascript' => '0',
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info("$code Lexicon key seeded!");

            $lang_translations = [
                'it' => [
                    'name' => 'Our stores'
                ],
                'en' => [
                    'name' => 'Our stores'
                ],
                'es' => [
                    'name' => 'Our stores'
                ],
                'fr' => [
                    'name' => 'Our stores'
                ],
                'de' => [
                    'name' => 'Our stores'
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang $code({$lang_data['lang_id']}) key seeded!");
            }
        }
    }
}