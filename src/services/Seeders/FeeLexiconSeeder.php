<?php

namespace services\Seeders;

use Seeder;
use Lexicon;
use DB;

class FeeLexiconSeeder extends Seeder
{
    public function run()
    {
        $record = Lexicon::where('code', 'msg_receipt_footer')->first();
        if ($record) {
            $this->command->line('FeeLexicon table is already seeded.');
        } else {
            $data = [
                'code' => 'msg_receipt_footer',
                'ldesc' => 'Messaggio visualizzato nel footer della ricevuta fiscale',
                'is_shortcode' => '0',
                'is_deep' => '0',
                'is_javascript' => '0',
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info('FeeLexicon table seeded!');

            $lang_translations = [
                'it' => [
                    'name' => "Questo documento non è valido come fattura e non può essere usato per detrarre l'IVA.<br>Operazione non soggetta all'obbligo di emissione fattura (ai sensi dell'art. 22, c.1, n. 1 D PR 633/72) né all'obbligo di certificazione fiscale (art. 2, lett. 00), DPR 696/96."
                ],
                'en' => [
                    'name' => "This document is not valid as an invoice and can not be used to deduct VAT. <br> Transaction not subject to the obligation to issue an invoice (pursuant to Article 22, paragraph 1, No. 1 D PR 633 / 72) nor to the obligation of fiscal certification (Article 2, letter 00), Presidential Decree 696/96."
                ],
                'es' => [
                    'name' => "Este documento no es válido como una factura y no puede utilizarse para deducir el IVA. <br> La transacción no está sujeta a la obligación de emitir una factura (de conformidad con el Artículo 22, párrafo 1, Nº 1, DPR 633 / 72) ni a la obligación de certificación fiscal (Artículo 2, letra 00), Decreto Presidencial 696/96."
                ],
                'fr' => [
                    'name' => "Ce document n'est pas valable en tant que facture et ne peut pas être utilisé pour déduire la TVA. <br> Opération non soumise à l'obligation d'émettre une facture (conformément à l'article 22, paragraphe 1, n ° 1 D PR 633 / 72) ni à l'obligation de certification fiscale (article 2, lettre 00), décret présidentiel 696/96.",
                ],
                'de' => [
                    'name' => 'Dieses Dokument gilt nicht als Rechnung und kann nicht zum Vorsteuerabzug verwendet werden. <br> Transaktion, für die keine Rechnungspflicht besteht (gemäß Artikel 22 Absatz 1 Nr. 1 D PR 633 / 72) noch die Verpflichtung zur Steuerbescheinigung (Artikel 2, Buchstabe 00), Präsidialerlass 696/96.'
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }

        //change the 'invoice' to 'ricevuta fiscale'
        $record = Lexicon::where('code', 'invoice')->first();
        if ($record) {
            $this->command->line("FeeLexicon invoice table is already seeded.");
        } else {
            $lang_translations = [
                'it' => [
                    'name' => "Ricevuta fiscale"
                ],
                'en' => [
                    'name' => "Receipt"
                ],
                'es' => [
                    'name' => "Recibo de impuestos"
                ],
                'fr' => [
                    'name' => "Reçu d'impôt",
                ],
                'de' => [
                    'name' => "Steuerbeleg",
                ],
            ];
            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $record->id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->where(['lang_id' => $lang_id, 'lexicon_id' => $record->id])->update($lang_data);
                $this->command->info("Lexicons Lang '{$lang_data['name']}' ({$lang_data['lang_id']}) table seeded!");
            }
        }

        //change the 'msg_note_invoice' to 'ricevuta fiscale'
        $record = Lexicon::where('code', 'msg_note_invoice')->first();
        if ($record) {
            $this->command->line("FeeLexicon msg_note_invoice table is already seeded.");
        } else {
            $lang_translations = [
                'it' => [
                    'name' => "<strong>Ricorda:</strong> puoi scaricare la ricevuta fiscale di un ordine solo quando lo status dell'ordine è <b>CHIUSO</b>."
                ],
                'en' => [
                    'name' => "<strong>Remember:</strong> you can download the order receipt when the status of the order is <b>CLOSED</b>."
                ],
                'es' => [
                    'name' => "<strong>Recuerde:</strong> puede descargar un recibo de pedido solo cuando el estado del pedido sea <b>CERRADO</b>."
                ],
                'fr' => [
                    'name' => "<strong>N'oubliez pas:</strong> vous ne pouvez télécharger un reçu de commande que lorsque le statut de la commande est <b>FERME</b>.",
                ],
                'de' => [
                    'name' => "<strong>Erinnern Sie sich:</strong> Sie können eine Bestellbestätigung nur herunterladen, wenn der Bestellstatus <b>GESCHLOSSEN</b> lautet.",
                ],
            ];
            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $record->id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->where(['lang_id' => $lang_id, 'lexicon_id' => $record->id])->update($lang_data);
                $this->command->info("Lexicons Lang '{$lang_data['name']}' ({$lang_data['lang_id']}) table seeded!");
            }
        }

        //change the 'lbl_invoice_available' to 'ricevuta fiscale'
        $record = Lexicon::where('code', 'lbl_invoice_available')->first();
        if ($record) {
            $this->command->line("FeeLexicon lbl_invoice_available table is already seeded.");
        } else {
            $lang_translations = [
                'it' => [
                    'name' => "Ricevuta fiscale disponibile"
                ],
                'en' => [
                    'name' => "Receipt available"
                ],
                'es' => [
                    'name' => "Recibo de impuestos disponible"
                ],
                'fr' => [
                    'name' => "Reçu d'impôt disponible",
                ],
                'de' => [
                    'name' => "Steuerbeleg verfügbar",
                ],
            ];
            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $record->id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->where(['lang_id' => $lang_id, 'lexicon_id' => $record->id])->update($lang_data);
                $this->command->info("Lexicons Lang '{$lang_data['name']}' ({$lang_data['lang_id']}) table seeded!");
            }
        }


        $code = 'lbl_invoice_required';
        $record = Lexicon::where('code', $code)->first();
        if ($record) {
            $this->command->line("FeeLexicon $code table is already seeded.");
        } else {
            $data = [
                'code' => $code,
                'ldesc' => 'Messaggio visualizzato nel Frontend per richiedere la fattura',
                'is_shortcode' => '0',
                'is_deep' => '0',
                'is_javascript' => '0',
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info("FeeLexicon $code table seeded!");

            $lang_translations = [
                'it' => [
                    'name' => "Ho bisogno della fattura fiscale"
                ],
                'en' => [
                    'name' => "I need a tax invoice"
                ],
                'es' => [
                    'name' => "Necesito una factura de impuestos"
                ],
                'fr' => [
                    'name' => "J'ai besoin d'une facture fiscale",
                ],
                'de' => [
                    'name' => 'Ich brauche eine Steuerrechnung'
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang $code ({$lang_data['lang_id']}) table seeded!");
            }
        }


        $code = 'lbl_invoice_asked';
        $record = Lexicon::where('code', $code)->first();
        if ($record) {
            $this->command->line("FeeLexicon $code table is already seeded.");
        } else {
            $data = [
                'code' => $code,
                'ldesc' => 'Messaggio visualizzato nel Frontend quanto è stata richiesta fattura',
                'is_shortcode' => '0',
                'is_deep' => '0',
                'is_javascript' => '0',
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info("FeeLexicon $code table seeded!");

            $lang_translations = [
                'it' => [
                    'name' => "Richiesta fattura fiscale"
                ],
                'en' => [
                    'name' => "Tax invoice requsted"
                ],
                'es' => [
                    'name' => "Factura de impuestos requerida"
                ],
                'fr' => [
                    'name' => "Facture fiscale requise",
                ],
                'de' => [
                    'name' => 'Steuerrechnung erforderlich'
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang $code ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }
}