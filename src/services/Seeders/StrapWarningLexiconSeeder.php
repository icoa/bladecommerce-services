<?php

namespace services\Seeders;

use Seeder;
use Lexicon;
use DB;

class StrapWarningLexiconSeeder extends Seeder
{
    public function run()
    {
        $code = 'msg_b2b_login_warning';
        $record = Lexicon::where('code', $code)->first();
        if ($record) {
            $this->command->line("$code lexicon key is already seeded.");
        } else {
            $data = [
                'code' => $code,
                'ldesc' => 'Messaggio di errore login Utente B2B pericoloso',
                'is_shortcode' => '0',
                'is_deep' => '0',
                'is_javascript' => '0',
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info("$code Lexicon key seeded!");

            $lang_translations = [
                'it' => [
                    'name' => 'Gentile cliente, il suo account non è stato attualmente abilitato ad effettuare l’acquisto. Pertanto, la invitiamo a contattare il servizio clienti di Morellato al numero  049/9323779 oppure 049/9323778, o all’indirizzo e-mail  infob2b.straps@morellato.com'
                ],
                'en' => [
                    'name' => 'Dear customers, at the moment your account is not authorized to effect the purchase. We recommend you to contact customer service of Morellato for assistance to the number 049/9323779 oppure 049/9323778 or to the e-mail address infob2b.straps@morellato.com'
                ],
                'es' => [
                    'name' => 'Dear customers, at the moment your account is not authorized to effect the purchase. We recommend you to contact customer service of Morellato for assistance to the number 049/9323779 oppure 049/9323778 or to the e-mail address infob2b.straps@morellato.com'
                ],
                'fr' => [
                    'name' => 'Dear customers, at the moment your account is not authorized to effect the purchase. We recommend you to contact customer service of Morellato for assistance to the number 049/9323779 oppure 049/9323778 or to the e-mail address infob2b.straps@morellato.com'
                ],
                'de' => [
                    'name' => 'Dear customers, at the moment your account is not authorized to effect the purchase. We recommend you to contact customer service of Morellato for assistance to the number 049/9323779 oppure 049/9323778 or to the e-mail address infob2b.straps@morellato.com'
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang $code({$lang_data['lang_id']}) key seeded!");
            }
        }
    }
}