<?php

namespace services\Seeders;

use Seeder;
use Lexicon;
use DB;

class ElasticSearchLexiconSeeder extends Seeder
{
    public function run()
    {
        $this->lex('lbl_es_not_found', [
            'it' => [
                'name' => "Purtroppo non siamo riusciti a trovare alcun prodotto..."
            ],
            'en' => [
                'name' => "Unfortunately we could not find any product..."
            ],
            'es' => [
                'name' => "Lamentablemente no pudimos encontrar ningún producto..."
            ],
            'fr' => [
                'name' => "Malheureusement, nous n'avons trouvé aucun produit...",
            ],
            'de' => [
                'name' => 'Leider konnten wir kein Produkt finden...'
            ],
        ]);

        $this->lex('lbl_es_suggest', [
            'it' => [
                'name' => "Ti suggeriamo di..."
            ],
            'en' => [
                'name' => "We suggest to..."
            ],
            'es' => [
                'name' => "Sugerimos..."
            ],
            'fr' => [
                'name' => "Nous suggérons...",
            ],
            'de' => [
                'name' => 'Wir schlagen vor...'
            ],
        ]);

        $this->lex('lbl_es_back_suggest', [
            'it' => [
                'name' => "Torna indietro e mostra i suggerimenti"
            ],
            'en' => [
                'name' => "Back to suggestions"
            ],
            'es' => [
                'name' => "Regresa y muestra las sugerencias"
            ],
            'fr' => [
                'name' => "Retournez et montrez les suggestions",
            ],
            'de' => [
                'name' => 'Gehe zurück und zeige die Vorschläge'
            ],
        ]);

        $this->lex('lbl_es_show_catalog', [
            'it' => [
                'name' => "Sfoglia tutto il catalogo"
            ],
            'en' => [
                'name' => "Browse the entire catalog"
            ],
            'es' => [
                'name' => "Navegar por todo el catálogo"
            ],
            'fr' => [
                'name' => "Parcourir le catalogue entier",
            ],
            'de' => [
                'name' => 'Durchsuchen Sie den gesamten Katalog'
            ],
        ]);

        $this->lex('lbl_es_show_sitemap', [
            'it' => [
                'name' => "Mostra la mappa del sito"
            ],
            'en' => [
                'name' => "Show the website's map"
            ],
            'es' => [
                'name' => "Mostrar el mapa del sitio"
            ],
            'fr' => [
                'name' => "Afficher le plan du site",
            ],
            'de' => [
                'name' => 'Zeigen Sie die Sitemap an'
            ],
        ]);

        $this->lex('lbl_es_contacts', [
            'it' => [
                'name' => "Contatta il nostro Staff"
            ],
            'en' => [
                'name' => "Contact our Staff"
            ],
            'es' => [
                'name' => "Contacta a nuestro personal"
            ],
            'fr' => [
                'name' => "Contactez notre personnel",
            ],
            'de' => [
                'name' => 'Kontaktieren Sie unsere Mitarbeiter'
            ],
        ]);
    }

    private function lex($code, $lang_translations)
    {
        $record = Lexicon::where('code', $code)->first();
        if ($record) {
            $this->command->line($code . ' lexicon code is already seeded.');
        } else {
            $data = [
                'code' => $code,
                'ldesc' => '',
                'is_shortcode' => '0',
                'is_deep' => '0',
                'is_javascript' => '0',
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info($code . ' lexicon code seeded!');

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }
}