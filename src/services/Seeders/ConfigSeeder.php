<?php

namespace services\Seeders;

use Seeder;
use DB;

class ConfigSeeder extends Seeder
{
    public function run()
    {
        $this->command->line('Running ConfigSeeder');

        $data = [
            '404_METATITLE' => [
                'it' => 'Pagina non trovata',
                'en' => 'Page Not Found',
                'es' => 'Página no encontrada',
                'fr' => 'Page non trouvée',
                'de' => 'Seite nicht gefunden',
            ],
            '404_METADESC' => [
                'it' => 'Not Found - Pagina non trovata',
                'en' => 'Page Not Found',
                'es' => 'Not Found - Página no encontrada',
                'fr' => 'Not Found - Page non trouvée',
                'de' => 'Not Found - Seite nicht gefunden',
            ],
        ];

        foreach ($data as $key => $item) {
            $record = DB::table('config')->where('cfg_key', $key)->first();
            if (is_null($record)) {
                DB::table('config')->insert(['cfg_key' => $key, 'cfg_group' => 'general']);

                foreach ($item as $lang => $text) {
                    DB::table('config_lang')->insert(['cfg_key' => $key, 'lang_id' => $lang, 'prod_value' => $text]);
                    $this->command->info("Seeded $key.$lang");
                }
            } else {
                $this->command->comment("Key $key already seeded");
            }
        }
    }
}