<?php

namespace services\Seeders;

use Seeder;
use Email;
use DB;

class GiftCardEmailSeeder extends Seeder
{
    public function run()
    {
        $sampleRecord = Email::getByCode('ORDER_CONFIRM');
        $code = 'GIFT_CARD';
        $record = Email::where('code', $code)->first();
        if ($record) {
            $this->command->line('GIFT_CARD Emails table is already seeded.');
        } else {
            $data = [
                'code' => $code,
                'name' => 'Gift Card',
                'sdesc' => 'Messaggio inviato all\'acquirente di una Gift Card',
                'sender' => $sampleRecord->sender,
                'active' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $email_id = DB::table('emails')->insertGetId($data);
            $this->command->info('GIFT_CARD Emails table seeded!');

            $lang_translations = [
                'it' => [
                    'shop_id' => 1,
                    'subject' => 'La tua Gift Card',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => '<p>Ciao [NAME],<br />
grazie per aver scelto una Gift Card [SITE]. Ti confermiamo che è attiva a partire da adesso per un anno: potrai utilizzarla per acquistare qualunque prodotto presente sul sito <a href="[URL]">[URL]</a>.</p>

<p>Ecco i dati della tua Gift Card:<br />
[CODES]</p>

<p>In allegato un pdf con tutte le informazioni su come utilizzare la tua Gift Card.</p>

<p>Se hai bisogno di aiuto, <a href="mailto:customercare@morellato.com">contattaci qui</a>, siamo a tua disposizione.</p>

<p>Trovi in calce alla presente mail l\'Informativa sulla privacy e le condizioni di utilizzo della tua Gift Card, che ti invitiamo a leggere attentamente.</p>

<p>Buoni acquisti, [SITE]</p>'
                ],
                'en' => [
                    'shop_id' => 1,
                    'subject' => 'Your Gift Card',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => '<p>Hello [NAME],<br />
thank You for choosing a [SITE] Gift Card. We confirm You that it is active from now to one year: You can use it to purchase any item on our website <a href="[URL]">[URL]</a>.</p>

<p>Here are the details about your Gift Card:<br />
[CODES]</p>

<p>You will find an attachment with all the information on how to use your Gift Card.</p>

<p>If you need help, <a href="mailto:customercare@morellato.com">contact us here</a>, we are at your disposal.</p>

<p>At the bottom of this e-mail you will find the privacy policy and terms of use of your Gift Card, which we invite you to read carefully.</p>

<p>Good purchases, [SITE]</p>'
                ],
                'es' => [
                    'shop_id' => 1,
                    'subject' => 'Su tarjeta de regalo',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => '<p>Hola [NAME],<br />
Gracias por elegir una tarjeta de regalo [SITE]. Le confirmamos que está activo desde ahora por un año: puede usarlo para comprar cualquier producto en el sitio <a href="[URL]">[URL]</a>.</p>

<p>Aquí están los detalles de su tarjeta de regalo:<br />
[CODES]</p>

<p>Adjunto un pdf con toda la información sobre cómo usar su tarjeta de regalo.</p>

<p>Si necesita ayuda, <a href="mailto:customercare@morellato.com">contáctenos aquí</a>, estamos a su disposición.</p>

<p>En la parte inferior de este correo electrónico, encontrará la política de privacidad y los términos de uso de su Tarjeta de regalo, que le invitamos a leer detenidamente.</p>

<p>Buenas compras, [SITE]</p>'
                ],
                'fr' => [
                    'shop_id' => 1,
                    'subject' => 'Votre carte-cadeau',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => '<p>Bonjour [NAME],<br />
Merci d\'avoir choisi une carte-cadeau [SITE]. Nous vous confirmons qu\'il est actif depuis maintenant un an: vous pouvez l\'utiliser pour acheter n\'importe quel produit sur le site <a href="[URL]">[URL]</a>.</p>

<p>Voici les détails de votre carte-cadeau:<br />
[CODES]</p>

<p>Joindre un pdf avec toutes les informations sur la façon d\'utiliser votre carte-cadeau.</p>

<p>Si vous avez besoin d\'aide, <a href="mailto:customercare@morellato.com">contactez-nous ici</a>, nous sommes à votre disposition.</p>

<p>Au bas de cet e-mail, vous trouverez la politique de confidentialité et les conditions d\'utilisation de votre carte-cadeau, que nous vous invitons à lire attentivement.</p>

<p>Bons achats, [SITE]</p>'
                ],
                'de' => [
                    'shop_id' => 1,
                    'subject' => 'Ihre Geschenkkarte',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => '<p>Hallo [NAME],<br />
Danke, dass Sie sich für eine [SITE] Geschenkkarte entschieden haben. Wir bestätigen Ihnen, dass es ab sofort für ein Jahr aktiv ist: Sie können damit jedes Produkt auf der Website kaufen <a href="[URL]">[URL]</a>.</p>

<p>Hier sind die Details Ihrer Geschenkkarte:<br />
[CODES]</p>

<p>Anbei ein PDF mit allen Informationen zur Verwendung Ihrer Geschenkkarte.</p>

<p>Wenn Sie Hilfe benötigen, <a href="mailto:customercare@morellato.com">kontaktieren Sie uns hier</a>, wir stehen Ihnen zur Verfügung.</p>

<p>Am Ende dieser E-Mail finden Sie die Datenschutzerklärung und die Nutzungsbedingungen Ihrer Geschenkkarte, die wir Ihnen zur sorgfältigen Lektüre empfehlen.</p>

<p>Gute Einkäufe, [SITE]</p>'
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['email_id'] = $email_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('emails_lang')->insert($lang_data);
                $this->command->info("GIFT_CARD Emails Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }





        $code = 'GIFT_CARD_RECIPIENT';
        $record = Email::where('code', $code)->first();
        if ($record) {
            $this->command->line('GIFT_CARD_RECIPIENT Emails table is already seeded.');
        } else {
            $data = [
                'code' => $code,
                'name' => 'Gift Card Recipient',
                'sdesc' => 'Messaggio inviato al destinatario di una Gift Card acquistata come regalo',
                'sender' => $sampleRecord->sender,
                'active' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $email_id = DB::table('emails')->insertGetId($data);
            $this->command->info('GIFT_CARD_RECIPIENT Emails table seeded!');

            $lang_translations = [
                'it' => [
                    'shop_id' => 1,
                    'subject' => 'Hai un nuovo regalo',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => 'Ciao [RECIPIENT_NAME],<br />
[NAME] ti ha regalato una Gift Card di [SITE]!<br />
<br />
<em><strong>[MESSAGE]</strong></em><br />
 
<p>Ecco i dati della tua Gift Card:<br />
[CODES]<br />
 </p>

<p>In allegato un pdf con tutte le informazioni su come utilizzare la tua Gift Card.</p>

<p>Se hai bisogno di aiuto, <a href="mailto:customercare@morellato.com">contattaci qui</a>, siamo a tua disposizione.</p>

<p>Trovi in calce alla presente mail l\'Informativa sulla privacy e le condizioni di utilizzo della tua Gift Card, che ti invitiamo a leggere attentamente.</p>

<p>Buoni acquisti, [SITE]</p>'
                ],
                'en' => [
                    'shop_id' => 1,
                    'subject' => 'You have a new gift',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => 'Hello [RECIPIENT_NAME],<br />
[NAME] sent You a [SITE] Gift Card!<br />
<br />
<em><strong>[MESSAGE]</strong></em><br />
 
<p>Here are the details about your Gift Card:<br />
[CODES]<br />
 </p>

<p>You will find an attachment with all the information on how to use your Gift Card.</p>

<p>If you need help, <a href="mailto:customercare@morellato.com">contact us here</a>, we are at your disposal.</p>

<p>At the bottom of this e-mail you will find the privacy policy and terms of use of your Gift Card, which we invite you to read carefully.</p>

<p>Good purchases, [SITE]</p>'
                ],
                'es' => [
                    'shop_id' => 1,
                    'subject' => 'Tienes un nuevo regalo',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => 'Hola [RECIPIENT_NAME],<br />
¡[NAME] te dio una tarjeta de regalo [SITE]!<br />
<br />
<em><strong>[MESSAGE]</strong></em>
<p>Aquí están los detalles de su tarjeta de regalo:<br />
[CODES]</p>

<p>Adjunto un pdf con toda la información sobre cómo usar su tarjeta de regalo.</p>

<p>Si necesita ayuda, <a href="mailto:customercare@morellato.com">contáctenos aquí</a>, estamos a su disposición.</p>

<p>En la parte inferior de este correo electrónico, encontrará la política de privacidad y los términos de uso de su Tarjeta de regalo, que le invitamos a leer detenidamente.</p>

<p>Buenas compras, [SITE]</p>'
                ],
                'fr' => [
                    'shop_id' => 1,
                    'subject' => 'Vous avez un nouveau cadeau',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => 'Salut [RECIPIENT_NAME],<br />
[NAME] vous a donné une carte-cadeau [SITE]!<br />
<br />
<em><strong>[MESSAGE]</strong></em>
<p>Voici les détails de votre carte-cadeau:<br />
[CODES]</p>

<p>Joindre un pdf avec toutes les informations sur la façon d\'utiliser votre carte-cadeau.</p>

<p>Si vous avez besoin d\'aide, <a href="mailto:customercare@morellato.com">contactez-nous ici</a>, nous sommes à votre disposition.</p>

<p>Au bas de cet e-mail, vous trouverez la politique de confidentialité et les conditions d\'utilisation de votre carte-cadeau, que nous vous invitons à lire attentivement.</p>

<p>Bons achats, [SITE]</p>'
                ],
                'de' => [
                    'shop_id' => 1,
                    'subject' => 'Du hast ein neues Geschenk',
                    'sender_name' => $sampleRecord->sender_name,
                    'body' => 'Hallo [RECIPIENT_NAME],<br />
[NAME] hat dir eine [SITE] Geschenkkarte gegeben!<br />
<br />
<em><strong>[MESSAGE]</strong></em>
<p>Hier sind die Details Ihrer Geschenkkarte:<br />
[CODES]</p>

<p>Anbei ein PDF mit allen Informationen zur Verwendung Ihrer Geschenkkarte.</p>

<p>Wenn Sie Hilfe benötigen, <a href="mailto:customercare@morellato.com">kontaktieren Sie uns hier</a>, wir stehen Ihnen zur Verfügung.</p>

<p>Am Ende dieser E-Mail finden Sie die Datenschutzerklärung und die Nutzungsbedingungen Ihrer Geschenkkarte, die wir Ihnen zur sorgfältigen Lektüre empfehlen.</p>

<p>Gute Einkäufe, [SITE]</p>'
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['email_id'] = $email_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('emails_lang')->insert($lang_data);
                $this->command->info("GIFT_CARD_RECIPIENT Emails Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }
}