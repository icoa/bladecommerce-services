<?php

namespace services\Seeders;

use Seeder;
use Lexicon;
use DB;

class CroLexiconSeeder extends Seeder
{
    public function run()
    {
        $this->promo_code();
        $this->voucher();
        $this->fidelity_card();
    }


    private function promo_code()
    {
        $code = 'lbl_promo_code';
        $record = Lexicon::where('code', $code)->first();
        if ($record) {
            $this->command->line($code . ' key is already seeded.');
        } else {
            $data = [
                'code' => $code,
                'ldesc' => '',
                'is_shortcode' => '0',
                'is_deep' => '0',
                'is_javascript' => '0',
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info('Lexicons table seeded!');

            $lang_translations = [
                'it' => [
                    'name' => 'Codice promozionale'
                ],
                'en' => [
                    'name' => 'Promo Code'
                ],
                'es' => [
                    'name' => 'Codigo promocional'
                ],
                'fr' => [
                    'name' => 'Code promotionnel'
                ],
                'de' => [
                    'name' => 'Aktionscode'
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }


    private function voucher()
    {
        $code = 'lbl_voucher_code';
        $record = Lexicon::where('code', $code)->first();
        if ($record) {
            $this->command->line($code . ' key is already seeded.');
        } else {
            $data = [
                'code' => $code,
                'ldesc' => '',
                'is_shortcode' => '0',
                'is_deep' => '0',
                'is_javascript' => '0',
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info('Lexicons table seeded!');

            $lang_translations = [
                'it' => [
                    'name' => 'Buoni acquisto'
                ],
                'en' => [
                    'name' => 'Insert you Voucher here'
                ],
                'es' => [
                    'name' => 'Compra de vales'
                ],
                'fr' => [
                    'name' => 'Bons d\'achat'
                ],
                'de' => [
                    'name' => 'Gutscheine kaufen'
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }


    private function fidelity_card()
    {
        $code = 'lbl_fidelity_cart';
        $record = Lexicon::where('code', $code)->first();
        if ($record) {
            $this->command->line($code . ' key is already seeded.');
        } else {
            $data = [
                'code' => $code,
                'ldesc' => '',
                'is_shortcode' => '0',
                'is_deep' => '0',
                'is_javascript' => '0',
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info('Lexicons table seeded!');

            $lang_translations = [
                'it' => [
                    'name' => 'Bluespirit Card'
                ],
                'en' => [
                    'name' => 'Bluespirit Card'
                ],
                'es' => [
                    'name' => 'Bluespirit Card'
                ],
                'fr' => [
                    'name' => 'Bluespirit Card'
                ],
                'de' => [
                    'name' => 'Bluespirit Card'
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }
}