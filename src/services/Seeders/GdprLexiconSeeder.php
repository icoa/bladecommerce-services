<?php

namespace services\Seeders;

use Seeder;
use Lexicon;
use DB;

class GdprLexiconSeeder extends Seeder
{
    public function run()
    {
        $this->incipit();
        $this->profile();
        $this->marketing();
        $this->fidelity();
    }

    private function incipit()
    {
        $code = 'lbl_gdpr_incipit';
        $record = Lexicon::where('code', $code)->first();

        $data = [
            'code' => $code,
            'ldesc' => 'Messaggio visualizzato in vari form di registrazione, per incipit della GDPR e link',
            'is_shortcode' => '0',
            'is_deep' => '0',
            'is_javascript' => '0',
            'created_by' => 1,
            'updated_by' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        $lang_translations = [
            'it' => [
                'name' => 'Ho letto <a href="javascript:;" rel="nofollow" data-toggle="modal" data-target="#t_and_c_m">l\'informativa</a> sul trattamento dei miei dati personali e:'
            ],
            'en' => [
                'name' => 'I have read <a href="javascript:;" rel="nofollow" data-toggle="modal" data-target="#t_and_c_m">the information</a> on the processing of my personal data and:'
            ],
            'es' => [
                'name' => 'He leído <a href="javascript:;" rel="nofollow" data-toggle="modal" data-target="#t_and_c_m">la información</a> sobre el procesamiento de mis datos personales y:'
            ],
            'fr' => [
                'name' => 'J\'ai lu <a href="javascript:;" rel="nofollow" data-toggle="modal" data-target="#t_and_c_m">les informations</a> sur le traitement de mes données personnelles et:'
            ],
            'de' => [
                'name' => 'Ich habe <a href="javascript:;" rel="nofollow" data-toggle="modal" data-target="#t_and_c_m">die Informationen</a> über die Verarbeitung meiner persönlichen Daten gelesen und:'
            ],
        ];

        if ($record) {
            //update
            $this->command->comment($code . ' lexicon key is already seeded.');
        } else {
            //create
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info($code . ' lexicon key is seeded!');

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }


    private function profile()
    {
        $code = 'lbl_gdpr_profile';
        $record = Lexicon::where('code', $code)->first();

        $data = [
            'code' => $code,
            'ldesc' => 'Messaggio visualizzato in vari form di registrazione, per Profilazione',
            'is_shortcode' => '0',
            'is_deep' => '0',
            'is_javascript' => '0',
            'created_by' => 1,
            'updated_by' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        $lang_translations = [
            'it' => [
                'name' => 'all\'uso dei miei dati personali per migliorare la mia esperienza di navigazione e ricevere proposte in linea con i miei interessi attraverso l’analisi dei miei precedenti acquisti.'
            ],
            'en' => [
                'name' => 'that my personal information may be used for improving my experience navigating the site and receiving offers in line with my interests through an analysis of my previous purchases.'
            ],
            'es' => [
                'name' => 'el uso de mis datos personales para mejorar mi experiencia de compra y recibir propuestas de acuerdo con mis intereses mediante el análisis de mis anteriores compras.'
            ],
            'fr' => [
                'name' => 'que mes données personnelles soient utilisées pour améliorer mon expérience de navigation et recevoir des propositions ciblées en fonction de mes centres d\'intérêt, à travers l\'analyse de mes achats précédents.'
            ],
            'de' => [
                'name' => 'auf die Verarbeitung meiner persönlichen Daten für Profiling-Aktivitäten, die darauf abzielen, mir die von mir ausgewählte Nummer von Kommunikationen, Informationen und Diensten anzubieten, die meinen Präferenzen und meinen Konsumentscheidungen entsprechen.'
            ],
        ];

        if ($record) {
            //update
            $this->command->comment($code . ' lexicon key is already seeded.');
        } else {
            //create
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info($code . ' lexicon key is seeded!');

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }


    private function marketing()
    {
        $code = 'lbl_gdpr_marketing';
        $record = Lexicon::where('code', $code)->first();

        $data = [
            'code' => $code,
            'ldesc' => 'Messaggio visualizzato in vari form di registrazione, per Comunicazione',
            'is_shortcode' => '0',
            'is_deep' => '0',
            'is_javascript' => '0',
            'created_by' => 1,
            'updated_by' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        $lang_translations = [
            'it' => [
                'name' => 'all\'uso dei miei dati personali per essere aggiornato sui nuovi arrivi, prodotti in esclusiva e per le finalità di marketing correlate ai servizi offerti.'
            ],
            'en' => [
                'name' => 'that my personal information may be used for updating me on new arrivals, exclusive products and for marketing purposes related to the services offered.'
            ],
            'es' => [
                'name' => 'el uso de mis datos personales para recibir información relativa a novedades, artículos en exclusiva y usos con finalidad de marketing relacionados con los servicios ofrecidos.'
            ],
            'fr' => [
                'name' => 'que mes données personnelles soient utilisées pour être informé(e) sur les nouveautés, les articles exclusifs et à des fins de marketing liées aux services offerts.'
            ],
            'de' => [
                'name' => 'zur Verarbeitung meiner persönlichen Daten für den Versand von Aktionen, Rabatten und Informationsmitteilungen an unsere Produkte und Dienstleistungen.'
            ],
        ];

        if ($record) {
            //update
            $this->command->comment($code . ' lexicon key is already seeded.');
        } else {
            //create
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info($code . ' lexicon key is seeded!');

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }


    private function fidelity()
    {
        $code = 'lbl_gdpr_fidelity';
        $record = Lexicon::where('code', $code)->first();

        $data = [
            'code' => $code,
            'ldesc' => 'Messaggio visualizzato in nel form registrazione Utente per Fidelity Card',
            'is_shortcode' => '0',
            'is_deep' => '0',
            'is_javascript' => '0',
            'created_by' => 1,
            'updated_by' => 1,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];

        $lang_translations = [
            'it' => [
                'name' => 'Confermando la registrazione dichiari di aver letto ed accettato il <a href="/bluespirit-card-regolamento-ufficiale.html" target="_blank">regolamento della Bluespirit Card</a>'
            ],
            'en' => [
                'name' => 'By confirming the registration you declare that you have read and accepted the <a href="/bluespirit-card-regolamento-ufficiale.html" target="_blank">Bluespirit Card regulations</a>'
            ],
            'es' => [
                'name' => 'Al confirmar el registro, declara que ha leído y aceptado <a href="/bluespirit-card-regolamento-ufficiale.html" target="_blank">las regulaciones de la tarjeta Bluespirit</a>'
            ],
            'fr' => [
                'name' => 'En confirmant l\'inscription, vous déclarez avoir lu et accepté <a href="/bluespirit-card-regolamento-ufficiale.html" target="_blank">les règlements de la Carte Bluespirit</a>'
            ],
            'de' => [
                'name' => 'Mit der Bestätigung der Registrierung erklären Sie, dass Sie <a href="/bluespirit-card-regolamento-ufficiale.html" target="_blank">die Bestimmungen der Bluespirit Card gelesen und akzeptiert haben</a>'
            ],
        ];

        if ($record) {
            //update
            $this->command->comment($code . ' lexicon key is already seeded.');
        } else {
            //create
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info($code . ' lexicon key is seeded!');

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }
}