<?php

namespace services\Seeders;

use Seeder;
use Payment;
use PaymentState;
use Email;
use CustomerGroup;
use Country;
use DB;
use Exception;

class PaymentModulesSeeder extends Seeder
{
    public function run()
    {
        $matrix = [
            Payment::TYPE_PAYPAL => Payment::MODULE_PAYPAL,
            Payment::TYPE_CREDITCARD => Payment::MODULE_CREDIT_CARD,
            Payment::TYPE_BANKTRANSFER => Payment::MODULE_BANKWIRE,
            Payment::TYPE_CASH => Payment::MODULE_CASHONDELIVERY,
            Payment::TYPE_SHIPPING_POINT => Payment::MODULE_SHOP_PICK,
            Payment::TYPE_SHOP_ANTICIPATED => Payment::MODULE_SHOP_PREPAID,
            Payment::TYPE_WITHDRAWAL => Payment::MODULE_SHOP_POSTPAID,
            Payment::TYPE_GIFTCARD => Payment::MODULE_GIFTCARD,
            Payment::TYPE_RIBA => Payment::MODULE_RIBA,
        ];
    }
}