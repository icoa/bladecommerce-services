<?php

namespace services\Seeders;

use Seeder;
use Email;
use DB;

class UpdateOrderStateCancelledSeeder extends Seeder
{
    public function run()
    {
        DB::table('order_states')->where('id', \OrderState::STATUS_CANCELED)->update(['send_email' => 0]);
        $this->command->line('Order State Cancelled seeded table is already seeded.');
    }
}