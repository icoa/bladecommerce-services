<?php

namespace services\Seeders;

use Seeder;
use Country;
use Currency;
use DB;

class CurrenciesSeeder extends Seeder
{

    /**
     * @return array
     */
    private function getData()
    {
        return [
            ['Albania', 'Leke', 'ALL', 'Lek'],
            ['America', 'Dollars', 'USD', '$'],
            ['Afghanistan', 'Afghanis', 'AFN', '؋'],
            ['Argentina', 'Pesos', 'ARS', '$'],
            ['Aruba', 'Guilders', 'AWG', 'ƒ'],
            ['Australia', 'Dollars', 'AUD', '$'],
            ['Azerbaijan', 'New Manats', 'AZN', 'ман'],
            ['Bahamas', 'Dollars', 'BSD', '$'],
            ['Barbados', 'Dollars', 'BBD', '$'],
            ['Belarus', 'Rubles', 'BYR', 'p.'],
            ['Belgium', 'Euro', 'EUR', '€'],
            ['Beliz', 'Dollars', 'BZD', 'BZ$'],
            ['Bermuda', 'Dollars', 'BMD', '$'],
            ['Bolivia', 'Bolivianos', 'BOB', '$b'],
            ['Bosnia and Herzegovina', 'Convertible Marka', 'BAM', 'KM'],
            ['Botswana', 'Pula', 'BWP', 'P'],
            ['Bulgaria', 'Leva', 'BGN', 'лв'],
            ['Brazil', 'Reais', 'BRL', 'R$'],
            ['Britain [United Kingdom]', 'Pounds', 'GBP', '£'],
            ['Brunei Darussalam', 'Dollars', 'BND', '$'],
            ['Cambodia', 'Riels', 'KHR', '៛'],
            ['Canada', 'Dollars', 'CAD', '$'],
            ['Cayman Islands', 'Dollars', 'KYD', '$'],
            ['Chile', 'Pesos', 'CLP', '$'],
            ['China', 'Yuan Renminbi', 'CNY', '¥'],
            ['Colombia', 'Pesos', 'COP', '$'],
            ['Costa Rica', 'Colón', 'CRC', '₡'],
            ['Croatia', 'Kuna', 'HRK', 'kn'],
            ['Cuba', 'Pesos', 'CUP', '₱'],
            ['Cyprus', 'Euro', 'EUR', '€'],
            ['Czech Republic', 'Koruny', 'CZK', 'Kč'],
            ['Denmark', 'Kroner', 'DKK', 'kr'],
            ['Dominican Republic', 'Pesos', 'DOP ', 'RD$'],
            ['East Caribbean', 'Dollars', 'XCD', '$'],
            ['Egypt', 'Pounds', 'EGP', '£'],
            ['El Salvador', 'Colones', 'SVC', '$'],
            ['England [United Kingdom]', 'Pounds', 'GBP', '£'],
            ['Euro', 'Euro', 'EUR', '€'],
            ['Falkland Islands', 'Pounds', 'FKP', '£'],
            ['Fiji', 'Dollars', 'FJD', '$'],
            ['France', 'Euro', 'EUR', '€'],
            ['Ghana', 'Cedis', 'GHC', '¢'],
            ['Gibraltar', 'Pounds', 'GIP', '£'],
            ['Greece', 'Euro', 'EUR', '€'],
            ['Guatemala', 'Quetzales', 'GTQ', 'Q'],
            ['Guernsey', 'Pounds', 'GGP', '£'],
            ['Guyana', 'Dollars', 'GYD', '$'],
            ['Holland [Netherlands]', 'Euro', 'EUR', '€'],
            ['Honduras', 'Lempiras', 'HNL', 'L'],
            ['Hong Kong', 'Dollars', 'HKD', '$'],
            ['Hungary', 'Forint', 'HUF', 'Ft'],
            ['Iceland', 'Kronur', 'ISK', 'kr'],
            ['India', 'Rupees', 'INR', 'Rp'],
            ['Indonesia', 'Rupiahs', 'IDR', 'Rp'],
            ['Iran', 'Rials', 'IRR', '﷼'],
            ['Ireland', 'Euro', 'EUR', '€'],
            ['Isle of Man', 'Pounds', 'IMP', '£'],
            ['Israel', 'New Shekels', 'ILS', '₪'],
            ['Italy', 'Euro', 'EUR', '€'],
            ['Jamaica', 'Dollars', 'JMD', 'J$'],
            ['Japan', 'Yen', 'JPY', '¥'],
            ['Jersey', 'Pounds', 'JEP', '£'],
            ['Kazakhstan', 'Tenge', 'KZT', 'лв'],
            ['Korea [North]', 'Won', 'KPW', '₩'],
            ['Korea [South]', 'Won', 'KRW', '₩'],
            ['Kyrgyzstan', 'Soms', 'KGS', 'лв'],
            ['Laos', 'Kips', 'LAK', '₭'],
            ['Latvia', 'Lati', 'LVL', 'Ls'],
            ['Lebanon', 'Pounds', 'LBP', '£'],
            ['Liberia', 'Dollars', 'LRD', '$'],
            ['Liechtenstein', 'Switzerland Francs', 'CHF', 'CHF'],
            ['Lithuania', 'Litai', 'LTL', 'Lt'],
            ['Luxembourg', 'Euro', 'EUR', '€'],
            ['Macedonia', 'Denars', 'MKD', 'ден'],
            ['Malaysia', 'Ringgits', 'MYR', 'RM'],
            ['Malta', 'Euro', 'EUR', '€'],
            ['Mauritius', 'Rupees', 'MUR', '₨'],
            ['Mexico', 'Pesos', 'MXN', '$'],
            ['Mongolia', 'Tugriks', 'MNT', '₮'],
            ['Mozambique', 'Meticais', 'MZN', 'MT'],
            ['Namibia', 'Dollars', 'NAD', '$'],
            ['Nepal', 'Rupees', 'NPR', '₨'],
            ['Netherlands Antilles', 'Guilders', 'ANG', 'ƒ'],
            ['Netherlands', 'Euro', 'EUR', '€'],
            ['New Zealand', 'Dollars', 'NZD', '$'],
            ['Nicaragua', 'Cordobas', 'NIO', 'C$'],
            ['Nigeria', 'Nairas', 'NGN', '₦'],
            ['North Korea', 'Won', 'KPW', '₩'],
            ['Norway', 'Krone', 'NOK', 'kr'],
            ['Oman', 'Rials', 'OMR', '﷼'],
            ['Pakistan', 'Rupees', 'PKR', '₨'],
            ['Panama', 'Balboa', 'PAB', 'B/.'],
            ['Paraguay', 'Guarani', 'PYG', 'Gs'],
            ['Peru', 'Nuevos Soles', 'PEN', 'S/.'],
            ['Philippines', 'Pesos', 'PHP', 'Php'],
            ['Poland', 'Zlotych', 'PLN', 'zł'],
            ['Qatar', 'Rials', 'QAR', '﷼'],
            ['Romania', 'New Lei', 'RON', 'lei'],
            ['Russia', 'Rubles', 'RUB', 'руб'],
            ['Saint Helena', 'Pounds', 'SHP', '£'],
            ['Saudi Arabia', 'Riyals', 'SAR', '﷼'],
            ['Serbia', 'Dinars', 'RSD', 'Дин.'],
            ['Seychelles', 'Rupees', 'SCR', '₨'],
            ['Singapore', 'Dollars', 'SGD', '$'],
            ['Slovenia', 'Euro', 'EUR', '€'],
            ['Solomon Islands', 'Dollars', 'SBD', '$'],
            ['Somalia', 'Shillings', 'SOS', 'S'],
            ['South Africa', 'Rand', 'ZAR', 'R'],
            ['South Korea', 'Won', 'KRW', '₩'],
            ['Spain', 'Euro', 'EUR', '€'],
            ['Sri Lanka', 'Rupees', 'LKR', '₨'],
            ['Sweden', 'Kronor', 'SEK', 'kr'],
            ['Switzerland', 'Francs', 'CHF', 'CHF'],
            ['Suriname', 'Dollars', 'SRD', '$'],
            ['Syria', 'Pounds', 'SYP', '£'],
            ['Taiwan', 'New Dollars', 'TWD', 'NT$'],
            ['Thailand', 'Baht', 'THB', '฿'],
            ['Trinidad and Tobago', 'Dollars', 'TTD', 'TT$'],
            ['Turkey', 'Lira', 'TRY', 'TL'],
            ['Turkey', 'Liras', 'TRL', '£'],
            ['Tuvalu', 'Dollars', 'TVD', '$'],
            ['Ukraine', 'Hryvnia', 'UAH', '₴'],
            ['United Kingdom', 'Pounds', 'GBP', '£'],
            ['United States of America', 'Dollars', 'USD', '$'],
            ['Uruguay', 'Pesos', 'UYU', '$U'],
            ['Uzbekistan', 'Sums', 'UZS', 'лв'],
            ['Vatican City', 'Euro', 'EUR', '€'],
            ['Venezuela', 'Bolivares Fuertes', 'VEF', 'Bs'],
            ['Vietnam', 'Dong', 'VND', '₫'],
            ['Yemen', 'Rials', 'YER', '﷼'],
            ['Zimbabwe', 'Zimbabwe Dollars', 'ZWD', 'Z$'],
        ];
    }

    private function getLookupTable()
    {
        $schema = '| AED || 784 || 2 || [[Dirham degli Emirati Arabi Uniti]] || [[Emirati Arabi Uniti]]
|-
| AFN || 971 || 2 || [[Afghani afghano]] || [[Afghanistan]]
|-
| ALL || 008 || 0 || [[Lek albanese]] || [[Albania]]
|-
| AMD || 051 || 2 || [[Dram armeno]] || [[Armenia]]
|-
| ANG || 532 || 2 || [[Fiorino delle Antille olandesi]] || [[Curaçao]], [[Sint Maarten]]
|-
| AOA || 973 || 2 || [[Kwanza angolano]] || [[Angola]]
|-
| ARS || 032 || 2 || [[Peso argentino]] || [[Argentina]]
|-
| AUD || 036 || 2 || [[Dollaro australiano]] || [[Australia]], [[Territorio antartico australiano]], [[Isola del Natale|Isola Christmas]], [[Isole Cocos e Keeling|Isole Cocos]], [[Isole Heard e McDonald]], [[Kiribati]], [[Nauru]], [[Isola Norfolk]], [[Tuvalu]]
|-
| AWG || 533 || 2 || [[Fiorino arubano]] || [[Aruba]]
|-
| AZN || 944 || 2 || [[Manat azero]] || [[Azerbaigian]]
|-
| BAM || 977 || 2 || [[Marco bosniaco]] || [[Bosnia ed Erzegovina]]
|-
| BBD || 052 || 2 || [[Dollaro di Barbados]] || [[Barbados]]
|-
| BDT || 050 || 2 || [[Taka bengalese]] || [[Bangladesh]]
|-
| BGN || 975 || 2 || [[Lev bulgaro|Nuovo lev bulgaro]] || [[Bulgaria]]
|-
| BHD || 048 || 3 || [[Dinaro del Bahrain]] || [[Bahrein]]
|-
| BIF || 108 || 0 || [[Franco del Burundi]] || [[Burundi]]
|-
| BMD || 060 || 2 || [[Dollaro della Bermuda]] || [[Bermuda]]
|-
| BND || 096 || 2 || [[Dollaro del Brunei]] || [[Brunei]]
|-
| BOB || 068 || 2 || [[Boliviano|Boliviano boliviano]] || [[Bolivia]]
|-
| BOV || 984 || 2 || Mvdol Boliviano (codice per i fondi) || [[Bolivia]]
|-
| BRL || 986 || 2 || [[Real brasiliano]] || [[Brasile]]
|-
| BSD || 044 || 2 || [[Dollaro delle Bahamas]] || [[Bahamas]]
|-
| BTN || 064 || 2 || [[Ngultrum del Bhutan]] || [[Bhutan]]
|-
| BWP || 072 || 2 || [[Pula del Botswana]] || [[Botswana]]
|-
| BYN || 933 || 2 || [[Rublo bielorusso]] || [[Bielorussia]]
|-
| BZD || 084 || 2 || [[Dollaro del Belize]] || [[Belize]]
|-
| CAD || 124 || 2 || [[Dollaro canadese]] || [[Canada]]
|-
| CDF || 976 || 2 || [[Franco congolese]] || [[Repubblica Democratica del Congo]]
|-
| CHE || 947 || 2 || WIR Euro ([[valuta complementare]] della [[WIR Bank]]) || [[Svizzera]]
|-
| CHF || 756 || 2 || [[Franco svizzero]] || [[Svizzera]], [[Liechtenstein]], [[Italia]] ([[Campione d\'Italia]])
|-
| CHW || 948 || 2 || WIR Franco ([[valuta complementare]] della [[WIR Bank]]) || [[Svizzera]]
|-
| CLF || 990 || 0 || Unidades de fomento Cilena (codice per i fondi) || [[Cile]]
|-
| CLP || 152 || 0 || [[Peso cileno]] || [[Cile]]
|-
| CNY || 156 || 2 || [[Renminbi cinese]] (Yuan) || [[Cina]]
|-
| COP || 170 || 2 || [[Peso colombiano]] || [[Colombia]]
|-
| COU || 970 || 2 || Unidad de Valor Real (codice per i fondi) || [[Colombia]]
|-
| CRC || 188 || 2 || [[Colón costaricano]] || [[Costa Rica]]
|-
| CUC || 931 || 2 || [[Peso cubano convertibile]] || [[Cuba]]
|-
| CUP || 192 || 2 || [[Peso cubano]] || [[Cuba]]
|-
| CVE || 132 || 2 || [[Escudo di Capo Verde]] || [[Capo Verde]]
|-
| CZK || 203 || 2 || [[Corona ceca]] || [[Repubblica Ceca]]
|-
| DJF || 262 || 0 || [[Franco gibutiano]] || [[Gibuti]]
|-
| DKK || 208 || 2 || [[Corona danese]] || [[Danimarca]], [[Fær Øer]], [[Groenlandia]]
|-
| DOP || 214 || 2 || [[Peso dominicano]] || [[Repubblica Dominicana]]
|-
| DZD || 012 || 2 || [[Dinaro algerino]] || [[Algeria]]
|-
| EGP || 818 || 2 || [[Lira egiziana]] (o sterlina) || [[Egitto]]
|-
| ERN || 232 || 2 || [[Nakfa eritreo]] || [[Eritrea]]
|-
| ETB || 230 || 2 || [[Birr etiope]] || [[Etiopia]]
|-
| EUR || 978 || 2 || [[Euro]] || Tutti i Paesi dell\'[[Unione Monetaria Europea]]
|-
| FJD || 242 || 2 || [[Dollaro delle Figi]] || [[Figi]]
|-
| FKP || 238 || 2 || [[Sterlina delle Falkland]] || [[Isole Falkland]]
|-
| GBP || 826 || 2 || [[Sterlina britannica]] (o lira sterlina) || [[Regno Unito]]
|-
| GEL || 981 || 2 || [[Lari georgiano]] || [[Georgia]]
|-
| GHS || 936 || 2 || [[Cedi ghanese]] || [[Ghana]]
|-
| GIP || 292 || 2 || [[Sterlina di Gibilterra]] || [[Gibilterra]]
|-
| GMD || 270 || 2 || [[Dalasi gambese]] || [[Gambia]]
|-
| GNF || 324 || 0 || [[Franco guineano]] || [[Guinea]]
|-
| GTQ || 320 || 2 || [[Quetzal guatemalteco]] || [[Guatemala]]
|-
| GYD || 328 || 2 || [[Dollaro della Guyana]] || [[Guyana]]
|-
| HKD || 344 || 2 || [[Dollaro di Hong Kong]] || [[Hong Kong]]
|-
| HNL || 340 || 2 || [[Lempira honduregna]] || [[Honduras]]
|-
| HRK || 191 || 2 || [[Kuna croata]] || [[Croazia]]
|-
| HTG || 332 || 2 || [[Gourde haitiano]] || [[Haiti]]
|-
| HUF || 348 || 2 || [[Fiorino ungherese]] || [[Ungheria]]
|-
| IDR || 360 || 2 || [[Rupia indonesiana]] || [[Indonesia]]
|-
| ILS || 376 || 2 || [[Nuovo siclo israeliano]] || [[Israele]]
|-
| INR || 356 || 2 || [[Rupia indiana]] || [[India]], [[Bhutan]]
|-
| IQD || 368 || 3 || [[Dinaro iracheno]] || [[Iraq]]
|-
| IRR || 364 || 2 || [[Rial iraniano]] || [[Iran]]
|-
| ISK || 352 || 2 || [[Corona islandese]] || [[Islanda]]
|-
| JMD || 388 || 2 || [[Dollaro giamaicano]] || [[Giamaica]]
|-
| JOD || 400 || 3 || [[Dinaro giordano]] || [[Giordania]]
|-
| JPY || 392 || 0 || [[Yen]] || [[Giappone]]
|-
| KES || 404 || 2 || [[Scellino keniota]] || [[Kenya]]
|-
| KGS || 417 || 2 || [[Som kirghizo]] || [[Kirghizistan]]
|-
| KHR || 116 || 2 || [[Riel cambogiano]] || [[Cambogia]]
|-
| KMF || 174 || 0 || [[Franco delle Comore]] || [[Comore]]
|-
| KPW || 408 || 2 || [[Won nordcoreano]] || [[Corea del Nord]]
|-
| KRW || 410 || 0 || [[Won sudcoreano]] || [[Corea del Sud]]
|-
| KWD || 414 || 3 || [[Dinaro kuwaitiano]] || [[Kuwait]]
|-
| KYD || 136 || 2 || [[Dollaro delle Cayman]] || [[Isole Cayman]]
|-
| KZT || 398 || 2 || [[Tenge kazako]] || [[Kazakistan]]
|-
| LAK || 418 || 2 || [[Kip laotiano]] || [[Laos]]
|-
| LBP || 422 || 2 || [[Lira libanese]] (o sterlina) || [[Libano]]
|-
| LKR || 144 || 2 || [[Rupia singalese]] || [[Sri Lanka]]
|-
| LRD || 430 || 2 || [[Dollaro liberiano]] || [[Liberia]]
|-
| LSL || 426 || 2 || [[Loti lesothiano]] || [[Lesotho]]
|-
| LYD || 434 || 3 || [[Dinaro libico]] || [[Libia]]
|-
| MAD || 504 || 2 || [[Dirham marocchino]] || [[Marocco]], [[Sahara Occidentale]]
|-
| MDL || 498 || 2 || [[Leu moldavo]] || [[Moldavia]]
|-
| MGA || 969 || 0 || [[Ariary malgascio]] || [[Madagascar]]
|-
| MKD || 807 || 2 || [[Dinaro macedone]] || [[Macedonia del Nord]]
|-
| MMK || 104 || 2 || [[Kyat birmano]] || [[Birmania]]
|-
| MNT || 496 || 2 || [[Tugrik mongolo]] || [[Mongolia]]
|-
| MOP || 446 || 2 || [[Pataca di Macao]] || [[Macao]]
|-
| MRO || 478 || 2 || [[Ouguiya mauritana]] || [[Mauritania]]
|-
| MUR || 480 || 2 || [[Rupia mauriziana]] || [[Mauritius]]
|-
| MVR || 462 || 2 || [[Rufiyaa delle Maldive]] || [[Maldive]]
|-
| MWK || 454 || 2 || [[Kwacha malawiano]] || [[Malawi]]
|-
| MXN || 484 || 2 || [[Peso messicano]] || [[Messico]]
|-
| MXV || 979 || 2 || Unidad de Inversion messicano (codice per i fondi) || [[Messico]]
|-
| MYR || 458 || 2 || [[Ringgit malese]] || [[Malaysia]]
|-
| MZN || 943 || 2 || [[Metical mozambicano]] || [[Mozambico]]
|-
| NAD || 516 || 2 || [[Dollaro namibiano]] || [[Namibia]]
|-
| NGN || 566 || 2 || [[Naira nigeriana]] || [[Nigeria]]
|-
| NIO || 558 || 2 || [[Córdoba nicaraguense]] || [[Nicaragua]]
|-
| NOK || 578 || 2 || [[Corona norvegese]] || [[Norvegia]]
|-
| NPR || 524 || 2 || [[Rupia nepalese]] || [[Nepal]]
|-
| NZD || 554 || 2 || [[Dollaro neozelandese]] || [[Nuova Zelanda]], [[Isole Cook]], [[Niue]], [[Isole Pitcairn|Pitcairn]], [[Tokelau]]
|-
| OMR || 512 || 3 || [[Rial dell\'Oman]] || [[Oman]]
|-
| PAB || 590 || 2 || [[Balboa panamense]] || [[Panama]]
|-
| PEN || 604 || 2 || [[Nuevo sol peruviano]] || [[Perù]]
|-
| PGK || 598 || 2 || [[Kina papuana]] || [[Papua Nuova Guinea]]
|-
| PHP || 608 || 2 || [[Peso filippino]] || [[Filippine]]
|-
| PKR || 586 || 2 || [[Rupia pakistana]] || [[Pakistan]]
|-
| PLN || 985 || 2 || [[Złoty polacco]] || [[Polonia]]
|-
| PYG || 600 || 0 || [[Guaraní paraguaiano]] || [[Paraguay]]
|-
| QAR || 634 || 2 || [[Rial del Qatar]] || [[Qatar]]
|-
| RON || 946 || 2 || [[Leu rumeno|Nuovo leu rumeno]] || [[Romania]]
|-
| RSD || 941 || 2 || [[Dinaro serbo]] || [[Serbia]]
|-
| RUB || 643 || 2 || [[Rublo russo]] || [[Russia]]
|-
| RWF || 646 || 0 || [[Franco ruandese]] || [[Ruanda]]
|-
| SAR || 682 || 2 || [[Rial saudita]] || [[Arabia Saudita]]
|-
| SBD || 090 || 2 || [[Dollaro delle Salomone]] || [[Isole Salomone]]
|-
| SCR || 690 || 2 || [[Rupia delle Seychelles]] || [[Seychelles]]
|-
| SDG || 938 || 2 || [[Sterlina sudanese]] || [[Sudan]]
|-
| SEK || 752 || 2 || [[Corona svedese]] || [[Svezia]]
|-
| SGD || 702 || 2 || [[Dollaro di Singapore]] || [[Singapore]]
|-
| SHP || 654 || 2 || [[Sterlina di Sant\'Elena]] || [[Sant\'Elena (isola)|Sant\'Elena]]
|-
| SLL || 694 || 2 || [[Leone sierraleonese]] || [[Sierra Leone]]
|-
| SOS || 706 || 2 || [[Scellino somalo]] || [[Somalia]]
|-
| SRD || 968 || 2 || [[Dollaro surinamese]] || [[Suriname]]
|-
| SSP || 938 || 2 || [[Sterlina sudsudanese]] || [[Sudan del Sud]]
|-
| STD || 678 || 2 || [[Dobra di São Tomé e Príncipe]] || [[São Tomé e Príncipe]]
|-
| SYP || 760 || 2 || [[Lira siriana]] (o sterlina) || [[Siria]]
|-
| SZL || 748 || 2 || [[Lilangeni dello Swaziland]] || [[Swaziland]]
|-
| THB || 764 || 2 || [[Baht thailandese]] || [[Thailandia]]
|-
| TJS || 972 || 2 || [[Somoni tagiko]] || [[Tagikistan]]
|-
| TMT || 795 || 2 || [[Manat turkmeno]] || [[Turkmenistan]]
|-
| TND || 788 || 3 || [[Dinaro tunisino]] || [[Tunisia]]
|-
| TOP || 776 || 2 || [[Paʻanga tongano]] || [[Tonga]]
|-
| TRY || 949 || 2 || [[Lira turca|Nuova lira turca]] || [[Turchia]]
|-
| TTD || 780 || 2 || [[Dollaro di Trinidad e Tobago]] || [[Trinidad e Tobago]]
|-
| TWD || 901 || 2 || [[Dollaro taiwanese|Nuovo dollaro taiwanese]] || [[Taiwan]]
|-
| TZS || 834 || 2 || [[Scellino tanzaniano]] || [[Tanzania]]
|-
| UAH || 980 || 2 || [[Grivnia ucraina]] || [[Ucraina]]
|-
| UGX || 800 || 2 || [[Scellino ugandese]] || [[Uganda]]
|-
| USD || 840 || 2 || [[Dollaro statunitense]] || [[Stati Uniti d\'America]], [[Samoa Americane]], [[Territorio Britannico dell\'Oceano Indiano]], [[Ecuador]], [[El Salvador]], [[Guam]], [[Haiti]], [[Isole Marshall]], [[Stati Federati di Micronesia|Micronesia]], [[Isole Marianne Settentrionali]], [[Palau (stato)|Palau]], [[Panama]], [[Timor Est]], [[Turks e Caicos]], [[Isole Vergini americane]]
|-
| USN || 997 || 2 || Dollaro statunitense (Next day) (codice per i fondi) || [[Stati Uniti d\'America]]
|-
| USS || 998 || 2 || Dollaro statunitense (Same day) (codice per i fondi)|| [[Stati Uniti d\'America]]
|-
| UYU || 858 || 2 || [[Peso uruguaiano]] || [[Uruguay]]
|-
| UZS || 860 || 2 || [[Som uzbeco]] || [[Uzbekistan]]
|-
| VEF || 862 || 2 || [[Bolívar venezuelano]] || [[Venezuela]]
|-
| VND || 704 || 2 || [[Đồng vietnamita]] || [[Vietnam]]
|-
| VUV || 548 || 0 || [[Vatu|Vatu di Vanuatu]] || [[Vanuatu]]
|-
| WST || 882 || 2 || [[Tala samoano]] || [[Samoa]]
|-
| XAF || 950 || 0 || [[Franco CFA]] BEAC || [[Camerun]], [[Repubblica Centrafricana]], [[Repubblica del Congo]], [[Ciad]], [[Guinea Equatoriale]], [[Gabon]]
|-
| XAG || 961 || . || [[Argento]] ||
|-
| XAL || ... || . || [[Alluminio]] ||
|-
| XAU || 959 || . || [[Oro]] ||
|-
| XBA || 955 || . || Unità Composita Europea (EURCO) (unità per il mercato obbligazionario) ||
|-
| XBB || 956 || . || Unità Monetaria Europea (E.M.U.-6) (unità per il mercato obbligazionario) ||
|-
| XBC || 957 || . || Unità di acconto Europea 9 (E.U.A.-9) (unità per il mercato obbligazionario) ||
|-
| XBD || 958 || . || Unità di acconto Europea 17 (E.U.A.-17) (unità per il mercato obbligazionario) ||
|-
| XCD || 951 || 2 || [[Dollaro dei Caraibi Orientali]] || [[Anguilla (isola)|Anguilla]], [[Antigua e Barbuda]], [[Dominica]], [[Grenada]], [[Montserrat (isola)|Montserrat]], [[Saint Kitts e Nevis]], [[Saint Vincent e Grenadine]], [[Santa Lucia (stato)|Santa Lucia]]
|-
| XCP || ... || . || [[Rame]] ||
|-
| XDR || 960 || . || [[Diritti Speciali di Prelievo]] || [[Fondo Monetario Internazionale]] (IMF)
|-
| XFO || Nil || . || Franco-Oro (valuta speciale) || [[Banca dei regolamenti internazionali]] (BRI), fino all\'aprile 2003
|-
| XFU || Nil || . || Franco UIC (valuta speciale) || [[Unione internazionale delle ferrovie]] (UIC), fino al novembre 2013
|-
| XOF || 952 || 0 || [[Franco CFA]] BCEAO || [[Benin]], [[Burkina Faso]], [[Costa d\'Avorio]], [[Guinea-Bissau]], [[Mali]], [[Niger]], [[Senegal]], [[Togo]]
|-
| XPD || 964 || . || [[Palladio (elemento chimico)|Palladio]] ||
|-
| XPF || 953 || 0 || [[Franco CFP]] || [[Polinesia Francese]], [[Nuova Caledonia]], [[Wallis e Futuna]]
|-
| XPT || 962 || . || [[Platino]] ||
|-
| XTS || 963 || . || Codice riservato a scopo di test ||
|-
| XXX || 999 || . || Nessuna valuta (nessuna transazione) ||
|-
| YER || 886 || 2 || [[Rial yemenita]] || [[Yemen]]
|-
| ZAR || 710 || 2 || [[Rand sudafricano]] || [[Sudafrica]]
|-
| ZMW || 894 || 2 || [[Kwacha zambiano]] || [[Zambia]]
|-
| ZWL || 932 || 2 || [[Dollaro zimbabwiano]] || [[Zimbabwe]]';

        $rows = explode('|-' . PHP_EOL . '|', $schema);
        $export = [];
        foreach ($rows as $row) {
            $data = explode('||', $row);

            $iso_code = trim($data[0]);
            $iso_code_num = trim($data[1]);
            $decimals = trim($data[2]);
            $export[$iso_code] = compact('iso_code', 'iso_code_num', 'decimals');
        }
        return $export;
    }

    private function getDefaultsCurrencies()
    {
        return [
            'EUR', 'GBP', 'USD'
        ];
    }


    private function getEnabledCurrencies()
    {
        return [
            'EUR', 'DKK', 'GBP', 'HRK', 'SEK', 'HUF', 'PLN', 'CZK', 'RON', 'BGN', 'USD'
        ];
    }


    public function run()
    {
        $lookUp = $this->getLookupTable();
        $data = $this->getData();

        $isCinturino = config('app.project') == 'cinturino';

        DB::beginTransaction();

        foreach ($data as $currency_array) {
            list($country, $name, $iso_code, $sign) = $currency_array;
            $iso_code_num = 0;
            $decimals = 2;

            $defaults = [
                'dec_point' => ',',
                'thousan_sep' => '.',
                'deleted' => 0,
                'active' => 0,
                'format' => 0,
                'conversion_rate' => 0,
                'blank' => 0,
            ];

            $isActive = in_array($iso_code, $this->getDefaultsCurrencies());
            if ($isActive == false and $isCinturino) {
                $isActive = in_array($iso_code, $this->getEnabledCurrencies());
            }

            $defaults['active'] = (int)$isActive;
            $active = (int)$isActive;

            if (isset($lookUp[$iso_code])) {
                $iso_code_num = $lookUp[$iso_code]['iso_code_num'];
                $decimals = $lookUp[$iso_code]['decimals'];
            }

            $payload = compact('name', 'iso_code', 'sign', 'iso_code_num', 'decimals', 'active');
            $obj = (object)$payload;
            $currency = Currency::where('iso_code', $obj->iso_code)->first();

            if ($currency) {
                unset($payload['name']);
                $currency->fill($payload);
                $currency->save();
                $this->command->comment("Updated currency [$currency->iso_code]");
            } else {
                $currency = new Currency($defaults);
                $payload['name'] = "{$payload['name']} ({$payload['iso_code']})";
                $currency->fill($payload);
                $currency->save();
                $this->command->comment("Inserted currency [$currency->iso_code]");
            }

            if ($currency) {
                $country_id = DB::table('countries_lang')->where('name', $country)->pluck('country_id');
                if ($country_id > 0) {
                    DB::table('countries')->where('id', $country_id)->update(['currency_id' => $currency->id]);
                    $this->command->comment("Updated country [$country_id]");
                }
            }
        }

        DB::commit();
    }
}