<?php

namespace services\Seeders;

use Seeder;
use Lexicon;
use DB;

class ElasticSearchSeeder extends Seeder
{
    public function run()
    {
        $code = 'lbl_livesearch_price';
        $record = Lexicon::where('code', $code)->first();
        if ($record) {
            $this->command->line("$code lexicon key is already seeded.");
        } else {
            $data = [
                'code' => $code,
                'ldesc' => 'Etichetta prezzo in Live Search',
                'is_shortcode' => '0',
                'is_deep' => '1',
                'is_javascript' => '1',
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info("$code Lexicon key seeded!");

            $lang_translations = [
                'it' => [
                    'name' => 'Prezzo'
                ],
                'en' => [
                    'name' => 'Price'
                ],
                'es' => [
                    'name' => 'Precio'
                ],
                'fr' => [
                    'name' => 'Prix'
                ],
                'de' => [
                    'name' => 'Preis'
                ],
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang $code({$lang_data['lang_id']}) key seeded!");
            }
        }
    }
}