<?php

namespace services\Seeders;

use Seeder;
use DB;

class MorellatoShopSeeder extends Seeder
{

    private function handleShopWithId($id)
    {
        $record = DB::table('mi_shops')->where('id', $id)->first();
        if (is_null($record)) {
            $this->command->line("MorellatoShop $id is already seeded.");
        } else {
            $newId = DB::table('mi_shops')->max('id') + 1;
            DB::table('mi_shops')->where('id', $id)->update(['id' => $newId]);
            DB::table('cart')->where('delivery_store_id', $id)->update(['delivery_store_id' => $newId]);
            DB::table('orders')->where('delivery_store_id', $id)->update(['delivery_store_id' => $newId]);
            DB::table('users')->where('shop_id', $id)->update(['shop_id' => $newId]);
            $this->command->info("MorellatoShop $id is successfully seeded with new id $newId.");
        }
    }

    public function run()
    {
        $this->handleShopWithId(1);
    }
}