<?php

namespace services\Seeders;

use Seeder;
use Email;
use DB;

class CustomerEmailWarningsSeeder extends Seeder
{

    public function run()
    {
        $this->WARNING_SHOP_PICKUP();
        $this->WARNING_SHOP_PACKAGE_READY();
        $this->WARNING_SHOP_CLERKS();
        $this->WARNING_GLS_CLERKS();
    }


    private function WARNING_SHOP_PICKUP()
    {
        $code = 'WARNING_SHOP_PICKUP';
        $sampleRecord = Email::getByCode('ORDER_CONFIRM');
        $record = Email::where('code', $code)->first();
        if ($record) {
            $this->command->line("Emails model {$code} is already seeded.");
        } else {
            $data = [
                'code' => $code,
                'name' => 'Email di avviso ritiro prodotto in negozio',
                'sdesc' => 'Messaggio inviato quando ai Clienti che non hanno ritirato il loro prodotti a negozio',
                'sender' => $sampleRecord->sender,
                'active' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $email_id = DB::table('emails')->insertGetId($data);
            $this->command->info("Emails model {$code} successfully seeded.");

            $shared_data = [
                'shop_id' => 1,
                'subject' => 'Hai dei prodotti da ritirare in un negozio Bluespirit!',
                'sender_name' => $sampleRecord->sender_name,
                'body' => '<p>Gentile <strong>[CUSTOMER_NAME]</strong>,<br>hai ricevuto questo messaggio perchè ti ricordiamo che hai degli articoli da ritirare in uno dei punti vendita Bluespirit!</p><p>Di seguito troverai i dettagli del tuo ordine e del punto vendita Bluespirit che hai scelto per il ritiro:</p><br><strong>PUNTO VENDITA SCELTO PER IL RITIRO:</strong><br>[SHOP_DETAILS]<br><br><br><strong>IL TUO ORDINE:</strong><br>[ORDER_DETAILS]'
            ];

            $lang_translations = [
                'it' => $shared_data,
                'en' => $shared_data,
                'es' => $shared_data,
                'fr' => $shared_data,
                'de' => $shared_data,
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['email_id'] = $email_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('emails_lang')->insert($lang_data);
                $this->command->info("Emails model {$code} Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }


    private function WARNING_SHOP_PACKAGE_READY()
    {
        $code = 'WARNING_SHOP_PACKAGE_READY';
        $sampleRecord = Email::getByCode('WARNING_SHOP_PICKUP');
        $record = Email::where('code', $code)->first();
        if ($record) {
            $this->command->line("Emails model {$code} is already seeded.");
        } else {
            $data = [
                'code' => $code,
                'name' => 'Email di avviso pacco pronto per il ritiro in negozio',
                'sdesc' => 'Messaggio inviato quando ai Clienti per avvisarli che il pacco è pronto per il ritiro in negozio',
                'sender' => $sampleRecord->sender,
                'active' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $email_id = DB::table('emails')->insertGetId($data);
            $this->command->info("Emails model {$code} successfully seeded.");

            $shared_data = [
                'shop_id' => 1,
                'subject' => 'Il tuo pacco è pronto per il ritiro!⏳',
                'sender_name' => $sampleRecord->sender_name,
                'body' => '<p>Buone Notizie&nbsp;<strong>[CUSTOMER_NAME]</strong>,<br />
Il tuo ordine &egrave; disponibile presso il nostro negozio!<br />
<br />
[SHOP_DETAILS]<br />
<br />
<br />
<strong>IL TUO ORDINE:</strong><br />
[ORDER_DETAILS]</p>
<p><u>Ti ricordiamo che hai 20 giorni per ritirare il tuo ordine!</u><br />
Per qualsiasi informazione non esitate a contattarci:<br />
Mail: <strong>[SITEMAIL]</strong><br />
Numero verde: <strong>800 910 262</strong><br />
A Presto!</p>'
            ];

            $lang_translations = [
                'it' => $shared_data,
                'en' => $shared_data,
                'es' => $shared_data,
                'fr' => $shared_data,
                'de' => $shared_data,
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['email_id'] = $email_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('emails_lang')->insert($lang_data);
                $this->command->info("Emails model {$code} Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }


    private function WARNING_SHOP_CLERKS()
    {
        $code = 'WARNING_SHOP_CLERKS';
        $sampleRecord = Email::getByCode('WARNING_SHOP_PICKUP');
        $record = Email::where('code', $code)->first();
        if ($record) {
            $this->command->line("Emails model {$code} is already seeded.");
        } else {
            $data = [
                'code' => $code,
                'name' => 'Email di sollecito ritiro ordine disponibile in store',
                'sdesc' => 'Il sistema dovrebbe inviare una specifica mail al negozio di ritiro nella quale indicare che l\'ordine in oggetto è fermo nel negozio nonostante il cliente abbia ricevuto una mail di sollecito, pertanto le commesse sono pregate di chiamare il cliente telefonicamente per sollecitare il ritiro',
                'sender' => $sampleRecord->sender,
                'active' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $email_id = DB::table('emails')->insertGetId($data);
            $this->command->info("Emails model {$code} successfully seeded.");

            $shared_data = [
                'shop_id' => 1,
                'subject' => 'Ordine non ritirato fermo nel negozio, Contatta il cliente!',
                'sender_name' => $sampleRecord->sender_name,
                'body' => '<h3>ATTENZIONE: Hai fermo nel tuo store un ordine [SITENAME] [ORDER_NUMBER].</h3>
Ti preghiamo di contattare telefonicamente il cliente per ricordargli che potr&agrave; ritirare il suo ordine presso il tuo negozio, questo il numero di telefono:<br />
<br />
<strong style="font-size: 17px">[CUSTOMER_CONTACT]</strong>
<div><br />
<br />
<strong>IMPORTANTE</strong>: Qual&#39;ora il cliente avesse gi&agrave; ritirato il prodotto ti preghiamo di contattarci </span> <a href="mailto:[SITEMAIL]?subject=ORDINE%20[ORDER_NUMBER]%20GIÀ%20RITIRATO&amp;body=Ciao%20Team%20[SITENAME]%20ti%20informo%20che%20l\'ordine%20[ORDER_NUMBER]%20è%20già%20stato%20ritirato%20dal%20cliente,%20vi%20preghiamo%20quindi%20di%20bloccare%20le%20mail%20di%20sollecito.%20grazie%20">cliccando qui (mail precompilata, devi solo premere invio per informare il team web).</a> <span>Grazie per la collaborazione.</span></div>
'
            ];

            $lang_translations = [
                'it' => $shared_data,
                'en' => $shared_data,
                'es' => $shared_data,
                'fr' => $shared_data,
                'de' => $shared_data,
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['email_id'] = $email_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('emails_lang')->insert($lang_data);
                $this->command->info("Emails model {$code} Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }


    private function WARNING_GLS_CLERKS()
    {
        $code = 'WARNING_GLS_CLERKS';
        $sampleRecord = Email::getByCode('WARNING_SHOP_PICKUP');
        $record = Email::where('code', $code)->first();
        if ($record) {
            $this->command->line("Emails model {$code} is already seeded.");
        } else {
            $data = [
                'code' => $code,
                'name' => 'Avviso spedizione in arrivo per lo store',
                'sdesc' => 'Mail di avviso che sulla base del feedback GLS "spedizioni partita" avvisa il negozio di ritiro che c\'è una spedizione in arrivo.',
                'sender' => $sampleRecord->sender,
                'active' => 1,
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $email_id = DB::table('emails')->insertGetId($data);
            $this->command->info("Emails model {$code} successfully seeded.");

            $shared_data = [
                'shop_id' => 1,
                'subject' => 'Avviso Spedizione Ordine Web in arrivo nel tuo negozio',
                'sender_name' => $sampleRecord->sender_name,
                'body' => '<div><strong>ATTENZIONE!!</strong></div>
<div>
<ul>
	<li>GLS ha appena Ritirato il tuo ordine [ORDER_NUMBER], e ti sar&agrave; consegnato domani.</li>
</ul>
</div>
<div>&nbsp;</div>
<div><strong>RICORDATI DI:</strong></div>
<div>
<ul>
	<li>confermarne la ricezione entro 3 ore dalla consegna mediante carico merce e bippatura sul pannello web;</li>
	<li>scontrinare la merce (se il cliente deve pagare in negozio) usando i tasti del pannello cassa WEB--&gt;Consegna WEB;</li>
</ul>
<div><strong>Grazie della collaborazione!</strong></div>
</div>'
            ];

            $lang_translations = [
                'it' => $shared_data,
                'en' => $shared_data,
                'es' => $shared_data,
                'fr' => $shared_data,
                'de' => $shared_data,
            ];

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['email_id'] = $email_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('emails_lang')->insert($lang_data);
                $this->command->info("Emails model {$code} Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }
}