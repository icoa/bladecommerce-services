<?php

namespace services\Seeders;

use Seeder;
use Lexicon;
use DB;

class TemplateLexiconSeeder extends Seeder
{
    public function run()
    {
        $this->lex('msg_auth_block', [
            'it' => [
                'name' => "Attenzione<br>questo contenuto è abilitato solo per gli Utenti registrati"
            ],
            'en' => [
                'name' => "Warning<br>this content is enabled only for registered users"
            ],
            'es' => [
                'name' => "Advertencia<br>este contenido está habilitado solo para usuarios registrados"
            ],
            'fr' => [
                'name' => "Avertissement<br>ce contenu n'est activé que pour les utilisateurs enregistrés",
            ],
            'de' => [
                'name' => 'Warnung<br>dieser Inhalt ist nur für registrierte Benutzer aktiviert',
            ],
        ], 'Testo presentato agli Utenti che accedono ad un contenuto privato');

    }

    private function lex($code, $lang_translations, $ldesc = '')
    {
        $record = Lexicon::where('code', $code)->first();
        if ($record) {
            $this->command->line($code . ' lexicon code is already seeded.');
        } else {
            $data = [
                'code' => $code,
                'ldesc' => $ldesc,
                'is_shortcode' => '0',
                'is_deep' => '0',
                'is_javascript' => '0',
                'created_by' => 1,
                'updated_by' => 1,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $lexicon_id = DB::table('lexicons')->insertGetId($data);
            $this->command->info($code . ' lexicon code seeded!');

            foreach ($lang_translations as $lang_id => $lang_data) {
                $lang_data['lexicon_id'] = $lexicon_id;
                $lang_data['lang_id'] = $lang_id;
                DB::table('lexicons_lang')->insert($lang_data);
                $this->command->info("Lexicons Lang ({$lang_data['lang_id']}) table seeded!");
            }
        }
    }
}