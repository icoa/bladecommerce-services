<?php

namespace services\Membership\Traits;

use Exception;
use Form;
use services\Membership\Models\Affiliate;
use Illuminate\Database\Eloquent\Builder;
use ORder;

/**
 * Trait TraitAffiliateOrder
 * @package services\Membership\Traits
 * @mixin Order
 */
trait TraitAffiliateOrder
{

    /**
     * @return int
     */
    function getAffiliateCount()
    {
        $ids = [];
        $products = $this->getProducts();
        foreach ($products as $product) {
            if ($product->affiliate_id > 0) {
                $ids[] = $product->affiliate_id;
            }
        }
        $ids = array_unique($ids);
        return count($ids);
    }

    /**
     * @return int|null
     */
    function getAffiliateId()
    {
        $ids = [];
        $products = $this->getProducts();
        foreach ($products as $product) {
            if ($product->affiliate_id > 0) {
                $ids[] = $product->affiliate_id;
            }
        }
        $ids = array_unique($ids);
        return !empty($ids) ? $ids[0] : null;
    }

    /**
     * @return Affiliate|null
     */
    function getAffiliate()
    {
        $id = $this->affiliate_id;
        return Affiliate::getObj($id);
    }

    /**
     * @return bool
     */
    function hasProductsWithAffiliate()
    {
        return $this->getAffiliateCount() > 0;
    }

    /**
     * @return null|string
     */
    function getActionsForAffiliate()
    {
        $btn = null;
        $s = "<div class='hidden'>" . Form::text("tracking[]", $this->tracking_url, ['placeholder' => 'Scrivi qui il codice']) . "<button class='btn btn-primary order-action' data-action='setShippingNumber' data-id='{$this->id}'>MODIFICA</button></div>";
        if (strlen($this->tracking_url) > 0) {
            $btn = "<a href='$this->tracking_url' target='_blank' class='btn btn-info'>Traccia la spedizione</a>";
            if ($this->canAddTrackingUrl()) {
                $btn .= "<br><a data-placement='left' class='btn btn-warning trigger-popover' href='javascript:;' data-id='{$this->id}'>Modifica link tracciamento</a>$s";
            }
        } else {
            if ($this->canAddTrackingUrl())
                $btn = "<a data-placement='left' class='btn btn-warning trigger-popover' href='javascript:;' data-id='{$this->id}'>Aggiungi link tracciamento</a>$s";
        }
        return $btn;
    }

    /**
     * @return null|string
     */
    function getAffiliateTrackingUrlAttribute()
    {
        return strlen($this->tracking_url) > 0 ? "<a href='$this->tracking_url' target='_blank'>$this->tracking_url</a>" : null;
    }

    /**
     * @return bool
     */
    function canAddTrackingUrl()
    {
        return $this->status == \OrderState::STATUS_SHIPPED;
    }

    /**
     * @param $order_id
     * @param bool $acl
     * @return string
     */
    static function getAffiliateStatusSelect($order_id, $acl = false)
    {
        $order = \OrderHelper::getOrder($order_id);

        $builder = \OrderState::rows()->where("deleted", 0)->where("hidden", 0)->orderBy('name');
        if ($acl) {
            $ids = config('membership.order_inheritance.' . $order->status, []);
            $ids = array_merge($ids, [$order->status]);
            $builder->whereIn('id', $ids);
        }
        $rows = $builder->get();
        $data = \EchoArray::set($rows)->keyIndex('id', 'name')->get();
        $data = ["" => "Scegli..."] + $data;

        $html = Form::select('status_id', $data, $order->status);
        return $html;
    }

    /**
     * @param Builder $builder
     * @param $affiliate
     * @throws Exception
     */
    public function scopeWithAffiliate(Builder $builder, $affiliate)
    {
        if(is_object($affiliate))
            $affiliate = $affiliate->id;

        if (!is_numeric($affiliate) or $affiliate <= 0)
            throw new Exception("Cannot resolve a valid affiliate ID");

        $builder->where('affiliate_id', $affiliate);
    }

    /**
     * @param Builder $builder
     * @throws Exception
     */
    public function scopeWithoutAffiliate(Builder $builder)
    {
        $builder->whereNull('affiliate_id');
    }
}