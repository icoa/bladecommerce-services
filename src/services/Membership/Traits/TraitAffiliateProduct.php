<?php

namespace services\Membership\Traits;

use DB;
use services\Membership\Models\AffiliateProductOuterStock;
use services\Membership\Models\Affiliate;

trait TraitAffiliateProduct
{

    private $_affiliates_stocks;
    private $_affiliates;
    public $affiliate;

    /**
     * @return mixed
     */
    function getAffiliateStocks()
    {
        if (isset($this->_affiliates_stocks))
            return $this->_affiliates_stocks;

        $stocks = AffiliateProductOuterStock::with('affiliate')
            ->inStocks()
            ->withProduct($this->id)
            ->withProductCombination($this->getCombinationId())
            ->get();
        $this->_affiliates_stocks = $stocks;
        return $stocks;
    }


    function getAffiliateStocksWithDetails()
    {
        return AffiliateProductOuterStock::with('affiliate', 'combination')
            ->withProduct($this->id)
            ->get();
    }

    /**
     * @return int
     */
    function getOverallAffiliateStocksQuantity()
    {
        $stocks = $this->getAffiliateStocks();
        $qty = 0;
        foreach ($stocks as $stock) {
            $qty += $stock->quantity;
        }
        return $qty;
    }

    /**
     * @return int
     */
    function getAffiliatesCount()
    {
        $affiliates = [];
        $stocks = $this->getAffiliateStocks();
        foreach ($stocks as $stock) {
            $affiliates[] = $stock->affiliate->id;
        }
        return count(array_unique($affiliates));
    }

    /**
     * @param bool $isDefaultView
     * @return array|mixed
     */
    function getAffiliates($isDefaultView = false)
    {
        //force to return an empty array if the function has been called for the first time and the products has combinations
        //by doing this the user is forced to select a combination option
        if ($isDefaultView and $this->hasCombinations())
            return [];

        if (isset($this->_affiliates))
            return $this->_affiliates;

        $stocks = $this->getAffiliateStocks();
        foreach ($stocks as $stock) {
            $stock->affiliate->prices = $this->getAffiliatePrices($stock->affiliate->id);
        }

        $this->_affiliates = $stocks;
        return $stocks;
    }

    /**
     * @return Affiliate|null
     */
    function getSingleAffiliate()
    {
        $stocks = $this->getAffiliates();
        foreach ($stocks as $stock) {
            return $stock->affiliate;
        }
        return null;
    }

    /**
     * @return AffiliateProductOuterStock|null
     */
    function getAffiliateStockById($id)
    {
        $stocks = $this->getAffiliates();
        foreach ($stocks as $stock) {
            if ($stock->affiliate->id == $id)
                return $stock;
        }
        return null;
    }

    /**
     * @return bool
     */
    function hasAffiliateAvailability()
    {
        return $this->getOverallAffiliateStocksQuantity() > 0;
    }


    /**
     * @param $affiliate_id
     * @return \stdClass|null
     */
    public function getAffiliatePrices($affiliate_id)
    {
        $price_offer = null;
        $rows = \ProductPrice::where('active', 1)
            ->where('global', 0)
            ->where('affiliate_id', $affiliate_id)
            ->where("product_id", $this->id)
            ->orderBy("position")->get();
        $now = time();
        $price_rules = [];
        $price_currency_id = \Core::getDefaultCurrency()->id;

        foreach ($rows as $rule) {
            //\Utils::log($rule,"RULE");
            $process_rule = false;
            if ($rule->date_from) {
                $rule_from = strtotime($rule->date_from);
                if ($now >= $rule_from) {
                    $process_rule = true;
                }
                if ($rule->date_to) {
                    $rule_to = strtotime($rule->date_to);
                    if ($now <= $rule_to) {
                        $process_rule = true;
                    }
                }
            } else {
                if ($rule->date_to) {
                    $rule_to = strtotime($rule->date_to);
                    if ($now <= $rule_to) {
                        $process_rule = true;
                    }
                }
            }
            if ($rule->date_from == null AND $rule->date_to == null) {
                $process_rule = true;
            }
            if ($process_rule) {
                $assert_country = \RestrictionHelper::country_id($rule->country_id);
                $assert_currency = \RestrictionHelper::currency_id($rule->currency_id);
                $assert_group = \RestrictionHelper::group_id($rule->customer_group_id);
                $process_rule = ($assert_country AND $assert_currency AND $assert_group);
            }
            if ($process_rule == true) {
                $price_offer = new \stdClass();
                $price_rules[] = $rule->price_rule_id;
                $public_prices = \ProductHelper::calculateProductPrice($this, $rule);
                $price_with_taxes = $public_prices['price_wt'];
                $price = $public_prices['price'];
                //\Utils::log($public_prices,"PUBLIC PRICES [$this->id]");
                $price_offer->price = $price_with_taxes;
                $price_offer->sell_price = $price;
                $price_offer->sell_price_wt = $price_with_taxes;
                $price_offer->price_nt = $price;
                if ($rule->reduction_currency > 0) {
                    $price_currency_id = $rule->reduction_currency;
                }
                if ($rule->stop_other_rules == 1) {
                    break;
                }
            }
        }

        //if no rule has been processed, than the price is equale to the standard product
        if (is_null($price_offer)) {
            $price_offer = new \stdClass();
            $price_offer->price = $this->price;
            $price_offer->sell_price = $this->sell_price;
            $price_offer->sell_price_wt = $this->sell_price_wt;
            $price_offer->price_nt = $this->price_nt;
        }

        //price normal calculation
        $price_official = $price_offer->sell_price_wt;
        $price_final = $price_offer->price;
        $price_saving = $price_official - $price_final;
        if (abs($price_saving) <= 0.2) $price_saving = 0;

        $current_currency = \Core::getCurrency();

        $price_offer->price_official = \Format::currency($price_official, true, $current_currency);
        $price_offer->price_final = \Format::currency($price_final, true, $current_currency);
        $price_offer->price_saving = \Format::currency($price_saving, true, $current_currency);
        $price_offer->price_label = \Format::currency($price_final, false, $current_currency);
        $price_offer->price_official_raw = $price_official;
        $price_offer->price_saving_raw = $price_saving;
        $price_offer->price_final_raw = $price_final;
        $price_offer->price_currency_id = $price_currency_id;
        //alias
        $price_offer->price_list = $price_offer->price_official;
        $price_offer->price_list_raw = $price_offer->price_official_raw;
        $price_offer->price_shop = $price_offer->price_final;
        $price_offer->price_shop_raw = $price_offer->price_final_raw;
        return $price_offer;
    }


    /**
     * @param $priceDefinition
     */
    function replacePrices($priceDefinition)
    {
        foreach ($priceDefinition as $key => $value) {
            $this->setAttribute($key, $value);
        }
    }

    /**
     * @param $id
     * @return Affiliate|null
     */
    function findAffiliate($id)
    {
        return Affiliate::getObj($id);
    }

    /**
     * @return bool
     */
    function hasAffiliate()
    {
        return isset($this->affiliate);
    }

    /**
     * @param $qty
     * @param int $affiliate_id
     * @param int $combination_id
     */
    function decrementAffiliateQty($qty, $affiliate_id = 0, $combination_id = 0)
    {
        audit($qty, __METHOD__);
        if ($combination_id > 0 and is_null($this->combination)) {
            $this->combination = \ProductCombination::getObj($combination_id);
        }
        try {
            $stock = $this->getAffiliateStockById($affiliate_id);
            audit($stock, __METHOD__ . '::found stock');
            $qty = abs($qty);
            $stock->quantity -= $qty;
            $stock->save();
        } catch (\Exception $e) {
            audit($e->getMessage(), "AFFILIATE PRODUCT DECREMENT QTY ISSUE");
        }
    }


    function scopeInStocksForAffiliates($query)
    {
        return $query->whereIn('id', function ($builder) {
            $builder->select('product_id')
                ->from('products_outer_stocks')
                ->where('warehouse', 'AF')
                ->where('quantity', '>', 0);
        });
    }
}