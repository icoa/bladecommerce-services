<?php

namespace services\Membership\Traits;

use DB;
use services\Membership\Models\AffiliateProductOuterStock;
use services\Membership\Models\Affiliate;
use Frontend\CartManager;

trait TraitAffiliateCartManager
{

    /**
     * @return int
     */
    function getAffiliateCount()
    {
        $ids = [];
        $products = $this->getProducts();
        foreach ($products as $product) {
            if ($product->cart_details->affiliate_id > 0) {
                $ids[] = $product->cart_details->affiliate_id;
            }
        }
        $ids = array_unique($ids);
        return count($ids);
    }

    /**
     * @return int|null
     */
    function getSingleAffiliateId()
    {
        $ids = [];
        $products = $this->getProducts();
        foreach ($products as $product) {
            if ($product->cart_details->affiliate_id > 0) {
                $ids[] = $product->cart_details->affiliate_id;
            }
        }
        $ids = array_unique($ids);
        return (!empty($ids) and count($ids) == 1) ? $ids[0] : null;
    }

    /**
     * @return bool
     */
    function hasProductsWithAffiliate()
    {
        return $this->getAffiliateCount() > 0;
    }

    /**
     * @return string
     */
    function getAffiliateMode()
    {
        if (!\Core::membership())
            return CartManager::AFFILIATE_MODE_DEFAULT;

        if (\FrontTpl::getScope() == 'page')
            return CartManager::AFFILIATE_MODE_DEFAULT;

        if (!$this->hasProductsWithAffiliate())
            return CartManager::AFFILIATE_MODE_DEFAULT;

        $totalProductsRows = count($this->getProducts());
        if ($totalProductsRows == $this->getAffiliateCount()) {
            return CartManager::AFFILIATE_MODE_COMPLETE;
        }

        return CartManager::AFFILIATE_MODE_MIXED;
    }

    /**
     * @param \Payment $payment
     * @return bool
     */
    function getPaymentAffiliateAssert(\Payment $payment)
    {
        if (!\Core::membership())
            return true;

        $affiliateMode = $this->getAffiliateMode();
        //audit($affiliateMode, __METHOD__ . '::affiliateMode');
        //audit($payment->id, __METHOD__ . '::payment id');

        //in 'default' mode the assertion is always true
        if ($affiliateMode == CartManager::AFFILIATE_MODE_DEFAULT) {
            return true;
        }
        //if the payment id is in blacklist, it is always disabled
        if (in_array($payment->id, config('membership.payments.always_disabled')))
            return false;

        //in 'mixed' mode, check specific blacklist
        if ($affiliateMode = CartManager::AFFILIATE_MODE_MIXED) {
            if (in_array($payment->id, config('membership.payments.disabled_mode_mixed')))
                return false;
        }

        return true;
    }
}