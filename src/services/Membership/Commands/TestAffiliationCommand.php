<?php

namespace services\Membership\Commands;

use Illuminate\Console\Command;
use services\Membership\Repositories\AffiliateBackendRepository;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Product;

class TestAffiliationCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'affiliation:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Performs some tests on affiliation env.';

    /**
     * @var AffiliateBackendRepository
     */
    protected $backendRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->backendRepository = app('services\Membership\Repositories\AffiliateBackendRepository');
        $this->info("Executing Affiliation Test...");
        //$this->testImportFile();
        $this->testProductStocks();
    }


    private function testImportFile()
    {
        $testFile = base_path('sources/affiliazione/Import_Model.csv');
        $this->backendRepository->importCsvStocksFile($testFile);
    }

    private function testProductStocks()
    {
        audit_watch();
        $this->comment("Testing " . __METHOD__);
        $model = Product::find(21022); //30110
        $stocks = $model->getAffiliateStocks();
        print_r($stocks->toArray());
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}