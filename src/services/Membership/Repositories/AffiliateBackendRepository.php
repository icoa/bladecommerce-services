<?php

namespace services\Membership\Repositories;

use services\Membership\Models\AffiliateProductOuterStock;
use services\Morellato\Repositories\Repository;
use Illuminate\Filesystem\Filesystem;
use League\Csv\Reader;
use DB;
use services\src\services\Membership\AffiliateLogger;
use Utils;
use services\Membership\Models\ImportAffiliateStock;
use Product;
use ProductCombination;
use AdminUser;
use App;
use Illuminate\Support\Str;

class AffiliateBackendRepository extends Repository
{
    /**
     * @var Filesystem
     */
    protected $files;

    /**
     * @var AffiliateLogger
     */
    protected $logger;


    protected $affiliate_id;
    protected $attribute_option_id;

    function __construct(Filesystem $files, AffiliateLogger $logger)
    {
        $this->files = $files;
        $this->logger = $logger;
        $this->affiliate_id = App::runningInConsole() ? 1 : AdminUser::get()->affiliate_id;
        $this->logger->setScope($this->affiliate_id);
    }

    function importCsvStocksFile($file)
    {
        $content = $this->files->get($file);
        $encoding = mb_detect_encoding($content);
        $this->log("Detected Encoding: $encoding");
        if ($encoding != 'UTF-8') {
            if ($encoding == 'ASCII')
                $encoding = 'Windows-1252';
        }
        $csv_content = iconv($encoding, "UTF-8", $content);
        //$this->log($csv_content, "CSV_CONTENT");
        //$csv_content = $content;
        $tempFile = $file . '.tmp';
        $this->files->put($tempFile, $csv_content);
        $reader = Reader::createFromPath($tempFile);
        $reader->setDelimiter(';');
        $reader->setNewline("\r\n");
        $reader->setEnclosure('"');

        $input_bom = $reader->getInputBOM();

        $this->log("Encoding: $encoding | BOM: $input_bom");

        if ($input_bom === Reader::BOM_UTF16_LE || $input_bom === Reader::BOM_UTF16_BE) {
            $this->log("CONVERTING BOM!!!");
            $reader->appendStreamFilter('convert.iconv.UTF-16/UTF-8');
        }
        $results = $reader->fetchAll();
        $this->processRows($results);
        $this->files->delete($tempFile);
    }

    private function log($text, $message = null)
    {
        audit($text, 'AffiliateBackendRepository::' . $message);
        //audit_error($text, 'AffiliateBackendRepository::' . $message);
    }


    private function truncateCsvRows()
    {
        ImportAffiliateStock::where('affiliate_id', $this->affiliate_id)->delete();
    }


    private function processRows($rows)
    {
        $count = count($rows);
        $this->log("Processing [$count] rows...");

        DB::beginTransaction();
        $counter = 0;
        $this->truncateCsvRows();

        foreach ($rows as $row) {
            $this->log($row, 'IMPORT ROW');

            $sku = Str::upper(trim($row[0]));
            $qty = Str::upper(trim($row[1]));
            $product_id = null;
            $product_combination_id = null;
            $affiliate_id = $this->affiliate_id;

            /*$debug = compact('sku', 'qty', 'affiliate_id');
            $this->log($debug, 'DEBUG ROW');
            $tests = [
                'counter' => $counter == 0 ? 'true' : 'false',
                'sku' => Str::startsWith($sku, 'SKU') == '﻿SKU' ? 'true' : 'false',
                'qty' => $qty == 'QTY' ? 'true' : 'false',
            ];
            $this->log($tests, 'TESTS ROW');*/

            if ($counter == 0 and Str::startsWith($sku, 'SKU') and Str::startsWith($qty, 'QTY')) {
                $this->log($row, 'SKIP FIRST ROW');
            } elseif (Str::length($sku) > 0 and Str::length($qty) > 0) {

                try {
                    $errors = [];

                    $product = Product::where('sku', $sku)->select('id')->first();
                    if (is_null($product)) {
                        $combination = ProductCombination::where('sku', $sku)->select(['id', 'product_id'])->first();
                        if (is_null($combination)) {
                            $errors[] = "Prodotto con SKU '$sku' non trovato";
                        } else {
                            $product_combination_id = $combination->id;
                            $product_id = $combination->product_id;
                        }
                    } else {
                        $product_id = $product->id;
                    }
                    if (!is_numeric($qty)) {
                        $errors[] = "Il valore della colonna 'qty' deve essere numerico";
                    } elseif ($qty < 0) {
                        $errors[] = "Il valore della colonna 'qty' non può essere negativo";
                    }
                    $this->log($errors, 'ERRORS');

                } catch (\Exception $e) {
                    $errors[] = $e->getMessage();
                    $this->log($e->getMessage());
                    audit($e->getTraceAsString());
                }

                $params = empty($errors) ? null : ['errors' => $errors];
                $data = compact('sku', 'qty', 'params', 'affiliate_id', 'product_id', 'product_combination_id');

                ImportAffiliateStock::create($data);
                $this->log($data, 'IMPORTED DATA');

                unset($combination);
                unset($product);
                unset($row);
                unset($id);
                gc_collect_cycles();
            }

            $counter++;

            $this->log("Record imported ($counter/$count)");
        }

        DB::commit();

        unset($rows);
        gc_collect_cycles();
    }


    public function getAttributeOptionId()
    {
        $option = \AttributeOption::where('attribute_id', config('membership.attribute_id'))->where('uname', $this->affiliate_id)->first();
        return $option ? $option->id : null;
    }

    public function getAttributeId()
    {
        return config('membership.attribute_id');
    }

    /**
     * Make temp imported CSV records into real stocks records
     */
    public function commitStocks()
    {
        //get the temp rows
        $rows = ImportAffiliateStock::withAffiliate($this->affiliate_id)->get();

        //get existing stocks
        $stocks = AffiliateProductOuterStock::withAffiliate($this->affiliate_id)->get();

        $many = count($rows);
        $this->logger->log("-------------------------------");
        $this->logger->log("Committing stocks from CSV file");
        $this->logger->log("Found [$many] records to import...");

        //building up a cache array for stocks
        $cache = [];
        $cache_stocks = [];
        $cache_products = [];
        $cache_products_combinations = [];
        foreach ($stocks as $stock) {
            $key = "$stock->product_id|$stock->product_combination_id";
            $cache[$key] = $stock->quantity;
            $cache_stocks[$key] = $stock;
        }
        $this->log($cache, 'commitStocks cache');

        $membership_attribute_id = $this->getAttributeId();
        $membership_attribute_option_id = $this->getAttributeOptionId();

        DB::beginTransaction();
        //cycle the temp rows and determine what to update or store
        foreach ($rows as $row) {
            $key = "$row->product_id|$row->product_combination_id";
            $data = [
                'product_id' => $row->product_id,
                'product_combination_id' => $row->product_combination_id,
                'quantity' => $row->qty,
                'outer_id' => $this->affiliate_id
            ];
            $cache_products[] = $row->product_id;
            $cache_products_combinations[] = $row->product_combination_id;
            $this->log($data, 'commitStocks::data');
            if (isset($cache[$key])) {
                //cache hit, a similar record already exists
                $this->log($row, 'commitStocks::update');
                $old_quantity = $cache[$key];
                $this->logger->log("Updating product_id [$row->product_id] - combination_id [$row->product_combination_id], from quantity ($old_quantity) to ($row->qty)");
                $cache_stocks[$key]->fill($data);
                $cache_stocks[$key]->save();
            } else {
                //cache missed
                $this->log($row, 'commitStocks::insert');
                $this->logger->log("Inserting quantity ($row->qty) for product_id [$row->product_id] - combination_id [$row->product_combination_id]");
                AffiliateProductOuterStock::create($data);
            }
            //add the affiliate as an attribute
            $product = Product::find($row->product_id);
            if ($product) {
                $this->log("Adding attribute|option $membership_attribute_id|$membership_attribute_option_id to product $product->id");
                $product->addAttribute($membership_attribute_id, [$membership_attribute_option_id]);
            }

            unset($product);
            unset($data);
            unset($key);
            gc_collect_cycles();
        }
        DB::commit();

        //now remove all existing stocks for unfounded products/combinations
        $remove_combinations = AffiliateProductOuterStock::withAffiliate($this->affiliate_id)->whereNotNull('product_combination_id')->whereNotIn('product_combination_id', $cache_products_combinations)->get();
        $this->log($remove_combinations, 'commitStocks::remove_combinations');
        foreach ($remove_combinations as $stock) {
            $this->logger->log("Removing stocks ($stock->quantity) for product_id [$stock->product_id] - combination_id [$stock->product_combination_id]");
            $stock->delete();
        }

        $remove_products = AffiliateProductOuterStock::withAffiliate($this->affiliate_id)->whereNotIn('product_id', $cache_products)->get();
        $this->log($remove_products, 'commitStocks::remove_products');
        foreach ($remove_products as $stock) {
            $this->logger->log("Removing stocks ($stock->quantity) for product_id [$stock->product_id]");
            //add the affiliate as an attribute
            $product = Product::find($stock->product_id);
            if ($product) {
                $product->removeAttribute($membership_attribute_id, [$membership_attribute_option_id]);
            }
            $stock->delete();

            unset($product);
            unset($stock);
            gc_collect_cycles();
        }

        //remote temp csv records
        $this->truncateCsvRows();
    }


    public function sendTrackingUrl(\Order $order)
    {
        $email = \Email::getByCode('UPDATE_STATUS', $order->lang_id);
        $data = $order->getTokens();

        $data['shipment_link'] = $order->tracking_url;
        $data['shipment_css'] = 'block';
        $recipient = $data['customer_email'];
        if ($email)
            $email->send($data, $recipient);
    }
}