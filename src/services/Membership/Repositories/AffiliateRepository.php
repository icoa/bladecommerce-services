<?php

namespace services\Membership\Repositories;

use services\Membership\Models\Affiliate;
use services\Morellato\Repositories\Repository;
use Illuminate\Filesystem\Filesystem;
use DB;
use services\src\services\Membership\AffiliateLogger;
use Utils;
use Product;
use ProductCombination;
use App;
use Illuminate\Support\Str;
use Order;
use Email;
use OrderState;

class AffiliateRepository extends Repository
{
    /**
     * @var Filesystem
     */
    protected $files;

    /**
     * @var AffiliateLogger
     */
    protected $logger;


    protected $debug = true;

    protected $affiliate_id;

    function __construct(Filesystem $files, AffiliateLogger $logger)
    {
        $this->files = $files;
        $this->logger = $logger;
    }

    /**
     * @param $affiliate_id
     * @return $this
     */
    function setAffiliateId($affiliate_id)
    {
        $this->affiliate_id = $affiliate_id;
        $this->logger->setScope($this->affiliate_id);
        return $this;
    }

    function orderPlaced(Order $order)
    {
        if ($this->debug)
            audit($order->id, __METHOD__ . ' - ORDER ID');

        //if the order is a parent, that get all the childre and re-execute the placed event
        if ($order->isParent()) {
            $childrens = $order->getChildren();
            foreach ($childrens as $children) {
                $this->orderPlaced($children);
            }
            return;
        }

        if ($order->hasProductsWithAffiliate()) {
            if ($this->debug)
                audit($order->id, __METHOD__ . ' - HAS AFFILIATE PRODUCTS');

            $affiliate = $order->getAffiliate();
            if ($affiliate) {
                if ($this->debug)
                    audit($affiliate->toArray(), __METHOD__ . ' - AFFILIATE FOUND');

                $this->sendEmail($order, $affiliate);
            }
            //decrement quantities
            $products = $order->getProducts();
            foreach ($products as $p) {
                if ($p->affiliate_id > 0) {
                    $p->product->decrementAffiliateQty($p->product_quantity, $p->affiliate_id, $p->product_combination_id);
                }
            }
        }
    }


    private function sendEmail(Order $order, Affiliate $affiliate)
    {
        $data = $order->getTokens();
        $lang_id = $order->lang_id;
        $recipient = $affiliate->email_internal;

        $email = Email::getByCode('ORDER_CONFIRM', $lang_id);

        if ($this->debug)
            audit($data, __METHOD__ . ' - ORDER EMAIL DATA');

        if ($email)
            $email->send($data, $recipient);
    }

    /**
     * @param Order $order
     * @param $status
     */
    public function orderStatusChanged($order, $status)
    {
        $affiliate = $order->getAffiliate();
        if(is_null($affiliate))
            return;

        switch ($status) {
            case OrderState::STATUS_PENDING:
                $this->warnUsersAboutPendingStatus($order);
                break;
        }
    }

    /**
     * @param Order $order
     */
    private function warnUsersAboutPendingStatus(Order $order)
    {
        $reference = $order->reference;
        $customer = $order->getCustomer();
        $recipient = config('membership.warn_email');

        $email = Email::getByCode('PENDING_ORDER', $order->lang_id);
        $data = [
            'ORDER_NUMBER' => $reference,
            'LINK' => $order->backend_link,
            'AFFILIATE_NAME' => $affiliate->name,
        ];

        if ($this->debug)
            audit($data, __METHOD__ . ' - ORDER EMAIL DATA');

        if ($email)
            $email->send($data, $recipient);
    }
}