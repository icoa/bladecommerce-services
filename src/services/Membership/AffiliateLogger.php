<?php

namespace services\src\services\Membership;

use services\Morellato\Loggers\FileLogger;
use File;

class AffiliateLogger extends FileLogger
{

    protected $rotate = true;

    function setScope($affiliate_id)
    {
        $root = storage_path("logs/affiliazione/$affiliate_id/");
        if (!File::isDirectory($root)) {
            File::makeDirectory($root, 765, true);
        }
        $this->logFile = $root . (($this->rotate) ? 'log-' . date('Y-m-d') . '.log' : 'log.log');
        $this->errorFile = $root . (($this->rotate) ? 'error-' . date('Y-m-d') . '.log' : 'error.log');
    }
}