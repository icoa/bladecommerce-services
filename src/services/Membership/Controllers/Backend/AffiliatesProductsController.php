<?php

namespace services\Membership\Controllers\Backend;

use App\Models\User;
use BackendController;
use Input;
use Illuminate\Support\Str;
use services\Membership\Models\Affiliate;
use Json;
use services\Membership\Models\AffiliateProductOuterStock;
use services\Membership\Models\ImportAffiliateStock;
use URL;
use Redirect;
use services\Membership\Repositories\AffiliateBackendRepository;
use AdminAction;
use AdminUser;
use DB;

class AffiliatesProductsController extends BackendController
{

    public $component_id = 86;
    public $title = 'Gestione Catalogo Affiliato';
    public $page = 'affiliates_products.index';
    public $pageheader = 'Gestione Catalogo Affiliato';
    public $iconClass = 'font-columns';
    public $model = 'Product';
    public $themeLayout = 'affiliates';
    protected $js = 'affiliates_products.js';
    protected $rules = array();
    protected $friendly_names = array(
        'fileInput' => 'File CSV Giacenze',
    );
    /**
     * @var AffiliateBackendRepository
     */
    protected $repository;

    /**
     * @var User
     */
    protected $user;

    /**
     * @var Affiliate
     */
    protected $affiliate;

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
        $this->repository = app('services\Membership\Repositories\AffiliateBackendRepository');
        $this->user = AdminUser::get();
        $this->affiliate = $this->user->getAffiliate();
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Prodotti');
        $this->toFooter("js/echo/$this->js");
        $this->pageheader = 'Gestione Prodotti';
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTable()
    {
        $lang_id = isset($_REQUEST['lang_id']) ? $_REQUEST['lang_id'] : 'it';

        $affiliate_id = $this->affiliate->id;

        $model = $this->model;

        $queryBuilder = $model::leftJoin('products_lang', 'products.id', '=', 'products_lang.product_id')
            ->where('products_lang.lang_id', $lang_id)
            ->leftJoin('brands_lang', function ($join) use ($lang_id) {
                $join->on('brands_lang.brand_id', '=', 'products.brand_id')->on('brands_lang.lang_id', "=", 'products_lang.lang_id');
            })->leftJoin('collections_lang', function ($join) use ($lang_id) {
                $join->on('collections_lang.collection_id', '=', 'products.collection_id')->on('collections_lang.lang_id', "=", 'products_lang.lang_id');
            })->join('products_outer_stocks', function ($join) use ($affiliate_id) {
                $join->on('products_outer_stocks.product_id', '=', 'products.id')
                    ->where('products_outer_stocks.warehouse', '=', DB::raw('AF'))
                    ->where('products_outer_stocks.outer_id', '=', DB::raw($affiliate_id));
            });

        //$queryBuilder->groupBy("products.id");

        \Utils::watch();
        $pages = $queryBuilder->select(
            'products.id',
            'products_lang.name',
            'sku',
            'products.ean13',
            'brands_lang.name as brand_name',
            'collections_lang.name as collection_name',
            'products_lang.published',
            'is_out_of_production',
            'products_outer_stocks.quantity',
            'price',
            'products_lang.ldesc',
            'products_lang.lang_id',
            'products_outer_stocks.product_combination_id'
        );
        return \Datatables::of($pages)
            ->edit_column('price', function ($data) {
                return '<b>' . \Format::currency($data['price'], true) . '</b>';
            })
            ->edit_column('name', function ($data) {
                $obj = \Product::getObj($data['id']);
                $combination = $data['product_combination_id'] > 0 ? \ProductCombination::getObj($data['product_combination_id']) : null;
                $after = ($combination) ? "<br><span class='label label-warning'>$combination->name</span>" : null;
                $link = $obj->permalink;
                return "<strong><a href='$link' target='_blank'>{$data['name']}</a></strong>$after";
            })
            ->edit_column('sku', function ($data) {
                $combination = $data['product_combination_id'] > 0 ? \ProductCombination::getObj($data['product_combination_id']) : null;
                $text = ($combination) ? $combination->sku : $data['sku'];
                return "<strong>$text</strong>";
            })
            ->edit_column('ean13', function ($data) {
                $combination = $data['product_combination_id'] > 0 ? \ProductCombination::getObj($data['product_combination_id']) : null;
                $text = ($combination) ? $combination->ean13 : $data['ean13'];
                return "<strong>$text</strong>";
            })
            ->edit_column('published', function ($data) {
                $v = $data['published'];
                return "<img src=\"/media/admin/mark$v.gif\"/>";
            })
            ->edit_column('quantity', function ($data) {
                $css = ($data['quantity'] >= 1) ? 'label-success' : 'label-important';
                return '<span class="label ' . $css . '">' . $data['quantity'] . '</span>';
            })
            ->edit_column('is_out_of_production', function ($data) {
                $v = $data['is_out_of_production'];
                return "<img src=\"/media/admin/mark$v.gif\"/>";
            })
            ->remove_column('ldesc')
            ->remove_column('lang_id')
            ->remove_column('product_combination_id')
            ->edit_column('id', function ($data) {
                return $data['id'];
            })
            ->make();
    }

    public function getImport()
    {
        $this->page = 'affiliates_products.import';
        $this->addBreadcrumb('Importa file Giacenze');
        $this->toFooter("js/echo/$this->js");
        $view = [
            'controllerAction' => $this->action('postImport')
        ];
        return $this->render($view);
    }

    public function postImport()
    {
        $data = Input::all();
        $fileInput = Input::file('fileInput');
        $this->rules['fileInput'] = 'required|mimes:csv,txt';

        audit($data, __METHOD__ . '::data');
        audit($fileInput, __METHOD__ . '::file');

        $validator = $this->getValidator();
        if ($validator->fails()) {
            audit($validator->errors(), __METHOD__ . '::errors');
            $url = URL::action($this->action("getImport"));
            return Redirect::to($url)->withErrors($validator);
        }
        //import the csv into temp table
        $this->repository->importCsvStocksFile($fileInput->getPathname());

        $url = URL::action($this->action("getImportCheck"));
        return Redirect::to($url);
    }


    public function getImportCheck()
    {
        $this->page = 'affiliates_products.check';
        $this->addBreadcrumb('Controllo file Giacenze');
        $this->toFooter("js/echo/$this->js");
        $view = [
            'controllerAction' => $this->action('postConfirm'),
            'backAction' => URL::action($this->action('getImport')),
            'rows' => ImportAffiliateStock::currents()->get()
        ];
        return $this->render($view);
    }


    public function postConfirm()
    {
        $url = URL::action($this->action("getIndex"));
        try {
            $this->repository->commitStocks();
            return Redirect::to($url)->with('feedback', 'ok');
        } catch (\Exception $e) {
            audit($e->getMessage(), __METHOD__);
            return Redirect::to($url)->with('feedback', 'ko');
        }
    }

    public function getExport()
    {
        $rows = AffiliateProductOuterStock::currents()->get();

        $lines = ['sku;qty'];
        foreach ($rows as $row) {
            $product = \Product::getObj($row->product_id);
            $combination = $row->product_combination_id > 0 ? \ProductCombination::getObj($row->product_combination_id) : null;
            $data = [$combination ? $combination->sku : $product->sku, $row->quantity];
            $lines[] = implode(';', $data);
        }

        $file = storage_path() . '/temp.csv';

        \File::put($file, implode(PHP_EOL, $lines));

        $filename = "prodotti_affiliato_" . time() . ".csv";

        return \Response::download($file, $filename, ['content-type' => 'text/csv']);
    }


    protected function actions_default($params = [])
    {
        $create = ($this->action("getImport", TRUE));
        $export = ($this->action("getExport", TRUE));
        $actions = array(
            new AdminAction('Aggiorna disponibilità', $create, 'font-plus', 'btn-success', 'Aggiorna la giacenza dei tuoi prodotti', 'new'),
            new AdminAction('Esporta', $export, 'font-download', 'btn-warning', 'Esporta tutti i dati in un file CSV', 'export'),
            new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente', 'reload'),
        );
        return $actions;
    }

}
