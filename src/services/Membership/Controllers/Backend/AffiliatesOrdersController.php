<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 10/04/2018
 * Time: 09:59
 */

namespace services\Membership\Controllers\Backend;

use AdminUser;
use BackendController;
use AdminAction;
use DB;
use Mainframe;
use Input;
use Order;
use Json;
use Product;
use Format;
use services\Membership\Models\AffiliateProductOuterStock;
use services\Membership\Repositories\AffiliateBackendRepository;

class AffiliatesOrdersController extends BackendController
{

    public $component_id = 86;
    public $title = 'Gestione Ordini Affiliato';
    public $page = 'affiliates_orders.index';
    public $pageheader = 'Gestione Ordini Affiliato';
    public $iconClass = 'font-columns';
    public $model = 'Order';
    public $themeLayout = 'affiliates';
    protected $user;
    protected $affiliate;
    protected $js = 'affiliates_orders.js';

    protected $rules = array(
        'carrier_id' => 'required',
        'payment_id' => 'required',
        'customer_id' => 'required',
        'status' => 'required',
        'payment_status' => 'required',
        'shipping_address_id' => 'required',
        'order_date' => 'required',
    );

    protected $friendly_names = array(
        'carrier_id' => 'Metodo di spedizione',
        'payment_id' => 'Metodo di pagamento',
        'customer_id' => 'Cliente',
        'status' => 'Status ordine',
        'payment_status' => 'Status pagamento',
        'shipping_address_id' => 'Indirizzo di spedizione',
        'order_date' => 'Data ordine',
    );
    /**
     * @var AffiliateBackendRepository
     */
    protected $repository;

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
        $this->user = AdminUser::get();
        $this->affiliate = $this->user->getAffiliate();
        $this->repository = app('services\Membership\Repositories\AffiliateBackendRepository');
    }


    public function dashboard()
    {
        $this->page = 'affiliates_orders.dashboard';
        $this->addBreadcrumb('Dashboard Affiliato');
        $this->toFooter("js/echo/$this->js");
        $view = [
            'user' => $this->user,
            'affiliate' => $this->affiliate,
            'lastDate' => 'N/D',
            'stats' => $this->stats()
        ];
        return $this->render($view);
    }

    private function stats()
    {
        //orders
        $affiliate_id = $this->affiliate->id;
        $orders = \Order::where('affiliate_id', $affiliate_id)->count();

        //total product
        $products = AffiliateProductOuterStock::currents()->count();

        //products in sell
        $products_ids = AffiliateProductOuterStock::currents()->inStocks()->lists('product_id');
        $sell = Product::inStocks()->whereIn('id', $products_ids)->count();

        return compact('orders', 'products', 'sell');
    }


    public function getIndex()
    {
        $this->addBreadcrumb('Gestione Ordini Affiliato');
        $this->toFooter("js/echo/$this->js");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }


    public function getPreview($id)
    {
        $this->page = 'affiliates_orders.preview';
        $this->toFooter("js/echo/$this->js");
        $this->pageheader = 'Dettagli Ordine';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $obj->expand();
        $view['obj'] = $obj;
        \AdminTpl::setData('order', $obj);
        return $this->render($view);
    }

    public function getCreate()
    {
        return \Redirect::to('/admin/affiliazione/products/import');
    }

    public function getTable()
    {
        $model = $this->model;

        \Utils::watch();
        $affiliate_id = $this->affiliate->id;

        $pages = $model::leftJoin('customers', 'customer_id', '=', 'customers.id')
            ->where('affiliate_id', $affiliate_id)
            ->groupBy('orders.id')
            ->select(
                'orders.id',
                'orders.reference',
                DB::raw("CONCAT_WS(' ',firstname,lastname,company) as customer_name"),
                'orders.status',
                'orders.payment',
                'orders.payment_status',
                'orders.carrier_id',
                'orders.total_order',
                'orders.created_at',
                'orders.gift_box'
            );

        $order_states = Mainframe::selectOrderStates(false);
        $payment_states = Mainframe::selectPaymentStates(false);

        return \Datatables::of($pages)
            ->having_column(['customer_name'])
            ->edit_column('created_at', function ($data) {
                return \Format::datetime($data['created_at']);
            })
            ->edit_column('reference', function ($data) {
                $link = \URL::action($this->action("getPreview"), $data['id']);
                return "<strong><a href='$link'>{$data['reference']}</a></strong>";
            })
            ->edit_column('customer_id', function ($data) {
                $order = \Order::getObj($data['id']);
                $customer = $order->getCustomer();
                $s = ($customer) ? $customer->getName() : "ND";
                return "<strong>{$s}</strong>";
            })
            ->edit_column('customer_name', function ($data) {
                //$order = \Order::getObj($data['id']);
                $s = $data['customer_name'];
                return $s;
            })
            ->edit_column('gift_box', function ($data) {
                $order = \Order::getObj($data['id']);
                return $order->getActionsForAffiliate();
            })
            ->edit_column('carrier_id', function ($data) {
                $order = \Order::getObj($data['id']);
                $customer = $order->getCarrier();
                $s = ($customer) ? $customer->name : "ND";
                return "<strong>{$s}</strong>";
            })
            ->edit_column('status', function ($data) use ($order_states) {
                $order = \Order::getObj($data['id']);
                $status = $order->getStatus();
                $s = ($status) ? "<span class='label' style='background-color:{$status->color};font-size:13px' title='{$status->name}'><i class='{$status->icon}'></i></span> {$status->name}" : "ND";

                return $s;
            })
            ->edit_column('payment_status', function ($data) use ($payment_states) {
                $order = \Order::getObj($data['id']);
                $status = $order->getPaymentStatus();
                $s = ($status) ? "<span class='label' style='background-color:{$status->color};font-size:13px' title='{$status->name}'><i class='{$status->icon}'></i></span>" : "ND";

                return $s;
            })
            ->edit_column('total_order', function ($data) {
                return "<strong>" . \Format::currency($data['total_order'], true) . "</strong>";
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    protected function actions_default($params = [])
    {
        $create = ($this->action("getCreate", TRUE));
        $export = ($this->action("getExport", TRUE));
        $actions = array(
            new AdminAction('Aggiorna disponibilità', $create, 'font-plus', 'btn-success', 'Aggiorna la giacenza dei tuoi prodotti', 'new'),
            new AdminAction('Esporta', $export, 'font-download', 'btn-warning', 'Esporta tutti i dati in un file CSV', 'export'),
            new AdminAction('Ricarica', 'javascript:EchoTable.reloadTable();', 'font-refresh', '', 'Aggiorna la tabella corrente', 'reload'),
        );
        return $actions;
    }


    protected function actions_create($params = [])
    {
        $index = \URL::action($this->action("getIndex"));

        $actions = array(
            new AdminAction('Torna indietro', $index, 'font-arrow-left', 'btn-default', 'Torna indietro alla pagina principale del Componente', 'back'),
        );

        return $actions;
    }


    function postSetStatus($order_id)
    {
        $status_id = \Input::get('status_id', null);
        $order = \Input::get('order', 0);
        $obj = Order::getObj($order_id);
        $success = true;
        if ($order == 1) {
            $success = $obj->updateStatus($status_id);
            $msg = $success ? "Aggiornamento status effettuato con successo" : "Aggiornamento status non eseguito";
            if ($success) {
                \Session::set('orderLastStatus', $status_id);
                $obj->handleStatusUpdateEmail();
            }
        } else {
            $success = $obj->updatePaymentStatus($status_id);
            $msg = $success ? "Aggiornamento status pagamento effettuato con successo" : "Aggiornamento status pagamento non eseguito";
            if ($success) {
                \Session::set('orderLastPaymentStatus', $status_id);
                $obj->handlePaymentStatusUpdateEmail();
            }
        }


        return Json::encode(compact('success', 'msg'));
    }

    function postRma($id)
    {
        $response = ['success' => false, 'msg' => "Si è verificato un problema interno; contattare il servizio di assistenza"];
        $order = Order::getObj($id);
        if ($order) {
            try {
                $order->payment_status = \Config::get('negoziando.payment_status_refunded');
                $order->status = \Config::get('negoziando.order_status_refunded');
                $order->save();

                $order->handlePaymentStatusUpdateEmail();
                $response['success'] = true;
                $response['msg'] = "Il rimborso dell'ordine <b>$order->reference</b> è stato richiesto con successo";
            } catch (Exception $e) {
                $response['msg'] = $e->getMessage();
            }
        }
        return Response::json($response);
    }


    function postSetTracking($order_id)
    {
        $model = \Order::getObj($order_id);
        $success = true;
        $url = null;
        if ($model) {
            $url = $model->tracking_url = Input::get('url');
            $model->save();
            $success = true;
        }
        return \Response::json(compact('success', 'url'));
    }

    function postSendTracking($order_id)
    {
        $model = \Order::getObj($order_id);
        $success = false;
        $msg = null;
        $url = null;
        if ($model) {
            try {
                $this->repository->sendTrackingUrl($model);
                $success = true;
            } catch (\Exception $e) {
                $msg = $e->getMessage();
            }
        }
        return \Response::json(compact('success', 'msg'));
    }

    function getExport()
    {
        $affiliate_id = $this->affiliate->id;

        $rows = \Order::leftJoin('customers', 'customer_id', '=', 'customers.id')
            ->where('affiliate_id', $affiliate_id)
            ->groupBy('orders.id')->get();

        $lines = ['N. ordine|Data|Nome|Cognome|Email|ID Cliente|Citta|Provincia|Codice Fiscale|Campagna|Coupon|Stato ordine|Pagamento|Stato pagamento|Spedizione|Tot Ordine|Sku prodotto|Nome Prodotto|Prezzo listino|Prezzo base|Prezzo finale|Prodotto disponibile|Numero prodotti|Brand|Genere'];
        foreach ($rows as $row) {

            $customer = $row->getCustomer();
            $address = $row->getShippingAddress();
            $campaignObj = $row->getCampaign();
            $statusObj = $row->getStatus();
            $paymentStatusObj = $row->getPaymentStatus();
            $carrierObj = $row->getCarrier();
            $paymentObj = $row->getPayment();
            $products = \OrderDetail::where('order_id', $row->id)->get();

            $firstname = ($customer) ? $customer->firstname : '';
            $lastname = ($customer) ? $customer->lastname : '';
            $email = ($customer) ? $customer->email : '';
            $customer_id = $row->customer_id;
            if ($customer AND $customer->people_id == 2) {
                $firstname = $customer->name;
                $lastname = '';
            }
            $status = $statusObj ? $statusObj->name : '';
            $paymentStatus = $paymentStatusObj ? $paymentStatusObj->name : '';
            $carrier = $carrierObj ? $carrierObj->name : '';
            $payment = $paymentObj ? $paymentObj->name : '';
            $campaign = $campaignObj ? $campaignObj->name : '';


            foreach ($products as $p) {

                $product = Product::getObj($p->product_id);
                if ($product) {

                    $product->setFullData();

                    $data = [];
                    $data[] = $row->reference;
                    $data[] = \Format::lang_date($row->created_at);
                    $data[] = $firstname;
                    $data[] = $lastname;
                    $data[] = $email;
                    $data[] = $customer_id;
                    $data[] = $address->city;
                    $data[] = $address->stateName;
                    $data[] = $address->cf;
                    $data[] = $campaign;
                    $data[] = $row->coupon_code;
                    $data[] = $status;
                    $data[] = $payment;
                    $data[] = $paymentStatus;
                    $data[] = $carrier;
                    $data[] = Format::currency($row->total_order);
                    //Sku prodotto|Nome Prodotto|Marca Prodotto|Categoria I livello Prodotto|Categoria II livello Prodotto|Prezzo listino|Prezzo base|Prezzo finale|Prodotto disponibile|Numero prodotti
                    $data[] = $p->product_reference;
                    $data[] = $p->product_name;
                    $data[] = Format::currency($product->sell_price_wt);
                    $data[] = Format::currency($p->product_sell_price_wt);
                    $data[] = Format::currency($p->cart_price_tax_incl);
                    $data[] = $p->product_quantity_in_stock;
                    $data[] = $p->product_quantity;
                    $data[] = $product->brand_name;
                    $data[] = $product->gender;

                    $lines[] = implode("|", $data);

                }

            }

        }

        $file = storage_path() . '/temp.csv';

        \File::put($file, implode(PHP_EOL, $lines));

        $filename = "ordini_affiliato_" . time() . ".csv";

        return \Response::download($file, $filename, ['content-type' => 'text/csv']);
    }
}