<?php

namespace services\Membership\Controllers;

use BackendController;
use Input;
use Illuminate\Support\Str;
use services\Membership\Models\Affiliate;
use Json;

class AffiliatesAddressController extends BackendController
{

    public $component_id = 83;
    public $title = 'Gestione Affiliati';
    public $page = 'affiliates.index';
    public $pageheader = 'Gestione Affiliati';
    public $iconClass = 'font-columns';
    public $model = 'MorellatoShop';
    protected $rules = array(
        'cd_neg' => 'required|unique:mi_shops,cd_neg',
        'name' => 'required',
        'email' => 'email',
        'address' => 'required',
        'cap' => 'required',
        'city' => 'required',
        'state' => 'required',
    );
    protected $friendly_names = array(
        'name' => 'Nome',
        'cd_neg' => 'Codice punto vendita',
        'email' => 'Email',
        'address' => 'Indirizzo',
        'cap' => 'C.A.P.',
        'city' => 'Città',
        'state' => 'Provincia',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }


    public function getTable($id)
    {
        $model = Affiliate::find($id);
        $html = ($model) ? $model->loadShopsTable() : null;
        $success = true;
        return Json::encode(compact("success", "html"));
    }

    public function getForm($id, $address_id)
    {
        $model = Affiliate::find($id);
        $html = ($model) ? $model->loadShopsForm($address_id) : null;
        $success = true;
        return Json::encode(compact("success", "html"));
    }

    public function postStore($id)
    {
        $data = Input::all();
        audit($data, __METHOD__);
        audit_watch();
        if ($data['shop_id'] > 0) {
            $this->rules['cd_neg'] .= ',' . $data['shop_id'];
        }
        $validator = $this->getValidator();
        $success = true;
        $errors = null;
        if ($validator->fails()) {
            $success = false;
            $items = [];
            foreach ($validator->errors()->all() as $error) {
                $items[] = $error;
            }
            $errors = implode('<br>', $items);
        } else {
            $conditions = ['id' => $data['shop_id']];
            unset($data['shop_id']);
            $model = \MorellatoShop::updateOrCreate($conditions, $data);
            $model->resolveAddress();
        }
        return Json::encode(compact("success", 'errors'));
    }

    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        $this->_prepare();
    }

    function _prepare()
    {
        if (count($_POST) == 0) {
            return;
        }

        $data = Input::all();
        $code = Input::get('cd_neg');
        $code = Str::slug(Str::upper($code));
        $data['cd_neg'] = $code;
        Input::replace($data);
    }

}
