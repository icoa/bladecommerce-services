<?php

namespace services\Membership\Controllers;

use BaseController;
use Input;
use Illuminate\Support\Str;
use ProductCombination;
use Product;
use Theme;
use Json;

class AffiliatesRestController extends BaseController
{
    function getCombination($combination_id)
    {
        $combination = ProductCombination::getObj($combination_id);
        $product = Product::getObj($combination->product_id);
        $product->combination = $combination;
        $affiliates = $product->getAffiliates();
        $obj = $product;
        $html = Theme::partial('product.membership.affiliates', compact('affiliates', 'obj'));
        return Json::encode(compact('html'));
    }

}
