<?php

namespace services\Membership\Controllers;

use BackendController;
use Input;
use Illuminate\Support\Str;

class AffiliatesController extends BackendController
{

    public $component_id = 86;
    public $title = 'Gestione Affiliati';
    public $page = 'affiliates.index';
    public $pageheader = 'Gestione Affiliati';
    public $iconClass = 'font-columns';
    public $model = 'services\Membership\Models\Affiliate';
    protected $rules = array(
        'code' => 'required|unique:affiliates,code',
        'name' => 'required',
        'company' => 'required',
        'email' => 'required|email|unique:users,email',
        'email_customer' => 'email',
        'email_internal' => 'required|email',
        'password' => 'required|min:4',
        'password_confirmation' => 'same:password',
        'address' => 'required',
        'cap' => 'required',
        'city' => 'required',
        'state' => 'required',
    );
    protected $friendly_names = array(
        'name' => 'Nome',
        'code' => 'Codice affiliato',
        'company' => 'Ragione sociale',
        'email' => 'Email per login',
        'email_customer' => 'Email assistenza clienti',
        'email_internal' => 'Email comunicazioni interne',
        'address' => 'Indirizzo',
        'cap' => 'C.A.P.',
        'city' => 'Città',
        'state' => 'Provincia',
    );

    function __construct()
    {
        parent::__construct();
        $this->className = __CLASS__;
    }

    public function getIndex()
    {
        $this->addBreadcrumb('Elenco Affiliati');
        $this->toFooter("js/echo/affiliates.js?v=4");
        $view = array();
        $this->toolbar();
        return $this->render($view);
    }

    public function getTrash()
    {
        $this->toFooter("js/echo/affiliates.js");
        $this->page = 'affiliates.trash';
        $this->pageheader = 'Cestino Affiliati';
        $this->iconClass = 'font-trash';
        $this->addBreadcrumb($this->pageheader);
        $view = array();
        $this->toolbar('trash');
        return $this->render($view);
    }

    public function getCreate()
    {
        $this->toFooter("js/echo/affiliates.js");
        $this->page = 'affiliates.create';
        $this->pageheader = 'Nuovo Affiliato';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        return $this->render($view);
    }

    public function getEdit($id)
    {
        $this->toFooter("js/echo/affiliates.js");
        $this->page = 'affiliates.create';
        $this->pageheader = 'Modifica Affiliato';
        $this->addBreadcrumb($this->pageheader);
        $this->toolbar('create');
        $view = array();
        $model = $this->model;
        $obj = $model::find($id);
        $view['obj'] = $obj;
        return $this->render($view);
    }

    public function getTable()
    {
        \Utils::watch();
        $model = $this->model;

        $pages = $model::select('id', 'code', 'name', 'piva', 'tel_customer', 'email_customer', 'tel_internal', 'email_internal', 'active', 'created_at');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::date($data['created_at']);
            })
            ->edit_column('name', function ($data) {
                $link = \URL::action($this->action("getEdit"), $data['id']);
                return "<strong><a href='$link'>{$data['name']}</a></strong>";
            })
            ->edit_column('email_internal', function ($data) {
                $link = $data['email_internal'];
                return "<a href='mailto:$link'>{$link}</a>";
            })
            ->edit_column('email_customer', function ($data) {
                $link = $data['email_customer'];
                return "<a href='mailto:$link'>{$link}</a>";
            })
            ->edit_column('active', function ($data) {
                return $data['active'] == 1 ? '<span class="label label-success">Sì</span>' : '<span class="label label-danger">No</span>';
            })
            ->add_column('actions', function ($data) {
                return $this->column_actions($data);
            })
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }


    public function getTabletrash()
    {
        $lang_id = \Core::getLang();
        $model = $this->model;
        $pages = $model::onlyTrashed()->leftJoin('countries_lang', 'affiliates.country_id', '=', 'countries_lang.country_id')
            ->where('lang_id', $lang_id)
            ->select('affiliates.id', 'affiliates.name as state', 'countries_lang.name as country', 'affiliates.active', 'affiliates.deleted_at', 'affiliates.created_at', 'countries_lang.lang_id');

        return \Datatables::of($pages)
            ->edit_column('created_at', function ($data) {
                return \Format::date($data['created_at']);
            })
            ->edit_column('deleted_at', function ($data) {
                return \Format::date($data['deleted_at']);
            })
            ->edit_column('state', function ($data) {
                return "<strong>{$data['country']}</strong>";
            })
            ->add_column('actions', function ($data) {
                return $this->column_trash_actions($data);
            })
            ->remove_column('lang_id')
            ->edit_column('id', function ($data) {
                return '<label class="pointer"><input type="checkbox" name="ids[]" class="style" value="' . $data['id'] . '" /> ' . $data['id'] . "</label>";
            })
            ->make();
    }

    function _before_create()
    {
        $this->_prepare();
    }

    function _before_update($model)
    {
        unset($this->rules['password']);
        if ($model->user) {
            $user_id = $model->user->id;
            $this->rules['email'] = 'required|email|unique:users,email,' . $user_id;
        }
        $this->rules['code'] .= ',' . $model->id;
        $this->_prepare();
    }

    function _prepare()
    {
        $data = \Input::all();
        if (count($data) == 0) {
            return;
        }
        $data['code'] = Str::upper(Str::slug(trim($data['code'])));
        $data['address'] = Str::upper(trim($data['address']));
        $data['cap'] = Str::upper(trim($data['cap']));
        $data['city'] = Str::upper(trim($data['city']));
        $data['state'] = Str::upper(trim($data['state']));
        \Input::replace($data);
    }


    function _after_update($model)
    {
        $model->createUser(\Input::get('password'));
        $model->makeDefaultShop();
        $model->makeAttribute();
    }

    function _after_create($model)
    {
        $model->createUser(\Input::get('password'));
        $model->makeDefaultShop();
        $model->makeAttribute();
    }

}
