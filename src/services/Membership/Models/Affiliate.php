<?php

namespace services\Membership\Models;

use SingleModel;
use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;
use App\Models\User;
use Sentry;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use MorellatoShop;
use Illuminate\Support\Str;
use Theme;

/**
 * services\Membership\Models\Affiliate
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property string $company
 * @property string $email
 * @property string $piva
 * @property string $tel_customer
 * @property string $tel_internal
 * @property string $email_customer
 * @property string $email_internal
 * @property string $cover
 * @property string $address
 * @property string $cap
 * @property string $city
 * @property string $state
 * @property boolean $active
 * @property integer $page_id
 * @property string $fees_sap_code
 * @property integer $created_by
 * @property \Carbon\Carbon $created_at
 * @property integer $updated_by
 * @property \Carbon\Carbon $updated_at
 * @property integer $deleted_by
 * @property \Carbon\Carbon $deleted_at
 * @property-read \App\Models\User $user
 * @property-read \Illuminate\Database\Eloquent\Collection|\MorellatoShop[] $shops
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereCompany($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereEmail($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate wherePiva($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereTelCustomer($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereTelInternal($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereEmailCustomer($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereEmailInternal($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereCover($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereAddress($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereCap($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereCity($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereState($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereActive($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate wherePageId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereFeesSapCode($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\Affiliate whereDeletedAt($value)
 */
class Affiliate extends SingleModel implements PresentableInterface
{
    /* LARAVEL PROPERTIES */
    protected $table = 'affiliates';
    protected $guarded = array();
    use SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $timestamps = true;


    public $db_fields = array(
        "code",
        "company",
        "name",
        "email",
        "piva",
        "tel_customer",
        "tel_internal",
        "email_customer",
        "email_internal",
        "cover",
        "active",
        "address",
        "cap",
        "city",
        "state",
        "page_id",
        "fees_sap_code",
    );

    /**
     * Return a created presenter.
     *
     * @return Robbo\Presenter\Presenter
     */
    public function getPresenter()
    {
        return new AffiliatePresenter($this);
    }

    function user()
    {
        return $this->hasOne('App\Models\User', 'affiliate_id');
    }

    function shops()
    {
        return $this->hasMany('MorellatoShop', 'affiliate_id');
    }

    function setCodeAttribute($value)
    {
        $this->attributes['code'] = Str::upper(Str::slug($value));
    }


    static function options()
    {
        $rows = Affiliate::orderBy('code')->select('id', 'name')->get();
        $data = ['' => 'Seleziona...'];
        foreach ($rows as $row) {
            $data[$row->id] = $row->name;
        }
        return $data;
    }

    static function codes()
    {
        $rows = Affiliate::orderBy('code')->select('id', 'name', 'code')->get();
        $data = ['' => 'Seleziona...'];
        foreach ($rows as $row) {
            $data[$row->code] = $row->name;
        }
        return $data;
    }


    static function getByCode($code)
    {
        return Affiliate::where('code', trim($code))->first();
    }

    /**
     * Try to create a backend user automatically
     */
    function createUser($password)
    {
        if ($this->email != '') {
            $user = User::where('email', $this->email)->first();
            if (is_null($user)) {
                //create the user
                $data = [
                    'affiliate_id' => $this->id,
                    'email' => $this->email,
                    'activated' => true,
                    'first_name' => $this->name,
                    'last_name' => '',
                    'password' => $password,
                ];

                audit($data, __METHOD__ . '::data');
                $user = Sentry::createUser($data);

                // Find the group using the group id
                $adminGroup = Sentry::findGroupById(2);

                // Assign the group to the user
                $user->addGroup($adminGroup);
            } else {
                //if the user exists update email+password
                $data = [
                    'email' => $this->email,
                    'first_name' => $this->name
                ];
                if (strlen($password) > 0) {
                    $data['password'] = $password;
                }
                audit($data, __METHOD__ . '::data');
                $user->fill($data);
                $user->save();
            }
        }
    }


    function makeDefaultShop()
    {
        $shop = MorellatoShop::where('affiliate_id', $this->id)->first();
        $model = ($shop) ? $shop : new MorellatoShop();
        $data = [
            'name' => $this->name,
            'cd_neg' => $this->code,
            'address' => $this->address,
            'cap' => $this->cap,
            'city' => $this->city,
            'state' => $this->state,
            'tel' => $this->tel_customer,
            'email' => $this->email_customer,
            'affiliate_id' => $this->id,
        ];
        audit($data, __METHOD__ . '::data');
        $model->fill($data);
        $model->save();
        $model->resolveAddress();
    }


    function makeAttribute()
    {
        $nav_id = $this->id;
        $code = $this->id;
        $option = $this->name;
        $attribute = \Attribute::find(config('membership.attribute_id'));
        if ($attribute) {
            $attribute->addSimpleOption($option, $code, $nav_id);
        }
    }


    function loadShopsTable()
    {
        $rows = $this->shops;
        return Theme::partial('affiliates.shops_index', compact('rows'));
    }

    function loadShopsForm($id)
    {
        $model = $this;
        return Theme::partial('affiliates.shops_create', compact('model', 'id'));
    }


}

class AffiliatePresenter extends Presenter
{


}
