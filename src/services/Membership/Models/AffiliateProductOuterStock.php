<?php

namespace services\Membership\Models;

use services\Membership\Models\Affiliate;
use AdminUser;

/**
 * services\Membership\Models\AffiliateProductOuterStock
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $product_combination_id
 * @property string $warehouse
 * @property integer $outer_id
 * @property integer $quantity
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \services\Membership\Models\Affiliate $affiliate
 * @property-read \Product $product
 * @property-read \ProductCombination $combination
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\AffiliateProductOuterStock whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\AffiliateProductOuterStock whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\AffiliateProductOuterStock whereProductCombinationId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\AffiliateProductOuterStock whereWarehouse($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\AffiliateProductOuterStock whereOuterId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\AffiliateProductOuterStock whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\AffiliateProductOuterStock whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\AffiliateProductOuterStock whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\AffiliateProductOuterStock whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\AffiliateProductOuterStock whereUpdatedAt($value)
 * @method static \services\Membership\Models\AffiliateProductOuterStock currents()
 * @method static \services\Membership\Models\AffiliateProductOuterStock withAffiliate($affiliate_id)
 * @method static \services\Membership\Models\ProductOuterStock withProduct($product_id)
 * @method static \services\Membership\Models\ProductOuterStock withProductCombination($product_combination_id = null)
 * @method static \services\Membership\Models\ProductOuterStock inStocks()
 */
class AffiliateProductOuterStock extends ProductOuterStock
{
    protected $warehouse_type = 'AF';

    function affiliate()
    {
        return $this->belongsTo('services\Membership\Models\Affiliate', 'outer_id');
    }

    function scopeCurrents($query)
    {
        $self = new static;
        $affiliate_id = AdminUser::get()->affiliate_id;
        return $query->where('warehouse', $self->warehouse_type)->where('outer_id', $affiliate_id);
    }

    function scopeWithAffiliate($query, $affiliate_id)
    {
        $self = new static;
        return $query->where('warehouse', $self->warehouse_type)->where('outer_id', $affiliate_id);
    }
}