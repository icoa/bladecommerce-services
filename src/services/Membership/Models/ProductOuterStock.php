<?php

namespace services\Membership\Models;

use services\Traits\HasParam;
use SingleModel;
use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;
use App\Models\User;
use Sentry;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use AdminUser;
use Illuminate\Support\Str;
use Theme;

/**
 * services\Membership\Models\ProductOuterStock
 *
 * @property integer $id
 * @property integer $product_id
 * @property integer $product_combination_id
 * @property string $warehouse
 * @property integer $outer_id
 * @property integer $quantity
 * @property integer $created_by
 * @property integer $updated_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \Product $product
 * @property-read \ProductCombination $combination
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ProductOuterStock whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ProductOuterStock whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ProductOuterStock whereProductCombinationId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ProductOuterStock whereWarehouse($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ProductOuterStock whereOuterId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ProductOuterStock whereQuantity($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ProductOuterStock whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ProductOuterStock whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ProductOuterStock whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ProductOuterStock whereUpdatedAt($value)
 * @method static \services\Membership\Models\ProductOuterStock withProduct($product_id)
 * @method static \services\Membership\Models\ProductOuterStock withProductCombination($product_combination_id = null)
 * @method static \services\Membership\Models\ProductOuterStock inStocks()
 */
class ProductOuterStock extends SingleModel
{

    /* LARAVEL PROPERTIES */
    protected $table = 'products_outer_stocks';
    protected $guarded = array();
    public $timestamps = true;
    protected $warehouse_type = 'XX';


    public $db_fields = array(
        "warehouse",
        "product_id",
        "product_combination_id",
        "quantity",
    );

    function product()
    {
        return $this->belongsTo('Product', 'product_id');
    }

    function combination()
    {
        return $this->belongsTo('ProductCombination', 'product_combination_id');
    }


    function scopeWithProduct($query, $product_id)
    {
        return $query->where('product_id', $product_id);
    }

    function scopeWithProductCombination($query, $product_combination_id = null)
    {
        return ($product_combination_id) ? $query->where('product_combination_id', $product_combination_id) : $query;
    }

    function scopeInStocks($query)
    {
        return $query->where('quantity', '>', 0);
    }

    /**
     * @param array $attributes
     * @return static
     */
    static function create(array $attributes)
    {
        $self = new static; //OBJECT INTANTIATION
        $attributes['warehouse'] = $self->warehouse_type;
        return parent::create($attributes);
    }
}