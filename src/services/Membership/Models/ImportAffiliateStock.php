<?php

namespace services\Membership\Models;

use services\Traits\HasParam;
use SingleModel;
use Robbo\Presenter\Presenter;
use Robbo\Presenter\PresentableInterface;
use App\Models\User;
use Sentry;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use AdminUser;
use Illuminate\Support\Str;
use Theme;

/**
 * services\Membership\Models\ImportAffiliateStock
 *
 * @property integer $id
 * @property string $sku
 * @property string $qty
 * @property integer $product_id
 * @property integer $product_combination_id
 * @property integer $affiliate_id
 * @property string $params
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property-read \Product $product
 * @property-read \ProductCombination $combination
 * @property-read mixed $product_name
 * @property-read mixed $errors
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ImportAffiliateStock whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ImportAffiliateStock whereSku($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ImportAffiliateStock whereQty($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ImportAffiliateStock whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ImportAffiliateStock whereProductCombinationId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ImportAffiliateStock whereAffiliateId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ImportAffiliateStock whereParams($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ImportAffiliateStock whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ImportAffiliateStock whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ImportAffiliateStock whereDeletedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ImportAffiliateStock whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ImportAffiliateStock whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Membership\Models\ImportAffiliateStock whereDeletedAt($value)
 * @method static \services\Membership\Models\ImportAffiliateStock currents()
 * @method static \services\Membership\Models\ImportAffiliateStock withAffiliate($affiliate_id)
 */
class ImportAffiliateStock extends SingleModel
{
    use HasParam;

    /* LARAVEL PROPERTIES */
    protected $table = 'import_affiliates_stocks';
    protected $guarded = array();
    use SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $timestamps = true;


    public $db_fields = array(
        "sku",
        "product_id",
        "product_combination_id",
        "qty",
    );

    function product()
    {
        return $this->belongsTo('Product', 'product_id');
    }

    function combination()
    {
        return $this->belongsTo('ProductCombination', 'product_combination_id');
    }

    function setSkuAttribute($value)
    {
        $this->attributes['sku'] = Str::upper(trim($value));
    }

    function getProductNameAttribute()
    {
        if ($this->product_id <= 0)
            return null;

        $product = \Product::getObj($this->product_id);
        return ($product) ? $product->name : null;
    }

    function getErrorsAttribute()
    {
        $html = null;
        if ($this->hasParam('errors')) {
            $errors = $this->getParam('errors');
            $html = implode('<br>', $errors);
        }
        return $html;
    }

    function scopeCurrents($query)
    {
        $affiliate_id = AdminUser::get()->affiliate_id;
        return $this->scopeWithAffiliate($query, $affiliate_id);
    }

    function scopeWithAffiliate($query, $affiliate_id)
    {
        return $query->where('affiliate_id', $affiliate_id);
    }
}