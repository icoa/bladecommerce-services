<?php namespace services;


use Product;
use SimpleXMLElement;
use Cfg;
use DB;
use Format;
use Currency;



class ZanoxFeed
{

    public $xml;
    protected $channel;
    protected $config;
    protected $lang;
    protected $locale;
    protected $currency;
    protected $country;

    protected $qs;
    protected $carrier_id;
    protected $zone_id;
    protected $debug = false;
    protected $cache = [];


    function __construct($lang, $locale)
    {
        $this->config = ZanoxConfig::load();
        \Utils::log($this->config,__CLASS__);
        $this->lang = $lang;
        $this->locale = $locale;
        $this->country = \Country::where('iso_code', $locale)->first();
        $this->currency = ($this->country) ? \Currency::getObj($this->country->currency_id) : \Core::getDefaultCurrency();
        \Core::setLang($lang);

        if(isset($this->config->{"custom_qs_".$locale})){
            $this->qs = trim($this->config->{"custom_qs_".$locale});
        }
        if(isset($this->config->{"export_shipping_carrier_".$locale})){
            $this->carrier_id = trim($this->config->{"export_shipping_carrier_".$locale});
        }
        if(isset($this->config->{"export_zone_".$locale})){
            $this->zone_id = trim($this->config->{"export_zone_".$locale});
        }
        if(\Input::get('debug',0) == 1){
            $this->debug = true;
        }

    }

    protected function DbToXml($string)
    {
        return htmlspecialchars($string, ENT_NOQUOTES, 'UTF-8');
        /*return htmlspecialchars(mb_convert_encoding($string, 'UTF-8', 'Windows-1252'), ENT_NOQUOTES, 'Windows-1252');
        return mb_convert_encoding(htmlspecialchars($string, ENT_NOQUOTES, 'Windows-1252'), 'UTF-8', 'Windows-1252');*/
    }

    public function getParam($param, $default = null)
    {
        if (isset($this->config->$param)) return $this->config->$param;
        return $default;
    }

    public function export()
    {
        ini_set('max_execution_time', 3600);
        $config = $this->config;
        \Utils::log($config, __METHOD__);
        $lang = $this->lang;
        $locale = $this->locale;
        $min_id = (int)$this->getParam('export_min_id', 0);
        $min_price = (float)$this->getParam('export_min_price', 0);
        $export_not_ean = (int)$this->getParam('export_not_ean', 1);
        $export_not_published = (int)$this->getParam('export_not_published', 1);
        $export_fp = (int)$this->getParam('export_fp', 1);


        $queryBuilder = Product::rows($lang)->where('id', '>', $min_id)->where('visibility', '!=', 'none');
        if ($min_price > 0) {
            $queryBuilder->where('price', '>=', $min_price);
        }
        if ($export_not_ean == 0) {
            $queryBuilder->where('ean13', '<>', '');
        }
        if ($export_not_published == 0) {
            $queryBuilder->where('published', 1);
        }
        if ($export_fp == 0) {
            $queryBuilder->where('is_out_of_production', 0);
        }

        if ($this->debug) $queryBuilder->take(30);

        $rows = $queryBuilder->orderBy('id', 'desc')->get();

        $this->xml = '<?xml version="1.0" encoding="UTF-8"?><m4n><data>';


        foreach ($rows as $product) {
            $product->setFullData();
            $this->createProductNode($product);
        }

        return $this->xml .'</data></m4n>';
    }



    private function createProductNode($product)
    {

        $export_sdesc = $this->getParam('export_sdesc', 0);
        $export_ldesc = $this->getParam('export_ldesc', 0);
        $export_attributes_desc = $this->getParam('export_attributes_desc', 1);
        $export_color = $this->getParam('export_color', 0);
        $export_color_attributes = $this->getParam('export_color_attributes', []);
        $export_size = $this->getParam('export_size', 0);
        $export_size_attributes = $this->getParam('export_size_attributes', []);
        $export_material = $this->getParam('export_material', 0);
        $export_materiale_attributes = $this->getParam('export_materiale_attributes', []);
        $export_gender = $this->getParam('export_gender', 0);
        $imageTypeThumb = $this->getParam('image_thumb', 'thumb');
        $imageTypeDefault = $this->getParam('image_default', 'default');
        $imageTypeLarge = $this->getParam('image_large', 'zoom');
        $availability = $this->getParam('availability', 'real');
        if ($availability == 'fake') {
            $product->availabilityStatus = 'in_stock';
        }


        $this->xml .= '<record>';
        $this->addNode('id', $product->id);
        $this->addNode('title', $product->name, true);
        $this->addNode('name', $product->name, true);
        $this->addNode('product_name', $product->name, true);

        $short_description = '';
        $long_description = '';
        if ($export_sdesc) {
            $short_description .= $product->getShortDescription() . " ";
        }
        if ($export_ldesc AND $product->ldesc != '') {
            $long_description .= $product->ldesc . " ";
        }
        if ($export_attributes_desc AND $product->attributes != '') {
            $short_description .= $product->attributes . " ";
        }
        $short_description = trim($short_description);
        $short_description = \Utils::stripTags($short_description);
        if ($short_description != '') {
            $this->addNode('description', $short_description, true);
        }
        $long_description = trim($long_description);
        $long_description = \Utils::stripTags($long_description);
        if ($long_description != '') {
            $this->addNode('long_description', $long_description, true);
        }
        $link = $product->link_absolute;
        if($this->qs){
            $link .= "?".$this->qs;
        }
        $this->addNode('url', $link);
        $this->addNode('product_url', $link);
        $this->addNode('product_condition', $product->condition);
        $this->addNode('stock_status', strtr($product->availabilityStatus,'_',' '));

        if($product->availabilityStatus == 'preorder'){
            $availability_days = (int) $product->availability_days;
            if($availability_days > 0){
                $date = date("c", strtotime("today +$availability_days days"));
                $this->addNode('delivery_time', $date);
            }
        }

        $product->qty = $product->qty <= 0 ? 0 : (int)$product->qty;

        $this->addNode('stock_amount', $product->qty );



        if($product->price_final_raw < $product->price_official_raw){
            $price = Format::currency($product->price_official_raw, false, $this->currency->id, true);
            $this->addNode('old_price', $price);

            //Savings percent
            //Savings absolute
            if($product->price_saving_raw > 0){
                $price = Format::currency($product->price_saving_raw, false, $this->currency->id, true);
                $this->addNode('savings_absolute', $price);

                //price_official_raw : 100 = price_final_raw : X
                // X =
                if(isset($product->price_offer)){
                    $price = str_replace(['-','%',' '],'',$product->price_offer);
                    $this->addNode('savings_percent', $price);
                }
            }
        }

        $price = Format::currency($product->price_final_raw, false, $this->currency->id, true);
        $this->addNode('price', $price);

        $this->addNode('currency', $this->currency->iso_code);

        $defaultImg = isset($product->{$imageTypeThumb . "Img"}) ? $product->{$imageTypeThumb . "Img"} : null;
        if($defaultImg AND $defaultImg != '' AND $defaultImg != '/media/no.gif'){
            $this->addNode('small_image_url', $defaultImg, true);
        }

        $defaultImg = isset($product->{$imageTypeDefault . "Img"}) ? $product->{$imageTypeDefault . "Img"} : null;
        if($defaultImg AND $defaultImg != '' AND $defaultImg != '/media/no.gif'){
            $this->addNode('medium_image_url', $defaultImg, true);
        }

        $defaultImg = isset($product->{$imageTypeLarge . "Img"}) ? $product->{$imageTypeLarge . "Img"} : null;
        if($defaultImg AND $defaultImg != '' AND $defaultImg != '/media/no.gif'){
            $this->addNode('large_image_url', $defaultImg, true);
        }

        $images = $product->getImages();

        $i = 0;
        foreach ($images as $img) {
            $defaultImg = isset($img->{$imageTypeDefault . "Img"}) ? $img->{$imageTypeDefault . "Img"} : $img->defaultImg;

            if ($img->cover == 0 AND $defaultImg != '' AND $defaultImg != '/media/no.gif') {
                $i++;
                $this->addNode('additional_image_'.$i, $defaultImg, true);
            }
        }

        $categories = [];
        \Helper::setCategoriesReverse($product->default_category_id, $categories);
        if (count($categories)) {
            $categories = array_reverse($categories);
            $data = [];
            foreach ($categories as $c) {
                $cat = \Category::getObj($c, $this->lang);
                if ($cat) $data[] = $cat->name;
            }
            if (count($data)) {
                $this->addNode('category_path', (implode(' / ', $data)), true);
            }
            if(isset($data[0])){
                $this->addNode('category', $data[0], true);
            }
            if(isset($data[1])){
                $this->addNode('subcategory', $data[1], true);
            }
            if(isset($data[3])){
                $this->addNode('third_category', $data[2], true);
            }
        }

        if (isset($product->brand_name)) {
            $this->addNode('manufacturer', $product->brand_name, true);
        }

        $ean = (trim($product->ean13) != '') ? str_pad(trim($product->ean13),13,'0',STR_PAD_LEFT) : false;
        $upc = (trim($product->upc) != '') ? str_pad(trim($product->upc),12,'0',STR_PAD_LEFT) : false;
        $sku = (trim($product->sku) != '') ? trim($product->sku) : false;

        if($ean)$this->addNode('ean', $ean);
        if($sku)$this->addNode('gtin', $sku);

        $attribute_ids = $product->getProductAttributesIds();

        if ($export_color and count($export_color_attributes)) {
            $string = $this->getAttributes($product->id,$attribute_ids,$export_color_attributes);
            if($string != '')$this->addNode('color', $string, true);
        }
        if ($export_material and count($export_materiale_attributes)) {
            $string = $this->getAttributes($product->id,$attribute_ids,$export_materiale_attributes);
            if($string != '')$this->addNode('material', $string, true);
        }
        if ($export_size and count($export_size_attributes)) {
            $string = $this->getAttributes($product->id,$attribute_ids,$export_size_attributes);
            if($string != '')$this->addNode('size', $string, true);
        }
        if($export_gender){
            $string = $this->getGenderString($product->id,$attribute_ids);
            if($string and $string != ''){
                if($string != '')$this->addNode('gender', $string);
            }
        }

        $deliveryTime = $product->qty > 0 ? '24H' : '16 days';
        $this->addNode('delivery_time', $deliveryTime);
        if($this->carrier_id AND $this->zone_id){
            $shipping_cost = $product->getShippingCostForCarrier($this->carrier_id,$this->zone_id);
            $shipping_cost = Format::currency($shipping_cost, false, $this->currency->id, true);
            $this->addNode('shipping_cost', $shipping_cost);
        }




        $this->xml .= '</record>';
    }


    private function getAttributes($product_id, $attribute_ids, $matches)
    {
        $ids = array_intersect($attribute_ids, $matches);
        $string = '';
        if (count($ids)) {
            $data = [];
            $attr = implode(",", $ids);
            $query = "SELECT name FROM attributes_options_lang
            WHERE lang_id='$this->lang' AND attribute_option_id IN
            (select attribute_val from products_attributes where product_id=$product_id and attribute_id in ($attr))";
            $rows = DB::select($query);
            if (count($rows)) {
                foreach ($rows as $row) {
                    $data[] = $row->name == '' ? $row->name : $row->name;
                }
            }
            if (count($data)) {
                foreach ($data as $name) {
                    if (\Str::length($name) <= 40 AND \Str::length($string) <= 100 AND !\Str::contains($string,$name) AND \Str::lower($name) != 'canvas') {
                        $string .= $name . "/";
                    }
                }
                $string = rtrim($string, "/");
            }
        }

        return $string;
    }


    private function getGoogleCategory($id)
    {
        if (isset($this->cache['cat' . $id])) {
            return $this->cache['cat' . $id];
        }
        $obj = DB::table('google_categories')->where('category_id', $id)->where('locale', $this->locale)->first();
        if ($obj) {
            $this->cache['cat' . $id] = $obj->sdesc;
            return $obj->sdesc;
        }
        return null;
    }


    private function getGenderString($product_id,$attributes){
        $gender_id = $this->getGenderId();
        if(in_array($gender_id,$attributes)){
            $query = "SELECT uname FROM attributes_options WHERE id IN (select attribute_val FROM products_attributes where product_id=$product_id AND attribute_id=$gender_id)";
            $rows = DB::select($query);
            if(count($rows)){
                foreach($rows as $row){
                    switch($row->uname){
                        case 'U':
                            return 'unisex';
                            break;
                        case 'M':
                            return 'male';
                            break;
                        case 'F':
                        case 'W':
                            return 'female';
                            break;
                    }
                }
            }
        }
        return null;
    }


    private function getGenderId(){
        if(isset($this->cache['gender'])){
            return $this->cache['gender'];
        }
        $id = \Attribute::where('code','gender')->pluck('id');
        $this->cache['gender'] = $id;
        return $id;
    }


    private function addNode($prop, $value, $isCdata = false, $mandatory = false, $isBoolean = false)
    {
        if ($isBoolean) {
            $value = ($value === false or $value === 0) ? "false" : $value;
            $value = ($value === true or $value === 1) ? "true" : $value;
        }
        if (($value and \Str::length($value) > 0) or $mandatory) {
            if ($isCdata)$value = '<![CDATA['.$value.']]>';
            else $value = \Utils::xml_entities($value);
            $this->xml .= "<$prop>$value</$prop>";
        }
    }

    private function addAttribute(SimpleXMLElement $node, $prop, $value, $isBoolean = false, $mandatory = false)
    {
        if ($isBoolean) {
            $value = ($value === false or $value === 0) ? "false" : $value;
            $value = ($value === true or $value === 1) ? "true" : $value;
        }

        if (($value and strlen($value) > 0) or $mandatory) {
            $value = $this->DbToXml($value);
            $node->addAttribute($prop, $value);
        }
    }
}