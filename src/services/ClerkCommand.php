<?php

namespace services;

use Illuminate\Console\Command;
use services\Morellato\CsvImporters\AnagraficaImporter;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config, DB;

class ClerkCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'clerk:exec';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build the json for Clerk';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->line("Executing ClerkExec...");
        $lang = $this->option('lang');
        $locale = $this->option('locale');
        if($lang == '')
            $lang = 'it';

        if($locale == '')
            $locale = 'IT';

        $locale = strtoupper($locale);
        $cf = new ClerkFeed($lang,$locale);
        $cf->setSave(true);
        $cf->setDebug($this->option('debug'));
        $cf->setConsole($this);
        //$json = $cf->export();
        $cf->saveFile();
    }




    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(

        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('lang', null, InputOption::VALUE_OPTIONAL, 'The language specified for some tasks', null),
            array('locale', null, InputOption::VALUE_OPTIONAL, 'The locale specified for some tasks', null),
            array('debug', false, InputOption::VALUE_OPTIONAL, 'Debug mode', null),
        );
    }

}