<?php namespace services;

/*<!-- Google Code per il tag di remarketing -->
<script type="text/javascript">
var google_tag_params = {
ecomm_prodid: 'REPLACE_WITH_VALUE',
ecomm_pagetype: 'REPLACE_WITH_VALUE',
ecomm_totalvalue: 'REPLACE_WITH_VALUE',
};
</script>


<script type="text/javascript">
 <![CDATA[
var google_conversion_id = 1014974690;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
 ]]>
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1014974690/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
*/

use Product;
use Cfg;
use DB;
use Format;
use Currency;


class GoogleRemarketingLib
{

    protected $debug = false;
    protected $lang;
    protected $currency;
    protected $options = [];
    protected $tagParams = [];
    protected $javascript = '';
    protected $conversion_id;
    protected $async = false;
    protected $active = false;

    function __construct($lang)
    {
        $this->lang = $lang;
        $this->config = GoogleRemarketingConfig::load();
        $this->active = $this->getLangParam('active',0) == 0 ? false : true;
        if($this->active === false)return;
        $this->debug = $this->getParam('debug', true);
        //\Utils::log($this->config,__CLASS__);

        $this->currency = ($this->getParam('currency')) ? \Currency::getObj($this->getParam('currency')) : \Core::getDefaultCurrency();
        \Core::setLang($lang);
        $this->meta = [];
        $this->append('<!-- Google Code remarketing tag -->');
        $this->conversion_id = $this->getLangParam('conversion_id');
        $this->addOption('google_conversion_id', $this->conversion_id);
        $this->addOption('google_custom_params', 'window.google_tag_params');
        $this->addOption('google_remarketing_only', $this->getLangParam('google_remarketing_only'));
        $this->addOption('google_conversion_label', $this->getLangParam('google_conversion_label'), true);
        $this->addOption('google_conversion_language', $this->getLangParam('google_conversion_language'), true);
        $this->addOption('google_conversion_format', $this->getLangParam('google_conversion_format'), true);
        $this->addOption('google_conversion_color', $this->getLangParam('google_conversion_color'), true);
        if ($this->currency) {
            $this->addOption('google_conversion_currency', $this->currency->iso_code, true);
        }
        $this->async = \Cfg::get('PAGINATION_TYPE','static') == 'ajax';

    }

    public function setDebug($mode){
        $this->debug = $mode;
    }

    public function getParam($param, $default = null)
    {
        if (isset($this->config->$param)) return $this->config->$param;
        return $default;
    }

    public function getLangParam($param, $default = null)
    {
        if (isset($this->config->{$param . "_" . $this->lang})) return $this->config->{$param . "_" . $this->lang};
        return $default;
    }

    function addOption($key, $value, $string = false)
    {
        if (is_array($value) OR is_object($value)) {
            $value = '[' . implode(',', $value) . ']';
        }
        if (trim($value) == '' or $value == null) return;
        $value = ($string) ? "\"$value\"" : $value;
        $this->options[$key] = $value;
    }

    function getOption($key, $default = null)
    {
        if (isset($this->options[$key])) return $this->options[$key];
        return $default;
    }

    function addTagParam($key, $value)
    {
        if($key == 'prodid' AND is_int($value)){
            $value = (string)$value;
        }
        if($key == 'prodid' AND is_array($value)){
            foreach($value as $k => $v){
                $value[$k] = (string)$v;
            }
        }
        if($key == 'category'){
            $value = str_replace('&','and',$value);
        }
        $this->tagParams[$key] = $value;
    }

    function getTagParam($key, $default = null)
    {
        if (isset($this->tagParams[$key])) return $this->options[$key];
        return $default;
    }

    function append($str)
    {
        $this->javascript .= $str . PHP_EOL;
    }

    function render($pagetype)
    {
        if($this->active === false)return "";
        //\Utils::log($pagetype, __METHOD__);
        if ($this->conversion_id == null OR (int)$this->conversion_id == 0) return null;
        $this->addTagParam('pagetype', $pagetype);
        if(!isset($this->tagParams['prodid'])){
            $this->addTagParam('prodid', '');
            $this->addTagParam('totalvalue', '');
        }

        $code = '';
        if ($this->async == true) {
            $code =  $this->render_async();
        }else{
            $code = $this->render_sync();
        }

        if($custom_script_global = $this->getLangParam('custom_script_global')){
            $code .= PHP_EOL . $custom_script_global;
        }

        if($pagetype == 'purchase' AND $custom_script_confirmation = $this->getLangParam('custom_script_confirmation')){
            $code .= PHP_EOL . $custom_script_confirmation;
        }

        return $code;
    }


    private function render_sync()
    {
        $data = [];
        foreach ($this->tagParams as $tagKey => $tagValue) {
            $key = 'ecomm_' . $tagKey;
            $data[$key] = $tagValue;
        }
        //\Utils::log($this->tagParams, __METHOD__);

        try {
            $json = $this->toJson($data);
            $this->append('<script type="text/javascript">');
            $this->append("var google_tag_params = $json;");
            $this->append("</script>");
            $this->append('<script type="text/javascript">');
            $this->append('/* <![CDATA[ */');
            foreach ($this->options as $optKey => $optValue) {
                $this->append("var $optKey = $optValue;");
            }
            $this->append('/* ]]> */');
            $this->append("</script>");
            if ($this->debug == true) {
                $this->append("<!-- DEBUG MODE: ON <script type=\"text/javascript\" src=\"//www.googleadservices.com/pagead/conversion.js\"></script> -->");
            } else {
                $this->append("<script type=\"text/javascript\" src=\"//www.googleadservices.com/pagead/conversion.js\" charset=\"utf-8\"></script>");
            }


            $noScript = $this->noScript();
            if ($this->debug == true) {
                $this->append("<!-- DEBUG MODE: ON " . $noScript . " -->");
            } else {
                $this->append($noScript);
            }
            $this->append('<!-- EOF :: Google Code remarketing tag -->');

            return $this->javascript;
        } catch (\Exception $e) {
            \Utils::error($e->getMessage(), __METHOD__);
            \Utils::error($e->getTraceAsString(), __METHOD__);
        }
        return null;
    }


    private function render_async()
    {
        $data = [];
        foreach ($this->tagParams as $tagKey => $tagValue) {
            $key = 'ecomm_' . $tagKey;
            $data[$key] = $tagValue;
        }
        //\Utils::log($this->tagParams, __METHOD__);

        try {
            if ($this->debug == true) {
                $this->append("<!-- DEBUG MODE: ON <script type=\"text/javascript\" src=\"//www.googleadservices.com/pagead/conversion_async.js\"></script> -->");
            } else {
                $this->append("<script type=\"text/javascript\" src=\"//www.googleadservices.com/pagead/conversion_async.js\" charset=\"utf-8\"></script>");
            }
            $main = new \stdClass();
            $main->google_conversion_id = $this->conversion_id;

            foreach ($this->options as $optKey => $optValue) {
                if($optValue == 'true' OR $optValue == 'false'){
                    $optValue = filter_var($optValue, FILTER_VALIDATE_BOOLEAN);
                }
                if($optValue[0] == '"'){
                    $optValue = str_replace('"','',$optValue);
                }
                $main->$optKey = $optValue;
            }
            $main->google_custom_params = $data;
            $json = $this->toJson($main);

            if ($this->debug == true) {
                $js = <<<JS
<script type="text/javascript">
/* <![CDATA[ */
/*DEBUG MODE: ON var google_remarketing_tag = $json; window.google_trackConversion(google_remarketing_tag);
if(window.jQuery){
    jQuery(document).ready(f);
}else{
    defer_functions.push(f);
}
*/
//]]>
var f = function(){
    Shared.listeners({'afterCatalogLoad':GoogleRemarketing.afterCatalogLoad});
};
</script>
JS;
            } else {
                $js = <<<JS
<script type="text/javascript">
/* <![CDATA[ */
var google_remarketing_tag = $json; window.google_trackConversion(google_remarketing_tag);
var f = function(){
    Shared.listeners({'afterCatalogLoad':GoogleRemarketing.afterCatalogLoad});
};
if(window.jQuery){
    jQuery(document).ready(f);
}else{
    defer_functions.push(f);
}
//]]>
</script>
JS;
            }
            $this->append($js);

            $noScript = $this->noScript();
            if ($this->debug == true) {
                $this->append("<!-- DEBUG MODE: ON " . $noScript . " -->");
            } else {
                $this->append($noScript);
            }

            $this->append('<!-- EOF :: Google Code remarketing tag -->');

            return $this->javascript;
        } catch (\Exception $e) {
            \Utils::error($e->getMessage(), __METHOD__);
            \Utils::error($e->getTraceAsString(), __METHOD__);
        }
        return null;
    }


    private function noScript()
    {
        $noScriptParams = [
            'value' => $this->getTagParam('google_conversion_value', 0),
            'guid' => 'ON',
            'script' => 0,
            'currency_code' => $this->getOption('google_conversion_currency'),
            'label' => $this->getOption('google_conversion_label'),
        ];
        $_params = '?';
        $_params .= http_build_query($noScriptParams, null, '&amp;');
        $_params = str_replace('%22', '', $_params);
        $noScript = <<<NOSCRIPT
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/$this->conversion_id/$_params"/>
</div>
</noscript>
NOSCRIPT;
        return $noScript;
    }


    private function toJson($object)
    {
        return json_encode($object, JSON_UNESCAPED_SLASHES);
    }


}