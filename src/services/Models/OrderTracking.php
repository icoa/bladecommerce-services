<?php

namespace services\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use OrderHistory;
use OrderState;
use services\Traits\HasParam;
use SingleModel;
use Order;
use Exception;
use Illuminate\Database\Eloquent\Builder;

/**
 * services\Models\OrderTracking
 *
 * @property integer $id 
 * @property integer $order_id 
 * @property string $order_reference 
 * @property string $order_parent_reference 
 * @property integer $order_status_id 
 * @property string $order_status_label 
 * @property string $source 
 * @property string $source_label 
 * @property integer $reason_id 
 * @property string $reason_label 
 * @property string $availability_shop 
 * @property string $delivery_shop 
 * @property string $feedback 
 * @property string $sap_delivery_id 
 * @property string $sap_sales_id 
 * @property string $sap_invoice_id 
 * @property string $sap_withdrawal_id 
 * @property string $params 
 * @property integer $created_by 
 * @property integer $updated_by 
 * @property \Carbon\Carbon $created_at 
 * @property \Carbon\Carbon $updated_at 
 * @property-read Order $order 
 * @property-read mixed $created_at_html 
 * @property-read mixed $updated_at_html 
 * @property-read mixed $source_html 
 * @property-read mixed $reason_html 
 * @property-read mixed $status_html 
 * @property-read mixed $feedback_html 
 * @property-read mixed $user_html 
 * @property-read \$this->getBlameableModel() $createdBy 
 * @property-read \$this->getBlameableModel() $updatedBy 
 * @property-read \$this->getBlameableModel() $deletedBy 
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereOrderReference($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereOrderParentReference($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereOrderStatusId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereOrderStatusLabel($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereSource($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereSourceLabel($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereReasonId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereReasonLabel($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereAvailabilityShop($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereDeliveryShop($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereFeedback($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereSapDeliveryId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereSapSalesId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereSapInvoiceId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereSapWithdrawalId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereParams($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderTracking whereUpdatedAt($value)
 * @method static \services\Models\OrderTracking withSource($source)
 * @method static \services\Models\OrderTracking withOrder($order_id)
 */
class OrderTracking extends SingleModel
{
    const SOURCE_USER = 'USR';
    const SOURCE_INTERNAL = 'INT';
    const SOURCE_WEB_SERVICE_2 = 'WS2';
    const SOURCE_GLS = 'GLS';

    const REASON_DEFAULT = 1;
    const REASON_PRODUCT_UNAVAILABLE = 2;
    const REASON_CUSTOMER_REQUEST = 3;
    const REASON_ORDER_MERGED = 4;
    const REASON_ORDER_DOUBLE = 5;
    const REASON_ORDER_UNPAID = 6;
    const REASON_ORDER_EMPTY = 7;
    const REASON_ORDER_REFUNDED = 8;
    const REASON_ORDER_REJECTED = 9;
    const REASON_ORDER_REPLACED = 10;
    const REASON_WS_NEW_NOTFOUND = 11;
    const REASON_WS_NEW_SILENT = 12;
    const REASON_WS_CANCELLED_UNPICKED = 13;
    const REASON_WS_CANCELLED_NOTFOUND = 14;
    const REASON_ORDER_WRONG_SLIPS = 15;
    const REASON_ORDER_REPLY_SAP = 16;
    const REASON_OTHER = 17;
    const REASON_SHOP_CONFIRM = 18;

    /* LARAVEL PROPERTIES */
    protected $table = 'order_trackings';
    protected $guarded = array('*');


    public $db_fields = array(
        'order_id',
        'order_reference',
        'order_parent_reference',
        'order_status_id',
        'order_status_label',
        'source',
        'source_label',
        'reason_id',
        'reason_label',
        'availability_shop',
        'delivery_shop',
        'availability_mode',
        'total_order',
        'feedback',
        'created_at',
        'created_by',
        'updated_at',
        'updated_by',
    );

    use HasParam;

    /**
     * @return BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    /**
     * @param Builder $builder
     * @param $source
     */
    public function scopeWithSource(Builder $builder, $source)
    {
        $builder->where('source', $source);
    }

    /**
     * @param Builder $builder
     * @param $order_id
     */
    public function scopeWithOrder(Builder $builder, $order_id)
    {
        $builder->where('order_id', $order_id);
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return (int)$this->getAttribute('order_id');
    }

    /**
     * @param int $order_id
     */
    public function setOrderId($order_id)
    {
        $this->setAttribute('order_id', $order_id);
    }

    /**
     * @param Order $order
     * @return $this
     */
    public function setOrder(Order $order)
    {
        $order_parent_reference = $order->isChild() ? $order->getParent()->reference : null;
        $source_shop = $order->getAvailabilityShop();
        $target_shop = $order->getDeliveryStore();
        $slip = $order->getMorellatoSlip();

        $data = [
            'order_id' => $order->id,
            'order_reference' => $order->reference,
            'order_parent_reference' => $order_parent_reference,
            'availability_shop' => $source_shop ? $source_shop->code : null,
            'delivery_shop' => $target_shop ? $target_shop->code : null,
            'sap_delivery_id' => $slip ? $slip->delivery_id : null,
            'sap_sales_id' => $slip ? $slip->sales_id : null,
            'sap_invoice_id' => $slip ? $slip->invoice_id : null,
            'sap_withdrawal_id' => $slip ? $slip->withdrawal_id : null,
            'availability_mode' => $order->availability_mode,
            'total_order' => $order->total_order,
        ];

        if ($slip) {
            $array = $slip->toArray();
            if (isset($array['order']))
                unset($array['order']);

            $this->setParam('sap_slip', $array);
        }

        $this->setAttributes($data);
        return $this;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->getAttribute('source');
    }

    /**
     * @param string $source
     */
    public function setType($source)
    {
        $this->setAttribute('source', $source);
    }

    /**
     * @param $val
     * @return null|string
     */
    private function carbonDateTime($val)
    {
        if (!$val instanceof Carbon) {
            if ($val == '0000-00-00 00:00:00')
                return null;

            $val = Carbon::parse($val);
        }
        return $val->format('d-m-Y H:i:s');
    }

    /**
     * @return null|string
     */
    public function getCreatedAtHtmlAttribute()
    {
        return $this->carbonDateTime($this->created_at);
    }

    /**
     * @return null|string
     */
    public function getUpdatedAtHtmlAttribute()
    {
        return $this->carbonDateTime($this->updated_at);
    }

    /**
     * @return array
     */
    public static function getSourceLabels()
    {
        return [
            self::SOURCE_GLS => 'GLS Tracking API',
            self::SOURCE_INTERNAL => 'Automatismo interno',
            self::SOURCE_USER => 'Operazione manuale',
            self::SOURCE_WEB_SERVICE_2 => 'Web Service 2',
        ];
    }

    /**
     * @param $source
     * @return string
     */
    public static function sourceToString($source)
    {
        $array = self::getSourceLabels();
        return array_key_exists($source, $array) ? $array[$source] : 'SOURCE_UNKNOWN';
    }

    /**
     * @param $source
     * @return bool
     */
    public static function isSourceValid($source)
    {
        return array_key_exists($source, self::getSourceLabels());
    }

    /**
     * @return array
     */
    public static function getReasonLabels()
    {
        return [
            self::REASON_DEFAULT => null,
            self::REASON_PRODUCT_UNAVAILABLE => 'Prodotto non disponibile',
            self::REASON_CUSTOMER_REQUEST => 'Richiesto dal cliente',
            self::REASON_ORDER_MERGED => 'Ordine unificato',
            self::REASON_ORDER_DOUBLE => 'Ordine doppio',
            self::REASON_ORDER_UNPAID => 'Ordine non pagato',
            self::REASON_ORDER_EMPTY => 'Ordine vuoto',
            self::REASON_ORDER_REFUNDED => 'Annullato reso',
            self::REASON_ORDER_REJECTED => 'Annullato respinto',
            self::REASON_ORDER_REPLACED => 'Annullato sostituito',
            self::REASON_WS_NEW_NOTFOUND => 'Ordine Riconfigurato NON Trovato',
            self::REASON_WS_NEW_SILENT => 'Ordine Riconfigurato Silente',
            self::REASON_WS_CANCELLED_UNPICKED => 'Ordine annullato per NON ritirato',
            self::REASON_WS_CANCELLED_NOTFOUND => 'Ordine annullato per NON trovato',
            self::REASON_ORDER_WRONG_SLIPS => 'Ordine con distinte errate di SAP (vendita, fattura, consegna, ritiro)',
            self::REASON_ORDER_REPLY_SAP => 'Ordine richiede reinvio forzato a SAP',
            self::REASON_OTHER => 'Altro',
            self::REASON_SHOP_CONFIRM => 'Attesa conferma negozio',
        ];
    }

    /**
     * @param $reason
     * @return string
     */
    public static function reasonToString($reason)
    {
        $array = self::getReasonLabels();
        return array_key_exists($reason, $array) ? $array[$reason] : 'REASON_UNKNOWN';
    }

    /**
     * @param $reason
     * @return bool
     */
    public static function isReasonValid($reason)
    {
        if (is_array($reason) and isset($reason['reason_id'])) {
            $reason = $reason['reason_id'];
        }
        return array_key_exists($reason, self::getReasonLabels());
    }

    /**
     * @return array
     */
    public static function getReasonForOrderCancellation()
    {
        $labels = self::getReasonLabels();

        $valid_reasons = [
            self::REASON_PRODUCT_UNAVAILABLE,
            self::REASON_CUSTOMER_REQUEST,
            self::REASON_ORDER_MERGED,
            self::REASON_ORDER_DOUBLE,
            self::REASON_ORDER_UNPAID,
            self::REASON_ORDER_EMPTY,
            self::REASON_ORDER_REFUNDED,
            self::REASON_ORDER_REJECTED,
            self::REASON_ORDER_REPLACED,
        ];

        foreach ($labels as $label_key => $label_value) {
            if (!in_array($label_key, $valid_reasons, true)) {
                unset($labels[$label_key]);
            }
        }

        return $labels;
    }

    /**
     * @return array
     */
    public static function getReasonForOrderIncrement()
    {
        $labels = self::getReasonLabels();

        $valid_reasons = [
            self::REASON_ORDER_WRONG_SLIPS,
            self::REASON_ORDER_REPLY_SAP,
            self::REASON_OTHER,
        ];

        foreach ($labels as $label_key => $label_value) {
            if (!in_array($label_key, $valid_reasons, true)) {
                unset($labels[$label_key]);
            }
        }

        return $labels;
    }


    /**
     * @param $source
     * @param null $reason
     * @return array
     */
    public static function by($source, $reason = null)
    {
        if (self::isSourceValid($source)) {
            if (null === $reason) {
                return [$source];
            }
            if (self::isReasonValid($reason)) {
                return [$source, $reason];
            }
        }
        return [];
    }


    /**
     * @param null $reason
     * @return array
     */
    public static function byInternal($reason = null)
    {
        return self::by(self::SOURCE_INTERNAL, $reason);
    }

    /**
     * @param null $reason
     * @return array
     */
    public static function byUser($reason = null)
    {
        return self::by(self::SOURCE_USER, $reason);
    }

    /**
     * @param null $reason
     * @return array
     */
    public static function byGls($reason = null)
    {
        return self::by(self::SOURCE_GLS, $reason);
    }

    /**
     * @param null $reason
     * @return array
     */
    public static function byWs2($reason = null)
    {
        return self::by(self::SOURCE_WEB_SERVICE_2, $reason);
    }

    /**
     * @param OrderHistory $orderHistory
     * @return static
     */
    public static function fromOrderHistory(OrderHistory $orderHistory)
    {
        $model = new static;
        $model->setOrder($orderHistory->order);

        $model->order_status_id = $orderHistory->status_id;
        $model->order_status_label = OrderState::statusToString($orderHistory->status_id);

        $saved_reason = $orderHistory->getReason();
        if ($saved_reason) {
            $model->feedback = trim(strip_tags($saved_reason));
        }

        $tracking = $orderHistory->getTrackingBy();
        $source = self::SOURCE_INTERNAL;
        $reason = self::REASON_DEFAULT;
        //audit($tracking, '$tracking', __METHOD__);

        if (!empty($tracking)) {
            $size = count($tracking);
            if ($size === 1) {
                list($source) = $tracking;
            }
            if ($size === 2) {
                list($source, $reason) = $tracking;
            }

            //in case $reason is an array, probably from frontend
            if (is_array($reason)) {
                $model->feedback = trim(strip_tags($reason['feedback']));
                $reason = $reason['reason_id'];
            }
        }

        if ($source) {
            $model->source = $source;
            $model->source_label = self::sourceToString($source);
        }
        if ($reason) {
            $model->reason_id = $reason;
            $model->reason_label = self::reasonToString($reason);
        }

        $array = $orderHistory->toArray();
        if (isset($array['order']))
            unset($array['order']);
        $model->setParam('order_history', $array);

        //audit($model->toArray(), __METHOD__);

        return $model;
    }

    /**
     * @param Order $order
     * @param $status_id
     * @param $tracking
     * @return static
     */
    public static function fromOrderTracking(Order $order, $status_id, $tracking){
        $model = new static;
        $model->setOrder($order);

        $model->order_status_id = $status_id;
        $model->order_status_label = OrderState::statusToString($status_id);

        $source = self::SOURCE_INTERNAL;
        $reason = self::REASON_DEFAULT;

        $size = count($tracking);
        if ($size === 1) {
            list($source) = $tracking;
        }
        if ($size === 2) {
            list($source, $reason) = $tracking;
        }

        $model->source = $source;
        $model->source_label = self::sourceToString($source);

        $model->reason_id = $reason;
        $model->reason_label = self::reasonToString($reason);

        return $model;
    }


    /**
     * @return null|string
     */
    public function getSourceHtmlAttribute()
    {
        $str = null;

        $key = $this->getSource();
        $labels = self::getSourceLabels();
        $label = isset($labels[$key]) ? $labels[$key] : null;

        switch ($key) {

            case self::SOURCE_GLS:
                $str = "<span class='label label-important'>$label</span>";
                break;

            case self::SOURCE_INTERNAL:
                $str = "<span class='label label-success'>$label</span>";
                break;

            case self::SOURCE_WEB_SERVICE_2:
                $str = "<span class='label label-inverse'>$label</span>";
                break;

            case self::SOURCE_USER:
                $str = "<span class='label label-info'>$label</span>";
                break;

        }

        return $str;
    }


    /**
     * @return null|string
     */
    public function getReasonHtmlAttribute()
    {
        $str = null;

        $key = $this->reason_id;
        $labels = self::getReasonLabels();
        $label = isset($labels[$key]) ? $labels[$key] : null;

        $order_cancellation_reasons = self::getReasonForOrderCancellation();
        $order_increment_reasons = self::getReasonForOrderIncrement();

        //audit(compact('key', 'order_cancellation_reasons', 'order_increment_reasons'));

        $choice = 1;
        if (array_key_exists($key, $order_cancellation_reasons)) {
            $choice = 2;
        }
        if (array_key_exists($key, $order_increment_reasons)) {
            $choice = 3;
        }

        switch ($choice) {

            case 1:
            default:
                $str = "<span class='label label-info'>$label</span>";
                break;

            case 2:
                $str = "<span class='label label-important'>$label</span>";
                break;

            case 3:
                $str = "<span class='label label-warning'>$label</span>";
                break;

        }

        return $str;
    }


    /**
     * @return null|string
     */
    public function getStatusHtmlAttribute()
    {
        /** @var OrderState $state */
        $state = OrderState::getObj($this->order_status_id);
        return $state->getName(true);
    }


    /**
     * @return null|string
     */
    public function getFeedbackHtmlAttribute()
    {
        return nl2br($this->feedback);
    }


    /**
     * @return null|string
     */
    public function getUserHtmlAttribute()
    {
        $user = $this->created_by > 0 ? \Sentry::findUserById($this->created_by) : null;
        return $this->created_by > 0 && $user ? $user->email : null;
    }
}