<?php

namespace services\Models;

use services\Traits\HasParam;
use SingleModel;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

/**
 * services\Models\BuilderBlueprint
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $params
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderBlueprint whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderBlueprint whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderBlueprint whereCode($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderBlueprint whereParams($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderBlueprint whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderBlueprint whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderBlueprint whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderBlueprint whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderBlueprint whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderBlueprint whereDeletedBy($value)
 */
class BuilderBlueprint extends SingleModel
{
    /* LARAVEL PROPERTIES */

    protected $table = 'builder_blueprints';
    protected $guarded = array();
    use SoftDeletingTrait;
    use HasParam;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $timestamps = true;


    public $db_fields = array(
        "name",
        "code",
        "params",
    );
}