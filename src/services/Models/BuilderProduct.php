<?php

namespace services\Models;

use services\Traits\HasParam;
use SingleModel;
use Product;
use Illuminate\Database\Eloquent\SoftDeletingTrait;

/**
 * services\Models\BuilderProduct
 *
 * @property integer $id
 * @property integer $product_id
 * @property string $type
 * @property integer $blueprint_id
 * @property string $attachment
 * @property string $params
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property-read Product $product
 * @property-read BuilderBlueprint $blueprint
 * @property-read mixed $image
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderProduct whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderProduct whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderProduct whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderProduct whereBlueprintId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderProduct whereAttachment($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderProduct whereParams($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderProduct whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderProduct whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderProduct whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderProduct whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderProduct whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderProduct whereDeletedBy($value)
 */
class BuilderProduct extends SingleModel
{
    use HasParam;
    /* LARAVEL PROPERTIES */
    const TYPE_BASE = 'base';
    const TYPE_DROPLET = 'droplet';

    protected $table = 'builder_products';
    protected $guarded = array();
    use SoftDeletingTrait;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $timestamps = true;


    public $db_fields = array(
        "product_id",
        "type",
        "config_string",
        "blueprint_id",
        "attachment",
        "params",
    );

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    public function blueprint(){
        return $this->belongsTo(BuilderBlueprint::class, 'blueprint_id');
    }

    public function getImageAttribute(){
        return public_path("/");
    }
}