<?php

namespace services\Models;

use SingleModel;
use Order;

/**
 * services\Models\OrderSlip
 *
 * @property integer $order_id
 * @property string $delivery_id
 * @property string $sales_id
 * @property string $invoice_id
 * @property string $withdrawal_id
 * @property \Carbon\Carbon $created_at
 * @property-read Order $order
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlip whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlip whereDeliveryId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlip whereSalesId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlip whereInvoiceId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlip whereWithdrawalId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlip whereCreatedAt($value)
 */
class OrderSlip extends SingleModel
{
    /* LARAVEL PROPERTIES */

    protected $table = 'mi_orders_slips';
    protected $guarded = array();
    protected $dates = ['created_at'];
    public $timestamps = false;

    public $primaryKey = 'order_id';


    public $db_fields = array(
        "order_id",
        "delivery_id",
        "sales_id",
        "invoice_id",
        "created_at",
    );

    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }
}