<?php

namespace services\Models;

use services\Traits\HasParam;
use SingleModel;
use Product;
use Illuminate\Database\Eloquent\SoftDeletingTrait;
use Intervention\Image\ImageManagerStatic as Image;
use File;
use Site;
use App;
use PDF;

/**
 * services\Models\BuilderPayload
 *
 * @property integer $id
 * @property string $secure_key
 * @property integer $product_id
 * @property string $lang_id
 * @property integer $currency_id
 * @property string $params
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property \Carbon\Carbon $deleted_at
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $deleted_by
 * @property-read Product $product
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderPayload whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderPayload whereSecureKey($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderPayload whereProductId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderPayload whereLangId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderPayload whereCurrencyId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderPayload whereParams($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderPayload whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderPayload whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderPayload whereDeletedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderPayload whereCreatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderPayload whereUpdatedBy($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\BuilderPayload whereDeletedBy($value)
 */
class BuilderPayload extends SingleModel
{
    /* LARAVEL PROPERTIES */

    protected $table = 'builder_payloads';
    protected $guarded = array();
    use SoftDeletingTrait;
    use HasParam;
    protected $softDelete = true;
    protected $dates = ['deleted_at'];
    public $timestamps = true;


    public $db_fields = array(
        "product_id",
        "lang_id",
        "secure_key",
        "product_id",
        "params",
    );

    public function product()
    {
        return $this->belongsTo(Product::class, 'product_id');
    }

    static function getByCode($secure_key)
    {
        return BuilderPayload::where('secure_key', $secure_key)->first();
    }

    public function makeImage()
    {
        $this->load(['product', 'product.builder_product']);
        $items = $this->getParam('items');
        $base = public_path($this->product->builder_image);
        audit($base, 'base image');
        audit($items, 'items');

        $root = storage_path('files/payloads/');

        $img = Image::make($base);
        foreach ($items as $item) {
            $item = (object)$item;
            $product = Product::getPublicObj($item->product_id);
            $itemImage = public_path($product->builder_image);
            audit($itemImage, 'item image');
            $x = $item->x * 2;
            $y = $item->y * 2;
            if(isset($item->doubleSize) and $item->doubleSize == true){
                $x -= $item->width;
                $y += (int)$item->offsetTop;
            }else{
                if ($item->attachment == 'top') {
                    $y += $item->height / 2;
                }
            }

            audit("Inserting $product->sku at [$x, $y]");
            $img->insert($itemImage, 'top-left', $x, $y);
        }

        $img->save($root . $this->secure_key . '.png');
    }

    public function makePdf()
    {
        $this->makeImage();
        $data = $this->getPdfData();
        audit($data, __METHOD__);
        $pdf = PDF::loadView('pdf.builder', $data);
        $pdf->setPaper('a4')->setOrientation('portrait');
        $output = $pdf->output();
        File::put($data['filepath'], $output);
    }

    private function getDocumentData()
    {
        $shop = \Core::getShopInfo();

        $data = [
            'shop_header' => $shop['name'],
            'shop_address' => $shop['full_address'],
            'shop_phone' => $shop['full_phone'],
            'shop_details' => $shop['details'],
            'shop_email' => "<a href='mailto:{$shop['email']}'>" . $shop['email'] . "</a>",
        ];

        $data['logo'] = Site::root() . \Cfg::get('OGP_IMAGE');
        $data['url'] = Site::root() . "/media/pdf";
        $data['site'] = Site::root();
        $data['short_name'] = \Cfg::get('SHORTNAME');

        return $data;
    }

    public function getFileData()
    {
        $filename = $this->secure_key;
        $filepath = storage_path("files/payloads/$filename.pdf");
        $hasFile = File::exists($filepath);
        $url = Site::root() . "/files/payload-pdf/{$this->id}?secure_key=" . $this->secure_key;
        $payloadUrl = Site::root() . "/files/payload-img/{$this->id}?secure_key=" . $this->secure_key;

        $data = [];
        $data['filename'] = $filename;
        $data['filepath'] = $filepath;
        $data['hasFile'] = $hasFile;
        $data['payloadImg'] = storage_path("files/payloads/$filename.png");
        $data['pdfUrl'] = $url;
        $data['payloadUrl'] = $payloadUrl;
        return $data;
    }

    private function getPdfData()
    {
        App::setLocale($this->lang_id);
        $this->load(['product', 'product.builder_product']);
        $product = Product::getFullProduct($this->product_id, $this->lang_id);

        $data = $this->getFileData();
        $data['title'] = $product->name;
        $data['name'] = $product->name;
        $data['image'] = Site::img($this->product->builder_image, true);
        $data['price'] = (float)$product->price_label;
        $data['formattedPrice'] = \Format::currency($data['price'], true, $this->currency_id);
        $total = $data['price'];

        $items = $this->getParam('items');
        $products = [];

        foreach ($items as $item) {
            $item = (object)$item;
            $product = Product::getPublicObj($item->product_id);
            $product->setFullData();
            $itemImage = Site::img($product->builder_image, true);
            $item->name = $product->name;
            $item->sku = $product->sku;
            $item->image = $itemImage;
            $item->price = (float)$product->price_label;
            $item->formattedPrice = \Format::currency($item->price, true, $this->currency_id);
            $total += $item->price;
            $products[] = $item;
        }

        $data['products'] = $products;
        $data['total'] = $total;
        $data['formattedTotal'] = \Format::currency($total, true, $this->currency_id);
        $data += $this->getDocumentData();
        return $data;
    }

}