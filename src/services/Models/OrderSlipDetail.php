<?php

namespace services\Models;

use Carbon\Carbon;
use SingleModel;
use Order;
use OrderDetail;
use Exception;
use Illuminate\Database\Eloquent\Builder;

/**
 * services\Models\OrderSlipDetail
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $order_detail_id
 * @property string $type
 * @property string $matnr
 * @property string $vbeln
 * @property string $vbelv
 * @property string $lgnum
 * @property string $posnv
 * @property string $posnn
 * @property string $response_at
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Order $order
 * @property-read OrderDetail $order_detail
 * @property-read mixed $type_html
 * @property-read mixed $summary_html
 * @property-read mixed $reason_html
 * @property-read mixed $sku_html
 * @property-read mixed $event_at_html
 * @property-read mixed $created_at_html
 * @property-read mixed $updated_at_html
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlipDetail whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlipDetail whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlipDetail whereOrderDetailId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlipDetail whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlipDetail whereMatnr($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlipDetail whereVbeln($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlipDetail whereVbelv($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlipDetail whereLgnum($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlipDetail wherePosnv($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlipDetail wherePosnn($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlipDetail whereResponseAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlipDetail whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\OrderSlipDetail whereUpdatedAt($value)
 * @method static \services\Models\OrderSlipDetail withOrder($order_id)
 */
class OrderSlipDetail extends SingleModel
{
    /**
     * Se ha valore "J", nelle colonne VBELN / POSNN  puoi trovare il numero della consegna / riga consegna  e nella colonna  VBELV / POSNV  il numero dell'ordine / riga ordine
     *
     * Generazione Delivery Id
     *
     * VBELN = Numero consegna
     * POSNN = Riga consegna
     * VBELV = Numero ordine
     * POSNV = Riga ordine
     * MATNR = Codice articolo
     * ERDAT = Data creazione
     * ERZET = Ora creazione
     */
    const TYPE_SHIPPING = 'J';

    /**
     * Se ha valore "M", nelle colonne VBELN / POSNN  puoi trovare il numero della fattura / riga fattura  al cliente finale e nelle colonne VBELV / POSNV il numero della consegna / riga consegna
     *
     * Fatturazione
     *
     * VBELN = Numero fattura
     * POSNN = Riga fattura
     * VBELV = Numero consegna
     * POSNV = Riga consegna
     * MATNR = Codice articolo
     * ERDAT = Data emissione scontrino
     * ERZET = Ora creazione
     */
    const TYPE_INVOICE = 'M';

    /**
     * Se ha valore "R", nelle colonne VBELN / POSNN puoi trovare il numero del documento di scarico di magazzino / riga documento e nelle colonne  VBELV / POSNV  il numero della consegna / riga consegna.
     * Se è presente questa riga significa che la merce è già stata spedita
     *
     * Prelievo merce
     *
     * VBELN = Numero ritiro corriere
     * POSNN = Riga documento
     * VBELV = Numero consegna
     * POSNV = Riga consegna
     * MATNR = Codice articolo
     * ERDAT = Data emissione scontrino
     * ERZET = Ora creazione
     */
    const TYPE_SHIPPED = 'R';

    /**
     * (tracciamento consegna del prodotto da DEUFOL O NEGOZIO al CLIENTE) Se ha valore “7” nella colonna VBELN puoi trovare il numero della consegna e nella colonna MATNR il numero dello scontrino di vendita. Nella colonna ERDAT è presente la data di emissione dello scontrino.
     *
     * EMESSO LO SCONTRINO, CLIENTE RITIRA IN NEGOZIO
     *
     * VBELV = Numero consegna
     * POSNV = Riga consegna
     * MATNR = Numero scontrino
     * ERDAT = Data emissione scontrina
     * ERZET = Ora creazione
     */
    const TYPE_SOLD = '7';

    /**
     * Se ha valore “8” nella colonna VBELN puoi trovare il numero della consegna e nella colonna MATNR il numero del ritiro da parte del corriere. Nella colonna ERDAT è presente la data di ritiro richiesta.
     *
     * SPEDIZIONE DA DEUFOL VS NEGOZIO/CLIENTE
     *
     * ERDAT = Data prenotazione ritiro
     * ERZET = Ora creazione
     *
     */
    const TYPE_CARRIER = '8';

    /**
     * Se ha valore “9” nella colonna VBELN puoi trovare il numero della consegna e nella colonna MATNR il numero del ritiro da parte del corriere. Nella colonna ERDAT è presente la data di creazione del ritiro. In questo caso il destinatario è il negozio di ritiro.
     *
     * SPEDIZIONE DA NEGOZIO VS CLIENTE
     *
     * VBELV = Numero consegna
     * POSNV = Riga consegna
     * MATNR = Numero ritiro corriere
     * ERDAT = Data creazione
     * ERZET = Ora creazione
     */
    const TYPE_CARRIER_SHOP = '9';

    /**
     * Se ha valore "C" ,  nelle colonne VBELV / POSNV puoi trovare il numero dell’ordine di vendita e nella colonna LGNUM la causale di annullamento della posizione.
     * (Z5 quando la merce è stata rifiutata, Z6 quando l’ordine è annullato es. per merce mancante, Z8 quando l’ordine aveva consegna da Deufol e, per l’impossibilità dell’evasione (es. articolo o corredo mancante), viene manualmente annullato)
     * Se è presente questa riga significa che la riga ordine è stata annullata
     *
     * VBELV = Numero ordine
     * POSNV = Riga consegna
     * MATNR = Codice articolo
     * ERDAT = Data creazione
     * ERZET = Ora creazione
     * LGNUM = Causale annullamento
     */
    const TYPE_CANCELLED = 'C';

    /**
     * Se ha valore "P", il pacco è pronto per il ritiro in store a cura del cliente;
     *
     * ERDAT = Data creazione
     * ERZET = Ora creazione
     */
    const TYPE_PACKAGE_READY = 'P';

    /**
     * Se ha valore "X", nelle colonne VBELN / POSNN significa che l’ordine su Negoziando
     * è stato indicato come “non trovato”. Se il flusso di “non trovato” viene poi
     * correttamente completato in sede con l’annullamento dell’ordine,
     * verrà inserito il motivo di rifiuto (Z6/Z5) nella riga del flusso che presenta il valore “C”.
     *
     * VBELV = Numero ordine
     * POSNV = Riga consegna
     * MATNR = Codice articolo
     * ERDAT = Data creazione
     * ERZET = Ora creazione
     * LGNUM = Causale annullamento
     */
    const TYPE_SHOP_NOT_FOUND = 'X';

    /**
     * Pacco ritirato in store (senza scontrino)
     *
     * VBELV = Numero consegna
     * POSNV = Riga consegna
     * ERDAT = Data creazione
     * ERZET = Ora creazione
     */
    const TYPE_RITIRED_ONLY = '1';

    /**
     * Non trovato (soft)
     */
    const TYPE_CANCELLED_NOT_FOUND = 'W';

    /**
     * Z5 quando la merce è stata rifiutata
     */
    const REASON_UNPICKED = 'Z5';

    /**
     * Z6 quando l’ordine è annullato es. per merce mancante
     */
    const REASON_NOT_FOUND = 'Z6';

    /**
     * Z8 quando l’ordine aveva consegna da Deufol e,
     * per l’impossibilità dell’evasione (es. articolo o corredo mancante),
     * viene manualmente annullato
     */
    const REASON_CANCELLED = 'Z8';

    /**
     * This is a custom label for TYPE_SOLD event (7)
     */
    const REASON_SHOP_SOLD = '7';

    /**
     * Se l’ordine su SAP non è annullato ma il relativo ordine su negoziando
     * ha status diverso da blank (ossia l’ordine è stato annullato solo da Negoziando),
     * lo stato dell’ordine Negoziando  viene riportato sulla colonna LGNUM:
     * RRR ( es. causale ANN, RRR “Annullato non ritirato”)
     */
    const REASON_CANCELLED_NOT_PICKED = 'RRR';
    const REASON_CANCELLED_NOT_PICKED_ALT = 'ANN';

    /**
     * NON TROVATO
     */
    const REASON_CANCELLED_NOT_FOUND = 'NNN';
    const REASON_CANCELLED_NOT_FOUND_ALT = 'ZN';

    /*
     * Non Gestito (silenti 48H)
     */
    const REASON_NOT_HANDLED = 'ZC';
    const REASON_NOT_HANDLED_ALT = 'INA';

    /*
     * Non Scontrinato correttamente
     */
    const REASON_NOT_RECEIPTED = 'ZY';

    /*
     * Ordine Reso on Store
     */
    const REASON_RMA_STORE = 'RES';

    /**
     * This is a custom label for TYPE_PACKAGE event (7)
     */
    const REASON_PACKAGE_READY = 'INT-P';

    /**
     * This is a custom label for TYPE_SOLD event (7)
     */
    const REASON_SHOP_SHIPPED = 'INT-SS';

    /**
     * This is a custom label for TYPE_CARRIER event (7)
     */
    const REASON_SHIPPED = 'INT-SH';

    /**
     * This is a custom label for TYPE_SHOP_NOT_FOUND event (X)
     */
    const REASON_SHOP_NOT_FOUND = 'INT-Z6';

    /**
     * This is a custom label for TYPE_RETIRED event (7)
     */
    const REASON_SHOP_RETIRED = 'INT-SR';


    /* LARAVEL PROPERTIES */

    protected $table = 'mi_orders_slips_details';
    protected $guarded = array("id", "created_at", "updated_at",);

    protected $order_id;

    protected $order_detail_id;

    protected $type;


    public $db_fields = array(
        "order_id",
        "order_detail_id",
        "type",
        "matnr",
        "vbeln",
        "vbelv",
        "lgnum",
        "posnv",
        "posnn",
        "response_at",
        "created_at",
        "updated_at",
    );

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order_detail()
    {
        return $this->belongsTo(OrderDetail::class, 'order_detail_id');
    }

    /**
     * @param Builder $builder
     * @param $order_id
     */
    function scopeWithOrder(Builder $builder, $order_id)
    {
        $builder->where('order_id', $order_id);
    }

    /**
     * @return OrderDetail|null
     */
    public function getOrderDetail()
    {
        return ($this->getAttribute('order_detail_id') > 0) ? OrderDetail::find($this->getAttribute('order_detail_id')) : null;
    }

    /**
     * @param $item
     */
    public function setAttributesByOrderFlowItem($item)
    {
        $type = $item->VBTYP_N;
        $matnr = isset($item->MATNR) ? $item->MATNR : null;
        $vbeln = isset($item->VBELN) ? $item->VBELN : null;
        $vbelv = isset($item->VBELV) ? $item->VBELV : null;
        $lgnum = isset($item->LGNUM) ? $item->LGNUM : null;
        $posnv = isset($item->POSNV) ? $item->POSNV : null;
        $posnn = isset($item->POSNN) ? $item->POSNN : null;

        $response_at = null;
        if (isset($item->ERDAT) and isset($item->ERZET) and ($item->ERDAT) != '0000-00-00') {
            $response_at = $item->ERDAT . ' ' . $item->ERZET;
        }

        $attributes = compact('type', 'matnr', 'vbeln', 'vbelv', 'lgnum', 'posnv', 'posnn', 'response_at');

        $this->setAttributes($attributes);
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * @param int $order_id
     */
    public function setOrderId($order_id)
    {
        $this->order_id = $order_id;
        $this->setAttribute('order_id', $order_id);
    }

    /**
     * @return int
     */
    public function getOrderDetailId()
    {
        return $this->order_detail_id;
    }

    /**
     * @param int $order_detail_id
     */
    public function setOrderDetailId($order_detail_id)
    {
        $this->order_detail_id = $order_detail_id;
        $this->setAttribute('order_detail_id', $order_detail_id);
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->getAttribute('type');
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
        $this->setAttribute('type', $type);
    }

    /**
     * @return string
     */
    public function getProductSku()
    {
        return trim($this->matnr);
    }

    /**
     * @return bool
     */
    public function hasProductSku()
    {
        $number = $this->getProductSku();
        return strlen($number) > 0;
    }

    /**
     * @return string
     */
    public function getCarrierShipping()
    {
        return trim($this->matnr);
    }

    /**
     * @return string
     */
    public function getReceipt()
    {
        return trim($this->matnr);
    }

    /**
     * @return bool
     */
    public function hasCarrierShipping()
    {
        $number = $this->getCarrierShipping();
        return strlen($number) > 0;
    }

    /**
     * @return string
     */
    public function getNumber()
    {
        return trim($this->vbeln);
    }

    /**
     * @return bool
     */
    public function hasNumber()
    {
        $number = $this->getNumber();
        return strlen($number) > 0;
    }

    /**
     * @return string
     */
    public function getShipping()
    {
        return $this->getNumber();
    }

    /**
     * @return bool
     */
    public function hasShipping()
    {
        return $this->hasNumber();
    }

    /**
     * @return string
     */
    public function getDelivery()
    {
        return trim($this->vbelv);
    }

    /**
     * @return bool
     */
    public function hasDelivery()
    {
        $number = $this->getDelivery();
        return strlen($number) > 0;
    }

    /**
     * @return string
     */
    public function getReason()
    {
        return trim($this->lgnum);
    }

    /**
     * @return bool
     */
    public function hasReason()
    {
        $number = $this->getReason();
        return strlen($number) > 0;
    }

    /**
     * @return string
     */
    public function getDocumentLine()
    {
        return trim($this->posnn);
    }

    /**
     * @return string
     */
    public function getOrderLine()
    {
        return trim($this->posnv);
    }

    /**
     * @throws Exception
     */
    public function findOrderDetail()
    {
        $order_id = $this->getOrderId();
        $sku = $this->getProductSku();

        if ($order_id <= 0)
            throw new Exception("order_id is not a valid integer");

        //disable because can occurs that the SKU is not passed
        /*if (strlen($sku) <= 0)
            throw new Exception("Product sku (matnr) is not valid");*/

        $orderDetail = OrderDetail::withOrder($order_id)
            ->withSku($sku)
            ->orderBy('id', 'desc')
            ->first();

        if ($orderDetail)
            $this->setOrderDetailId($orderDetail->id);

    }

    /**
     * @return array
     */
    public function getPayloadData()
    {
        return ['matnr', 'vbeln', 'vbelv', 'lgnum', 'posnv', 'posnn'];
    }

    /**
     * @return null|OrderSlipDetail
     */
    public function upsertPayload()
    {

        if ($this->getAttribute('order_id') > 0) {

            $params = [
                'order_id' => (int)$this->getAttribute('order_id'),
                'order_detail_id' => (int)$this->getAttribute('order_detail_id'),
                'type' => $this->getAttribute('type'),
            ];

            $existingModel = self::where($params)->orderBy('response_at', 'desc')->first();

            if ($existingModel) {
                $responseAt = $this->getAttribute('response_at');
                if ($responseAt == '0000-00-00 00:00:00')
                    $responseAt = null;

                $oldResponseAt = $existingModel->getAttribute('response_at');
                if ($oldResponseAt == '0000-00-00 00:00:00')
                    $oldResponseAt = null;

                $upsert = false;
                if ($responseAt == null or $oldResponseAt == null) {
                    $upsert = true;
                } else {
                    $responseDate = Carbon::parse($this->getAttribute('response_at'));
                    $oldResponseDate = Carbon::parse($existingModel->getAttribute('response_at'));
                    $upsert = $responseDate->gt($oldResponseDate);
                }

                //new response is newer, update the model
                if ($upsert) {
                    $existingModel->fill($this->toArray());
                    $existingModel->save();
                    audit("OrderSlipDetail updated");
                    return $existingModel;
                }
                return null;
            } else {
                $this->save();
                audit("OrderSlipDetail inserted");
                return $this;
            }
        }

        return null;

    }


    /**
     * @return null|string
     */
    public function getTypeHtmlAttribute()
    {
        $str = null;

        $key = $this->getType();
        $labels = self::getTypeLabels();
        $label = isset($labels[$key]) ? $labels[$key] : null;

        switch ($key) {

            case self::TYPE_SHIPPING:
                $str = "<span class='label label-inverse'>$label</span>";
                break;

            case self::TYPE_INVOICE:
                $str = "<span class='label label-info'>$label</span>";
                break;

            case self::TYPE_PACKAGE_READY:
            case self::TYPE_SOLD:
            case self::TYPE_SHIPPED:
            case self::TYPE_RITIRED_ONLY:
                $str = "<span class='label label-success'>$label</span>";
                break;

            case self::TYPE_CARRIER_SHOP:
            case self::TYPE_CARRIER:
            case self::TYPE_CANCELLED_NOT_FOUND:
                $str = "<span class='label label-warning'>$label</span>";
                break;

            case self::TYPE_SHOP_NOT_FOUND:
            case self::TYPE_CANCELLED:
                $str = "<span class='label label-important'>$label</span>";
                break;

        }

        return $str;
    }


    public function getSummaryHtmlAttribute()
    {
        $str = null;
        switch ($this->getType()) {

            case self::TYPE_INVOICE:
            case self::TYPE_SHIPPING:
                $number = $this->getNumber();
                $str = "<strong>$number</strong>";
                break;

            case self::TYPE_SHIPPED:
                //Se ha valore "R", nelle colonne VBELN / POSNN puoi trovare il numero del documento di scarico di magazzino / riga documento e nelle colonne  VBELV / POSNV  il numero della consegna / riga consegna.
                $number = $this->getNumber(); //VBELN
                $delivery = $this->getDelivery(); //VBELV
                $str = "N° scarico magazzino: <strong>$number</strong> | Numero consegna: <strong>$delivery</strong>";
                break;

            case self::TYPE_CARRIER:
                //Se ha valore “8” nella colonna VBELN puoi trovare il numero della consegna e nella colonna MATNR il numero del ritiro da parte del corriere. Nella colonna ERDAT è presente la data di ritiro richiesta.
                $number = $this->getNumber(); //VBELN => This was wrong
                $number = $this->getDelivery(); //VBELV
                $carrier_shipping = $this->getCarrierShipping(); //MATNR
                $str = "Numero consegna <strong>$number</strong> | Numero ritiro corriere: <strong>$carrier_shipping</strong>";
                break;

            case self::TYPE_SOLD:
                //(tracciamento consegna del prodotto da DEUFOL O NEGOZIO al CLIENTE) Se ha valore “7” nella colonna VBELN puoi trovare il numero della consegna e nella colonna MATNR il numero dello scontrino di vendita. Nella colonna ERDAT è presente la data di emissione dello scontrino.
                $number = $this->getNumber(); //VBELN => This was wrong
                $number = $this->getDelivery(); //VBELV
                $receipt = $this->getReceipt(); //MATNR
                $str = "Numero consegna <strong>$number</strong> | Numero scontrino: <strong>$receipt</strong>";
                break;

            case self::TYPE_CARRIER_SHOP:
                //Se ha valore “9” nella colonna VBELN puoi trovare il numero della consegna e nella colonna MATNR il numero del ritiro da parte del corriere. Nella colonna ERDAT è presente la data di creazione del ritiro. In questo caso il destinatario è il negozio di ritiro.
                $number = $this->getNumber(); //VBELN => This was wrong
                $number = $this->getDelivery(); //VBELV
                $carrier_shipping = $this->getCarrierShipping(); //MATNR
                $str = "Numero consegna <strong>$number</strong> | Numero ritiro corriere: <strong>$carrier_shipping</strong>";
                break;

            case self::TYPE_CANCELLED:
                /*
                Se ha valore "C" ,  nelle colonne VBELV / POSNV puoi trovare il numero dell’ordine di vendita e nella colonna LGNUM la causale di annullamento della posizione.
                (Z5 quando la merce è stata rifiutata, Z6 quando l’ordine è annullato es. per merce mancante, Z8 quando l’ordine aveva consegna da Deufol e, per l’impossibilità dell’evasione (es. articolo o corredo mancante), viene manualmente annullato)
                Se è presente questa riga significa che la riga ordine è stata annullata
                */
                $delivery = $this->getDelivery(); //VBELV
                $str = "Numero ordine di vendita: <strong>$delivery</strong>";
                break;

            case self::TYPE_PACKAGE_READY:
                /*
                Se ha valore "P", il pacco è pronto per il ritiro in store a cura del cliente;
                */
                $str = 'Pacco pronto (ritiro in store)';
                break;

            case self::TYPE_SHOP_NOT_FOUND:
                //Se ha valore “X” non trovato in Negoziando
                $str = 'Non trovato in Deufol';
                break;

            case self::TYPE_RITIRED_ONLY:
                $str = 'Pacco ritirato in store';
                break;

            case self::TYPE_CANCELLED_NOT_FOUND:
                $str = 'Non trovato (cancellazione soft)';
                break;

        }

        return $str;
    }


    public function getReasonHtmlAttribute()
    {
        $str = null;

        $key = $this->getReason();
        $labels = self::getReasonLabels();
        $label = isset($labels[$key]) ? $labels[$key] : null;

        if($label){
            $str = "<span class='label'>$label</span>";
        }

        return $str;
    }


    public function getSkuHtmlAttribute()
    {
        $str = null;
        switch ($this->getType()) {

            case self::TYPE_CARRIER:
            case self::TYPE_CARRIER_SHOP:
            case self::TYPE_SOLD:
                $str = 'N/D';
                break;

            default:
                $sku = $this->getProductSku();
                $str = "<code>$sku</code>";
                break;

        }

        return $str;
    }


    private function carbonDateTime($val)
    {
        if (!$val instanceof Carbon) {
            if ($val == '0000-00-00 00:00:00')
                return null;

            $val = Carbon::parse($val);
        }
        return $val->format('d-m-Y H:i:s');
    }

    public function getEventAtHtmlAttribute()
    {
        if($this->response_at == '' or $this->response_at == null)
            return 'N/D';

        return $this->carbonDateTime($this->response_at);
    }

    public function getCreatedAtHtmlAttribute()
    {
        return $this->carbonDateTime($this->created_at);
    }

    public function getUpdatedAtHtmlAttribute()
    {
        return $this->carbonDateTime($this->updated_at);
    }

    static function getTypeLabels()
    {
        return [
            self::TYPE_SHIPPING => '[ J ] Numero consegna',
            self::TYPE_INVOICE => '[ M ] Numero fattura',
            self::TYPE_SHIPPED => '[ R ] Merce spedita',
            self::TYPE_SOLD => '[ 7 ] Emesso scontrino (ritiro negozio)',
            self::TYPE_CARRIER => '[ 8 ] Ritiro corriere (dest. cliente)',
            self::TYPE_CARRIER_SHOP => '[ 9 ] Ritiro corriere (dest. negozio)',
            self::TYPE_CANCELLED => '[ C ] Riga ordine annullata',
            self::TYPE_PACKAGE_READY => '[ P ] Pacco pronto (ritiro negozio)',
            self::TYPE_SHOP_NOT_FOUND => '[ X ] Non trovato (Deufol)',
            self::TYPE_RITIRED_ONLY => '[ 1 ] Pacco ritirato in negozio',
            self::TYPE_CANCELLED_NOT_FOUND => '[ W ] Non trovato (cancellazione soft)',
        ];
    }

    static function getReasonLabels()
    {
        return [
            self::REASON_UNPICKED => '[ Z5 ] Merce rifiutata/non ritirata',
            self::REASON_CANCELLED_NOT_PICKED => '[ RRR ] Non ritirato',
            self::REASON_CANCELLED_NOT_FOUND => '[ NNN ] Non trovato',
            self::REASON_CANCELLED_NOT_FOUND_ALT => '[ ZN ] Non trovato',
            self::REASON_CANCELLED => '[ Z8 ] Annullato manualmente',
            self::REASON_NOT_HANDLED => '[ ZC ] Non gestito',
            self::REASON_NOT_HANDLED_ALT => '[ INA ] Non gestito',
            self::REASON_NOT_FOUND => '[ Z6 ] Annullato da web',
            self::REASON_CANCELLED_NOT_PICKED_ALT => '[ ANN ] Annullato da web',
            self::REASON_NOT_RECEIPTED => '[ ZY ] Non scontrinato correttamente',
            self::REASON_RMA_STORE => '[ RES ] Ordine reso in store',
        ];
    }

    static function selectTypes($withBlank = false)
    {
        $data = self::getTypeLabels();
        if ($withBlank) $data = ["" => "Scegli..."] + $data;
        return $data;
    }

    static function selectReasons($withBlank = false)
    {
        $data = self::getReasonLabels();
        if ($withBlank) $data = ["" => "Scegli..."] + $data;
        return $data;
    }
}