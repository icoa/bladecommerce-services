<?php

namespace services\Models;

use Carbon\Carbon;
use SingleModel;
use Order;
use Customer;
use Email;
use Exception;
use Illuminate\Database\Eloquent\Builder;

/**
 * services\Models\CustomerEmailWarning
 *
 * @property integer $id
 * @property string $type
 * @property integer $customer_id
 * @property integer $order_id
 * @property integer $email_id
 * @property string $recipient
 * @property string $message
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Order $order
 * @property-read Customer $customer
 * @property-read Email $email
 * @property-read mixed $created_at_html
 * @property-read mixed $updated_at_html
 * @property-read mixed $type_html
 * @property-read \$this->getBlameableModel() $createdBy
 * @property-read \$this->getBlameableModel() $updatedBy
 * @property-read \$this->getBlameableModel() $deletedBy
 * @method static \Illuminate\Database\Query\Builder|\services\Models\CustomerEmailWarning whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\CustomerEmailWarning whereType($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\CustomerEmailWarning whereCustomerId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\CustomerEmailWarning whereOrderId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\CustomerEmailWarning whereEmailId($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\CustomerEmailWarning whereRecipient($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\CustomerEmailWarning whereMessage($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\CustomerEmailWarning whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\services\Models\CustomerEmailWarning whereUpdatedAt($value)
 * @method static \services\Models\CustomerEmailWarning withType($type)
 * @method static \services\Models\CustomerEmailWarning withOrder($order_id)
 * @method static \services\Models\CustomerEmailWarning withCustomer($customer_id)
 * @method static \services\Models\CustomerEmailWarning withEmail($email_id)
 * @method static \services\Models\CustomerEmailWarning withEmailCode($code)
 */
class CustomerEmailWarning extends SingleModel
{
    const TYPE_SHOP_PICKUP = 'P';
    const TYPE_PACKAGE_READY = 'R';
    const TYPE_SHOP_TARGET_SHIPMENT = 'S';
    const TYPE_SHOP_CUSTOMER_CALL = 'C';

    /* LARAVEL PROPERTIES */
    protected $table = 'customer_email_warnings';
    protected $guarded = array("id", "created_at", "updated_at",);


    public $db_fields = array(
        "order_id",
        "customer_id",
        "email_id",
        "type",
        "recipient",
        "message",
        "created_at",
        "updated_at",
    );

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(Order::class, 'order_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function email()
    {
        return $this->belongsTo(Email::class, 'email_id');
    }

    /**
     * @param Builder $builder
     * @param $type
     */
    function scopeWithType(Builder $builder, $type)
    {
        $builder->where('type', $type);
    }

    /**
     * @param Builder $builder
     * @param $order_id
     */
    function scopeWithOrder(Builder $builder, $order_id)
    {
        $builder->where('order_id', $order_id);
    }

    /**
     * @param Builder $builder
     * @param $customer_id
     */
    function scopeWithCustomer(Builder $builder, $customer_id)
    {
        $builder->where('customer_id', $customer_id);
    }

    /**
     * @param Builder $builder
     * @param $email_id
     */
    function scopeWithEmail(Builder $builder, $email_id)
    {
        $builder->where('email_id', $email_id);
    }

    /**
     * @param Builder $builder
     * @param $code
     */
    function scopeWithEmailCode(Builder $builder, $code)
    {
        $email_id = Email::getByCode($code)->id;
        $builder->where('email_id', $email_id);
    }

    /**
     * @return int
     */
    public function getOrderId()
    {
        return $this->getAttribute('order_id');
    }

    /**
     * @param int $order_id
     */
    public function setOrderId($order_id)
    {
        $this->setAttribute('order_id', $order_id);
    }

    /**
     * @return int
     */
    public function getCustomerId()
    {
        return $this->getAttribute('customer_id');
    }

    /**
     * @param int $customer_id
     */
    public function setCustomerId($customer_id)
    {
        $this->setAttribute('customer_id', $customer_id);
    }

    /**
     * @return int
     */
    public function getEmailId()
    {
        return $this->getAttribute('email_id');
    }

    /**
     * @param int $email_id
     */
    public function setEmailId($email_id)
    {
        $this->setAttribute('email_id', $email_id);
    }

    /**
     * @param int $code
     */
    public function setEmailIdByCode($code)
    {
        $email_id = Email::getByCode($code)->id;
        $this->setAttribute('email_id', $email_id);
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->getAttribute('type');
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->setAttribute('type', $type);
    }

    /**
     * @param $val
     * @return null|string
     */
    private function carbonDateTime($val)
    {
        if (!$val instanceof Carbon) {
            if ($val == '0000-00-00 00:00:00')
                return null;

            $val = Carbon::parse($val);
        }
        return $val->format('d-m-Y H:i:s');
    }

    /**
     * @return null|string
     */
    public function getCreatedAtHtmlAttribute()
    {
        return $this->carbonDateTime($this->created_at);
    }

    /**
     * @return null|string
     */
    public function getUpdatedAtHtmlAttribute()
    {
        return $this->carbonDateTime($this->updated_at);
    }

    /**
     * @return array
     */
    static function getTypeLabels()
    {
        return [
            self::TYPE_SHOP_PICKUP => "Mancato ritiro negozio",
            self::TYPE_PACKAGE_READY => "Pacco pronto ritiro",
            self::TYPE_SHOP_TARGET_SHIPMENT => "Consegna corriere",
            self::TYPE_SHOP_CUSTOMER_CALL => "Sollecito telefono Cliente",
        ];
    }


    /**
     * @return null|string
     */
    public function getTypeHtmlAttribute()
    {
        $str = null;

        $key = $this->getType();
        $labels = self::getTypeLabels();
        $label = isset($labels[$key]) ? $labels[$key] : null;

        switch ($key) {

            case self::TYPE_SHOP_PICKUP:
                $str = "<span class='label label-important'>$label</span>";
                break;

            case self::TYPE_PACKAGE_READY:
                $str = "<span class='label label-success'>$label</span>";
                break;

            case self::TYPE_SHOP_TARGET_SHIPMENT:
                $str = "<span class='label label-info'>$label</span>";
                break;

            case self::TYPE_SHOP_CUSTOMER_CALL:
                $str = "<span class='label label-warning'>$label</span>";
                break;

        }

        return $str;
    }
}