<?php


namespace services;

use Cfg;
use Illuminate\Console\Command;


class FeatsManager
{

    /**
     * @return $this
     */
    public function getSelf()
    {
        return $this;
    }

    /**
     * @param $key
     * @return bool
     */
    public function enabled($key)
    {
        return config('services.' . $key, false) === true;
    }

    /**
     * @param $key
     * @return bool
     */
    public function disabled($key)
    {
        return !$this->enabled($key);
    }

    /**
     * @return bool
     */
    public function sapReceipts()
    {
        return $this->enabled('sap_fees');
    }

    /**
     * @return bool
     */
    public function receipts()
    {
        return $this->enabled('fees') or $this->enabled('sap_fees');
    }

    /**
     * @return bool
     */
    public function noReceipts()
    {
        return $this->disabled('fees') and $this->disabled('sap_fees');
    }

    /**
     * @return bool
     */
    public function bladeReceipts()
    {
        return $this->enabled('fees') and $this->disabled('sap_fees');
    }


    /**
     * @param $key
     * @return bool
     */
    public function switchEnabled($key)
    {
        return (int)Cfg::get($key, 0) === 1;
    }


    /**
     * @param $key
     * @return bool
     */
    public function switchDisabled($key)
    {
        return (int)Cfg::get($key, 0) === 0;
    }

    /**
     * @return array
     */
    public function allSwitches()
    {
        return config('soap.switches', []);
    }

    /**
     * @param $key
     * @return bool
     */
    public function switchDefault($key)
    {
        return (bool)config("soap.switches.$key.default", false);
    }

    /**
     * @param $key
     * @return bool
     */
    public function switchHasChanged($key)
    {
        return (int)Cfg::get($key, 0) !== (int)$this->switchDefault($key);
    }

    /**
     * @return bool
     */
    public function switchesAreNotDefault()
    {
        $switches = $this->allSwitches();
        foreach ($switches as $key => $payload) {
            if ($this->switchHasChanged($key) === true)
                return true;
        }
        return false;
    }

    /**
     * @param Command $console
     */
    public function runSeeder(Command $console)
    {
        $switches = $this->allSwitches();
        foreach ($switches as $key => $payload) {
            $hasValue = Cfg::real($key, null);
            if ($hasValue === null) {
                Cfg::save($key, $payload['default'], 'features');
                $console->line("Upserted config value for [$key]");
            }
        }
    }
}