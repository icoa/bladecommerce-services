<?php

namespace services\Remote\Controllers;

use Input, Response, Request;
use BaseController;
use services\Fees\Repositories\FeesBackendRepository;
use services\Repositories\UserRepository;

class RemoteController extends BaseController
{

    /**
     * @var UserRepository
     */
    protected $userRepository;

    /**
     * @var FeesBackendRepository
     */
    protected $feesBackendRepository;

    function __construct(UserRepository $userRepository, FeesBackendRepository $feesBackendRepository)
    {
        $this->userRepository = $userRepository;
        $this->feesBackendRepository = $feesBackendRepository;
        parent::__construct();
    }

    function getPing()
    {
        return 'pong (GET) from ' . \Cfg::get('SHORTNAME');
    }

    function postPing()
    {
        return 'pong (POST) from ' . \Cfg::get('SHORTNAME');
    }

    function getJson()
    {
        $from = Request::header('X-BLADE-FROM');
        $data = Input::all();
        $data['ts'] = time();
        $data['owner'] = \Cfg::get('SHORTNAME');
        audit($from, __METHOD__ . '::from');
        audit($data, __METHOD__ . '::data');
        return Response::json($data);
    }

    function postJson()
    {
        $from = Request::header('X-BLADE-FROM');
        $data = Input::all();
        $data['ts'] = time();
        $data['owner'] = \Cfg::get('SHORTNAME');
        audit($from, __METHOD__ . '::from');
        audit($data, __METHOD__ . '::data');
        return Response::json($data);
    }

    function postGdprCustomerFlags()
    {
        $email = Input::get('email');
        $marketing = Input::get('marketing');
        $profile = Input::get('profile');
        $newsletter = Input::get('newsletter');
        $flags = compact('marketing', 'profile', 'newsletter');
        $response = $this->userRepository->updateCustomerGdprFlagsByEmail($email, $flags);
        return Response::json($response);
    }

    function postGdprNewsletterFlags()
    {
        $email = Input::get('email');
        $marketing = Input::get('marketing');
        $profile = Input::get('profile');
        $flags = compact('marketing', 'profile');
        $response = $this->userRepository->updateNewsletterGdprFlagsByEmail($email, $flags);
        return Response::json($response);
    }

    function postFetchFeeFile()
    {
        $week = Input::get('week');
        $year = Input::get('year');

        audit(compact('week', 'year'), __METHOD__);

        $content = $this->feesBackendRepository->getCsvFileContent($week, $year);
        $response = Response::make($content);
        return $response;
    }
}