<?php

namespace services\Remote;

use DB;
use Exception;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Carbon\Carbon;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\ClientException;

/**
 * Class RemoteClient
 * @package services\Repositories
 */
class RemoteClient
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var ResponseInterface
     */
    protected $response;

    /**
     * @var bool
     */
    protected $debug = false;

    /**
     * @var mixed
     */
    protected $error = null;

    /**
     * RemoteRepository constructor.
     */
    function __construct()
    {
        $this->makeClient();
    }

    protected function makeClient()
    {
        $config = [
            'auth' => [
                config('remote.internal.username', 'username'),
                config('remote.internal.password', 'password'),
            ],
            'timeout' => 30,
            'verify' => false,
            'headers' => [
                'X-BLADE-FROM' => config('remote.internal.from')
            ]
        ];
        $this->client = new Client($config);
    }

    /**
     * @param $url
     * @param array $options
     * @return RemoteClient
     */
    public function get($url, $options = [])
    {
        return $this->makeRequest('GET', $url, $options);
    }

    /**
     * @param $url
     * @param array $options
     * @return RemoteClient
     */
    public function post($url, $options = [])
    {
        return $this->makeRequest('POST', $url, $options);
    }

    /**
     * @param $verb
     * @param $url
     * @param array $options
     * @return $this
     */
    protected function makeRequest($verb, $url, $options = [])
    {
        try {
            $this->makeClient();
            $this->error = null;
            //$debug = compact('verb', 'url', 'options');
            //audit($debug, __METHOD__);
            $response = $this->client->send($this->client->createRequest($verb, $url, $options));
            $this->response = $response;
        } catch (ClientException $e) {
            $this->error = $e->getMessage();
            audit_exception($e);
        } catch (Exception $e) {
            $this->error = $e->getMessage();
            audit_exception($e);
        }
        return $this;
    }

    /**
     * @return ResponseInterface|null
     */
    public function getResponse()
    {
        return $this->response;
    }

    /**
     * @return int|null
     */
    public function getResponseStatus()
    {
        $response = $this->getResponse();
        return $response ? $response->getStatusCode() : null;
    }

    /**
     * @return null|\Psr\Http\Message\StreamInterface
     */
    public function getResponseBody()
    {
        $response = $this->getResponse();

        $results = $response ? (string)$response->getBody() : $this->error;

        if ($response) {
            $contentType = $response->getHeader('Content-Type');
            if ($contentType == 'application/json' and is_string($results)) {
                $results = (array)json_decode($results);
            }
        }

        //audit($results, __METHOD__);
        return $results;
    }


}