<?php

namespace services\Remote\Repositories;

use services\Remote\Exceptions\TenantNotDefinedException;
use services\Remote\Exceptions\TenantNotFoundException;
use services\Repositories\Repository;
use services\Remote\RemoteClient;
use services\Traits\TraitParams;

/**
 * Class RemoteRepository
 * @package services\Repositories
 */
class RemoteRepository extends Repository
{

    use TraitParams;

    /**
     * @var RemoteClient
     */
    protected $client;

    /**
     * @var array
     */
    protected $tenants = [];

    /**
     * @var array
     */
    protected $responses = [];

    /**
     * @var bool
     */
    protected $broadcast_mode = false;

    /**
     * @var bool
     */
    protected $queue_mode = false;

    /**
     * @var string
     */
    protected $verb = 'GET';

    /**
     * @var mixed
     */
    protected $tenant;

    /**
     * RemoteRepository constructor.
     * @param RemoteClient $client
     */
    function __construct(RemoteClient $client)
    {
        $this->client = $client;
        $this->tenants = config('remote.internal.tenants', []);
        $this->queue_mode = config('remote.internal.queue', false);
        $this->setup();
    }

    protected function setup(){

    }

    /**
     * @return bool
     */
    function isQueueEnabled()
    {
        return $this->queue_mode;
    }

    /**
     * @param bool $enabled
     * @return $this
     */
    function queue($enabled = true)
    {
        $this->queue_mode = $enabled;
        return $this;
    }

    /**
     * @param bool $enabled
     * @return $this
     */
    function broadcast($enabled = true)
    {
        $this->broadcast_mode = $enabled;
        return $this;
    }

    /**
     * @param $route
     * @return RemoteRepository
     */
    function get($route)
    {
        $this->verb = 'GET';
        return ($this->broadcast_mode) ? $this->sendAll($route) : $this->send($route);
    }

    /**
     * @param $route
     * @return RemoteRepository
     */
    function post($route)
    {
        $this->verb = 'POST';
        return ($this->broadcast_mode) ? $this->sendAll($route) : $this->send($route);
    }

    /**
     * @param $route
     * @return RemoteRepository
     */
    function sendAll($route)
    {
        return $this->makeRequest($this->tenants, $route);
    }

    /**
     * @param $route
     * @return RemoteRepository
     * @throws TenantNotDefinedException
     */
    function send($route)
    {
        if (is_null($this->tenant))
            throw new TenantNotDefinedException("No tenant defined for requesting API route '$route'");

        $key = $this->tenant;
        $tenants = [$key => $this->tenants[$key]];
        return $this->makeRequest($tenants, $route);
    }

    /**
     * @param $tenants
     * @param $route
     * @return $this
     */
    private function makeRequest($tenants, $route)
    {
        $verb = strtolower($this->verb);
        $this->responses = [];
        $options = [];
        $params = $this->getParams();
        if (!empty($params)) {
            $options['json'] = $params;
        }
        if($this->isQueueEnabled()){
            return $this->queueRequest(compact('verb', 'tenants', 'route', 'options'));
        }
        foreach ($tenants as $key => $tenant) {
            $uri = $this->makeUri($route, $tenant);
            audit(compact('key', 'tenant', 'verb', 'uri'), __METHOD__);
            $response = $this->client->$verb($this->makeUri($route, $tenant), $options);
            $this->responses[$key] = clone $response;
        }
        return $this;
    }

    /**
     * @param array $payload
     * @return $this
     */
    private function queueRequest(array $payload){
        audit($payload, __METHOD__);
        \Queue::push('services\Jobs\RemoteApiRequestJob',
            $payload,
            config('cache.prefix')
        );
        return $this;
    }

    /**
     * @param array $payload
     */
    public function resumeRequest(array $payload){
        audit($payload, __METHOD__);

        $verb = $payload['verb'];
        $tenants = $payload['tenants'];
        $route = $payload['route'];
        $options = $payload['options'];

        foreach ($tenants as $key => $tenant) {
            $response = $this->client->$verb($this->makeUri($route, $tenant), $options);
            $this->responses[$key] = clone $response;
        }
    }

    /**
     * @param $route
     * @param null $domain
     * @return string
     */
    private function makeUri($route, $domain = null)
    {
        if (is_null($domain))
            $domain = config('app.url');

        return $domain . '/' . ltrim($route, '/');
    }

    /**
     * @return array
     */
    function getResponses()
    {
        return $this->responses;
    }

    /**
     * @return array
     */
    private function responsesArray()
    {
        $data = [];
        foreach ($this->responses as $key => $response) {
            $data[$key] = $response->getResponseBody();
        }
        return $data;
    }

    /**
     *
     */
    protected function cleanUp()
    {
        $this->tenant = null;
        $this->responses = [];
        $this->broadcast_mode = false;
        $this->verb = 'GET';
        $this->queue_mode = config('remote.internal.queue', false);
    }

    /**
     * @param $key
     * @return $this
     * @throws TenantNotFoundException
     */
    function toTenant($key)
    {
        $key = strtoupper($key);
        $tenant_exists = isset($this->tenants[$key]);
        if ($tenant_exists) {
            $this->tenant = $key;
            return $this;
        } else {
            throw new TenantNotFoundException("Tenant [$key] not found in the configurations array");
        }
    }

    /*
     *
     */
    function response()
    {
        $data = $this->responsesArray();
        $response = $this->broadcast_mode ? $data : (isset($data[$this->tenant]) ? $data[$this->tenant] : null);
        $this->cleanUp();
        return $response;
    }

}