<?php


namespace services\Deploy;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;

class NginxDeployCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'deploy:nginx';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create all config files for Nginx server';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $repository = new NginxConsoleRepository();
        $repository->setConsole($this);
        $scope = $this->option('scope');
        $this->info("Generating deploy files within [$scope] scope");
        $repository->setEnv($scope);

        $move = $this->option('move');

        if($move){
            $this->comment('WARNING: You are about to move all Nginx config files...');
            $bool = $this->confirm('Are You sure to re-deploy all Nginx conf files? (original files will be overwritten)');
            if($bool)
                $repository->move();
            return;
        }


        $repository->make();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('move', null, InputOption::VALUE_OPTIONAL, 'Move Nginx files instead of generating them', null),
            array('scope', null, InputOption::VALUE_OPTIONAL, 'Bind env to this scope', 'live'),
        );
    }
}