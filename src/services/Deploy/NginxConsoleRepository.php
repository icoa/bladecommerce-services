<?php


namespace services\Deploy;


use Illuminate\Console\Command;
use View;

class NginxConsoleRepository
{
    /**
     * @var Command
     */
    protected $console;

    /**
     * @var string
     */
    protected $env;

    /**
     * @var string
     */
    protected $target_dir;

    /**
     * @var string
     */
    protected $nginx_dir = '/etc/nginx/';

    /**
     * @return Command
     */
    protected function getConsole()
    {
        return $this->console;
    }

    /**
     * @param Command $console
     */
    public function setConsole(Command $console)
    {
        $this->console = $console;
    }

    /**
     * @return string
     */
    public function getEnv()
    {
        return $this->env;
    }

    /**
     * @param null $env
     * @return NginxConsoleRepository
     */
    public function setEnv($env)
    {
        $this->env = $env;
        $this->target_dir = base_path("resources/nginx/{$env}/");
        if (!is_dir($this->target_dir . 'common')) {
            if (!mkdir($concurrentDirectory = $this->target_dir . 'common') && !is_dir($concurrentDirectory)) {
                throw new \RuntimeException(sprintf('Directory "%s" was not created', $concurrentDirectory));
            }
        }
        return $this;
    }

    /**
     * @param string $key
     * @param null $default
     * @return mixed
     */
    protected function getConfig($key, $default = null)
    {
        return config("deploy.{$this->env}.{$key}", $default);
    }

    /**
     * @return array
     */
    protected function getAllConfig()
    {
        $config = config("deploy.{$this->env}", []);
        if(!array_has($config, 'include_path')){
            array_set($config, 'include_path', '');
        }else{
            array_set($config, 'include_path', rtrim(array_get($config, 'include_path'), '/') . '/');
        }
        return $config;
    }

    /**
     * @param $scope
     * @param bool $common
     * @return string
     */
    protected function getTargetFile($scope, $common = false)
    {
        return $this->target_dir . (($common) ? 'common/' . $scope . '.conf' : $scope . '.conf');
    }

    /**
     * @param $scope
     * @param bool $common
     * @return string
     */
    protected function getNginxTargetFile($scope, $common = false)
    {
        if($scope === 'nginx'){
            return $this->nginx_dir . 'nginx.conf';
        }
        return $this->nginx_dir . (($common) ? 'common/' . $scope . '.conf' : 'sites-available/' . $scope . '.conf');
    }

    /**
     * @return array
     */
    protected function getMainScopes()
    {
        return ['nginx', 'ssl', 'cdn'];
    }

    /**
     * @return array
     */
    protected function getCommonScopes()
    {
        return [
            'blacklist', 'brotli', 'cors', 'cors-map', 'limits', 'powered', 'ssl', 'mobile', 'fastcgi_cache', 'security', 'sfc', 'sfc_headers'
        ];
    }

    /**
     *
     */
    public function make()
    {
        $console = $this->getConsole();

        $common_scopes = $this->getCommonScopes();
        foreach ($common_scopes as $scope) {
            $content = View::make('nginx.common.' . $scope, $this->getAllConfig());
            $target_file = $this->getTargetFile($scope, true);
            file_put_contents($target_file, $content);
            $console->info("Configuration (common) file [$target_file] successfully generated");
        }

        $main_scopes = $this->getMainScopes();
        foreach ($main_scopes as $scope) {
            $content = View::make('nginx.' . $scope, $this->getAllConfig());
            $target_file = $this->getTargetFile($scope);
            file_put_contents($target_file, $content);
            $console->info("Configuration (main) file [$target_file] successfully generated");
        }
    }

    /**
     *
     */
    public function move()
    {
        $console = $this->getConsole();

        if(!is_dir($this->nginx_dir)){
            $console->error("WARNING: nginx dir [{$this->nginx_dir}] cannot be found! Can't do anything...");
            return;
        }

        if(!is_writable($this->nginx_dir)){
            $console->error("WARNING: nginx dir [{$this->nginx_dir}] is not writable! Can't do anything...");
            return;
        }

        $common_scopes = $this->getCommonScopes();
        foreach ($common_scopes as $scope) {
            $source_file = $this->getTargetFile($scope, true);
            $target_file = $this->getNginxTargetFile($scope, true);
            copy($source_file, $target_file);
            $console->info("Configuration (common) file [$source_file] successfully copied to [$target_file]");
        }

        $main_scopes = $this->getMainScopes();
        foreach ($main_scopes as $scope) {
            $source_file = $this->getTargetFile($scope);
            $target_file = $this->getNginxTargetFile($scope);
            copy($source_file, $target_file);
            $console->info("Configuration (main) file [$source_file] successfully copied to [$target_file]");
        }

        $console->comment('All conf files have been copied/deployed to proper paths.');
        $console->comment('Now You should remove all links in "/etc/nginx/sites-enabled" are create the new ones that point to regular conf files:');
        $console->comment('cd /etc/nginx/sites-enabled');
        $console->comment('ln -s ../sites-available/cdn.conf cdn');
        $console->comment('ln -s ../sites-available/ssl.conf ssl');
    }
}