<?php

namespace services;

class GeoIpLoc
{

    static function country($ip)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "http://www.geoplugin.net/xml.gp?ip=" . $ip);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_TIMEOUT, 1);
        $ip_data_in = curl_exec($ch); // string
        curl_close($ch);

        $ip_data = simplexml_load_string($ip_data_in);

        if (isset($ip_data->geoplugin_countryCode)) {
            return $ip_data->geoplugin_countryCode;
        }
        return null;
    }

}