<?php


namespace services\Fees;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use Order;
use Format;
use League\Csv\Writer;
use Exception;
use SplFileObject;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;
use services\Fees\Repositories\FeesRemoteRepository;
use services\Ftp\Connection;

class FeesCsvWriter
{

    const ROW_TYPE_SUMMARY = 9;
    const ROW_TYPE_STARLING = 2;
    const ROW_TYPE_SELLING = 1;

    const CSV_DELIMITER = ";";
    const CSV_NEWLINE = "\r\n";


    /**
     * @var Order[]
     */
    protected $orders;

    /**
     * @var array
     */
    protected $lines = [];

    /**
     * @var null
     */
    protected $week = null;

    /**
     * @var null
     */
    protected $year = null;

    /**
     * @var Filesystem
     */
    protected $files;

    /**
     * @var FeesCsvWriter
     */
    protected $writer;

    /**
     * @var FeesRemoteRepository
     */
    protected $remoteRepository;

    /**
     * @var Connection
     */
    protected $ftp;


    function __construct(Filesystem $files, FeesRemoteRepository $remoteRepository, Connection $ftp)
    {
        $this->files = $files;
        $this->remoteRepository = $remoteRepository;
        $this->ftp = $ftp;
    }

    /**
     * @return array
     */
    protected function getHeadings()
    {
        return [
            'company' => 'Soc.',
            'code' => 'codice negozio',
            'customer' => 'Cliente',
            'org' => 'OrgCm',
            'es' => 'Es.',
            'receipt_date' => 'Data Scontrino',
            'receipt_number' => 'numero ricevuta',
            'receipt_row' => 'Riga Scontrino',
            'row_type' => 'Tipo Riga',
            'reason' => 'Causale',
            'cancelled' => 'cancellato',
            'suspended' => 'Movimento Sospeso',
            'currency' => 'Div.',
            'section' => 'Rep.',
            'clerk' => 'Commesso',
            'grm1' => 'GrM1',
            'grm4' => 'GrM4',
            'sku' => 'Materiale',
            'qty' => 'Quantita',
            'unit' => 'UM',
            'amount_gross' => 'Ricavo lordo riga',
            'amount_net' => 'Ricavo Netto riga',
            'vat_code' => 'Codice IVA',
            'tax_rate' => 'Aliquota IVA',
            'payment_type' => 'Tipo Pagamento',
            'amount_payment' => 'Importo pagamento',
            'order_reference' => 'Ordine web',
            'transaction_id' => 'ID Transazione',
            'customer_details' => 'Dett. Cliente',
            'shipping_country' => 'Paese Spedizione',
        ];
    }

    /**
     * @param Order[] $orders
     * @return $this
     */
    function setOrders($orders)
    {
        $this->orders = $orders;
        return $this;
    }

    /**
     * @return Collection
     */
    protected function getRows()
    {
        /* Constants */
        $company = config('fees.company_code');
        $org = config('fees.organization_code');
        $customer = config('fees.sap_code');
        $code = 'W' . $company;

        $rows = Collection::make([]);

        $pivotDay = $day = null;

        if ($this->orders and !empty($this->orders)):

            foreach ($this->orders as $order) {

                //we must skip all orders with sap invoice
                if (true) {

                    $order->setInnerScope(Order::SCOPE_FEES);
                    $products = $order->getProducts();
                    $docDate = Carbon::parse($order->invoice_date);
                    $invoice_number = $order->getFeeCodeAttribute();
                    $row_counter = 0;
                    $day = $docDate->format('dmY');
                    $customer = $order->getSapPaymentCustomerCode();
                    $customer_details = $order->getFeeCustomerNameDescription();
                    $transaction_id = $order->getFeeTransactionCodeAttribute();
                    $shipping_country = $order->getFeeShippingCountryAttribute();

                    //Handle the day summary
                    if ($pivotDay != $day) {
                        //add summary rows for pivot day
                        $this->addSummaryRows($rows, $pivotDay);
                        $pivotDay = $day;
                    }

                    foreach ($products as $product) {
                        $row_counter++;
                        $row = [
                            /* Constants and default values */
                            'company' => $company,
                            'org' => $org,
                            'customer' => $customer,
                            'customer_details' => $customer_details,
                            'transaction_id' => $transaction_id,
                            'shipping_country' => $shipping_country,
                            'code' => $code,
                            'es' => date('Y'),
                            'cancelled' => null,
                            'suspended' => null,
                            'section' => null,
                            'clerk' => null,
                            'grm1' => null,
                            'grm4' => null,
                            'payment_type' => null,
                            'amount_payment' => 0,
                            'unit' => 'ST',
                            'currency' => 'EUR',
                            'order_reference' => $order->reference,

                            /* Dynamic values */
                            'receipt_date' => $docDate->format('dmY'),
                            'receipt_row' => $row_counter,
                            'receipt_number' => $invoice_number,
                            'sku' => $product->getFeesSkuAttribute(),
                            'qty' => $product->getFeesProductQuantityAttribute(),
                            'tax_rate' => $product->getFeesTaxRateAttribute(),
                            'vat_code' => $product->getFeesVatCodeAttribute(),
                            'amount_gross' => $product->getFeesAmountGrossAttribute(),
                            'amount_net' => $product->getFeesAmountNetAttribute(),

                            /* Variable values (may change with the context) */
                            'row_type' => $product->getFeesRowTypeAttribute(),
                            'reason' => $product->getFeesReasonAttribute(),
                            'payment_source' => $product->getFeesPaymentSourceAttribute(), //this is not needed in the CSV, but it is required for summary calculations
                        ];

                        audit($row, __METHOD__ . '::ADDING ROW');
                        audit($pivotDay, __METHOD__ . '::$pivotDay');
                        $rows->push($row);
                    }

                    //add additions costs per-order, like shipping + payment costs
                    $this->addOrderCostsRows($rows, $order);

                }

            }

            //add summary rows for last day
            $this->addSummaryRows($rows, $day);

        endif;

        return $rows;
    }


    /**
     * @param Collection $rows
     */
    private function addSummaryRows(Collection &$rows, $day)
    {
        if (!$rows->isEmpty()) {

            $summaryRow = $rows->first(); //to get all 'constant' values, get the first element
            $lastRow = $rows->last();
            //reset not-needed values for summary rows
            $summaryRow['receipt_date'] = $lastRow['receipt_date'];
            $summaryRow['receipt_row'] = null;
            $summaryRow['receipt_number'] = null;
            $summaryRow['payment_source'] = null;
            $summaryRow['sku'] = null;
            $summaryRow['qty'] = null;
            $summaryRow['unit'] = null;
            $summaryRow['vat_code'] = null;
            $summaryRow['reason'] = null;
            $summaryRow['amount_gross'] = 0;
            $summaryRow['amount_net'] = 0;
            $summaryRow['amount_payment'] = 0;
            $summaryRow['tax_rate'] = 0;
            $summaryRow['grm1'] = null;
            $summaryRow['order_reference'] = null;
            $summaryRow['customer'] = null;
            $summaryRow['customer_details'] = null;
            $summaryRow['transaction_id'] = null;
            $summaryRow['shipping_country'] = null;
            $summaryRow['row_type'] = self::ROW_TYPE_SUMMARY; //this flags the row as a 'summary row'

            //clone the 'template' row
            $summaryItems = [
                'POS' => $summaryRow,
                'BON' => $summaryRow,
            ];

            $summaryItems['POS']['payment_type'] = 'POS';
            $summaryItems['BON']['payment_type'] = 'BON';

            //use the summary for CNT only if the Cash is enabled
            if (config('fees.cash_enabled', true)) {
                $summaryItems['CNT'] = $summaryRow;
                $summaryItems['CNT']['payment_type'] = 'CNT';
            }

            foreach ($rows as $dayRow) {
                $source = $dayRow['payment_source'];
                if ($dayRow['row_type'] != self::ROW_TYPE_SUMMARY and $dayRow['receipt_date'] == $day) {
                    audit($source . ' => ' . $dayRow['amount_gross'] * $dayRow['qty'], __METHOD__ . '::ADDING SUM');
                    $summaryItems[$source]['amount_payment'] += ((float)$dayRow['amount_gross'] * abs($dayRow['qty']));
                }
            }

            foreach ($summaryItems as $summaryItem) {
                //add the summary row only in the grant total is not 0
                audit($summaryItem, __METHOD__ . '::ADDING SUMMARY');
                $rows->push($summaryItem);
            }
        }
    }

    /**
     * @param Collection $rows
     * @param Order $order
     */
    private function addOrderCostsRows(Collection &$rows, Order $order)
    {
        $defaultTaxRate = $this->getDefaultTaxRate();

        if (!$rows->isEmpty()) {

            //if the order is a starling, treat all amounts as negative
            $factor = $order->isStarling() ? -1 : 1;
            $reason = $order->isStarling() ? 'RES' : 'VEN';

            $costsRow = $rows->last(); //to get all 'constant' values, get the last element
            $counter = $costsRow['receipt_row'];

            //add additional shipping costs
            if ($order->total_shipping > 0) {
                $counter++;
                //reset not-needed values for summary rows
                $costsRow['receipt_row'] = $counter;
                $costsRow['sku'] = config('fees.shipment_sku', '21000239');
                $costsRow['qty'] = 1 * $factor;
                $costsRow['reason'] = $reason;
                $costsRow['amount_gross'] = (float)($order->total_shipping_tax_incl * $factor);
                $costsRow['amount_net'] = (float)($order->total_shipping_tax_excl * $factor);
                $costsRow['tax_rate'] = $defaultTaxRate - 1;
                $costsRow['vat_code'] = ($defaultTaxRate - 1) * 100;

                $rows->push($costsRow);
            }

            //add additional payment costs
            if ($order->total_payment > 0) {
                $counter++;
                //reset not-needed values for summary rows
                $costsRow['receipt_row'] = $counter;
                $costsRow['sku'] = config('fees.additional_costs_sku', '21000146');
                $costsRow['qty'] = 1 * $factor;
                $costsRow['reason'] = $reason;
                $costsRow['amount_gross'] = (float)($order->total_payment_tax_incl * $factor);
                $costsRow['amount_net'] = (float)($order->total_payment_tax_excl * $factor);
                $costsRow['tax_rate'] = $defaultTaxRate - 1;
                $costsRow['vat_code'] = ($defaultTaxRate - 1) * 100;

                $rows->push($costsRow);
            }

            //add additional discount (negative)costs
            if ($order->total_discounts > 0) {
                $counter++;
                //reset not-needed values for summary rows
                $costsRow['receipt_row'] = $counter;
                $costsRow['sku'] = config('fees.additional_costs_sku', '21000146');
                $costsRow['qty'] = -1 * $factor;
                $costsRow['reason'] = $reason;
                $costsRow['amount_gross'] = (float)(-$order->total_discounts * $factor);
                $costsRow['amount_net'] = (float)(-\Core::untax($order->total_discounts, $defaultTaxRate) * $factor);
                $costsRow['tax_rate'] = $defaultTaxRate - 1;
                $costsRow['vat_code'] = ($defaultTaxRate - 1) * 100;

                $rows->push($costsRow);
            }

        }
    }

    /**
     * @return mixed
     */
    private function getDefaultTaxRate()
    {
        $taxGroup = \TaxGroup::getObj(\Cfg::get('TAX_GROUP_DEFAULT', 1));
        return $taxGroup->getDefaultTaxRatio();
    }

    /**
     * @param null|int $week
     * @return $this
     */
    public function setWeek($week = null)
    {
        if ((int)$week <= 0)
            $week = null;

        $this->week = $week;
        return $this;
    }

    /**
     * @return null|int
     */
    public function getWeek()
    {
        return is_null($this->week) ? Carbon::now()->weekOfYear : $this->week;
    }

    /**
     * @param null|int $year
     * @return $this
     */
    public function setYear($year = null)
    {
        if ((int)$year <= 0)
            $year = null;

        $this->year = $year;
        return $this;
    }

    /**
     * @return null|int
     */
    public function getYear()
    {
        return is_null($this->year) ? Carbon::now()->year : $this->year;
    }

    /**
     * @return string
     */
    public function getCsvFilePath()
    {
        $fileName = $this->getCsvFileName();
        return storage_path("morellato/csv/invoices/$fileName");
    }

    /**
     * @return string
     */
    public function getCsvFileName()
    {
        $week = $this->getWeek();
        $year = $this->getYear();
        $companyCode = config('fees.company_code');
        return "contab{$companyCode}-{$year}{$week}.csv";
    }

    /**
     * @param bool $skip_first_line
     * @param bool $force_generation
     * @return null|string
     */
    public function getCsvFileContent($skip_first_line = true, $force_generation = false)
    {
        $filePath = $this->getCsvFilePath();

        audit($filePath, __METHOD__ . '::filePath');

        if (!$this->files->exists($filePath)) {
            $this->make();
        } elseif ($force_generation) {
            $this->make();
        }

        $content = null;
        if (!$this->files->exists($filePath))
            return null;

        $content = $this->files->get($filePath);

        if (strlen($content) == 0)
            return null;

        if ($skip_first_line) {
            $lines = explode(self::CSV_NEWLINE, $content);
            array_shift($lines);
            $content = implode(self::CSV_NEWLINE, $lines);
        }

        return trim($content);
    }

    /**
     * @return array
     */
    public function makeCsvContent()
    {
        $headings = $this->getHeadings();
        $keys = array_keys($headings);
        $header = array_values($headings);

        $rows = $this->getRows();

        //audit($rows, __METHOD__ . '::rows');


        $float_columns = [
            'amount_gross',
            'amount_net',
            'amount_payment',
            'tax_rate',
        ];


        $contents = [$header];
        foreach ($rows as $row) {
            $line = [];
            foreach ($keys as $key) {
                if (in_array($key, $float_columns)) {
                    $row[$key] = Format::floatCsv($row[$key]);
                }
                $line[] = $row[$key];
            }
            $contents[] = $line;
        }

        return $contents;
    }

    /**
     * @param bool $regular
     * @return $this
     */
    public function make($regular = true)
    {
        $contents = $this->makeCsvContent();

        /**
         * La tabella deve essere in formato csv, con il nome di contab+codice società+"-"+numeroweek.csv esempio contab05-201814.csv.
         */
        $targetFilePath = $regular ? $this->getCsvFilePath() : $this->getCustomFilePath();

        $writer = Writer::createFromPath(new SplFileObject($targetFilePath, 'w'), 'w');
        $writer->setEncodingFrom('UTF-8');
        $writer->setDelimiter(self::CSV_DELIMITER); //the delimiter will be the tab character
        $writer->setNewline(self::CSV_NEWLINE); //use windows line endings for compatibility with some csv libraries
        //$writer->setOutputBOM(Writer::BOM_UTF8); //adding the BOM sequence on output
        //$writer->insertOne($header);
        $writer->insertAll($contents);

        return $this;
    }

    /**
     * @return string
     */
    public function getCustomFilePath()
    {
        return storage_path('corrispettivi.csv');
    }


    public function mergeFromRemote()
    {
        //skip the entire process if the option is not enabled
        if (config('fees.merge_csv_files', false) === false)
            return $this;

        //prepare the params for the remote request
        $route = 'api/remote/fetch-fee-file';

        $payload = [
            'year' => $this->getYear(),
            'week' => $this->getWeek(),
        ];

        audit(compact('route', 'payload'), __METHOD__ . '::payload');

        try {

            $currentFilePath = $this->getCsvFilePath();

            //check if the current file is empty, even with the headings;
            //is that is the case we have to create an empty file with the headings, since the current tenant could be empty but not the remote ones
            /*$currentContent = $this->getCsvFileContent(false);
            if (Str::length($currentContent) <= 0) {
                $headings = $this->getHeadings();
                $header = array_values($headings);
                $writer = Writer::createFromPath(new SplFileObject($currentFilePath, 'w'), 'w');
                $writer->setDelimiter(self::CSV_DELIMITER); //the delimiter will be the tab character
                $writer->setNewline(self::CSV_NEWLINE); //use windows line endings for compatibility with some csv libraries
                //$writer->setOutputBOM(Writer::BOM_UTF8); //adding the BOM sequence on output
                $writer->insertOne($header);
            }*/

            $response = $this->remoteRepository
                ->setParams($payload)
                ->broadcast()
                ->post($route)
                ->response();

            audit($response, __METHOD__ . '::response');

            //append the content to the current file
            foreach ($response as $tenant => $content) {
                if (Str::length($content) > 0){
                    $this->files->append($currentFilePath, $content . PHP_EOL);
                }
            }

        } catch (Exception $e) {
            audit_exception($e);
            audit_remote($e->getMessage(), 'getRemoteCsvFiles');
        }

        return $this;
    }


    function upload()
    {
        //skip the entire process if the option is not enabled
        if (config('fees.remote_storage', false) === false)
            return $this;

        try {

            $currentFilePath = $this->getCsvFilePath();
            $currentFileName = $this->getCsvFileName();

            $default = config('ftp.default');
            $ftp_config = config('ftp.connections.' . $default);
            $remoteSavePath = $ftp_config['inPath'];
            $remoteFilePath = $remoteSavePath . '/' . $currentFileName;

            audit(compact('currentFilePath', 'remoteFilePath'), __METHOD__);

            $this->ftp->connect();
            $this->ftp->upload($currentFilePath, $remoteFilePath, FTP_BINARY);

        } catch (Exception $e) {
            audit_exception($e);
            audit_remote($e->getMessage(), 'submitToExternalStorage');
        }

        return $this;
    }
}