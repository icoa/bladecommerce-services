<?php
namespace services\Fees\Traits;

use Order;

trait TraitFeesRma
{
    /**
     * @return array
     */
    function getProductsMatrixDetailsForNewOrder()
    {
        /* @var $order Order */
        $order = $this->getOrder();

        $order_products = $order->getProducts();

        $rma_products = $this->getProducts();

        $details = [];

        foreach ($order_products as $op) {
            //insert the reference anyway, than take it back if the conditions are met
            $details[$op->id] = $op->product_quantity;

            foreach ($rma_products as $rp) {

                if ($rp->product_id == $op->product_id) {
                    //if the quantities are not equal, than the items goes to the details difference
                    if ($rp->product_quantity != $op->product_quantity) {
                        $details[$op->id] = $op->product_quantity - $rp->product_quantity;
                        if ($details[$op->id] <= 0) {
                            unset($op->id);
                        }
                    }
                }

            }
        }

        return $details;
    }
}