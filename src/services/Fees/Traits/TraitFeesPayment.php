<?php
namespace services\Fees\Traits;

use Payment;

trait TraitFeesPayment
{
    /**
     * CAUSALE
     * cnt = contanti
     * bon = bonifici
     * pos = elettronici
     * @return string|null
     */
    function getFeesReasonAttribute()
    {
        $pivot = $this->module;

        // electronics payment
        if (in_array($pivot, [Payment::MODULE_PAYPAL, Payment::MODULE_CREDIT_CARD, Payment::MODULE_GIFTCARD]))
            return 'POS';

        if (in_array($pivot, [Payment::MODULE_BANKWIRE, Payment::MODULE_RIBA]))
            return 'BON';

        if (in_array($pivot, [Payment::MODULE_CASHONDELIVERY]))
            return 'CNT';

        return null;
    }
}