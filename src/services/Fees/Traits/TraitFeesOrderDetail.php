<?php
namespace services\Fees\Traits;

use Format;
use services\Fees\FeesCsvWriter;
use services\Membership\Models\Affiliate;

trait TraitFeesOrderDetail
{
    /**
     * TIPORIGA
     * 1 = vendite
     * 2 = resi
     * 9 = somme totali della giornata
     * @return string
     */
    function getFeesRowTypeAttribute()
    {
        $str = FeesCsvWriter::ROW_TYPE_SELLING;
        if ($this->getOrder()->isStarling())
            $str = FeesCsvWriter::ROW_TYPE_STARLING;

        return $str;
    }

    /**
     * CAUSALE
     * ven = vendite
     * res = resi
     * cnt = contanti
     * bon = bonifici
     * pos = elettronici
     * @return string
     */
    function getFeesReasonAttribute()
    {
        $str = 'VEN';

        /*$payment = $this->getOrder()->getPayment();
        if ($payment) {
            $result = $payment->getFeesReasonAttribute();
            if ($result)
                $str = $result;
        }*/

        if ($this->getOrder()->isStarling())
            $str = 'RES';

        return $str;
    }

    /**
     * PAYMENT SOURCE
     * cnt = contanti
     * bon = bonifici
     * pos = elettronici
     * @return string
     */
    function getFeesPaymentSourceAttribute()
    {
        $str = '';
        $payment = $this->getOrder()->getPayment();
        if ($payment) {
            $result = $payment->getFeesReasonAttribute();
            if ($result)
                $str = $result;
        }
        return $str;
    }

    /**
     * MATERIALE
     * @return string
     */
    function getFeesSkuAttribute()
    {
        //check if the products is 'affiliate' management
        if ($this->affiliate_id > 0) {
            $affiliate = Affiliate::getObj($this->affiliate_id);
            if ($affiliate)
                return (string)$affiliate->fees_sap_code;
        }

        //check if the product is marked as 'local'
        if ($this->isModeLocal() or $this->isModeOffline()) {
            $product = $this->getProduct();
            if ($product and !$product->isVirtual())
                return (string)config('fees.availability_local_sku');
        }

        $str = trim($this->product_sap_reference);

        if ($str == '')
            $str = trim($this->product_reference);

        return $str;
    }

    /**
     * Product quantity (negative in case of starling)
     * @return int
     */
    function getFeesProductQuantityAttribute()
    {
        $qty = (int)$this->product_quantity;
        if ($this->getOrder()->isStarling())
            $qty = $qty * -1;

        return $qty;
    }

    /**
     * Tax rate
     * @return float
     */
    function getFeesTaxRateAttribute()
    {
        return $this->tax_rate == 0 ? 0 : $this->tax_rate - 1;
    }

    /**
     * Tax rate
     * @return int
     */
    function getFeesVatCodeAttribute()
    {
        //"articolo 2" is when there are non-taxable items, like Gift Cards
        return $this->tax_rate == 0 ? 'articolo 2' : ($this->tax_rate - 1) * 100;
    }

    /**
     * Amount Gross (Ricavo lordo riga)
     * @return float
     */
    function getFeesAmountGrossAttribute()
    {
        $amount = (float)($this->cart_price_tax_incl);
        if($amount <= 0.2){
            $amount = 0;
        }

        if ($this->getOrder()->isStarling())
            $amount = $amount * -1;

        return $amount;
    }

    /**
     * Amount Net (Ricavo netto riga)
     * @return float
     */
    function getFeesAmountNetAttribute()
    {
        //$amount = (float)($this->product_item_price_tax_excl);
        $amount = (float)($this->cart_price_tax_excl);
        if($amount <= 0.2){
            $amount = 0;
        }

        if ($this->getOrder()->isStarling())
            $amount = $amount * -1;

        return $amount;
    }
}