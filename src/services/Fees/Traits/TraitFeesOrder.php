<?php
namespace services\Fees\Traits;

use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Order;
use Rma;
use Payment;
use OrderState;
use Core;
use PDF;
use File;
use Format;
use Site;
use PaymentState;
use Illuminate\Support\Str;

/**
 * Trait TraitFeesOrder
 * @package services\Fees\Traits
 * @mixin Order
 */
trait TraitFeesOrder
{

    /**
     * @return HasOne
     */
    function child_starling()
    {
        return $this->hasOne(Order::class, 'fee_parent_id')->where('invoice_type', Order::INVOICE_TYPE_STARLING);
    }

    /**
     * @return Order|null
     */
    function getChildStarling()
    {
        return $this->child_starling()->first();
    }

    /**
     * @return HasOne
     */
    function child_receipt()
    {
        return $this->hasOne(Order::class, 'fee_parent_id')->where('invoice_type', Order::INVOICE_TYPE_RECEIPT);
    }

    /**
     * @return Order|null
     */
    function getChildReceipt()
    {
        return $this->child_receipt()->first();
    }

    /**
     * @return BelongsTo
     */
    function fee_parent()
    {
        return $this->belongsTo(Order::class, 'fee_parent_id');
    }

    /**
     * @return Order|null
     */
    function getFeeParentOrder()
    {
        return $this->fee_parent()->first();
    }

    /**
     * @return bool
     */
    function hasFeeParent()
    {
        return $this->fee_parent_id > 0;
    }

    /**
     * @param Builder $query
     * @param $type
     * @return Builder
     */
    function scopeWithInvoiceType(Builder $query, $type)
    {
        return is_array($type) ? $query->whereIn('invoice_type', $type) : $query->where('invoice_type', $type);
    }

    /**
     * @param Builder $query
     * @param $type
     * @return Builder
     */
    function scopeWithoutInvoiceType(Builder $query, $type)
    {
        return $query->where(function ($subQuery) use ($type) {
            is_array($type) ? $subQuery->whereNotIn('invoice_type', $type) : $subQuery->where('invoice_type', '!=', $type);
            $subQuery->orWhereNull('invoice_type');
        });
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    function scopeOnlyInvoices(Builder $query)
    {
        $query->where('invoice_required', 1);
        return $this->scopeWithInvoiceType($query, Order::INVOICE_TYPE_INVOICE);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    function scopeOnlyReceipts(Builder $query)
    {
        $this->scopeWithFeesRequirements($query);
        return $this->scopeWithInvoiceType($query, Order::INVOICE_TYPE_RECEIPT);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    function scopeOnlyStarlings(Builder $query)
    {
        $this->scopeWithFeesRequirements($query);
        return $this->scopeWithInvoiceType($query, Order::INVOICE_TYPE_STARLING);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    function scopeWithoutStarlings(Builder $query)
    {
        return $this->scopeWithoutInvoiceType($query, Order::INVOICE_TYPE_STARLING);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    function scopeWithFees(Builder $query)
    {
        $this->scopeWithFeesRequirements($query);
        return $this->scopeWithInvoiceType($query, [Order::INVOICE_TYPE_RECEIPT, Order::INVOICE_TYPE_STARLING]);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    function scopeWithFeesRequirements(Builder $query)
    {
        $validPaymentMethods = self::getFeeValidPaymentMethods();
        return $query->where('invoice_required', 0)->whereIn('module', $validPaymentMethods);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    function scopeWithInvoiceSent(Builder $query)
    {
        return $query->where('invoice_required', 0)->where('invoice_sent', 1);
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    function scopeWithoutInvoiceSent(Builder $query)
    {
        return $query->where('invoice_required', 0)->where('invoice_sent', 0);
    }

    /**
     * @param Builder $query
     * @param null $date
     * @return Builder
     */
    function scopeWithFeesDate(Builder $query, $date = null)
    {
        if (is_null($date)) {
            $date = date('Y-m-d');
        }
        return $query->where('invoice_date', $date);
    }

    /**
     * @param Builder $query
     *
     * If the week parameter is null, the current week will be used
     * @param null $week
     * @return Builder
     */
    function scopeWithFeesWeek(Builder $query, $week = null)
    {
        if (is_null($week) or (int)$week <= 0) {
            $week = Carbon::now()->weekOfYear;
        }
        return $query->whereRaw("WEEKOFYEAR(invoice_date)=$week");
    }

    /**
     * @param Builder $query
     *
     * If the year parameter is null, the current year will be used
     * @param null $year
     * @return Builder
     */
    function scopeWithFeesYear(Builder $query, $year = null)
    {
        if (is_null($year) or (int)$year <= 0) {
            $year = Carbon::now()->year;
        }
        return $query->whereRaw("YEAR(invoice_date)=$year");
    }

    /**
     * @param Builder $query
     * @return Builder
     */
    function scopeWithCorrectDateForFees(Builder $query)
    {
        $pivot_date = config('fees.pivot_date', '2018-10-01');
        return $query->where('created_at', '>=', $pivot_date);
    }

    /**
     * @return bool
     */
    function isReceipt()
    {
        return $this->invoice_type == Order::INVOICE_TYPE_RECEIPT;
    }

    /**
     * @return bool
     */
    function isInvoice()
    {
        return $this->invoice_type == Order::INVOICE_TYPE_RECEIPT;
    }

    /**
     * @return bool
     */
    function isStarling()
    {
        return $this->invoice_type == Order::INVOICE_TYPE_STARLING;
    }

    /**
     * @return bool
     */
    function isFee()
    {
        return $this->isReceipt() or $this->isStarling();
    }

    /**
     * @return bool
     */
    function isInvoiceRequired()
    {
        return $this->invoice_required == 1;
    }

    /**
     * @return bool
     */
    function hasInvoiceNumber()
    {
        return $this->invoice_number > 0;
    }

    /**
     * @return array
     */
    static function getFeeValidPaymentMethods()
    {
        $validPaymentModules = [
            Payment::MODULE_PAYPAL,
            Payment::MODULE_CREDIT_CARD,
            Payment::MODULE_BANKWIRE,
            Payment::MODULE_RIBA,
            Payment::MODULE_GIFTCARD,
        ];

        //if the CASH management is enabled, add the 'cash-on-delivery' payment module
        if (config('fees.cash_enabled', true)) {
            $validPaymentModules[] = Payment::MODULE_CASHONDELIVERY;
        }

        return $validPaymentModules;
    }

    /**
     * @return array
     */
    static function getFeeValidOrderStates()
    {
        $validOrderStates = [
            OrderState::STATUS_CANCELED,
            OrderState::STATUS_CANCELED_AFFILIATE,
            OrderState::STATUS_REFUNDED,
            OrderState::STATUS_REFUNDED_PARTIALLY,
        ];

        return $validOrderStates;
    }

    /**
     * @return array
     */
    static function getFeeValidPaymentStates()
    {
        $validStates = [
            PaymentState::STATUS_COMPLETED,
            PaymentState::STATUS_PENDING_CASH,
        ];

        return $validStates;
    }

    /**
     * @return Carbon
     */
    function getFeeDate()
    {
        $date = is_null($this->invoice_date) ? Carbon::now() : Carbon::parse($this->invoice_date);
        return $date;
    }

    /**
     * @return string
     */
    function getFeeOrgAttribute()
    {
        return (string)config('fees.organization_code', 'XX');
    }

    /**
     * @return null|string
     */
    function getFeeNumberAttribute()
    {
        $number = !$this->hasInvoiceNumber() ? null : str_pad($this->invoice_number, config('fees.max_digits', 5), '0', STR_PAD_LEFT);
        return $number;
    }

    /**
     * @return null|string
     */
    function getFeeCodeAttribute()
    {
        /**
         * For SAP a 'fee-code' is built upon:
         * - invoice date year, ex '2018'
         * - organization unit code, ex '01'
         * - invoice number (max 5 digits), ex '00053'
         *
         * Example: '2018-0100001'
         */
        if (is_null($this->invoice_date) or !$this->hasInvoiceNumber()) {
            return null;
        }

        $year = $this->getFeeDate()->format('Y');
        $org = $this->getFeeOrgAttribute();
        $number = $this->getFeeNumberAttribute();

        return $year . '-' . $org . $number;
    }

    /**
     * @return bool
     */
    function canCreateFee()
    {
        //audit($this->toArray(), __METHOD__);
        $test = false;
        /**
         * an order is a valid candidate for a 'fee' if:
         *
         * the 'invoice' is not required
         * there are no setted 'document_number' (invoice_number)
         * the payment module is 'online' or a 'bank transfer'
         * the order status is not 'CANCELLED', 'AFFILIATE_CANCELLED', 'REFUNDED', 'REFUNDED_PARTIALLY'
         */

        $validPaymentModules = self::getFeeValidPaymentMethods();

        $validOrderStates = self::getFeeValidOrderStates();

        $debug_tests = [
            'not_master' => !$this->isMaster() ? 'passed' : 'not-passed',
            'invoice_required' => !$this->isInvoiceRequired() ? 'passed' : 'not-passed',
            'module' => in_array($this->module, $validPaymentModules) ? 'passed' : 'not-passed',
            'status' => !in_array($this->status, $validOrderStates) ? 'passed' : 'not-passed',
        ];

        audit($debug_tests, __METHOD__);

        if (
            !$this->isMaster() and
            !$this->isInvoiceRequired() and
            in_array($this->module, $validPaymentModules) and
            !in_array($this->status, $validOrderStates)
        ) {
            $test = true;
        }
        return $test;
    }

    /**
     * @return bool
     */
    function canCreateReceipt()
    {
        return !$this->hasInvoiceNumber()
        and !$this->isReceipt()
        and $this->isValidForReceipt();
    }

    /**
     * @return bool
     */
    function canCreateStarling()
    {
        return $this->canCreateFee()
        and $this->hasInvoiceNumber()
        and !$this->isStarling()
        and $this->isReceipt();
    }

    /**
     * @return bool
     */
    function isValidForReceipt()
    {
        //check if the order has a valid receipt requirement, even if the 'receipt' has not been generated
        $tests = [
            'can_create_fee' => $this->canCreateFee(),
            'payment_status' => in_array($this->payment_status, $this->getFeeValidPaymentStates()),
            'no_sap_invoice' => $this->hasSapInvoice() === false,
        ];

        audit($tests, __METHOD__);

        foreach ($tests as $test) {
            if ($test === false)
                return false;
        }

        return true;
    }

    /**
     *
     */
    function createReceipt()
    {
        $this->createFeeByType(Order::INVOICE_TYPE_RECEIPT);
    }

    /**
     * @return Order
     */
    function createStarling()
    {
        $order = \OrderManager::createStarlingOrder($this);
        if ($order)
            $order->createFeeByType(Order::INVOICE_TYPE_STARLING);

        return $order;
    }

    protected function createFeeByType($invoice_type)
    {
        /**
         * A "receipt" is made of:
         *
         * - progressive 'invoice_number', maintained by the system
         * - invoice_date, taken from the 'created' build event
         * - invoice_type, which is always INVOICE_TYPE_RECEIPT
         */
        $number_taken = false;
        try {
            $invoice_number = Core::getInvoiceNumber();
            $invoice_date = date('Y-m-d');
            $data = compact('invoice_number', 'invoice_date', 'invoice_type');
            audit($data, __METHOD__);
            $this->fill($data);
            $this->save();

            //now create the PDF file for the document
            $this->createFeeDocumentFile();
            $number_taken = true;
        } catch (Exception $e) {
            audit_exception($e, __METHOD__);
            $number_taken = false;
            $data = ['invoice_number' => 0, 'invoice_date' => null, 'invoice_type' => null];
            $this->fill($data);
            $this->save();
        }

        //if the number has been taken, increment the general number no matter what
        if ($number_taken) {
            Core::setInvoiceNumber($invoice_number);
        }
    }

    /**
     * @return array
     */
    function getReceiptData()
    {
        $invoice = $this->getInvoice();

        $data = $this->getDocumentData();

        $data += [
            'invoice_number' => $invoice['number'],
            'invoice_date' => Format::date($invoice['date']),
            'order_reference' => $this->reference,
            'order_number' => $this->reference,
            'notes' => $invoice['notes'],
        ];

        $data['filename'] = $invoice['filename'];
        $data['filepath'] = $invoice['filepath'];
        $data['hasFile'] = $invoice['hasFile'];
        $data['title'] = trans('fee.pdf.title', [
            'invoice_number' => $data['invoice_number'],
            'invoice_date' => $data['invoice_date'],
            'billing_header' => $data['billing_header'],
        ]);

        return $data;
    }


    /**
     * @return array|null
     */
    function getReceiptParams()
    {
        $invoice_id = $this->invoice_number;
        if ($invoice_id <= 0) {
            return null;
        }
        $number = $this->getFeeCodeAttribute();
        $data = [
            'type' => 'invoice',
            'id' => $this->invoice_number,
            'number' => $number,
            'code' => $number,
            'reference' => $number,
            'date' => Format::date($this->invoice_date),
            'notes' => $this->invoice_notes
        ];
        $filename = 'INVOICE_' . $this->id;
        $filepath = storage_path("files/invoices/$filename.pdf");
        $hasFile = File::exists($filepath);
        $url = Site::root() . "/files/invoice/{$this->id}?secure_key=" . $this->secure_key;

        $data['filename'] = $filename;
        $data['filepath'] = $filepath;
        $data['hasFile'] = $hasFile;
        $data['url'] = $url;
        return $data;
    }


    /**
     * Create the PDF file for that document
     */
    function createFeeDocumentFile()
    {
        //force locale change
        \Core::swapLocale($this->lang_id);

        //change order inner scope
        $this->setInnerScope(Order::SCOPE_DOCUMENT);

        $data = $this->getReceiptData();
        audit($data, __METHOD__);
        $type = $this->invoice_type;
        if ($type == '')
            $type = 'receipt';

        $pdf = PDF::loadView('pdf.' . $type, $data);
        if (File::exists($data['filepath'])) {
            File::delete($data['filepath']);
        }
        $output = $pdf->output();
        File::put($data['filepath'], $output);

        //restore locale
        \Core::restoreLocale();

        //restore order inner scope
        //change order inner scope
        $this->setInnerScope(Order::SCOPE_DEFAULT);
    }

    /**
     * @return null|Rma
     */
    function getLatestRma()
    {
        $found = null;
        $rmas = $this->getRma();
        foreach ($rmas as $rma) {
            if ($this->getParam('latest_rma_id') == $rma->id) {
                return $rma;
            }
        }
        return $found;
    }

    /**
     * Checks if the "receipt" code should be required, depending on its "payment method"
     *
     * @return bool
     */
    function isLocalReceiptFeeRequired()
    {
        $test = false;
        if (in_array($this->module, [
            Payment::MODULE_SHOP_POSTPAID,
            Payment::MODULE_SHOP_PREPAID,
        ])) {
            $test = true;
        }

        if ($this->module == Payment::MODULE_BANKWIRE and $this->payment_status == PaymentState::STATUS_COMPLETED) {
            $test = true;
        }

        if ($this->module == Payment::MODULE_RIBA and $this->payment_status == PaymentState::STATUS_COMPLETED) {
            $test = true;
        }

        if ($test === true) {
            return $test;
        }

        // custom 'online' transactions
        if ($this->isChild() == false
            and in_array($this->module, [
                Payment::MODULE_CREDIT_CARD,
                Payment::MODULE_PAYPAL,
            ]) and $this->payment_status == PaymentState::STATUS_COMPLETED
            and $this->hasTransactions() == false
        ) {
            $test = true;
        }

        return $test;
    }

    /**
     * Prints out the "receipt" code for the Order
     * Warning: a "receipt" code is valid only for Bankwire (CRO Code) or for Shop payment (Scontrino)
     *
     * @return string|null
     */
    function getLocalReceiptFeeAttribute()
    {
        return ($this->isLocalReceiptFeeRequired() and trim($this->receipt) != '') ? trim($this->receipt) : null;
    }

    /**
     * Prints out the "receipt" code for the Order
     * Warning: a "receipt" code is valid only for Bankwire (CRO Code) or for Shop payment (Scontrino)
     *
     * @return string
     */
    function getLocalReceiptFeeHtmlAttribute()
    {
        $isRequired = $this->isLocalReceiptFeeRequired();
        $receiptCode = $this->getLocalReceiptFeeAttribute();
        $hasReceiptCode = $receiptCode != null;
        $str = "<span class='label label-info'>Non richiesto per questo ordine</span>";
        if ($isRequired) {
            if ($hasReceiptCode) {
                $str = "<span class='label label-success'>$receiptCode</span>";
            } else {
                $str = "<span class='label label-important'>CODICE MANCANTE</span>";
            }
        }
        return $str;
    }

    /**
     * @return bool
     */
    function hasLocalReceiptFeeWarning()
    {
        return (($this->isReceipt() or $this->isStarling()) and $this->isLocalReceiptFeeRequired() === true and trim($this->receipt) == '');
    }

    /**
     * @param Builder $query
     * @param bool $includeOnlinePayments
     * @return Builder
     */
    function scopeWithLocalReceiptFeeWarning(Builder $query, $includeOnlinePayments = false)
    {
        //add minimum date
        $minDate = Carbon::now()->addMonth(-1)->format('Y-m-d');
        $query = $this->withFees($query);
        $builder = $query->whereNull('receipt')
            ->whereDate('created_at', '>=', $minDate)
            ->whereIn('module', [Payment::MODULE_BANKWIRE, Payment::MODULE_RIBA])
            ->where('payment_status', PaymentState::STATUS_COMPLETED);

        if (true === $includeOnlinePayments) {
            $builder->where(function ($innerQuery) {
                $innerQuery->where(function ($subQuery) {
                    $subQuery->whereIn('module', [Payment::MODULE_BANKWIRE, Payment::MODULE_RIBA]);
                    $subQuery->orWhere(function ($nesterQuery) {
                        $nesterQuery->whereIn('module', [
                            Payment::MODULE_PAYPAL,
                        ])->whereRaw("EXISTS
    (SELECT count(transactions_paypal.id) as cnt
     FROM `transactions_paypal` JOIN `transactions` ON transactions_paypal.transaction_ref = transactions.id
     WHERE transactions.order_id = orders.id HAVING cnt=0)");
                    });
                    $subQuery->orWhere(function ($nesterQuery) {
                        $nesterQuery->whereIn('module', [
                            Payment::MODULE_CREDIT_CARD,
                        ])->whereRaw("EXISTS
    (SELECT count(transactions_safepay.id) as cnt
     FROM `transactions_safepay` JOIN `transactions` ON transactions_safepay.transaction_ref = transactions.id
     WHERE transactions.order_id = orders.id HAVING cnt=0)");
                    });
                });
            });
        }

        return $builder;
    }

    /**
     * Return the "transaction code" for the payment method, please note that:
     *
     * - if the "payment" is BANKWIRE, than the code is "receipt"
     * - if the "payment" is PAYPAL, than the code must be fetched from the latest "transaction" in the transactions table
     *
     * @return string|null
     */
    function getFeeTransactionCodeAttribute()
    {
        $code = null;

        switch ($this->module) {
            case Payment::MODULE_BANKWIRE:
            case Payment::MODULE_RIBA:
                $code = $this->receipt;
                break;

            case Payment::MODULE_PAYPAL:
                $transactions = $this->getTransactions();
                //default to the receipt, if the are transactions the code is override
                $code = $this->receipt;
                foreach ($transactions as $transaction) {
                    if ($transaction->payment_status == 'Completed' and strlen($transaction->transaction_id) > 0) {
                        $code = $transaction->transaction_id;
                        break;
                    }
                }
                break;

            case Payment::MODULE_CREDIT_CARD:
                $transactions = $this->getTransactions();
                //default to the receipt, if the are transactions the code is override
                $code = $this->receipt;
                foreach ($transactions as $transaction) {
                    if ($transaction->status == 'completed' and strlen($transaction->externaltransactionid) > 0) {
                        $code = $transaction->externaltransactionid;
                        break;
                    }
                }
                break;
        }

        //if no code has been assigned, then reset to NULL
        if (strlen($code) == 0)
            $code = null;

        //at this point, if the code is still NULL and the Order is a child, maybe the transaction has been saved on the "MASTER" order
        if (is_null($code) and $this->isChild()) {
            $master = $this->getParent();
            if ($master) {
                $code = $master->getFeeTransactionCodeAttribute();
            }
        }

        //at this point, if the code is still NULL and the Order has been forked from a Fee Parent, maybe the transaction has been saved on the "MASTER" order
        if (is_null($code) and $this->hasFeeParent()) {
            $parent = $this->getFeeParentOrder();
            if ($parent) {
                $code = $parent->getFeeTransactionCodeAttribute();
                //re-check the grand-parent if the order has been double-forked
                if (is_null($code) and $parent->hasFeeParent()) {
                    $grandParent = $this->getFeeParentOrder();
                    if ($grandParent) {
                        $code = $grandParent->getFeeTransactionCodeAttribute();
                    }
                }
            }
        }

        return $code;
    }

    /**
     * @return string
     */
    function getFeeCustomerNameDescription()
    {
        $str = '';
        $customer = $this->getCustomer();
        if ($customer) {
            $str = Str::upper(trim($customer->name));
        }
        $shippingAddress = $this->getShippingAddress();
        $billingAddress = $this->getBillingAddress();
        if ($billingAddress) {
            $str .= ' - ' . Str::upper(trim($billingAddress->city));
        } elseif ($shippingAddress) {
            $str .= ' - ' . Str::upper(trim($shippingAddress->city));
        }
        return $str;
    }


    /**
     * @return string
     */
    function getFeeShippingCountryAttribute()
    {
        $str = '';
        /* @var \Address $shippingAddress */
        $shippingAddress = $this->getShippingAddress();
        if ($shippingAddress) {
            $str = $shippingAddress->countryCode();
        }
        return $str;
    }
}