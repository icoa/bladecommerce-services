<?php

namespace services\Fees\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use services\Fees\Repositories\FeesBackendRepository;

class ScanOrderForFeesCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'fees:scan';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scan all order with valid requirements for fee creation, and try to create missing receipts';

    /**
     * @var FeesBackendRepository
     */
    protected $backendRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->backendRepository = app('services\Fees\Repositories\FeesBackendRepository');
        $this->backendRepository->setConsole($this);
        if(feats()->sapReceipts()){
            $this->comment("Warning: this feature is disabled, exiting now!");
            return;
        }
        $this->info("Scanning all orders for missing fees...");
        $this->backendRepository->checkOrdersForMissingReceipts();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}