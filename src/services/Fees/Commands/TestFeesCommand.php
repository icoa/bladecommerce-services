<?php

namespace services\Fees\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use services\Fees\Repositories\FeesBackendRepository;

class TestFeesCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'fees:test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Performs some tests on fees env.';

    /**
     * @var FeesBackendRepository
     */
    protected $backendRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->backendRepository = app('services\Fees\Repositories\FeesBackendRepository');
        $this->backendRepository->setConsole($this);
        $this->info("Executing Fees Test...");
        //$this->backendRepository->genericTests();
        //$this->backendRepository->testCsvWriter();
        $this->backendRepository->testRemote();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}