<?php

namespace services\Fees\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use services\Fees\Repositories\FeesBackendRepository;

class MakeFeesCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'fees:make';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create the CSV file for fees management';

    /**
     * @var FeesBackendRepository
     */
    protected $backendRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->backendRepository = app('services\Fees\Repositories\FeesBackendRepository');
        $this->backendRepository->setConsole($this);
        if(feats()->sapReceipts()){
            $this->comment("Warning: this feature is disabled, exiting now!");
            return;
        }
        $week = $this->option('week');
        $year = $this->option('year');
        $ftp = $this->option('ftp');
        $skipUpload = $this->option('skip-upload');

        if ($ftp == 1) {
            $this->info("Uploading CSV fee file...");
            $filePath = $this->backendRepository->uploadCsvFile($week, $year)->getCsvFilePath();
            $this->info("CSV file $filePath successfully uploaded");
        } else {
            $this->info("Creating CSV fee file...");
            $upload = !($skipUpload == 1);
            $filePath = $this->backendRepository->setUpload($upload)->createCsvFile($week, $year)->getCsvFilePath();
            $this->info("CSV file saved as $filePath");
            if ($upload) {
                $this->info("CSV file also uploaded to the target FTP server");
            } else {
                $this->comment("Attention: the CSV file has not been uploaded to the target FTP server");
            }
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('week', null, InputOption::VALUE_OPTIONAL, 'The week number used for CSV file generation.', null),
            array('year', null, InputOption::VALUE_OPTIONAL, 'The year number used for CSV file generation', null),
            array('ftp', null, InputOption::VALUE_OPTIONAL, 'If this option is passed, than the service will only try to upload the CSV file to the FTP server', null),
            array('skip-upload', null, InputOption::VALUE_OPTIONAL, 'If this option is passed, than the service will not upload the CSV file to the FTP server', null),
        );
    }

}