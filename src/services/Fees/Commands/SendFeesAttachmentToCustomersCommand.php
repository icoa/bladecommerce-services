<?php

namespace services\Fees\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use services\Fees\Repositories\FeesBackendRepository;

class SendFeesAttachmentToCustomersCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'fees:mailer';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send the email with attachment to all customers';

    /**
     * @var FeesBackendRepository
     */
    protected $backendRepository;

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $this->backendRepository = app('services\Fees\Repositories\FeesBackendRepository');
        $this->backendRepository->setConsole($this);
        if(feats()->sapReceipts()){
            $this->comment("Warning: this feature is disabled, exiting now!");
            return;
        }
        $this->info("Sending email attachments to customers...");
        $this->backendRepository->sendEmailAttachmentsToCustomers();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}