<?php

namespace services\Fees\Repositories;

use services\Remote\Repositories\RemoteRepository;

class FeesRemoteRepository extends RemoteRepository
{

    protected function setup(){
        //always disable queue
        $this->queue_mode = false;
        $loaded_tenants = $this->tenants;
        $service_tenants = config('fees.tenants_codes', []);
        foreach ($loaded_tenants as $code => $loaded_tenant){
            if(!in_array($code, $service_tenants)){
                unset($loaded_tenants[$code]);
            }
        }
        $this->tenants = $loaded_tenants;
    }

    protected function cleanUp()
    {
        parent::cleanUp();
        $this->queue_mode = false;
    }
}