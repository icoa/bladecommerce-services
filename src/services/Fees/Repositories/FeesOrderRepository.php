<?php

namespace services\Fees\Repositories;

use services\Morellato\Repositories\Repository;
use Order;
use OrderState;
use PaymentState;
use Payment;
use OrderManager;
use Exception;

class FeesOrderRepository extends Repository
{
    /**
     * @param Order $order
     * @param $status
     */
    public function orderStatusChanged(Order $order, $status)
    {
        //COD (cash on delivery - contrassegno) -> create the receipt only if the OrderState is WORKING
        if($order->hasPayment([Payment::TYPE_CASH])){
            if($status == OrderState::STATUS_WORKING){
                $this->tryCreateReceipt($order);
            }
            return;
        }
        //further check the payment status, even if this event is about order state
        if(in_array($status, [
            OrderState::STATUS_WORKING,
            OrderState::STATUS_CLOSED,
            OrderState::STATUS_DELIVERED,
            OrderState::STATUS_SHIPPED,
            OrderState::STATUS_READY,
        ])){
            $this->orderPaymentStatusChanged($order, $order->payment_status);
        }
    }

    /**
     * @param Order $order
     * @param $status
     */
    public function orderPaymentStatusChanged(Order $order, $status)
    {
        switch ($status) {
            //try to create a 'receipt' where the payment status is 'COMPLETED'
            case PaymentState::STATUS_COMPLETED:
                $this->tryCreateReceipt($order);
                break;

            //try to create a 'starling' where the payment status is 'REFUNDED' or 'PARTIALLY_REFUNDED'
            case PaymentState::STATUS_REFUNDED:
            case PaymentState::STATUS_REFUNDED_PARTIALLY:
                $this->tryCreateStarling($order);
                break;

        }
    }

    /**
     * @param Order $order
     */
    function orderPlaced(Order $order)
    {
        //when an order is placed, if the order has a Gift Card movement, we have to check if the payment status is 'COMPLETED' in order to create a receipt automatically
        if($order->module == Payment::MODULE_GIFTCARD and $order->payment_status == PaymentState::STATUS_COMPLETED){
            $this->tryCreateReceipt($order);
        }
    }

    /**
     * @param Order $order
     */
    protected function tryCreateReceipt(Order $order)
    {
        audit(__METHOD__);
        if ($order->canCreateReceipt()) {
            audit($order->id, 'Proceed to create a receipt for order');
            $order->createReceipt();
        }
    }

    /**
     * @param Order $order
     */
    protected function tryCreateStarling(Order $order)
    {
        if ($order->canCreateStarling()) {

            try {
                //always create the starling first
                audit($order->id, 'Proceed to create a starling for order');
                $order->createStarling();

                //create the details matrix
                $details = [];
                $rma = $order->getLatestRma();

                if ($rma) {
                    audit($rma->id, 'Found the RMA for the details difference generation');

                    $details = $rma->getProductsMatrixDetailsForNewOrder();

                    audit($details, __METHOD__ . '::product matrix details');

                    //in this case, if the details are empty, do not create a third order in automatic
                    if (!empty($details)) {
                        $newOrder = OrderManager::createOrderWithDetails($order, $details);

                        if (is_null($newOrder)) {
                            throw new Exception('Could not create the new order');
                        }
                    }
                }

            } catch (\Exception $e) {
                audit_exception($e, __METHOD__);
            }

        }
    }
}