<?php

namespace services\Fees\Repositories;

use services\Fees\FeesCsvWriter;
use services\Morellato\Repositories\Repository;
use Order;
use OrderManager;
use Exception;
use File;
use OrderHelper;

class FeesBackendRepository extends Repository
{

    /**
     * @var FeesCsvWriter
     */
    protected $writer;

    protected $upload = true;

    function __construct(FeesCsvWriter $writer)
    {
        $this->writer = $writer;
    }

    function genericTests()
    {
        $console = $this->getConsole();
        $console->line(__METHOD__);

        $console->comment("Getting receipts...");
        $rows = Order::onlyReceipts()->get();
        $console->info("Found => " . count($rows));

        $console->comment("Getting invoices...");
        $rows = Order::onlyInvoices()->get();
        $console->info("Found => " . count($rows));

        $console->comment("Getting starlings...");
        $rows = Order::onlyStarlings()->get();
        $console->info("Found => " . count($rows));

        $console->comment("Getting all fees...");
        $rows = Order::withFees()->get();
        $console->info("Found => " . count($rows));
    }

    function testCsvWriter()
    {
        audit_watch();
        $console = $this->getConsole();
        $console->line(__METHOD__);

        $rows = Order::withFees()
            ->orderBy('invoice_date')
            ->orderBy('invoice_number')
            ->get();
        $console->info("Orders found => " . count($rows));

        $this->writer->setOrders($rows)->make(false);
    }

    function testCsvWriterWeek()
    {
        audit_watch();
        $console = $this->getConsole();
        $console->line(__METHOD__);

        $rows = Order::withFees()->withFeesWeek()
            ->orderBy('invoice_date')
            ->orderBy('invoice_number')
            ->get();
        $console->info("Orders found => " . count($rows));

        $this->writer->setOrders($rows)->make(true);
    }

    function testRemote()
    {

    }

    /**
     * @param $upload
     * @return $this
     */
    function setUpload($upload)
    {
        $this->upload = $upload;
        return $this;
    }

    /**
     * @param array $params
     * @return string
     */
    function createCustomCsvFile($params = [])
    {
        $builder = Order::withFees()
            ->orderBy('invoice_date')
            ->orderBy('invoice_number');


        if (array_get($params, 'from')) {
            $from = \Format::sqlDate($params['from']);
            $builder->where('invoice_date', '>=', $from);
        }
        if (array_get($params, 'to')) {
            $to = \Format::sqlDate($params['to']);
            $builder->where('invoice_date', '<=', $to);
        }

        $rows = $builder->get();

        return $this->writer
            ->setOrders($rows)
            ->make(false)
            ->getCustomFilePath();
    }

    /**
     * @param $week
     * @param $year
     * @return FeesCsvWriter
     */
    function createCsvFile($week, $year)
    {
        audit_watch();
        $rows = Order::withFees()
            ->withFeesWeek($week)
            ->withFeesYear($year)
            ->orderBy('invoice_date')
            ->orderBy('invoice_number')
            ->get();

        $writer = $this->writer
            ->setWeek($week)
            ->setYear($year)
            ->setOrders($rows)
            ->make(true)
            ->mergeFromRemote();

        if ($this->upload == true) {
            $writer->upload();
        }

        return $writer;
    }

    /**
     * @param $week
     * @param $year
     * @return null|string
     */
    function getCsvFileContent($week, $year)
    {
        $rows = Order::withFees()
            ->withFeesWeek($week)
            ->withFeesYear($year)
            ->orderBy('invoice_date')
            ->orderBy('invoice_number')
            ->get();

        return $this->writer
            ->setWeek($week)
            ->setYear($year)
            ->setOrders($rows)
            ->getCsvFileContent(true, true);
    }

    /**
     * @param $week
     * @param $year
     * @return FeesCsvWriter
     */
    function uploadCsvFile($week, $year)
    {
        return $this->writer
            ->setWeek($week)
            ->setYear($year)
            ->upload();
    }

    /**
     * Scan all order and send email attachment to customers
     */
    function sendEmailAttachmentsToCustomers()
    {
        /* @var $orders Order[] */
        $orders = Order::withFees()->withoutInvoiceSent()->orderBy('id', 'asc')->get();

        $many = count($orders);

        $this->getConsole()->comment("Found ($many) orders to send...");

        $counter = 0;
        foreach ($orders as $order) {
            $counter++;
            $this->getConsole()->line("Sending order [$order->id] ($counter/$many)");
            OrderManager::sendFeeAttachmentByEmail($order, true, false);
        }
    }


    function checkOrdersForMissingReceipts()
    {
        //search the order table for
        audit_watch();
        $console = $this->getConsole();
        /* @var $orders Order[] */
        $orders = Order::withCorrectDateForFees()->withFeesRequirements()->get();
        $many = count($orders);
        $console->comment("Found [$many] orders to process...");
        $counter = 0;

        $ids = [];

        foreach ($orders as $order) {
            $counter++;
            $console->comment("Checking order [$order->id] ($counter/$many)");

            //check missing receipt
            if ($order->canCreateReceipt()) {
                $console->line("Found a valid order [$order->id] with a missing receipt");
                try {
                    $order->createReceipt();
                    $ids[] = $order->id;
                    $console->info("Receipt created successfully");
                } catch (Exception $e) {
                    $console->error("WARNING: receipt not created!");
                    $console->error($e->getMessage());
                    audit_exception($e);
                }
            }

            //check missing PDF file generation
            if ($order->hasInvoiceNumber()) {
                $invoice = $order->getInvoice();
                if ($invoice and !File::exists($invoice['filepath'])) {
                    $console->line("Found a valid order [$order->id] without the PDF file");
                    try {
                        OrderHelper::createDocumentFile('invoice', $order->id);
                        if (File::exists($invoice['filepath'])) {
                            $console->info("PDF file {$invoice['filepath']} successfully created");
                        }
                    } catch (Exception $e) {
                        $console->error("WARNING: PDF file {$invoice['filepath']} not created!");
                        $console->error($e->getMessage());
                        audit_exception($e);
                    }
                }
            }
        }

        if (!empty($ids)) {
            $console->line("Here the list of orders with new receipts");
            print_r($ids);
        }

    }

}