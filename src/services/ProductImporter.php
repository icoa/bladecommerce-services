<?php namespace services;

use DB;
use Schema;
use SimpleXMLElement;
use Exception;

/**
 * Class ProductImporter
 */
class ProductImporter extends AbstractImporter {

    const C_TABLE_UPDATED = "danea_import_updated_products";

    const C_TABLE_DELETED = "danea_import_deleted_products";

    /**
     * @var
     */
    protected $fieldRules;
    protected $textFields;

    /**
     * Create Node Rules
     */
    protected function createNodeRules()
    {
        // General
        $rules['internal_id'] = ["type" => "bigInteger", "nullable" => false];
        $rules['code'] = ["type" => "string", "length" => 100, "nullable" => false];
        $rules['barcode'] = ["type" => "string", "length" => 50];
        $rules['description'] = ["type" => "longText"];
        $rules['description_html'] = ["type" => "longText"];
        $rules['category'] = ["type" => "string", "length" => 100];
        $rules['subcategory'] = ["type" => "string", "length" => 100];
        for ($i = 2; $i <= 9; $i++) {
            $rules['subcategory'.$i] = ["type" => "string", "length" => 100];
        }
        $rules['vat'] = ["type" => "decimal"];
        $rules['vat_perc'] = ["type" => "decimal"];
        $rules['vat_class'] = ["type" => "string", "length" => 100];
        $rules['vat_description'] = ["type" => "string", "length" => 500];
        $rules['um'] = ["type" => "string", "length" => 10];
        $rules['producer_name'] = ["type" => "string", "length" => 100];
        $rules['link'] = ["type" => "string", "length" => 500];
        for ($i = 1; $i <= 4; $i++) {
            $rules['custom_field'.$i] = ["type" => "longText"];
        }
        $rules['notes'] = ["type" => "longText"];
        $rules['image_file_name'] = ["type" => "string", "length" => 100];

        // Prices
        for ($i = 1; $i <= 9; $i++) {
            $rules['net_price'.$i] = ["type" => "decimal"];
        }
        for ($i = 1; $i <= 9; $i++) {
            $rules['gross_price'.$i] = ["type" => "decimal"];
        }
        $rules['net_eco_fee'] = ["type" => "decimal"];
        $rules['gross_eco_fee'] = ["type" => "decimal"];

        // Supplier
        $rules['supplier_code'] = ["type" => "string", "length" => 100];
        $rules['supplier_name'] = ["type" => "string", "length" => 100];
        $rules['supplier_product_code'] = ["type" => "string", "length" => 100];
        $rules['supplier_net_price'] = ["type" => "decimal"];
        $rules['supplier_gross_price'] = ["type" => "decimal"];
        $rules['supplier_gross_price'] = ["type" => "longText"];

        // Warehouse
        $rules['manage_warehouse'] = ["type" => "boolean"];
        $rules['warehouse_location'] = ["type" => "string", "length" => 100];
        $rules['min_stock'] = ["type" => "integer"];
        $rules['available_qty'] = ["type" => "integer"];
        $rules['ordered_qty'] = ["type" => "integer"];

        // Dimensions
        $rules['size_um'] = ["type" => "string", "length" => 100];
        $rules['net_size_x'] = ["type" => "decimal"];
        $rules['net_size_y'] = ["type" => "decimal"];
        $rules['net_size_z'] = ["type" => "decimal"];
        $rules['packing_size_x'] = ["type" => "decimal"];
        $rules['packing_size_y'] = ["type" => "decimal"];
        $rules['packing_size_z'] = ["type" => "decimal"];
        $rules['weight_um'] = ["type" => "string", "length" => 100];
        $rules['net_weight'] = ["type" => "decimal"];
        $rules['gross_weight'] = ["type" => "decimal"];

        // Custom
        $rules['out_of_production'] = ["type" => "boolean"];
        $rules['availability'] = ["type" => "integer"];


        $this->fieldRules = $rules;
    }

    protected function getConvertableFields(){

        if(is_array($this->textFields) AND count($this->textFields) > 0){
            return  $this->textFields;
        }

        $this->textFields = [];
        if(is_array($this->fieldRules) AND count($this->fieldRules) > 0){
            foreach($this->fieldRules as $key => $rule){
                if($rule['type'] == 'string'){
                    $this->textFields[] = $key;
                }
            }
        }
        return $this->textFields;
    }

    /**
     * Prepare Database Tables
     */
    protected function prepareDB()
    {

        DB::table(self::C_TABLE_DELETED)->truncate();
        DB::table(self::C_TABLE_UPDATED)->truncate();
        return;

        // Check if exists deleted products table
        if ( ! Schema::hasTable(self::C_TABLE_DELETED))
        {
            // Create deleted products table
            Schema::create(self::C_TABLE_DELETED, function($table)
            {
                $table->string('code', 100)->unique();
            });
        }

        // Drop old updated products table
        Schema::dropIfExists(self::C_TABLE_UPDATED);

        // Create new updated products table
        Schema::create(self::C_TABLE_UPDATED, function($table)
        {
            $this->createTableFields($table, $this->fieldRules);
        });

    }

    protected function processNode()
    {
        if ($this->reader->name == 'UpdatedProducts' or $this->reader->name == 'DeletedProducts'  or $this->reader->name == 'Products') {

            $this->parent = $this->reader->name;

        } else if ($this->reader->name == 'Product') {

            while ($this->reader->name == 'Product') {

                $this->processProduct();

                $this->reader->next();

            }

        }
    }


    /**
     * Process product
     */
    private function processProduct()
    {
        // Get the product node
        $product = new SimpleXMLElement($this->reader->readOuterXML());

        $fields = $this->getFieldsFromNode($product);

        $textFields = $this->getConvertableFields();

        if ($this->parent == "UpdatedProducts" OR $this->parent == "Products") {

            foreach($fields as $key => $value){
                if(in_array($key,$textFields)){
                    $fields[$key] = $this->xmlToDb($value);
                }
            }

            DB::table(self::C_TABLE_UPDATED)->insert($fields);
        } else if ($this->parent == "DeletedProducts") {
            try {
                DB::table(self::C_TABLE_DELETED)->insert($fields);
            } catch (Exception $e) {
                // The code already exists but we ignore this
            }
        }
    }

    /**
     * @param $fieldName
     * @return string
     */
    protected function formatField($fieldName)
    {
        if ($fieldName == "custom_field1") {
            $fieldName = "out_of_production";
        } elseif ($fieldName == "custom_field2") {
            $fieldName = "availability";
        }

        return $fieldName;
    }

    /**
     * @param $name
     * @param $value
     * @return int
     */
    protected function checkFieldValue($name, $value)
    {
        if ($name == 'manage_warehouse') {
            $value = ($value == "true");
        }

        return $value;
    }

}