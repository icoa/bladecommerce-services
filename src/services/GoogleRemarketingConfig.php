<?php  namespace services;


use Order;
use Utils;
use Cfg;
use DB;
use Format;


class GoogleRemarketingConfig
{

    public $xml;
    protected $params;
    static $config;

    static function get($key,$default = null){
        $config = self::load();
        if(isset($config) AND isset($config->$key) AND $config->$key != null){
            return $config->$key;
        }
        return $default;
    }

    static function getByLang($lang,$key,$default = null){
        $config = self::load();
        if(isset($config) AND isset($config->{$key.'_'.$lang}) AND $config->{$key.'_'.$lang} != null){
            return $config->{$key.'_'.$lang};
        }
        return $default;
    }

    static function set($key,$value){
        $config = self::load();
        $config->$key = $value;
        \Utils::log($config,__METHOD__);
        self::save();
    }

    static function setMultiple($data){
        $config = self::load();
        foreach($data as $key => $value){
            $config->$key = $value;
        }
        \Utils::log($config,__METHOD__);
        self::save();
    }

    static function save(){
        $config = self::load();
        Cfg::save('GOOGLE_REMARKETING_CONFIG',serialize($config));
        self::load(true);
    }

    static function load($forceReload = false){
        if(!$forceReload AND self::$config){
            return self::$config;
        }
        $str = Cfg::get('GOOGLE_REMARKETING_CONFIG',null);
        if($str AND $str != ''){
            self::$config = unserialize($str);
        }else{
            self::$config =  new \stdClass();
        }
        return self::$config;
    }

}