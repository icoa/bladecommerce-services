<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 19/02/2019
 * Time: 12:15
 */

namespace services;

use Exception;
use Order;
use OrderState;
use OrderDetail;
use Config;
use Mail;
use OrderHelper;
use services\Models\OrderSlipDetail;
use services\Models\OrderTracking;
use services\Traits\TraitLocalDebug;

class OrderCollectorConsumer
{
    use TraitLocalDebug;

    protected $localDebug = false;

    protected $orders = [];

    protected $cache = [];

    /**
     * @var OrderCollectorService
     */
    protected $service;

    /**
     * OrderCollectorConsumer constructor.
     * @param OrderCollectorService $service
     */
    function __construct(OrderCollectorService $service)
    {
        $this->service = $service;
    }

    /**
     *
     */
    public function takeActionsAfterOrderFlow()
    {
        if ($this->service->isEmpty()) {
            $this->audit('No items collected in OrderCollectorService');
            return;
        }

        $items = $this->service->all();
        foreach ($items as $item) {
            try {
                $this->handleOrderFlow($item);
            } catch (Exception $e) {
                audit_exception($e, __METHOD__);
            }
        }

        $this->notifyOrderFlowItems();
    }

    /**
     * @param $item
     * @throws Exception
     */
    protected function handleOrderFlow($item)
    {
        $label = $item->label;

        /** @var Order $order */
        $order = $item->payload;
        if (!$order instanceof Order) {
            if (is_numeric($order)) {
                $order = Order::getObj($order);
                if (is_null($order))
                    throw new Exception("Could not find a valid Order with the given id [$item->payload]");
            } else {
                throw new Exception("The payload must be an order_id (integer) or an Order instance; neither is provided");
            }
        }
        // reload order for avoid false checks
        $order->fresh();

        $this->audit("Processing order [$order->id]", __METHOD__);
        $notification = null;

        $orderSlip = $order->getMorellatoSlip();
        if ($orderSlip) {
            $params = ['sales_id' => 'SAP SALES', 'delivery_id' => 'SAP DELIVERY', 'withdrawal_id' => 'SAP NUMRIT'];
            foreach ($params as $field => $name) {
                if (isset($orderSlip->$field) and strlen($orderSlip->$field) > 0) {
                    $notification .= "<u>{$name}</u>: <strong>{$orderSlip->$field}</strong>,";
                }
            }
            $notification = rtrim($notification, ',') . '<br><br>';
        }

        switch ($label) {
            case OrderSlipDetail::REASON_UNPICKED: //Z5/RRR
                //ACTION: set Order status to CANCELLED_SAP, collect the Order for notification
                $this->audit('ACTION: set Order status to CANCELLED_SAP, collect the Order for notification', __METHOD__);
                $order->setStatus(
                    OrderState::STATUS_CANCELED_SAP,
                    true,
                    "Ricevuto 'prodotto non ritirato' (Z5/RRR) in WS2",
                    OrderTracking::byWs2(OrderTracking::REASON_WS_CANCELLED_UNPICKED)
                );
                $notification .= "<strong style=\"color:#c00\">Ricevuto 'prodotto non ritirato' (Z5/RRR) in WS2. Ordine impostato a 'ANNULLATO - NON RITIRATO'; controllare scheda ordine per maggiori dettagli</strong>";
                break;

            case OrderSlipDetail::REASON_NOT_FOUND: //Z6/ANN
                //ACTION: find a new BEST-SHOP, collect the Order for notification
                //$this->audit("ACTION: find a new BEST-SHOP, collect the Order for notification", __METHOD__);
                //$notification .= "Ricevuto 'non trovato' (Z6) in WS2; tentativo di attribuzione nuovo best-shop in automatico:";
                //$this->findNewBestShop($order, $notification);
                $notification .= "Ricevuto 'Annullato da WEB' (Z6/ANN) in WS2; Ordine impostato a 'ANNULLATO'";
                $order->setStatus(
                    OrderState::STATUS_CANCELED,
                    true,
                    "Ricevuto 'Annullato da WEB' (Z6/ANN) in WS2",
                    OrderTracking::byWs2(OrderTracking::REASON_WS_CANCELLED_NOTFOUND)
                );
                break;

            case OrderSlipDetail::REASON_CANCELLED: //Z8
                //ACTION: cancelled manually, collect the Order for notification
                $this->audit('ACTION: cancelled manually, set Order status to PENDING and collect the Order for notification', __METHOD__);
                $notification .= "Ricevuto 'cancellazione manuale' (Z8) in WS2. Ordine impostato a 'IN ATTESA'; controllare scheda ordine per maggiori dettagli";
                $order->setStatus(
                    OrderState::STATUS_PENDING,
                    true,
                    "Ricevuto 'cancellazione manuale' (Z8) in WS2",
                    OrderTracking::byWs2()
                );
                break;

            case OrderSlipDetail::REASON_NOT_HANDLED: //ZC
                //ACTION: cancelled manually, collect the Order for notification
                $this->audit('ACTION: non-handled', __METHOD__);
                $notification .= "Ricevuto 'Non Gestito (silenti 48H)' (ZC o INA) in WS2. tentativo di attribuzione nuovo best-shop(diverso) in automatico:";
                $this->findNewBestShop(
                    $order,
                    $notification,
                    "Ricevuto 'Non Gestito (silenti 48H)' (ZC o INA) in WS2",
                    OrderTracking::byWs2(OrderTracking::REASON_WS_NEW_SILENT)
                );
                break;

            case OrderSlipDetail::REASON_SHOP_SOLD: //7
                //ACTION: auto-close the order
                $this->audit("ACTION: auto-close the order", __METHOD__);
                $notification .= "<strong style=\"color:green\">Ricevuto 'EMESSO LO SCONTRINO, CLIENTE RITIRA IN NEGOZIO' (7) in WS2. Ordine impostato a 'CHIUSO'; controllare scheda ordine per maggiori dettagli</strong>";
                $order->setStatus(
                    OrderState::STATUS_CLOSED,
                    true,
                    "Ricevuto 'EMESSO LO SCONTRINO, CLIENTE RITIRA IN NEGOZIO' (7) in WS2",
                    OrderTracking::byWs2()
                );
                break;

            case OrderSlipDetail::REASON_RMA_STORE: //RES
                //ACTION: auto-close the order
                $this->audit('ACTION: set order to REFUNDED', __METHOD__);
                $notification .= "<strong style=\"color:green\">Ricevuto 'Reso in negozio' (RES) in WS2. Ordine impostato a 'RIMBORSATO'; controllare scheda ordine per maggiori dettagli</strong>";
                $order->setStatus(
                    OrderState::STATUS_REFUNDED,
                    true,
                    "Ricevuto 'Reso in negozio' (RES) in WS2",
                    OrderTracking::byWs2()
                );
                break;

            case OrderSlipDetail::REASON_PACKAGE_READY: //P
                //ACTION: set status 'ready for pickup'
                if (!$order->hasCurrentStatus([
                    OrderState::STATUS_READY_PICKUP,
                    OrderState::STATUS_CLOSED,
                    OrderState::STATUS_CANCELED,
                    OrderState::STATUS_REFUNDED,
                    OrderState::STATUS_WITHDRAWED,
                ])
                ) {
                    $this->audit("ACTION: resulted 'package ready'", __METHOD__);
                    $notification .= "<strong style=\"color:darkolivegreen\">Ricevuto 'pacco pronto ritiro in negozio' (P) in WS2. Ordine impostato a 'PRONTO PER IL RITIRO (IN STORE)' ed inviata mail al Cliente; controllare scheda ordine per maggiori dettagli</strong>";
                    $order->setStatus(
                        OrderState::STATUS_READY_PICKUP,
                        true,
                        "Ricevuto 'pacco pronto ritiro in negozio' (P) in WS2",
                        OrderTracking::byWs2()
                    );
                }
                break;

            case OrderSlipDetail::REASON_SHOP_RETIRED: //1
                //ACTION: set status 'shop_retired'
                if (!$order->hasCurrentStatus([
                    OrderState::STATUS_WITHDRAWED,
                    OrderState::STATUS_CLOSED,
                    OrderState::STATUS_CANCELED,
                    OrderState::STATUS_REFUNDED,
                ])
                ) {
                    $this->audit("ACTION: resulted 'shop retired'", __METHOD__);
                    $notification .= "<strong style=\"color:blueviolet\">Ricevuto 'pacco ritirato a negozio' (1) in WS2. Ordine impostato a 'RITIRATO DAL CLIENTE'; controllare scheda ordine per maggiori dettagli</strong>";
                    $order->setStatus(
                        OrderState::STATUS_WITHDRAWED,
                        true,
                        "Ricevuto 'pacco ritirato a negozio' (1) in WS2",
                        OrderTracking::byWs2()
                    );
                }
                break;

            case OrderSlipDetail::REASON_SHIPPED: //SH
                //ACTION: set order to 'shipped' and send an email to destination shop
                $this->audit("ACTION: resulted 'shipped'", __METHOD__);
                $notification .= "<strong style=\"color:darkolivegreen\">Ricevuto 'spedizione da deufol/negozio vs cliente' (8) in WS2. Ordine impostato a 'SPEDITO'.</strong>";
                $order->setStatus(
                    OrderState::STATUS_SHIPPED,
                    true,
                    "Ricevuto 'spedizione da deufol/negozio vs cliente' (8) in WS2",
                    OrderTracking::byWs2()
                );
                break;

            case OrderSlipDetail::REASON_SHOP_SHIPPED: //SS
                //ACTION: set order to 'shipped' and send an email to destination shop
                $this->audit("ACTION: resulted 'shop-shipped'", __METHOD__);
                $notification .= "<strong style=\"color:darkolivegreen\">Ricevuto 'spedizione negozio vs negozio' (9) in WS2. Ordine impostato a 'SPEDITO' ed email di avviso inviata al negozio destinatario.</strong>";
                $order->setStatus(
                    OrderState::STATUS_SHIPPED,
                    true,
                    "Ricevuto 'spedizione negozio vs negozio' (9) in WS2",
                    OrderTracking::byWs2()
                );
                break;

            case OrderSlipDetail::REASON_SHOP_NOT_FOUND: //X
                //ACTION: find a new BEST-SHOP but without the current shop, collect the Order for notification
                $this->audit('ACTION: find a new UNIQUE-BEST-SHOP, collect the Order for notification', __METHOD__);
                $notification .= "Ricevuto 'non trovato in Deufol' (X) in WS2; tentativo di attribuzione nuovo best-shop(diverso) in automatico:";
                $this->findNewBestShop(
                    $order,
                    $notification,
                    "Ricevuto 'non trovato in Deufol' (X) in WS2",
                    OrderTracking::byWs2(OrderTracking::REASON_WS_NEW_NOTFOUND)
                );
                break;

            case OrderSlipDetail::REASON_CANCELLED_NOT_FOUND: //W - NNN
                //ACTION: find a new BEST-SHOP but without the current shop, collect the Order for notification
                $this->audit('ACTION: find a new UNIQUE-BEST-SHOP, collect the Order for notification', __METHOD__);
                $notification .= "Ricevuto 'non trovato in Deufol, cancellazione Soft' (W) in WS2; nessuna azione in automatico.";
                //$this->findNewBestShop($order, $notification, true);
                break;
        }

        \Registry::set("order-notification-{$order->id}", $notification);
        $this->orders[] = $order;
    }

    /**
     * @param Order $order
     * @param $notification
     * @param null|string $migration_reason
     * @param null|array $tracking_by
     */
    private function findNewBestShop(Order &$order, &$notification, $migration_reason = null, $tracking_by = null)
    {
        if ((int)array_get($this->cache, "{$order->id}_BS_FOUND") === 1) {
            $notification .= '<br><strong style="color:#333">Condizione ignorata perchè per questo ordine è stato già calcolato un best-shop in questo ciclo operativo.</strong>';
            return;
        }

        //if the Order is in state READY, than don't do nothing
        if ($order->hasCurrentStatus([
            OrderState::STATUS_WITHDRAWED,
            OrderState::STATUS_READY,
            OrderState::STATUS_READY_PICKUP,
            OrderState::STATUS_CLOSED,
            OrderState::STATUS_CANCELED,
            OrderState::STATUS_REFUNDED,
            OrderState::STATUS_PENDING,
            OrderState::STATUS_OUTOFSTOCK,
        ])
        ) {
            $notification .= '<br><strong style="color:brown">Azione di attribuzione nuovo best-shop non applicabile per questo ordine. Non si trova in uno stato che ne permette riassegnazione</strong>';
            return;
        }

        $solvable = [];
        $bestShop = OrderHelper::findBestShopByOrderDetails($order, $solvable);
        if ($bestShop === null or strlen($bestShop) <= 0) {
            $notification .= '<br><strong style="color:red">Non risulta possibile trovare un nuovo best-shop per questo ordine. Impostato stato "IN ATTESA".</strong>';
            $order->setStatus(
                OrderState::STATUS_PENDING,
                true,
                $migration_reason . ' - Nuovo best-shop NON TROVATO in automatico',
                OrderTracking::byWs2()
            );
            return;
        }

        //set taken quantities
        OrderHelper::assignBestShopToOrderDetails($order, $bestShop);

        //now that a new shop has been found, update the order and every OrderDetail
        //update the order asap
        $order->availability_shop_id = $bestShop;
        $order->availability_mode = Order::MODE_SHOP;

        try {
            $order->doActionIncrease();
        } catch (\Exception $e) {
            $msg = $e->getMessage();
            $notification .= '<br><strong style="color:red">Attenzione: il sistema non è riuscito a incrementare ordine in automatico. Messaggio di errore: ' . $msg . '</strong>';
            audit_exception($e, __METHOD__);
        }

        //update OrderDetails
        $products = $order->getProducts(false);
        foreach ($products as $product) {
            if ($product->isModeShop()) {
                $product->availability_shop_id = $bestShop;
                $product->status = OrderDetail::STATUS_SHOP_MOVED;
                if (is_array($solvable) and isset($solvable[$product->getSapSku()])) {
                    $product->availability_solvable_shops = implode(',', $solvable[$product->getSapSku()]);
                }
                $product->save();
            }
        }

        $notification .= '<br><strong style="color:#ff6600">Impostato nuovo best-shop "' . $bestShop . '". Ordine reimpostato allo status "NUOVO"</strong>';
        $order->setStatus(OrderState::STATUS_NEW, true, $migration_reason, $tracking_by);

        array_set($this->cache, "{$order->id}_BS_FOUND", 1);
    }

    /**
     * @return void
     */
    private function notifyOrderFlowItems()
    {
        if (config('soap.order_status_emails.send', false) === false)
            return;

        /** @var Order[] $orders */
        $orders = $this->orders;

        $rows = [];

        foreach ($orders as $order) {
            $id = $order->id;
            $reference = $order->reference;
            $carrier = $order->carrierName();
            $payment = $order->payment()->name;
            $status = $order->statusName();
            $payment_status = $order->paymentStatusName();
            $url = $order->getBackendLinkAttribute();
            $notification = \Registry::get("order-notification-{$order->id}");
            $rows[] = (object)compact('id', 'reference', 'carrier', 'payment', 'status', 'payment_status', 'url', 'notification');
        }

        Config::set('mail.pretend', false);

        $recipient_external = config('soap.order_status_emails.recipient_internal');

        Mail::send('emails.morellato.ws2_orders_actions', compact('rows'), function ($mail) use ($recipient_external) {
            $mail->from(\Cfg::get('MAIL_GENERAL_ADDRESS'));
            $mail->subject("Blade report: WebService2 Orders actions");
            $mail->to($recipient_external);
            $mail->bcc('f.politi@icoa.it');
        });
    }
}