<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 19/02/2019
 * Time: 11:06
 */

namespace services;

use Illuminate\Support\Collection;
use Order;
use services\Patterns\Singleton;

class OrderCollectorService extends Singleton
{

    /**
     * @var Collection
     */
    protected $items;

    /**
     * @return self
     */
    public static function getInstance()
    {
        return parent::getInstance();
    }

    protected function __construct()
    {
        $this->items = Collection::make([]);
    }

    /**
     * @param $payload
     * @return $this
     */
    public function push($payload)
    {
        $this->items->push($payload);
        return $this;
    }

    /**
     * @param $label
     * @param $payload
     * @return $this
     */
    public function pushWithLabel($label, $payload)
    {
        if($payload instanceof Order){
            foreach($this->items as $item){
                if($item->label === $label and $item->payload instanceof Order and (int)$item->payload->id === (int)$payload->id){
                    //this item is already inserted, skip it
                    return $this;
                }
            }
        }
        $item = new \stdClass();
        $item->label = $label;
        $item->payload = $payload;
        //avoid double insert

        $this->items->push($item);
        return $this;
    }

    /**
     * @param $key
     * @param $value
     * @return $this
     */
    public function put($key, $value)
    {
        $this->items->put($key, $value);
        return $this;
    }

    /**
     * @return array
     */
    public function all()
    {
        return $this->items->all();
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->items->toArray();
    }

    /**
     * @return bool
     */
    public function isEmpty()
    {
        return $this->items->isEmpty();
    }
}