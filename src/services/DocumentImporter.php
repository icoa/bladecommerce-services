<?php  namespace services;

use DB;
use Schema;
use SimpleXMLElement;

class DocumentImporter extends AbstractImporter {

    const C_TABLE_DOCUMENTS = "danea_import_documents";

    /**
     * @var
     */
    protected $companyFieldRules;

    /**
     * @var
     */
    protected $documentFieldRules;

    /**
     * @return mixed
     */
    protected function createNodeRules()
    {
        // COMPANY FIELDS
        $companyRules['id'] = ["type" => "bigIncrements"];
        $companyRules['name'] = ["type" => "string", "length" => 100];
        $companyRules['address'] = ["type" => "string", "length" => 100];
        $companyRules['postcode'] = ["type" => "string", "length" => 100];
        $companyRules['city'] = ["type" => "string", "length" => 100];
        $companyRules['province'] = ["type" => "char", "length" => 2];
        $companyRules['country'] = ["type" => "string", "length" => 100];
        $companyRules['fiscal_code'] = ["type" => "string", "length" => 100];
        $companyRules['vat_code'] = ["type" => "string", "length" => 100];
        $companyRules['tel'] = ["type" => "string", "length" => 100];
        $companyRules['fax'] = ["type" => "string", "length" => 100];
        $companyRules['email'] = ["type" => "string", "length" => 100];
        $companyRules['home_page'] = ["type" => "string", "length" => 100];
        $this->companyFieldRules = $companyRules;


        // ROW FIELDS
        $rowFields['document_id'] = ["type" => "bigInteger"];
        $rowFields['code'] = ["type" => "string", "length" => 100];
        $rowFields['supplier_code'] = ["type" => "string", "length" => 100];
        $rowFields['description'] = ["type" => "longText"];
        $rowFields['qty'] = ["type" => "integer"];
        $rowFields['um'] = ["type" => "string", "length" => 10];
        $rowFields['size'] = ["type" => "string", "length" => 50];
        $rowFields['color'] = ["type" => "string", "length" => 50];
        $rowFields['lot'] = ["type" => "string", "length" => 50];
        $rowFields['expiry_date'] = ["type" => "date"];
        $rowFields['serial'] = ["type" => "string", "length" => 50];
        $rowFields['price'] = ["type" => "decimal"];
        $rowFields['discounts'] = ["type" => "decimal"];
        $rowFields['eco_fee'] = ["type" => "decimal"];
        $rowFields['vat'] = ["type" => "decimal"];
        $rowFields['vat_perc'] = ["type" => "decimal"];
        $rowFields['vat_class'] = ["type" => "string", "length" => 100];
        $rowFields['vat_description'] = ["type" => "string", "length" => 500];
        $rowFields['total'] = ["type" => "decimal"];
        $rowFields['withholding_tax'] = ["type" => "boolean"];
        $rowFields['stock'] = ["type" => "boolean"];
        $rowFields['notes'] = ["type" => "longText"];
        $rowFields['commission_perc'] = ["type" => "decimal"];


        // PAYMENT FIELDS
        $paymentFields['document_id'] = ["type" => "bigInteger"];
        $paymentFields['advance'] = ["type" => "boolean"];
        $paymentFields['date'] = ["type" => "date"];
        $paymentFields['amount'] = ["type" => "decimal"];
        $paymentFields['paid'] = ["type" => "boolean"];


        // DOCUMENT FIELDS
        $documentFieldRules['id'] = ["type" => "bigIncrements"];
        // General
        $documentFieldRules['document_type'] = ["type" => "char", "length" => 1, "nullable" => false];
        $documentFieldRules['date'] = ["type" => "date"];
        $documentFieldRules['number'] = ["type" => "integer"];
        $documentFieldRules['numbering'] = ["type" => "string", "length" => 10];
        // Customer
        $documentFieldRules['customer_code'] = ["type" => "string", "length" => 100];
        $documentFieldRules['customer_web_login'] = ["type" => "string", "length" => 100];
        $documentFieldRules['customer_name'] = ["type" => "string", "length" => 100];
        $documentFieldRules['customer_address'] = ["type" => "string", "length" => 100];
        $documentFieldRules['customer_postcode'] = ["type" => "string", "length" => 100];
        $documentFieldRules['customer_city'] = ["type" => "string", "length" => 100];
        $documentFieldRules['customer_province'] = ["type" => "char", "length" => 2];
        $documentFieldRules['customer_country'] = ["type" => "string", "length" => 100];
        $documentFieldRules['customer_fiscal_code'] = ["type" => "string", "length" => 100];
        $documentFieldRules['customer_vat_code'] = ["type" => "string", "length" => 100];
        $documentFieldRules['customer_tel'] = ["type" => "string", "length" => 100];
        $documentFieldRules['customer_cell_phone'] = ["type" => "string", "length" => 100];
        $documentFieldRules['customer_fax'] = ["type" => "string", "length" => 100];
        $documentFieldRules['customer_email'] = ["type" => "string", "length" => 100];
        $documentFieldRules['customer_pec'] = ["type" => "string", "length" => 100];
        $documentFieldRules['customer_reference'] = ["type" => "string", "length" => 100];
        // Delivery
        $documentFieldRules['delivery_name'] = ["type" => "string", "length" => 100];
        $documentFieldRules['delivery_address'] = ["type" => "string", "length" => 100];
        $documentFieldRules['delivery_postcode'] = ["type" => "string", "length" => 100];
        $documentFieldRules['delivery_city'] = ["type" => "char", "length" => 2];
        $documentFieldRules['delivery_province'] = ["type" => "string", "length" => 100];
        $documentFieldRules['delivery_country'] = ["type" => "string", "length" => 100];
        // Transport
        $documentFieldRules['carrier'] = ["type" => "string", "length" => 100];
        $documentFieldRules['transport_reason'] = ["type" => "string", "length" => 100];
        $documentFieldRules['goods_appearance'] = ["type" => "string", "length" => 100];
        $documentFieldRules['num_of_pieces'] = ["type" => "integer"];
        $documentFieldRules['transport_date_time'] = ["type" => "datetime"];
        $documentFieldRules['shipment_terms'] = ["type" => "string", "length" => 100];
        $documentFieldRules['transported_weight'] = ["type" => "integer"];
        $documentFieldRules['tracking_number'] = ["type" => "string", "length" => 100];
        // Note and Comments
        $documentFieldRules['internal_comment'] = ["type" => "string", "length" => 500];
        for ($i = 1; $i <= 4; $i++) {
            $documentFieldRules['custom_field'.$i] = ["type" => "longText"];
        }
        $documentFieldRules['foot_notes'] = ["type" => "string", "length" => 500];
        // Other fields
        $documentFieldRules['cost_description'] = ["type" => "string", "length" => 500];
        $documentFieldRules['cost_vat_code'] = ["type" => "string", "length" => 100];
        $documentFieldRules['cost_vat_code_perc'] = ["type" => "tinyInteger"];
        $documentFieldRules['cost_vat_code_class'] = ["type" => "string", "length" => 100];
        $documentFieldRules['cost_vat_code_description'] = ["type" => "string", "length" => 500];
        $documentFieldRules['cost_amount'] = ["type" => "decimal"];
        $documentFieldRules['total_without_tax'] = ["type" => "decimal"];
        $documentFieldRules['vat_amount'] = ["type" => "decimal"];
        $documentFieldRules['total'] = ["type" => "decimal"];
        $documentFieldRules['payment_advance_amount'] = ["type" => "decimal"];
        $documentFieldRules['prices_include_vat'] = ["type" => "boolean"];
        $documentFieldRules['withholding_tax_perc'] = ["type" => "decimal"];
        $documentFieldRules['withholding_tax_perc2'] = ["type" => "decimal"];
        $documentFieldRules['withholding_tax_amount'] = ["type" => "decimal"];
        $documentFieldRules['withholding_tax_amount_b'] = ["type" => "decimal"];
        $documentFieldRules['withholding_tax_name_b'] = ["type" => "decimal"];
        $documentFieldRules['contrib_description'] = ["type" => "string", "length" => 500];
        $documentFieldRules['contrib_perc'] = ["type" => "decimal"];
        $documentFieldRules['contrib_subject_to_withholding_tax'] = ["type" => "boolean"];
        $documentFieldRules['contrib_amount'] = ["type" => "decimal"];
        $documentFieldRules['contrib_vat_code'] = ["type" => "string", "length" => 100];
        $documentFieldRules['delayed_vat'] = ["type" => "boolean"];
        $documentFieldRules['delayed_vat_desc'] = ["type" => "string", "length" => 500];
        $documentFieldRules['delayed_vat_due_within_one_year'] = ["type" => "boolean"];
        $documentFieldRules['warehouse'] = ["type" => "string", "length" => 100];
        $documentFieldRules['price_list'] = ["type" => "string", "length" => 100];
        $documentFieldRules['payment_name'] = ["type" => "string", "length" => 100];
        $documentFieldRules['payment_bank'] = ["type" => "string", "length" => 100];
        $documentFieldRules['sales_agent'] = ["type" => "string", "length" => 100];
        $documentFieldRules['expected_conclusion_date'] = ["type" => "date"];
        $documentFieldRules['doc_reference'] = ["type" => "string", "length" => 100];
        // Rows
        $documentFieldRules['rows'] = ["type" => "subnode", $rowFields];
        // Payments
        $documentFieldRules['payments'] = ["type" => "subnode", $paymentFields];

        $this->documentFieldRules = $documentFieldRules;
    }

    /**
     * @return mixed
     */
    protected function prepareDB()
    {
        // Drop documents table
        Schema::dropIfExists(self::C_TABLE_DOCUMENTS);

        // Create documents table
        Schema::create(self::C_TABLE_DOCUMENTS, function($table)
        {
            $this->createTableFields($table, $this->documentFieldRules);
        });
    }

    /**
     * @return mixed
     */
    protected function processNode()
    {
        if ($this->reader->name == 'Document') {

            while ($this->reader->name == 'Document') {

                $this->processDocument();

                $this->reader->next();

            }

        }
    }

    private function processDocument()
    {
        // Get the product node
        $document = new SimpleXMLElement($this->reader->readOuterXML());

        $fields = $this->getFieldsFromNode($document);

        DB::table(self::C_TABLE_DOCUMENTS)->insert($fields);
    }
}