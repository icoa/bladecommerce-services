<?php

namespace services\Jobs;

use services\Remote\Repositories\RemoteRepository;

class RemoteApiRequestJob extends BladeJob
{
    public function fire($job, $data)
    {
        audit(__METHOD__);
        audit($data, __METHOD__ . '::data');
        /**
         * var RemoteRepository
         */
        $repository = app(RemoteRepository::class);
        $repository->resumeRequest($data);
        $job->delete();
    }
}