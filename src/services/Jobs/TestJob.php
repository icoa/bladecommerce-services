<?php

namespace services\Jobs;

class TestJob extends BladeJob
{
    public function fire($job, $data)
    {
        audit(__METHOD__);
        audit($data, __METHOD__ . '::data');
        $job->delete();
    }
}