<?php

namespace services\Emergency\Commands;

use Illuminate\Console\Command;
use services\Emergency\Repositories\EmergencyRepository;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Sentry;
use Illuminate\Encryption\Encrypter;

class EmergencyHandleCatalog extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'emergency:catalog';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Performs some task to handle catalog in the state of emergency';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        /** @var EmergencyRepository $repository */
        $repository = app(EmergencyRepository::class);
        $repository->setConsole($this);
        $repository->catalog();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array();
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array();
    }

}