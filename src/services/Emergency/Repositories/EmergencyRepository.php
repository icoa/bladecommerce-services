<?php


namespace services\Emergency\Repositories;

use services\Morellato\Repositories\Repository;
use DB;
use ProductSpecificStock;
use Product;

class EmergencyRepository extends Repository
{
    public function catalog()
    {
        $console = $this->getConsole();

        DB::beginTransaction();
        // Reset all imported stocks for warehouse NG and W3
        $console->comment('Reset all imported stocks for warehouse NG and W3');
        ProductSpecificStock::whereIn('warehouse', ['NG', 'W3'])->update([
            'quantity_shop' => 0,
            'available_at' => '2020-04-03',
        ]);

        // Set to 'on' featured flags for products that are available
        $console->comment('Set to \'on\' featured flags for products that are available');
        Product::query()->update(['is_featured' => 0]);
        Product::where('is_out_of_production', 0)->whereIn('id', function ($query) {
            $query->select('product_id')->from('products_specific_stocks')
                ->where(function ($suqbquery) {
                    $suqbquery->where('quantity_24', '!=', 0)->orWhere('quantity_48', '!=', 0);
                })->whereIn('warehouse', ['01', '03', 'WM']);
        })->update(['is_featured' => 1]);

        // Update products cache
        $console->comment('Update products cache');
        DB::table('cache_products')->update(['is_featured' => 0]);
        DB::table('cache_products')->whereIn('id', function ($query) {
            $query->select('id')->from('products')->where('is_featured', 1);
        })->update(['is_featured' => 1]);

        // Purge catalog cache
        $console->comment('Purge catalog cache');
        $console->call('cache:purge', ['target' => 'catalog']);

        DB::commit();
    }
}