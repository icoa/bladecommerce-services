<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 07/12/2018
 * Time: 17:14
 */

namespace services\IpResolver\Adapters;

use services\IpResolver\IpPayload;

interface IpResolverInterface
{
    /**
     * @param $ip
     * @return IpPayload
     */
    public function fetchPayload($ip);
}