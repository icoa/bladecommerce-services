<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 07/12/2018
 * Time: 16:36
 */

namespace services\IpResolver\Adapters;

use services\IpResolver\IpPayload;

class IpResolverAdapter implements IpResolverInterface
{
    /**
     * @param $ip
     * @return IpPayload
     */
    public function fetchPayload($ip)
    {
        $payload = new IpPayload();
        return $payload;
    }

    /**
     * @param $ip
     * @return integer
     */
    public function ip2long($ip)
    {
        return ip2long($ip);
    }

    /**
     * Get the IP version and number of the given IP address
     *
     * This method will return an array, whose components will be:
     * - first: 4 if the given IP address is an IPv4 one, 6 if it's an IPv6 one,
     *          or fase if it's neither.
     * - second: the IP address' number if its version is 4, the number string if
     *           its version is 6, false otherwise.
     *
     * @access private
     * @static
     * @param string $ip IP address to extract the version and number for
     * @return array
     */
    public function ipVersionAndNumber($ip)
    {
        if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            return [4, sprintf('%u', ip2long($ip))];
        } elseif (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
            return [6, $ip];
        } else {
            // Invalid IP address, return falses
            return [false, false];
        }
    }

    /**
     * @param $ip
     * @return bool
     */
    public function isIpv4($ip)
    {
        return filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4);
    }
}