<?php

namespace services\IpResolver\Adapters;

use services\IpResolver\IpPayload;
use IP2Location\Database;

class Ip2LocationAdapter extends IpResolverAdapter
{

    protected $dbPath;
    //protected $dbPathIp6;

    function __construct(array $config)
    {
        $this->dbPath = array_get($config, 'dbPath', storage_path('tools/IP2LOCATION-DB9.BIN'));
        //$this->dbPathIp6 = array_get($config, 'dbPathIp6', storage_path('tools/IP2LOCATION-DB9-IPV6.BIN'));
    }

    /**
     * @param $ip
     * @return IpPayload
     */
    public function fetchPayload($ip)
    {
        /*
         * [ipNumber] => 1327158095
            [ipVersion] => 4
            [ipAddress] => 79.26.211.79
            [mcc] => This parameter is unavailable in selected .BIN data file. Please upgrade.
            [mnc] => This parameter is unavailable in selected .BIN data file. Please upgrade.
            [mobileCarrierName] => This parameter is unavailable in selected .BIN data file. Please upgrade.
            [weatherStationName] => This parameter is unavailable in selected .BIN data file. Please upgrade.
            [weatherStationCode] => This parameter is unavailable in selected .BIN data file. Please upgrade.
            [iddCode] => This parameter is unavailable in selected .BIN data file. Please upgrade.
            [areaCode] => This parameter is unavailable in selected .BIN data file. Please upgrade.
            [latitude] => 41.89474105835
            [longitude] => 12.48390007019
            [countryName] => Italy
            [countryCode] => IT
            [usageType] => This parameter is unavailable in selected .BIN data file. Please upgrade.
            [elevation] => This parameter is unavailable in selected .BIN data file. Please upgrade.
            [netSpeed] => This parameter is unavailable in selected .BIN data file. Please upgrade.
            [timeZone] => This parameter is unavailable in selected .BIN data file. Please upgrade.
            [zipCode] => This parameter is unavailable in selected .BIN data file. Please upgrade.
            [domainName] => This parameter is unavailable in selected .BIN data file. Please upgrade.
            [isp] => This parameter is unavailable in selected .BIN data file. Please upgrade.
            [cityName] => Rome
            [regionName] => Lazio
         * */
        $payload = new IpPayload();

        try {
            //$db = new Database($this->isIpv4($ip) ? $this->dbPath : $this->dbPathIp6, Database::FILE_IO);
            $db = new Database($this->dbPath, Database::FILE_IO);

            $records = $db->lookup($ip, Database::ALL);

            if (is_array($records) and !empty($records)) {
                foreach ($records as $key => $record) {
                    if(in_array($record, [
                        Database::FIELD_NOT_KNOWN,
                        Database::FIELD_NOT_SUPPORTED,
                        Database::INVALID_IP_ADDRESS,
                        'This parameter is unavailable in selected .BIN data file. Please upgrade.'
                    ], true)){
                        unset($records[$key]);
                    }
                }
                $payload->fill($records);
            }

        } catch (\Exception $e) {
            audit_error("Could not resolve IP [$ip]", __METHOD__);
            audit_error($e->getMessage(), __METHOD__);
        }

        return $payload;
    }
}