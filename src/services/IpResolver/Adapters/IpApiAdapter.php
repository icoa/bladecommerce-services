<?php

namespace services\IpResolver\Adapters;

use services\IpResolver\IpPayload;
use GuzzleHttp\Client;

class IpApiAdapter extends IpResolverAdapter
{
    protected $domain = 'https://api.ipapi.com';
    /**
     * @var string
     */
    protected $access_key;

    /**
     * @var bool
     */
    protected $security;

    protected $client;

    /**
     * IpApiAdapter constructor.
     * @param array $config
     */
    function __construct(array $config)
    {
        $this->client = new Client();
        $this->access_key = (string)array_get($config, 'access_key');
        $this->security = (bool)array_get($config, 'security', false);
    }

    /**
     * @param $ip
     * @return string
     */
    protected function getUri($ip)
    {
        $access_key = $this->access_key;
        $uri = "{$this->domain}/{$ip}?access_key={$access_key}";
        if ($this->security) {
            $uri .= '&security=1';
        }
        return $uri;
    }

    /**
     * @param $ip
     * @return IpPayload
     */
    public function fetchPayload($ip)
    {
        /*
         * ip: "79.27.84.167",
type: "ipv4",
continent_code: "EU",
continent_name: "Europe",
country_code: "IT",
country_name: "Italy",
region_code: "62",
region_name: "Latium",
city: "Fiumicino",
zip: "00054",
latitude: 41.7709,
longitude: 12.2366,
location: {
geoname_id: 3176923,
capital: "Rome",
languages: [
{
code: "it",
name: "Italian",
native: "Italiano"
}
],
country_flag: "http://assets.ipapi.com/flags/it.svg",
country_flag_emoji: "🇮🇹",
country_flag_emoji_unicode: "U+1F1EE U+1F1F9",
calling_code: "39",
is_eu: true
},
time_zone: {
id: "Europe/Rome",
current_time: "2018-12-07T16:48:17+01:00",
gmt_offset: 3600,
code: "CET",
is_daylight_saving: false
},
currency: {
code: "EUR",
name: "Euro",
plural: "euros",
symbol: "€",
symbol_native: "€"
},
connection: {
asn: 3269,
isp: "Telecom Italia"
}
         * */
        $payload = new IpPayload();

        try {
            $url = $this->getUri($ip);
            //audit($url, __METHOD__, 'URL');
            $response = $this->client->get($url);
            $status = $response->getStatusCode();
            //audit($status, __METHOD__, 'STATUS');
            if ($status != 200)
                throw new \Exception('IpApi service has not responded with a valid 200 OK Status code');

            // Decode JSON response:
            $api_result = json_decode($response->getBody(), true);
            //audit($api_result, __METHOD__, 'JSON RESPONSE');

            $params = [
                'ipNumber' => $this->ip2long($ip),
                'ipVersion' => substr(array_get($api_result, 'type'), -1, 1),
                'ipAddress' => array_get($api_result, 'ip'),
                'latitude' => array_get($api_result, 'latitude'),
                'longitude' => array_get($api_result, 'longitude'),
                'countryName' => array_get($api_result, 'country_name'),
                'countryCode' => array_get($api_result, 'country_code'),
                'cityName' => array_get($api_result, 'city'),
                'regionName' => array_get($api_result, 'region_name'),
                'isp' => array_get($api_result, 'connection.isp'),
                'currencyCode' => array_get($api_result, 'currency.code'),
            ];

            //audit($params, 'PAYLOAD PARAMS');

            $payload->fill($params);

            return $payload;

        } catch (\Exception $e) {
            audit_exception($e, __METHOD__);
        }

        return $payload;
    }
}