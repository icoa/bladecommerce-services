<?php


namespace services\IpResolver\Ip2Location;

use Closure;
use DB, Utils, Config, Exception;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use ZipArchive;

class DownloadService
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Filesystem
     */
    protected $files;

    /**
     * @var string
     */
    protected $endpoint = 'https://www.ip2location.com/download?token=[token]&file=[database_code]';

    /**
     * @var string|null
     */
    protected $error;

    /**
     * @var Command $command
     */
    protected $command;

    /**
     * CommanderActApiService constructor.
     * @param Filesystem $files
     */
    function __construct(Filesystem $files)
    {
        $this->files = $files;
        $this->client = new Client([
            'verify' => false,
        ]);
    }

    /**
     * @return string|null
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param string|null $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }

    /**
     * @return Command
     */
    public function getCommand()
    {
        return $this->command;
    }

    /**
     * @param Command $command
     * @return DownloadService
     */
    public function setCommand($command)
    {
        $this->command = $command;
        return $this;
    }

    /**
     * @param Closure $call
     */
    protected function terminal(Closure $call)
    {
        if ($this->command) {
            $call();
        }
    }

    /**
     * @return string
     */
    protected function getTempFile()
    {
        return $this->getFolder() . 'download.zip';
    }

    /**
     * @return string
     */
    protected function getDatabaseFile()
    {
        return $this->getFolder() . 'IP2LOCATION-DB9.BIN';
    }

    /**
     * @return string
     */
    protected function getBinaryFile()
    {
        return 'IPV6-COUNTRY-REGION-CITY-LATITUDE-LONGITUDE-ZIPCODE.BIN';
    }

    /**
     * @return string
     */
    protected function getFolder()
    {
        return storage_path('tools/');
    }

    /**
     * @return string
     * @throws Exception
     */
    protected function makeUri()
    {
        $params = [
            'token' => config('template.ipResolver.ip2location.token', null),
            'database_code' => config('template.ipResolver.ip2location.database_code', null),
        ];

        $tokens = [];

        foreach ($params as $key => $value) {
            if ($value === null or strlen($value) == 0) {
                throw new Exception("Parameter [$key] is mandatory to create the endpoint in Ip2Location download service");
            }
            $tokens[] = "[$key]";
        }

        return str_replace($tokens, array_values($params), $this->endpoint);
    }

    /**
     * @return $this
     */
    function backup()
    {
        $original_file = $this->getDatabaseFile();
        $backup_file = $this->getFolder() . 'IP2LOCATION-DB9.' . date('Ymd') . '.BAK';
        if (file_exists($original_file)) {
            $this->terminal(function () use ($original_file, $backup_file) {
                $this->command->info("Copying file $original_file to $backup_file");
            });
            copy($original_file, $backup_file);
        }
        return $this;
    }

    /**
     * @return DownloadService
     */
    function download()
    {
        try {
            $uri = $this->makeUri();
            audit($uri, '$uri', __METHOD__);

            $this->terminal(function () use ($uri) {
                $this->command->line("Downloading file from $uri");
            });

            $head = $this->client->head($uri);
            $statusCode = (int)$head->getStatusCode();

            $this->terminal(function () use ($statusCode) {
                $this->command->line("Response status code is $statusCode");
            });

            if ($statusCode === 200) {
                $this->terminal(function () {
                    $this->command->info('Saving temporary file to ' . $this->getTempFile());
                });

                $res = $this->client->get($uri);
                $resourceBody = $res->getBody();

                file_put_contents($this->getTempFile(), $resourceBody);

            } else {
                throw new Exception("Ip2Location downloader has responded with a status code of [$statusCode]");
            }
        } catch (Exception $e) {
            $this->setError($e->getMessage());

            $this->terminal(function () use ($e) {
                $this->command->error($e->getMessage());
            });
        }

        return $this;
    }

    function finish()
    {
        $file = $this->getTempFile();
        $db_file = $this->getDatabaseFile();
        try {
            //extract temp file
            $zip = new ZipArchive;
            $res = $zip->open($file);
            if ($res === TRUE) {
                $zip->extractTo($this->getFolder(), [$this->getBinaryFile()]);
                $zip->close();

                //move file
                rename($this->getFolder() . $this->getBinaryFile(), $db_file);

                //remove temp file
                unlink($file);

                $this->terminal(function () use ($file, $db_file) {
                    $this->command->info("Temp file $file has been successfully extracted to $db_file");
                });
            }
        } catch (Exception $e) {
            $this->setError($e->getMessage());

            $this->terminal(function () use ($e) {
                $this->command->error($e->getMessage());
            });
        }

        return $this;

    }

    function sendReport()
    {
        $recipient_internal = 'f.politi@icoa.it';
        $customer = $this->getCustomer();
        $error = $this->getError();
        $customer->api_message = $error ? $error : 'Successfully removed';

        $rows = [$customer];

        Mail::send('emails.vendors.commanderAct_customer_delete', compact('rows'), function ($mail) use ($recipient_internal) {
            $mail->from(\Cfg::get('MAIL_GENERAL_ADDRESS'));
            $mail->subject("Blade report: cancellazione Utente effettuata su CommanderAct");
            $mail->to($recipient_internal);
            $mail->bcc('f.politi@icoa.it');
        });
    }
}