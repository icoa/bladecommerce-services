<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 07/12/2018
 * Time: 16:41
 */

namespace services\IpResolver;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class IpPayload extends Model
{
    /*public $ipNumber;
    public $ipVersion;
    public $ipAddress;
    public $latitude;
    public $longitude;
    public $countryName;
    public $countryCode;
    public $cityName;
    public $regionName;
    public $isp;
    public $currencyCode;*/

    protected $fillable = [
        'ipNumber',
        'ipVersion',
        'ipAddress',
        'latitude',
        'longitude',
        'countryName',
        'countryCode',
        'cityName',
        'regionName',
        'isp',
        'currencyCode',
    ];

    /**
     * @return bool
     */
    function isResolved()
    {
        return $this->getAttribute('latitude') != '' and $this->getAttribute('longitude') != '';
    }

    /**
     * @param $key
     * @return bool
     */
    function hasAttribute($key)
    {
        $attribute = $this->getAttribute($key);
        return ($attribute != null and Str::length($attribute) > 0);
    }
}