<?php


namespace services\IpResolver\Commands;

use Illuminate\Console\Command;
use services\IpResolver\Ip2Location\DownloadService;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Config;

class Ip2LocationDownloaderCommand extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'ip2location:download';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Download new binary database from Ip2Location service';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        /** @var DownloadService $service */
        $service = app(DownloadService::class);
        $service
            ->setCommand($this)
            ->backup()
            ->download()
            ->finish();
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */

    protected function getArguments()
    {
        return array(

        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(

        );
    }

}