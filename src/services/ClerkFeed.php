<?php namespace services;


use Product;
use SimpleXMLElement;
use Cfg;
use DB;
use Format;
use Currency;
use Carbon\Carbon;

class ClerkFeed
{

    public $xml;
    protected $channel;
    protected $config;
    protected $lang;
    protected $locale;
    protected $currency;
    protected $country;
    protected $qs;
    protected $console;
    protected $debug = true;
    protected $save = true;
    protected $cache = [];
    protected $customLabels = [];
    protected $export_categories = false;
    protected $export_sales = false;

    function __construct($lang, $locale)
    {
        $this->lang = $lang;
        $this->locale = $locale;
        $this->country = \Country::where('iso_code', $locale)->first();
        $this->currency = ($this->country) ? \Currency::getObj($this->country->currency_id) : \Core::getDefaultCurrency();
        \Core::setLang($lang);
        \App::setLocale($lang);
        $this->debug = \Config::get('app.debug');
        if (\Input::get('nosave') == 1)
            $this->save = false;
    }

    public function setSave($mode)
    {
        $this->save = false;
    }

    public function setDebug($mode)
    {
        $this->debug = $mode;
    }

    public function setConsole($console)
    {
        $this->console = $console;
    }

    public function export()
    {
        //audit_watch();
        $lang = $this->lang;
        $locale = $this->locale;
        $lastOrderKey = 'CLERK_LAST_ORDER_' . $lang . '_' . $locale;


        $queryBuilder = Product::rows($lang)->where('published', 1);

        $queryBuilder->where('visibility', '<>', 'none')->where('is_out_of_production', 0);

        if ($this->debug OR \Input::get('take', 0) > 0) $queryBuilder->take(\Input::get('take', 10));

        //$queryBuilder->where('sku','R8823125001');

        $rows = $queryBuilder->orderBy('id', 'desc')->lists('id');

        $many = count($rows);
        if ($this->console) {
            $this->console->line("Found ($many) products");
        }

        $json = new \stdClass();
        $json->products = [];

        if($this->export_categories)
            $json->categories = [];

        if($this->export_sales)
            $json->sales = [];

        $counter = 0;
        foreach ($rows as $row) {
            $counter++;
            if ($this->console) {
                $this->console->line("Creating node for product [$row] ($counter/$many)");
            }
            $node = $this->createProductNode($row);
            if ($node) {
                $json->products[] = $node;
            }
            $node = null;
            unset($node);
        }

        if($this->export_categories):

        $categories = \Category::with('subcategories')->rows($lang)->where('published', 1)->orderBy('id', 'asc')->get();
        $many = count($categories);
        if ($this->console) {
            $this->console->line("Found ($many) categories");
        }
        $counter = 0;
        foreach ($categories as $category) {
            $counter++;
            if ($this->console) {
                $this->console->line("Creating node for category [$category->name] ($counter/$many)");
            }
            $node = $this->createCategoryNode($category);
            if ($node) {
                $json->categories[] = $node;
            }
            $node = null;
            unset($node);
        }

        endif;

        if($this->export_sales):

        $paid_status = \OrderState::where('paid', 1)->lists('id');
        $paid_payment_status = \PaymentState::where('paid', 1)->lists('id');
        $last = \Cfg::real($lastOrderKey, date(strtotime(date('Y-m-d H:i:s') . ' -4 months')));
        $queryBuilder = \Order::with(['customer', 'details'])
            ->where('lang_id', $lang)
            ->whereIn('status', $paid_status)
            ->whereIn('payment_status', $paid_payment_status)
            ->where('created_at', '>=', $last);


        if ($this->debug) $queryBuilder->take(10);

        $rows = $queryBuilder->orderBy('id', 'desc')->get();
        $many = count($rows);
        if ($this->console) {
            $this->console->line("Found ($many) orders");
        }
        $counter = 0;
        foreach ($rows as $row) {
            $counter++;
            if ($this->console) {
                $this->console->line("Creating node for order [$row->reference] ($counter/$many)");
            }
            $node = $this->createOrderNode($row);
            if ($node) {
                $json->sales[] = $node;
            }
            $node = null;
            unset($node);
        }

        if (count($json->sales) > 0 AND $this->save)
            \Cfg::save($lastOrderKey, date('Y-m-d H:i:s'));

        endif;

        return $json;

    }


    private function createProductNode($id)
    {
        $node = null;
        try {
            $product = Product::getProductFeed($id, $this->lang);
            $node = new \stdClass();
            $node->id = $product->id;
            $node->sku = trim($product->sku);
            $node->name = trim($product->name);
            $node->outpro = $product->is_out_of_production ? true : false;
            $node->new = $product->is_new ? true : false;
            $node->outlet = $product->is_outlet ? true : false;
            $node->brand_id = $product->brand_id;
            $node->collection_id = $product->collection_id;
            $node->availability = $product->availabilityStatus;
            $node->in_stock = $product->availabilityStatus == 'in_stock';
            if (isset($product->gender)) {
                $node->gender = $product->gender;
            }
            if (\Str::length($product->metakeywords) > 0) {
                $node->keywords = $product->metakeywords;
            }
            if (\Str::length($product->metadescription) > 0) {
                $node->metadescription = $product->metadescription;
            }

            $description = '';
            $description .= $product->getShortDescription() . " ";
            $description .= $product->attributes . " ";
            $description = trim($description);
            $description = \Utils::stripTags($description);
            $node->description = $description;

            $link = $product->link_absolute;
            if ($this->qs) {
                $link .= "?" . $this->qs;
            }
            $node->url = $link;

            $defaultImg = $product->smallImg;
            if ($defaultImg != '' AND $defaultImg != '/media/no.gif') {
                $node->image = \Site::rootify($defaultImg);
                $node->image_retina = \Site::rootify($product->smallImgRetina);
            }

            if (isset($product->brand_name)) {
                $node->brand = $product->brand_name;
                $node->brand_image = \FrontTpl::img($product->brand_image_thumb, true, '/images/5a/70/30');
                $node->brand_image_retina = str_replace(['.jpg', '.png'], ['@2x.jpg', '@2x.png'], $node->brand_image);
            }
            if (isset($product->collection_name)) {
                $node->collection = $product->collection_name;
            }

            $price = Format::currency($product->price_official_raw, false, $this->currency->id, true);
            $node->list_price = $price;

            $price = Format::currency($product->price_final_raw, false, $this->currency->id, true);
            $node->price = $price;

            $node->is_on_sale = $product->price_final_raw < $product->price_official_raw;

            $node->categories = [];
            if(isset($product->related_entities) and isset($product->related_entities['categories'])){
                $categories = $product->related_entities['categories'];
            }else{
                $categories = $product->getCategoriesIds();
            }
            if (is_array($categories)) {
                foreach ($categories as $category) {
                    $node->categories[] = $category;
                }
            }
            if ($product->default_category_id > 0 and !in_array($product->default_category_id, $node->categories)) {
                $node->categories[] = $product->default_category_id;
            }
            if ($product->main_category_id > 0 and !in_array($product->main_category_id, $node->categories)) {
                $node->categories[] = $product->main_category_id;
            }

            try{
                if(isset($product->related_entities) and isset($product->related_entities['attributes_labels'])){
                    $attributes_labels = $product->related_entities['attributes_labels'];
                    $option_values = $product->related_entities['option_values'];
                    foreach($attributes_labels as $index => $attributes_label){
                        $label = preg_replace('/[0-9]+/', '', $attributes_label);
                        $label = \Str::slug(trim($label), '_');
                        $node->$label = trim($option_values[$index]);
                    }
                }else{
                    $attributes = $product->getFrontAttributes();
                    foreach ($attributes as $name => $attribute) {
                        $attribute->label = preg_replace('/[0-9]+/', '', $attribute->label);
                        $label = \Str::slug(trim($attribute->label), '_');
                        if (\Str::length($label) < 32 AND $attribute->label != '' AND trim($attribute->value) != '')
                            $node->$label = trim($attribute->value);
                    }
                }
            }catch (\Exception $e){

            }


        } catch (\Exception $e) {

        }

        unset($product);

        return $node;
    }


    private function createCategoryNode($product)
    {
        $node = null;
        try {
            $node = new \stdClass();
            $node->id = $product->id;
            $node->name = trim($product->name);
            $node->url = $product->link;
            $node->subcategories = $product->subcategories()->lists('id');
        } catch (\Exception $e) {

        }

        return $node;
    }

    private function createOrderNode($product)
    {
        $version = config('plugins_settings.clerk.feed.orderType', 2);
        switch ($version) {
            default:
            case 1:
                return $this->createOrderNodeV1($product);
                break;
            case 2:
                return $this->createOrderNodeV2($product);
                break;
        }
    }

    private function createOrderNodeV1($product)
    {
        $node = null;
        try {
            $node = new \stdClass();
            $node->id = $product->id;
            $node->customer = $product->customer_id;
            $node->email = trim($product->customer->email);
            $node->products = $product->details()->lists('product_id');
            $node->time = $product->created_at->timestamp;
        } catch (\Exception $e) {
            \Utils::log($e->getMessage(), __METHOD__);
        }

        return $node;
    }

    private function createOrderNodeV2(\Order $product)
    {
        $node = null;
        try {
            $node = new \stdClass();
            $node->id = $product->id;
            $node->reference = $product->reference;
            $node->state = \OrderState::statusToString($product->status);
            $node->payment_state = \PaymentState::statusToString($product->payment_status);
            $node->customer = $product->customer_id;
            $customer = $product->getCustomer();
            $node->email = $customer ? trim($customer->email) : null;
            $node->created_at = $this->transformDate($product->created_at);
            $node->time = $product->created_at->timestamp;
            $products = [];
            $details = $product->details()->get();
            foreach ($details as $detail) {
                $product_node = new \stdClass();
                $product_node->id = $detail->product_id;
                $product_node->quantity = $detail->product_quantity;
                $product_node->price = \Format::float($detail->product_item_price);
                $products[] = $product_node;
            }
            $node->products = $products;
        } catch (\Exception $e) {
            \Utils::log($e->getMessage(), __METHOD__);
            \Utils::log($e->getTraceAsString(), __METHOD__);
        }

        return $node;
    }


    function getJsonFile()
    {
        $folder = storage_path('xml/clerk');
        if (!is_dir($folder)) {
            mkdir($folder, 0755);
        }
        $filename = "clerk-{$this->lang}-{$this->locale}.json";
        return $folder . '/' . $filename;
    }

    function saveFile()
    {
        $json = $this->export();
        $content = json_encode($json, JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
        $filename = $this->getJsonFile();
        $this->console->info("File saved at $filename");
        \File::put($filename, $content);
    }

    /**
     * @param $date
     * @return string
     */
    private function transformDate($date)
    {
        if ($date instanceof Carbon) {

        } else {
            $date = Carbon::parse($date);
        }
        return $date->toIso8601String();
    }
}