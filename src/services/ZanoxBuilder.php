<?php namespace services;




class ZanoxBuilder
{
    protected $lang;
    protected $locale;
    protected $filename;

    function __construct($lang,$locale){
        $this->locale = $locale;
        $this->lang = $lang;
        $this->filename = strtolower('zanox_' . "$lang-$locale" . ".xml");
    }


    function batch(){
        $gm = new ZanoxFeed($this->lang,$this->locale);
        try{
            $xml = $gm->export();
            $dir = storage_path("xml/zanox");
            if(!is_dir($dir))mkdir($dir);
            $file = $dir."/".$this->filename;
            \File::put($file,$xml);
            return $xml;
        }catch(\Exception $e){
            \Utils::error($e->getMessage(),__METHOD__);
            \Utils::error($e->getTraceAsString(),__METHOD__);
        }
        return null;
    }


}
