<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 14/02/2019
 * Time: 11:17
 */

namespace services\TagCommander;

use Order;
use OrderDetail;
use Format;
use Illuminate\Support\Str;

class TagCommanderOrderRefundItem
{

    const PRODUCT_SEPARATOR = '--';
    const PRODUCT_ATTRIBUTES_SEPARATOR = '__';

    /**
     * @var Order
     */
    protected $order;

    /**
     * @var OrderDetail[]
     */
    protected $details;

    /**
     * TagCommanderOrderRefundItem constructor.
     * @param Order $order
     * @param OrderDetail[] $details
     */
    function __construct(Order $order, $details = [])
    {
        $this->order = $order;
        $this->details = $details;
    }

    /**
     * @return array|null
     */
    function makePayload()
    {
        if (is_null($this->order) or is_null($this->details) or empty($this->details)) {
            return null;
        }
        $order_id = $this->getOrderId();
        $refund_currency = $this->order->getCurrency()->iso_code;
        $refund_amount = $this->getRefundAmount();
        $refund_products_info = $this->getRefundProductsInfo();
        return compact('order_id', 'refund_currency', 'refund_amount', 'refund_products_info');
    }


    /**
     * @return string
     */
    protected function getOrderId()
    {
        $reference = $this->order->reference;
        if ($this->order->isChild()) {
            $master = $this->order->getParent();
            $reference = $master->reference;
        }
        return $reference;
    }

    /**
     * @return float
     */
    protected function getRefundAmount()
    {
        $amount = 0;
        foreach ($this->details as $detail) {
            $quantity = $detail->product_quantity;
            $unit_price = $detail->cart_price_tax_excl;
            $amount += $quantity * $unit_price;
        }
        return Format::money($amount, $this->order->currency_id);
    }

    /**
     * @return string
     */
    protected function getRefundProductsInfo()
    {
        $str_items = [];
        foreach ($this->details as $detail) {
            $partials = [];
            $partials[] = $detail->product_reference; //sku
            $partials[] = $detail->product_quantity; //quantity
            $partials[] = Format::money($detail->cart_price_tax_excl, $this->order->currency_id); //unit_price
            $partials[] = Str::slug($detail->getAttribute('label'));
            $str_items[] = implode(self::PRODUCT_ATTRIBUTES_SEPARATOR, $partials);
        }
        $str = implode(self::PRODUCT_SEPARATOR, $str_items);
        return $str;
    }
}