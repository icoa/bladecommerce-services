<?php


namespace services\TagCommander;

use Customer;
use DB, Utils, Config, Exception;
use Illuminate\Support\Str;
use Illuminate\Filesystem\Filesystem;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Mail;

/**
 * Class CommanderActApiService
 * @package services\TagCommander
 */
class CommanderActApiService
{
    /**
     * @var Client
     */
    protected $client;

    /**
     * @var Filesystem
     */
    protected $files;

    /**
     * @var string
     */
    protected $endpoint = '/engage/user/?site=[site]&user_id=[user_id]&token=[token]';

    /**
     * @var Customer|null
     */
    protected $customer;

    /**
     * @var string|null
     */
    protected $error;

    /**
     * CommanderActApiService constructor.
     * @param Filesystem $files
     */
    function __construct(Filesystem $files)
    {
        $this->files = $files;
        $this->client = new Client(['verify' => false]);
    }

    /**
     * @param Customer $customer
     * @return $this
     */
    function setCustomer(Customer $customer)
    {
        $this->customer = $customer;
        return $this;
    }

    /**
     * @return Customer|null
     */
    function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @return string|null
     */
    public function getError()
    {
        return $this->error;
    }

    /**
     * @param string|null $error
     */
    public function setError($error)
    {
        $this->error = $error;
    }


    /**
     * @return string
     * @throws Exception
     */
    protected function makeUri()
    {

        $customer = $this->getCustomer();

        $params = [
            'site' => config('plugins_settings.commanderAct.api.site', null),
            'token' => config('plugins_settings.commanderAct.api.token', null),
            'user_id' => $customer ? $customer->getUuidAttribute() : null,
            'domain' => config('plugins_settings.commanderAct.api.domain', null),
        ];

        if (\App::environment() !== 'live') {
            $params['user_id'] = 999999999;
        }

        $tokens = [];

        foreach ($params as $key => $value) {
            if ($value === null or strlen($value) == 0) {
                throw new Exception("Parameter [$key] is mandatory to create the endpoint in CommanderActApiService");
            }
            $tokens[] = "[$key]";
        }

        $endpoint = $params['domain'] . str_replace($tokens, array_values($params), $this->endpoint);
        return $endpoint;
    }

    /**
     * @return bool
     */
    function deleteCustomer()
    {
        try {
            $uri = $this->makeUri();
            audit($uri, '$uri', __METHOD__);
            $res = $this->client->put($uri);

            $mime = null;
            $statusCode = $res->getStatusCode();

            if ($statusCode == 200) {
                $resourceBody = $res->getBody();
                audit($resourceBody, '$resourceBody', __METHOD__);
                $payload = json_decode($resourceBody);
                if ($payload->success == true) {
                    DB::table('customers')->where('id', $this->getCustomer()->id)->update(['deleted' => 1]);
                    return true;
                }
            } else {
                throw new Exception("CommanderAct API has responded with a status code of [$statusCode]");
            }
        } catch (Exception $e) {
            $this->setError($e->getMessage());
            return false;
        }

        return false;
    }

    function sendReport()
    {
        $recipient_internal = 'f.politi@icoa.it';
        $customer = $this->getCustomer();
        $error = $this->getError();
        $customer->api_message = $error ? $error : 'Successfully removed';

        $rows = [$customer];

        Mail::send('emails.vendors.commanderAct_customer_delete', compact('rows'), function ($mail) use ($recipient_internal) {
            $mail->from(\Cfg::get('MAIL_GENERAL_ADDRESS'));
            $mail->subject("Blade report: cancellazione Utente effettuata su CommanderAct");
            $mail->to($recipient_internal);
            $mail->bcc('f.politi@icoa.it');
        });
    }
}