<?php


namespace services\TagCommander;

use Order;
use OrderState;
use Carbon\Carbon;
use services\Traits\TraitLocalDebug;
use Exception;
use Config;
use Mail;

class TagCommanderOrderRepository
{

    const OPTION_KEY = 'tc_refund_executed_at';
    const OPTION_END_KEY = 'tc_refund_ends_at';

    use TraitLocalDebug;

    /**
     * @var Carbon
     */
    protected $start_date;

    /**
     * @var Carbon
     */
    protected $end_date;

    /**
     * @var bool
     */
    protected $localDebug = false;

    /**
     * @var array
     */
    protected $tellings = [];

    /**
     * @return Carbon
     */
    public function getStartDate()
    {
        return is_null($this->start_date) ? Carbon::yesterday() : $this->start_date;
    }

    /**
     * @param string|Carbon $start_date
     * @return self
     */
    public function setStartDate($start_date)
    {
        if ($start_date instanceof Carbon) {
            $this->start_date = $start_date;
        } else {
            $this->start_date = Carbon::parse($start_date);
        }
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEndDate()
    {
        return is_null($this->end_date) ? Carbon::tomorrow() : $this->end_date;
    }

    /**
     * @param mixed $end_date
     * @return self
     */
    public function setEndDate($end_date)
    {
        if ($end_date instanceof Carbon) {
            $this->end_date = $end_date;
        } else {
            $this->end_date = Carbon::parse($end_date);
        }
        return $this;
    }

    /**
     * @param bool $skip_checks
     * @return null|string
     */
    public function make($skip_checks = false)
    {
        $now = Carbon::now();
        $pivot = Carbon::now();
        if ($skip_checks == false) {
            $latest_execution = option(self::OPTION_KEY);
            $stops_at = option(self::OPTION_END_KEY);
            if (is_null($latest_execution)) {
                //set to relative start, like 2019-02-14 00:01:00
                $this->setStartDate($pivot->setTime(0, 1, 0));
                //set to relative end, like 2019-02-15 00:01:00
                $this->setEndDate($pivot->addDay()->setTime(0, 1, 0));
            } else {
                $this->setStartDate($latest_execution);
                $this->setEndDate($pivot->addDay()->setTime(0, 1, 0));
            }
            $start_str = $this->getStartDate()->format('Y-m-d H:i:s');
            $end_str = $this->getEndDate()->format('Y-m-d H:i:s');
            $this->tell("Looking for refund order in the range [$start_str => $end_str]");

            //if saved latest execution is already set, then check if the class should run
            if (!is_null($latest_execution) and $this->getStartDate()->gte($now)) {
                $this->tell("Tag generation is skipped since START_DATE is greater than LATEST_EXECUTION");
                $this->audit([$this->getStartDate(), $now], 'SKIPPED');
                return null;
            }

            //if STOPS_AT is set, then NOW must be greather the STOPS_AT
            if (!is_null($stops_at)) {
                $stops = Carbon::parse($stops_at);
                if ($now->lt($stops)) {
                    $this->tell("Tag generation is skipped since NOW is greater than STOPS_AT");
                    return null;
                }
            }
        }

        $orders = $this->getRefundOrders();

        $total = count($orders);
        $this->tell("Selected a total of [$total] orders for refund");

        $script = $this->toJson($orders);

        $this->audit($script, 'SCRIPT');

        if ($total > 0) {
            $end_str = $this->getEndDate()->format('Y-m-d H:i:s');
            $now_str = $now->format('Y-m-d H:i:s');
            option()->set(self::OPTION_KEY, $now_str);
            option()->set(self::OPTION_END_KEY, $end_str);
            $this->tell("Setting EXEC_AT to $now_str amd STOP_AP $end_str and sending email");
            $this->notifyOrders($orders);
        }

        return $script;

    }

    /**
     * @return \Order[]
     */
    function getRefundOrders()
    {
        $this->audit_watch();
        $start_date = $this->getStartDate()->format('Y-m-d');
        $end_date = $this->getEndDate()->format('Y-m-d');
        $this->audit("start: $start_date, end: $end_date", __METHOD__);

        $invalid_modes = [Order::MODE_MASTER];

        /** @var Order[] $orders */
        $orders = Order::whereNotIn('availability_mode', $invalid_modes)->refundableOrSoftCancelled([$start_date, $end_date])->get();

        foreach ($orders as $order) {
            $this->audit($order->id, 'Wrapping order for refund');
            $tc_payload = null;
            $order_details = [];
            //if the order has RMA and is generally in 'REFUNDED' states
            if ($order->hasOwnState([OrderState::STATUS_REFUNDED, OrderState::STATUS_REFUNDED_PARTIALLY]) and $order->hasRma()) {
                //Get the RMA(s)...
                $rmas = $order->getRma();
                foreach ($rmas as $rma) {
                    //..check if the RMA status is 'CONFIRMED'
                    if ($rma->isConfirmed()) {
                        //..get the products that are contained in the RMA
                        $rma_order_details = $rma->getProducts();
                        foreach ($rma_order_details as $order_detail) {
                            if (isset($order_detail->detail)) {
                                //add the details to the general array
                                $order_detail->detail->product_quantity = $order_detail->product_quantity;
                                $order_detail->detail->product = $order_detail->product;
                                $order_details[] = $order_detail->detail;
                            }
                        }
                    }
                }
            } else {
                //than the order is probably in a 'SOFT_CANCELLED' status, and we fetch all product details
                $order_details = $order->getProducts();
            }


            try {
                $orderRefundItem = new TagCommanderOrderRefundItem($order, $order_details);
                $tc_payload = $orderRefundItem->makePayload();
                $this->audit($tc_payload, 'TC_PAYLOAD');
            } catch (Exception $e) {
                audit_exception($e, __METHOD__);
            }

            $order->tc_payload = $tc_payload;
        }

        return $orders;
    }


    /**
     * @param Order[] $orders
     * @return null|string
     */
    protected function toJson($orders)
    {
        $items = [];
        foreach ($orders as $index => $order) {
            if (isset($order->tc_payload) and is_array($order->tc_payload)) {
                $items[$index] = <<<SCRIPT
tc_vars["refund_array"].push([]);
tc_vars["refund_array"][$index]["order_id"] = "{$order->tc_payload['order_id']}";
tc_vars["refund_array"][$index]["refund_amount"] = "{$order->tc_payload['refund_amount']}";
tc_vars["refund_array"][$index]["refund_currency"] = "{$order->tc_payload['refund_currency']}";
tc_vars["refund_array"][$index]["refund_products_info"] = "{$order->tc_payload['refund_products_info']}";
SCRIPT;
            }
        }

        if (!empty($items)) {
            $env_work = config('plugins_settings.commanderAct.env', 'prod');
            $script = "tc_vars = []; tc_vars[\"env_work\"] = \"$env_work\";" . PHP_EOL;
            $script .= "tc_vars[\"refund_array\"] = [];" . PHP_EOL;
            $script .= 'tc_vars["env_template"] = "refund";' . PHP_EOL . implode(PHP_EOL, $items);
            return $script;
        }
        return null;
    }


    /**
     * @param Order[] $orders
     * @return void
     */
    private function notifyOrders($orders)
    {
        if (config('plugins_settings.tagCommander.refund.send_email', false) === false)
            return;


        $rows = [];

        foreach ($orders as $order) {
            $id = $order->id;
            $reference = $order->reference;
            $carrier = $order->carrierName();
            $payment = $order->payment()->name;
            $status = $order->statusName();
            $payment_status = $order->paymentStatusName();
            $url = $order->getBackendLinkAttribute();
            $payload = print_r($order->tc_payload, 1);
            $rows[] = (object)compact('id', 'reference', 'carrier', 'payment', 'status', 'payment_status', 'url', 'payload');
        }

        Config::set('mail.pretend', false);

        $recipient_external = config('plugins_settings.tagCommander.refund.recipient');

        Mail::send('emails.morellato.tagCommanderRefund', compact('rows'), function ($mail) use ($recipient_external) {
            $mail->from(\Cfg::get('MAIL_GENERAL_ADDRESS'));
            $mail->subject("Blade report: TagCommander Refund Orders");
            $mail->to($recipient_external);
            $mail->bcc('f.politi@icoa.it');
        });
    }

    /**
     * @param $message
     * @param null $pre
     */
    protected function tell($message, $pre = null)
    {
        if (is_array($message) or is_object($message)) {
            $message = print_r($message, 1);
        }
        $message = ($pre) ? $pre . ' => ' . $message : $message;
        $this->tellings[] = $message;
    }

    public function getSpeech()
    {
        return implode(PHP_EOL, $this->tellings);
    }
}