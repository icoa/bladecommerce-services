<?php


namespace services\Repositories;

use Core;
use DB;
use services\Traits\TraitConsole;
use SplFileObject;
use Exception;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Coupon;
use League\Csv\Writer;

class CartRuleRepository extends Repository
{

    use TraitConsole;

    const CSV_DELIMITER = ";";
    const CSV_NEWLINE = "\r\n";

    /**
     * @param int $size
     * @param bool $uppercase
     * @return string
     */
    public function generateUniqueCode($size = 10, $uppercase = true)
    {
        $code = Core::randomString($size, $uppercase);
        $exists = Coupon::where('code', $code)->where('active', 1)->count();
        if ($exists)
            return $this->generateUniqueCode($size, $uppercase);

        return $code;
    }


    /**
     * @param array $params
     * @throws Exception
     */
    public function generateCoupons($params = [])
    {
        $console = $this->getConsole();
        $required = [
            'len',
            'total',
            'cart_rule_id',
        ];

        foreach ($required as $key) {
            if (!isset($params[$key]))
                throw new Exception("Missing required parameter [$key]");
        }
        $cp_len = $params["len"];
        $cp_total = $params["total"];
        $cart_rule_id = $params["cart_rule_id"];
        $cp_prefix = isset($params["prefix"]) ? $params["prefix"] : null;
        $cp_suffix = isset($params["suffix"]) ? $params["suffix"] : null;


        if ($cart_rule_id > 0) {
            $random = '';
            $random_len = $cp_len - Str::length($cp_prefix) - Str::length($cp_suffix);

            if ($console)
                $this->console->comment("Generating [$cp_total] codes...");


            for ($i = 0; $i < $cp_total; $i++) {
                DB::beginTransaction();
                try {
                    if ($random_len > 0) {
                        $random = $this->generateUniqueCode($random_len);
                    }
                    $code = Str::upper($cp_prefix . $random . $cp_suffix);
                    $coupon = new Coupon();
                    $coupon->code = $code;
                    $coupon->cart_rule_id = $cart_rule_id;
                    $coupon->save();
                    unset($coupon);
                    DB::commit();

                    if ($console)
                        $this->console->info("Successfully generated code [$code] ($i/$cp_total)");

                } catch (Exception $e) {
                    DB::rollBack();
                    throw new Exception("Internal error; operation reverted: " . $e->getMessage());
                }
            }

        } else {
            throw new Exception('Per generare codici aggiuntivi devi prima salvare per la prima volta');
        }
    }


    /**
     * @param array $params
     * @throws Exception
     */
    public function generateCouponsSimple($params = [])
    {
        $console = $this->getConsole();
        $required = [
            'len',
            'total',
            'cart_rule_id',
        ];

        foreach ($required as $key) {
            if (!isset($params[$key]))
                throw new Exception("Missing required parameter [$key]");
        }
        $cp_len = $params["len"];
        $cp_total = $params["total"];
        $cart_rule_id = $params["cart_rule_id"];
        $cp_prefix = isset($params["prefix"]) ? $params["prefix"] : null;
        $cp_suffix = isset($params["suffix"]) ? $params["suffix"] : null;
        $lines = [];

        if ($cart_rule_id > 0) {
            $random = '';
            $random_len = $cp_len - Str::length($cp_prefix) - Str::length($cp_suffix);
            $created_at = $updated_at = date('Y-m-d H:i:s');

            if ($console)
                $this->console->comment("Generating [$cp_total] codes...");

            for ($i = 0; $i < $cp_total; $i++) {
                try {
                    if ($random_len > 0) {
                        $random = Core::randomString($random_len);
                    }
                    $code = Str::upper($cp_prefix . $random . $cp_suffix);
                    $active = 1;
                    $data = compact('code', 'cart_rule_id', 'active', 'created_at', 'updated_at');
                    $field_list = implode(',', array_keys($data));
                    $value_list = "'" . implode("','", array_values($data)) . "'";
                    $query = "INSERT INTO coupons($field_list) VALUES($value_list);";
                    $lines[] = $query;

                    if ($console)
                        $this->console->info("Successfully generated code [$code] ($i/$cp_total)");

                } catch (Exception $e) {
                    throw new Exception("Internal error; operation reverted: " . $e->getMessage());
                }
            }

        } else {
            throw new Exception('Per generare codici aggiuntivi devi prima salvare per la prima volta');
        }

        $sqlFile = storage_path('coupons.sql');
        file_put_contents($sqlFile, implode(PHP_EOL, $lines));

        if ($console)
            $this->console->info("Successfully generated file $sqlFile");
    }


    /**
     * @param $cart_rule_id
     * @return string
     * @throws Exception
     */
    public function exportCsvForCoupons($cart_rule_id)
    {
        if ($cart_rule_id <= 0) {
            throw new Exception('Invalid cart_rule_id');
        }
        $console = $this->getConsole();
        $lines = [];
        $coupons = Coupon::where('cart_rule_id', $cart_rule_id)->get();
        $headers = ['code', 'active', 'used_by', 'used_at', 'created_at'];
        $many = count($coupons);

        if ($console)
            $this->console->comment("Exporting [$many] codes...");

        foreach ($coupons as $coupon) {
            $lines[] = [
                'code' => $coupon->code,
                'active' => $coupon->active,
                'used_by' => $coupon->used_by,
                'used_at' => $coupon->used_at,
                'created_at' => $coupon->created_at,
            ];
        }

        $rows = array_merge([$headers], $lines);

        $targetFilePath = storage_path("{$cart_rule_id}_export_coupons.csv");

        $writer = Writer::createFromPath(new SplFileObject($targetFilePath, 'w'), 'w');
        $writer->setEncodingFrom('UTF-8');
        $writer->setDelimiter(self::CSV_DELIMITER); //the delimiter will be the tab character
        $writer->setNewline(self::CSV_NEWLINE); //use windows line endings for compatibility with some csv libraries
        $writer->insertAll($rows);

        if ($console)
            $this->console->info("Successfully generated file at [$targetFilePath]");

        return $targetFilePath;
    }

}