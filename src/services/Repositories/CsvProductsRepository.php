<?php

namespace services\Repositories;

use Illuminate\Database\Eloquent\Builder;
use Product;
use Brand;
use SplFileObject;
use Exception;
use League\Csv\Writer;
use Illuminate\Support\Str;
use Format;

class CsvProductsRepository extends Repository
{

    const CSV_DELIMITER = ';';
    const CSV_NEWLINE = "\r\n";

    /**
     * @param array $params
     * @return Builder
     */
    function build(array $params)
    {
        $from = array_get($params, 'from', '');
        $to = array_get($params, 'to', '');
        $brand_ids = array_get($params, 'brand_ids', []);
        $warehouse_ids = array_get($params, 'warehouse_ids', []);
        $is_outlet = (int)array_get($params, 'is_outlet', 0);
        $is_new = (int)array_get($params, 'is_new', 0);
        $is_featured = (int)array_get($params, 'is_featured', 0);
        $is_soldout = (int)array_get($params, 'is_soldout', 0);
        $is_available = (int)array_get($params, 'is_available', 0);

        $query = Product::query();
        if ($from !== '') {
            $from = Format::sqlDate($from);
            $query->where('created_at', '>=', $from);
        }
        if ($to !== '') {
            $to = Format::sqlDate($to);
            $query->where('created_at', '<=', $to);
        }
        if (count($brand_ids) > 0) {
            $query->whereIn('brand_id', $brand_ids);
        }
        if ($is_outlet === 1) {
            $query->where('is_outlet', 1);
        }
        if ($is_new === 1) {
            $query->where('is_new', 1);
        }
        if ($is_featured === 1) {
            $query->where('is_featured', 1);
        }
        if ($is_soldout === 1) {
            $query->where('is_soldout', 1);
        }
        if ($is_available === 1) {
            $query->where('qty', '>', 0);
        }
        if (!empty($warehouse_ids)) {
            $query->whereIn('id', function ($subquery) use ($warehouse_ids) {
                $subquery->select('product_id')->from('products_specific_stocks')->whereIn('warehouse', $warehouse_ids);
            });
        }

        $query->orderBy('id', 'desc');

        return $query;
    }


    /**
     * @param Builder $query
     * @return string
     */
    function export(Builder $query)
    {

        $platform = \Cfg::get('SHORTNAME');

        $headers = [
            'id' => 'ID',
            'sku' => 'SKU',
            'sapsku' => 'SAPSKU',
            'qty' => 'QTY',
            'sell_price_wt' => 'SELL PRICE WT',
            'is_new' => 'NEW',
            'is_soldout' => 'SOLDOUT',
            'is_outlet' => 'OUTLET',
            'is_featured' => 'FEATURED',
            'is_out_of_production' => 'OUT-OF-PRODUCTION',
            'brand' => 'BRAND',
        ];

        $lines = [];

        /** @var Product[] $products */
        $products = $query->get();

        foreach ($products as $product) {
            $brand = Brand::getObj($product->brand_id);
            $lines[] = [
                'id' => $product->id,
                'sku' => $product->sku,
                'sapsku' => $product->sap_sku,
                'qty' => $product->qty,
                'sell_price_wt' => $product->sell_price_wt,
                'is_new' => $product->is_new,
                'is_soldout' => $product->is_soldout,
                'is_outlet' => $product->is_outlet,
                'is_featured' => $product->is_featured,
                'is_out_of_production' => $product->is_out_of_production,
                'brand' => $brand ? $brand->name : null,
            ];
        }

        $rows = array_merge([$headers], $lines);

        $platform = Str::slug($platform, '_');

        $targetFilePath = storage_path($platform . '_catalog_export.csv');

        $writer = Writer::createFromPath(new SplFileObject($targetFilePath, 'w'), 'w');
        $writer->setEncodingFrom('UTF-8');
        $writer->setDelimiter(self::CSV_DELIMITER); //the delimiter will be the tab character
        $writer->setNewline(self::CSV_NEWLINE); //use windows line endings for compatibility with some csv libraries
        $writer->insertAll($rows);
        return $targetFilePath;
    }

    function getProducts()
    {
        $headers = [
            'sku' => 'SKU',
            'website_product_id' => 'WEBSITE_PRODUCT_ID',
            'name' => 'NAME',
            'available' => 'AVAILABLE',
            'description' => 'DESCRIPTION',
            'current_price' => 'CURRENT_PRICE',
            'base_price' => 'BASE_PRICE',
            'discount_percentage' => 'DISCOUNT_PERCENTAGE',
            'url' => 'URL',
            'image_url' => 'IMAGE_URL',
            'thumbnail_url' => 'THUMBNAIL_URL',
            'ean' => 'EAN',
            'size' => 'SIZE',
            'color' => 'COLOR',
            'brand_name' => 'BRAND_NAME',
            'brand_url' => 'BRAND_URL',
            'brand_image_url' => 'BRAND_IMAGE_URL',
            'collection_name' => 'COLLECTION_NAME',
            'collection_url' => 'COLLECTION_URL',
            'category_name' => 'CATEGORY_NAME',
            'category_url' => 'CATEGORY_URL',
            'subcategory_name' => 'SUBCATEGORY_NAME',
            'subcategory_url' => 'SUBCATEGORY_URL',
            'gender_name' => 'GENDER_NAME',
            'gender_url' => 'GENDER_URL',
        ];


        $lines = [];
        $platform = \Cfg::get('SHORTNAME');

        Product::orderBy('id', 'desc')
            ->inStocks()
            ->outlet(0)
            ->soldout(0)
            ->select('id')
            ->chunk(100, function ($rows) use (&$lines, $platform) {
                /** @var Product[] $rows */
                foreach ($rows as $row) {

                    $product = Product::getFullProduct($row->id);

                    //audit($product, __METHOD__);

                    if ($product) {
                        $available = $product->availabilityStatus == 'in_stock';
                        $url = $product->link_absolute;
                        $base_price = $product->price_official_raw;
                        $current_price = $product->price_final_raw;
                        $ean = $product->ean13;
                        $image_url = $product->defaultImg;
                        $thumbnail_url = $product->thumbImg;
                        $brand_name = $product->brand_name;
                        $brand_url = \Link::absolute()->to('brand', $product->brand_id);
                        $brand_image_url = \Site::rootify($product->brand_image_default);
                        $collection_name = $product->collection_name;
                        $collection_url = \Link::absolute()->to('collection', $product->collection_id);
                        $category_name = $product->main_category_name;
                        $category_url = ($product->main_category_id > 0) ? \Link::absolute()->to('category', $product->main_category_id) : null;
                        $subcategory_name = $product->category_name;
                        $subcategory_url = ($product->default_category_id > 0) ? \Link::absolute()->to('category', $product->default_category_id) : null;
                        $gender_name = $product->gender;
                        $gender_url = ($product->attribute_gender > 0) ? \Link::absolute()->to('gender', $product->attribute_gender) : null;

                        $data = [
                            'sku' => $product->sku,
                            'website_product_id' => $product->id,
                            'name' => $product->name,
                            'available' => (int)$available,
                            'description' => null,
                            'current_price' => $current_price,
                            'base_price' => $base_price,
                            'discount_percentage' => null,
                            'url' => $url,
                            'image_url' => $image_url,
                            'thumbnail_url' => $thumbnail_url,
                            'ean' => $ean,
                            'size' => null,
                            'color' => null,
                            'brand_name' => $brand_name,
                            'brand_url' => $brand_url,
                            'brand_image_url' => $brand_image_url,
                            'collection_name' => $collection_name,
                            'collection_url' => $collection_url,
                            'category_name' => $category_name,
                            'category_url' => $category_url,
                            'subcategory_name' => $subcategory_name,
                            'subcategory_url' => $subcategory_url,
                            'gender_name' => $gender_name,
                            'gender_url' => $gender_url,
                        ];

                        $lines[] = $data;
                    }

                }
            });


        audit($headers, __METHOD__, 'HEADERS');
        audit($lines, __METHOD__, 'LINES');

        $rows = array_merge([$headers], $lines);

        $platform = Str::slug($platform, '_');

        $targetFilePath = storage_path($platform . '_catalog_export.csv');

        $writer = Writer::createFromPath(new SplFileObject($targetFilePath, 'w'), 'w');
        $writer->setEncodingFrom('UTF-8');
        $writer->setDelimiter(self::CSV_DELIMITER); //the delimiter will be the tab character
        $writer->setNewline(self::CSV_NEWLINE); //use windows line endings for compatibility with some csv libraries
        $writer->insertAll($rows);
        return $targetFilePath;
    }
}