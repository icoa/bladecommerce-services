<?php

namespace services\Repositories;

use DB;

class ResellerRepository extends Repository
{

    protected $latitude;
    protected $longitude;
    protected $radius = 50.0;
    protected $distance_unit = 111.045; //km

    /**
     * @return mixed
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * @param mixed $latitude
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;
    }

    /**
     * @return mixed
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * @param mixed $longitude
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;
    }

    /**
     * @return int
     */
    public function getRadius()
    {
        return $this->radius;
    }

    /**
     * @param int $radius
     */
    public function setRadius($radius)
    {
        $this->radius = $radius;
    }

    /**
     * @return float
     */
    public function getDistanceUnit()
    {
        return $this->distance_unit;
    }

    /**
     * @param float $distance_unit
     */
    public function setDistanceUnit($distance_unit)
    {
        $this->distance_unit = $distance_unit;
    }


    /**
     * @param $latitude
     * @param $longitude
     * @return $this
     */
    function setCoordinates($latitude, $longitude)
    {
        $this->setLatitude($latitude);
        $this->setLongitude($longitude);
        return $this;
    }

    /**
     * @return mixed
     */
    private function getBrandId(){
        return config('morellato.reseller_brand_id', 1);
    }

    /**
     * @return string
     */
    private function getQuery(){

        $brand_id = $this->getBrandId();

        $query = <<<QUERY
SELECT *
  FROM (
 SELECT 
				z.*,
        p.radius,
        p.distance_unit
                 * DEGREES(ACOS(COS(RADIANS(p.latpoint))
                 * COS(RADIANS(z.latitude))
                 * COS(RADIANS(p.longpoint - z.longitude))
                 + SIN(RADIANS(p.latpoint))
                 * SIN(RADIANS(z.latitude)))) AS distance
  FROM resellers AS z
  JOIN ( 
        SELECT  $this->latitude AS latpoint, $this->longitude AS longpoint,
                $this->radius AS radius, $this->distance_unit AS distance_unit
    ) AS p ON 1=1
  WHERE 
    active=1 and isnull(deleted_at) and brand=$brand_id and
	z.latitude is not null and z.longitude is not null
	and z.latitude
     BETWEEN p.latpoint  - (p.radius / p.distance_unit)
         AND p.latpoint  + (p.radius / p.distance_unit)
    AND z.longitude
     BETWEEN p.longpoint - (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
         AND p.longpoint + (p.radius / (p.distance_unit * COS(RADIANS(p.latpoint))))
 ) AS d
 WHERE (distance <= radius or ISNULL(distance))
 ORDER BY distance
QUERY;
        return $query;
    }

    function getStores()
    {
        $query = $this->getQuery();
        \Utils::log($query);
        $results = DB::select($query);
        $rows = [];
        foreach($results as $result){
            $obj = new \Reseller((array)$result);
            $rows[] = $obj;
        }
        return $rows;
    }

    function search($latitude, $longitude){
        $theme = \FrontTpl::getTheme();
        $stores = $this->setCoordinates($latitude, $longitude)->getStores();
        audit($stores, __METHOD__);
        return $theme->partial('store.resellers', compact('stores'));
    }
}