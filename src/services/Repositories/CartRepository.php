<?php

namespace services\Repositories;

use DB;
use Cart;
use CartProduct;
use Customer;
use services\Traits\TraitConsole;
use SplFileObject;
use Exception;
use League\Csv\Writer;
use Carbon\Carbon;
use Illuminate\Support\Str;

class CartRepository extends Repository
{

    use TraitConsole;

    const CSV_DELIMITER = ";";
    const CSV_NEWLINE = "\r\n";

    function getAbandonedCarts($start_date)
    {
        $date = Carbon::parse($start_date)->format('Y-m-d');
        $headers = [
            'platform' => 'Platform',
            'cart_id' => 'Cart ID',
            'secure_key' => 'Cart Secure Key',
            'payment_method' => 'Payment Method',
            'shipment_method' => 'Shipment Method',
            'coupon_code' => 'Coupon Code',
            'campaign' => 'Campaign',
            'lang_id' => 'Site Language',
            'currency_id' => 'Site Currency',
            'origin' => 'Site Origin',
            'created_at' => 'Created At',
            'customer_name' => 'Customer Name',
            'customer_email' => 'Customer Email',
            'customer_communication' => 'Customer Communication Agreement',
            'customer_segmentation' => 'Customer Segmentation Agreement',
            'customer_city' => 'Customer City',
            'customer_phone' => 'Customer Phone',
            'customer_mobile' => 'Customer Phone 2',
        ];

        $currenciesRows = \Mainframe::currencies();
        $currencies = [];
        foreach ($currenciesRows as $currenciesRow) {
            $currencies[$currenciesRow->id] = $currenciesRow->iso_code;
        }

        $max_products = 10;

        for ($i = 0; $i < $max_products; $i++) {
            $headers[$i . '_product_sku'] = 'Product SKU #' . $i;
            $headers[$i . '_product_name'] = 'Product Name #' . $i;
            $headers[$i . '_product_brand'] = 'Product Brand #' . $i;
            $headers[$i . '_product_url'] = 'Product URL #' . $i;
            $headers[$i . '_product_image'] = 'Product Image #' . $i;
            $headers[$i . '_product_price'] = 'Product Price #' . $i;
            $headers[$i . '_product_final_price'] = 'Product Discounted Price #' . $i;
        }

        $lines = [];
        $platform = \Cfg::get('SHORTNAME');
        $link_template = config('plugins_settings.selligent.cart_link_decorator', 'https://gruppomorellato.commander1.com/c3/?tcs=4190&chn=recupero_carrello&src=selligent&cmp=&tp=sale&mcr=&prm=&kw=&cty=&url=%s?utm_medium=recupero_carrello&utm_source=selligent&utm_content=&utm_campaign=0&site=gruppo_morellato');
        $console = $this->getConsole();

        Cart::where('created_at', '>=', $date)->whereNotIn('id', function ($query) {
            $query->select('cart_id')->from('orders')->whereNull('deleted_at');
        })
            ->orderBy('id', 'desc')
            ->chunk(100, function ($rows) use (&$lines, $platform, $currencies, $max_products, $link_template, $console) {
                /** @var Cart[] $rows */
                foreach ($rows as $row) {

                    /** @var Customer $customer */
                    $customer = $row->getCustomer();
                    $address = $row->getShippingAddress();
                    $payment = $row->getPayment();
                    $carrier = $row->getCarrier();
                    $campaign = $row->getCampaign();

                    $data = [
                        'platform' => $platform,
                        'cart_id' => $row->id,
                        'secure_key' => $row->secure_key,
                        'payment_method' => ($payment) ? $payment->name : null,
                        'shipment_method' => ($carrier) ? $carrier->name : null,
                        'coupon_code' => $row->coupon_code,
                        'campaign' => ($campaign) ? $campaign->name : null,
                        'lang_id' => $row->lang_id,
                        'currency_id' => $currencies[$row->currency_id],
                        'origin' => $row->origin,
                        'created_at' => $row->created_at,
                        'customer_name' => ($customer) ? $customer->getName() : null,
                        'customer_email' => ($customer) ? $customer->email : null,
                        'customer_communication' => ($customer) ? (int)$customer->marketing : 0,
                        'customer_segmentation' => ($customer) ? (int)$customer->profile : 0,
                        'customer_city' => ($address) ? $address->name : null,
                        'customer_phone' => ($address) ? $address->phone : null,
                        'customer_mobile' => ($address) ? $address->phone_mobile : null,
                    ];

                    $cart_products = $row->getProducts();

                    $i = 0;
                    foreach ($cart_products as $cart_product) {
                        $product = isset($cart_product->product) ? $cart_product->product : null;
                        if ($product) {

                            $product_url = urlencode($product->link_absolute);
                            $page_url = sprintf($link_template, $product_url);

                            $data[$i . '_product_sku'] = $product->sku;
                            $data[$i . '_product_name'] = $product->name;
                            $data[$i . '_product_brand'] = $product->brand_name;
                            $data[$i . '_product_url'] = $page_url;
                            $data[$i . '_product_image'] = \Site::rootify($product->defaultImg);
                            $data[$i . '_product_price'] = $product->price_official_raw;
                            $data[$i . '_product_final_price'] = $product->price_final_raw;
                            $i++;
                        }
                    }

                    //add remaining rows
                    for (; $i < $max_products; $i++) {
                        $data[$i . '_product_sku'] = null;
                        $data[$i . '_product_name'] = null;
                        $data[$i . '_product_brand'] = null;
                        $data[$i . '_product_url'] = null;
                        $data[$i . '_product_image'] = null;
                        $data[$i . '_product_price'] = null;
                        $data[$i . '_product_final_price'] = null;
                    }

                    $lines[] = $data;
                }
            });


        //audit($headers, __METHOD__, 'HEADERS');
        //audit($lines, __METHOD__, 'LINES');

        $rows = array_merge([$headers], $lines);

        $targetFilePath = $this->getFilePath();

        $writer = Writer::createFromPath(new SplFileObject($targetFilePath, 'w'), 'w');
        $writer->setEncodingFrom('UTF-8');
        $writer->setDelimiter(self::CSV_DELIMITER); //the delimiter will be the tab character
        $writer->setNewline(self::CSV_NEWLINE); //use windows line endings for compatibility with some csv libraries
        $writer->insertAll($rows);
        return $targetFilePath;
    }


    /**
     * @return string
     */
    protected function getFilePath()
    {
        $filename = 'selligent_abandoned_carts.csv';
        $dir = storage_path('xml/selligent');
        if (!is_dir($dir)) {
            mkdir($dir, 0775);
        }
        return "$dir/$filename";
    }

    /**
     * This should be fetched with
     * es. http://morellato.local/feeds/selligent_abandoned_carts
     *
     * @param $file
     * @return string
     */
    static function getPathByFile($file)
    {
        return storage_path("xml/selligent/{$file}csv");
    }
}