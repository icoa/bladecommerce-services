<?php

namespace services\Repositories;

use Carbon\Carbon;
use Validator;
use Order;
use Customer;
use GiftCard;
use Email;
use Str;
use Link;
use GiftCardMovement;

class GiftCardRepository extends Repository
{
    protected $cards = [];

    protected $debug = true;

    protected $lang_id = null;

    /**
     * @param $data
     * @return array
     */
    function validate($data)
    {
        $success = true;
        $msg = null;
        $params = [];

        $rules = [
            'gc_sender_name' => 'required',
            'gc_sender_email' => 'required|email',
            'gc_recipient_name' => 'required',
            'gc_recipient_email' => 'required|email|confirmed',
        ];

        $trans = [
            'gc_sender_name.required' => trans("ajax.mandatory_field", ['field' => trans('template.giftcard.gc_sender_name')]),
            'gc_sender_email' => trans('template.giftcard.gc_sender_email'),
            'gc_recipient_name' => trans('template.giftcard.gc_recipient_name'),
            'gc_recipient_email' => trans('template.giftcard.gc_recipient_email'),
        ];

        $validator = Validator::make($data, $rules);
        if ($validator->fails()) {
            $success = false;
            foreach ($validator->errors()->all() as $error) {
                $msg .= $error . '<br>';
            }
        } else {
            $params['gc_sender_name'] = ucwords(trim($data['gc_sender_name']));
            $params['gc_sender_email'] = (trim($data['gc_sender_email']));
            $params['gc_recipient_name'] = ucwords(trim($data['gc_recipient_name']));
            $params['gc_recipient_email'] = (trim($data['gc_recipient_email']));
            $params['gc_message'] = (trim($data['gc_message']));
        }

        $params['gift_selected'] = isset($data['gift_selected']) ? 1 : 0;

        return compact('success', 'msg', 'params');
    }

    /**
     * @param Order $order
     */
    function orderPlaced(Order $order)
    {
        if ($this->debug)
            audit($order->id, __METHOD__);

        $this->lang_id = $order->lang_id;

        if ($this->debug)
            audit($this->lang_id, 'SETTING LANG TO');
        //when the order is placed and it has virtual products...
        if ($order->hasVirtualProducts() and $order->hasBeenPaid()) {
            //...generate gift cards with codes, and send an email to the gift recipient
            $this->generateGiftCards($order);
            //..send an email to the customer, with a PDF attachment
            $this->sendEmailToCustomer($order->getCustomer());
        }

        if ($order->hasGiftcard()) {
            $giftCard = $order->getGiftcard();

            if ($this->debug) {
                audit($giftCard->toArray(), __METHOD__);
                audit($order->toArray(), __METHOD__);
            }

            $existingMovement = GiftCardMovement::where(['order_id' => $order->id, 'gift_card_id' => $giftCard->id])->count();
            if($existingMovement > 0){
                audit('This movement has already been processed; skip it for ordinary reasons', __METHOD__);
                return;
            }

            $subTotal = $order->getSubTotalExceptExtra();
            $usage = $subTotal;
            if ($this->debug) {
                audit($subTotal, 'getSubTotalExceptExtra');
            }
            $updatedAmount = $giftCard->amount - $subTotal;
            if ($updatedAmount < 0) {
                $updatedAmount = 0;
                $usage = $giftCard->amount;
            }
            $giftCard->amount = $updatedAmount;
            $giftCard->save();
            if ($this->debug) {
                audit($giftCard, 'UPDATED GIFT CARD');
            }
            $this->addMovement($giftCard, $order, $usage);
        }

    }


    function addMovement(GiftCard $giftCard, Order $order, $amount)
    {
        $data = [
            'gift_card_id' => $giftCard->id,
            'order_id' => $order->id,
            'customer_id' => $order->customer_id,
            'amount' => $amount,
        ];
        if ($this->debug) {
            audit($data, __METHOD__);
        }

        GiftCardMovement::create($data);
    }

    /**
     * @param GiftCard $card
     */
    protected function addGiftCard(GiftCard $card)
    {
        $this->cards[] = $card;
    }

    /**
     * @return array
     */
    protected function getGiftCards()
    {
        return $this->cards;
    }

    /**
     * @param Order $order
     */
    protected function generateGiftCards(Order $order)
    {
        if ($this->debug)
            audit($order->id, __METHOD__);

        $code_length = config('negoziando.giftcard.code_length', 12);
        $ttl = config('negoziando.giftcard.ttl', 365);
        $customer_name = $order->getCustomer()->name;

        $products = $order->getProducts();
        foreach ($products as $product) {
            $qty = $product->product_quantity;

            if ($this->debug)
                audit($qty, 'PRODUCT_QTY');

            if ($product->product->type == 'virtual') {

                $params = $product->getAttribute('params');

                if ($this->debug)
                    audit($params, 'PRODUCT_PARAMS');

                while ($qty > 0) {
                    $giftCard = new GiftCard();

                    $amount = \Product::where('id', $product->product_id)->pluck('sell_price');
                    $code = Str::upper(Str::random($code_length));
                    $expires = Carbon::now()->addDays($ttl)->format('Y-m-d H:i:s');

                    $giftCard->fill([
                        'code' => $code,
                        'order_id' => $order->id,
                        'order_detail_id' => $product->id,
                        'product_id' => $product->product_id,
                        'name' => $product->product_name,
                        'customer_name' => $customer_name,
                        'amount' => $amount,
                        'original_amount' => $amount,
                        'expires_at' => $expires,
                    ]);

                    if ($this->debug)
                        audit($giftCard->toArray(), 'GIFT_CARD');

                    $giftCard->save();

                    if ($params and isset($params['gift_selected']) and $params['gift_selected'] == 1) {
                        $this->sendEmailToRecipient($giftCard, $params);
                    } else {
                        $this->addGiftCard($giftCard);
                    }

                    $qty--;
                }

            }
        }
    }

    /**
     * @param Customer $customer
     */
    protected function sendEmailToCustomer(Customer $customer)
    {
        if ($this->debug)
            audit($customer->id, __METHOD__);

        $codes = [];
        $giftCards = $this->getGiftCards();

        //if there are no gift cards
        if (empty($giftCards)) {
            if ($this->debug)
                audit($customer->id, 'THERE ARE NO CARDS FOR CUSTOMER');
        }

        foreach ($giftCards as $giftCard) {
            $codes[] = $giftCard->code;
        }

        $email = Email::getByCode('GIFT_CARD', $this->lang_id);
        $data = [
            'NAME' => $customer->name,
            'CODES' => implode('<br>', $codes),
            'URL' => $this->getSiteUrl(),
        ];

        if ($this->debug)
            audit($data, 'EMAIL_DATA');

        $attachments = [['file' => $this->getAttachment(), 'as' => $codes[0] . '.pdf']];

        if ($this->debug)
            audit($attachments, 'EMAIL_ATTACHMENTS');

        $email->setCustomer($customer)->send($data, $customer->email, $attachments);
    }

    /**
     * @return string
     */
    protected function getAttachment()
    {
        return storage_path('files/giftcards/' . Str::upper($this->lang_id) . '_GIFT_CARD.pdf');
    }

    /**
     * @return string
     */
    protected function getSiteUrl()
    {
        $url = Link::absolute()->shortcut('home', $this->lang_id);
        return "<a href='$url'>$url</a>";
    }

    /**
     * @param GiftCard $giftCard
     * @param array $params
     */
    protected function sendEmailToRecipient(GiftCard $giftCard, array $params)
    {
        if ($this->debug)
            audit($giftCard->id, __METHOD__);

        $recipient_name = $params['gc_recipient_name'];
        $recipient_email = $params['gc_recipient_email'];
        $message = $params['gc_message'];
        $sender_name = $params['gc_sender_name'];
        $sender_email = $params['gc_sender_email'];

        $email = Email::getByCode('GIFT_CARD_RECIPIENT', $this->lang_id);
        $data = [
            'NAME' => "$sender_name ($sender_email)",
            'RECIPIENT_NAME' => $recipient_name,
            'MESSAGE' => $message,
            'CODES' => $giftCard->code,
            'URL' => $this->getSiteUrl(),
        ];

        if ($this->debug)
            audit($data, 'EMAIL_DATA');

        $attachments = [['file' => $this->getAttachment(), 'as' => $giftCard->code . '.pdf']];

        if ($this->debug)
            audit($attachments, 'EMAIL_ATTACHMENTS');

        $email->send($data, $recipient_email, $attachments);
    }


    /**
     * @param $code
     * @return GiftCard|null
     * @throws \Exception
     */
    function check($code)
    {
        if ($this->debug)
            audit($code, __METHOD__);

        $code_length = config('negoziando.giftcard.code_length', 12);


        if (Str::length($code) != $code_length) {
            throw new \Exception(lex('error_giftcard_wrong_length'), 400);
        }

        $giftCard = GiftCard::where('code', $code)->first();
        if ($giftCard) {
            $expires = Carbon::parse($giftCard->expires_at);
            $now = Carbon::now();
            $diffDays = $expires->diffInDays($now, false);

            if ($this->debug) {
                audit($diffDays, 'DIFF DAYS');
                audit($giftCard->toArray(), 'GIFT CARD');
            }

            if ($diffDays >= 0) {
                throw new \Exception(lex('error_giftcard_expired'), 500);
            }

            return $giftCard;
        } else {
            throw new \Exception(lex('error_giftcard_notfound'), 404);
        }
    }

    /**
     * @param $code
     * @return GiftCard|null
     */
    function getByCode($code)
    {
        return GiftCard::where('code', $code)->first();
    }
}