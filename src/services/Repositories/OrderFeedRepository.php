<?php


namespace services\Repositories;


use Carbon\Carbon;
use Illuminate\Console\Command;
use League\Csv\Writer;
use Order;
use Illuminate\Database\Query\Builder;
use OrderState;
use services\Morellato\Gls\Xml\Reader\CarrierShipmentXmlReader;
use services\Morellato\Gls\Xml\Reader\ShipmentXmlReader;

class OrderFeedRepository
{

    protected $console = null;

    /**
     * @return null|Command
     */
    protected function getConsole()
    {
        return $this->console;
    }

    /**
     * @param Command $console
     */
    public function setConsole(Command $console)
    {
        $this->console = $console;
    }

    /**
     * @param Builder $query
     * @return string
     */
    public function bestShops($query)
    {
        $console = $this->getConsole();
        /** @var Order[] $orders */
        $orders = $query->where('availability_mode', 'shop')
            ->orderBy('id', 'desc')
            //->take(20)
            ->get();

        $header = ['Data', 'N_Ordine', 'ID_Ordine', 'Status_Ordine', 'Status_Pagamento', 'Importo', 'Url', 'Negozio_Giacenza', 'Negozio_Ritiro', 'Esito_Controllo'];

        $many = count($orders);
        $counter = 0;
        $export_rows = [$header];
        foreach ($orders as $order) {
            $counter++;
            $order_shop = $order->availability_shop_id;
            $order_shop_delivery = $order->getDeliveryStoreEntityCode();
            if ($console)
                $console->line("Order #$order->id [$order->reference] ($counter/$many) with shop => $order_shop");
            $products = $order->getProducts(true, 'NG');
            $total_products = count($products);
            $found = 0;
            foreach ($products as $product) {
                $product_shop = $product->availability_shop_id;
                $row_shops = explode(',', $product->availability_solvable_shops);
                if (in_array($product_shop, $row_shops)) {
                    $found++;
                }
            }
            $export_row = [
                'Data' => $order->created_at->format('Y-m-d'),
                'N_Ordine' => $order->reference,
                'ID_Ordine' => $order->id,
                'Status_Ordine' => $order->statusName(),
                'Status_Pagamento' => $order->paymentStatusName(),
                'Importo' => $order->total_order,
                'Url' => $order->getBackendLinkAttribute(),
                'Negozio_Giacenza' => $order_shop,
                'Negozio_Ritiro' => $order_shop_delivery,
                'Esito_Controllo' => $total_products === $found ? 'OK' : 'KO',
            ];
            $export_rows[] = $export_row;
        }

        $platform = config('app.project');

        $sourcePath = storage_path('morellato/csv/export');
        $filename = $sourcePath . "/{$platform}_report_orders_best_shops_" . date('Ymd') . '.csv';
        if (file_exists($filename))
            unlink($filename);

        touch($filename);

        if ($console)
            $console->line("Exporting into CSV...");

        //we create the CSV into memory
        $csv = Writer::createFromPath($filename);
        $csv->setDelimiter(';');
        $csv->setEncodingFrom('UTF-8');


        foreach ($export_rows as $row) {
            $csv->insertOne($row);
        }

        if ($console)
            $console->info("File is available at: $filename");

        return $filename;
    }


    /**
     * @param Builder $query
     * @return string
     */
    public function working($query)
    {
        $console = $this->getConsole();
        /** @var Order[] $orders */
        $orders = $query->withStatus([OrderState::STATUS_WORKING])->withHistoryStates([OrderState::STATUS_WORKING], '2019-12-03')
            ->orderBy('id', 'desc')
            //->take(20)
            ->get();

        $pivot_date = Carbon::parse('2019-12-04');

        /** @var ShipmentXmlReader $shipmentXmlReader */
        $shipmentXmlReader = \App::make(ShipmentXmlReader::class);
        /** @var CarrierShipmentXmlReader $carrierShipmentXmlReader */
        $carrierShipmentXmlReader = \App::make(CarrierShipmentXmlReader::class);

        $header = [
            'Data',
            'N_Ordine',
            'ID_Ordine',
            'Status_Ordine',
            'Status_Pagamento',
            'Importo',
            'Url',
            'Negozio_Giacenza',
            'Negozio_Ritiro',
            'Penultimo_Status_WS2',
            'Ultimo_Status_WS2',
            'Tracciamento_XML_GLS',
            'GLS_Modalita',
            'GLS_Numero_Spedizione',
            'GLS_Ritiro_Corriere',
        ];

        $many = count($orders);
        $counter = 0;
        $export_rows = [$header];
        foreach ($orders as $order) {
            $counter++;
            $status = $order->getLastHistoryStatus('O');
            $last_status = $status ? (int)$status->status_id : 0;
            $last_date = ($status) ? Carbon::parse($status->created_at) : null;
            if ($last_status === OrderState::STATUS_WORKING and $last_date and $last_date->lt($pivot_date)):
                $order_shop = $order->availability_shop_id;
                $order_shop_delivery = $order->getDeliveryStoreEntityCode();

                if ($console)
                    $console->line("Order #$order->id [$order->reference] ($counter/$many) with shop => $order_shop");

                $slip_details = $order->order_slip_details()->orderBy('created_at', 'desc')->get();
                $total_slip_details = count($slip_details);
                $last_slip_detail = ($total_slip_details > 0) ? $slip_details[0] : null;
                $before_last_slip_detail = ($total_slip_details > 1) ? $slip_details[1] : null;

                $glsTrackingMode = $order->getGlsTrackingMode();
                $shipping_number = $order->getGlsShippingNumber();
                $carrier_shipping = $order->getGlsCarrierShipping();

                $xml_uri = null;

                if ($glsTrackingMode === 'numrit') {
                    if (strlen($carrier_shipping) >= 6) {
                        $carrierShipmentXmlReader->setModeByOrder($order)->setCarrierShipment($carrier_shipping, false);
                        $xml_uri = $carrierShipmentXmlReader->getGlsXmlUrl();
                    }
                } else {
                    if (strlen($carrier_shipping) >= 6) {
                        $shipmentXmlReader->setModeByOrder($order)->setBda($shipping_number, false);
                        $xml_uri = $shipmentXmlReader->getGlsXmlUrl();
                    }
                }

                $export_row = [
                    'Data' => $order->created_at->format('Y-m-d'),
                    'N_Ordine' => $order->reference,
                    'ID_Ordine' => $order->id,
                    'Status_Ordine' => $order->statusName(),
                    'Status_Pagamento' => $order->paymentStatusName(),
                    'Importo' => $order->total_order,
                    'Url' => $order->getBackendLinkAttribute(),
                    'Negozio_Giacenza' => $order_shop,
                    'Negozio_Ritiro' => $order_shop_delivery,
                    'Penultimo_Status_WS2' => $before_last_slip_detail ? $before_last_slip_detail->type . "({$before_last_slip_detail->lgnum})" : '',
                    'Ultimo_Status_WS2' => $last_slip_detail ? $last_slip_detail->type . "({$last_slip_detail->lgnum})" : '',
                    'Tracciamento_XML_GLS' => $xml_uri,
                    'GLS_Modalita' => $glsTrackingMode,
                    'GLS_Numero_Spedizione' => $shipping_number,
                    'GLS_Ritiro_Corriere' => $carrier_shipping,
                ];
                $export_rows[] = $export_row;
            endif;
        }

        $platform = config('app.project');

        $sourcePath = storage_path('morellato/csv/export');
        $filename = $sourcePath . "/{$platform}_report_orders_working_30_nov_" . date('Ymd') . '.csv';
        if (file_exists($filename))
            unlink($filename);

        touch($filename);

        if ($console)
            $console->line("Exporting into CSV...");

        //we create the CSV into memory
        $csv = Writer::createFromPath($filename);
        $csv->setDelimiter(';');
        $csv->setEncodingFrom('UTF-8');


        foreach ($export_rows as $row) {
            $csv->insertOne($row);
        }

        if ($console)
            $console->info("File is available at: $filename");

        return $filename;
    }

    /**
     * @param Builder $query
     * @return string
     */
    public function workingWithoutSlips($query)
    {
        $console = $this->getConsole();
        /** @var Order[] $orders */
        $orders = $query->withStatus([OrderState::STATUS_WORKING])
            ->orderBy('id', 'desc')
            //->take(20)
            ->get();

        $header = [
            'Data',
            'N_Ordine',
            'ID_Ordine',
            'Status_Ordine',
            'Status_Pagamento',
            'Importo',
            'Url',
            'Master',
            'Tipo_Ordine',
            'SAP_Sales_ID',
            'SAP_Delivery_ID',
            'SAP_Withdrawal_ID',
        ];

        $many = count($orders);
        $counter = 0;
        $export_rows = [$header];
        foreach ($orders as $order) {
            $counter++;

            if ($console)
                $console->line("Order #$order->id [$order->reference] ($counter/$many)");

            $slip = $order->getMorellatoSlip();

            $sales_id = ($slip) ? $slip->sales_id : '';
            $delivery_id = ($slip) ? $slip->delivery_id : '';
            $withdrawal_id = ($slip) ? $slip->withdrawal_id : '';

            if ($sales_id === '' or $delivery_id === ''):

                $export_row = [
                    'Data' => $order->created_at->format('Y-m-d'),
                    'N_Ordine' => $order->reference,
                    'ID_Ordine' => $order->id,
                    'Status_Ordine' => $order->statusName(),
                    'Status_Pagamento' => $order->paymentStatusName(),
                    'Importo' => $order->total_order,
                    'Url' => $order->getBackendLinkAttribute(),
                    'Master' => $order->isMaster() ? 'SI' : 'NO',
                    'Tipo_Ordine' => $order->availability_mode,
                    'SAP_Sales_ID' => $sales_id,
                    'SAP_Delivery_ID' => $delivery_id,
                    'SAP_Withdrawal_ID' => $withdrawal_id,
                ];
                $export_rows[] = $export_row;

            endif;
        }

        $platform = config('app.project');

        $sourcePath = storage_path('morellato/csv/export');
        $filename = $sourcePath . "/{$platform}_orders_working_without_slips_" . date('Ymd') . '.csv';
        if (file_exists($filename))
            unlink($filename);

        touch($filename);

        if ($console)
            $console->line("Exporting into CSV...");

        //we create the CSV into memory
        $csv = Writer::createFromPath($filename);
        $csv->setDelimiter(';');
        $csv->setEncodingFrom('UTF-8');


        foreach ($export_rows as $row) {
            $csv->insertOne($row);
        }

        if ($console)
            $console->info("File is available at: $filename");

        return $filename;
    }

    /**
     * @param Builder $query
     * @return string
     */
    public function masterOrdersAutoStatus($query)
    {
        $console = $this->getConsole();
        /** @var Order[] $orders */
        $orders = $query->withAvailabilityModes('master')
            ->orderBy('id', 'desc')
            //->take(20)
            ->get();

        $header = [
            'Data',
            'N_Ordine',
            'ID_Ordine',
            'Status_Ordine',
            'Status_Pagamento',
            'Importo',
            'Url',
            'Nuovo_Stato',
        ];

        $export_rows = [$header];

        foreach ($orders as $order) {
            $childrens = $order->getChildren();
            $new_status = null;

            $matrix = [
                OrderState::STATUS_NEW => 0,
                OrderState::STATUS_WORKING => 0,
                OrderState::STATUS_SHIPPED => 0,
                OrderState::STATUS_READY => 0,
                OrderState::STATUS_CLOSED => 0,
            ];

            foreach ($childrens as $children) {
                if (isset($matrix[$children->status])) {
                    $matrix[$children->status]++;
                }
            }

            // Tutti i figli chiusi: status master chiuso
            if ($matrix[OrderState::STATUS_CLOSED] === count($childrens)) {
                $new_status = OrderState::STATUS_CLOSED;
            } elseif ($matrix[OrderState::STATUS_READY] > 0) {
                $new_status = OrderState::STATUS_READY;
            } elseif ($matrix[OrderState::STATUS_SHIPPED] > 0) {
                $new_status = OrderState::STATUS_SHIPPED;
            } elseif ($matrix[OrderState::STATUS_WORKING] > 0) {
                $new_status = OrderState::STATUS_WORKING;
            } elseif ($matrix[OrderState::STATUS_NEW] > 0) {
                $new_status = OrderState::STATUS_NEW;
            }

            if ($new_status and $new_status !== (int)$order->status) {
                if ($console)
                    $console->info("Order #{$order->id} should be migrated to status [$new_status]");

                $export_row = [
                    'Data' => $order->created_at->format('Y-m-d'),
                    'N_Ordine' => $order->reference,
                    'ID_Ordine' => $order->id,
                    'Status_Ordine' => $order->statusName(),
                    'Status_Pagamento' => $order->paymentStatusName(),
                    'Importo' => $order->total_order,
                    'Url' => $order->getBackendLinkAttribute(),
                    'Nuovo_Stato' => OrderState::getObj($new_status)->name
                ];
                $export_rows[] = $export_row;
            }
        }

        $platform = config('app.project');

        $sourcePath = storage_path('morellato/csv/export');
        $filename = $sourcePath . "/{$platform}_orders_masters_assign_status_" . date('Ymd') . '.csv';
        if (file_exists($filename))
            unlink($filename);

        touch($filename);

        if ($console)
            $console->line("Exporting into CSV...");

        //we create the CSV into memory
        $csv = Writer::createFromPath($filename);
        $csv->setDelimiter(';');
        $csv->setEncodingFrom('UTF-8');


        foreach ($export_rows as $row) {
            $csv->insertOne($row);
        }

        if ($console)
            $console->info("File is available at: $filename");

        return $filename;
    }


    /**
     * @param Builder $query
     * @return string
     */
    public function gls($query)
    {
        $console = $this->getConsole();
        /** @var Order[] $orders */
        $orders = $query->orderBy('id', 'desc')
            //->take(20)
            ->get();

        $header = [
            'Data',
            'N_Ordine',
            'ID_Ordine',
            'Status_Ordine',
            'Status_Pagamento',
            'Importo',
            'Url',
            'Master',
            'Tipo_Ordine',
            'GLS_Modalita',
            'GLS_Numero_Spedizione',
            'GLS_Ritiro_Corriere',
            'GLS_Response',
        ];

        $export_rows = [$header];

        $many = count($orders);
        $counter = 0;
        foreach ($orders as $order) {
            $counter++;

            if ($console)
                $console->line("Order #$order->id [$order->reference] ($counter/$many)");

            $glsTrackingMode = $order->getGlsTrackingMode();
            $shipping_number = $order->getGlsShippingNumber();
            $carrier_shipping = $order->getGlsCarrierShipping();
            $last_tracking = $order->getGlsLatestTrackingResult();


            $export_row = [
                'Data' => $order->created_at->format('Y-m-d'),
                'N_Ordine' => $order->reference,
                'ID_Ordine' => $order->id,
                'Status_Ordine' => $order->statusName(),
                'Status_Pagamento' => $order->paymentStatusName(),
                'Importo' => $order->total_order,
                'Url' => $order->getBackendLinkAttribute(),
                'Master' => $order->isMaster() ? 'SI' : 'NO',
                'Tipo_Ordine' => $order->availability_mode,
                'GLS_Modalita' => $glsTrackingMode,
                'GLS_Numero_Spedizione' => $shipping_number,
                'GLS_Ritiro_Corriere' => $carrier_shipping,
                'GLS_Response' => $last_tracking ? $last_tracking : null,
            ];
            $export_rows[] = $export_row;
        }

        $platform = config('app.project');

        $sourcePath = storage_path('morellato/csv/export');
        $filename = $sourcePath . "/{$platform}_export_orders_for_gls_" . date('Ymd') . '.csv';
        if (file_exists($filename))
            unlink($filename);

        touch($filename);

        if ($console)
            $console->line("Exporting into CSV...");

        //we create the CSV into memory
        $csv = Writer::createFromPath($filename);
        $csv->setDelimiter(';');
        $csv->setEncodingFrom('UTF-8');

        foreach ($export_rows as $row) {
            $csv->insertOne($row);
        }

        if ($console)
            $console->info("File is available at: $filename");

        return $filename;
    }
}