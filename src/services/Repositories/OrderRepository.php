<?php

namespace services\Repositories;


use Order;
use services\Bluespirit\Negoziando\Fidelity\FidelityOrderRepository;
use services\Bluespirit\Negoziando\Fidelity\FidelityRepository;
use services\Fees\Repositories\FeesOrderRepository;
use services\Membership\Repositories\AffiliateRepository;
use services\Bluespirit\Negoziando\Repositories\NegoziandoOrderRepository;
use OrderState;
use PaymentState;

class OrderRepository extends Repository
{
    /**
     * @var Order;
     */
    protected $order;

    /**
     * @var FidelityRepository
     */
    protected $fidelityRepository;

    /**
     * @var GiftCardRepository
     */
    protected $giftCardRepository;

    /**
     * @var ConfiguratorRepository
     */
    protected $configuratorRepository;

    /**
     * @var AffiliateRepository
     */
    protected $affiliateRepository;

    /**
     * @var FeesOrderRepository
     */
    protected $feesRepository;

    /**
     * @var NegoziandoOrderRepository
     */
    protected $negoziandoOrderRepository;

    /**
     * @var bool
     */
    protected $fidelity_enabled = false;

    /**
     * @var bool
     */
    protected $builder_enabled = false;

    /**
     * @var bool
     */
    protected $giftcard_enabled = false;

    /**
     * @var bool
     */
    protected $membership_enabled = false;

    /**
     * @var bool
     */
    protected $fees_enabled = false;

    /**
     * @var bool
     */
    protected $negoziando_enabled = false;


    function __construct(
        FidelityOrderRepository $fidelityRepository,
        GiftCardRepository $giftCardRepository,
        ConfiguratorRepository $configuratorRepository,
        AffiliateRepository $affiliateRepository,
        FeesOrderRepository $feesRepository,
        NegoziandoOrderRepository $negoziandoOrderRepository
    )
    {
        $this->fidelityRepository = $fidelityRepository;
        $this->giftCardRepository = $giftCardRepository;
        $this->configuratorRepository = $configuratorRepository;
        $this->affiliateRepository = $affiliateRepository;
        $this->feesRepository = $feesRepository;
        $this->negoziandoOrderRepository = $negoziandoOrderRepository;
        $this->fidelity_enabled = config('negoziando.fidelity_enabled', false);
        $this->giftcard_enabled = config('negoziando.giftcard.enabled', false);
        $this->builder_enabled = config('builder.enabled', false);
        $this->membership_enabled = config('membership.enabled', false);
        $this->fees_enabled = feats()->bladeReceipts();
        $this->negoziando_enabled = feats()->switchEnabled(\Core\Cfg::SERVICE_FIDELITY_CARD_REGISTER);
    }

    /**
     * @param Order $order
     * @return $this
     */
    function setOrder(Order $order)
    {
        $this->order = $order;
        return $this;
    }

    /**
     * @return Order
     */
    function getOrder()
    {
        return $this->order;
    }

    /**
     * Place order event
     */
    public function orderPlaced()
    {
        if ($this->negoziando_enabled) {
            audit('negoziandoOrderRepository.orderPlaced', __METHOD__);
            $this->negoziandoOrderRepository->orderPlaced($this->getOrder());
        }
        if ($this->fidelity_enabled) {
            audit('fidelityRepository.orderPlaced', __METHOD__);
            $this->fidelityRepository->orderPlaced($this->getOrder());
        }
        if ($this->giftcard_enabled) {
            audit('giftCardRepository.orderPlaced', __METHOD__);
            $this->giftCardRepository->orderPlaced($this->getOrder());
        }
        if ($this->builder_enabled) {
            audit('configuratorRepository.orderPlaced', __METHOD__);
            $this->configuratorRepository->orderPlaced($this->getOrder());
        }
        if ($this->membership_enabled) {
            audit('affiliateRepository.orderPlaced', __METHOD__);
            $this->affiliateRepository->orderPlaced($this->getOrder());
        }
        if ($this->fees_enabled) {
            audit('fees.orderPlaced', __METHOD__);
            $this->feesRepository->orderPlaced($this->getOrder());
        }
    }


    /**
     * Order status change
     * @param $status
     * @param $old_status
     */
    public function orderStatusChanged($status, $old_status)
    {
        if ($this->negoziando_enabled) {
            audit('negoziandoOrderRepository.orderStatusChanged', __METHOD__);
            $this->negoziandoOrderRepository->orderStatusChanged($this->getOrder(), $status);
        }
        if ($this->fidelity_enabled) {
            audit('fidelity.orderStatusChanged', __METHOD__);
            $this->fidelityRepository->orderStatusChanged($this->getOrder(), $status);
        }
        if ($this->membership_enabled) {
            audit('affiliate.orderStatusChanged', __METHOD__);
            $this->affiliateRepository->orderStatusChanged($this->getOrder(), $status);
        }
        if ($this->fees_enabled) {
            audit('fees.orderStatusChanged', __METHOD__);
            $this->feesRepository->orderStatusChanged($this->getOrder(), $status);
        }
    }

    /**
     * Order payment status change
     * @param $status
     * @param $old_status
     */
    public function orderPaymentStatusChanged($status, $old_status)
    {
        if ($this->negoziando_enabled) {
            audit('negoziandoOrderRepository.orderPaymentStatusChanged', __METHOD__);
            $this->negoziandoOrderRepository->orderPaymentStatusChanged($this->getOrder(), $status);
        }
        if ($this->fees_enabled) {
            audit('fees.orderPaymentStatusChanged', __METHOD__);
            $this->feesRepository->orderPaymentStatusChanged($this->getOrder(), $status);
        }
    }

    /**
     * @param $id
     * @return OrderState|null
     */
    protected function getOrderState($id)
    {
        return OrderState::getObj($id);
    }

    /**
     * @param $id
     * @return PaymentState|null
     */
    protected function getOrderPaymentState($id)
    {
        return PaymentState::getObj($id);
    }


}