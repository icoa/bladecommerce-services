<?php

namespace services\Repositories;

use DB;
use services\Models\BuilderPayload;
use services\Transformers\BuilderAttributeTransformer;
use services\Transformers\BuilderBaseItemTransformer;
use services\Transformers\BuilderBaseTransformer;
use services\Transformers\BuilderCategoryTransformer;
use services\Transformers\BuilderCollectionTransformer;
use services\Transformers\BuilderDropletTransformer;
use services\Transformers\ProductBaseTransformer;
use stdClass;
use Product;
use Attribute;
use AttributeOption;
use Site;
use Collection;
use services\Models\BuilderProduct;
use services\Models\BuilderBlueprint;
use PDF;
use CartManager;
use Order;
use Customer;
use Email;

class ConfiguratorRepository extends Repository
{

    private $attribute_id;

    protected $debug = false;

    function __construct()
    {
        $this->attribute_id = config('builder.family_attribute_id');
    }

    function getFamilies($collection_id)
    {
        //http://morellatoweb.local/rest/configurator/families/248
        //http://morellatoweb.local/rest/configurator/families/2981
        $products_ids = Product::withCollections($collection_id)->onlyDroplets()->lists('id');
        $rows = AttributeOption::rows()
            ->whereIn('id', function ($query) use ($products_ids) {
                $query->select('attribute_val')->from('products_attributes')
                    ->where('attribute_id', $this->attribute_id)
                    ->whereIn('product_id', $products_ids);
            })->orderBy('position')
            ->get();
        return $rows;
    }

    function apiFamilies($collection_id)
    {
        $items = $this->getFamilies($collection_id);
        return fractal()->collection($items, new BuilderAttributeTransformer());
    }


    function getDroplets($collection_id, $family_id)
    {
        //http://morellatoweb.local/rest/configurator/droplets/248/4940
        //http://morellatoweb.local/rest/configurator/droplets/2981/4941
        $items = Product::rows()->withCollections($collection_id)
            ->withAttributeOptions($family_id)
            ->onlyDroplets()
            ->with('builder_product')
            ->orderBy('position')
            ->get();

        foreach ($items as $key => $item) {
            $dragPath = public_path("/assets/products/builder/$item->sku.png");
            if (!file_exists($dragPath)) {
                unset($items[$key]);
            }
        }

        return $items;
    }

    function apiDroplets($collection_id, $family_id)
    {
        $items = $this->getDroplets($collection_id, $family_id);
        return fractal()->collection($items, new BuilderDropletTransformer());
    }


    function getCollections()
    {
        $collections = config('builder.collections');
        $rows = Collection::rows()->whereIn('id', $collections)->orderBy('position')->get();
        return $rows;
    }

    function apiCollections()
    {
        $items = $this->getCollections();
        return fractal()->collection($items, new BuilderCollectionTransformer());
    }


    function getCategories($collection_id)
    {
        $rows = Product::rows()->onlyBases()
            ->withCollections($collection_id)
            ->groupBy('default_category_id')
            ->orderBy('main_category_id')
            ->orderBy('position')
            ->get();
        return $rows;
    }

    function apiCategories($collection_id)
    {
        $items = $this->getCategories($collection_id);
        return fractal()->collection($items, new BuilderCategoryTransformer());
    }


    function getBases($collection_id, $category_id)
    {
        $rows = Product::rows()
            ->withCollections($collection_id)
            ->withCategories($category_id)
            ->onlyBases()
            ->orderBy('position')
            ->get();
        return $rows;
    }

    function apiBases($collection_id, $category_id)
    {
        $items = $this->getBases($collection_id, $category_id);
        return fractal()->collection($items, new BuilderBaseTransformer());
    }

    function getBase($base_id)
    {
        audit_watch();
        $row = Product::getObj($base_id);
        $row->load('builder_product', 'builder_product.blueprint');
        if ($row) {
            return $row;
        }
        return null;
    }

    function apiBase($base_id)
    {
        $item = $this->getBase($base_id);
        return fractal()->item($item, new BuilderBaseItemTransformer());
    }

    function save($data)
    {
        audit($data, __METHOD__);
        try {
            $model = BuilderPayload::where('secure_key', $data['secure_key'])->first();
            if (is_null($model)) {
                $model = new BuilderPayload();
            }
            $pdf = $data['pdf'];
            unset($data['pdf']);
            $model->fill($data);
            $model->save();
            if ($pdf and $pdf == true) {
                $model->makePdf();
            } else {
                $model->makeImage();
            }
            return $model->getFileData();
        } catch (\Exception $e) {
            audit($e->getMessage());
        }
        return false;
    }


    function apiPreset($secure_key)
    {
        $model = BuilderPayload::where('secure_key', $secure_key)->first();
        $items = [];
        if ($model) {
            $items = $model->getParam('items');
            foreach ($items as $key => $item) {
                $fractal = (new BuilderDropletTransformer())->transform(Product::getPublicObj($item['product_id']));
                $items[$key]['model'] = $fractal;
            }
        }
        return $items;
    }


    function addToCart($secure_key)
    {
        $success = false;
        $model = BuilderPayload::where('secure_key', $secure_key)->first();
        if ($model) {
            try {
                $product = Product::getFullProduct($model->product_id);
                $product->cart_params = ['builder' => $secure_key, 'base' => true];
                CartManager::add($product, 1);
                audit($product->sku, 'ADDED PRODUCT TO CART');
                $items = $model->getParam('items');
                audit($items, 'BUILDER ITEMS');
                foreach ($items as $key => $item) {
                    $product = Product::getFullProduct($item['product_id']);
                    $product->cart_params = ['builder' => $secure_key, 'base' => false];
                    CartManager::add($product, 1);
                    audit($product->sku, 'ADDED ITEM TO CART');
                }
                $success = true;
            } catch (\Exception $e) {
                audit($e->getMessage(), __METHOD__);
                audit($e->getTraceAsString(), __METHOD__);
            }

        }
        return $success;
    }


    /**
     * @param Order $order
     */
    function orderPlaced(Order $order)
    {
        if ($this->debug)
            audit($order->id, __METHOD__ . ' - ORDER ID');


        if ($order->hasBuilderProducts()) {
            audit($order->id, __METHOD__ . ' - HAS BUILDER PRODUCTS');
            $attachments = [];
            //get all builder base products
            $bases = $order->getBuilderProductsWithPayload();
            //for each base, check if the PDF has been created, otherwise force PDF creation
            foreach ($bases as $base) {
                if ($base->payload) {
                    $payloadData = $base->payload->getFileData();
                    $attachments[] = $payloadData['filepath'];
                    if ($payloadData['hasFile'] == false) {
                        $base->payload->makePdf();
                    }
                }
            }
            if ($this->debug)
                audit($attachments, 'CONFIGURATOR_ATTACHMENTS');
            //..send an email to the customer, with a PDF attachment
            if (!empty($attachments))
                $this->sendEmailToCustomer($order, $attachments);
        }
    }


    /**
     * @param Order $order
     * @param array $files
     */
    protected function sendEmailToCustomer(Order $order, $files = [])
    {
        $customer = $order->getCustomer();
        $attachments = [];

        $counter = 1;
        foreach ($files as $file) {
            $name = $order->reference . '_BUILD' . $counter . '.pdf';
            $attachments[] = ['file' => $file, 'as' => $name];
            $counter++;
        }

        $email = Email::getByCode('CONFIGURATOR', $order->lang_id);
        $data = [
            'NAME' => $customer->name,
            'ORDER' => $order->reference,
        ];

        if ($this->debug)
            audit($data, 'EMAIL_DATA');

        if ($this->debug)
            audit($attachments, 'EMAIL_ATTACHMENTS');

        if ($this->debug) {
            \Config::set('mail.pretend', false);
        }

        $email->setCustomer($customer)->send($data, $customer->email, $attachments);
    }

}