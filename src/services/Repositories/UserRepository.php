<?php
/**
 * Created by PhpStorm.
 * User: Fabio
 * Date: 20/11/2017
 * Time: 17:06
 */

namespace services\Repositories;

use CustomerGroup;
use Exception;
use Country;
use Customer;
use Newsletter;
use Address;
use Illuminate\Support\Str;
use Input;
use Format;
use Core;
use Hash;
use FrontTpl;
use services\Remote\Repositories\RemoteRepository;
use services\Strap\Repositories\StrapUserRepository;
use Session;
use FrontUser;
use Carbon\Carbon;
use services\Bluespirit\Negoziando\Fidelity\FidelityCustomerRepository;
use services\IsoCodes\ZipCode;

class UserRepository extends Repository
{
    /**
     * @var RemoteRepository
     */
    protected $remoteRepository;

    /**
     * @var StrapUserRepository
     */
    protected $strapUserRepository;


    function __construct(RemoteRepository $remoteRepository, StrapUserRepository $strapUserRepository)
    {
        $this->remoteRepository = $remoteRepository;
        $this->strapUserRepository = $strapUserRepository;
    }

    /**
     * @param $email
     * @return bool
     */
    protected function isEmailUnique($email, $id = null)
    {
        $builder = Customer::whereEmail($email)->where("active", 1);
        if ($id) {
            $builder->where('id', '!=', $id);
        }
        $customer = $builder->first();
        return is_null($customer);
    }

    /**
     * @return array
     */
    protected function getRequiredRegisterData()
    {

        $data = Input::all();
        $profile = Input::get('profile');
        $marketing = Input::get('marketing');

        $address_id = Input::get('address_id', -1);
        $customer_id = Input::get('id', 0);

        //audit($data, __METHOD__);

        $errors = [];

        if ($profile == null) {
            $errors[] = trans("ajax.personal_consent_data");
        }
        if ($marketing == null) {
            $errors[] = trans("ajax.personal_consent_marketing");
        }

        if (!\Utils::isEmail($data['email'])) {
            $errors[] = trans("ajax.email_must_valid");
        }

        $required = [
            'email' => trans("ajax.email_address"),
            'passwd' => trans("ajax.password"),
            'confirm_passwd' => trans("ajax.password_confirm"),
        ];

        //update profile action
        if ($customer_id > 0) {
            $changePasswd = (isset($data['passwd']) AND $data['passwd'] != '');
            if ($changePasswd == false) {
                //if the password has not changed, then is not required
                unset($required['passwd']);
                unset($required['confirm_passwd']);
            }
        }

        if (isset($data['people_id']) and $data['people_id'] == '1') {
            $required['firstname'] = trans("ajax.firstname");
            $required['lastname'] = trans("ajax.lastname");
            $required['day'] = trans("ajax.birth_day");
            $required['month'] = trans("ajax.birth_month");
            $required['year'] = trans("ajax.birth_year");
        } else {
            $required['company'] = trans("ajax.company");
        }

        if ($address_id != -1) {
            $required += [
                'address1' => trans("ajax.address"),
                'address2' => trans("ajax.address2"),
                'postcode' => trans("ajax.postcode"),
                'city' => trans("ajax.city"),
                'country_id' => trans("ajax.country"),
                'state_id' => trans("ajax.state"),
                'phone' => trans("ajax.phone"),
            ];

            if ($data['people_id'] == '1') {
                if (feats()->noReceipts()) {
                    $required['cf'] = trans('ajax.cf');
                }
            } else {
                $required['vat_number'] = trans("ajax.vat");
            }
        }

        if (isset($data['country_id'])) {
            $country = Country::getObj($data['country_id']);
            if ($country AND $country->hasStates() == false) {
                unset($required['state_id']);
            }
            if ($country and $country->iso_code) {
                $valid = ZipCode::validate($data['postcode'], $country->iso_code);
                if ($valid === false) {
                    $errors[] = trans("validation.zip", ['attribute' => trans("ajax.postcode")]);
                }
            }
            if (is_null($country)) {
                if (isset($required['country_id']))
                    unset($required['country_id']);

                if (isset($required['state_id']))
                    unset($required['state_id']);
            }
        }

        foreach ($required as $key => $field) {
            if (trim($data[$key]) == '') {
                $errors[] = trans("ajax.mandatory_field", ['field' => $field]);
            }
        }

        if (isset($required['passwd']) and isset($required['confirm_passwd'])) {
            if (Str::length($data['passwd']) < 6) {
                $errors[] = trans("ajax.password_6");
            }
            if (Str::length($data['passwd']) >= 6 AND $data['passwd'] != $data['confirm_passwd']) {
                $errors[] = trans("ajax.password_mismatch");
            }
        }

        $email = Str::lower(trim($data['email']));
        $email_length = Str::length($email);

        if ($email_length > 0 and $customer_id <= 0) {
            if (!$this->isEmailUnique($email)) {
                $errors[] = trans("ajax.email_used");
            }
        }

        if ($email_length > 0 and $customer_id > 0) {
            if (!$this->isEmailUnique($email, $customer_id)) {
                $errors[] = trans("ajax.email_used");
            }
        }

        $hasDate = ($data['year'] > 0 and $data['month'] > 0 and $data['day'] > 0);
        if (config('auth.customer.avoid_minors', false) === true and $hasDate and $data['people_id'] == '1') {
            try {
                $date = Carbon::create($data['year'], $data['month'], $data['day']);
                if ($date->diffInYears() < 18) {
                    $errors[] = trans("ajax.registration_minor");
                }
            } catch (Exception $e) {
                $errors[] = trans("ajax.registration_minor");
            }
        }

        if (feats()->enabled('recaptcha') and count($errors) == 0) {
            $response = Input::get('g-recaptcha-response');
            $validRecaptcha = \Recaptcha::verifyResponse($response, 'register');
            if ($validRecaptcha == false) {
                $errors[] = trans("ajax.recaptcha");
            }
        }

        return $errors;
    }


    /**
     * @param string $action
     * @param null $customer
     * @return array
     */
    protected function getCustomerData($action = 'register', $customer = null)
    {
        $data = Input::all();
        audit($data, __METHOD__);

        $marketing = $newsletter = Input::get('marketing', 0);
        $profile = Input::get('profile', 0);

        $email = Str::lower(trim($data['email']));
        $birthday = null;

        if ($data['year'] > 0 and $data['month'] > 0 and $data['day'] > 0) {
            $date = Carbon::create($data['year'], $data['month'], $data['day']);
            $birthday = $date->format('Y-m-d');
        }

        $secure_key = \Format::secure_key();

        $customer_data = [
            'firstname' => ucfirst($data['firstname']),
            'lastname' => ucfirst($data['lastname']),
            'company' => ucfirst($data['company']),
            'gender_id' => ($data['gender_id']),
            'people_id' => ($data['people_id']),
            'birthday' => $birthday,
            'email' => $email,
            'newsletter' => $newsletter,
            'marketing' => $marketing,
            'profile' => $profile,
        ];

        $customer_data['name'] = $customer_data['people_id'] == 1 ? $customer_data['firstname'] . " " . $customer_data['lastname'] : $customer_data['company'];

        if (isset($data['shop_id'])) {
            $customer_data['shop_id'] = $data['shop_id'];
        }

        if (isset($data['siret'])) {
            $customer_data['siret'] = $data['siret'];
        }

        if ($action == 'register') {
            $customer_data['default_group_id'] = (isset($data['default_group_id']) and $data['default_group_id'] > 0) ? $data['default_group_id'] : 3;
            $customer_data['alt_group_id'] = (isset($data['alt_group_id']) and $data['alt_group_id'] > 0) ? $data['alt_group_id'] : null;
            $customer_data['passwd'] = Hash::make(trim($data['passwd']));
            $customer_data['secure_key'] = $secure_key;
            $customer_data['lang_id'] = FrontTpl::getLang();
        } else {
            $changePasswd = (isset($data['passwd']) AND $data['passwd'] != '');

            if ($changePasswd) {
                $customer_data['passwd'] = Hash::make(trim($data['passwd']));
                $customer_data['last_passwd_gen'] = \Format::now();
            }

            //customer is becoming registered
            if ($customer and $customer->guest == 1) {
                $customer_data += [
                    'active' => 1,
                    'guest' => 0,
                    'default_group_id' => 3,
                    'passwd' => Hash::make(trim($data['passwd'])),
                    'last_passwd_gen' => Format::now(),
                    'secure_key' => $secure_key,
                ];
            }
        }

        return $customer_data;
    }


    /**
     * @param null $customer
     * @return array
     */
    protected function getCustomerAddressData($customer = null)
    {

        $data = Input::all();

        $customer_data = $this->getCustomerData();

        if (isset($data['country_id'])) {
            $address_data = [
                'company' => $customer_data['company'],
                'firstname' => $customer_data['firstname'],
                'lastname' => $customer_data['lastname'],
                'country_id' => (int)$data['country_id'],
                'state_id' => (int)$data['state_id'],
                'address1' => ucfirst($data['address1']),
                'address2' => trim($data['address2']),
                'postcode' => ucfirst($data['postcode']),
                'city' => ucfirst($data['city']),
                'phone' => ($data['phone']),
                'phone_mobile' => ($data['phone_mobile']),
                'vat_number' => isset($data['vat_number']) ? Str::upper($data['vat_number']) : null,
                'cf' => isset($data['cf']) ? Str::upper($data['cf']) : null,
                'extrainfo' => trim($data['extrainfo']),
            ];

            if ($customer) {
                $address_data['customer_id'] = $customer->id;
            }
        } else {
            $address_data = [];
        }

        return $address_data;
    }


    /**
     * @param $customer_id
     * @return Customer|null
     */
    protected function updateCustomer($customer_id)
    {
        $customer = Customer::find($customer_id);
        $customer_data = $this->getCustomerData('update', $customer);
        audit($customer_data, __METHOD__);
        $customer->setAttributes($customer_data);
        $customer->save();
        if ($customer->id > 0) {
            $customer->handleNewsletter();
        }
        return $customer;
    }

    /**
     * @param int $address_id
     * @return Customer
     */
    protected function insertCustomer($address_id = 0)
    {
        //$fidelity_enabled = config('negoziando.fidelity_enabled', false);
        $fidelity_enabled = feats()->switchEnabled(\Core\Cfg::SERVICE_FIDELITY_CARD_REGISTER);
        $customer_data = $this->getCustomerData();

        $customer = new Customer();
        $customer->setAttributes($customer_data);
        audit($customer->toArray(), __METHOD__ . '::insertCustomer');
        $customer->save();
        $send_email = true;
        $address = $this->insertCustomerAddress($address_id, $customer);
        if ($address) {
            $send_email = false;
        }

        if ($customer->id > 0) {
            $customer->handleNewsletter();
        }

        if($customerGroup = $this->getCustomerGroupByGrpCode()){
            $customer->addAltGroup($customerGroup->id);
        }

        if ($fidelity_enabled) {
            $customerRepository = $this->getFidelityCustomerRepository();
            $customer_data['card_code'] = Input::get('card_code');
            $fidelityCustomer = $this->getFidelityCustomer($customer_data['email'], $customer_data['card_code']);
            if ($fidelityCustomer) {
                audit($fidelityCustomer->toArray(), __METHOD__);
                audit($fidelityCustomer->getCardCode(), 'card_code');

                $customerRepository->sync($customer, $fidelityCustomer);

                //throw new Exception('custom stop');
                //return null;
            } else {
                if ($address) {
                    audit('fidelityCustomer not found, create a new one', __METHOD__);
                    $customerRepository->resetParams();
                    $customerRepository->setCustomer($customer);
                    $fidelityCustomer = $customerRepository->insertFidelityCustomer();
                    $customerRepository->setParam('card_code', $fidelityCustomer->getCardCode());
                    $customerRepository->setParam('customer_code', $fidelityCustomer->getCustomerCode());
                    $customerRepository->upsertCustomer($customer, $fidelityCustomer);
                }
                //throw new Exception('stop');
            }

        }

        if ($send_email) {
            $this->sendActivationMail($customer);
        } else {
            //auto-activate the user and auto-login
            audit("auto-activate the user and auto-login", __METHOD__);
            $customer->setAttribute('active', 1);
            $customer->save();
            \Session::put("blade_auth", $customer->id);
            \FrontUser::checkCart();
        }

        return $customer;

    }

    /**
     * @param $address_id
     * @param null $customer
     * @return Address|null
     */
    protected function updateCustomerAddress($address_id, $customer = null)
    {
        $address = null;
        if ($customer and $customer->id > 0 AND $address_id > 0) {
            //save address for customer
            $address_data = $this->getCustomerAddressData($customer);
            if (!empty($address_data)) {
                $address = Address::find($address_id);
                $address->setAttributes($address_data);
                $address->save();
            }
        }
        return $address;
    }

    /**
     * @param $address_id
     * @param null $customer
     * @return Address|null
     */
    protected function insertCustomerAddress($address_id, $customer = null)
    {
        $address = null;
        if ($customer and $customer->id > 0 AND $address_id != -1) {
            //save address for customer
            $address_data = $this->getCustomerAddressData($customer);
            if (!empty($address_data)) {
                $address = new Address();
                $address->setAttributes($address_data);
                $address->save();
            }
        }
        return $address;
    }


    /**
     * @return array
     */
    public function register()
    {

        $customer_id = Input::get('id', 0);
        $address_id = Input::get('address_id', -1);
        $errors = $this->getRequiredRegisterData();

        if (count($errors) == 0) {
            if ($customer_id > 0) {
                //update customer
                try {
                    $customer = $this->updateCustomer($customer_id);
                    $this->updateCustomerAddress($address_id, $customer);
                } catch (Exception $e) {
                    audit($e->getMessage(), "REGISTRATION EXCEPTION");
                    audit_error($e->getMessage(), __METHOD__);
                    audit_error($e->getTraceAsString(), __METHOD__);
                    $errors[] = trans("ajax.registration_error");
                }
            } else {
                //insert customer
                try {
                    $this->insertCustomer($address_id);
                } catch (Exception $e) {
                    audit($e->getMessage(), "REGISTRATION EXCEPTION");
                    audit($e->getTraceAsString());
                    audit_error($e->getMessage(), __METHOD__);
                    audit_error($e->getTraceAsString(), __METHOD__);
                    $errors[] = trans("ajax.registration_error");
                }
            }
        }

        if (count($errors) == 0) {
            $success = true;
            $msg = trans("ajax.registration_ok");
        } else {
            $success = false;
            $msg = implode("<br>", $errors);
        }

        return compact('success', 'msg');
    }


    /**
     * @return array
     */
    public function update()
    {
        $customer_id = Input::get('id', 0);
        $address_id = Input::get('address_id', -1);
        $errors = $this->getRequiredRegisterData();

        if (count($errors) == 0) {
            if ($customer_id > 0) {
                //update customer
                try {
                    $customer = $this->updateCustomer($customer_id);
                    $this->updateCustomerAddress($address_id, $customer);
                } catch (Exception $e) {
                    audit($e->getMessage(), "REGISTRATION EXCEPTION");
                    audit_error($e->getMessage(), __METHOD__);
                    audit_error($e->getTraceAsString(), __METHOD__);
                    $errors[] = trans("ajax.registration_error");
                }
            }
        }

        if (count($errors) == 0) {
            $success = true;
            $msg = trans("ajax.update_ok");
        } else {
            $success = false;
            $msg = implode("<br>", $errors);
        }

        return compact('success', 'msg');
    }


    /**
     * @param $customer
     */
    private function sendActivationMail($customer)
    {
        try {
            $email = $customer->email;
            if (\App::environment() == 'local') {
                $email = 'f.politi@m.icoa.it';
            }
            $activation = \Link::absolute()->to("base", 5) . "?activation=" . $customer->secure_key;
            $name = ($customer->company != '') ? $customer->company : $customer->firstname . " " . $customer->lastname;
            $mail_data = [
                'name' => $name,
                'activation' => $activation,
            ];
            $code = 'USER_REGISTER';
            $mail = \Email::getByCode($code, \Core::getLang());
            $mail->setCustomer($customer)->send($mail_data, $email);
        } catch (Exception $e) {
            \Utils::log($e->getMessage(), "REGISTRATION EXCEPTION");
            audit_error($e->getMessage(), __METHOD__);
            audit_error($e->getTraceAsString(), __METHOD__);
        }
    }

    /**
     * @param null $email
     * @param null $card_code
     * @param null $customer_code
     * @return mixed|null
     */
    protected function checkFidelityCustomer($email = null, $card_code = null, $customer_code = null)
    {
        $response = null;
        //Bluespirit card flow

        if ($customer_code == 'unassigned')
            $customer_code = '';

        if ($card_code != '' or $email != '' or $customer_code != '') {
            $repository = fidelityRepository();

            audit('contacting fidelity repository');
            $response = $repository
                ->setParam('card_code', $card_code)
                ->setParam('email', $email)
                ->setParam('customer_code', $customer_code)
                ->check();

            if (is_null($response)) {
                audit('response is null');
            } else {
                audit($response, 'response');
            }
        }
        return $response;
    }

    /**
     * @param null $email
     * @param null $card_code
     * @param null $customer_code
     * @return null|\services\Bluespirit\Negoziando\Fidelity\FidelityCustomer
     */
    public function getFidelityCustomer($email = null, $card_code = null, $customer_code = null)
    {
        $response = $this->checkFidelityCustomer($email, $card_code, $customer_code);
        if (is_null($response)) {
            return null;
        }
        if (isset($response->customer_code)) {
            //get the fidelity customer
            audit('getting remote fidelity customer => ' . $response->customer_code, __METHOD__);
            //audit_track('getting remote fidelity customer => ' . $response->customer_code, __METHOD__);
            $customer = fidelityCustomer(['customer_code' => $response->customer_code]);
            $customer->load();
            $customer->setCardCode($response->card_code);
            audit($customer->toArray(), __METHOD__);
            //audit_track($customer->toArray(), __METHOD__);
            return $customer;
        }
        return null;
    }

    /**
     * @param $activation
     * @return string
     */
    public function activateUser($activation)
    {
        $customer = Customer::where('secure_key', $activation)->first();
        if ($customer and $customer->id) {
            if ($customer->active == 1) {
                return 'already_active';
            } else {
                $customer->active = 1;
                $customer->save();
                return $this->userActivated($customer);
            }
        }
        return 'unknown';
    }

    /**
     * @return FidelityCustomerRepository
     */
    public function getFidelityCustomerRepository()
    {
        return app(FidelityCustomerRepository::class);
    }


    /**
     * @param Customer $customer
     */
    protected function userActivated(Customer $customer)
    {
        $fidelity_enabled = feats()->switchEnabled(\Core\Cfg::SERVICE_FIDELITY_CARD_REGISTER);
        if ($fidelity_enabled) {
            //audit_track('enter.userActivated');
            //if fidelity is enabled then we must check the existance of the fidelity customer
            //if the fidelity customer exists, then we sync the two entities, otherwise we have to create the fidelity customer from scratch
            $fidelityCustomer = $this->getFidelityCustomer($customer->email);
            $customerRepository = $this->getFidelityCustomerRepository();
            //we store the 'old' card_code variable for the Customer; if the value is 'unassigned', then the Customer has been registered from the Fidelity register page
            $oldCustomerCode = $customer->customer_code;
            if ($fidelityCustomer) {
                //upsert
                //audit_track('UPSERT NOTHING', __METHOD__);
                $card_code = $fidelityCustomer->getCardCode();
                $customer_code = $fidelityCustomer->getCustomerCode();
                //audit_track(compact('card_code', 'customer_code'), __METHOD__);
                if ($card_code != '') {
                    $customer->card_code = $card_code;
                }
                if ($customer_code != '') {
                    $customer->customer_code = $customer_code;
                }
                //audit_track($customer, __METHOD__);
                $customer->save();
            } else {
                //create
                $customerRepository->setCustomer($customer);
                //audit_track('insertFidelityCustomer', __METHOD__);
                $fidelityCustomer = $customerRepository->insertFidelityCustomer();
                $card_code = $fidelityCustomer->getCardCode();
                $customer_code = $fidelityCustomer->getCustomerCode();
                if ($card_code != '') {
                    $customer->card_code = $card_code;
                }
                if ($customer_code != '') {
                    $customer->customer_code = $customer_code;
                }
                //audit_track($customer, __METHOD__);
                $customer->save();
            }
            $customer->forceLogin();

            //ok, now we have to add 50 points to the Fidelity Card, but only if the Customer has been registered from the Fidelity Registration page, so check the 'old' value for 'customer_code'
            if ($fidelityCustomer) {
                $fidelityCustomer->addPoints(config('negoziando.card.register_points', 50));
            } else {
                audit_error('Warning: cannot add register_points to Fidelity, here are the details:');
                audit_error($oldCustomerCode, __METHOD__ . '::oldCustomerCode');
                audit_error($customer, __METHOD__ . '::Customer');
                audit_error($fidelityCustomer, __METHOD__ . '::fidelityCustomer');
            }

            $groupRedirect = $this->handleCustomerGroupRedirect($customer);
            if(null !== $groupRedirect){
                return $groupRedirect;
            }

            return 'redirect:' . \Link::to('page', config('negoziando.fidelity_page_id', false)) . '?fe_scope_name=register_activated';
        }

        $groupRedirect = $this->handleCustomerGroupRedirect($customer);
        if(null !== $groupRedirect){
            return $groupRedirect;
        }

        return 'success';
    }

    /**
     * @param Customer $customer
     * @return string|null
     */
    protected function handleCustomerGroupRedirect(Customer $customer){
        $customerGroup = $this->getCustomerGroupByGrpCode();
        if(null === $customerGroup){
            $customerGroup = $customer->getAltCustomerGroup();
        }
        if($customerGroup){
            $customer->addAltGroup($customerGroup->id);
            \Core::resetVolatileSessionForCustomer();
            $customer->forceLogin();
            if($link = $customerGroup->getRedirectLinkAttribute()){
                return 'redirect:' . $link . '?fe_scope_name=register_activated';
            }
        }
        return null;
    }

    /**
     * @param $email
     * @param $passwd
     * @param $return
     * @return array
     */
    public function login($email, $passwd, $return = '')
    {
        $redirect = null;
        $email = Str::lower(trim($email));
        $action = Str::lower(trim(Input::get('inner-action', 'login')));
        $passwd = trim($passwd);
        $errors = [];
        if ($email === '') {
            $errors[] = trans('ajax.email_mandatory');
        } else {
            if (!\Utils::isEmail($email)) {
                $errors[] = trans('ajax.email_must_valid');
            }
        }
        if ($passwd === '') {
            $errors[] = trans('ajax.password_mandatory');
        }

        if ($email !== '' AND $passwd !== '') {

            if (count($errors) === 0) {
                $customer = Customer::whereEmail($email)->whereActive(1)->whereGuest(0)->first();
                if ($customer and $customer->id > 0) {
                    if (Hash::check($passwd, $customer->passwd)) {

                        if (feats()->enabled('recaptcha')) {
                            $response = Input::get('g-recaptcha-response');
                            $validRecaptcha = \Recaptcha::verifyResponse($response, $action);
                            if ($validRecaptcha == false) {
                                $errors[] = trans('ajax.recaptcha');
                            }
                        }

                        $this->customLoginErrors($customer, $errors);

                        if (count($errors) === 0) {
                            Session::put('blade_auth', $customer->id);
                            FrontUser::checkCart();
                            //$errors[] = 'stop';
                            $payload = $this->userAuthenticated($customer);
                            if(isset($payload['redirect'])){
                                $redirect = $payload['redirect'];
                            }
                        }

                    } elseif (md5($passwd) == $customer->passwd_md5) { //even check for MD5 version of the password, in order to be compatible with Magento

                        if (feats()->enabled('recaptcha')) {
                            $response = Input::get('g-recaptcha-response');
                            $validRecaptcha = \Recaptcha::verifyResponse($response, $action);
                            if ($validRecaptcha == false) {
                                $errors[] = trans('ajax.recaptcha');
                            }
                        }

                        if (count($errors) === 0) {
                            $customer->passwd = Hash::make($passwd);
                            $customer->passwd_md5 = null;
                            $customer->save();
                            Session::put('blade_auth', $customer->id);
                            FrontUser::checkCart();
                            //$errors[] = 'stop';
                            $payload = $this->userAuthenticated($customer);
                            if(isset($payload['redirect'])){
                                $redirect = $payload['redirect'];
                            }
                        }

                    } else {
                        $errors[] = trans('ajax.password_incorrect');
                    }
                } else {
                    $errors[] = trans('ajax.account_disabled');
                }
            }

        }


        if (count($errors) === 0) {
            $success = true;
            $msg = trans('ajax.login_ok');
            if (null === $redirect && $return !== '') {
                if(\core::isUrlStrictToSameHost($return)){
                    $redirect = $return;
                }
            }
        } else {
            $success = false;
            $msg = implode('<br>', $errors);
        }

        return compact('success', 'msg', 'errors', 'redirect');
    }

    /**
     * @return bool
     */
    protected function hasGrpCode()
    {
        return $this->getGrpCode() > 0;
    }

    /**
     * @return int
     */
    protected function getGrpCode()
    {
        return (int)\Session::get('blade_customer_group_id');
    }

    /**
     * @return CustomerGroup|null
     */
    protected function getCustomerGroupByGrpCode()
    {
        return $this->hasGrpCode() ? CustomerGroup::getObj($this->getGrpCode()) : null;
    }

    /**
     * @param Customer $customer
     * @return array
     */
    protected function userAuthenticated(Customer $customer)
    {
        $data = [];
        $fidelity_enabled = feats()->switchEnabled(\Core\Cfg::SERVICE_FIDELITY_CARD_REGISTER);
        if ($fidelity_enabled) {
            try {
                audit($customer, 'Getting fidelity customer');
                $fidelityCustomer = $this->getFidelityCustomer($customer->email, $customer->card_code, $customer->customer_code);

                if ($fidelityCustomer) {
                    audit($fidelityCustomer->toArray(), __METHOD__);
                    $customerRepository = $this->getFidelityCustomerRepository();
                    audit('Syncing customer and fidelityCustomer');
                    $customerRepository->sync($customer, $fidelityCustomer);
                } else {
                    $customerRepository = $this->getFidelityCustomerRepository();

                    //create
                    $customerRepository->setCustomer($customer);
                    $fidelityCustomer = $customerRepository->insertFidelityCustomer();
                    $card_code = $fidelityCustomer->getCardCode();
                    $customer_code = $fidelityCustomer->getCustomerCode();
                    if ($card_code != '') {
                        $customer->card_code = $card_code;
                    }
                    if ($customer_code != '') {
                        $customer->customer_code = $customer_code;
                    }
                    $customer->save();
                }
            } catch (Exception $e) {
                audit($e->getMessage(), __METHOD__);
                audit($e->getTraceAsString(), __METHOD__);
            }
        }
        // add alternative group to customer
        if($this->hasGrpCode()){
            $customer->addAltGroup($this->getGrpCode());
            $customerGroup = $this->getCustomerGroupByGrpCode();
            if($customerGroup && $link = $customerGroup->getRedirectLinkAttribute()){
                $data['redirect'] = $link;
            }
            \Core::resetVolatileSessionForCustomer();
        }
        //call CartManager login event
        \CartManager::onCustomerLogin();
        return $data;
    }


    /**
     * @return array
     */
    protected function getRequiredRegisterDataFidelity()
    {
        $data = Input::all();
        $profile = Input::get('profile');
        $marketing = Input::get('marketing');

        $errors = [];

        if ($profile == null) {
            $errors[] = trans("ajax.personal_consent_data");
        }
        if ($marketing == null) {
            $errors[] = trans("ajax.personal_consent_marketing");
        }

        if (!\Utils::isEmail($data['email'])) {
            $errors[] = trans("ajax.email_must_valid");
        }

        $required = [
            'email' => trans("ajax.email_address"),
            'passwd' => trans("ajax.password"),
            'confirm_passwd' => trans("ajax.password_confirm"),
        ];

        $required['firstname'] = trans("ajax.firstname");
        $required['lastname'] = trans("ajax.lastname");
        $required['day'] = trans("ajax.birth_day");
        $required['month'] = trans("ajax.birth_month");
        $required['year'] = trans("ajax.birth_year");

        $use_address = Input::get('use_address') == '1';

        if ($use_address) {

            $required += [
                'address1' => trans("ajax.address"),
                'address2' => trans("ajax.address2"),
                'postcode' => trans("ajax.postcode"),
                'city' => trans("ajax.city"),
                'country_id' => trans("ajax.country"),
                'state_id' => trans("ajax.state"),
                'phone' => trans("ajax.phone"),
            ];

            if ($data['people_id'] == '1') {
                if (\Config::get('app.cf_required', true)) {
                    $required['cf'] = trans('ajax.cf');
                }
            }

        }

        if (isset($data['country_id'])) {
            $country = Country::getObj($data['country_id']);
            if ($country AND $country->hasStates() == false) {
                unset($required['state_id']);
            }
            if ($country and $country->iso_code) {
                $valid = ZipCode::validate($data['postcode'], $country->iso_code);
                if ($valid === false) {
                    $errors[] = trans("validation.zip", ['attribute' => trans("ajax.postcode")]);
                }
            }
            if (is_null($country)) {
                if (isset($required['country_id']))
                    unset($required['country_id']);

                if (isset($required['state_id']))
                    unset($required['state_id']);
            }
        }

        foreach ($required as $key => $field) {
            if (trim($data[$key]) == '') {
                $errors[] = trans("ajax.mandatory_field", ['field' => $field]);
            }
        }
        if (Str::length($data['passwd']) < 6) {
            $errors[] = trans("ajax.password_6");
        }
        if (Str::length($data['passwd']) >= 6 AND $data['passwd'] != $data['confirm_passwd']) {
            $errors[] = trans("ajax.password_mismatch");
        }

        $email = Str::lower(trim($data['email']));

        if (Str::length($email) > 0 and !$this->isEmailUnique($email)) {
            $errors[] = trans("ajax.email_used");
        }

        $hasDate = ($data['year'] > 0 and $data['month'] > 0 and $data['day'] > 0);
        if (config('auth.customer.avoid_minors', false) === true and $hasDate and $data['people_id'] == '1') {
            try {
                $date = Carbon::create($data['year'], $data['month'], $data['day']);
                if ($date->diffInYears() < 18) {
                    $errors[] = trans("ajax.registration_minor");
                }
            } catch (Exception $e) {
                $errors[] = trans("ajax.registration_minor");
            }
        }

        if (feats()->enabled('recaptcha') and count($errors) == 0) {
            $response = Input::get('g-recaptcha-response');
            $validRecaptcha = \Recaptcha::verifyResponse($response, 'register');
            if ($validRecaptcha == false) {
                $errors[] = trans("ajax.recaptcha");
            }
        }

        return $errors;
    }


    /**
     * @return array
     */
    public function register_fidelity()
    {
        $errors = $this->getRequiredRegisterDataFidelity();

        if (count($errors) == 0) {
            //insert customer
            try {
                $customer_data = $this->getCustomerData();
                $customer_data['customer_code'] = 'unassigned'; //this will flag the customer, so when we active this user we know that has been registered from here

                $customer = new Customer();
                $customer->setAttributes($customer_data);

                audit($customer->toArray(), __METHOD__ . '::insertCustomer');

                $customer->save();
                $address = $this->insertCustomerAddress(0, $customer);

                if ($address)
                    audit($address->toArray(), __METHOD__ . '::insertAddress');

                if ($customer->id > 0) {
                    $customer->handleNewsletter();
                }

                $this->sendActivationMail($customer);
            } catch (Exception $e) {
                audit($e->getMessage(), "REGISTRATION EXCEPTION");
                audit($e->getTraceAsString());
                \Utils::error($e->getMessage(), "REGISTRATION EXCEPTION");
                \Utils::error($e->getTraceAsString());
                $errors[] = trans("ajax.registration_error");
            }
        }

        if (count($errors) == 0) {
            $success = true;
            $msg = trans("ajax.registration_ok");
        } else {
            $success = false;
            $msg = implode("<br>", $errors);
        }

        return compact('success', 'msg');
    }

    /**
     * @param $email
     * @param $flags
     * @return array
     */
    public function updateCustomerGdprFlagsByEmail($email, $flags)
    {
        $customer = Customer::active()->where('email', $email)->first();
        return $this->updateCustomerGdprFlags($customer, $flags);
    }

    /**
     * @param Customer $customer
     * @param $flags
     * @return array
     */
    public function updateCustomerGdprFlags($customer, $flags)
    {
        audit($flags, __METHOD__);
        $success = false;
        $error = null;
        if ($customer) {
            try {
                $fields = ['marketing', 'profile', 'newsletter'];
                foreach ($fields as $field) {
                    if (isset($flags[$field])) {
                        $customer->$field = $flags[$field];
                    }
                }
                audit($customer->toArray(), __METHOD__ . '::saving');
                $customer->broadcast_events = false;
                $customer->save();
                $success = true;
            } catch (Exception $e) {
                $error = $e->getMessage();
                audit_exception($e);
            }
        } else {
            $error = "Customer not found";
        }
        return compact('success', 'error');
    }

    /**
     * @param $email
     * @param $flags
     * @return array
     */
    public function updateNewsletterGdprFlagsByEmail($email, $flags)
    {
        $newsletter = Newsletter::where('email', $email)->first();
        return $this->updateNewsletterGdprFlags($newsletter, $flags);
    }

    /**
     * @param $newsletter
     * @param $flags
     * @return array
     */
    public function updateNewsletterGdprFlags($newsletter, $flags)
    {
        audit($flags, __METHOD__);
        $success = false;
        $error = null;
        if ($newsletter) {
            try {
                $fields = ['marketing', 'profile'];
                foreach ($fields as $field) {
                    if (isset($flags[$field])) {
                        $newsletter->$field = $flags[$field];
                    }
                }
                audit($newsletter->toArray(), __METHOD__ . '::saving');
                $newsletter->broadcast_events = false;
                $newsletter->save();
                $success = true;
            } catch (Exception $e) {
                $error = $e->getMessage();
                audit_exception($e);
            }
        } else {
            $error = "Newsletter not found";
        }
        return compact('success', 'error');
    }

    /**
     * @param Customer $customer
     */
    public function broadcastCustomerGdprFlags(Customer $customer)
    {
        if ($this->isRemoteEnabled() === false)
            return;

        $route = 'api/remote/gdpr-customer-flags';
        $payload = [
            'marketing' => $customer->marketing,
            'profile' => $customer->profile,
            'newsletter' => $customer->newsletter,
            'email' => $customer->email
        ];
        audit($payload, __METHOD__ . '::payload');
        $response = $this->remoteRepository->setParams($payload)->broadcast()->post($route)->response();
        audit($response, __METHOD__ . '::response');
    }

    /**
     * @param Newsletter $newsletter
     */
    public function broadcastNewsletterGdprFlags(Newsletter $newsletter)
    {
        audit(__METHOD__);
        if ($this->isRemoteEnabled() === false)
            return;

        $route = 'api/remote/gdpr-newsletter-flags';
        $payload = [
            'marketing' => $newsletter->marketing,
            'profile' => $newsletter->profile,
            'email' => $newsletter->email
        ];
        audit($payload, __METHOD__ . '::payload');
        $response = $this->remoteRepository->setParams($payload)->broadcast()->post($route)->response();
        audit($response, __METHOD__ . '::response');
    }

    /**
     * @return bool
     */
    private function isRemoteEnabled()
    {
        if (isset($this->remote_enabled))
            return $this->remote_enabled;

        $this->remote_enabled = config('services.remote', false);
        return $this->remote_enabled;
    }

    /**
     * @param $email
     * @return array
     */
    public function passwordRecovery($email)
    {

        $email = \Str::lower(trim($email));

        $errors = [];
        if ($email == "") {
            $errors[] = trans("ajax.email_mandatory");
        } else {
            if (!\Utils::isEmail($email)) {
                $errors[] = trans("ajax.email_must_valid");
            }
        }

        if ($email != "") {

            if (count($errors) == 0) {
                $customer = \Customer::whereEmail($email)->whereActive(1)->first();
                if ($customer and $customer->id > 0) {

                    if (feats()->enabled('recaptcha')) {
                        $response = Input::get('g-recaptcha-response');
                        $validRecaptcha = \Recaptcha::verifyResponse($response, 'password_recovery');
                        if ($validRecaptcha == false) {
                            $errors[] = trans("ajax.recaptcha");
                        }
                    }

                    if (count($errors) == 0) {
                        $new_passwd = \Core::randomString(8, false);
                        $customer->passwd = \Hash::make($new_passwd);
                        $customer->last_passwd_gen = \Format::now();
                        $customer->save();
                        $name = ($customer->company != '') ? $customer->company : $customer->firstname . " " . $customer->lastname;
                        $mail_data = [
                            'name' => $name,
                            'passwd' => $new_passwd,
                        ];
                        $code = 'PASSWORD_RECOVERY';
                        $mail = \Email::getByCode($code, \Core::getLang());
                        $mail->setCustomer($customer)->send($mail_data, $email);
                    }

                } else {
                    $errors[] = trans("ajax.account_disabled");
                }
            }

        }


        if (count($errors) == 0) {
            $success = true;
            $msg = trans("ajax.password_retrieve_ok");
        } else {
            $success = false;
            $msg = implode("<br>", $errors);
        }
        return compact("success", "msg");
    }

    /**
     * @return array
     */
    public function logout()
    {
        $success = true;
        \Session::forget('blade_auth');
        \CartManager::forgetCheckout();
        \CartManager::onCustomerLogout();
        $msg = trans("ajax.logout_ok");
        return compact('success', 'msg');
    }

    /**
     * @param Customer $customer
     * @param $errors
     */
    private function customLoginErrors(Customer $customer, &$errors)
    {
        $this->strapUserRepository->customLoginErrors($customer, $errors);
    }


    /**
     * @return array
     */
    public function register_b2b_straps()
    {
        $errors = $this->strapUserRepository->getRequiredRegisterDataB2B();

        if (count($errors) == 0) {
            //send email
            try {
                $this->strapUserRepository->sendRegisterB2bEmail();
            } catch (Exception $e) {
                $errors[] = trans("ajax.registration_error");
            }
        }

        if (count($errors) == 0) {
            $success = true;
            $msg = trans("custom.b2b.register.registration_ok");
        } else {
            $success = false;
            $msg = implode("<br>", $errors);
        }

        return compact('success', 'msg');
    }
}