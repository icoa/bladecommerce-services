<?php  namespace services;

use DB;
use Exception;
use SimpleXMLElement;
use XMLReader;

abstract class AbstractImporter {

    /**
     * @var
     */
    protected $lastError;

    /**
     * @var
     */
    protected $parent;

    /**
     * @return mixed
     */
    abstract protected function createNodeRules();

    /**
     * @return mixed
     */
    abstract protected function prepareDB();

    /**
     * @return mixed
     */
    abstract protected function processNode();

    /**
     * @param XMLReader $reader
     */
    function __construct(XMLReader $reader)
    {
        $this->reader = $reader;

        $this->createNodeRules();
    }

    /**
     * @param string $file
     * @return mixed
     */
    public function import($file)
    {
        $this->lastError = "";

        try
        {
            $this->prepareDB();

            DB::beginTransaction();

            // Load the selected XML file to the DOM
            $this->reader->open($file);

            // Var to store parent name
            $this->parent = "";

            // For each node
            while ($this->reader->read()) {

                $this->processNode();

            }

            DB::commit();

            $this->reader->close();

            return true;

        }
        catch (Exception $e)
        {
            DB::rollBack();

            $this->lastError = $e->getMessage();

            $this->reader->close();

            return false;
        }

    }

    /**
     * @param $name
     * @return string
     * @throws Exception
     */
    protected function checkFieldName($name)
    {
        $fieldName = $this->convertFromCamelCaseToUnderscore($name);
        $fieldName = $this->formatField($fieldName);

        if (!array_key_exists($fieldName, $this->fieldRules)) {
            //throw new Exception("ERROR: the node '$fieldName' is not allowed.'");
        }

        return $fieldName;
    }

    /**
     * @param $name
     * @param $value
     * @return int
     */
    protected function checkFieldValue($name, $value)
    {
        return $value;
    }

    /**
     * @param $fieldName
     * @return mixed
     */
    protected function formatField($fieldName)
    {
        return $fieldName;
    }

    /**
     * @param SimpleXMLElement $node
     * @return array
     */
    protected function getFieldsFromNode(SimpleXMLElement $node)
    {
        $fields = [];

        foreach ($node->children() as $field)
        {
            $name = $field->getName();

            $fieldName = $this->checkFieldName($name);

            $fieldValue = $this->checkFieldValue($fieldName, (string) $field);

            $fields[$fieldName] = $fieldValue;

            $this->processAttributes($fields, $name, $field->attributes());
        }

        return $fields;
    }

    /**
     * @param $table
     * @param $fieldRules
     */
    protected function createTableFields($table, $fieldRules)
    {
        foreach ($fieldRules as $field => $options)
        {
            $type = $options["type"];

            if ($type != "subnode")
            {
                $nullable = array_key_exists("nullable", $options) ? $options["nullable"] : true;

                if ($type == "string") {
                    $length = array_key_exists("length", $options) ? $options["length"] : 256;
                    if ($nullable) {
                        $table->$type($field, $length)->nullable();
                    } else {
                        $table->$type($field, $length);
                    }

                } else if ($type == "decimal") {
                    $precision = array_key_exists("precision", $options) ? $options["precision"] : 10;
                    $scale = array_key_exists("scale", $options) ? $options["scale"] : 3;
                    if ($nullable) {
                        $table->$type($field, $precision, $scale)->nullable();
                    } else {
                        $table->$type($field, $precision, $scale);
                    }

                } else {
                    if ($nullable) {
                        $table->$type($field)->nullable();
                    } else {
                        $table->$type($field);
                    }

                }

            }
        }
    }

    /**
     * @param $fields
     * @param $name
     * @param $attributes
     */
    protected function processAttributes(&$fields, $name, $attributes)
    {
        if ($attributes) {
            foreach ($attributes as $key => $value)
            {
                $attributeName = $this->checkFieldName($name . $key);

                $attributeValue = $this->checkFieldValue($attributeName, (string) $value);

                $fields[$attributeName] = $attributeValue;
            }
        }
    }

    /**
     * @param $input
     * @return string
     */
    protected function convertFromCamelCaseToUnderscore($input)
    {
        preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $input, $matches);
        $ret = $matches[0];
        foreach ($ret as &$match) {
            $match = $match == strtoupper($match) ? strtolower($match) : lcfirst($match);
        }
        return implode('_', $ret);
    }

    /**
     * @return mixed
     */
    public function getLastError()
    {
        return $this->lastError;
    }

    protected function xmlToDb($string){
        return mb_convert_encoding($string, 'UTF-8', 'Windows-1252');
    }

    protected function DbToXml($string){
        return mb_convert_encoding($string, 'Windows-1252', 'UTF-8');
    }

}